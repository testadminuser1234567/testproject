<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex,nofollow">
<meta name="description" content="仙台・宮城・東北地方の求人多数！U Iターン転職希望、地元で仕事探し、求人情報をお求めの方は、転職エージェントのヒューレックスへ。" />
<meta http-equiv="imagetoolbar" content="no" />
<meta http-equiv="Content-Language" content="ja" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="JavaScript" />
<link rel="shortcut icon" href="/favicon.ico" />
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all" />
<link href="../css/fb.css" rel="stylesheet" type="text/css" media="all" />
<link href="./form.css" rel="stylesheet" type="text/css" />
<title>東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<meta name="copyright" content="" />
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="../js/rollover.js"></script>
<script type="text/javascript" src="../js/ga.js"></script>
<script type="text/javascript" src="./form_validation.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>
<?php require_once("../entry/system/esc.php");?>
<div id="fbPage" class="page common">
<h1><img src="img/logo.gif" /></h1>
<p id="mainContents">
<?php if($apply=="適正年収診断"):?>
<img src="img/form_main_01.jpg" alt="適正年収診断申し込みフォーム" />
<?php elseif($apply=="応募書類の作成代行サービス"):?>
<img src="img/form_main_02.jpg" alt="応募書類の作成代行サービス申し込みフォーム" />
<?php elseif($apply=="転職可能性個別カウンセリング"):?>
<img src="img/form_main_03.jpg" alt="転職可能性個別カウンセリング申し込みフォーム" />
<?php endif;?>
</p>
<p>下記の内容で送信致します。</p>
<table width="810" class="normal">
<tr>
<th><label for="shimei">氏名<span class="point">※</span></label></th>
<td><?php echo esc($shimei);?></td>
</tr>
<tr>
<th><label for="kana">フリガナ<span class="point">※</span></label></th>
<td><?php echo esc($kana);?></td>
</tr>
<tr>
<th><label for="tel1">電話<span class="point">※</span></label></th>
<td><?php echo esc($tel1);?></td>
</tr>
<tr>
<th><label for="mail1">メール<span class="point">※</span></label></th>
<td><?php echo esc($mail1);?></td>
</tr>
<tr class="career">
<th>転職意欲<span class="point">※</span></th>
<td><?php echo esc($iyoku);?></td>
</tr>
<tr class="career">
<th>連絡可能時間帯<span class="point">※</span></th>
<td>平日：<?php echo esc($renraku1);?><br />
土日：<?php echo esc($renraku2);?></td>
</tr>
</table>
<!-- start -->
<div class="btnSend">
<table style="margin-right:auto;margin-left:auto">
<tr>
<td>
<form id="entryForm" action="mail.php" method="post">
<input type="hidden" name="mode" value="return">
<input type="image" style="height:36px;" value="戻る" src="./img/btn_back.gif" alt="戻る" />
<input type="hidden" name="shimei" value="<?php echo esc($shimei);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="tel1" value="<?php echo esc($tel1);?>">
<input type="hidden" name="mail1" value="<?php echo esc($mail1);?>">
<input type="hidden" name="agree" value="<?php echo esc($agree);?>">

<input type="hidden" name="renraku1" value="<?php echo esc($renraku1);?>">
<input type="hidden" name="renraku2" value="<?php echo esc($renraku2);?>">

<input type="hidden" name="iyoku" value="<?php echo esc($iyoku);?>">

<input type="hidden" name="apply" value="<?php echo esc($apply);?>">
</form>
</td>
<td>
<form id="entryForm" action="mail.php" method="post">
<input name="btnKakunin2" type="image" id="btnKakunin2"  style="float:left;;margin-left:5px;" value="送信する" src="img/btn_send.gif" alt="上記の内容で送信" />
<input type="hidden" name="mode" value="send">
<input type="hidden" name="shimei" value="<?php echo esc($shimei);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="tel1" value="<?php echo esc($tel1);?>">
<input type="hidden" name="mail1" value="<?php echo esc($mail1);?>">
<input type="hidden" name="agree" value="<?php echo esc($agree);?>">

<input type="hidden" name="renraku1" value="<?php echo esc($renraku1);?>">
<input type="hidden" name="renraku2" value="<?php echo esc($renraku2);?>">

<input type="hidden" name="iyoku" value="<?php echo esc($iyoku);?>">

<input type="hidden" name="apply" value="<?php echo esc($apply);?>">

</form>
</td>
</tr>
</table>
</div>
<!-- end -->
</form>
</div>
<div id="fb-root"></div>
<script src="https://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
  FB.init({
    appId  : '1482840638618071',
    oauth  : true,
  });

  FB.Canvas.setAutoGrow();

  function growSize() {
    var height = Number(
      document.getElementById('myContent').style.height.split('px')[0])
      + 100;
    document.getElementById('myContent').style.height = height + 'px';
  }

    $(function(){
      FB.Canvas.scrollTo(0,0);
    });

</script>


<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>