<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex,nofollow">
<meta name="description" content="仙台・宮城・東北地方の求人多数！U Iターン転職希望、地元で仕事探し、求人情報をお求めの方は、転職エージェントのヒューレックスへ。" />
<meta http-equiv="imagetoolbar" content="no" />
<meta http-equiv="Content-Language" content="ja" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="JavaScript" />
<link rel="shortcut icon" href="/favicon.ico" />
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all" />
<link href="../css/fb.css" rel="stylesheet" type="text/css" media="all" />
<title>東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<meta name="copyright" content="" />
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="../js/rollover.js"></script>
<script type="text/javascript" src="../js/ga.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>
<div id="fbPage" class="page common">
<h1><img src="img/logo.gif" /></h1>
<p id="mainContents"><img src="img/index_main.jpg" alt="仕事を通して発展・成長を。東北でキャリアアップ！！" /></p>
<p class="Lead">東北地方では復興需要や景気の回復に伴い、求人数が過去に例を見ないほど多くなっています。<br />
Ｕ・Ｉターンやキャリアアップを図るには今がまさに絶好のチャンス！<br />
この度、東北地方に縁があり素晴らしいご経歴をお持ちの方を対象に特別な３つのコンテンツをご用意いたしました。<br />
今すぐに転職したい方からこれから転職を検討される方まで、どうぞお気軽にご利用下さい！</p>
<p class="aCenter"><img src="img/index_img_01.gif" alt="contents01 東北地方での適正年収診断 contents02 選考を勝ち抜く応募書類の作成代行サービス contents03 東北地方での転職可能性は？(個別カウンセリング)" /></p>
<div class="titleBox">
<h2><img src="img/index_title_01.gif" alt="東北地方でのあなたの適正年収を診断" /></h2>
<div class="titleBoxSub">
<h3><img src="img/ico_01.gif" alt="" />こんな疑問を感じた事はありませんか？</h3>
<ul class="list01">
<li>今もらっている給料って多いのかな？少ないのかな？</li>
<li>Uターンで転職したらどのくらいの給料になるんだろう？</li>
</ul>
<p class="txt">経験によって給与は千差万別。<br />
東北の転職マーケットを知りつくしたコンサルタントがあなたのキャリアをお聞きした上で、「東北地方でのあなたの適正年収」を診断します。</p>
<h3><img src="img/ico_02.gif" alt="" />具体的な流れ</h3>
<p class="aCenter"><img src="img/index_img_02.gif" alt="具体的な流れ" /></p>
<p class="aCenter btn"><a href="https://www.hurex.jp/fb/form.php?p=1"><img src="img/index_btn_01.gif" alt="適正年収診断に申し込む（無料！）" /></a></p>
</div>
</div>
<p class="line"><img src="img/line.gif" /></p>
<div class="titleBox">
<h2><img src="img/index_title_02.gif" alt="選考を勝ち抜く応募書類の作成代行サービス" /></h2>
<div class="titleBoxSub">
<h3><img src="img/ico_01.gif" alt="" />こんな事を感じたことはありませんか？</h3>
<ul class="list01">
<li>仕事が忙しくて職務経歴書なんて作る時間が無い…</li>
<li>転職をするのが初めてで書類の作り方が解らない…</li>
</ul>
<p class="txt">書類作成の段階から始っています。選考を有利に進めるため、あなたの良さを存分にアピールした応募書類の作成を専門のコンサルタントが代行します。</p>
<h3><img src="img/ico_02.gif" alt="" />具体的な流れ</h3>
<p class="aCenter"><img src="img/index_img_03.gif" alt="具体的な流れ" /></p>
<p class="aCenter btn"><a href="https://www.hurex.jp/fb/form.php?p=2"><img src="img/index_btn_02.gif" alt="応募書類の作成代行サービスに申し込む（無料！）" /></a></p>
</div>
</div>
<p class="line"><img src="img/line.gif" /></p>
<div class="titleBox">
<h2><img src="img/index_title_03.gif" alt="東北地方での転職可能性個別カウンセリング" /></h2>
<div class="titleBoxSub">
<h3><img src="img/ico_01.gif" alt="" />こんな事を考えたことはありませんか？</h3>
<ul class="list01">
<li>自分の経験でいったいどの様な会社へ転職できるのだろう？</li>
<li>いつかは東北に帰りたいけど、本当にUターン出来るかな？</li>
</ul>
<p class="txt">東北地方の転職マーケットは情報量が少ないためよく分からないのではないでしょうか。<br />
東北の求人情報に詳しいコンサルタントがあなたの希望条件に照らし合わせて東北地方での転職が成功する可能性を診断します。</p>
<h3><img src="img/ico_02.gif" alt="" />具体的な流れ</h3>
<p class="aCenter"><img src="img/index_img_04.gif" alt="具体的な流れ" /></p>
<p class="aCenter btn"><a href="https://www.hurex.jp/fb/form.php?p=3"><img src="img/index_btn_03.gif" alt="転職可能性個別カウンセリングに申し込む（無料！）" /></a></p>
</div>
</div>
</div>
<div id="fb-root"></div>
<script src="https://connect.facebook.net/en_US/all.js"></script>
<script type="text/javascript">
  FB.init({
    appId  : '1482840638618071',
    oauth  : true,
  });

  FB.Canvas.setAutoGrow();

  function growSize() {
    var height = Number(
      document.getElementById('myContent').style.height.split('px')[0])
      + 100;
    document.getElementById('myContent').style.height = height + 'px';
  }
  
  
  

    $('.btn a').click(function() {
      FB.Canvas.scrollTo(0,0);
    });


</script>


<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>