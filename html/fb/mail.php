<?php
session_cache_limiter('private_no_expire');
session_start();

include (dirname(__FILE__) . "/system/template/template.php");
include (dirname(__FILE__) . "/system/validation.php");
//クラス読み込み
$template = new template();
$validation = new Validation();

//セッションにデータを格納
setSession($_POST);

//セッションデータをname属性の名前に設定（ボディで使用するため
include (dirname(__FILE__) . "/system/set_session.php");

//コンフィグ読込
include (dirname(__FILE__) . "/config.php");

//送信
if($_POST["mode"] == "send"){
	include (dirname(__FILE__) . "/system/send.php");
//戻る
}else if($_POST["mode"] == "return"){
	$template->view(Index, $_POST, $err);
//確認処理
}else if($_POST["mode"] == "confirm"){
	//セッションにデータを格納
	setSession($_POST);

	//エラーチェック
	$err = "";
	$err = $validation->run();

/*
	if(empty($_POST["tel1"]) && empty($_POST['tel2'])){
		$err .= "電話は自宅もしくは携帯のどちらかは必ず御入力下さい。";
	}
	if(empty($_POST["mail1"]) && empty($_POST['mail2'])){
		$err .= "メールは自宅もしくは携帯のどちらかは必ず御入力下さい。";
	}
*/

	//エラーがあったら初期画面へ
	if(!empty($err)){
		$template->view(Index, $_SESSION, $err);
	}else{
		$template->view(Confirm, $_SESSION, "");
	}
//その他
}else{
	$data="";
	$err="";
	header("HTTP/1.1 200 ok");
	$url = Index;
	header("Location: $url");
	exit();
}

/*
/  セッションにデータ格納
*/
function setSession($posts){
	foreach($posts as $key => $val){
		unset($_SESSION[$key]);
		$_SESSION[$key] = $posts[$key];
	}
}
?>