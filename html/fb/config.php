<?php
//入力ページの設定
define('Index', 'form.php');
//確認ページの設定（使わない場合はこのまま）
define('Confirm', 'confirm.php');
//完了ページの設定
define('Finish', 'finish.php');

define('ENCODE', 'UTF-8');

//ユーザの名前設定
$unit_name = $shimei . "様";
//ユーザの宛先設定
$email = $mail1;
$today = Date('Y年m月d日');

//サンクスメール設定
$thanks_mail_subject = $apply . "申込みありがとうございます。";
$thanks_mail_from = "touroku@hurex.co.jp";
//$thanks_mail_from = "tosa@smt-net.co.jp";
$thanks_mail_str = "ヒューレックス株式会社";
$thanks_body= <<< EOF
$shimei 様

この度はヒューレックス $apply 申込みいただき、
誠にありがとうございます。

後ほど担当者よりご連絡させていただきます。

弊社では、個人情報保護の観点から、個人情報に関する適切な取り扱い
および管理に努めております。
ご登録頂いた個人情報は、職業紹介の目的にのみ使用し
他の目的で使用することはございません。

─────────────────────────────□■─
　このメールに関するお問い合わせは、人材紹介サービス担当まで
────────────────────────────────
ヒューレックス株式会社
〒980-6117 宮城県仙台市青葉区中央1-3-1 アエル17階
TEL 0120-14-1150
E-mail touroku@hurex.co.jp
――――――――――――――――――――――――――――――――

このメール配信に心当たりのない方は、大変お手数ですが
下記までご連絡ください。
touroku@hurex.co.jp

EOF;

//通知メール設定
$order_mail_subject = "【登録通知】ENTRY（facebookページ）";
$order_mail_to[] = array('touroku@hurex.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('tosa@smt-net.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('nobuyoshi.takahashi@hurex.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('t-kojima@smt-net.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('info@smt-net.co.jp', '追加宛先');
$order_body= <<< EOF
ご担当者様
ホームページ(facebookページ）より、 $shimei 様のエントリーがありました。
HRBCにログインし、エントリー情報をご確認下さい。

EOF;

//項目チェック用
$validation->set_rules('agree','個人情報保護方針','required');
$validation->set_rules('shimei','氏名','required|max_length[25]');
$validation->set_rules('kana','フリガナ','required|max_length[25]');
//$validation->set_rules('year','年','required');
//$validation->set_rules('month','月','required');
//$validation->set_rules('day','日','required');
//$validation->set_rules('sex','性別','required');
//$validation->set_rules('zip','郵便番号','required|max_length[8]|alpha_dash');
//$validation->set_rules('pref','都道府県','required');
//$validation->set_rules('address','住所','required|max_length[50]');
$validation->set_rules('tel1','電話','required|max_length[15]');
//$validation->set_rules('tel2','電話2','max_length[15]');
$validation->set_rules('mail1','メール','required|valid_email|max_length[50]');
//$validation->set_rules('mail2','メール2','valid_email|max_length[50]');
$validation->set_rules('renraku1','連絡可能時間帯（平日）','required');
$validation->set_rules('renraku2','連絡可能時間帯（土日）','required');
$validation->set_rules('iyoku','転職意欲','required');
//$validation->set_rules('kibo_jiki_year','転職希望年','required');
//$validation->set_rules('kibo_jiki_month','転職希望月','required');
//$validation->set_rules('kibo_jiki_day','転職希望日','required');
//$validation->set_rules('school_div_id','最終学歴','required');
//$validation->set_rules('school','学校名','max_length[50]|required');
//$validation->set_rules('company_number','転職回数','');
/*
$validation->set_rules('division','学部・学科名','max_length[30]');
$validation->set_rules('zaiseki_year_from','在籍年','');
$validation->set_rules('zaiseki_year_from','在籍月','');
$validation->set_rules('zaiseki_month_to','在籍年','');
$validation->set_rules('zaiseki_month_from','在籍月','');
$validation->set_rules('company_number','在籍社数','');
$validation->set_rules('jokyo','就業状況','');
$validation->set_rules('job1','経験職種','');
$validation->set_rules('jigyo_naiyo','事業内容','max_length[10000]');
$validation->set_rules('jigyo_naiyo2','事業内容2','max_length[10000]');
$validation->set_rules('jigyo_naiyo3','事業内容3','max_length[10000]');
$validation->set_rules('company_name','企業名','');
$validation->set_rules('biz1','業種','');
$validation->set_rules('income','年収','');
$validation->set_rules('shugyo_keitai','就業形態','');
$validation->set_rules('yakushoku','役職','');
$validation->set_rules('shugyo_year_from','就業年','');
$validation->set_rules('shugyo_month_from','就業月','');
$validation->set_rules('shugyo_year_to','就業年','');
$validation->set_rules('shugyo_month_to','就業月','');
$validation->set_rules('shugyo_naiyo','就業内容','max_length[10000]');
$validation->set_rules('shugyo_naiyo2','就業内容2','max_length[10000]');
$validation->set_rules('shugyo_naiyo3','就業内容3','max_length[10000]');
$validation->set_rules('comment','その他コンサルタントに伝えたいこと','max_length[10000]');
$validation->set_rules('kibo_biz1','希望業種','');
$validation->set_rules('kibo_job1','希望職種','');
$validation->set_rules('kibo_pref1','希望勤務地','');
$validation->set_rules('kibo_income','希望年収','');
$validation->set_rules('howto','弊社を知ったきっかけ','');
$validation->set_rules('toefl','TOEFL','numeric');
$validation->set_rules('toeic','TOEIC','numeric');
$validation->set_rules('kibo_income','希望年収','numeric');
*/


//ログファイル出力 on or off
$log = "off";

?>