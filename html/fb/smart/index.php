<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="index,follow">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<link rel="canonical" href="">
<link href="css/import.css" rel="stylesheet" type="text/css" media="all">
<title>東北でキャリアアップ!! | 東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="../../js/rollover.js"></script>
<script type="text/javascript" src="../../js/ga.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>
<div id="fbPage">
<p id="logo"><a href="index.php"><img src="img/logo.gif" width="146" height="43"></a></p>
<p id="mainContents"><img src="img/index_main.jpg" width="100%"></p>
<!-- co start -->
<div class="co">
<p>東北地方では復興需要や景気の回復に伴い、求人数が過去に例を見ないほど多くなっています。<br>
Ｕ・Ｉターンやキャリアアップを図るには今がまさに絶好のチャンス！<br>
この度、東北地方に縁があり素晴らしいご経歴をお持ちの方を対象に特別な３つのコンテンツをご用意いたしました。<br>
今すぐに転職したい方からこれから転職を検討される方まで、どうぞお気軽にご利用下さい！</p>
<!-- ankerBox start -->
<div id="ankerBox">
<ul>
<li> <a href="#a01">
<p>contents01</p>
<h2>東北地方での適正年収診断</h2>
</a></li>
<li> <a href="#a01">
<p>contents02</p>
<h2>選考を勝ち抜く応募書類の作成代行サービス</h2>
</a></li>
<li> <a href="#a03">
<p>contents03</p>
<h2>東北地方での転職可能性は？<span class="ss">(個別カウンセリング)</span></h2>
</a></li>
</ul>
</div>
<!-- ankerBox end -->
<!-- box start -->
<section class="box" id="a01">
<h1><span class="ss">contents01</span><br>
東北地方でのあなたの適正年収を診断</h1>
<div class="boxSub">
<h2 class="ico01">こんな疑問を感じた事はありませんか？</h2>
<ul class="list01">
<li>今もらっている給料って多いのかな？少ないのかな？</li>
<li>Uターンで転職したらどのくらいの給料になるんだろう？</li>
</ul>
<p>経験によって給与は千差万別。<br>
東北の転職マーケットを知りつくしたコンサルタントがあなたのキャリアをお聞きした上で、「東北地方でのあなたの適正年収」を診断します。</p>
<h2 class="ico02">具体的な流れ</h2>
<ul class="flow">
<li><img src="img/flow01_01.gif" width="100%"></li>
<li class="ya"><img src="img/flow_ya.gif" width="44" height="12"></li>
<li><img src="img/flow01_02.gif" width="100%"></li>
<li class="ya"><img src="img/flow_ya.gif" width="44" height="12"></li>
<li><img src="img/flow01_03.gif" width="100%"></li>
</ul>
<p class="aCenter btn"><a href="https://www.hurex.jp/fb/smart/form.php?p=1"><img src="img/btn_contact_01.gif" width="90%"></a></p>
</div>
</section>
<!-- box end -->
<!-- box start -->
<section class="box" id="a02">
<h1><span class="ss">contents02</span><br>
選考を勝ち抜く応募書類の作成代行サービス</h1>
<div class="boxSub">
<h2 class="ico01">こんな疑問を感じた事はありませんか？</h2>
<ul class="list01">
<li>仕事が忙しくて職務経歴書なんて作る時間が無い…</li>
<li>転職をするのが初めてで書類の作り方が解らない…</li>
</ul>
<p>書類作成の段階から始っています。選考を有利に進めるため、あなたの良さを存分にアピールした応募書類の作成を専門のコンサルタントが代行します。</p>
<h2 class="ico02">具体的な流れ</h2>
<ul class="flow">
<li><img src="img/flow02_01.gif" width="100%"></li>
<li class="ya"><img src="img/flow_ya.gif" width="44" height="12"></li>
<li><img src="img/flow02_02.gif" width="100%">
<p>※事前に履歴書のご準備をお願いさせて頂く場合がございます。</p>
</li>
<li class="ya"><img src="img/flow_ya.gif" width="44" height="12"></li>
<li><img src="img/flow02_03.gif" width="100%"></li>
</ul>
<p class="aCenter btn"><a href="https://www.hurex.jp/fb/smart/form.php?p=2"><img src="img/btn_contact_02.gif" width="90%"></a></p>
</div>
</section>
<!-- box end -->
<!-- box start -->
<section class="box" id="a03">
<h1><span class="ss">contents03</span><br>
東北地方での転職可能性個別カウンセリング</h1>
<div class="boxSub">
<h2 class="ico01">こんな疑問を感じた事はありませんか？</h2>
<ul class="list01">
<li>自分の経験でいったいどの様な会社へ転職できるのだろう？</li>
<li>いつかは東北に帰りたいけど、本当にUターン出来るかな？</li>
</ul>
<p>東北地方の転職マーケットは情報量が少ないためよく分からないのではないでしょうか。<br>
東北の求人情報に詳しいコンサルタントがあなたの希望条件に照らし合わせて東北地方での転職が成功する可能性を診断します。</p>
<h2 class="ico02">具体的な流れ</h2>
<ul class="flow">
<li><img src="img/flow03_01.gif" width="100%"></li>
<li class="ya"><img src="img/flow_ya.gif" width="44" height="12"></li>
<li><img src="img/flow03_02.gif" width="100%">
<p>※事前に職務経歴書・履歴書のご準備をお願いさせて頂く場合がございます。</p>
</li>
<li class="ya"><img src="img/flow_ya.gif" width="44" height="12"></li>
<li><img src="img/flow03_03.gif" width="100%"></li>
</ul>
<p class="aCenter btn"><a href="https://www.hurex.jp/fb/smart/form.php?p=3"><img src="img/btn_contact_03.gif" width="90%"></a></p>
</div>
</section>
<!-- box end -->
</div>
<!-- co end -->
</div>
<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>