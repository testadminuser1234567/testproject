<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="index,follow">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<link rel="canonical" href="">
<link href="css/import.css" rel="stylesheet" type="text/css" media="all">
<title>東北でキャリアアップ!! | 東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="../../js/rollover.js"></script>
<script type="text/javascript" src="./form_validation.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>
<div id="fbPage">
<p id="logo"><a href="index.php"><img src="img/logo.gif" width="146" height="43"></a></p>
<p id="mainContents">
<?php if($_GET['p']==1 || (!empty($apply) && $apply=="【スマートフォン】適正年収診断")):?>
<img src="img/form_main_01.jpg" width="100%">
<input type="hidden" name="apply" value="【スマートフォン】適正年収診断">
<?php elseif($_GET['p']==2 || (!empty($apply) && $apply=="【スマートフォン】応募書類の作成代行サービス")):?>
<img src="img/form_main_02.jpg" width="100%">
<input type="hidden" name="apply" value="【スマートフォン】応募書類の作成代行サービス">
<?php elseif($_GET['p']==3 || (!empty($apply) && $apply=="【スマートフォン】転職可能性個別カウンセリング")):?>
<img src="img/form_main_03.jpg" width="100%">
<input type="hidden" name="apply" value="【スマートフォン】転職可能性個別カウンセリング">
<?php endif;?>
</p>
<!-- co start -->
<div class="co">
<p>下記の内容で送信致します。</p>
<h2>氏名<span class="point">※</span></h2>
<p><?php echo $shimei;?></p>
<h2>フリガナ<span class="point">※</span></h2>
<p><?php echo $kana;?></p>
<h2>電話<span class="point">※</span></h2>
<p><?php echo $tel1;?></p>
<h2>メール<span class="point">※</span></h2>
<p><?php echo $mail1;?></p>
<h2>転職意欲<span class="point">※</span></h2>
<p><?php echo $iyoku;?></p>
<h2>連絡可能時間帯</h2>
<p>平日：<?php echo $renraku1;?><br>
土日：<?php echo $renraku2;?></p>

<!-- start -->
<div class="btnSend">
<table style="margin-right:auto;margin-left:auto;">
<tr>

<td align="center" style="padding-right:5px;">
<form id="entryForm" action="mail.php" method="post">
<input type="hidden" name="mode" value="return">
<input type="image" src="./img/btn_back.gif" value="戻る" width="71" height="36">
</form>
</td>
<td align="center">
<form id="entryForm" action="mail.php" method="post">
<input name="btnKakunin2" type="image" src="img/btn_send.gif" id="btnKakunin2" value="送信する" width="211" height="36">
<input type="hidden" name="mode" value="send">
<input type="hidden" name="shimei" value="<?php echo $shimei;?>">
<input type="hidden" name="kana" value="<?php echo $kana;?>">
<input type="hidden" name="tel1" value="<?php echo $tel1;?>">
<input type="hidden" name="mail1" value="<?php echo $mail1;?>">
<input type="hidden" name="agree" value="<?php echo $agree;?>">

<input type="hidden" name="renraku1" value="<?php echo $renraku1;?>">
<input type="hidden" name="renraku2" value="<?php echo $renraku2;?>">

<input type="hidden" name="iyoku" value="<?php echo $iyoku;?>">

<input type="hidden" name="apply" value="<?php echo $apply;?>">

</form>
</td>
</tr>
</table>
</div>
<!-- end -->
</div>
<!-- co end -->
</div>
<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>