<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="index,follow">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<link rel="canonical" href="">
<link href="css/import.css" rel="stylesheet" type="text/css" media="all">
<title>東北でキャリアアップ!! | 東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="../../js/rollover.js"></script>
<script type="text/javascript" src="../../js/ga.js"></script>
<script type="text/javascript" src="./form_validation.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	var date = new Date();
	var trans_id = parseInt((new Date)/1000);
  ga('create', 'UA-6405327-12', 'hurex.jp');

  ga('send', 'pageview');
	ga('require', 'ecommerce', 'ecommerce.js');
	ga('send', {
		'hitType': 'pageview',
		'page': '/fb/smart/finish.php',
		'title': 'エントリー完了ページ'
	});
	ga('ecommerce:addTransaction', {
		'id': trans_id,
		'affiliation': 'HUREX_WEB',
		'revenue': '20000',
		'shipping': '0',
		'tax': '0'
	});
	ga('ecommerce:addItem', {
		'id': trans_id,
		'name': 'エントリー/スマホ',
		'sku': 'es0001',
		'category': 'WEB',
		'price': '20000',
		'quantity': '1'
	});
	ga('ecommerce:send');
</script>

<script type="text/javascript">
	var date = new Date();
	var trans_id = parseInt((new Date)/1000);
  var _gaq = _gaq || [];
  var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
  _gaq.push(['_require', 'inpage_linkid', pluginUrl]);
  _gaq.push(['_setAccount', 'UA-6405327-10']);
  _gaq.push(['_trackPageview','/fb/smart/finish.php']);
  _gaq.push(['_addTrans',
    trans_id,
    '',
    '20000',
    '',
    '0',
    '',
    '',
    '日本'
  ]);

  _gaq.push(['_addItem',
    trans_id,
    'es0001',
    'エントリー/スマホ',
    '',
    '20000',
    '1'
  ]);

  _gaq.push(['_trackTrans']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>
<div id="fbPage">
<p id="logo"><a href="index.php"><img src="img/logo.gif" width="146" height="43"></a></p>
<p id="mainContents">
<?php if($_GET['p']=="【スマートフォン】適正年収診断"):?>
<img src="img/form_main_01.jpg" width="100%" />
<?php elseif($_GET['p']=="【スマートフォン】応募書類の作成代行サービス"):?>
<img src="img/form_main_02.jpg" width="100%" />
<?php elseif($_GET['p']=="【スマートフォン】転職可能性個別カウンセリング"):?>
<img src="img/form_main_03.jpg" width="100%" />
<?php endif;?>
</p>
<!-- co start -->
<div class="co">
<p>登録が完了致しました。<br>担当より折り返しご連絡致しますのでお待ちください。</p>
<p class="btnLink"><a href="https://www.hurex.jp/smart/">トップページに戻る</a></p>
</div>
<!-- co end -->
</div>
<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>