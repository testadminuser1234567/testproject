<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="index,follow">
<meta name="keywords" content="">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<link rel="canonical" href="">
<link href="css/import.css" rel="stylesheet" type="text/css" media="all">
<title>東北でキャリアアップ!! | 東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="../../js/rollover.js"></script>
<script type="text/javascript" src="../../js/ga.js"></script>
<script type="text/javascript" src="./form_validation.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>
<div id="fbPage">
<p id="logo"><a href="index.php"><img src="img/logo.gif" width="146" height="43"></a></p>
<form id="entryForm" action="mail.php" method="post">
<input type="hidden" name="mode" value="confirm">
<p id="mainContents">
<?php if($_GET['p']==1 || (!empty($apply) && $apply=="【スマートフォン】適正年収診断")):?>
<img src="img/form_main_01.jpg" width="100%">
<input type="hidden" name="apply" value="【スマートフォン】適正年収診断">
<?php elseif($_GET['p']==2 || (!empty($apply) && $apply=="【スマートフォン】応募書類の作成代行サービス")):?>
<img src="img/form_main_02.jpg" width="100%">
<input type="hidden" name="apply" value="【スマートフォン】応募書類の作成代行サービス">
<?php elseif($_GET['p']==3 || (!empty($apply) && $apply=="【スマートフォン】カウンセリング")):?>
<img src="img/form_main_03.jpg" width="100%">
<input type="hidden" name="apply" value="【スマートフォン】転職可能性個別カウンセリング">
<?php endif;?>
</p>
<!-- co start -->
<div class="co">
<p>下記フォームにご入力の上、[入力内容の確認ページへ]ボタンをクリックしてください。<br>
<span class="point">※</span>は必須項目です。<br>
当サイトは、お客様のプライバシー保護のため、個人情報入力ページにおいてSSL暗号化通信を採用しています。</p>
<div class="error"><?php echo $error;?></div>
<h2>氏名<span class="point">※</span></h2>
<p><input type="text" name="shimei" id="shimei" class="required txtBox" value="<?php echo $shimei;?>" maxlength="25" style="width:100%;"></p>
<h2>フリガナ<span class="point">※</span></h2>
<p><input type="text" name="kana" id="kana" class="required txtBox" value="<?php echo $kana;?>" maxlength="25" style="width:100%;"></p>
<h2>電話<span class="point">※</span></h2>
<p><input type="text" name="tel1" id="tel1" class="required  txtBox" value="<?php echo $tel1;?>" maxlength="15" style="width:100%;"></p>
<h2>メール<span class="point">※</span></h2>
<p><input type="text" name="mail1" id="mail1" class="required mail txtBox" maxlength="50"  value="<?php echo $mail1;?>" style="width:100%;"></p>
<h2>転職意欲<span class="point">※</span></h2>
<p><select name="iyoku" id="iyoku" class="required">
<option>▼</option>
<option value="今すぐの転職を希望" <?php echo $iyoku=="今すぐの転職を希望" ? "selected" : "";?>>今すぐの転職を希望</option>
<option value="今すぐでは無いが将来的に転職を希望" <?php echo $iyoku=="今すぐでは無いが将来的に転職を希望" ? "selected" : "";?>>今すぐでは無いが将来的に転職を希望</option>
<option value="転職は検討していない" <?php echo $iyoku=="転職は検討していない" ? "selected" : "";?>>転職は検討していない</option>
</select></p>
<h2>連絡可能時間帯</h2>
<p>平日：<select name="renraku1" id="renraku1" class="required">
<option>▼</option>
<option value="9-12時" <?php echo $renraku1=="9-12時" ? "selected" : "";?>>9-12時</option>
<option value="12-13時" <?php echo $renraku1=="12-13時" ? "selected" : "";?>>12-13時</option>
<option value="13-16時" <?php echo $renraku1=="13-16時" ? "selected" : "";?>>13-16時</option>
<option value="16-19時" <?php echo $renraku1=="16-19時" ? "selected" : "";?>>16-19時</option>
<option value="19時以降" <?php echo $renraku1=="19時以降" ? "selected" : "";?>>19時以降</option>
<option value="終日" <?php echo $renraku1=="終日" ? "selected" : "";?>>終日</option>
</select><br>
土日：<select name="renraku2" id="renraku2" class="required">
<option>▼</option>
<option value="9-12時" <?php echo $renraku2=="9-12時" ? "selected" : "";?>>9-12時</option>
<option value="12-13時" <?php echo $renraku2=="12-13時" ? "selected" : "";?>>12-13時</option>
<option value="13-16時" <?php echo $renraku2=="13-16時" ? "selected" : "";?>>13-16時</option>
<option value="16-19時" <?php echo $renraku2=="16-19時" ? "selected" : "";?>>16-19時</option>
<option value="19時以降" <?php echo $renraku2=="19時以降" ? "selected" : "";?>>19時以降</option>
<option value="終日" <?php echo $renraku2=="終日" ? "selected" : "";?>>終日</option>
</select></p>
<!-- privacyBox start -->
<div id="privacyBox">
<div id="privacyBoxSub">
<h2>■ 個人情報保護方針 <span class="point">※</span></h2>
<textarea id="policyBox" readonly>
プライバシーポリシー

ヒューレックス株式会社は、個人情報保護の重要性に鑑み、また、業務に従事するすべての者が、その責任を認識し、個人情報の保護に関する法律（職安法第51条および個人情報保護法）に即し個人情報保護法その他の関連法令・ガイドラインを遵守して、個人情報を適正に取り扱うとともに、安全管理について適切な措置を講じます。

ヒューレックス株式会社は、個人情報の取扱いが適正に行われるように従業者への教育・指導を徹底し、適正な取扱いが行われるよう取り組んでまいります。また、個人情報の取扱いに関する苦情・相談に迅速に対応し、当社の個人情報の取扱い及び安全管理に関わる適切な措置については、適宜見直し改善いたします。


（1）個人情報の取得
ヒューレックス株式会社は、十分な安全管理措置を講じたうえで、業務上必要な範囲で、かつ、適法で公正な手段により個人情報を取得します。

（2）個人情報の利用目的
ヒューレックス株式会社は、ご登録者の経歴、職歴を正しく把握し、適切なアドバイスや求人企業をご紹介するために必要な情報をご登録者よりお預かりいたします。取得した個人情報を、当社の営む求人紹介業務、および転職サービス（海外・国内研修）、転職にともなう渡航・旅行に関連するサービスの提供等に利用します。

上記の利用目的の変更は、相当の関連性を有すると合理的に認められる範囲においてのみ行い、変更する場合には、その内容をご本人に対し、原則として書面等により通知し、またはホームページ（URL）等により公表します。

（3）個人データの安全管理措置
ヒューレックス株式会社は、取り扱う個人データの漏えい、滅失またはき損の防止その他の個人データの安全管理のため、安全管理に関する取扱規程等の整備および実施体制の整備等、十分なセキュリティ対策を講じるとともに、正確性・最新性を確保するために、必要かつ適切な措置を講じ、万が一、問題等が発生した場合は、速やかに適当な是正対策をします。

（4）個人データの第三者への提供
ヒューレックス株式会社は、法令または裁判所その他の政府機関より適法に開示を要求された場合を除き、ご本人の同意なく求人会社を含め第三者に個人データを提供することはありません。ご登録された個人情報の変更、削除を希望される場合は、 touroku＠hurex.co.jp までご連絡ください。

（5）当社に対するご照会
下記の窓口にお問い合わせください。ご照会者の詳細を確認させていただいたうえで対応させていただきますので、あらかじめご了承願います。


【お問い合わせ窓口】
ヒューレックス株式会社 
〒980-6117 宮城県仙台市青葉区中央1-3-1 アエル17階 
TEL: 0120-14-1150 FAX: 022-723-1738 
Email: touroku＠hurex.co.jp 
担当：神谷貴宏 

以上
</textarea>
<p class="checkboxrequired"><input type="checkbox" class="rc" name="agree[]" value="同意する" id="agreeChk" <?php echo $agree['同意する'];?>> <label for="agreeChk">同意する</label></p>
</div>
</div>
<!-- privacyBox end -->
<!-- start -->
<div class="btnSend">
<input name="btnKakunin" type="image" src="img/btn_check.gif" id="btnKakunin" value="送信確認画面へ" width="211" height="36" onClick="return check(1);">
</div>
<!-- end -->
</form>
</div>
<!-- co end -->
</div>
<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>