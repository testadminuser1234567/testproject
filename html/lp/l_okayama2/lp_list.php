<?php
/*
/ 求人情報一覧テンプレート
*/
?>

	<!--求人情報一覧-->
	<div class="row">
		<?php if($total_cnt > 0):?>
		<?php foreach($datas as $k => $v):?>
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 is-work-list" data-wow-delay="0.5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
			<a href="../../mypage/signup/regist?p=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>&lpjob" class="lp_ishi_ver_ai">
				<h3><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></h3>
				<p><span><strong>想定年収：</strong><?php if(!empty($v['minsalary'])):?><?php echo htmlspecialchars($v['minsalary'], ENT_QUOTES, 'UTF-8');?><?php if(empty($v['maxsalary'])):?><?php else:?>〜<?php endif;?><?php endif;?><?php if(!empty($v['maxsalary'])):?><?php echo htmlspecialchars($v['maxsalary'], ENT_QUOTES, 'UTF-8');?>万円&nbsp;<?php endif;?>&nbsp;</span> <?php if(!empty($v['employ_name'])):?>
					<?php $tmpe = explode(",",$v['employ_name']);?>
					<?php foreach($tmpe as $ek => $ev):?>
					<?php echo htmlspecialchars($ev, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpe) > 1):?><?php endif;?>
					<?php endforeach;?>
					<?php endif;?></p>
				<div class="lp_ishi_ver_ai_btn"><img src="images/btn_request_v2.png" alt="詳しく聞く(転職支援申込)"></div>
			</a>
		</div>
		<?php endforeach;?>
		<?php else:?>
		<p>お探しの条件での求人情報はございません。</p>
		<?php endif;?>
		</div>
    </div>		
<script>
$(".hurex_srch").hide();
$("#searchBtn").click(function(){
$(".hurex_srch").slideToggle();
});
</script>