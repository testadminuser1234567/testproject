<?php
/*
/ 求人情報一覧テンプレート
*/
?>
		<div class="row">
		<?php if($total_cnt > 0):?>
		<?php foreach($datas as $k => $v):?>
			  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 wow fadeInUp is-work-list" data-wow-delay="0.5s">
				<a href="../../entry03/form.html?id=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>" class="lp_ishi_ga_detail recrt">
				<div class="recinfo">
				  <table border="0" cellspacing="0" cellpadding="0" height="100%">
					<tr>
					  <td height="80%" valign="middle">
						  <h3><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></h3>
						</td>
					</tr>
					<tr>
					  <td height="20%" valign="middle">
						  <p><span><strong>想定年収：</strong><?php if(!empty($v['minsalary'])):?><?php echo htmlspecialchars($v['minsalary'], ENT_QUOTES, 'UTF-8');?><?php if(empty($v['maxsalary'])):?>万円<?php else:?>〜<?php endif;?><?php endif;?><?php if(!empty($v['maxsalary'])):?><?php echo htmlspecialchars($v['maxsalary'], ENT_QUOTES, 'UTF-8');?>万円&nbsp;<?php endif;?></span>　<?php if(!empty($v['employ_name'])):?>
					<?php $tmpe = explode(",",$v['employ_name']);?>
					<?php foreach($tmpe as $ek => $ev):?>
					<?php echo htmlspecialchars($ev, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpe) > 1):?><br /><?php endif;?>
					<?php endforeach;?>
					<?php endif;?></p>
						</td>
					</tr>
				  </table>
				</div>
				<div class="recbtn"><img src="images/btn_detail.png"></div>
				</a>
			  </div>

		<?php endforeach;?>
		<?php else:?>
		<p>お探しの条件での求人情報はございません。</p>
		<?php endif;?>
		</div>
		
	  </div>
	  <div class="container">
		<div class="col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2 is-work-list-form">
		  <div class="smpphlo"><a href="https://www.hurex.jp/smart/entry/form.php?=lp_05" class="lp_ishi_ga_sp_05"><img src="images/btn_request_ishi.png" class="img-responsive lp_ishi_ga_sp_05"></a></div>
		  <div class="pcphlo"><a class="text-center" href="https://www.hurex.jp/entry/form.html?=lp_05" title="無料転職支援に申し込む" class="lp_ishi_ga_pc_05"><img src="images/btn_request_ishi.png" class="img-responsive"></a></div>
		</div>
	  </div>
	</div>
<script>
$(".hurex_srch").hide();
$("#searchBtn").click(function(){
    $(".hurex_srch").slideToggle();
});
</script>