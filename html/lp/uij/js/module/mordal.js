// -----------------------------------
//      Mordal
// -----------------------------------

$(function() {
    var w = $(window).width();
    var x = 480;
    if (x <= w) {
        // mouseleave
        var flag = 0;
        document.body.addEventListener("mouseleave", function() {
            if (flag) return false;
            $('#mordal').fadeIn(200);
            $('#mordal__inner').delay(200);
            flag = 1;
        }, false);

        // Close
        $('#mordal__close').on('click', function() {
            $('#mordal').delay(300).fadeOut(200);
        });
    }
});