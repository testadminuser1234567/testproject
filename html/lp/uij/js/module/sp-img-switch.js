// -----------------------------------
//		Image - switch
// -----------------------------------
$(function() {
	var $elem = $('.js-image-switch-sp');
	var sp = '-sp.';
	var pc = '-pc.';
	var replaceWidth = 481;

	// sp
	function imageSwitch() {
		var windowWidth = parseInt($(window).width());
		$elem.each(function() {
			var $this = $(this);
			if (windowWidth >= replaceWidth) {
				$this.attr('src', $this.attr('src').replace(sp, pc));
			} else {
				$this.attr('src', $this.attr('src').replace(pc, sp));
			}
		});
	}
	imageSwitch();

	// resize
	var resizeTimer;
	$(window).on('resize', function() {
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			imageSwitch();
		}, 200);
	});
});
