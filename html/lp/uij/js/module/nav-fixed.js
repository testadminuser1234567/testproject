// -----------------------------------
//		Navigation - fixed
// -----------------------------------

$(function() {
    var nav = $('header');
    var navHeight = nav.height();
    var navTop = nav.offset().top + 20;
    var showFlag = false;
    nav.removeClass("fixed");
    nav.css('top', -navHeight + 'px');
    $(window).scroll(function() {
        var winTop = $(this).scrollTop();
        if (winTop >= navTop) {
            if (showFlag == false) {
                showFlag = true;
                nav.addClass('fixed').stop().animate({ 'top': '0px' }, 500);
            }
        } else if (winTop <= navTop) {
            if (showFlag) {
                showFlag = false;
                nav.stop().animate({ 'top': -navHeight + 'px' }, 0, function() {
                    nav.removeClass('fixed');
                });
            }
        }
    });
});