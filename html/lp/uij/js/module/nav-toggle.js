// -----------------------------------
//      Navigation - SmartDevice
// -----------------------------------

// btn toggle
$(document).ready(function() {
    "use strict";
    var w = $(window).width();
    var x = 480;
    if (w <= x) {
        var params = { height: "toggle", opacity: "toggle" };
        $(".btn_sdnav, .header__nav .anchor").not(".header__nav li.header__cta a").click(function() {
            $(".header__nav:not(:animated)").animate(params);
            $(".btn_sdnav_icon").toggleClass("active");
            return false;
            if ($(".btn_sdnav").attr("class") != "opened") {} else {
                $(".btn_sdnav").removeAttr("class");
            }
        });
    }
});

// Nav Responsive
$(window).resize(function() {
    var w = $(window).width();
    var x = 480;
    if (w <= x) {
        $(".header__nav").css('display', 'none')
        $(".btn_sdnav_icon").removeClass("active");
    } else {
        $(".header__nav").css('display', 'block');
        $(".btn_sdnav_icon").addClass("active");
    }
});