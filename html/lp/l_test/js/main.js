;(function () {
	
	'use strict';

	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
			BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
			iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
			Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
			Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
			any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};

	var goToTop = function() {

		$('.js-gotop').on('click', function(event){
			
			event.preventDefault();

			$('html, body').animate({
				scrollTop: $('html').offset().top
			}, 500, 'easeInOutExpo');
			
			return false;
		});

		$(window).scroll(function(){

			var $win = $(window);
			if ($win.scrollTop() > 200) {
				$('.js-top').addClass('active');
			} else {
				$('.js-top').removeClass('active');
			}

		});
	
	};


	// Loading page
	var loaderPage = function() {
		$(".orign-loader").fadeOut("slow");
	};

	var parallax = function() {

		if ( !isMobile.any() ) {
			$(window).stellar({
				horizontalScrolling: false,
				hideDistantElements: false, 
				responsive: true

			});
		}
	};


	var tabs = function() {

		// Auto adjust height
		$('.orign-tab-content-wrap').css('height', 0);
		var autoHeight = function() {

			setTimeout(function(){

				var tabContentWrap = $('.orign-tab-content-wrap'),
					tabHeight = $('.orign-tab-nav').outerHeight(),
					formActiveHeight = $('.tab-content.active').outerHeight(),
					totalHeight = parseInt(tabHeight + formActiveHeight + 90);

					tabContentWrap.css('height', totalHeight );

				$(window).resize(function(){
					var tabContentWrap = $('.orign-tab-content-wrap'),
						tabHeight = $('.orign-tab-nav').outerHeight(),
						formActiveHeight = $('.tab-content.active').outerHeight(),
						totalHeight = parseInt(tabHeight + formActiveHeight + 90);

						tabContentWrap.css('height', totalHeight );
				});

			}, 100);
			
		};

		autoHeight();


		// Click tab menu
		$('.orign-tab-nav a').on('click', function(event){
			
			var $this = $(this),
				tab = $this.data('tab');

			$('.tab-content')
				.addClass('animated-fast fadeOutDown');

			$('.orign-tab-nav li').removeClass('active');
			
			$this
				.closest('li')
					.addClass('active')

			$this
				.closest('.orign-tabs')
					.find('.tab-content[data-tab-content="'+tab+'"]')
					.removeClass('animated-fast fadeOutDown')
					.addClass('animated-fast active fadeIn');


			autoHeight();
			event.preventDefault();

		}); 
	};

	
	$(function(){
		goToTop();
		loaderPage();
		parallax();
		tabs();
	});


}());


$(function(){
    $(".is-bg-move").css("background-position" , "0% bottom");//横に流したい＝Xの値を動かすため、Xの値にあたる部分を「0%」としています。
    
    var scrollSpeed = -1;//px 移動する量
    var imgWidth = 1216;//画像の幅
    var posX = 0;//背景のスタート位置（この後の記述でこの変数posXの数値が増加する事で背景画像の移動が可能となります）
    
    setInterval(function(){
        if(posX >= imgWidth){ posX = 0; }//変数posXの値が画像の横幅の数値まで増加したらposXの値を0に戻す
        
        posX += scrollSpeed;//posXの値に、scrollSpeedの値を足していく
        
        $(".is-bg-move").css("background-position" , posX + "px bottom")//上でどんどん増加するposXをbackground-positionの横位置に挿入
      } , 30);//移動スピードを変更したい時はこの「50」の数値を増減させる
      
});