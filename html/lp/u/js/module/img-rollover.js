// -----------------------------------
//		Image - Rollover Alpha
// -----------------------------------

$(function() {
	var nav = $('.alpha');
	nav.hover(
		function() {
			$(this).fadeTo(600, 0.7);
		},
		function() {
			$(this).fadeTo(600, 1);
		}
	);
});
