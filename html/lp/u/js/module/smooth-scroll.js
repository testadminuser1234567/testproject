// -----------------------------------
//		Smooth Scroll
// -----------------------------------

$(function() {
	$('a[href^="#"]').click(function() {
        var headHight = 70;
        var speed = 800;
        var href = $(this).attr("href");
        var target = $(href == "#" || href == "" ? 'html' : href);
        var position = target.offset().top - headHight;
        $("html, body").animate({ scrollTop: position }, speed, "swing");
        return false;
    });
});