<?php
class MailForm
{

	/** 設定ファイルパス */
	const PATH_INI_FILE = "mail.ini";

	/** メールテンプレートパス：管理者 */
	const PATH_MAIL_TEMPLATE_ADMIN = "mail_template_admin.txt";

	/** メールテンプレートパス：顧客 */
	const PATH_MAIL_TEMPLATE_CUSTOMER = "mail_template_customer.txt";

	/** リクエストパラメータ */
	private $params;

	/**
	 * constructor
	 */
	public function __construct($params)
	{
		$this->params = $params;
	}

	/**
	 * HTML特殊文字のエスケープ
	 */
	public function escape()
	{
		foreach ($this->params as $key => $value) {
			if (is_string($value)) {
				$val[$key] =
					htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
			}
		}

		return $val;
	}

	/**
	 * パラメータチェック
	 */
	public function check()
	{
		$errormessage = array();

		// -- お名前
		$val = $this->params['name'];
		// 必須チェック
		$this->check_required($errormessage, $val, "お名前");

		// -- ふりがな
		$val = $this->params['namekana'];
		// 必須チェック
		$this->check_required($errormessage, $val, "ふりがな");

		// -- 電話番号
		$val = $this->params['tel'];
		// 必須チェック
		$this->check_required($errormessage, $val, "電話番号");
		// 数値チェック
		$this->check_digit($errormessage, $val, "電話番号");

		// -- 住所：都道府県
		$val = $this->params['prefectures'];
		// 必須チェック
		$this->check_required($errormessage, $val, "住所：都道府県");

		// -- 最終学歴
		$val = $this->params['academicbackground'];
		// 必須チェック
		$this->check_required($errormessage, $val, "最終学歴");

		// -- 生年月日：年
		$val = $this->params['birthyear'];
		// 必須チェック
		$this->check_required($errormessage, $val, "生年月日：年");

		// -- 生年月日：月
		$val = $this->params['birthmonth'];
		// 必須チェック
		$this->check_required($errormessage, $val, "生年月日：月");

		// -- 生年月日：日
		$val = $this->params['birthdate'];
		// 必須チェック
		$this->check_required($errormessage, $val, "生年月日：日");

		// -- その他ご希望記入欄
		$val = $this->params['coment'];
		// 機種依存文字チェック
		$this->check_platform_dependent_characters($errormessage, $val,
															"その他ご希望記入欄");

		// -- メールアドレス
		$val = $this->params['mail'];
		// 文字変換
		$val = mb_convert_kana($this->params['mail'], "KVa");
		// 必須チェック
		$this->check_required($errormessage, $val, "メールアドレス");
		// 形式チェック
		$this->check_mail($errormessage, $val, "メールアドレス");


		// -- 生年月日 日付チェック
		$val =
			str_pad($this->params['birthyear'],  2, "0", STR_PAD_LEFT) . "/" .
			str_pad($this->params['birthmonth'], 2, "0", STR_PAD_LEFT) . "/" .
			str_pad($this->params['birthdate'],  2, "0", STR_PAD_LEFT);
		$this->check_date($errormessage, $val, "生年月日");


		return $errormessage;
	}

	public function send()
	{

		// 定義ファイル読込
		$ini = parse_ini_file(self::PATH_INI_FILE);

		// NULLバイト文字削除
		foreach ($this->params as $key => $value) {
			if (is_string($value)) {
				$val[$key] = str_replace("\0", "", $value);
				$val[$key] = str_replace("\\0", "", $value);
			}
		}

		// 管理者宛メール送信
		$address_list = $ini['ADDRESS_ADMIN'];
		$to = join(",", $address_list);
		$subject = $this->replace_text($ini['SUBJECT_ADMIN'], $val);
		$message = file_get_contents(self::PATH_MAIL_TEMPLATE_ADMIN);
		$message = $this->replace_text($message, $val);
		$from = $address_list[0];
		$this->sendmail($to, $subject, $message, $from);

		// 顧客メールアドレスの文字変換
		$addresscustomer = mb_convert_kana($val['mail'], "KVa");
		// 顧客宛メール送信
		$to = $addresscustomer;
		$subject = $this->replace_text($ini['SUBJECT_CUSTOMER'], $val);
		$message = file_get_contents(self::PATH_MAIL_TEMPLATE_CUSTOMER);
		$message = $this->replace_text($message, $val);
		$from = $address_list[0];
		$this->sendmail($to, $subject, $message, $from, $from);
	}

	/**
	 * メール送信
	 */
	private function sendmail(
		$to, $subject, $message, $from, $bcc="")
	{
		mb_language("Japanese");
		mb_internal_encoding("UTF-8");

		$headers = "From: " . $from . "\n";
		if (!empty($bcc))
		{
			$headers.="Bcc: " . $bcc . "\n";
		}

		$envelope_from = escapeshellarg($from);

		// 送信
		mb_send_mail($to, $subject, $message, $headers, "-f" . $envelope_from);
	}

	/**
	 * メールテキスト編集
	 */
	private function replace_text($text, $val)
	{
		$valnames = array(
			"name",
			"namekana",
			"tel",
			"prefectures",
			"academicbackground",
			"birthyear",
			"birthmonth",
			"birthdate",
			"coment",
			"mail"
		);

		$pattern = array();
		$replacement = array();
		foreach ($valnames as $valname) {
			$pattern[] = "/##" . $valname . "##/";
			$replacement[] = $val[$valname];
		}

		return preg_replace($pattern, $replacement, $text);
	}

	/**
	 * 必須チェック
	 */
	private function check_required(&$errormessage, $value, $name)
	{
		if (is_null($value) || $value === "") {
			$errormessage[] = sprintf("%sを入力してください。", $name);
		}
	}

	/**
	 * 数字チェック
	 */
	private function check_digit(&$errormessage, $value, $name)
	{
		if (is_null($value) || $value === "") {
			return;
		}
		if (!ctype_digit($value)) {
			$errormessage[] = sprintf("%sが数字ではありません。", $name);
		}
	}

	/**
	 * 日付チェック
	 */
	private function check_date(&$errormessage, $value, $name)
	{
		if (is_null($value) || $value === "") {
			return;
		}

		if ($value !== date("Y/m/d", strtotime($value))) {
			$errormessage[] = sprintf("%sが正しい日付ではありません。", $name);
		}
	}

	/**
	 * メール形式チェック
	 */
	private function check_mail(&$errormessage, $value, $name)
	{
		if (is_null($value) || $value === "") {
			return;
		}
		if (!((bool)filter_var($value, FILTER_VALIDATE_EMAIL))) {
			$errormessage[] = sprintf("%sのメールアドレス形式が違います。", $name);
		}
	}

	/**
	 * 機種依存文字チェック
	 */
	private function check_platform_dependent_characters(
		&$errormessage, $value, $name)
	{
		if (strlen($value) !=
			strlen(mb_convert_encoding(
				mb_convert_encoding(
					$value,'SJIS','UTF-8'),'UTF-8','SJIS'))) {

				$errormessage[] = sprintf(
					"%sに機種依存文字は入力しないでください。", $name);
		}
	}

}
