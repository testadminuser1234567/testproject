<?php
	require_once('MailForm.php');

	$mf = new MailForm($_POST);

	// 入力チェックエラー時の値復元用
	// HTML特殊文字のエスケープ
	$values = $mf->escape();
?>

<?php

	function write_select_prefectures($invalue) {
		echo '<option value="">選択してください</option>';
		$items = array(
			"北海道", "青森県", "岩手県", "宮城県", "秋田県", "山形県", "福島県",
			"茨城県", "栃木県", "群馬県", "埼玉県", "千葉県", "東京都", "神奈川県",
			"新潟県", "富山県", "石川県", "福井県", "山梨県", "長野県", "岐阜県",
			"静岡県", "愛知県", "三重県", "滋賀県", "京都府", "大阪府", "兵庫県",
			"奈良県", "和歌山県", "鳥取県", "島根県", "岡山県", "広島県", "山口県",
			"徳島県", "香川県", "愛媛県", "高知県", "福岡県", "佐賀県", "長崎県",
			"熊本県", "大分県", "宮崎県", "鹿児島県", "沖縄県"
		);
		foreach ($items as $value) {
			$selected = ($value === $invalue ? 'selected="selected"' : '');
			echo sprintf('<option value="%1$s" %2$s>%1$s</option>', $value, $selected);
		}
	}

	function write_select_academicbackground($invalue) {
		echo '<option value="">選択してください</option>';
		$items = array(
			"大学院卒（博士）",
			"大学院卒（修士）",
			"大学卒",
			"高専卒",
			"短大卒",
			"専門各種学校卒",
			"高校卒",
			"その他"
		);
		foreach ($items as $value) {
			$selected = ($value === $invalue ? 'selected="selected"' : '');
			echo sprintf('<option value="%1$s" %2$s>%1$s</option>', $value, $selected);
		}
	}

	function write_select_birthyear($invalue) {
		echo '<option value=""></option>';
		for ($i = 1950; $i <= 1996; $i++) {
			$selected = ((string)$i === $invalue ? 'selected="selected"' : '');
			echo sprintf('<option value="%1$d" %2$s>%1$d</option>', $i, $selected);
		}
	}

	function write_select_birthmonth($invalue) {
		echo '<option value=""></option>';
		for ($i = 1; $i <= 12; $i++) {
			$selected = ((string)$i === $invalue ? 'selected="selected"' : '');
			echo sprintf('<option value="%1$d" %2$s>%1$d</option>', $i, $selected);
		}
	}

	function write_select_birthdate($invalue) {
		echo '<option value=""></option>';
		for ($i = 1; $i <= 31; $i++) {
			$selected = ((string)$i === $invalue ? 'selected="selected"' : '');
			echo sprintf('<option value="%1$d" %2$s>%1$d</option>', $i, $selected);
		}
	}

?>
<!doctype html>
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="ja" />
	<meta name="viewport" content="width=320,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>一流の転職をするならエクセレトス(EXCELLETOS)</title>
	<meta name="description" content="一流の転職をするならエクセレトス(EXCELLETOS)。東北の中小企業は､優秀な後継者･後継幹部になり得る皆さんをお待ちしています。"/>
	<meta name="Keywords" content="EXCELLETOS,転職,求人,東北,仙台,宮城,HUREX,ヒューレックス,福島,山形" />
	<link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/jquery.smoothScroll.js"></script>
	<script src="js/common.js"></script>
	
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
	<!-- End Google Tag Manager -->
</head>
<body>
<div class="content">
	<header>
	<div class="header_wrap">
		<h1><a href="#"><img src="img/logo.png" alt="一流の転職をするなら　エクセレトス(EXCELLETOS)" /></a><em>一流の転職をするなら　エクセレトス(EXCELLETOS)" /></em></h1>
		
		<img src="img/menu.png" class="menu sp" />
		
		<nav class="nav_wrap">
		
		<img src="img/menu_close.png" class="menu sp" />
		
		<ul>
			<li class="link"><a href="#talent">求める人材</a></li>
			<li class="link"><a href="#job">求人情報</a></li>
			<!--<li class="link"><a href="#solution">登録者実績</a></li>-->
			<li class="link"><a href="#conversation">地方創生対談</a></li>
			<li class="btn01 btn"><a href="#entry">後継者･幹部に立候補する</a></li>
			<!--<li class="btn02 btn"><a href="">採用をお考えの企業さまへ</a></li>-->
		</ul>
		</nav>
	</div>
	</header>
	
	<div class="con01">
		<div class="top_area">
			<img src="img/top_01.png" class="top_img" alt="経営者のバトンを受け、新たな挑戦へ" />
			<div class="top_text">
				<h2>経営者<span>の</span>バトン<span>を受け､</span><br/>新<span>たな</span>挑戦<span>へ</span></h2>
				<p class="p01">プロフェッショナルと<br class="sp" />地域企業をマッチング</p>
				<p class="p02">年収800万円以上の<br class="sp" />非公開求人多数掲載</p>
			</div>
		</div>
		
		<div class="con_box">
			<h2 class="center"><img src="img/con01_01.png" alt="次はあなたの番だ。さぁ､東北で走り出せ" /></h2>
			<h3 class="line_o"><span>7割以上の企業が､<br class="sp">後継者不足に悩んでいる</span></h3>
			<p>近年、日本では経営者の高齢化が加速しており、<br class="pc" />
			60歳代後半から70歳代の「高齢社長」も数多く見られるようになっています。<br/>
			「任せられる後継者がいない」ということが大きな理由として挙げられており、<br class="pc" />
			実際に<strong>7割を超える企業が後継者不在</strong><span>※</span>に頭を抱えています。<br/>
			<span>※2016/11/25 帝国データバンク：全国オーナー企業分析</span></p>
			<p>その傾向が顕著に表れているのが東北地方です。<br/>
			人口流出が激化し、優秀な人材の東北離れが進むなか、エグゼクティブ層の不足は年々深刻化するばかり。<br/>
			「<strong>後継者</strong>」だけではなく、社長の右腕・左腕といった「<strong>後継幹部</strong>」の欠如が、<br class="pc" />
			事業拡大や新分野参入の際に大きな障壁となっています。<br/>
			<br/>
			後継者・後継幹部の不足は、東北経済の発展を妨げている要因の1つとして挙げられるだけではなく、<br class="pc" />
			東北企業の多くが頭を悩ませている課題でもあります。</p>
			
			<div class="task">
				<h3 class="center"><span>地方企業が<br class="sp" />抱える課題</span></h3>
				<ul>
					<li>社内に安心して任せられる<strong class="ora">後継者がいない</strong></li>
					<li>後継者候補はいるが、まだ若く、<strong class="ora">スキル経験ともに未熟</strong></li>
					<li>後継者教育をしたいが、右腕左腕になり得る<strong class="ora">適任者がいない</strong></li>
					<li>海外進出など後継者と共に立ち上げてくれる次世代の<strong class="ora">経営幹部が不在</strong></li>
				</ul>
			</div>
			
			<p class="p03">今こそ、あなたの経験やスキルを<br class="sp" />東北の企業で活かすチャンスです</p>
			
			<p class="p04">東北<span class="small">の</span>中小企業<span class="small">は､</span><br class="sp" />優秀<span class="small">な</span><strong>後継者･後継幹部<span class="small">に<br class="sp" />なり得る</span><br />皆<span class="small">さん</strong>を</span>お待ちしています｡</p>
			
		</div>
	</div>
	
	<div id="talent" class="con02">
		<div class="con_box">
			<img src="img/con02_01.png" class="arr" />
			<h2 class="center"><img src="img/con02_02.png" alt="こんなエグゼクティブな人材を求めています" class="pc" /><img src="img/con02_02_sp.png" alt="こんなエグゼクティブな人材を求めています" class="sp" /></h2>
			<ul>
				<li>
					<img src="img/con02_list01.png" alt="経営のスキルを磨かれてきた方" />
					<h3><span>経営のスキルを磨かれてきた方</span></h3>
					これまで培ってきた自分の経験や知識が、<strong>経営者としてどれだけ通用するのか</strong>試したい
				</li>
				<li>
					<img src="img/con02_list02.png" alt="可能性を広げたい方" />
					<h3><span>可能性を広げたい方</span></h3>
					自ら厳しい環境に身を置き、<strong>ビジネスマン・経営者としての可能性</strong>を広げたい
				</li>
				<li>
					<img src="img/con02_list03.png" alt="今よりもやりがいを求める方" />
					<h3><span>今よりもやりがいを求める方</span></h3>
					今以上に責任のあるポジションに就き、新規事業の立ち上げなどに<strong>挑戦できる仕事をしたい</strong>
				</li>
				<li>
					<img src="img/con02_list04.png" alt="独立に向けてノウハウを吸収したい方" />
					<h3><span>独立に向けてノウハウを吸収したい方</span></h3>
					社長の右腕、左腕として経営をサポートしながら、<strong>独立に向けて知識・ノウハウを吸収</strong>したい
				</li>
				<li>
					<img src="img/con02_list05.png" alt="地方に貢献したい方" />
					<h3><span>地方に貢献したい方</span></h3>
					これまで得たノウハウ・ナレッジを東北の企業で活かして、<strong>地域経済の活性化</strong>に貢献したい
				</li>
				<li>
					<img src="img/con02_list06.png" alt="東北の市場を動かしたい方" />
					<h3><span>東北の市場を動かしたい方</span></h3>
					後継者、もしくは後継幹部として<strong>東北の市場を動かしたい</strong>
				</li>
			</ul>
			<div class="btn_entry">
			<a href="#entry"><img src="img/btn_entry.png" alt="東北企業の後継者､後継幹部に立候補する" class="pc" /><img src="img/btn_entry_sp.png" alt="東北企業の後継者､後継幹部に立候補する" class="sp" /></a>
			</div>
		</div>
	</div>
	
	<!--
	<div id="solution" class="con03">
		<div class="con_box">
			<h2 class="title title_w">転職成功談<span>SOLUTION</span></h2>
			<ul>
				<li class="case01">
					<div class="img"><img src="img/con03_02.png" class="arr" /></div>
					<div class="text">
						<table>
							<tr class="before">
								<th>前職</th>
								<td>旅館業 支配人候補(仙台)</td>
							</tr>
							<tr class="after">
								<th>現職</th>
								<td>旅館業 支配人候補(仙台)</td>
							</tr>
						</table>
						<p>ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソ</p>
					</div>
				</li>
				<li class="case02">
					<div class="img"><img src="img/con03_03.png" class="arr" /></div>
					<div class="text">
						<table>
							<tr class="before">
								<th>前職</th>
								<td>大手消費財メーカー グループマネージャー(東京)</td>
							</tr>
							<tr class="after">
								<th>現職</th>
								<td>旅館業 支配人候補(仙台)</td>
							</tr>
						</table>
						<p>ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソードが掲載されます。ここに転職成功エピソ</p>
					</div>
				</li>
			</ul>
		</div>
	</div>
	-->
	
	<div id="job" class="con04">
		<div class="con_box">
			<h2 class="title">求人情報<span>JOB</span></h2>
			<ul class="job_lists">
				<li>
					<h3 class="right">総合設備エンジニアリング会社 / 社長の右腕</h3>
					<p class="income">700<span>－</span>1,000<span>万円</span></p>
					<table>
						<tr class="pm">
							<th>ポジション</th>
							<td colspan="3">経営企画（社長の右腕）</td>
						</tr>
						<tr class="pm">
							<th>ミッション</th>
							<td colspan="3">グループ会社全体における事業戦略の構築と舵取り</td>
						</tr>
						<tr>
							<th>事業概要</th>
							<td colspan="3">給排水衛生設備、冷暖房空調設備の各種設計・施工</td>
						</tr>
						<tr>
							<th>本社所在地</th>
							<td colspan="3">山形県山形市</td>
						</tr>
						<tr>
							<th>売上高</th>
							<td>50～100憶円</td>
							<th>従業員数</th>
							<td>101～150名</td>
						</tr>
					</table>
					<div class="btn"><a href="#entry"><img src="img/con04_btn.png" /></a></div>
				</li>
				<li>
					<h3 class="right">土木・建築業 / 営業部長</h3>
					<p class="income">600<span>－</span>700<span>万円</span></p>
					<table>
						<tr class="pm">
							<th>ポジション</th>
							<td colspan="3">営業部長</td>
						</tr>
						<tr class="pm">
							<th>ミッション</th>
							<td colspan="3">新規事業の立ち上げ、営業部門の強化</td>
						</tr>
						<tr>
							<th>事業概要</th>
							<td colspan="3">土木建築請負業・木材の販売及び製材業</td>
						</tr>
						<tr>
							<th>本社所在地</th>
							<td colspan="3">宮城県仙台市</td>
						</tr>
						<tr>
							<th>売上高</th>
							<td>300～350億円</td>
							<th>従業員数</th>
							<td>151～200名</td>
						</tr>
					</table>
					<div class="btn"><a href="#entry"><img src="img/con04_btn.png" /></a></div>
				</li>
				<li>
					<h3 class="right">ネットショップモール運営 / エンジニア責任者</h3>
					<p class="income">600<span>－</span>800<span>万円</span></p>
					<table>
						<tr class="pm">
							<th>ポジション</th>
							<td colspan="3">経営企画（社長の右腕）</td>
						</tr>
						<tr class="pm">
							<th>ミッション</th>
							<td colspan="3">サービスを機能させるためのWebアプリ開発</td>
						</tr>
						<tr>
							<th>事業概要</th>
							<td colspan="3">ネットショッピングモール運営､オークション運営等</td>
						</tr>
						<tr>
							<th>本社所在地</th>
							<td colspan="3">東京都世田谷区</td>
						</tr>
						<tr>
							<th>売上高</th>
							<td>1,000憶円以上</td>
							<th>従業員数</th>
							<td>5,001名以上</td>
						</tr>
					</table>
					<div class="btn"><a href="#entry"><img src="img/con04_btn.png" /></a></div>
				</li>
				<li>
					<h3 class="right">電気工事業 / 営業（次期社長候補）</h3>
					<p class="income"><span>－</span>1,200<span>万円</span></p>
					<table>
						<tr class="pm">
							<th>ポジション</th>
							<td colspan="3">社長候補</td>
						</tr>
						<tr class="pm">
							<th>ミッション</th>
							<td colspan="3">現社長の後任として、企業を繁栄・発展させる</td>
						</tr>
						<tr>
							<th>事業概要</th>
							<td colspan="3">電気工事業</td>
						</tr>
						<tr>
							<th>本社所在地</th>
							<td colspan="3">宮城県</td>
						</tr>
						<tr>
							<th>売上高</th>
							<td>100億-300憶円</td>
							<th>従業員数</th>
							<td>351～500名</td>
						</tr>
					</table>
					<div class="btn"><a href="#entry"><img src="img/con04_btn.png" /></a></div>
				</li>
				<li>
					<h3 class="right">住宅販売業 / 会社のNo.2（営業本部長）</h3>
					<p class="income">800<span>－</span>1,200<span>万円</span></p>
					<table>
						<tr class="pm">
							<th>ポジション</th>
							<td colspan="3">会社のNo.2（営業本部長）</td>
						</tr>
						<tr class="pm">
							<th>ミッション</th>
							<td colspan="3">10以上の営業拠点の取りまとめ</td>
						</tr>
						<tr>
							<th>事業概要</th>
							<td colspan="3">建築工事業，不動産売買・仲介・管理業</td>
						</tr>
						<tr>
							<th>本社所在地</th>
							<td colspan="3">宮城県</td>
						</tr>
						<tr>
							<th>売上高</th>
							<td>100億～300億円</td>
							<th>従業員数</th>
							<td>151～350名</td>
						</tr>
					</table>
					<div class="btn"><a href="#entry"><img src="img/con04_btn.png" /></a></div>
				</li>
				<li>
					<h3 class="right">タブレット端末ソリューション事業 / 企画責任者</h3>
					<p class="income">600<span>－</span>1,000<span>万円</span></p>
					<table>
						<tr class="pm">
							<th>ポジション</th>
							<td colspan="3">社長候補</td>
						</tr>
						<tr class="pm">
							<th>ミッション</th>
							<td colspan="3">インターネット事業、ビジネスソリューション事業</td>
						</tr>
						<tr>
							<th>事業概要</th>
							<td colspan="3">電気工事業</td>
						</tr>
						<tr>
							<th>本社所在地</th>
							<td colspan="3">宮城県</td>
						</tr>
						<tr>
							<th>売上高</th>
							<td>1,000憶円以上</td>
							<th>従業員数</th>
							<td>5,001名以上</td>
						</tr>
					</table>
					<div class="btn"><a href="#entry"><img src="img/con04_btn.png" /></a></div>
				</li>
			</ul>
		</div>
	</div>
	
	<div class="con05">
		<div class="con_box">
			<div class="img">
				<img src="img/con05_01.png" alt="秋田銀行,青森銀行,岩手銀行,七十七銀行,常陽銀行,東邦銀行,山形銀行,HUREX" class="pc" />
				<img src="img/con05_01_sp.png" alt="秋田銀行,青森銀行,岩手銀行,七十七銀行,常陽銀行,東邦銀行,山形銀行,HUREX" class="sp" />
				<p class="center">ヒューレックスは銀行とタッグを組むことで、<br/>企業へ優秀な人材を紹介しています。</p>
			</div>
			<ul>
				<li>
					<h2>金融機関「お墨付き」の<br class="pc" />転職エージェント</h2>
					<p>ヒューレックスは日本で唯一、東北全県の地方銀行と業務提携している転職エージェントです。人材紹介業としては初めての取り組みで、現在も北関東を中心に銀行とのパートナーシップを強化しています。</p>
				</li>
				<li>
					<h2>銀行と共に企業を支援</h2>
					<p>銀行は地元の優良企業へ資金を融資し、経営に関連するさまざまな相談を受けます。企業から集まる相談の多くは「後継者や経営幹部の不足」。しかし、銀行が直接、企業の採用活動を支援することはできません。そこで白羽の矢が立ったのが、東日本での実績が豊富なヒューレックスでした。<br/>
					ヒューレックスのミッションは地域の事業承継や採用支援を通して東北を守ること。これからもヒューレックスは、地域経済を支えている銀行とタッグを組み、地方創生の実現に向けて歩み続けます。</p>
				</li>
			</ul>
		</div>
	</div>
	
	<div id="conversation" class="con06">
		<div class="con_box">
			<h2 class="title title_w">地方創生対談<span>conversation</span></h2>
			<ul>
				<li>
					<h3><img src="img/con06_01.png" alt="小池百合子,Yuriko Koike" /></h3>
					<h4><span>経済はサステイナブル社会の根幹</span></h4>
					<ul class="btn">
						<li><a href="https://www.hurex.jp/interview/vol_028/" target="_blank"><img src="img/con06_btn01.png" alt="前編" /></a></li>
						<li><a href="https://www.hurex.jp/interview/vol_029/" target="_blank"><img src="img/con06_btn02.png" alt="後編" /></a></li>
					</ul>
				</li>
				<li>
					<h3><img src="img/con06_02.png" alt="石破茂,Shigeru Ishiba" /></h3>
					<h4><span>産学官金労言の連携不可欠</span></h4>
					<ul class="btn">
						<li><a href="https://www.hurex.jp/interview/vol_026/" target="_blank"><img src="img/con06_btn01.png" alt="前編" /></a></li>
						<li><a href="https://www.hurex.jp/interview/vol_027/" target="_blank"><img src="img/con06_btn02.png" alt="後編" /></a></li>
					</ul>
				</li>
				<li>
					<h3><img src="img/con06_03.png?d=0929" alt="増田寛也,Hiroya Masuda" /></h3>
					<h4><span>東北各県は貪欲に取り組むべき</span></h4>
					<ul class="btn">
						<li><a href="https://www.hurex.jp/interview/vol_023/" target="_blank"><img src="img/con06_btn01.png" alt="前編" /></a></li>
						<li><a href="https://www.hurex.jp/interview/vol_024/" target="_blank"><img src="img/con06_btn02.png" alt="後編" /></a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	
	<div id="entry" class="con07">
		<div class="con_box">
			<h2 class="title">東北企業の後継者､後継幹部に立候補する<span>ENTRY</span></h2>
			<div class="form">
				<?php
					if (!empty($_POST['message'])) {
						foreach ($_POST['message'] as $key => $value) {
							echo "<div class='form_err'>" . $value . "</div>";
						}
					}
				?>
				<form action="mailconfirm.php#entry" method="post">
					<table>
						<tr>
							<th><span class="mark">必須</span> お名前</th>
							<td>
								<input type="text" name="name" id="name" value="<?php echo $values['name'] ?>" placeholder="仙台 太郎" size="20" />　
							</td>
						</tr>
						<tr>
							<th><span class="mark">必須</span> ふりがな</th>
							<td>
								<input type="text" name="namekana" id="namekana" value="<?php echo $values['namekana'] ?>" placeholder="せんだい たろう" size="20" />　
							</td>
						</tr>
						<tr>
							<th><span class="mark">必須</span> 電話番号</th>
							<td>
								<input type="tel" name="tel" id="tel" value="<?php echo $values['tel'] ?>" size="15" placeholder="09012345678" />
							</td>
						</tr>
						<tr>
							<th><span class="mark">必須</span> 住所(都道府県)</th>
							<td>
								<select name="prefectures" id="prefectures">
									<?php write_select_prefectures($values['prefectures']); ?>
								</select>
							</td>
						</tr>
						<tr>
							<th><span class="mark">必須</span> 最終学歴</th>
							<td>
								<select name="academicbackground" id="academicbackground">
									<?php write_select_academicbackground($values['academicbackground']); ?>
								</select>
							</td>
						</tr>
						<tr>
							<th><span class="mark">必須</span> 生年月日</th>
							<td>
								<select name="birthyear" id="birthyear">
									<?php write_select_birthyear($values['birthyear']); ?>
								</select>
								<select name="birthmonth" id="birthmonth">
									<?php write_select_birthmonth($values['birthmonth']); ?>
								</select>
								<select name="birthdate" id="birthdate">
									<?php write_select_birthdate($values['birthdate']); ?>
								</select>
							</td>
						</tr>
						<tr>
							<th><span class="mark gray">任意</span> その他 ご希望記入欄</th>
							<td>
								<textarea name="coment" id="coment" rows="10"><?php echo $values['coment'] ?></textarea>
							</td>
						</tr>
						<tr>
							<th><span class="mark">必須</span> メールアドレス</th>
							<td>
								<input type="email" name="mail" id="mail" value="<?php echo $values['mail'] ?>" size="30" placeholder="info@excelletos.com" />
							</td>
						</tr>
					</table>

					<button class="submit" type="submit"><img src="img/btn_submit.png" alt="入力した内容を確認する" /></button>
				</form>
			</div>
		</div>
	</div>
	
	<footer>
		<img src="img/ft_logo.png" alt="一流の転職をするならエクセレトス(EXCELLETOS)" />
	</footer>

</div> <!-- /content -->
<script src="//app.gorilla-efo.com/js/efo.777.js" type="text/javascript"></script>
</body>
</html>
