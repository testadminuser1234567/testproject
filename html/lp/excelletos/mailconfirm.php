<?php
	require_once('MailForm.php');

	$mf = new MailForm($_POST);

	// パラメータチェック
	$errormessage = $mf->check();
	if (!empty($errormessage)) {
		// エラーメッセージ設定
		$_POST['message'] = $errormessage;
		// 入力画面に遷移
		require('mailtop.php');
		exit();
	}

	// HTML特殊文字のエスケープ
	$values = $mf->escape();
?>

<!doctype html>
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="ja" />
	<meta name="viewport" content="width=320,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>送信確認 | 一流の転職をするならエクセレトス(EXCELLETOS)</title>
	<meta name="description" content="一流の転職をするなら「EXCELLETOS」。東北の中小企業は､優秀な後継者･後継幹部になり得る皆さんをお待ちしています。"/>
	<meta name="Keywords" content="EXCELLETOS,転職,求人,東北,仙台,宮城,HUREX,ヒューレックス,福島,山形" />
	<link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/jquery.smoothScroll.js"></script>
	<script src="js/common.js"></script>
	
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
	<!-- End Google Tag Manager -->
</head>
<body>
<div class="content con07">
	<header>
	<div class="header_wrap">
		<h1><a href="mailtop.php"><img src="img/logo.png" alt="一流の転職をするなら「EXCELLETOS」" /></a></h1>
		
		<img src="img/menu.png" class="menu sp" />
		
		<nav class="nav_wrap">
		
		<img src="img/menu_close.png" class="menu sp" />
		
		<ul>
			<li class="link"><a href="mailtop.php#talent">求める人材</a></li>
			<li class="link"><a href="mailtop.php#job">求人情報</a></li>
			<!--<li class="link"><a href="mailtop.php#solution">登録者実績</a></li>-->
			<li class="link"><a href="mailtop.php#conversation">地方創生対談</a></li>
			<li class="btn01 btn"><a href="mailtop.php#entry">後継者･幹部に立候補する</a></li>
			<!--<li class="btn02 btn"><a href="">採用をお考えの企業さまへ</a></li>-->
		</ul>
		</nav>
	</div>
	</header>
	
	<div id="entry" class="con_box">
		<h2 class="confilm_h2 center">以下の内容でメールを送信します。<br class="sp" />よろしいですか？</h2>
		<div class="form confilm">
		<form action="mailcomplete.php#entry" method="post">
			<input type="hidden" name="name" value="<?php echo $values['name'] ?>">
			<input type="hidden" name="namekana" value="<?php echo $values['namekana'] ?>">
			<input type="hidden" name="tel" value="<?php echo $values['tel'] ?>">
			<input type="hidden" name="prefectures" value="<?php echo $values['prefectures'] ?>">
			<input type="hidden" name="academicbackground" value="<?php echo $values['academicbackground'] ?>">
			<input type="hidden" name="birthyear" value="<?php echo $values['birthyear'] ?>">
			<input type="hidden" name="birthmonth" value="<?php echo $values['birthmonth'] ?>">
			<input type="hidden" name="birthdate" value="<?php echo $values['birthdate'] ?>">
			<input type="hidden" name="coment" value="<?php echo $values['coment'] ?>">
			<input type="hidden" name="mail" value="<?php echo $values['mail'] ?>">

			<table>
				<tr>
					<th>お名前</th>
					<td><?php echo $values['name'] ?></td>
				</tr>
				<tr>
					<th>ふりがな</th>
					<td><?php echo $values['namekana'] ?></td>
				</tr>
				<tr>
					<th>電話番号</th>
					<td><?php echo $values['tel'] ?></td>
				</tr>
				<tr>
					<th>住所：都道府県</th>
					<td><?php echo $values['prefectures'] ?></td>
				</tr>
				<tr>
					<th>最終学歴</th>
					<td><?php echo $values['academicbackground'] ?></td>
				</tr>
				<tr>
					<th>生年月日</th>
					<td><?php echo $values['birthyear'] ?>年<?php echo $values['birthmonth'] ?>月<?php echo $values['birthdate'] ?>日</td>
				</tr>
				<tr>
					<th>その他ご希望記入欄</th>
					<td><?php echo $values['coment'] ?></td>
				</tr>
				<tr>
					<th>メールアドレス</th>
					<td><?php echo $values['mail'] ?></td>
				</tr>
			</table><br/>

			<button type="submit" class="submit"><img src="img/submit2.png" alt="送信" /></button>
		</form>
		<br/>
		<form action="mailtop.php#entry" method="post">
			<input type="hidden" name="name" value="<?php echo $values['name'] ?>">
			<input type="hidden" name="namekana" value="<?php echo $values['namekana'] ?>">
			<input type="hidden" name="tel" value="<?php echo $values['tel'] ?>">
			<input type="hidden" name="prefectures" value="<?php echo $values['prefectures'] ?>">
			<input type="hidden" name="academicbackground" value="<?php echo $values['academicbackground'] ?>">
			<input type="hidden" name="birthyear" value="<?php echo $values['birthyear'] ?>">
			<input type="hidden" name="birthmonth" value="<?php echo $values['birthmonth'] ?>">
			<input type="hidden" name="birthdate" value="<?php echo $values['birthdate'] ?>">
			<input type="hidden" name="coment" value="<?php echo $values['coment'] ?>">
			<input type="hidden" name="mail" value="<?php echo $values['mail'] ?>">
			<button type="submit" class="submit cancel"><img src="img/cancel.png" alt="戻る" /></button>
		</form>
		</div>

	</div>
	
	<footer>
		<img src="img/ft_logo.png" alt="一流の転職をするなら「EXCELLETOS」" />
	</footer>
</div> <!-- /content -->

</body>
</html>
