<?php
	require_once('MailForm.php');

	$mf = new MailForm($_POST);

	// パラメータチェック
	$errormessage = $mf->check();
	if (!empty($errormessage)) {
		// エラーメッセージ設定
		$_POST['message'] = $errormessage;
		// 入力画面に遷移
		require('mailtop.php');
		exit();
	}

	// メール送信
	$mf->send();
?>

<!doctype html>
<html lang="ja">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Language" content="ja" />
	<meta name="viewport" content="width=320,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<title>メール送信完了 | 一流の転職をするならエクセレトス(EXCELLETOS)</title>
	<meta name="description" content="一流の転職をするなら「EXCELLETOS」。東北の中小企業は､優秀な後継者･後継幹部になり得る皆さんをお待ちしています。"/>
	<meta name="Keywords" content="EXCELLETOS,転職,求人,東北,仙台,宮城,HUREX,ヒューレックス,福島,山形" />
	<link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="js/jquery.smoothScroll.js"></script>
	<script src="js/common.js"></script>
	
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
	<!-- End Google Tag Manager -->
</head>
<body>
<div class="content">
	<header>
	<div class="header_wrap">
		<h1><a href="mailtop.php"><img src="img/logo.png" alt="一流の転職をするなら「EXCELLETOS」" /></a></h1>
		
		<img src="img/menu.png" class="menu sp" />
		
		<nav class="nav_wrap">
		
		<img src="img/menu_close.png" class="menu sp" />
		
		<ul>
			<li class="link"><a href="mailtop.php#talent">求める人材</a></li>
			<li class="link"><a href="mailtop.php#job">求人情報</a></li>
			<!--<li class="link"><a href="mailtop.php#solution">登録者実績</a></li>-->
			<li class="link"><a href="mailtop.php#conversation">地方創生対談</a></li>
			<li class="btn01 btn"><a href="mailtop.php#entry">後継者･幹部に立候補する</a></li>
			<!--<li class="btn02 btn"><a href="">採用をお考えの企業さまへ</a></li>-->
		</ul>
		</nav>
	</div>
	</header>

	<div class="form comp">
		メールを送信いたしました。<br/>
		近日中に担当よりメール､もしくはお電話にて<br class="pc"/>
		ご連絡をさせていただきますので､<br class="pc"/>今しばらくお待ちください。
		<br><br/>
		<a href="mailtop.php">トップへ戻る</a>
	</div>
	
	<footer>
		<img src="img/ft_logo.png" alt="一流の転職をするなら「EXCELLETOS」" />
	</footer>
</div> <!-- /content -->
<script src="//app.gorilla-efo.com/js/efo.663.last.js" type="text/javascript"></script>
</body>
</html>
