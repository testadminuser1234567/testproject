<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'ad111u8txo_wp');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'ad111u8txo');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'YxZMmeGt');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1:3307');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'r4Kw1z,-){V5<fK)G:Jn-+r+&M2VQ+@us0?%BPQV|Jt8++e#Qvh l#bErZAp<C1&');
define('SECURE_AUTH_KEY',  '~:0JW)+Ph8BxgD-T8&$E;v.Kvs%i?/R@)g#;k-FH_}+S|8{:c-:I#6Qcq-M%?F:w');
define('LOGGED_IN_KEY',    '(LTK 3|?u<}8+N.T5,:]!j+1RHLJJV+L1|[Vf$(3AAm@)=MDPGl+|-2Z|~t`[ [M');
define('NONCE_KEY',        'e*lhZD#5dKs2S^?SPXp,$||J:?4}SeAy_mh+|BOKS}/MN32~qaPE~?1e|$UmCWc`');
define('AUTH_SALT',        'uA}-s-shHf;S~kN*9@]G`/aQ_{}-sR_gh?Y]D;&_<ZuyY1ZmiT-z|;T&j!N6(]pp');
define('SECURE_AUTH_SALT', 'K^nlv$_+wKQqtKHy.>P;q~D)d&6/REID;h=-;VZ|dc^&:v!8OO5{.T|PP=4bpxMi');
define('LOGGED_IN_SALT',   '{w&W%|=^3N@8_mRi-Xg-VPr=P9Q7?tp:y=LLP,5X]qePd@|.C{Ds QfeaAcJ1(|L');
define('NONCE_SALT',       '1aWO(KralJ!Bb6q8K@JxlJa|9q7dB;u.#)xWCgunDks5]]HlU:I&%b+2UC:)FKRS');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
