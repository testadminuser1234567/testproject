<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="pan">
<div id="panSub">
<ul class="clearfix">
<li><a href="/">ホーム</a></li>
<li>展職ブログ</li>
</ul>
</div>
</div>
<!-- pan end -->
<!-- co start -->
<div id="co" class="clearfix">
<div id="coL">
<!-- page start -->
<div class="page" id="blogPage">
<h3>ブログ</h3>
<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
?>


		<?php
			if ( have_posts() ) :
				echo '<ul class="list01">';
				// Start the Loop.
				while ( have_posts() ) : the_post();
					
					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					$more="";
					$thumb = get_the_post_thumbnail();
					if(empty($thumb)) $thumb = "<img src=\"" . site_url() . "/img/noimage.gif\">";

					$tit = get_the_title();
					$pm = get_permalink();
$category = get_the_category(); 
					$conLen = mb_strlen(strip_tags(get_the_content("")),'UTF-8');
					$content = mb_substr(strip_tags(get_the_content("")),0,100,'UTF-8');
					if($conLen>100) $more = '&nbsp;&nbsp;<a href="'.get_permalink().'">...続きを読む</a>';
					echo '<li class="clearfix"><p class="photo">' . preg_replace('/(width|height)=\"\d*\"\s/', "", $thumb) . '</p>' . '  <span class="category">' .  $category[0]->cat_name . '</span><br />' . '  <a href="' . $pm . '"><strong>' . $tit . '</strong></a><br />' . $content . $more .'</li>';

				endwhile;
				echo '</ul>';
				// Previous/next post navigation.
//				twentyfourteen_paging_nav();

			else :
				// If no content, include the "No posts found" template.
				get_template_part( 'content', 'none' );

			endif;
if (function_exists("pagination")) {
	pagination($additional_loop->max_num_pages);
}

		?>

</div>
</div>
<?php
get_sidebar();
get_footer();