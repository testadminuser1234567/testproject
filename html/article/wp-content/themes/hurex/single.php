<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<!-- pan start -->
<div id="pan">
<div id="panSub">
<ul class="clearfix">
<li><a href="../index.php">ホーム</a></li>
<li><a href="<?php bloginfo('url'); ?>">展職ブログ</a></li>
<li><?php echo get_the_title();?></li>
</ul>
</div>
</div>
<!-- pan end -->
<div id="co" class="clearfix">
<div id="coL">
<!--
<ul class="snsb clearfix">
<li>
<div class="fb-share-button" data-href="<?php the_permalink(); ?>" data-layout="button_count"></div>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&appId=386404771535028&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</li>
<li>
<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
</li>
</ul>
-->
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
$category = get_the_category(); 
					echo "<div class=\"page b-article\"><h3><span class=\"category\">" . $category[0]->cat_name ."</span>\n<span class=\"date\">" . get_the_date() . "</span><br />";
					echo get_the_title() . "</h3><div class='b-detail'>";
					echo '' . get_the_content() . "</div></div>";

//previous_post_link('<span class="prev">%link</span>', '&lt 前の記事へ');
//next_post_link('<span class="next">%link</span>', '次の記事へ &gt');
//previous_post_link('<span class="prev">&lt%link</span>');
//next_post_link('<span class="next">%link&gt</span>');
//					get_template_part( 'content', get_post_format() );

					// Previous/next post navigation.
//					twentyfourteen_post_nav();

					// If comments are open or we have at least one comment, load up the comment template.
//					if ( comments_open() || get_comments_number() ) {
//						comments_template();
//					}
				endwhile;
			?>
</div>


<?php
get_sidebar( 'content' );
get_sidebar();
get_footer();