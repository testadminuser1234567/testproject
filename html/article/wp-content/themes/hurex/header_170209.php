<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="robots" content="index,follow" />
<meta name="description" content="仙台駅から歩いて5分、アエル17階にある東北最大級の転職支援会社。仙台・宮城・東北地方の非公開求人多数！無料転職相談実施中！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。" />
<meta http-equiv="imagetoolbar" content="no" />
<meta http-equiv="Content-Language" content="ja" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="JavaScript" />
<link rel="alternate" type="application/rss+xml" title="展職ブログ &raquo; フィード" href="<?php echo home_url();?>/artcile/?feed=rss2" />
<link rel="alternate" type="application/rss+xml" title="展職ブログ &raquo; コメントフィード" href="<?php echo home_url();?>/article/?feed=comments-rss2" />
<link href="<?php echo home_url();?>/article/wp-content/themes/hurex/layout.css" rel="stylesheet" type="text/css" media="all" />
<link href="/css/import.css" rel="stylesheet" type="text/css" media="all" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="<?php echo home_url();?>/article/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="<?php echo home_url();?>/article/wp-includes/wlwmanifest.xml" /> <meta name="copyright" content="" />
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="/js/jquery.page-scroller-308.js" charset="utf-8"></script>
<script type="text/javascript" src="/js/rollover.js"></script>
<script type="text/javascript" src="/js/common.js"></script>
<!--[if lte IE 6]>   
<script type="text/javascript" src="/js/DD_belatedPNG_0.0.8a.js">   
</script>   
<script type="text/javascript">  /* EXAMPLE */  DD_belatedPNG.fix('img');</script>   
<![endif]-->
<script type="text/javascript" src="/js/ga.js"></script>
<script>
$(function(){
	$('#entryPage p.txtMoreBtn').click(function() {
		$(this).next().slideToggle();
	}).next().hide();
});
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-6405327-10', 'auto');
ga('require', 'displayfeatures');
ga('send', 'pageview');

</script>
<!-- Facebook Conversion Code for ヒューレックス リマーケ -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6022076371230', {'value':'0.00','currency':'JPY'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none"
src="https://www.facebook.com/tr?ev=6022076371230&amp;cd[value]=0.00&amp;cd[
currency]=JPY&amp;noscript=1"
/></noscript>
</head>

<body>
<!-- header start -->
<div id="header" class="clearfix">
<h1>ヒューレックスは東北の企業と人を繋ぐ東北No.1の転職支援会社です。</h1>
<?php  $Path = "/"; include(dirname(___FILE__) . '/../include/header.html'); ?>
</div>
<!-- header end -->
<?php  $Path = "/"; include(dirname(___FILE__) . '/../include/navi.html'); ?>
<div id="main" class="common">
<div id="mainArea">
<p><img src="/article/img/title.png" /></p>
</div>
</div>