<?php
$domain = $_SERVER['HTTP_HOST'];
if ($domain == "hurex.jp" && empty($_SERVER['HTTPS'])) {
    header("Location: https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
    exit;
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="index,follow">
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形">
<meta name="description" content="東北地方の求人多数！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon-precomposed" href="https://www.hurex.jp/smart/webclip.png">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="format-detection" content="telephone=no">
<link rel="canonical" href="https://www.hurex.jp/reentry/index.html">
<title>すでに登録されている方専用エントリー｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<link href="../css/import.css" rel="stylesheet" type="text/css">
<link href="form.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="../js/remodal/remodal.css">
<link rel="stylesheet" href="../js/remodal/remodal-default-theme.css">
<script src="../js/remodal/remodal.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="./form_validation.js"></script>
<script>
$(function(){
	$('.txtMoreBtn').click(function() {
		$(this).next().slideToggle();
	}).next().hide();
});
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/menu.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/header.html'); ?>
<!-- main start -->
<div id="mainCommon">
<h2>すでに登録されている方専用エントリー</h2>
</div>
<p class="flow"><img src="../entry/img/step_01.png" width="100%" alt=""/></p>
<!-- box start -->
<section class="box normal">

<?php if (strtotime(date('Y-m-d H:i')) < strtotime('2016-12-12 00:00') || strtotime(date('Y-m-d H:i')) >= strtotime('2016-12-12 03:00')):?>
<div id="entryPage" class="new">
<p>本エントリーフォームは、すでに弊社にエントリーされことがある方専用のエントリーフォームです。<br>
下記フォームにご入力の上、[入力内容を確認する]ボタンをクリックしてください。</p>
<div class="ssl clearfix">
<div class="seal">
<script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hurex.jp&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=ja"></script></div>
<p>当サイトは、お客様のプライバシー保護のため、個人情報入力ページにおいてSSL暗号化通信を採用しています</p>
</div>

<?php require_once("../../entry/system/esc.php");?>
<form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="mode" value="confirm">
<div class="error"><?php echo pf($error);?></div>
<h2 class="no"><span class="<?php if($shimei):?>ok<?php else:?>hissu<?php endif;?> shimei">氏名</span></h2>
<p><input name="shimei" type="text" class="hissu required txtBox" id="shimei" placeholder="入力例:鈴木 太郎" style="width:100%;" value="<?php echo esc($shimei);?>" maxlength="25" /></p>

<h2 class="no"><span class="<?php if($kana):?>ok<?php else:?>hissu<?php endif;?> kana">ふりがな</span></h2>
<p><input type="text" name="kana" id="kana" class="hissu required txtBox" placeholder="入力例:すずき たろう" value="<?php echo esc($kana);?>" maxlength="25" style="width:100%;" /></p>
<h2><span class="<?php if($tel1):?>ok<?php else:?>hissu<?php endif;?> tel1">電話</span></h2>
<p><input name="tel1" type="number" class="hissu txtBox" id="tel1" placeholder="入力例:08098765432" style="width:100%;" value="<?php echo esc($tel1);?>" /></p>



<h2><span class="<?php if($comment):?>ok<?php else:?>nini<?php endif;?> comment">ご希望やご質問を記入ください</span></h2>
<p>
<textarea name="comment" cols="70" rows="10" id="comment" placeholder="記入例：&#13;&#10;ご希望年収、転勤の有無、&#13;&#10;電話連絡は19時以降希望など"><?php echo esc($comment);?></textarea>
</p>


<!-- privacyBox start -->
<div id="privacyBox">
<div id="privacyBoxSub">
<p><a href="../pdf/pp.pdf" target="_blank">個人情報保護への取り組み</a>をご覧ください</p>
<p class="checkboxrequired">
<label for="agreeChk" class="CheckBoxLabelClass privacy<?php if($agree):?> checked<?php endif;?>">
<input type="hidden" name="agree" value="0" />
<input type="checkbox" class="rc checkbox " name="agree" value="1" id="agreeChk" <?php if(esc($agree)=="1"):?>checked<?php endif;?>>
個人情報保護方針に同意する</label>
</p>
</div>
</div>
<!-- privacyBox end -->
<!-- start -->

<div class="btnSend check">
<input name="btnKakunin" type="image" id="btnKakunin" class="image wide" onClick="return check(1);" value="送信確認画面へ" src="../entry/img/btn_check_<?php if($agree==1):?>on<?php else:?>off<?php endif;?>.png" alt="入力内容の確認ページへ">
</div>
<!-- end -->
<?php 
if(!empty($_GET['id'])){
	$job_id="";
	$job_id = htmlspecialchars($_GET['id'], ENT_QUOTES, 'UTF-8');
	if(!is_numeric($job_id)){
		$job_id = "";
	}
}else if(empty($job_id)){
	$job_id = "";
}
?>
<input type="hidden" name="job_id" value="<?php echo esc($job_id);?>" />
</form>



<?php else:?>

<div id="entryPage">

<p class="no"><span class="txtRed"><strong>※メンテナンスのご案内</strong></span></p>
<p>ただ今、エントリーフォームはメンテナンス中です。<br>
下記日時にて実施しておりますので、メンテナンス終了まで今しばらくお待ちくださいませ。</p>
<div class="cautionBox">
<p>メンテナンス時間：2016/12/12（月）00:00～03:00<br>
お急ぎの場合は<a href="mailto:info@hurex.co.jp">info@hurex.co.jp</a>までご連絡ください。</p>
</div>
<p>ご不便をお掛け致しますがご了承の程何卒よろしくお願いいたします。</p>

<?php endif;?>

</div>

</div>
</section>
<!-- box end -->
<div id="co">
<?php $Path = "../"; include(dirname(__FILE__).'/../include/menuBox.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/bnrBox.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footLink.html'); ?>
</div>
<!-- co end -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footer.html'); ?>
<script>
$(function(){
    var checkbox = $('input[type="checkbox"]');
    var radio = $('input[type="radio"]');
 
    boxChecked();
    checkbox.on('change', function(){
        boxChecked();
    });
 
    radioChecked();
    radio.on('click', function(){
        radioChecked();
    });
 
    function boxChecked() {
        checkbox.each(function() {
            if($(this).prop('checked')) {
                $(this).parent().addClass('checked');
            } else {
                $(this).parent().removeClass('checked');
            }
        });
    }
    function radioChecked() {
        radio.each(function() {
            if($(this).prop('checked')) {
                $(this).parent().addClass('checked');
            } else {
                $(this).parent().removeClass('checked');
            }
        });
    }
});
</script>
<!-- リマーケティング タグの Google コード -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963710423;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963710423/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- リマーケティングタグ Yahoo -->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=1Wjm96V";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=1Wjm96V" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>
