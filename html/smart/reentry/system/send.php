<?php
/*
/  送信処理
*/
//エラーチェック
$err = "";
$err = $validation->run();

//エラーがあったら初期画面へ
if(!empty($err)){
	$template->view(Index, $_SESSION, $err);
}else{
	//管理者宛
	include (dirname(__FILE__) . "/qdmail.php");

		require ($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/system_entry/getjob.php');

		if(!empty($data)){
//            $order_mail_subject = $order_mail_subject . " " . $data['job_id'] . " " . $data['job_title'] . " " . $shimei;
            $order_mail_subject = $order_mail_subject . "" . $data['job_id'] . " / " . $shimei . " " . $tel1;
		}

		//送信者（お客さん宛）
		$to = array($email , $unit_name );
		$from = array($thanks_mail_from , $thanks_mail_str );

		$qdmail = new Qdmail();
		//$qdmail->charsetBody( 'utf-8' , 'base64' ) ;
		$qdmail->kana(true);
		$qdmail->mtaOption( "-f $thanks_mail_from" );

/*	
		$qdmail -> easyText(
			$to,
			$thanks_mail_subject ,
			$thanks_body,
			$from
		);
*/

		//管理者宛
	//	$kanri_to[] = $order_mail_to;
	//	$kanri_from = array($mail , $unit_name );
	//	$order_mail_other['from'][] = array($thanks_mail_from, $thanks_mail_str);
		$order_mail_other['from'][] = array($send_from, $send_from);
		$qdmail -> easyText(
			$order_mail_to,
			$order_mail_subject ,
			$order_body,
			$order_mail_other
		);

		if($log=="on"){
			$today = date("Y/m/d G:i");
			$today = $today . "\r\n";	
			//ログの書き込み
			$pointer=fopen("system/log.txt", "a");
			flock($pointer, LOCK_EX);
			fputs($pointer, $today);
			flock($pointer, LOCK_UN);
			fclose($pointer);
		}

		// セッション変数を全て解除する
		$_SESSION = array();
		// 最終的に、セッションを破壊する
		session_destroy();
		header("HTTP/1.1 200 ok");
		$url = Finish;
		header("Location: $url");
		exit();
}
?>