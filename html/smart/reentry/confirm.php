<?php
$domain = $_SERVER['HTTP_HOST'];
if ($domain == "hurex.jp" && empty($_SERVER['HTTPS'])) {
    header("Location: https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
    exit;
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="index,follow">
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形">
<meta name="description" content="東北地方の求人多数！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon-precomposed" href="https://www.hurex.jp/smart/webclip.png">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="format-detection" content="telephone=no">
<link rel="canonical" href="https://www.hurex.jp/reentry/confirm.html">
<title>すでに登録されている方専用エントリー｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<link href="../css/import.css" rel="stylesheet" type="text/css">
<link href="form.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="../js/remodal/remodal.css">
<link rel="stylesheet" href="../js/remodal/remodal-default-theme.css">
<script src="../js/remodal/remodal.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="./form_validation.js"></script>


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/menu.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/header.html'); ?>
<!-- main start -->
<div id="mainCommon">
<h2>すでに登録されている方専用エントリー</h2>
</div>
<!-- main end -->
<p class="flow"><img src="../entry/img/step_02.png" width="100%" alt=""/></p>
<!-- box start -->
<section class="box normal">

<?php require_once("../../entry/system/esc.php");?>
<div id="entryPage" class="new">
<p>以下の内容で送信致します。</p>

<h2 class="no"><span class="<?php if($shimei):?>ok<?php else:?>hissu<?php endif;?> shimei">氏名</span></h2>
<p><?php echo esc($shimei);?></p>

<h2 class="no"><span class="<?php if($kana):?>ok<?php else:?>hissu<?php endif;?> kana">ふりがな</span></h2>
<p><?php echo esc($kana);?></p>

<h2><span class="<?php if($tel1):?>ok<?php else:?>hissu<?php endif;?> tel1">電話</span></h2>
<p><?php echo esc($tel1);?></p>

<h2><span class="<?php if($comment):?>ok<?php else:?>nini<?php endif;?> comment">その他</span></h2>
<p>
<?php echo nl2br(esc($comment));?>
</p>

<!-- start -->
<div class="btnSend aCenter">
<form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data">

<input name="btnSend" type="image" class="image" id="btnSend" style=" width: 90%; height:auto; " value="送信する" onclick="window.onbeforeunload=null;" src="../entry/img/btn_send.png" alt="上記の内容で送信">
<input type="hidden" name="mode" value="send">
<input type="hidden" name="shimei" value="<?php echo esc($shimei);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="tel1" value="<?php echo esc($tel1);?>">
<input type="hidden" name="comment" value="<?php echo esc($comment);?>">
<input type="hidden" name="agree" value="<?php echo esc($agree);?>">
<input type="hidden" name="job_id" value="<?php echo esc($job_id);?>">
</form>
</div><br />
<form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data" class="aCenter">
<input type="hidden" name="mode" value="return">

<input type="image" class="image" value="戻る" style=" width: 60%; height:auto; " src="../entry/img/btn_back.png" onclick="window.onbeforeunload=null;" alt="戻る">
<input type="hidden" name="shimei" value="<?php echo esc($shimei);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="tel1" value="<?php echo esc($tel1);?>">
<input type="hidden" name="comment" value="<?php echo esc($comment);?>">
<input type="hidden" name="agree" value="<?php echo esc($agree);?>">
<input type="hidden" name="job_id" value="<?php echo esc($job_id);?>">
</form>


</div>
</section>
<!-- box end -->
<div id="co">
<?php $Path = "../"; include(dirname(__FILE__).'/../include/menuBox.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/bnrBox.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footLink.html'); ?>
</div>
<!-- co end -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footer.html'); ?>

<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>