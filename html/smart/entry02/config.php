<?php
//入力ページの設定
define('Index', 'form.php');
//確認ページの設定（使わない場合はこのまま）
define('Confirm', 'confirm.php');
//完了ページの設定
define('Finish', 'finish.php');

define('ENCODE', 'UTF-8');

//ユーザの名前設定
$unit_name = $shimei . "様";
//ユーザの宛先設定
if(!empty($mail1)){
	$email = $mail1;
}else{
	$email = $mail2;
}
$today = Date('Y年m月d日');
$send_mail="touroku@hurex.co.jp";
//$send_mail="tosa@smt-net.co.jp";


$birthday = $year . "年" . $month . "月" . $day . "日";
$zaiseki = $zaiseki_year_from . "年" . $zaiseki_month_from . "月～" . $zaiseki_year_to . "年" . $zaiseki_month_to . "月";
$shugyo = $shugyo_year_from."年" .$shugyo_month_from . "月～" . $shugyo_year_to . "年" . $shugyo_month_to . "月";
$shugyo2 = $shugyo_year_from2."年" .$shugyo_month_from2 . "月～" . $shugyo_year_to2 . "年" . $shugyo_month_to2 . "月";
$shugyo3 = $shugyo_year_from3."年" .$shugyo_month_from3 . "月～" . $shugyo_year_to3 . "年" . $shugyo_month_to3 . "月";

/*
$job1_mail = $job["$job1"];
$job2_mail = $job["$job2"];
$job3_mail = $job["$job3"];
$biz1_mail = $biz["$biz1"];
$biz2_mail = $biz["$biz2"];
$biz3_mail = $biz["$biz3"];
$kibo_biz1_mail = $biz["$kibo_biz1"];
$kibo_biz2_mail = $biz["$kibo_biz2"];
$kibo_biz3_mail = $biz["$kibo_biz3"];
$kibo_job1_mail = $job["$kibo_job1"];
$kibo_job2_mail = $job["$kibo_job2"];
$kibo_job3_mail = $job["$kibo_job3"];
*/
$tmp_company_number=$company_number;
if($company_number==5){
	$tmp_company_number="5社以上";
}else{
	$tmp_company_number.="社";
}

//サンクスメール設定
$thanks_mail_subject = "【" . $shimei . " 様】東北Ｕ・Ｉターン転職相談会 in 東京へお申し込みいただき、ありがとうございます／ヒューレックス株式会社";
$thanks_mail_from = "touroku@hurex.co.jp";
//$thanks_mail_from = "tosa@smt-net.co.jp";
$thanks_mail_str = "ヒューレックス株式会社";
$thanks_body= <<< EOF
$shimei 様

お世話になっております。
転職支援会社 ヒューレックスでございます。

この度は、弊社ホームページより東北Ｕ・Ｉターン転職相談会 in 東京へお申し込みいただき、誠にありがとうございました。

今後ご転職活動を支援させていただきますので、どうぞ宜しくお願い致します。

さて、弊社では毎週数多くの方から東北Ｕ・Ｉターン転職相談会 in 東京へのお申し込みを頂戴しておりますが、定数に限りがある関係上、すぐに具体的な求人情報をご案内できる方から優先的にご予約を受け付けさせていただいております。

そのため、まずはこれまでのご経験やご希望条件を確認させていただき、ご紹介できる求人を探して参りたいと思います。
　
簡単な入力フォームを用意しておりますので、下記URLよりアクセスしていただき、ご入力をお願い致します。

ご入力された内容の確認が取れましたら、改めて担当より連絡いたします。

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【ご経歴・ご希望条件入力フォーム】
　　▼　　▼　　▼　　▼　　▼
https://business.form-mailer.jp/fms/831589fa42879

※お申し込み時に履歴書や職務経歴書を添付された方は、ご入力いただかなくて結構です。
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱いおよび管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
  　仙台市青葉区中央1-3-1 アエル17階
　E-mail　ca@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■東日本事業承継推進機構（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。



＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

EOF;

//通知メール設定
$order_mail_subject = "【東北Ｕ・Ｉターン出張相談会in東京  登録通知】ENTRY（スマートフォンサイト）";
$order_mail_to[] = array('touroku@hurex.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('tosa@smt-net.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('nobuyoshi.takahashi@hurex.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('t-kojima@smt-net.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('info@smt-net.co.jp', '追加宛先');
$order_body= <<< EOF
ご担当者様
ホームページ(スマートフォンサイト）より、 $shimei 様のエントリーがありました。
HRBCにログインし、エントリー情報をご確認下さい。

[氏名]
$shimei

[ふりがな]
$kana

[生年月日]
$birthday

[性別]
$sex

[現住所]
$pref

[電話番号]
$tel1

[メールアドレス]
$mail1

[最終学歴]
$school_div_id

[経験社数]
$tmp_company_number

[勤務地（第1希望)]
$expectarea1

[勤務地（第2希望)]
$expectarea2

[勤務地（第3希望)]
$expectarea3

[ご希望の参加日]
$comment

EOF;

//項目チェック用
$validation->set_rules('agree','個人情報保護方針','required');
$validation->set_rules('shimei','氏名','required|max_length[25]');
$validation->set_rules('kana','フリガナ','required|max_length[25]');
$validation->set_rules('year','年','required');
$validation->set_rules('month','月','required');
$validation->set_rules('day','日','required');
$validation->set_rules('sex','性別','required');
//$validation->set_rules('zip','郵便番号','required|max_length[8]|alpha_dash');
$validation->set_rules('pref','都道府県','required');
//$validation->set_rules('address','住所','required|max_length[50]');
$validation->set_rules('tel1','電話','required|max_length[15]');
//$validation->set_rules('tel2','電話2','max_length[15]');
//$validation->set_rules('jokyo','就業状況','required');
$validation->set_rules('mail1','メール','required|valid_email|max_length[50]');
//$validation->set_rules('mail2','メール2','valid_email|max_length[50]');
$validation->set_rules('renraku1','連絡可能時間帯（平日）','');
$validation->set_rules('renraku2','連絡可能時間帯（土日）','');
$validation->set_rules('school_div_id','最終学歴','required');
//$validation->set_rules('school','学校名','max_length[50]|required');
$validation->set_rules('company_number','転職回数','');
$validation->set_rules('comment','ご希望の参加日','required');
/*
$validation->set_rules('division','学部・学科名','max_length[30]');
$validation->set_rules('zaiseki_year_from','在籍年','');
$validation->set_rules('zaiseki_year_from','在籍月','');
$validation->set_rules('zaiseki_month_to','在籍年','');
$validation->set_rules('zaiseki_month_from','在籍月','');
$validation->set_rules('company_number','在籍社数','');
$validation->set_rules('jokyo','就業状況','');
$validation->set_rules('job1','経験職種','');
$validation->set_rules('jigyo_naiyo','事業内容','max_length[10000]');
$validation->set_rules('jigyo_naiyo2','事業内容2','max_length[10000]');
$validation->set_rules('jigyo_naiyo3','事業内容3','max_length[10000]');
$validation->set_rules('company_name','企業名','');
$validation->set_rules('biz1','業種','');
$validation->set_rules('income','年収','');
$validation->set_rules('shugyo_keitai','就業形態','');
$validation->set_rules('yakushoku','役職','');
$validation->set_rules('shugyo_year_from','就業年','');
$validation->set_rules('shugyo_month_from','就業月','');
$validation->set_rules('shugyo_year_to','就業年','');
$validation->set_rules('shugyo_month_to','就業月','');
$validation->set_rules('shugyo_naiyo','就業内容','max_length[10000]');
$validation->set_rules('shugyo_naiyo2','就業内容2','max_length[10000]');
$validation->set_rules('shugyo_naiyo3','就業内容3','max_length[10000]');
$validation->set_rules('comment','その他コンサルタントに伝えたいこと','max_length[10000]');
$validation->set_rules('kibo_biz1','希望業種','');
$validation->set_rules('kibo_job1','希望職種','');
$validation->set_rules('kibo_pref1','希望勤務地','required');
$validation->set_rules('kibo_income','希望年収','');
$validation->set_rules('howto','弊社を知ったきっかけ','');
$validation->set_rules('toefl','TOEFL','numeric');
$validation->set_rules('toeic','TOEIC','numeric');
$validation->set_rules('kibo_income','希望年収','numeric');
*/
$validation->set_rules('expectarea1','勤務地（第1希望','required');
$validation->set_rules('expectarea2','勤務地（第2希望','');
$validation->set_rules('expectarea3','勤務地（第3希望','');

//ログファイル出力 on or off
$log = "off";

?>