<?php
$domain = $_SERVER['HTTP_HOST'];
if ($domain == "hurex.jp" && empty($_SERVER['HTTPS'])) {
    header("Location: https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
    exit;
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="index,follow">
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形">
<meta name="description" content="東北地方の求人多数！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon-precomposed" href="https://www.hurex.jp/smart/webclip.png">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="format-detection" content="telephone=no">
<link rel="canonical" href="https://www.hurex.jp/entry/confirm.php">
<title>東北 U・Iターン出張相談会 in 東京｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<link href="../css/import.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="../js/remodal/remodal.css">
<link rel="stylesheet" href="../js/remodal/remodal-default-theme.css">
<script src="../js/remodal/remodal.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="./form_validation.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/menu.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/header.html'); ?>
<!-- main start -->
<div id="mainCommon">
<h2>東北 U・Iターン出張相談会 in 東京</h2>
</div>
<!-- main end -->
<p class="flow"><img src="../entry/img/step_02.png" width="100%" alt=""/></p>
<!-- box start -->
<section class="box normal">
<?php require_once("../../entry/system/esc.php");?>
<div id="entryPage" class="new">
<p>以下の内容で送信致します。</p>

<h2 class="no"><span class="<?php if($shimei):?>ok<?php else:?>hissu<?php endif;?> shimei">氏名</span></h2>
<p><?php echo esc($shimei);?></p>

<h2 class="no"><span class="<?php if($kana):?>ok<?php else:?>hissu<?php endif;?> kana">ふりがな</span></h2>
<p><?php echo esc($kana);?></p>

<h2><span class="<?php if($year && $month && $day):?>ok<?php else:?>hissu<?php endif;?> birth">生年月日</span></h2>
<p><?php echo esc($year);?>年<?php echo esc($month);?>月<?php echo esc($day);?>日</p>
<h2><span class="<?php if($sex):?>ok<?php else:?>hissu<?php endif;?> sex">性別</span></h2>
<p>
<?php echo esc($sex);?>
</p>
<h2><span class="<?php if($pref):?>ok<?php else:?>hissu<?php endif;?> pref">現住所</span></h2>
<p>

<?php echo esc($pref);?>
</p>
<h2><span class="<?php if($tel1):?>ok<?php else:?>hissu<?php endif;?> tel1">電話</span></h2>
<p><?php echo esc($tel1);?></p>
<h2><span class="<?php if($mail1):?>ok<?php else:?>hissu<?php endif;?> mail1">メール</span></h2>
<p><?php echo esc($mail1);?>
</p>
<h2><span class="<?php if($school_div_id):?>ok<?php else:?>hissu<?php endif;?> school_div_id">最終学歴</span></h2>
<p>
<?php echo esc($school_div_id);?>
</p>
<h2><span class="<?php if($company_number):?>ok<?php else:?>hissu<?php endif;?> company_number">経験社数</span></h2>
<p><?php if($company_number==5):?>
5社以上
<?php else:?>
<?php echo esc($company_number);?>社
<?php endif;?>
</p>

        <h2><span class="<?php if($jokyo):?>ok<?php else:?>hissu<?php endif;?> jokyo">就業状況</span></h2>
		<p><?php echo esc($jokyo);?></p>
<h2><span class="<?php if($expectarea1):?>ok<?php else:?>hissu<?php endif;?> expectarea1">勤務地（第1希望）:都道府県</span></h2>
        <p>
<?php echo esc($expectarea1);?>
        </p>

<h2><span class="<?php if($expectarea2):?>ok<?php else:?>nini<?php endif;?> expectarea2">勤務地（第2希望）:都道府県</span></h2>
        <p>
<?php echo esc($expectarea2);?>
        </p>

<h2><span class="<?php if($expectarea3):?>ok<?php else:?>nini<?php endif;?> expectarea3">勤務地（第3希望）:都道府県</span></h2>
        <p>
<?php echo esc($expectarea3);?>
        </p>

<?php if(!empty($comment)):?>

<h2><span class="<?php if($comment):?>ok<?php else:?>hissu<?php endif;?> comment">ご希望の参加日をご記入ください</span></h2>
<p>
<?php echo nl2br(esc($comment));?>
</p>

<?php endif;?>

<!-- start -->
<div class="btnSend aCenter">

<form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data">
<input name="btnSend" type="image" class="image" id="btnSend" style=" width: 90%; height:auto; " value="送信する" src="../entry/img/btn_send.png" onclick="window.onbeforeunload=null;" alt="上記の内容で送信">
<input type="hidden" name="mode" value="send">
<input type="hidden" name="shimei" value="<?php echo esc($shimei);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="year" value="<?php echo esc($year);?>">
<input type="hidden" name="month" value="<?php echo esc($month);?>">
<input type="hidden" name="day" value="<?php echo esc($day);?>">
<input type="hidden" name="sex" value="<?php echo esc($sex);?>">
<input type="hidden" name="zip" value="<?php echo esc($zip);?>">
<input type="hidden" name="pref" value="<?php echo esc($pref);?>">
<input type="hidden" name="address" value="<?php echo esc($address);?>">
<input type="hidden" name="tel1" value="<?php echo esc($tel1);?>">
<input type="hidden" name="tel2" value="<?php echo esc($tel2);?>">
<input type="hidden" name="mail1" value="<?php echo esc($mail1);?>">
<input type="hidden" name="mail2" value="<?php echo esc($mail2);?>">
<input type="hidden" name="file1" value="<?php echo esc($file1);?>">
<input type="hidden" name="file2" value="<?php echo esc($file2);?>">
<input type="hidden" name="agree" value="<?php echo esc($agree);?>">
<input type="hidden" name="jokyo" value="<?php echo esc($jokyo);?>">
<input type="hidden" name="renraku1" value="<?php echo esc($renraku1);?>">
<input type="hidden" name="renraku2" value="<?php echo esc($renraku2);?>">

<input type="hidden" name="school_div_id" value="<?php echo esc($school_div_id);?>">
<input type="hidden" name="school" value="<?php echo esc($school);?>">
<input type="hidden" name="company_number" value="<?php echo esc($company_number);?>">

<input type="hidden" name="kibo_job1" value="<?php echo esc($kibo_job1);?>">
<input type="hidden" name="kibo_pref1" value="<?php echo esc($kibo_pref1);?>">
<input type="hidden" name="kibo_jiki_year" value="<?php echo esc($kibo_jiki_year);?>">
<input type="hidden" name="kibo_jiki_month" value="<?php echo esc($kibo_jiki_month);?>">
<input type="hidden" name="kibo_jiki_day" value="<?php echo esc($kibo_jiki_day);?>">
<input type="hidden" name="kibou_jiki" value="<?php echo esc($kibou_jiki);?>">
<input type="hidden" name="comment" value="<?php echo esc($comment);?>">
    <input type="hidden" name="expectarea1" value="<?php echo esc($expectarea1);?>">
    <input type="hidden" name="expectarea2" value="<?php echo esc($expectarea2);?>">
    <input type="hidden" name="expectarea3" value="<?php echo esc($expectarea3);?>">
<input type="hidden" name="job_id" value="<?php echo esc($job_id);?>">
</form>
</div>
<br>

<form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data" class="aCenter">
<input type="hidden" name="mode" value="return">
<input type="image" class="image" value="戻る" style=" width: 60%; height:auto; " src="../entry/img/btn_back.png" onclick="window.onbeforeunload=null;" alt="戻る">
<input type="hidden" name="shimei" value="<?php echo esc($shimei);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="year" value="<?php echo esc($year);?>">
<input type="hidden" name="month" value="<?php echo esc($month);?>">
<input type="hidden" name="day" value="<?php echo esc($day);?>">
<input type="hidden" name="sex" value="<?php echo esc($sex);?>">
<input type="hidden" name="zip" value="<?php echo esc($zip);?>">
<input type="hidden" name="pref" value="<?php echo esc($pref);?>">
<input type="hidden" name="address" value="<?php echo esc($address);?>">
<input type="hidden" name="tel1" value="<?php echo esc($tel1);?>">
<input type="hidden" name="tel2" value="<?php echo esc($tel2);?>">
<input type="hidden" name="mail1" value="<?php echo esc($mail1);?>">
<input type="hidden" name="mail2" value="<?php echo esc($mail2);?>">
<input type="hidden" name="file1" value="<?php echo esc($file1);?>">
<input type="hidden" name="file2" value="<?php echo esc($file2);?>">
<input type="hidden" name="agree" value="<?php echo esc($agree);?>">
<input type="hidden" name="jokyo" value="<?php echo esc($jokyo);?>">
<input type="hidden" name="renraku1" value="<?php echo esc($renraku1);?>">
<input type="hidden" name="renraku2" value="<?php echo esc($renraku2);?>">

<input type="hidden" name="school_div_id" value="<?php echo esc($school_div_id);?>">
<input type="hidden" name="school" value="<?php echo esc($school);?>">
<input type="hidden" name="company_number" value="<?php echo esc($company_number);?>">

<input type="hidden" name="kibo_job1" value="<?php echo esc($kibo_job1);?>">
<input type="hidden" name="kibo_pref1" value="<?php echo esc($kibo_pref1);?>">
<input type="hidden" name="kibo_jiki_year" value="<?php echo esc($kibo_jiki_year);?>">
<input type="hidden" name="kibo_jiki_month" value="<?php echo esc($kibo_jiki_month);?>">
<input type="hidden" name="kibo_jiki_day" value="<?php echo esc($kibo_jiki_day);?>">
<input type="hidden" name="kibou_jiki" value="<?php echo esc($kibou_jiki);?>">
<input type="hidden" name="comment" value="<?php echo esc($comment);?>">
    <input type="hidden" name="expectarea1" value="<?php echo esc($expectarea1);?>">
    <input type="hidden" name="expectarea2" value="<?php echo esc($expectarea2);?>">
    <input type="hidden" name="expectarea3" value="<?php echo esc($expectarea3);?>">
<input type="hidden" name="job_id" value="<?php echo esc($job_id);?>">
</form>



</div>



</section>
<!-- box end -->
<div id="co">
<?php $Path = "../"; include(dirname(__FILE__).'/../include/menuBox.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/bnrBox.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footLink.html'); ?>
</div>
<!-- co end -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footer.html'); ?>

<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>
