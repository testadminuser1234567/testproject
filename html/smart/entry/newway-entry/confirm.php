<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="index,follow">
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形">
<meta name="description" content="東北地方の求人多数！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon-precomposed" href="https://www.hurex.jp/smart/webclip.png">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="format-detection" content="telephone=no">
<link rel="canonical" href="https://www.hurex.jp/entry/newway-entryy/confirm.php">
<title>新卒採用エントリーフォーム｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<link href="../../css/import.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="../../js/remodal/remodal.css">
<link rel="stylesheet" href="../../js/remodal/remodal-default-theme.css">
<script src="../../js/remodal/remodal.js"></script>
<script type="text/javascript" src="../../js/common.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<?php $Path = "../../"; include(dirname(__FILE__).'/../../include/menu.html'); ?>
<?php $Path = "../../"; include(dirname(__FILE__).'/../../include/header.html'); ?>
<!-- main start -->
<div id="mainCommon">
<h2>採用情報</h2>
</div>
<!-- main end -->

<!-- box start -->
<section class="box">
<h1>新卒採用エントリーフォーム</h1>

<?php require_once("../../../entry/system/esc.php");?>
<div id="entryPage">
<p class="pctL">
<script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hurex.jp&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=ja"></script>
</p>
<p>当サイトは、お客様のプライバシー保護のため、個人情報入力ページにおいてSSL暗号化通信を採用しています</p>
<br class="clearBT">
<form action="mail.php" method="post">
<input type="hidden" name="mode" value="send">
<p>以下の内容で送信致します。</p>
<table summary="フォーム" class="normal">
<tr>
<th>募集職種<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo esc($job);?></td>
</tr>
<tr>
<th>氏名<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo esc($onamae);?></td>
</tr>
<tr>
<th>フリガナ<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo esc($kana);?></td>
</tr>
<tr>
<th>郵便番号<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo esc($zip);?></td>
</tr>
<tr>
<th>住所<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo esc($address);?></td>
</tr>
<tr>
<th>電話番号<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo esc($tel);?></td>
</tr>
<tr>
<th>E-mail<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo esc($mail);?></td>
</tr>
<tr>
<th>生年月日<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo esc($birthday);?></td>
</tr>
<tr>
<th>最終学歴(卒業見込み)<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo esc($graduate);?></td>
</tr>
<tr>
<th>学校学部名<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo esc($gakubu);?></td>
</tr>
<tr>
<th>応募理由<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo nl2br(esc($reason));?></td>
</tr>
<tr>
<th>学生生活で頑張ったことは何ですか<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo nl2br(esc($school));?></td>
</tr>
<tr>
<th>今までで一番困難だったことは何ですか<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo nl2br(esc($hard));?></td>
</tr>
<tr>
<th>その困難をどのように乗り越えましたか<span class="point">※</span></th>
</tr>
<tr>
<td><?php echo nl2br(esc($over));?></td>
</tr>
</table>
<div style="display:block;height:20px;">
<form id="entryForm" action="mail.php" method="post">
<input type="submit" name="ret" value=" 戻る " style="float:left;margin-left:10px;">
<input type="hidden" name="mode" value="return">
<input type="hidden" name="job" value="<?php echo esc($job);?>">
<input type="hidden" name="onamae" value="<?php echo esc($onamae);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="zip" value="<?php echo esc($zip);?>">
<input type="hidden" name="address" value="<?php echo esc($address);?>">
<input type="hidden" name="tel" value="<?php echo esc($tel);?>">
<input type="hidden" name="birthday" value="<?php echo esc($birthday);?>">
<input type="hidden" name="mail" value="<?php echo esc($mail);?>">
<input type="hidden" name="graduate" value="<?php echo esc($graduate);?>">
<input type="hidden" name="gakubu" value="<?php echo esc($gakubu);?>">
<input type="hidden" name="reason" value="<?php echo esc($reason);?>">
<input type="hidden" name="school" value="<?php echo esc($school);?>">
<input type="hidden" name="hard" value="<?php echo esc($hard);?>">
<input type="hidden" name="over" value="<?php echo esc($over);?>">
</form>
<form id="entryForm" action="mail.php" method="post">
<input type="submit" name="submit" value="送信する" style="float:left;margin-left:10px">
<input type="hidden" name="mode" value="send">
<input type="hidden" name="job" value="<?php echo esc($job);?>">
<input type="hidden" name="onamae" value="<?php echo esc($onamae);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="zip" value="<?php echo esc($zip);?>">
<input type="hidden" name="address" value="<?php echo esc($address);?>">
<input type="hidden" name="tel" value="<?php echo esc($tel);?>">
<input type="hidden" name="birthday" value="<?php echo esc($birthday);?>">
<input type="hidden" name="mail" value="<?php echo esc($mail);?>">
<input type="hidden" name="graduate" value="<?php echo esc($graduate);?>">
<input type="hidden" name="gakubu" value="<?php echo esc($gakubu);?>">
<input type="hidden" name="reason" value="<?php echo esc($reason);?>">
<input type="hidden" name="school" value="<?php echo esc($school);?>">
<input type="hidden" name="hard" value="<?php echo esc($hard);?>">
<input type="hidden" name="over" value="<?php echo esc($over);?>">
</form>
</div>
</div>



</section>
<!-- box end -->

<!-- co start -->
<div id="co">
<?php $Path = "../../"; include(dirname(__FILE__).'/../../include/menuBox.html'); ?>
<?php $Path = "../../"; include(dirname(__FILE__).'/../../include/bnrBox.html'); ?>
<?php $Path = "../../"; include(dirname(__FILE__).'/../../include/footLink.html'); ?>
</div>
<!-- co end -->
<?php $Path = "../../"; include(dirname(__FILE__).'/../../include/footer.html'); ?>

<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>
