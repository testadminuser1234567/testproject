<?php
class template 
{
	function template(){
	}
	
	function view($template, $data, $err){
		$error="";
		$template_name = $template;
		
		//エラーデータ格納
		$error=$err;
		
		//postデータを設定
		if(!empty($data)){
			foreach($data as $key => $val){
				if(!preg_match('/^submit$/', $key) && !preg_match('/^btn/', $key) && !preg_match('/^mode/', $key)){
					if(is_array($val)){
						for($i=0;$i<count($val);$i++){
							if(is_string($val[$i])){
								//入力エラー時は可変変数の配列に格納（$checkbox[0]=>"test")
								if($template==Index){
									$tmp_val = $val[$i];
									//エラー時に値を設定（チェックボックス）
									${$key}[$val[$i]] = "checked";
									//確認と送信時に渡す値（配列）
									${$key}[$i] = htmlspecialchars($val[$i], ENT_QUOTES);
								//確認画面の場合は画面用に改行入りで格納
								}else if($template==Confirm){
									$tmp_val = $tmp_val . htmlspecialchars($val[$i], ENT_QUOTES) ."<br/>";
									$$key = $tmp_val;
									$_SESSION[$key][$i] = htmlspecialchars($val[$i], ENT_QUOTES);
								}
							}
						}
					}else if(is_string($val)){
						$$key = htmlspecialchars($val, ENT_QUOTES);
						$_SESSION[$key] = htmlspecialchars($val, ENT_QUOTES);
					}
					//配列用の変数初期化
					$tmp_val = "";
				}
			}
		}
		//初期化
		$data="";
		$err="";
		include (dirname(__FILE__) . "/../../$template_name");
	}
}
?>