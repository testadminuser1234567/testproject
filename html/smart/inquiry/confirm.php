<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="noindex,nofollow">
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形">
<meta name="description" content="お問合せ、東北、宮城で仕事探し、正社員。契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon-precomposed" href="https://www.hurex.jp/smart/webclip.png">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="format-detection" content="telephone=no">
<link rel="canonical" href="https://www.hurex.jp/entry/confirm.php">
<title>お問い合わせ｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<link href="../css/import.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="../js/remodal/remodal.css">
<link rel="stylesheet" href="../js/remodal/remodal-default-theme.css">
<script src="../js/remodal/remodal.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/menu.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/header.html'); ?>
<!-- main start -->
<div id="mainCommon">
<h2>お問い合わせ</h2>
</div>
<!-- main end --><!-- box start -->
<section class="box">
<h1>お問い合わせ</h1>

<div id="entryPage">
<p class="pctR">
<script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hurex.jp&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=ja"></script>
</p>
<?php require_once("../../entry/system/esc.php");?>
<div class="txtDetail">
<p>以下の内容で送信致します。</p>	
<h2 class="no">お名前<span class="point">※</span></h2>
<p><?php echo esc($onamae);?></p>
<h2>ふりがな</h2>
<p><?php echo esc($kana);?></p>
<h2>E-mail<span class="point">※</span></h2>
<p><?php echo esc($mail);?></p>
<h2>電話番号<span class="point">※</span></h2>
<p><?php echo esc($tel);?></p>
<h2>お問合せの種類</h2>
<p><?php echo esc($kubun);?></p>
<h2>弊社のサービスをどこでお知りになりましたか？ </h2>
<p><?php echo esc($how);?></p>
<h2>送信内容<span class="point">※</span></h2>
<p><?php echo nl2br(esc($naiyo));?></p>
<form id="entryForm" action="mail.php" method="post">
<div style="display:block;height:20px;">
<form id="entryForm" action="mail.php" method="post">
<input type="submit" name="ret" value=" 戻る " style="float:left;margin-left:10px;">
<input type="hidden" name="mode" value="return">
<input type="hidden" name="onamae" value="<?php echo esc($onamae);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="tel" value="<?php echo esc($tel);?>">
<input type="hidden" name="kubun" value="<?php echo esc($kubun);?>">
<input type="hidden" name="mail" value="<?php echo esc($mail);?>">
<input type="hidden" name="how" value="<?php echo esc($how);?>">
<input type="hidden" name="naiyo" value="<?php echo esc($naiyo);?>">
</form>
<form id="entryForm" action="mail.php" method="post">
<input type="submit" name="submit" value="送信する" style="float:left;margin-left:10px">
<input type="hidden" name="mode" value="send">
<input type="hidden" name="onamae" value="<?php echo esc($onamae);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="tel" value="<?php echo esc($tel);?>">
<input type="hidden" name="kubun" value="<?php echo esc($kubun);?>">
<input type="hidden" name="mail" value="<?php echo esc($mail);?>">
<input type="hidden" name="how" value="<?php echo esc($how);?>">
<input type="hidden" name="naiyo" value="<?php echo esc($naiyo);?>">
</form>
</div>
</div>
</div>
</section>
<!-- box end -->
<div id="co">
<?php $Path = "../"; include(dirname(__FILE__).'/../include/menuBox.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/bnrBox.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footLink.html'); ?>
</div>
<!-- co end -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footer.html'); ?>
<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>
