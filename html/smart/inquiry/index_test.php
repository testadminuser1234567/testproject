<?php
$domain = $_SERVER['HTTP_HOST'];
if ($domain == "hurex.jp" && empty($_SERVER['HTTPS'])) {
    header("Location: https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
    exit;
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="index,follow">
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形">
<meta name="description" content="お問合せ、東北、宮城で仕事探し、正社員。契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon-precomposed" href="https://www.hurex.jp/smart/webclip.png">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="format-detection" content="telephone=no">
<link rel="canonical" href="https://www.hurex.jp/inquiry/index.html">
<title>お問い合わせ｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<link href="../css/import.css" rel="stylesheet" type="text/css">
<link href="form.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="../js/remodal/remodal.css">
<link rel="stylesheet" href="../js/remodal/remodal-default-theme.css">
<script src="../js/remodal/remodal.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="./form_validation.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/menuSSL.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/headerSSL.html'); ?>
<!-- main start -->
<div id="mainCommon">
<h2>お問い合わせ</h2>
</div>
<!-- main end --><!-- box start -->
<section class="box">
<h1>お問い合わせ</h1>

<div id="entryPage">

<p class="pctR"><script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hurex.jp&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=ja"></script></p>
<p>
当サイトは、お客様のプライバシー保護のため、個人情報入力ページにおいてSSL暗号化通信を採用しています</p>
<?php require_once("../../entry/system/esc.php");?>
<?php echo pf($error);?>
<form action="mail.php" method="post">
<input type="hidden" name="mode" value="confirm">	
<h2 class="no">お名前<span class="point">※</span></h2>
<p><input class="hissu required" name="onamae" type="text" id="onamae" value="<?php echo esc($onamae);?>" style="width:94%;" /></p>
<h2>ふりがな</h2>
<p><input class="normalTxt" name="kana" type="text" id="kana" value="<?php echo esc($kana);?>" style="width:94%;" /></p>
<h2>E-mail<span class="point">※</span></h2>
<p><input class="hissu required mail" name="mail" type="text" id="mail" value="<?php echo esc($mail);?>" style="width:94%;" /></p>
<h2>電話番号<span class="point">※</span></h2>
<p><input class="normalTxt" name="tel" type="text" id="tel" value="<?php echo esc($tel);?>" style="width:94%;" /></p>
<h2>お問合せの種類<span class="point">※</span></h2>
<p><select class="normalTxt hissu required" name="kubun" id="kubun" style="width:94%;">
<option value="">お問合せの種類をお選び下さい</option>
<option value="登録について"  <?php echo esc($kubun)=="登録について" ? "selected" : "";?> >1.登録について</option>
<option value="求人について" <?php echo esc($kubun)=="求人について" ? "selected" : "";?>>2.求人について</option>
<option value="個人情報の取り扱いについて" <?php echo esc($kubun)=="個人情報の取り扱いについて" ? "selected" : "";?>>3.個人情報の取り扱いについて</option>
<option value="採用情報について" <?php echo esc($kubun)=="採用情報について" ? "selected" : "";?>>4.採用情報について</option>
<option value="サービスについて" <?php echo esc($kubun)=="サービスについて" ? "selected" : "";?>>5.サービスについて</option>
<option value="いつかは転職についてご相談" <?php echo esc($kubun)=="いつかは転職についてご相談" ? "selected" : "";?>>6.いつかは転職についてご相談</option>
<option value="その他お問い合わせ" <?php echo esc($kubun)=="その他お問い合わせ" ? "selected" : "";?>>7.その他お問い合わせ</option>
</select></p>
<h2>弊社のサービスをどこでお知りになりましたか？ </h2>
<p><select class="normalTxt" name="how" id="how" style="width:94%;">
<option value="">お選び下さい</option>
<option value="弊社ホームページ" <?php echo esc($how)=="弊社ホームページ" ? "selected" : "";?>>1.弊社ホームページ</option>
<option value="求人サイト" <?php echo esc($how)=="求人サイト" ? "selected" : "";?>>2.求人サイト</option>
<option value="求人誌" <?php echo esc($how)=="求人誌" ? "selected" : "";?>>3.求人誌</option>
<option value="知人の紹介" <?php echo esc($how)=="知人の紹介" ? "selected" : "";?>>4.知人の紹介</option>
<option value="CM" <?php echo esc($how)=="CM" ? "selected" : "";?>>5.CM</option>
<option value="雑誌や新聞広告" <?php echo esc($how)=="雑誌や新聞広告" ? "selected" : "";?>>6.雑誌や新聞広告</option>
<option value="弊社の取り組みが紹介された新聞記事" <?php echo esc($how)=="弊社の取り組みが紹介された新聞記事" ? "selected" : "";?>>7.弊社の取り組みが紹介された新聞記事</option>
</select></p>
<h2>送信内容<span class="point">※</span></h2>
<p><textarea class="hissu required" name="naiyo" cols="40" rows="5" id="naiyo" style="width:94%;"><?php echo esc($naiyo);?></textarea></p>
<p class="aCenter"><input class="submit button" id="contact_submit" type="submit" name="Submit" value="上記内容の確認画面へ" /></p>
</form>

</div>
</section>
<!-- box end -->
<div id="co">
<?php $Path = "../"; include(dirname(__FILE__).'/../include/menuBoxSSL.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/bnrBoxSSL.html'); ?>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footLinkSSL.html'); ?>
</div>
<!-- co end -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footerSSL.html'); ?>
<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>
