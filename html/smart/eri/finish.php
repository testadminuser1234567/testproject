<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="index,follow">
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形">
<meta name="description" content="東北地方の求人多数！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon-precomposed" href="https://www.hurex.jp/smart/webclip.png">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="format-detection" content="telephone=no">
<link rel="canonical" href="https://www.hurex.jp/entry/finish.php">
<title>ERICSSON｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<link href="../css/import.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="../js/remodal/remodal.css">
<link rel="stylesheet" href="../js/remodal/remodal-default-theme.css">
<script src="../js/remodal/remodal.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<!-- Facebook Conversion Code for 転職支援申込み -->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []); if (!_fbq.loaded) { var fbds = document.createElement('script'); fbds.async = true; fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6035195483782', {'value':'0.01','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none"
src="https://www.facebook.com/tr?ev=6035195483782&amp;cd[value]=0.01&amp;cd[
currency]=USD&amp;noscript=1"
/></noscript>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Facebook Conversion Code for 登録完了 -->
<script>(function() {
var _fbq = window._fbq || (window._fbq = []);
if (!_fbq.loaded) {
var fbds = document.createElement('script');
fbds.async = true;
fbds.src = '//connect.facebook.net/en_US/fbds.js';
var s = document.getElementsByTagName('script')[0];
s.parentNode.insertBefore(fbds, s);
_fbq.loaded = true;
}
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6027957872489', {'value':'0.00','currency':'JPY'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6027957872489&amp;cd[value]=0.00&amp;cd[currency]=JPY&amp;noscript=1" /></noscript>
</head>
<body>
<?php
$r = htmlspecialchars($_GET["rid"], ENT_QUOTES, 'UTF-8');
?>
<IMG SRC="https://www.rentracks.jp/secure/e.gifx?sid=2057&pid=3142&price=1&quantity=1&reward=-1&cinfo=<?php echo htmlspecialchars($r, ENT_QUOTES, 'UTF-8');?>" width="1" height="1">
<!-- Google Tag Manager -->
<script>
	var date = new Date();
	var trans_id = parseInt((new Date)/1000);
dataLayer = [{
    'transactionId': trans_id,
    'transactionAffiliation': 'HUREX_WEB',
    'transactionTotal': 20000 ,
    'transactionTax': 0,
    'transactionShipping': 0,
    'transactionProducts': [{
        'sku': 'es0001',
        'name': 'エントリー/スマホ',
        'category': 'WEB',
        'price': 20000,
        'quantity': 1    
    }]
}];
</script>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<!-- -->
<div id="readBox">
    <h1><img src="img/logo.png" width="73" height="65" alt="ERICSSON"/></h1>
    <h2>登録が完了致しました</h2>
    <p>担当より折り返しご連絡致しますのでお待ちください。</p>
</div>
<!-- -->
<p class="flow"><img src="../entry/img/step_03.png" width="100%" alt=""/></p>
<!-- SB追加 2014/08/21 -->
<!-- Google Code for &#30003;&#12375;&#36796;&#12415; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 966187790;
var google_conversion_language = "ja";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "dRsYCMnA-1UQjrbbzAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/966187790/?label=dRsYCMnA-1UQjrbbzAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Facebook Conversion Code for ヒューレックスのCV -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6016811407072', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6016811407072&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
<!-- Google Code for &#12467;&#12531;&#12496;&#12540;&#12472;&#12519;&#12531;&#12375;&#12383;&#12518;&#12540;&#12470;&#12540; -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 966187790;
var google_conversion_label = "e0D_COu--1UQjrbbzAM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/966187790/?value=1.00&amp;label=e0D_COu--1UQjrbbzAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Yahoo Code for your Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var yahoo_conversion_id = 1000089031;
var yahoo_conversion_label = "-XxECM3G-1UQ9-vU0AM";
var yahoo_conversion_value = 20000;
/* ]]> */
</script>
<script type="text/javascript" src="https://s.yimg.jp/images/listing/tool/cv/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="https://b91.yahoo.co.jp/pagead/conversion/1000089031/?value=20000&amp;label=-XxECM3G-1UQ9-vU0AM&amp;guid=ON&amp;script=0&amp;disvt=true"/>
</div>
</noscript>
<!-- Begin INDEED conversion code -->
<script type="text/javascript">
/* <![CDATA[ */
var indeed_conversion_id = '5775465888875426';
var indeed_conversion_label = '';
/* ]]> */
</script>
<script type="text/javascript" src="//conv.indeed.com/pagead/conversion.js">
</script>
<noscript>
<img height=1 width=1 border=0 src="//conv.indeed.com/pagead/conv/5775465888875426/?script=0">
</noscript>
<!-- End INDEED conversion code -->

<!-- 20141203 追加 -->
<!-- Google Code for &#30003;&#36796; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963710423;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "NN7OCO_57VcQ15vEywM";
var google_conversion_value = 1.00;
var google_conversion_currency = "JPY";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/963710423/?value=1.00&amp;currency_code=JPY&amp;label=NN7OCO_57VcQ15vEywM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- リマーケティング タグの Google コード -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963710423;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963710423/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- リマーケティングタグ Yahoo -->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=1Wjm96V";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=1Wjm96V" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
<p id="copyForm">&copy; 2017 ERICSSON</p>
</body>
</html>
