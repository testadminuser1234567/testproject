<?php
$domain = $_SERVER['HTTP_HOST'];
if ($domain == "hurex.jp" && empty($_SERVER['HTTPS'])) {
    header("Location: https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
    exit;
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="index,follow">
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形">
<meta name="description" content="東北地方の求人多数！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon-precomposed" href="https://www.hurex.jp/smart/webclip.png">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="format-detection" content="telephone=no">
<link rel="canonical" href="https://www.hurex.jp/entry/form.html">
<title>ERICSSON｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<link href="../css/import.css" rel="stylesheet" type="text/css">
<link href="form.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="./form_validation.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<?php
//HRBCマスターと連動
$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);
?>
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<!-- -->
<div id="readBox">
    <h1><img src="img/logo.png" width="73" height="65" alt="ERICSSON"/></h1>
    <h2>エリクソン仙台 応募専用ページ</h2>
    <p>
	この度は、「エリクソン仙台」立ち上げに伴うスターティングメンバーの募集にご関心をお持ちいただき、誠にありがとうございます。<br />
	応募にあたり、以下の必要事項を入力の上、選考にお申し込みください。<br />
	お申込後、2営業日以内に採用事務局よりご連絡させていただきます。<br />
	<br />
	皆様のご応募を心よりお待ちしております。
	</p>
</div>
<!-- -->
<p class="flow"><img src="../entry/img/step_01.png" width="100%" alt=""/></p>
<!-- box start -->
<section class="box normal">
<div id="entryPage" class="new">
<p>下記フォームにご入力の上、[入力内容を確認する]ボタンをクリックしてください。</p>
<div class="ssl clearfix">
<div class="seal">
<script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hurex.jp&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=ja"></script></div>
<p>当サイトは、お客様のプライバシー保護のため、個人情報入力ページにおいてSSL暗号化通信を採用しています</p>
</div>
<?php require_once("../../entry/system/esc.php");?>
<form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="mode" value="confirm">
<div class="error"><?php echo pf($error);?></div>
<h2 class="no"><span class="<?php if($shimei):?>ok<?php else:?>hissu<?php endif;?> shimei">氏名</span></h2>
<p><input name="shimei" type="text" class="hissu required txtBox" id="shimei" placeholder="入力例:鈴木 太郎" style="width:100%;" value="<?php echo esc($shimei);?>" maxlength="25" /></p>

<h2 class="no"><span class="<?php if($kana):?>ok<?php else:?>hissu<?php endif;?> kana">ふりがな</span></h2>
<p><input type="text" name="kana" id="kana" class="hissu required txtBox" placeholder="入力例:すずき たろう" value="<?php echo esc($kana);?>" maxlength="25" style="width:100%;" /></p>
<h2><span class="<?php if($year && $month && $day):?>ok<?php else:?>hissu<?php endif;?> birth">生年月日</span></h2>
<p><select name="year" class="hissu required" id="year">
<option value="" label="年">年</option>
<?php 
$y= date('Y');
$y_from = $y-18;
$y_to = $y-63;
?>
<?php for($y=$y_from;$y>=$y_to;$y--):?>
<option label="<?php echo esc($y);?>年" value="<?php echo esc($y);?>" <?php if($year==$y):?>selected<?php endif;?>><?php echo esc($y);?>年</option>
<?php endfor;?>
</select>
<select name="month" class="hissu required" id="month">
<option value="" label="月">月</option>
<?php for($m=1;$m<=12;$m++):?>
<option label="<?php echo esc($m);?>月" value="<?php echo esc($m);?>" <?php if($month==$m):?>selected<?php endif;?>><?php echo esc($m);?>月</option>
<?php endfor;?>
</select>
<select name="day" class="hissu required" id="day">
<option value="" label="日">日</option>
<?php for($d=1;$d<=31;$d++):?>
<option label="<?php echo esc($d);?>日" value="<?php echo esc($d);?>" <?php if($day==$d):?>selected<?php endif;?>><?php echo esc($d);?>日</option>
<?php endfor;?>
</select></p>
<h2><span class="<?php if($sex):?>ok<?php else:?>hissu<?php endif;?> sex">性別</span></h2>
<p>
<span class="hissu">
<?php foreach($tmpgender['Item'] as $k=>$v):?>
<?php foreach($v['Item'] as $k2=>$v2):?>
<label for="sex0<?php echo esc($k2);?>" class="radiolabel sexlabel">
<input type="radio" name="sex" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" class="radio sexradio<?php if(count($v['Item'])-1 == $k2):?> required<?php endif;?>"  <?php if($sex==$v2['Option.P_Name']):?>checked<?php endif;?> id="sex0<?php echo esc($k2);?>"  />
<?php print_r($v2['Option.P_Name']);?></label>

<?php endforeach;?>
<?php endforeach;?>
</span>
<p id="sex_err"></p>
<h2><span class="<?php if($pref):?>ok<?php else:?>hissu<?php endif;?> pref">住所</span></h2>
<p><input type="text" name="pref" id="pref" class="hissu required txtBox" placeholder="入力例:宮城県仙台市青葉区中央1-1 〇〇マンションA101" value="<?php echo esc($pref);?>" maxlength="25" style="width:100%;" /></p>
<h2><span class="<?php if($tel1):?>ok<?php else:?>hissu<?php endif;?> tel1">電話</span></h2>
<p><input name="tel1" type="number" class="hissu txtBox" id="tel1" placeholder="入力例:08098765432" style="width:100%;" value="<?php echo esc($tel1);?>" /></p>
<h2><span class="<?php if($mail1):?>ok<?php else:?>hissu<?php endif;?> mail1">メール</span></h2>
<p><input type="email" name="mail1" class="hissu txtBox" id="mail1" placeholder="入力例:test@gmail.com" style="width:100%;"  value="<?php echo esc($mail1);?>" />
<span id="mailerr"></span>

※@hurex.co.jpからのメールを受信可能にドメイン設定をお願いします。<br>
※携帯電話など、常時確認できるメールアドレスのご記載をお願いします。
</p>
<h2><span class="<?php if($school_div_id):?>ok<?php else:?>hissu<?php endif;?> school_div_id">最終学歴</span></h2>
<p>
<span class="hissu">
<?php foreach($tmpbackground['Item'] as $k=>$v):?>
<?php foreach($v['Item'] as $k2=>$v2):?>
<label for="gakureki0<?php echo esc($k2);?>" class="radiolabel gakurekilabel">
<input type="radio" class="radio gakurekiradio<?php if($k2-1==count($tmpbackground['Item'])):?> required<?php endif;?>" name="school_div_id" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if($school_div_id==$v2['Option.P_Name']):?>checked<?php endif;?> id="gakureki0<?php echo esc($k2);?>">
<?php echo esc($v2['Option.P_Name']);?>
</label>
<?php endforeach;?>
<?php endforeach;?>
</span>
<p id="school_div_id_err"></p>
<h2><span class="<?php if($company_number):?>ok<?php else:?>hissu<?php endif;?> company_number">経験社数</span></h2>
<p><span class="hissu">
<label for="keiken01" class="radiolabel companylabel">
<input type="radio" class="radio compradio" name="company_number" value="1" <?php echo esc($company_number)=="1" ? "checked" : "";?> id="keiken01" />
1社</label>
<label for="keiken02" class="radiolabel companylabel">
<input type="radio" class="radio compradio" name="company_number" value="2" <?php echo esc($company_number)=="2" ? "checked" : "";?> id="keiken02" />
2社</label>
<label for="keiken03" class="radiolabel companylabel">
<input type="radio" class="radio compradio" name="company_number" value="3" <?php echo esc($company_number)=="3" ? "checked" : "";?> id="keiken03" />
3社</label>
<label for="keiken04" class="radiolabel companylabel">
<input type="radio" class="radio compradio" name="company_number" value="4" <?php echo esc($company_number)=="4" ? "checked" : "";?> id="keiken04" />
4社</label>
<label for="keiken05" class="radiolabel companylabel">
<input type="radio" class="radio compradio required" name="company_number" value="5" <?php echo esc($company_number)=="5" ? "checked" : "";?> id="keiken05" />
5社以上</label>
</span>
<p id="company_number_err"></p>
<h2><span class="<?php if($qualification):?>ok<?php else:?>nini<?php endif;?> qualification">免許･資格</span></h2>
<p><textarea name="qualification" cols="60" rows="4" id="qualification"><?php echo $qualification;?></textarea></p>



<h2>職務経歴(直近3社)</h2>
<!-- keirekiBox start -->
<div class="keirekiBox">
    <h3>直近1社目</h3>
    <h4>会社名</h4>
    <p>
        <input name="company1" type="text" class="txtBox" id="company1" value="<?php echo esc($company1);?>" maxlength="25" style="width:100%;" />
    </p>
    <h4>在職期間</h4>
    <p><select name="year1_from" id="year1_from">
                <option value="" label="年">年</option>
                <?php 
$y= date('Y');
$y_from = $y;
$y_to = 1980;
?>
                <?php for($y=$y_from;$y>=$y_to;$y--):?>
                <option label="<?php echo esc($y);?>年" value="<?php echo esc($y);?>" <?php if(esc($year1_from)==esc($y)):?>selected<?php endif;?>><?php echo esc($y);?>年</option>
                <?php endfor;?>
            </select>
                <select name="month1_from" id="month1_from">
                    <option value="" label="月">月</option>
                    <?php for($m=1;$m<=12;$m++):?>
                    <option label="<?php echo esc($m);?>月" value="<?php echo esc($m);?>" <?php if(esc($month1_from)==esc($m)):?>selected<?php endif;?>><?php echo esc($m);?>月</option>
                    <?php endfor;?>
                </select>
                ～<br>
                <select name="year1_to" id="year1_to">
                    <option value="" label="年">年</option>

                    <?php for($y=$y_from;$y>=$y_to;$y--):?>
                    <option label="<?php echo esc($y);?>年" value="<?php echo esc($y);?>" <?php if(esc($year1_to)==esc($y)):?>selected<?php endif;?>><?php echo esc($y);?>年</option>
                    <?php endfor;?>
                </select>
                <select name="month1_to" id="month1_to">
                    <option value="" label="月">月</option>
                    <?php for($m=1;$m<=12;$m++):?>
                    <option label="<?php echo esc($m);?>月" value="<?php echo esc($m);?>" <?php if(esc($month1_to)==esc($m)):?>selected<?php endif;?>><?php echo esc($m);?>月</option>
                    <?php endfor;?>
                </select></p>
    <h4>職務内容</h4>
    <p><textarea name="naiyo1" cols="15" rows="4" id="naiyo1"><?php echo $naiyo1;?></textarea></p>
</div>
<!-- keirekiBox end -->
<!-- keirekiBox start -->
<div class="keirekiBox">
    <h3>直近2社目</h3>
    <h4>会社名</h4>
    <p>
        <input name="company2" type="text" class="txtBox" id="company2" value="<?php echo esc($company2);?>" maxlength="25" style="width:100%;" />
    </p>
    <h4>在職期間</h4>
    <p>
        <select name="year2_from" id="year2_from">
<option value="" label="年">年</option>
<?php for($y=$y_from;$y>=$y_to;$y--):?>
<option label="<?php echo esc($y);?>年" value="<?php echo esc($y);?>" <?php if(esc($year2_from)==esc($y)):?>selected<?php endif;?>><?php echo esc($y);?>年</option>
<?php endfor;?>
</select>
                <select name="month2_from" id="month2_from">
<option value="" label="月">月</option>
<?php for($m=1;$m<=12;$m++):?>
<option label="<?php echo esc($m);?>月" value="<?php echo esc($m);?>" <?php if(esc($month2_from)==esc($m)):?>selected<?php endif;?>><?php echo esc($m);?>月</option>
<?php endfor;?>
</select>
～<br>
<select name="year2_to" id="year2_to">
<option value="" label="年">年</option>
<?php for($y=$y_from;$y>=$y_to;$y--):?>
<option label="<?php echo esc($y);?>年" value="<?php echo esc($y);?>" <?php if(esc($year2_to)==esc($y)):?>selected<?php endif;?>><?php echo esc($y);?>年</option>
<?php endfor;?>
</select>
<select name="month2_to" id="month2_to">
<option value="" label="月">月</option>
<?php for($m=1;$m<=12;$m++):?>
<option label="<?php echo esc($m);?>月" value="<?php echo esc($m);?>" <?php if(esc($month2_to)==esc($m)):?>selected<?php endif;?>><?php echo esc($m);?>月</option>
<?php endfor;?>
</select>
    </p>
    <h4>職務内容</h4>
    <p>
        <textarea name="naiyo2" cols="15" rows="4" id="naiyo2"><?php echo $naiyo2;?></textarea>
    </p>
</div>
<!-- keirekiBox end -->
<!-- keirekiBox start -->
<div class="keirekiBox">
    <h3>直近3社目</h3>
    <h4>会社名</h4>
    <p>
        <input name="company3" type="text" class="txtBox" id="company3" value="<?php echo esc($company3);?>" maxlength="25" style="width:100%;" />
    </p>
    <h4>在職期間</h4>
    <p>
        <select name="year3_from" id="year3_from">
                <option value="" label="年">年</option>
                <?php for($y=$y_from;$y>=$y_to;$y--):?>
                <option label="<?php echo esc($y);?>年" value="<?php echo esc($y);?>" <?php if(esc($year3_from)==esc($y)):?>selected<?php endif;?>><?php echo esc($y);?>年</option>
                <?php endfor;?>
            </select>
                <select name="month3_from" id="month3_from">
                    <option value="" label="月">月</option>
                    <?php for($m=1;$m<=12;$m++):?>
                    <option label="<?php echo esc($m);?>月" value="<?php echo esc($m);?>" <?php if(esc($month3_from)==esc($m)):?>selected<?php endif;?>><?php echo esc($m);?>月</option>
                    <?php endfor;?>
                </select>
                ～<br>
                <select name="year3_to" id="year3_to">
                    <option value="" label="年">年</option>
                    <?php for($y=$y_from;$y>=$y_to;$y--):?>
                    <option label="<?php echo esc($y);?>年" value="<?php echo esc($y);?>" <?php if(esc($year3_to)==esc($y)):?>selected<?php endif;?>><?php echo esc($y);?>年</option>
                    <?php endfor;?>
                </select>
                <select name="month3_to" id="month3_to">
                    <option value="" label="月">月</option>
                    <?php for($m=1;$m<=12;$m++):?>
                    <option label="<?php echo esc($m);?>月" value="<?php echo esc($m);?>" <?php if(esc($month3_to)==esc($m)):?>selected<?php endif;?>><?php echo esc($m);?>月</option>
                    <?php endfor;?>
                </select>
    </p>
    <h4>職務内容</h4>
    <p>
        <textarea name="naiyo3" cols="15" rows="4" id="naiyo3"><?php echo $naiyo3;?></textarea>
    </p>
</div>
<!-- keirekiBox end -->
<!-- privacyBox start -->
<div id="privacyBox">
<div id="privacyBoxSub">
<p><a href="pp.pdf" target="_blank">個人情報取り扱いに関する承諾</a>をご覧ください</p>
<p class="checkboxrequired">
<label for="agreeChk" class="CheckBoxLabelClass privacy<?php if($agree):?> checked<?php endif;?>">
<input type="hidden" name="agree" value="0" />
<input type="checkbox" class="rc checkbox " name="agree" value="1" id="agreeChk" <?php if(esc($agree)=="1"):?>checked<?php endif;?>>
個人情報取り扱いに関する承諾に同意する</label>
</p>
</div>
</div>
<!-- privacyBox end -->
<!-- start -->
<div class="btnSend check">
<input name="btnKakunin" type="image" id="btnKakunin" class="image wide" onClick="return check(1);" value="送信確認画面へ" src="../entry/img/btn_check_<?php if($agree==1):?>on<?php else:?>off<?php endif;?>.png" alt="入力内容の確認ページへ">
</div>
<!-- end -->
<?php 
if(!empty($_GET['id'])){
	$job_id="";
	$job_id = htmlspecialchars($_GET['id'], ENT_QUOTES, 'UTF-8');
	if(!is_numeric($job_id)){
		$job_id = "";
	}
}else if(empty($job_id)){
	$job_id = "";
}
?>
<input type="hidden" name="job_id" value="<?php echo esc($job_id);?>" />
</form>

<p class="ishi-form-bottom">
<span>
※この求人は、採用事務局をヒューレックス株式会社に委託しています。<br>
ご不明な点がございましたら、下記までお問い合わせください。
</span>
<span>
<b>【 採用事務局 】</b><br>
ヒューレックス株式会社<br>「エリクソン仙台　採用事務局」<br>
Tel：022-723-1770 <br> Email：<a href="mailto:info@hurex.co.jp">&#105;&#110;fo&#64;&#104;ure&#120;&#46;&#99;&#111;&#46;j&#112;</a>
</span>
</p>


</div>
</div>
</section>
<!-- box end -->
<script>
$(function(){
    var checkbox = $('input[type="checkbox"]');
    var radio = $('input[type="radio"]');
 
    boxChecked();
    checkbox.on('change', function(){
        boxChecked();
    });
 
    radioChecked();
    radio.on('click', function(){
        radioChecked();
    });
 
    function boxChecked() {
        checkbox.each(function() {
            if($(this).prop('checked')) {
                $(this).parent().addClass('checked');
            } else {
                $(this).parent().removeClass('checked');
            }
        });
    }
    function radioChecked() {
        radio.each(function() {
            if($(this).prop('checked')) {
                $(this).parent().addClass('checked');
            } else {
                $(this).parent().removeClass('checked');
            }
        });
    }
});
</script>
<!-- リマーケティング タグの Google コード -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963710423;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963710423/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- リマーケティングタグ Yahoo -->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=1Wjm96V";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
<iframe src="//b.yjtag.jp/iframe?c=1Wjm96V" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
<p id="copyForm">&copy; 2017 ERICSSON</p>
</body>
</html>
