<?php
$domain = $_SERVER['HTTP_HOST'];
if ($domain == "hurex.jp" && empty($_SERVER['HTTPS'])) {
    header("Location: https://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}");
    exit;
}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="robots" content="index,follow">
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形">
<meta name="description" content="東北地方の求人多数！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<link rel="apple-touch-icon-precomposed" href="https://www.hurex.jp/smart/webclip.png">
<link rel="shortcut icon" href="/favicon.ico">
<meta name="format-detection" content="telephone=no">
<link rel="canonical" href="https://www.hurex.jp/entry/form.html">
<title>ERICSSON｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<link href="../css/import.css" rel="stylesheet" type="text/css">
<link href="form.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="./form_validation.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<?php
//HRBCマスターと連動
$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);
?>
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<!-- -->
<div id="readBox">
    <h1><img src="img/logo.png" width="73" height="65" alt="ERICSSON"/></h1>
    <h2>入力内容確認</h2>
</div>
<!-- -->
<p class="flow"><img src="../entry/img/step_02.png" width="100%" alt=""/></p>
<!-- box start -->
<section class="box normal">
    <div id="entryPage" class="new">
        <p>以下の内容で送信致します。</p>
    <?php require($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/site/setmaster.php');?>
    <?php require_once("system/esc.php");?>
        <h2 class="no"><span class="<?php if($shimei):?>ok<?php else:?>hissu<?php endif;?> shimei">氏名</span></h2>
        <p><?php echo esc($shimei);?></p>
        <h2 class="no"><span class="<?php if($kana):?>ok<?php else:?>hissu<?php endif;?> kana">ふりがな</span></h2>
        <p><?php echo esc($kana);?></p>
        <h2><span class="<?php if($year && $month && $day):?>ok<?php else:?>hissu<?php endif;?> birth">生年月日</span></h2>
        <p><?php echo esc($year);?>年<?php echo esc($month);?>月<?php echo esc($day);?>日</p>
        <h2><span class="<?php if($sex):?>ok<?php else:?>hissu<?php endif;?> sex">性別</span></h2>
        <p> <?php echo esc($sex);?> </p>
        <h2><span class="<?php if($pref):?>ok<?php else:?>hissu<?php endif;?> pref">現住所</span></h2>
        <p> <?php echo esc($pref);?> </p>
        <h2><span class="<?php if($tel1):?>ok<?php else:?>hissu<?php endif;?> tel1">電話</span></h2>
        <p><?php echo esc($tel1);?></p>
        <h2><span class="<?php if($mail1):?>ok<?php else:?>hissu<?php endif;?> mail1">メール</span></h2>
        <p><?php echo esc($mail1);?> </p>
        <h2><span class="<?php if($school_div_id):?>ok<?php else:?>hissu<?php endif;?> school_div_id">最終学歴</span></h2>
        <p> <?php echo esc($school_div_id);?> </p>
        <h2><span class="<?php if($company_number):?>ok<?php else:?>hissu<?php endif;?> company_number">経験社数</span></h2>
        <p>
            <?php if($company_number==5):?>
            5社以上
            <?php else:?>
            <?php echo esc($company_number);?>社
            <?php endif;?>
        </p>
        <h2><span class="<?php if($qualification):?>ok<?php else:?>nini<?php endif;?> qualification">免許･資格</span></h2>
        <p><?php echo nl2br(esc($qualification));?></p>


<?php if(!empty(company1) || !empty($year1_from) || !empty($month1_from) || !empty($year1_to) || !empty($month1_to) || !empty($naiyo1)):?><h2>職務経歴(直近3社)</h2>
<!-- keirekiBox start -->
<div class="keirekiBox">
    <h3>直近1社目</h3>
    <h4>会社名</h4>
    <p>
        <?php echo esc($company1);?>
    </p>
    <h4>在職期間</h4>
    <p><?php echo esc($year1_from);?>年<?php echo esc($month1_from);?>月
                ～
                <?php echo esc($year1_to);?>年<?php echo esc($month1_to);?>月</p>
    <h4>職務内容</h4>
    <p><?php echo nl2br(esc($naiyo1));?></p>
</div>
<!-- keirekiBox end -->
<?php endif;?>
<?php if(!empty(company2) || !empty($year2_from) || !empty($month2_from) || !empty($year2_to) || !empty($month2_to) || !empty($naiyo2)):?>
<!-- keirekiBox start -->
<div class="keirekiBox">
    <h3>直近2社目</h3>
    <h4>会社名</h4>
    <p>
        <?php echo esc($company2);?>
    </p>
    <h4>在職期間</h4>
    <p>
        <?php echo esc($year2_from);?>年<?php echo esc($month2_from);?>月
                ～
                <?php echo esc($year2_to);?>年<?php echo esc($month2_to);?>月
    </p>
    <h4>職務内容</h4>
    <p>
        <?php echo nl2br(esc($naiyo2));?>
    </p>
</div>
<!-- keirekiBox end -->
<?php endif;?>
<?php if(!empty(company3) || !empty($year3_from) || !empty($month3_from) || !empty($year3_to) || !empty($month3_to) || !empty($naiyo3)):?>
<!-- keirekiBox start -->
<div class="keirekiBox">
    <h3>直近3社目</h3>
    <h4>会社名</h4>
    <p>
        <?php echo esc($company3);?>
    </p>
    <h4>在職期間</h4>
    <p>
        <?php echo esc($year3_from);?>年<?php echo esc($month3_from);?>月
                ～
                <?php echo esc($year3_to);?>年<?php echo esc($month3_to);?>月
    </p>
    <h4>職務内容</h4>
    <p>
        <?php echo nl2br(esc($naiyo3));?>
    </p>
</div>
<!-- keirekiBox end -->
<?php endif;?>

        <!-- start -->
        <div class="btnSend aCenter">
            <form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data">
                <input name="btnSend" type="image" class="image" id="btnSend" style=" width: 90%; height:auto; " value="送信する" src="../entry/img/btn_send.png" onclick="window.onbeforeunload=null;" alt="上記の内容で送信">
                <input type="hidden" name="mode" value="send">
            <input type="hidden" name="shimei" value="<?php echo esc($shimei);?>" />
            <input type="hidden" name="kana" value="<?php echo esc($kana);?>" />
            <input type="hidden" name="year" value="<?php echo esc($year);?>" />
            <input type="hidden" name="month" value="<?php echo esc($month);?>" />
            <input type="hidden" name="day" value="<?php echo esc($day);?>" />
            <input type="hidden" name="sex" value="<?php echo esc($sex);?>" />
            <input type="hidden" name="zip" value="<?php echo esc($zip);?>" />
            <input type="hidden" name="pref" value="<?php echo esc($pref);?>" />
            <input type="hidden" name="tel1" value="<?php echo esc($tel1);?>" />
            <input type="hidden" name="mail1" value="<?php echo esc($mail1);?>" />
            <input type="hidden" name="file1" value="<?php echo esc($file1);?>" />
            <input type="hidden" name="file2" value="<?php echo esc($file2);?>" />
            <input type="hidden" name="agree" value="<?php echo esc($agree);?>" />
            <input type="hidden" name="school_div_id" value="<?php echo esc($school_div_id);?>" />
            <input type="hidden" name="company_number" value="<?php echo esc($company_number);?>" />
            <input type="hidden" name="company1" value="<?php echo esc($company1);?>" />
            <input type="hidden" name="year1_from" value="<?php echo esc($year1_from);?>" />
            <input type="hidden" name="month1_from" value="<?php echo esc($month1_from);?>" />
            <input type="hidden" name="year1_to" value="<?php echo esc($year1_to);?>" />
            <input type="hidden" name="month1_to" value="<?php echo esc($month1_to);?>" />
            <input type="hidden" name="naiyo1" value="<?php echo esc($naiyo1);?>" />
            <input type="hidden" name="company2" value="<?php echo esc($company2);?>" />
            <input type="hidden" name="year2_from" value="<?php echo esc($year2_from);?>" />
            <input type="hidden" name="month2_from" value="<?php echo esc($month2_from);?>" />
            <input type="hidden" name="year2_to" value="<?php echo esc($year2_to);?>" />
            <input type="hidden" name="month2_to" value="<?php echo esc($month2_to);?>" />
            <input type="hidden" name="naiyo2" value="<?php echo esc($naiyo2);?>" />
            <input type="hidden" name="company3" value="<?php echo esc($company3);?>" />
            <input type="hidden" name="year3_from" value="<?php echo esc($year3_from);?>" />
            <input type="hidden" name="month3_from" value="<?php echo esc($month3_from);?>" />
            <input type="hidden" name="year3_to" value="<?php echo esc($year3_to);?>" />
            <input type="hidden" name="month3_to" value="<?php echo esc($month3_to);?>" />
            <input type="hidden" name="naiyo3" value="<?php echo esc($naiyo3);?>" />
            <input type="hidden" name="qualification" value="<?php echo esc($qualification);?>" />
            </form>
        </div>
        <br>
        <form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data" class="aCenter">
            <input type="hidden" name="mode" value="return">
            <input type="image" class="image" value="戻る" style=" width: 60%; height:auto; " src="../entry/img/btn_back.png" alt="戻る" onclick="window.onbeforeunload=null;">
             <input type="hidden" name="shimei" value="<?php echo esc($shimei);?>" />
            <input type="hidden" name="kana" value="<?php echo esc($kana);?>" />
            <input type="hidden" name="year" value="<?php echo esc($year);?>" />
            <input type="hidden" name="month" value="<?php echo esc($month);?>" />
            <input type="hidden" name="day" value="<?php echo esc($day);?>" />
            <input type="hidden" name="sex" value="<?php echo esc($sex);?>" />
            <input type="hidden" name="zip" value="<?php echo esc($zip);?>" />
            <input type="hidden" name="pref" value="<?php echo esc($pref);?>" />
            <input type="hidden" name="tel1" value="<?php echo esc($tel1);?>" />
            <input type="hidden" name="mail1" value="<?php echo esc($mail1);?>" />
            <input type="hidden" name="file1" value="<?php echo esc($file1);?>" />
            <input type="hidden" name="file2" value="<?php echo esc($file2);?>" />
            <input type="hidden" name="agree" value="<?php echo esc($agree);?>" />
            <input type="hidden" name="school_div_id" value="<?php echo esc($school_div_id);?>" />
            <input type="hidden" name="company_number" value="<?php echo esc($company_number);?>" />
            <input type="hidden" name="company1" value="<?php echo esc($company1);?>" />
            <input type="hidden" name="year1_from" value="<?php echo esc($year1_from);?>" />
            <input type="hidden" name="month1_from" value="<?php echo esc($month1_from);?>" />
            <input type="hidden" name="year1_to" value="<?php echo esc($year1_to);?>" />
            <input type="hidden" name="month1_to" value="<?php echo esc($month1_to);?>" />
            <input type="hidden" name="naiyo1" value="<?php echo esc($naiyo1);?>" />
            <input type="hidden" name="company2" value="<?php echo esc($company2);?>" />
            <input type="hidden" name="year2_from" value="<?php echo esc($year2_from);?>" />
            <input type="hidden" name="month2_from" value="<?php echo esc($month2_from);?>" />
            <input type="hidden" name="year2_to" value="<?php echo esc($year2_to);?>" />
            <input type="hidden" name="month2_to" value="<?php echo esc($month2_to);?>" />
            <input type="hidden" name="naiyo2" value="<?php echo esc($naiyo2);?>" />
            <input type="hidden" name="company3" value="<?php echo esc($company3);?>" />
            <input type="hidden" name="year3_from" value="<?php echo esc($year3_from);?>" />
            <input type="hidden" name="month3_from" value="<?php echo esc($month3_from);?>" />
            <input type="hidden" name="year3_to" value="<?php echo esc($year3_to);?>" />
            <input type="hidden" name="month3_to" value="<?php echo esc($month3_to);?>" />
            <input type="hidden" name="naiyo3" value="<?php echo esc($naiyo3);?>" />
            <input type="hidden" name="qualification" value="<?php echo esc($qualification);?>" />
        </form>
    </div>
    </div>
</section>
<!-- box end -->
<script>
$(function(){
    var checkbox = $('input[type="checkbox"]');
    var radio = $('input[type="radio"]');
 
    boxChecked();
    checkbox.on('change', function(){
        boxChecked();
    });
 
    radioChecked();
    radio.on('click', function(){
        radioChecked();
    });
 
    function boxChecked() {
        checkbox.each(function() {
            if($(this).prop('checked')) {
                $(this).parent().addClass('checked');
            } else {
                $(this).parent().removeClass('checked');
            }
        });
    }
    function radioChecked() {
        radio.each(function() {
            if($(this).prop('checked')) {
                $(this).parent().addClass('checked');
            } else {
                $(this).parent().removeClass('checked');
            }
        });
    }
});
</script>
<!-- リマーケティング タグの Google コード -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963710423;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963710423/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- リマーケティングタグ Yahoo -->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=1Wjm96V";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
<iframe src="//b.yjtag.jp/iframe?c=1Wjm96V" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
<p id="copyForm">&copy; 2017 ERICSSON</p>
</body>
</html>
