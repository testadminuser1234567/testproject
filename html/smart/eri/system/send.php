<?php
/*
/  送信処理
*/
//エラーチェック
$err = "";
$err = $validation->run();

//エラーがあったら初期画面へ
if(!empty($err)){
	$template->view(Index, $_SESSION, $err);
}else{
	//管理者宛
	include (dirname(__FILE__) . "/qdmail.php");

	//portarシステム用
	//include ($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/system_entry/smart/entry01.php');

	//正常
	if(empty($error)){

		//送信者（お客さん宛）
		$to = array($email , $unit_name );
//		$to = array($send_mail , $send_mail );
		$from = array($thanks_mail_from , $thanks_mail_str );

		$qdmail = new Qdmail();
		//$qdmail->charsetBody( 'utf-8' , 'base64' ) ;
		$qdmail->kana(true);
		$qdmail->mtaOption( "-f $thanks_mail_from" );
	
		$qdmail -> easyText(
			$to,
			$thanks_mail_subject ,
			$thanks_body,
			$from
		);

		//管理者宛
	//	$kanri_to[] = $order_mail_to;
	//	$kanri_from = array($mail , $unit_name );
		if(!empty($file1) || !empty($file2)){
			$attachfile = array();
			//$attachFile ="/usr/home/z113109/entry/smart/" . $file1;

			if(!empty($file1)) $attachFile[] = array($_SERVER["DOCUMENT_ROOT"] .  "/../entry/smart/" . $file1 , '履歴書' );
			if(!empty($file2)) $attachFile[] = array($_SERVER["DOCUMENT_ROOT"] .  "/../entry/smart/" . $file2 , '職務経歴書' );
			$order_mail_other['from'][] = array($send_mail, $send_mail);
			$qdmail -> easyText(
				$order_mail_to,
				$order_mail_subject ,
				$order_body,
				$order_mail_other,
				$attachFile
			);
		}else{
			$order_mail_other['from'][] = array($send_mail, $send_mail);
			$qdmail -> easyText(
				$order_mail_to,
				$order_mail_subject ,
				$order_body,
				$order_mail_other
			);
		}

		if($log=="on"){
			$today = date("Y/m/d G:i");
			$today = $today . "\r\n";	
			//ログの書き込み
			$pointer=fopen("system/log.txt", "a");
			flock($pointer, LOCK_EX);
			fputs($pointer, $today);
			flock($pointer, LOCK_UN);
			fclose($pointer);
		}

		// セッション変数を全て解除する
		$_SESSION = array();
		// 最終的に、セッションを破壊する
		session_destroy();
		header("HTTP/1.1 200 ok");
		$url = Finish . "?rid=" . htmlspecialchars($rId, ENT_QUOTES, 'UTF-8');
		header("Location: $url");
		exit();
	//エラー時
	}else{
		$_SESSION['file1']="";
		$_SESSION['file2']="";
//		$err = "データエラーが発生しました。<br />" . $err_message;
		$err = $err_message . "<br>";
		$template->view(Index, $_SESSION, $err);
	}
}
?>