<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="index,follow" />
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形" />
<meta name="description" content="東北地方の求人多数！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。" />
<meta http-equiv="imagetoolbar" content="no" />
<meta http-equiv="Content-Language" content="ja" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="JavaScript" />
<link rel="shortcut icon" href="/favicon.ico" />
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all" />
<link rel="canonical" href="https://www.hurex.jp/reentry/index.html">
<link href="./form.css" rel="stylesheet" type="text/css" />
<title>すでに登録されている方専用エントリー｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<meta name="copyright" content="" />
<script type="text/javascript" src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="../js/remodal/remodal.css">
<link rel="stylesheet" href="../js/remodal/remodal-default-theme.css">
<script src="../js/remodal/remodal.js"></script>
<script type="text/javascript" src="../js/rollover.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<!--[if lte IE 6]>   
<script type="text/javascript" src="../js/DD_belatedPNG_0.0.8a.js">   
</script>   
<script type="text/javascript">  /* EXAMPLE */  DD_belatedPNG.fix('img');</script>   
<![endif]-->
<script type="text/javascript" src="./form_validation.js"></script>
<script>
$(function(){
	$('#entryPage p.txtMoreBtn').click(function() {
		$(this).next().slideToggle();
	}).next().hide();
});
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body class="nofloating">
<IMG SRC="https://www.rentracks.jp/secure/e.gifx?sid=2057&pid=3142&price=1&quantity=1&reward=-1&cinfo=<?php echo date('YmdHis');?>" width="1" height="1">
<!-- Google Tag Manager -->
<script>
	var date = new Date();
	var trans_id = parseInt((new Date)/1000);
dataLayer = [{
    'transactionId': trans_id,
    'transactionAffiliation': 'HUREX_WEB',
    'transactionTotal': 20000 ,
    'transactionTax': 0,
    'transactionShipping': 0,
    'transactionProducts': [{
        'sku': 'es0001',
        'name': 'エントリー',
        'category': 'WEB',
        'price': 20000,
        'quantity': 1    
    }]
}];
</script>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<!-- header start -->
<div id="header" class="clearfix">
<h1>すでに登録されている方専用エントリー｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</h1>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/header.html'); ?>
</div>
<!-- header end -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/navi.html'); ?>
<!-- main start -->
<div id="main" class="common">
<div id="mainArea">
<p><img src="img/title.png" alt="すでに登録されている方専用エントリー" /></p>
</div>
</div>
<!-- main end -->
<!-- pan start -->
<div id="pan">
<div id="panSub">
<ul class="clearfix">
<li><a href="../index.html">ホーム</a></li>
<li>すでに登録されている方専用エントリー</li>
</ul>
</div>
</div>
<!-- pan end -->
<!-- co start -->
<div id="co" class="clearfix">
<div id="coL">
<!-- page start -->
<div class="page new" id="entryPage">
<p><img src="../entry/img/step_03.png" alt=""/></p>
<p class="read"><span class="txtFinish">登録が完了致しました。</span><br />担当より折り返しご連絡致しますのでお待ちください。</p>
<h3 class="finish first">お知り合いを紹介するとAmazonギフト券1万円分がもらえる!</h3>
<p class="read">あなたとお知り合いの方の双方に１万円分のAmazonギフト券を贈呈いたします。<br />
ギフト券は、ご登録いただいたメールアドレスにコードで配信をさせていただきます。<br />
詳細は以下をご覧ください。</p>
<p class="aCenter"><img src="../campaign/img/main.jpg" width="532" height="409" alt=""/></p>
<h3 class="finish">キャンペーン対象とさせていただく条件は以下の全てを満たす場合とさせていただきます </h3>
<p>1.あなた：ヒューレックスの転職支援サービスにご登録済みの方<br />
2.お知り合い/ご友人：キャンペーン専用フォームからのご登録<br />
3.お知り合いの方がヒューレックスを通して転職に成功された場合<br />
4.お知り合いの方がその転職先で3ヶ月間勤務したことの確認が取れた場合</p>
<p class="aCenter"><a href="mailto:?subject=紹介キャンペーン&body=ご紹介された方専用エントリーフォームはこちら%0d%0ahttps://www.hurex.jp/campaign/form.html"><img src="../entry/img/btn_mail.png" width="406" height="71" alt=""/></a></p>
</div>
<!-- page end -->
</div>
<div id="coR">
<?php $Path = "../"; include(dirname(__FILE__).'/../include/right.html'); ?>
</div>
</div>
<!-- co end -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footer.html'); ?>
<!-- SB追加 2014/08/21 -->
<!-- Google Code for &#30003;&#12375;&#36796;&#12415; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 966187790;
var google_conversion_language = "ja";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "dRsYCMnA-1UQjrbbzAM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/966187790/?label=dRsYCMnA-1UQjrbbzAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Facebook Conversion Code for ヒューレックスのCV -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6016811407072', {'value':'0.00','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6016811407072&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" /></noscript>
<!-- Google Code for &#12467;&#12531;&#12496;&#12540;&#12472;&#12519;&#12531;&#12375;&#12383;&#12518;&#12540;&#12470;&#12540; -->
<!-- Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. For instructions on adding this tag and more information on the above requirements, read the setup guide: google.com/ads/remarketingsetup -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 966187790;
var google_conversion_label = "e0D_COu--1UQjrbbzAM";
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/966187790/?value=1.00&amp;label=e0D_COu--1UQjrbbzAM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- Yahoo Code for your Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var yahoo_conversion_id = 1000089031;
var yahoo_conversion_label = "-XxECM3G-1UQ9-vU0AM";
var yahoo_conversion_value = 20000;
/* ]]> */
</script>
<script type="text/javascript" src="https://s.yimg.jp/images/listing/tool/cv/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="https://b91.yahoo.co.jp/pagead/conversion/1000089031/?value=20000&amp;label=-XxECM3G-1UQ9-vU0AM&amp;guid=ON&amp;script=0&amp;disvt=true"/>
</div>
</noscript>
<!-- Begin INDEED conversion code -->
<script type="text/javascript">
/* <![CDATA[ */
var indeed_conversion_id = '5775465888875426';
var indeed_conversion_label = '';
/* ]]> */
</script>
<script type="text/javascript" src="//conv.indeed.com/pagead/conversion.js">
</script>
<noscript>
<img height=1 width=1 border=0 src="//conv.indeed.com/pagead/conv/5775465888875426/?script=0">
</noscript>
<!-- End INDEED conversion code -->

<!-- 20141203 追加 -->
<!-- Google Code for &#30003;&#36796; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963710423;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "NN7OCO_57VcQ15vEywM";
var google_conversion_value = 1.00;
var google_conversion_currency = "JPY";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/963710423/?value=1.00&amp;currency_code=JPY&amp;label=NN7OCO_57VcQ15vEywM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- リマーケティング タグの Google コード -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963710423;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963710423/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- リマーケティングタグ Yahoo -->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=1Wjm96V";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=1Wjm96V" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>

<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>