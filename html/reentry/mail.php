<?php
session_cache_limiter('private_no_expire');
session_start();

//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', '1');
ini_set('error_reporting', 0);
ini_set('display_errors', '0');
mb_language('Japanese');
ini_set('default_charset', 'UTF-8');
ini_set('mbstring.detect_order', 'auto');
ini_set('mbstring.http_input'  , 'auto');
ini_set('mbstring.http_output' , 'pass');
ini_set('mbstring.internal_encoding', 'UTF-8');
ini_set('mbstring.script_encoding'  , 'UTF-8');
ini_set('mbstring.substitute_character', 'none');
mb_regex_encoding('UTF-8');

define('ENCODE', 'UTF-8');

include (dirname(__FILE__) . "/system/template/template.php");
include (dirname(__FILE__) . "/system/validation.php");
//クラス読み込み
$template = new template();
$validation = new Validation();

//セッションにデータを格納
foreach($_POST as $key => $val){
	unset($_SESSION[$key]);
	$_SESSION[$key] = $_POST[$key];
}
//セッションデータをname属性の名前に設定（ボディで使用するため
include (dirname(__FILE__) . "/system/set_session.php");

//コンフィグ読込
include (dirname(__FILE__) . "/config.php");

//送信
if($_POST["mode"] == "send"){
	include (dirname(__FILE__) . "/system/send.php");
//戻る
}else if($_POST["mode"] == "return"){
	$template->view(Index, $_POST, $err);
//確認処理
}else if($_POST["mode"] == "confirm"){
	//セッションにデータを格納
	foreach($_POST as $key => $val){
		unset($_SESSION[$key]);
		$_SESSION[$key] = $_POST[$key];
	}

	//エラーチェック
	$err = "";
	$err = $validation->run();

	//エラーがあったら初期画面へ
	if(!empty($err)){
		$template->view(Index, $_SESSION, $err);
	}else{
		$template->view(Confirm, $_SESSION, $err);
	}
//その他
}else{
	$data="";
	$err="";
	header("HTTP/1.1 200 ok");
	$url = Index;
	header("Location: $url");
	exit();
}

?>