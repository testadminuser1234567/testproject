<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="index,follow" />
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形" />
<meta name="description" content="東北地方の求人多数！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。" />
<meta http-equiv="imagetoolbar" content="no" />
<meta http-equiv="Content-Language" content="ja" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="JavaScript" />
<link rel="shortcut icon" href="/favicon.ico" />
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all" />
<link rel="canonical" href="https://www.hurex.jp/reentry/index.html">
<link href="./form.css" rel="stylesheet" type="text/css" />
<title>すでに登録されている方専用エントリー｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<meta name="copyright" content="" />
<script type="text/javascript" src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="../js/remodal/remodal.css">
<link rel="stylesheet" href="../js/remodal/remodal-default-theme.css">
<script src="../js/remodal/remodal.js"></script>
<script type="text/javascript" src="../js/rollover.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<!--[if lte IE 6]>   
<script type="text/javascript" src="../js/DD_belatedPNG_0.0.8a.js">   
</script>   
<script type="text/javascript">  /* EXAMPLE */  DD_belatedPNG.fix('img');</script>   
<![endif]-->
<script type="text/javascript" src="./form_validation.js"></script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body class="nofloating">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<!-- header start -->
<div id="header" class="clearfix">
<h1>すでに登録されている方専用エントリー｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</h1>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/header.html'); ?>
</div>
<!-- header end -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/navi.html'); ?>
<!-- main start -->
<div id="main" class="common">
<div id="mainArea">
<p><img src="img/title.png" alt="すでに登録されている方専用エントリー" /></p>
</div>
</div>
<!-- main end -->
<!-- pan start -->
<div id="pan">
<div id="panSub">
<ul class="clearfix">
<li><a href="../index.html">ホーム</a></li>
<li>すでに登録されている方専用エントリー</li>
</ul>
</div>
</div>
<!-- pan end -->
<!-- co start -->
<div id="co" class="clearfix">
<div id="coL">
<!-- page start -->
<div class="page new" id="entryPage">
<p><img src="../entry/img/step_02.png" alt=""/></p>
<p>以下の内容で送信致します。</p>
<?php require($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/site/setmaster.php');?>
<?php require_once("../entry/system/esc.php");?>
<table width="650">
<tr>
<th class="<?php if($shimei):?>ok<?php else:?>hissu<?php endif;?> ">氏名</th>
<td><?php echo esc($shimei);?></td>
</tr>
<tr>
<th class="<?php if($kana):?>ok<?php else:?>hissu<?php endif;?>">ふりがな</th>
<td><?php echo esc($kana);?></td>
</tr>
<tr>
<th class="<?php if($tel1):?>ok<?php else:?>hissu<?php endif;?>">電話</th>
<td><?php echo esc($tel1);?></td>
</tr>
<?php if(!empty($comment)):?>
<tr>
<th class="<?php if($comment):?>ok<?php else:?><?php endif;?> career">その他、<br />ご希望などございましたら<br />記入をお願いいたします</th>
<td><?php echo nl2br(esc($comment));?></td>
</tr>
<?php endif;?>

</table>
<!-- start -->
<div class="btnSend">
<form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="shimei" value="<?php echo esc($shimei);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="tel1" value="<?php echo esc($tel1);?>">
<input type="hidden" name="comment" value="<?php echo esc($comment);?>">
<input type="hidden" name="agree" value="<?php echo esc($agree);?>">
<input type="hidden" name="job_id" value="<?php echo esc($job_id);?>">
<input name="btnSend" type="image" class="image" id="btnSend" value="送信する" onclick="window.onbeforeunload=null;" src="../entry/img/btn_send.png" alt="上記の内容で送信" width="502" height="63" />
<input type="hidden" name="mode" value="send">
</form>
</div>
<div class="aCenter">
<form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="mode" value="return">
<input type="hidden" name="shimei" value="<?php echo esc($shimei);?>">
<input type="hidden" name="tel1" value="<?php echo esc($tel1);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="comment" value="<?php echo esc($comment);?>">
<input type="hidden" name="agree" value="<?php echo esc($agree);?>">
<input type="hidden" name="job_id" value="<?php echo esc($job_id);?>">

<input type="image" value="戻る" src="../entry/img/btn_back.png" width="210" height="36" alt="戻る" onclick="window.onbeforeunload=null;" style="border:none;padding:0px;"/>
</form>
<div class="clearBT"></div>

</div>
</div>
<!-- page end -->
</div>
<div id="coR">
<?php $Path = "../"; include(dirname(__FILE__).'/../include/right.html'); ?>
</div>
</div>
<!-- co end -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footer.html'); ?>

<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>