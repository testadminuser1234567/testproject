<?php
//入力ページの設定
define('Index', 'index.html');
//確認ページの設定（使わない場合はこのまま）
define('Confirm', 'confirm.php');
//完了ページの設定
define('Finish', 'finish.php');

define('ENCODE', 'UTF-8');

//ユーザの名前設定
$unit_name = $onamae . "様";
$email = $mail;

$send_from = "info@hurex.co.jp";

//サンクスメール設定
$thanks_mail_subject = "求人のご依頼・お問合せありがとうございました。【ヒューレックス株式会社】";
$thanks_mail_from = "info@hurex.co.jp";
$thanks_mail_str = "ヒューレックス株式会社";
$thanks_body= <<< EOF
この度はヒューレックス株式会社公式ホームページより
求人のご依頼・お問合せ頂きまして誠にありがとうございました。
下記内容にて受け付けましたので内容をご確認下さいませ。

内容確認の上、担当よりご連絡申しあげます。

※本メールは自動返信されております。


【送信内容】
御社名　： $company
部署名	： $busyo
ご担当者名  ： $onamae
ご住所　： $address
メールアドレス　：　$mail
お問い合わせ内容：
$naiyo
$naiyoadd

━━━━━━━━━━━━━━━━━━━━━━━━━
ヒューレックス株式会社

〒980-6117 宮城県仙台市青葉区中央1-3-1 アエル17階
Tel. 0120-14-1150 / Fax. 022-723-1738
E-mail. nobuyoshi.takahashi@hurex.co.jp
─────────────────────────
■HUREX 公式WEBサイト「企業と人を繋ぐ人財紹介会社」
　https://www.hurex.jp/
■HUREX 公式facebookページ「東北の今を情報発信！」
　http://www.facebook.com/hurex.japan
━━━━━━━━━━━━━━━━━━━━━━━━━


EOF;

//通知メール設定
$order_mail_subject = "【お問合せ】HPより求人のご依頼・お問合せがありました。";
$order_mail_to[] = array('info@hurex.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('tosa@smt-net.co.jp', 'ヒューレックス');
//$order_mail_to[] = array('info@smt-net.co.jp', '追加宛先');
$order_body= <<< EOF
ホームページより
下記内容にて求人のご依頼・お問合せがありました。

────────────────────────────
▼送信内容
────────────────────────────

御社名　： $company
部署名	： $busyo
ご担当者名  ： $onamae
ご住所　： $address
メールアドレス　：　$mail
お問い合わせ内容：
$naiyo
$naiyoadd


EOF;

//項目チェック用
$validation->set_rules('company','御社名','required');
$validation->set_rules('onamae','ご担当者名','required');
$validation->set_rules('address','ご住所','required');
$validation->set_rules('mail','メールアドレス','required|valid_email');
$validation->set_rules('naiyo','お問い合わせ内容','required');

//ログファイル出力 on or off
$log = "off";

?>