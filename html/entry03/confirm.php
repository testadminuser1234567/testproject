<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="index,follow" />
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形" />
<meta name="description" content="東北地方の求人多数！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。" />
<meta http-equiv="imagetoolbar" content="no" />
<meta http-equiv="Content-Language" content="ja" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="JavaScript" />
<link rel="shortcut icon" href="/favicon.ico" />
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all" />
<link href="form.css" rel="stylesheet" type="text/css" media="all" />
<link rel="canonical" href="https://www.hurex.jp/entry/form.html">
<title>求人詳細問い合わせ｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<meta name="copyright" content="" />
<script type="text/javascript" src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="../js/remodal/remodal.css">
<link rel="stylesheet" href="../js/remodal/remodal-default-theme.css">
<script src="../js/remodal/remodal.js"></script>
<script type="text/javascript" src="../js/rollover.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<!--[if lte IE 6]>   
<script type="text/javascript" src="../js/DD_belatedPNG_0.0.8a.js">   
</script>   
<script type="text/javascript">  /* EXAMPLE */  DD_belatedPNG.fix('img');</script>   
<![endif]-->
<script type="text/javascript" src="./form_validation.js"></script>
<script>
$(function(){
	$('#entryPage p.txtMoreBtn').click(function() {
		$(this).next().slideToggle();
	}).next().hide();
});
</script>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<body class="nofloating">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<!-- header start -->
<div id="header" class="clearfix">
<h1>求人詳細問い合わせ｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</h1>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/header.html'); ?>
</div>
<!-- header end -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/navi.html'); ?>
<!-- main start -->
<div id="main" class="common">
<div id="mainArea">
<p><img src="img/title.png" alt="求人詳細問い合わせ" /></p>
</div>
</div>
<!-- main end -->
<!-- pan start -->
<div id="pan">
<div id="panSub">
<ul class="clearfix">
<li><a href="../index.html">ホーム</a></li>
<li>求人詳細問い合わせ</li>
</ul>
</div>
</div>
<!-- pan end -->
<!-- co start -->
<div id="co" class="clearfix">
<div id="coL">
<!-- page start -->



<div class="page new" id="entryPage">
<p><img src="../entry/img/step_02.png" alt=""/></p>
<p>以下の内容で送信致します。</p>
<?php require($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/site/setmaster.php');?>
<?php require_once("../entry/system/esc.php");?>
<table width="720">
<tr>
<th class="<?php if($shimei):?>ok<?php else:?>hissu<?php endif;?> ">氏名</th>
<td><?php echo esc($shimei);?></td>
</tr>
<tr>
<th class="<?php if($kana):?>ok<?php else:?>hissu<?php endif;?>">ふりがな</th>
<td><?php echo esc($kana);?></td>
</tr>
<tr>
<th class="<?php if($year && $month && $day):?>ok<?php else:?>hissu<?php endif;?>">生年月日</th>
<td>
<?php echo esc($year);?>年<?php echo esc($month);?>月<?php echo esc($day);?>日
</td>
</tr>
<tr>
<th class="<?php if($sex):?>ok<?php else:?>hissu<?php endif;?>">性別</th>
<td>
<?php echo esc($sex);?>
</td>
</tr>
<tr>
<th class="<?php if($pref):?>ok<?php else:?>hissu<?php endif;?>">現住所</th>
<td>
<?php echo esc($pref);?>
</td>
</tr>
<tr>
<th class="<?php if($tel1):?>ok<?php else:?>hissu<?php endif;?>">電話</th>
<td><?php echo esc($tel1);?></td>
</tr>
<tr>
<th class="<?php if($mail1):?>ok<?php else:?>hissu<?php endif;?>">メール</th>
<td><?php echo esc($mail1);?></td>
</tr>
<?php if(!empty(esc($school_div_id))):?>
<tr>
<th class="<?php if($school_div_id):?>ok<?php else:?>hissu<?php endif;?>">最終学歴</th>
<td><?php echo esc($school_div_id);?></td>
</tr>
<?php endif;?>
<?php if(!empty(esc($company_number)) || esc($company_number) == 0):?>
<tr>
<th class="<?php if($company_number):?>ok<?php else:?>hissu<?php endif;?>">経験社数</th>
<td>
<?php if(esc($company_number)==5):?>5社以上
<?php else:?>
<?php echo esc($company_number);?>社
<?php endif;?>
</td>
</tr>
<?php endif;?>

    <?php if(!empty(esc($expectarea1))):?>
        <tr>
<th class="<?php if($jokyo):?>ok<?php else:?>hissu<?php endif;?>">就業状況</th>
<td><?php echo esc($jokyo);?></td>
</tr>
<tr>
<th class="<?php if($expectarea1):?>ok<?php else:?>hissu<?php endif;?> expectarea1">勤務地<br />
（第1希望）</th>
<td><?php echo esc($expectarea1);?></td>
</tr>
    <?php endif;?>

    <?php if(!empty(esc($expectarea2))):?>
        <tr>
<th class="<?php if($expectarea2):?>ok<?php else:?><?php endif;?> expectarea2">勤務地<br />
（第2希望）</th>
<td><?php echo esc($expectarea2);?></td>
        </tr>
    <?php endif;?>

    <?php if(!empty(esc($expectarea3))):?>
        <tr>
<th class="<?php if($expectarea3):?>ok<?php else:?><?php endif;?> expectarea3">勤務地<br />
（第3希望）</th>
<td><?php echo esc($expectarea3);?></td>
        </tr>
    <?php endif;?>

<?php if(!empty(esc($comment))):?>
<tr>
<th class="<?php if($comment):?>ok<?php else:?><?php endif;?> career">その他、<br />ご希望などございましたら<br />記入をお願いいたします</th>
<td><?php echo nl2br(esc($comment));?></td>
</tr>
<?php endif;?>
<tr>
<th class="<?php if($file1):?>ok<?php else:?><?php endif;?>">履歴書</th>
<td><?php echo esc($file1);?>
</td>
</tr>
<tr>
<th class="<?php if($file2):?>ok<?php else:?><?php endif;?>">職務経歴書</th>
<td><?php echo esc($file2);?>
</td>
</tr>
<tr>
<th class="<?php if($file3):?>ok<?php else:?><?php endif;?>">その他</th>
<td><?php echo esc($file3);?>
</td>
</tr>

</table>


<!-- start -->
<div class="btnSend">
<form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data">

<input name="btnSend" type="image" class="image" id="btnSend" value="送信する" onclick="window.onbeforeunload=null;" src="../entry/img/btn_send.png" alt="上記の内容で送信" width="502" height="63" />
<input type="hidden" name="mode" value="send">
<input type="hidden" name="shimei" value="<?php echo esc($shimei);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="year" value="<?php echo esc($year);?>">
<input type="hidden" name="month" value="<?php echo esc($month);?>">
<input type="hidden" name="day" value="<?php echo esc($day);?>">
<input type="hidden" name="sex" value="<?php echo esc($sex);?>">
<input type="hidden" name="zip" value="<?php echo esc($zip);?>">
<input type="hidden" name="pref" value="<?php echo esc($pref);?>">
<input type="hidden" name="address" value="<?php echo esc($address);?>">
<input type="hidden" name="tel1" value="<?php echo esc($tel1);?>">
<input type="hidden" name="tel2" value="<?php echo esc($tel2);?>">
<input type="hidden" name="mail1" value="<?php echo esc($mail1);?>">
<input type="hidden" name="mail2" value="<?php echo esc($mail2);?>">
<input type="hidden" name="file1" value="<?php echo esc($file1);?>">
<input type="hidden" name="file2" value="<?php echo esc($file2);?>">
<input type="hidden" name="file3" value="<?php echo esc($file3);?>">
<input type="hidden" name="file4" value="<?php echo esc($file4);?>">
<input type="hidden" name="agree" value="<?php echo esc($agree);?>">

<input type="hidden" name="renraku" value="<?php echo esc($renraku);?>">

<input type="hidden" name="school_div_id" value="<?php echo esc($school_div_id);?>">
<input type="hidden" name="school" value="<?php echo esc($school);?>">
<input type="hidden" name="division" value="<?php echo esc($division);?>">
<input type="hidden" name="zaiseki_year_from" value="<?php echo esc($zaiseki_year_from);?>">
<input type="hidden" name="zaiseki_month_from" value="<?php echo esc($zaiseki_month_from);?>">
<input type="hidden" name="zaiseki_year_to" value="<?php echo esc($zaiseki_year_to);?>">
<input type="hidden" name="zaiseki_month_to" value="<?php echo esc($zaiseki_month_to);?>">
<input type="hidden" name="company_number" value="<?php echo esc($company_number);?>">
<input type="hidden" name="jokyo" value="<?php echo esc($jokyo);?>">
<input type="hidden" name="job1" value="<?php echo esc($job1);?>">
<input type="hidden" name="job2" value="<?php echo esc($job2);?>">
<input type="hidden" name="job3" value="<?php echo esc($job3);?>">


<input type="hidden" name="graduate_kubun" value="<?php echo esc($graduate_kubun);?>">

<input type="hidden" name="company_name" value="<?php echo esc($company_name);?>">
<input type="hidden" name="busho" value="<?php echo esc($busho);?>">
<input type="hidden" name="biz1" value="<?php echo esc($biz1);?>">
<input type="hidden" name="employees" value="<?php echo esc($employees);?>">
<input type="hidden" name="jigyo_naiyo" value="<?php echo esc($jigyo_naiyo);?>">
<input type="hidden" name="income" value="<?php echo esc($income);?>">
<input type="hidden" name="shugyo_keitai" value="<?php echo esc($shugyo_keitai);?>">
<input type="hidden" name="yakushoku" value="<?php echo esc($yakushoku);?>">
<input type="hidden" name="shugyo_year_from" value="<?php echo esc($shugyo_year_from);?>">
<input type="hidden" name="shugyo_month_from" value="<?php echo esc($shugyo_month_from);?>">
<input type="hidden" name="shugyo_year_to" value="<?php echo esc($shugyo_year_to);?>">
<input type="hidden" name="shugyo_month_to" value="<?php echo esc($shugyo_month_to);?>">
<input type="hidden" name="shugyo_naiyo" value="<?php echo esc($shugyo_naiyo);?>">

<input type="hidden" name="company_name2" value="<?php echo esc($company_name2);?>">
<input type="hidden" name="busho2" value="<?php echo esc($busho2);?>">
<input type="hidden" name="biz2" value="<?php echo esc($biz2);?>">
<input type="hidden" name="employees2" value="<?php echo esc($employees2);?>">
<input type="hidden" name="jigyo_naiyo2" value="<?php echo esc($jigyo_naiyo2);?>">
<input type="hidden" name="income2" value="<?php echo esc($income2);?>">
<input type="hidden" name="shugyo_keitai2" value="<?php echo esc($shugyo_keitai2);?>">
<input type="hidden" name="yakushoku2" value="<?php echo esc($yakushoku2);?>">
<input type="hidden" name="shugyo_year_from2" value="<?php echo esc($shugyo_year_from2);?>">
<input type="hidden" name="shugyo_month_from2" value="<?php echo esc($shugyo_month_from2);?>">
<input type="hidden" name="shugyo_year_to2" value="<?php echo esc($shugyo_year_to2);?>">
<input type="hidden" name="shugyo_month_to2" value="<?php echo esc($shugyo_month_to2);?>">
<input type="hidden" name="shugyo_naiyo2" value="<?php echo esc($shugyo_naiyo2);?>">

<input type="hidden" name="company_name3" value="<?php echo esc($company_name3);?>">
<input type="hidden" name="busho3" value="<?php echo esc($busho3);?>">
<input type="hidden" name="biz3" value="<?php echo esc($biz3);?>">
<input type="hidden" name="employees3" value="<?php echo esc($employees3);?>">
<input type="hidden" name="jigyo_naiyo3" value="<?php echo esc($jigyo_naiyo3);?>">
<input type="hidden" name="income3" value="<?php echo esc($income3);?>">
<input type="hidden" name="shugyo_keitai3" value="<?php echo esc($shugyo_keitai3);?>">
<input type="hidden" name="yakushoku3" value="<?php echo esc($yakushoku3);?>">
<input type="hidden" name="shugyo_year_from3" value="<?php echo esc($shugyo_year_from3);?>">
<input type="hidden" name="shugyo_month_from3" value="<?php echo esc($shugyo_month_from3);?>">
<input type="hidden" name="shugyo_year_to3" value="<?php echo esc($shugyo_year_to3);?>">
<input type="hidden" name="shugyo_month_to3" value="<?php echo esc($shugyo_month_to3);?>">
<input type="hidden" name="shugyo_naiyo3" value="<?php echo esc($shugyo_naiyo3);?>">

<input type="hidden" name="sikaku1" value="<?php echo esc($sikaku1);?>">
<input type="hidden" name="sikaku_year1" value="<?php echo esc($sikaku_year1);?>">
<input type="hidden" name="sikaku_month1" value="<?php echo esc($sikaku_month1);?>">
<input type="hidden" name="sikaku2" value="<?php echo esc($sikaku2);?>">
<input type="hidden" name="sikaku_year2" value="<?php echo esc($sikaku_year2);?>">
<input type="hidden" name="sikaku_month2" value="<?php echo esc($sikaku_month2);?>">
<input type="hidden" name="sikaku3" value="<?php echo esc($sikaku3);?>">
<input type="hidden" name="sikaku_year3" value="<?php echo esc($sikaku_year3);?>">
<input type="hidden" name="sikaku_month3" value="<?php echo esc($sikaku_month3);?>">

<input type="hidden" name="conversation" value="<?php echo esc($conversation);?>">
<input type="hidden" name="reading" value="<?php echo esc($reading);?>">
<input type="hidden" name="writing" value="<?php echo esc($writing);?>">
<input type="hidden" name="toeic" value="<?php echo esc($toeic);?>">
<input type="hidden" name="toefl" value="<?php echo esc($toefl);?>">

<input type="hidden" name="other_language1" value="<?php echo esc($other_language1);?>">
<input type="hidden" name="other_language2" value="<?php echo esc($other_language2);?>">

<input type="hidden" name="kibo_biz1" value="<?php echo esc($kibo_biz1);?>">
<input type="hidden" name="kibo_biz2" value="<?php echo esc($kibo_biz2);?>">
<input type="hidden" name="kibo_biz3" value="<?php echo esc($kibo_biz3);?>">
<input type="hidden" name="kibo_job1" value="<?php echo esc($kibo_job1);?>">
<input type="hidden" name="kibo_job2" value="<?php echo esc($kibo_job2);?>">
<input type="hidden" name="kibo_job3" value="<?php echo esc($kibo_job3);?>">
<input type="hidden" name="kibo_pref1" value="<?php echo esc($kibo_pref1);?>">
    <input type="hidden" name="kibo_pref2" value="<?php echo esc($kibo_pref2);?>">
    <input type="hidden" name="kibo_pref3" value="<?php echo esc($kibo_pref3);?>">
<input type="hidden" name="kibo_income" value="<?php echo esc($kibo_income);?>">
<input type="hidden" name="kibo_jiki_year" value="<?php echo esc($kibo_jiki_year);?>">
<input type="hidden" name="kibo_jiki_month" value="<?php echo esc($kibo_jiki_month);?>">
<input type="hidden" name="kibo_jiki_day" value="<?php echo esc($kibo_jiki_day);?>">
<input type="hidden" name="kibou_jiki" value="<?php echo esc($kibou_jiki);?>">

<input type="hidden" name="howto" value="<?php echo esc($howto);?>">
    <input type="hidden" name="expectarea1" value="<?php echo esc($expectarea1);?>">
    <input type="hidden" name="expectarea2" value="<?php echo esc($expectarea2);?>">
    <input type="hidden" name="expectarea3" value="<?php echo esc($expectarea3);?>">
<input type="hidden" name="comment" value="<?php echo esc($comment);?>">
<?php 
if(!empty($_POST['id'])){
	$job_id="";
	$job_id = htmlspecialchars($_GET['id'], ENT_QUOTES, 'UTF-8');
	if(!is_numeric($job_id)){
		$job_id = "";
	}
}else if(empty($job_id)){
	$job_id = "";
}
?>
<input type="hidden" name="job_id" value="<?php echo esc($job_id);?>">
</form>
</div>
<div class="aCenter">
<form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="mode" value="return">
<input type="hidden" name="shimei" value="<?php echo esc($shimei);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="year" value="<?php echo esc($year);?>">
<input type="hidden" name="month" value="<?php echo esc($month);?>">
<input type="hidden" name="day" value="<?php echo esc($day);?>">
<input type="hidden" name="sex" value="<?php echo esc($sex);?>">
<input type="hidden" name="zip" value="<?php echo esc($zip);?>">
<input type="hidden" name="pref" value="<?php echo esc($pref);?>">
<input type="hidden" name="address" value="<?php echo esc($address);?>">
<input type="hidden" name="tel1" value="<?php echo esc($tel1);?>">
<input type="hidden" name="tel2" value="<?php echo esc($tel2);?>">
<input type="hidden" name="mail1" value="<?php echo esc($mail1);?>">
<input type="hidden" name="mail2" value="<?php echo esc($mail2);?>">
<input type="hidden" name="file1" value="<?php echo esc($file1);?>">
<input type="hidden" name="file2" value="<?php echo esc($file2);?>">
<input type="hidden" name="file3" value="<?php echo esc($file3);?>">
<input type="hidden" name="file4" value="<?php echo esc($file4);?>">
<input type="hidden" name="agree" value="<?php echo esc($agree);?>">

<input type="hidden" name="renraku" value="<?php echo esc($renraku);?>">

<input type="hidden" name="school_div_id" value="<?php echo esc($school_div_id);?>">
<input type="hidden" name="school" value="<?php echo esc($school);?>">
<input type="hidden" name="division" value="<?php echo esc($division);?>">
<input type="hidden" name="zaiseki_year_from" value="<?php echo esc($zaiseki_year_from);?>">
<input type="hidden" name="zaiseki_month_from" value="<?php echo esc($zaiseki_month_from);?>">
<input type="hidden" name="zaiseki_year_to" value="<?php echo esc($zaiseki_year_to);?>">
<input type="hidden" name="zaiseki_month_to" value="<?php echo esc($zaiseki_month_to);?>">
<input type="hidden" name="company_number" value="<?php echo esc($company_number);?>">
<input type="hidden" name="jokyo" value="<?php echo esc($jokyo);?>">
<input type="hidden" name="job1" value="<?php echo esc($job1);?>">
<input type="hidden" name="job2" value="<?php echo esc($job2);?>">
<input type="hidden" name="job3" value="<?php echo esc($job3);?>">


<input type="hidden" name="graduate_kubun" value="<?php echo esc($graduate_kubun);?>">

<input type="hidden" name="company_name" value="<?php echo esc($company_name);?>">
<input type="hidden" name="busho" value="<?php echo esc($busho);?>">
<input type="hidden" name="biz1" value="<?php echo esc($biz1);?>">
<input type="hidden" name="employees" value="<?php echo esc($employees);?>">
<input type="hidden" name="jigyo_naiyo" value="<?php echo esc($jigyo_naiyo);?>">
<input type="hidden" name="income" value="<?php echo esc($income);?>">
<input type="hidden" name="shugyo_keitai" value="<?php echo esc($shugyo_keitai);?>">
<input type="hidden" name="yakushoku" value="<?php echo esc($yakushoku);?>">
<input type="hidden" name="shugyo_year_from" value="<?php echo esc($shugyo_year_from);?>">
<input type="hidden" name="shugyo_month_from" value="<?php echo esc($shugyo_month_from);?>">
<input type="hidden" name="shugyo_year_to" value="<?php echo esc($shugyo_year_to);?>">
<input type="hidden" name="shugyo_month_to" value="<?php echo esc($shugyo_month_to);?>">
<input type="hidden" name="shugyo_naiyo" value="<?php echo esc($shugyo_naiyo);?>">

<input type="hidden" name="company_name2" value="<?php echo esc($company_name2);?>">
<input type="hidden" name="busho2" value="<?php echo esc($busho2);?>">
<input type="hidden" name="biz2" value="<?php echo esc($biz2);?>">
<input type="hidden" name="employees2" value="<?php echo esc($employees2);?>">
<input type="hidden" name="jigyo_naiyo2" value="<?php echo esc($jigyo_naiyo2);?>">
<input type="hidden" name="income2" value="<?php echo esc($income2);?>">
<input type="hidden" name="shugyo_keitai2" value="<?php echo esc($shugyo_keitai2);?>">
<input type="hidden" name="yakushoku2" value="<?php echo esc($yakushoku2);?>">
<input type="hidden" name="shugyo_year_from2" value="<?php echo esc($shugyo_year_from2);?>">
<input type="hidden" name="shugyo_month_from2" value="<?php echo esc($shugyo_month_from2);?>">
<input type="hidden" name="shugyo_year_to2" value="<?php echo esc($shugyo_year_to2);?>">
<input type="hidden" name="shugyo_month_to2" value="<?php echo esc($shugyo_month_to2);?>">
<input type="hidden" name="shugyo_naiyo2" value="<?php echo esc($shugyo_naiyo2);?>">

<input type="hidden" name="company_name3" value="<?php echo esc($company_name3);?>">
<input type="hidden" name="busho3" value="<?php echo esc($busho3);?>">
<input type="hidden" name="biz3" value="<?php echo esc($biz3);?>">
<input type="hidden" name="employees3" value="<?php echo esc($employees3);?>">
<input type="hidden" name="jigyo_naiyo3" value="<?php echo esc($jigyo_naiyo3);?>">
<input type="hidden" name="income3" value="<?php echo esc($income3);?>">
<input type="hidden" name="shugyo_keitai3" value="<?php echo esc($shugyo_keitai3);?>">
<input type="hidden" name="yakushoku3" value="<?php echo esc($yakushoku3);?>">
<input type="hidden" name="shugyo_year_from3" value="<?php echo esc($shugyo_year_from3);?>">
<input type="hidden" name="shugyo_month_from3" value="<?php echo esc($shugyo_month_from3);?>">
<input type="hidden" name="shugyo_year_to3" value="<?php echo esc($shugyo_year_to3);?>">
<input type="hidden" name="shugyo_month_to3" value="<?php echo esc($shugyo_month_to3);?>">
<input type="hidden" name="shugyo_naiyo3" value="<?php echo esc($shugyo_naiyo3);?>">

<input type="hidden" name="sikaku1" value="<?php echo esc($sikaku1);?>">
<input type="hidden" name="sikaku_year1" value="<?php echo esc($sikaku_year1);?>">
<input type="hidden" name="sikaku_month1" value="<?php echo esc($sikaku_month1);?>">
<input type="hidden" name="sikaku2" value="<?php echo esc($sikaku2);?>">
<input type="hidden" name="sikaku_year2" value="<?php echo esc($sikaku_year2);?>">
<input type="hidden" name="sikaku_month2" value="<?php echo esc($sikaku_month2);?>">
<input type="hidden" name="sikaku3" value="<?php echo esc($sikaku3);?>">
<input type="hidden" name="sikaku_year3" value="<?php echo esc($sikaku_year3);?>">
<input type="hidden" name="sikaku_month3" value="<?php echo esc($sikaku_month3);?>">

<input type="hidden" name="conversation" value="<?php echo esc($conversation);?>">
<input type="hidden" name="reading" value="<?php echo esc($reading);?>">
<input type="hidden" name="writing" value="<?php echo esc($writing);?>">
<input type="hidden" name="toeic" value="<?php echo esc($toeic);?>">
<input type="hidden" name="toefl" value="<?php echo esc($toefl);?>">

<input type="hidden" name="other_language1" value="<?php echo esc($other_language1);?>">
<input type="hidden" name="other_language2" value="<?php echo esc($other_language2);?>">

<input type="hidden" name="kibo_biz1" value="<?php echo esc($kibo_biz1);?>">
<input type="hidden" name="kibo_biz2" value="<?php echo esc($kibo_biz2);?>">
<input type="hidden" name="kibo_biz3" value="<?php echo esc($kibo_biz3);?>">
<input type="hidden" name="kibo_job1" value="<?php echo esc($kibo_job1);?>">
<input type="hidden" name="kibo_job2" value="<?php echo esc($kibo_job2);?>">
<input type="hidden" name="kibo_job3" value="<?php echo esc($kibo_job3);?>">
<input type="hidden" name="kibo_pref1" value="<?php echo esc($kibo_pref1);?>">
    <input type="hidden" name="kibo_pref2" value="<?php echo esc($kibo_pref2);?>">
    <input type="hidden" name="kibo_pref3" value="<?php echo esc($kibo_pref3);?>">
<input type="hidden" name="kibo_income" value="<?php echo esc($kibo_income);?>">
<input type="hidden" name="kibo_jiki_year" value="<?php echo esc($kibo_jiki_year);?>">
<input type="hidden" name="kibo_jiki_month" value="<?php echo esc($kibo_jiki_month);?>">
<input type="hidden" name="kibo_jiki_day" value="<?php echo esc($kibo_jiki_day);?>">
<input type="hidden" name="kibou_jiki" value="<?php echo esc($kibou_jiki);?>">

<input type="hidden" name="howto" value="<?php echo esc($howto);?>">
    <input type="hidden" name="expectarea1" value="<?php echo esc($expectarea1);?>">
    <input type="hidden" name="expectarea2" value="<?php echo esc($expectarea2);?>">
    <input type="hidden" name="expectarea3" value="<?php echo esc($expectarea3);?>">
<input type="hidden" name="comment" value="<?php echo esc($comment);?>">

<input type="hidden" name="job_id" value="<?php echo esc($job_id);?>">
<input type="image" value="戻る" src="../entry/img/btn_back.png" width="210" height="36" alt="戻る" onclick="window.onbeforeunload=null;" style="border:none;padding:0px;"/>
</form>
<div class="clearBT"></div>
</div>
</div>
<!-- page end -->
</div>
<div id="coR">
<?php $Path = "../"; include(dirname(__FILE__).'/../include/right.html'); ?>
</div>
</div>
<!-- co end -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footer.html'); ?>

<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>
