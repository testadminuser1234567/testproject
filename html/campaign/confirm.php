<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="index,follow" />
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形" />
<meta name="description" content="東北地方の求人多数！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。" />
<meta http-equiv="imagetoolbar" content="no" />
<meta http-equiv="Content-Language" content="ja" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="JavaScript" />
<link rel="shortcut icon" href="/favicon.ico" />
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all" />
<link rel="canonical" href="https://www.hurex.jp/campaign/form.html">
<title>お知り合い紹介キャンペーン｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<meta name="copyright" content="" />
<script type="text/javascript" src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="../js/remodal/remodal.css">
<link rel="stylesheet" href="../js/remodal/remodal-default-theme.css">
<script src="../js/remodal/remodal.js"></script>
<script type="text/javascript" src="../js/rollover.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script type="text/javascript" src="../js/jquery.tile.js"></script>
<script>
$(window).load(function(){
  $("#entryPage #jissekiBox .box").tile(3);
});
</script>
<!--[if lte IE 6]>   
<script type="text/javascript" src="../js/DD_belatedPNG_0.0.8a.js">   
</script>   
<script type="text/javascript">  /* EXAMPLE */  DD_belatedPNG.fix('img');</script>   
<![endif]-->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<!-- header start -->
<div id="header" class="clearfix">
<h1>お知り合い紹介キャンペーン｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</h1>
<?php $Path = "../"; include(dirname(__FILE__).'/../include/header.html'); ?>
</div>
<!-- header end -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/navi.html'); ?>
<!-- main start -->
<div id="main" class="common">
<div id="mainArea">
<p><img src="img/title.png" alt="お知り合い紹介キャンペーン" /></p>
</div>
</div>
<!-- main end -->
<!-- pan start -->
<div id="pan">
<div id="panSub">
<ul class="clearfix">
<li><a href="https://www.hurex.jp/">ホーム</a></li>
<li>お知り合い紹介キャンペーン</li>
</ul>
</div>
</div>
<!-- pan end -->
<!-- co start -->
<div id="co" class="clearfix">
<div id="coL">
<!-- page start -->
<div class="page" id="entryPage">
<h2><img src="img/main.jpg" alt="お知り合い紹介キャンペーン 転職成功で、Amazonギフト券1万円分プレゼント" /></h2>
<h2 class="titleImg"><img src="img/form_title_03.gif" alt="キャンペーン・転職支援サービスへ登録 " /></h2>
<p><img src="img/step_02.gif" alt="入力内容確認" /></p>
<?php require($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/site/setmaster.php');?>
<?php require_once("system/esc.php");?>
<table width="650">
<tr>
<th><label for="shoukai_name">ご紹介元のお名前</label></th>
<td><?php echo esc($shoukai_name);?></td>
</tr>
<tr>
<th><label for="shoukai_mail">ご紹介元のメールアドレス</label></th>
<td><?php echo esc($shoukai_mail);?></td>
</tr>
<tr>
<th><label for="shimei">氏名</label></th>
<td><?php echo esc($shimei);?></td>
</tr>
<tr>
<th>生年月日</th>
<td>
<?php echo esc($year);?>年<?php echo esc($month);?>月<?php echo esc($day);?>日
</td>
</tr>
<tr>
<th>性別</th>
<td>
<?php echo esc($sex);?>
</td>
</tr>
<tr>
<th>現住所</th>
<td>
<?php echo esc($pref);?>
</td>
</tr>
<tr>
<th><label for="tel1">電話</label></th>
<td><?php echo esc($tel1);?></td>
</tr>
<tr>
<th><label for="mail1">メール</label></th>
<td><?php echo esc($mail1);?></td>
</tr>
<?php if(!empty(esc($school_div_id))):?>
<tr>
<th>最終学歴</th>
<td><?php echo esc($school_div_id);?></td>
</tr>
<?php endif;?>
<?php if(!empty(esc($company_number)) || esc($company_number) == 0):?>
<tr>
<th>経験社数</th>
<td>
<?php if(esc($company_number)==5):?>5社以上
<?php else:?>
<?php echo esc($company_number);?>社
<?php endif;?>
</td>
</tr>
<?php endif;?>

    <?php if(!empty(esc($expectarea1))):?>
        <tr>
            <th>勤務地（第1希望）</th>
            <td><?php echo esc($expectarea1);?></td>
        </tr>
    <?php endif;?>

    <?php if(!empty(esc($expectarea2))):?>
        <tr>
            <th>勤務地（第2希望）</th>
            <td><?php echo esc($expectarea2);?></td>
        </tr>
    <?php endif;?>

    <?php if(!empty(esc($expectarea3))):?>
        <tr>
            <th>勤務地（第3希望）</th>
            <td><?php echo esc($expectarea3);?></td>
        </tr>
    <?php endif;?>

<?php if(!empty(esc($comment))):?>
<tr>
<th>その他、<br />ご希望などございましたら<br />記入をお願いいたします</th>
<td><?php echo nl2br(esc($comment));?></td>
</tr>
<?php endif;?>


</table>
<!-- start -->
<div class="btnSend">
<form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="mode" value="return">
<input type="hidden" name="shoukai_name" value="<?php echo esc($shoukai_name);?>">
<input type="hidden" name="shoukai_mail" value="<?php echo esc($shoukai_mail);?>">
<input type="hidden" name="shimei" value="<?php echo esc($shimei);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="year" value="<?php echo esc($year);?>">
<input type="hidden" name="month" value="<?php echo esc($month);?>">
<input type="hidden" name="day" value="<?php echo esc($day);?>">
<input type="hidden" name="sex" value="<?php echo esc($sex);?>">
<input type="hidden" name="zip" value="<?php echo esc($zip);?>">
<input type="hidden" name="pref" value="<?php echo esc($pref);?>">
<input type="hidden" name="address" value="<?php echo esc($address);?>">
<input type="hidden" name="tel1" value="<?php echo esc($tel1);?>">
<input type="hidden" name="tel2" value="<?php echo esc($tel2);?>">
<input type="hidden" name="mail1" value="<?php echo esc($mail1);?>">
<input type="hidden" name="mail2" value="<?php echo esc($mail2);?>">
<input type="hidden" name="file1" value="<?php echo esc($file1);?>">
<input type="hidden" name="file2" value="<?php echo esc($file2);?>">
<input type="hidden" name="file3" value="<?php echo esc($file3);?>">
<input type="hidden" name="file4" value="<?php echo esc($file4);?>">
<input type="hidden" name="agree" value="<?php echo esc($agree);?>">

<input type="hidden" name="renraku" value="<?php echo esc($renraku);?>">

<input type="hidden" name="school_div_id" value="<?php echo esc($school_div_id);?>">
<input type="hidden" name="school" value="<?php echo esc($school);?>">
<input type="hidden" name="division" value="<?php echo esc($division);?>">
<input type="hidden" name="zaiseki_year_from" value="<?php echo esc($zaiseki_year_from);?>">
<input type="hidden" name="zaiseki_month_from" value="<?php echo esc($zaiseki_month_from);?>">
<input type="hidden" name="zaiseki_year_to" value="<?php echo esc($zaiseki_year_to);?>">
<input type="hidden" name="zaiseki_month_to" value="<?php echo esc($zaiseki_month_to);?>">
<input type="hidden" name="company_number" value="<?php echo esc($company_number);?>">
<input type="hidden" name="jokyo" value="<?php echo esc($jokyo);?>">
<input type="hidden" name="job1" value="<?php echo esc($job1);?>">
<input type="hidden" name="job2" value="<?php echo esc($job2);?>">
<input type="hidden" name="job3" value="<?php echo esc($job3);?>">


<input type="hidden" name="graduate_kubun" value="<?php echo esc($graduate_kubun);?>">

<input type="hidden" name="company_name" value="<?php echo esc($company_name);?>">
<input type="hidden" name="busho" value="<?php echo esc($busho);?>">
<input type="hidden" name="biz1" value="<?php echo esc($biz1);?>">
<input type="hidden" name="employees" value="<?php echo esc($employees);?>">
<input type="hidden" name="jigyo_naiyo" value="<?php echo esc($jigyo_naiyo);?>">
<input type="hidden" name="income" value="<?php echo esc($income);?>">
<input type="hidden" name="shugyo_keitai" value="<?php echo esc($shugyo_keitai);?>">
<input type="hidden" name="yakushoku" value="<?php echo esc($yakushoku);?>">
<input type="hidden" name="shugyo_year_from" value="<?php echo esc($shugyo_year_from);?>">
<input type="hidden" name="shugyo_month_from" value="<?php echo esc($shugyo_month_from);?>">
<input type="hidden" name="shugyo_year_to" value="<?php echo esc($shugyo_year_to);?>">
<input type="hidden" name="shugyo_month_to" value="<?php echo esc($shugyo_month_to);?>">
<input type="hidden" name="shugyo_naiyo" value="<?php echo esc($shugyo_naiyo);?>">

<input type="hidden" name="company_name2" value="<?php echo esc($company_name2);?>">
<input type="hidden" name="busho2" value="<?php echo esc($busho2);?>">
<input type="hidden" name="biz2" value="<?php echo esc($biz2);?>">
<input type="hidden" name="employees2" value="<?php echo esc($employees2);?>">
<input type="hidden" name="jigyo_naiyo2" value="<?php echo esc($jigyo_naiyo2);?>">
<input type="hidden" name="income2" value="<?php echo esc($income2);?>">
<input type="hidden" name="shugyo_keitai2" value="<?php echo esc($shugyo_keitai2);?>">
<input type="hidden" name="yakushoku2" value="<?php echo esc($yakushoku2);?>">
<input type="hidden" name="shugyo_year_from2" value="<?php echo esc($shugyo_year_from2);?>">
<input type="hidden" name="shugyo_month_from2" value="<?php echo esc($shugyo_month_from2);?>">
<input type="hidden" name="shugyo_year_to2" value="<?php echo esc($shugyo_year_to2);?>">
<input type="hidden" name="shugyo_month_to2" value="<?php echo esc($shugyo_month_to2);?>">
<input type="hidden" name="shugyo_naiyo2" value="<?php echo esc($shugyo_naiyo2);?>">

<input type="hidden" name="company_name3" value="<?php echo esc($company_name3);?>">
<input type="hidden" name="busho3" value="<?php echo esc($busho3);?>">
<input type="hidden" name="biz3" value="<?php echo esc($biz3);?>">
<input type="hidden" name="employees3" value="<?php echo esc($employees3);?>">
<input type="hidden" name="jigyo_naiyo3" value="<?php echo esc($jigyo_naiyo3);?>">
<input type="hidden" name="income3" value="<?php echo esc($income3);?>">
<input type="hidden" name="shugyo_keitai3" value="<?php echo esc($shugyo_keitai3);?>">
<input type="hidden" name="yakushoku3" value="<?php echo esc($yakushoku3);?>">
<input type="hidden" name="shugyo_year_from3" value="<?php echo esc($shugyo_year_from3);?>">
<input type="hidden" name="shugyo_month_from3" value="<?php echo esc($shugyo_month_from3);?>">
<input type="hidden" name="shugyo_year_to3" value="<?php echo esc($shugyo_year_to3);?>">
<input type="hidden" name="shugyo_month_to3" value="<?php echo esc($shugyo_month_to3);?>">
<input type="hidden" name="shugyo_naiyo3" value="<?php echo esc($shugyo_naiyo3);?>">

<input type="hidden" name="sikaku1" value="<?php echo esc($sikaku1);?>">
<input type="hidden" name="sikaku_year1" value="<?php echo esc($sikaku_year1);?>">
<input type="hidden" name="sikaku_month1" value="<?php echo esc($sikaku_month1);?>">
<input type="hidden" name="sikaku2" value="<?php echo esc($sikaku2);?>">
<input type="hidden" name="sikaku_year2" value="<?php echo esc($sikaku_year2);?>">
<input type="hidden" name="sikaku_month2" value="<?php echo esc($sikaku_month2);?>">
<input type="hidden" name="sikaku3" value="<?php echo esc($sikaku3);?>">
<input type="hidden" name="sikaku_year3" value="<?php echo esc($sikaku_year3);?>">
<input type="hidden" name="sikaku_month3" value="<?php echo esc($sikaku_month3);?>">

<input type="hidden" name="conversation" value="<?php echo esc($conversation);?>">
<input type="hidden" name="reading" value="<?php echo esc($reading);?>">
<input type="hidden" name="writing" value="<?php echo esc($writing);?>">
<input type="hidden" name="toeic" value="<?php echo esc($toeic);?>">
<input type="hidden" name="toefl" value="<?php echo esc($toefl);?>">

<input type="hidden" name="other_language1" value="<?php echo esc($other_language1);?>">
<input type="hidden" name="other_language2" value="<?php echo esc($other_language2);?>">

<input type="hidden" name="kibo_biz1" value="<?php echo esc($kibo_biz1);?>">
<input type="hidden" name="kibo_biz2" value="<?php echo esc($kibo_biz2);?>">
<input type="hidden" name="kibo_biz3" value="<?php echo esc($kibo_biz3);?>">
<input type="hidden" name="kibo_job1" value="<?php echo esc($kibo_job1);?>">
<input type="hidden" name="kibo_job2" value="<?php echo esc($kibo_job2);?>">
<input type="hidden" name="kibo_job3" value="<?php echo esc($kibo_job3);?>">
<input type="hidden" name="kibo_pref1" value="<?php echo esc($kibo_pref1);?>">
    <input type="hidden" name="kibo_pref2" value="<?php echo esc($kibo_pref2);?>">
    <input type="hidden" name="kibo_pref3" value="<?php echo esc($kibo_pref3);?>">
<input type="hidden" name="kibo_income" value="<?php echo esc($kibo_income);?>">
<input type="hidden" name="kibo_jiki_year" value="<?php echo esc($kibo_jiki_year);?>">
<input type="hidden" name="kibo_jiki_month" value="<?php echo esc($kibo_jiki_month);?>">
<input type="hidden" name="kibo_jiki_day" value="<?php echo esc($kibo_jiki_day);?>">
<input type="hidden" name="kibou_jiki" value="<?php echo esc($kibou_jiki);?>">

<input type="hidden" name="howto" value="<?php echo esc($howto);?>">

<input type="hidden" name="comment" value="<?php echo esc($comment);?>">

    <input type="hidden" name="expectarea1" value="<?php echo esc($expectarea1);?>">
    <input type="hidden" name="expectarea2" value="<?php echo esc($expectarea2);?>">
    <input type="hidden" name="expectarea3" value="<?php echo esc($expectarea3);?>">

    <input type="hidden" name="job_id" value="<?php echo esc($job_id);?>">
<input type="image" style="float:left;margin-left:100px;height:45px; width:auto;" value="戻る" src="./img/btn_back.gif" alt="戻る" />
</form>
<form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data">
<input name="btnSend" type="image" id="btnSend" style="float:left;margin-left:50px;margin-top:5px;" value="送信する" src="./img/btn_send.gif" alt="上記の内容で送信" />
<input type="hidden" name="mode" value="send">
<input type="hidden" name="shoukai_name" value="<?php echo esc($shoukai_name);?>">
<input type="hidden" name="shoukai_mail" value="<?php echo esc($shoukai_mail);?>">
<input type="hidden" name="shimei" value="<?php echo esc($shimei);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="year" value="<?php echo esc($year);?>">
<input type="hidden" name="month" value="<?php echo esc($month);?>">
<input type="hidden" name="day" value="<?php echo esc($day);?>">
<input type="hidden" name="sex" value="<?php echo esc($sex);?>">
<input type="hidden" name="zip" value="<?php echo esc($zip);?>">
<input type="hidden" name="pref" value="<?php echo esc($pref);?>">
<input type="hidden" name="address" value="<?php echo esc($address);?>">
<input type="hidden" name="tel1" value="<?php echo esc($tel1);?>">
<input type="hidden" name="tel2" value="<?php echo esc($tel2);?>">
<input type="hidden" name="mail1" value="<?php echo esc($mail1);?>">
<input type="hidden" name="mail2" value="<?php echo esc($mail2);?>">
<input type="hidden" name="file1" value="<?php echo esc($file1);?>">
<input type="hidden" name="file2" value="<?php echo esc($file2);?>">
<input type="hidden" name="file3" value="<?php echo esc($file3);?>">
<input type="hidden" name="file4" value="<?php echo esc($file4);?>">
<input type="hidden" name="agree" value="<?php echo esc($agree);?>">

<input type="hidden" name="renraku" value="<?php echo esc($renraku);?>">

<input type="hidden" name="school_div_id" value="<?php echo esc($school_div_id);?>">
<input type="hidden" name="school" value="<?php echo esc($school);?>">
<input type="hidden" name="division" value="<?php echo esc($division);?>">
<input type="hidden" name="zaiseki_year_from" value="<?php echo esc($zaiseki_year_from);?>">
<input type="hidden" name="zaiseki_month_from" value="<?php echo esc($zaiseki_month_from);?>">
<input type="hidden" name="zaiseki_year_to" value="<?php echo esc($zaiseki_year_to);?>">
<input type="hidden" name="zaiseki_month_to" value="<?php echo esc($zaiseki_month_to);?>">
<input type="hidden" name="company_number" value="<?php echo esc($company_number);?>">
<input type="hidden" name="jokyo" value="<?php echo esc($jokyo);?>">
<input type="hidden" name="job1" value="<?php echo esc($job1);?>">
<input type="hidden" name="job2" value="<?php echo esc($job2);?>">
<input type="hidden" name="job3" value="<?php echo esc($job3);?>">


<input type="hidden" name="graduate_kubun" value="<?php echo esc($graduate_kubun);?>">

<input type="hidden" name="company_name" value="<?php echo esc($company_name);?>">
<input type="hidden" name="busho" value="<?php echo esc($busho);?>">
<input type="hidden" name="biz1" value="<?php echo esc($biz1);?>">
<input type="hidden" name="employees" value="<?php echo esc($employees);?>">
<input type="hidden" name="jigyo_naiyo" value="<?php echo esc($jigyo_naiyo);?>">
<input type="hidden" name="income" value="<?php echo esc($income);?>">
<input type="hidden" name="shugyo_keitai" value="<?php echo esc($shugyo_keitai);?>">
<input type="hidden" name="yakushoku" value="<?php echo esc($yakushoku);?>">
<input type="hidden" name="shugyo_year_from" value="<?php echo esc($shugyo_year_from);?>">
<input type="hidden" name="shugyo_month_from" value="<?php echo esc($shugyo_month_from);?>">
<input type="hidden" name="shugyo_year_to" value="<?php echo esc($shugyo_year_to);?>">
<input type="hidden" name="shugyo_month_to" value="<?php echo esc($shugyo_month_to);?>">
<input type="hidden" name="shugyo_naiyo" value="<?php echo esc($shugyo_naiyo);?>">

<input type="hidden" name="company_name2" value="<?php echo esc($company_name2);?>">
<input type="hidden" name="busho2" value="<?php echo esc($busho2);?>">
<input type="hidden" name="biz2" value="<?php echo esc($biz2);?>">
<input type="hidden" name="employees2" value="<?php echo esc($employees2);?>">
<input type="hidden" name="jigyo_naiyo2" value="<?php echo esc($jigyo_naiyo2);?>">
<input type="hidden" name="income2" value="<?php echo esc($income2);?>">
<input type="hidden" name="shugyo_keitai2" value="<?php echo esc($shugyo_keitai2);?>">
<input type="hidden" name="yakushoku2" value="<?php echo esc($yakushoku2);?>">
<input type="hidden" name="shugyo_year_from2" value="<?php echo esc($shugyo_year_from2);?>">
<input type="hidden" name="shugyo_month_from2" value="<?php echo esc($shugyo_month_from2);?>">
<input type="hidden" name="shugyo_year_to2" value="<?php echo esc($shugyo_year_to2);?>">
<input type="hidden" name="shugyo_month_to2" value="<?php echo esc($shugyo_month_to2);?>">
<input type="hidden" name="shugyo_naiyo2" value="<?php echo esc($shugyo_naiyo2);?>">

<input type="hidden" name="company_name3" value="<?php echo esc($company_name3);?>">
<input type="hidden" name="busho3" value="<?php echo esc($busho3);?>">
<input type="hidden" name="biz3" value="<?php echo esc($biz3);?>">
<input type="hidden" name="employees3" value="<?php echo esc($employees3);?>">
<input type="hidden" name="jigyo_naiyo3" value="<?php echo esc($jigyo_naiyo3);?>">
<input type="hidden" name="income3" value="<?php echo esc($income3);?>">
<input type="hidden" name="shugyo_keitai3" value="<?php echo esc($shugyo_keitai3);?>">
<input type="hidden" name="yakushoku3" value="<?php echo esc($yakushoku3);?>">
<input type="hidden" name="shugyo_year_from3" value="<?php echo esc($shugyo_year_from3);?>">
<input type="hidden" name="shugyo_month_from3" value="<?php echo esc($shugyo_month_from3);?>">
<input type="hidden" name="shugyo_year_to3" value="<?php echo esc($shugyo_year_to3);?>">
<input type="hidden" name="shugyo_month_to3" value="<?php echo esc($shugyo_month_to3);?>">
<input type="hidden" name="shugyo_naiyo3" value="<?php echo esc($shugyo_naiyo3);?>">

<input type="hidden" name="sikaku1" value="<?php echo esc($sikaku1);?>">
<input type="hidden" name="sikaku_year1" value="<?php echo esc($sikaku_year1);?>">
<input type="hidden" name="sikaku_month1" value="<?php echo esc($sikaku_month1);?>">
<input type="hidden" name="sikaku2" value="<?php echo esc($sikaku2);?>">
<input type="hidden" name="sikaku_year2" value="<?php echo esc($sikaku_year2);?>">
<input type="hidden" name="sikaku_month2" value="<?php echo esc($sikaku_month2);?>">
<input type="hidden" name="sikaku3" value="<?php echo esc($sikaku3);?>">
<input type="hidden" name="sikaku_year3" value="<?php echo esc($sikaku_year3);?>">
<input type="hidden" name="sikaku_month3" value="<?php echo esc($sikaku_month3);?>">

<input type="hidden" name="conversation" value="<?php echo esc($conversation);?>">
<input type="hidden" name="reading" value="<?php echo esc($reading);?>">
<input type="hidden" name="writing" value="<?php echo esc($writing);?>">
<input type="hidden" name="toeic" value="<?php echo esc($toeic);?>">
<input type="hidden" name="toefl" value="<?php echo esc($toefl);?>">

<input type="hidden" name="other_language1" value="<?php echo esc($other_language1);?>">
<input type="hidden" name="other_language2" value="<?php echo esc($other_language2);?>">

<input type="hidden" name="kibo_biz1" value="<?php echo esc($kibo_biz1);?>">
<input type="hidden" name="kibo_biz2" value="<?php echo esc($kibo_biz2);?>">
<input type="hidden" name="kibo_biz3" value="<?php echo esc($kibo_biz3);?>">
<input type="hidden" name="kibo_job1" value="<?php echo esc($kibo_job1);?>">
<input type="hidden" name="kibo_job2" value="<?php echo esc($kibo_job2);?>">
<input type="hidden" name="kibo_job3" value="<?php echo esc($kibo_job3);?>">
<input type="hidden" name="kibo_pref1" value="<?php echo esc($kibo_pref1);?>">
    <input type="hidden" name="kibo_pref2" value="<?php echo esc($kibo_pref2);?>">
    <input type="hidden" name="kibo_pref3" value="<?php echo esc($kibo_pref3);?>">
<input type="hidden" name="kibo_income" value="<?php echo esc($kibo_income);?>">
<input type="hidden" name="kibo_jiki_year" value="<?php echo esc($kibo_jiki_year);?>">
<input type="hidden" name="kibo_jiki_month" value="<?php echo esc($kibo_jiki_month);?>">
<input type="hidden" name="kibo_jiki_day" value="<?php echo esc($kibo_jiki_day);?>">
<input type="hidden" name="kibou_jiki" value="<?php echo esc($kibou_jiki);?>">

<input type="hidden" name="howto" value="<?php echo esc($howto);?>">

<input type="hidden" name="comment" value="<?php echo esc($comment);?>">
    <input type="hidden" name="expectarea1" value="<?php echo esc($expectarea1);?>">
    <input type="hidden" name="expectarea2" value="<?php echo esc($expectarea2);?>">
    <input type="hidden" name="expectarea3" value="<?php echo esc($expectarea3);?>">
<input type="hidden" name="job_id" value="<?php echo esc($job_id);?>">
</form>
</div>
<p><strong><span class="txtRed">※</span></strong>Amazon.co.jpは、本キャンペーンのスポンサーではありません。<br />
<strong><span class="txtRed">※</span></strong>Amazon、Amazon.co.jpおよびそのロゴはAmazon.com,Inc.またはその関連会社の商標です。</p>

</div>
<!-- page end -->
</div>
<div id="coR">
<?php $Path = "../"; include(dirname(__FILE__).'/../include/right.html'); ?>
</div>
</div>
<!-- co end -->
<?php $Path = "../"; include(dirname(__FILE__).'/../include/footer.html'); ?>
<!-- リマーケティング タグの Google コード -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963710423;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963710423/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- リマーケティングタグ Yahoo -->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=1Wjm96V";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
  <iframe src="//b.yjtag.jp/iframe?c=1Wjm96V" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script> <script type="text/javascript"> twttr.conversion.trackPid('l5c0l');</script>
<noscript>
<img height="1" width="1" style="display:none;" alt=""
src="https://analytics.twitter.com/i/adsct?txn_id=l5c0l&p_id=Twitter" /> <img height="1" width="1" style="display:none;" alt=""
src="//t.co/i/adsct?txn_id=l5c0l&p_id=Twitter" /></noscript>

<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'W5O5K72Z6Z';
var yahoo_retargeting_label = '';
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>


<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>