<?php
//入力ページの設定
define('Index', 'form.html');
//確認ページの設定（使わない場合はこのまま）
define('Confirm', 'confirm.php');
//完了ページの設定
define('Finish', 'finish.php');

define('ENCODE', 'UTF-8');

//ユーザの名前設定
$unit_name = $shimei . "様";
//ユーザの宛先設定
if(!empty($mail1)){
	$email = $mail1;
}else{
	$email = $mail2;
}
$today = Date('Y年m月d日');
$send_mail="touroku@hurex.co.jp";
//$send_mail="tosa@smt-net.co.jp";

$birthday = $year . "年" . $month . "月" . $day . "日";
/*
$zaiseki = $zaiseki_year_from . "年" . $zaiseki_month_from . "月～" . $zaiseki_year_to . "年" . $zaiseki_month_to . "月";
$shugyo = $shugyo_year_from."年" .$shugyo_month_from . "月～" . $shugyo_year_to . "年" . $shugyo_month_to . "月";
$shugyo2 = $shugyo_year_from2."年" .$shugyo_month_from2 . "月～" . $shugyo_year_to2 . "年" . $shugyo_month_to2 . "月";
$shugyo3 = $shugyo_year_from3."年" .$shugyo_month_from3 . "月～" . $shugyo_year_to3 . "年" . $shugyo_month_to3 . "月";
*/
/*
$job1_mail = $job["$job1"];
$job2_mail = $job["$job2"];
$job3_mail = $job["$job3"];
$biz1_mail = $biz["$biz1"];
$biz2_mail = $biz["$biz2"];
$biz3_mail = $biz["$biz3"];
$kibo_biz1_mail = $biz["$kibo_biz1"];
$kibo_biz2_mail = $biz["$kibo_biz2"];
$kibo_biz3_mail = $biz["$kibo_biz3"];
$kibo_job1_mail = $job["$kibo_job1"];
$kibo_job2_mail = $job["$kibo_job2"];
$kibo_job3_mail = $job["$kibo_job3"];
*/

//サンクスメール設定
$thanks_mail_subject = "転職支援サービスへの申し込みありがとうございます。【ヒューレックス株式会社】";
$thanks_mail_from = "touroku@hurex.co.jp";
//$thanks_mail_from = "tosa@smt-net.co.jp";
$thanks_mail_str = "ヒューレックス株式会社";
$thanks_body= <<< EOF
$shimei 様

この度はヒューレックス転職支援サービスへご登録いただき、
誠にありがとうございます。

内容確認の上、担当よりご連絡いたします。

弊社では、個人情報保護の観点から、個人情報に関する適切な取り扱い
および管理に努めております。
ご登録頂いた個人情報は、職業紹介の目的にのみ使用し
他の目的で使用することはございません。

─────────────────────────────□■─
　このメールに関するお問い合わせは、人材紹介サービス担当まで
────────────────────────────────
ヒューレックス株式会社
〒980-6117 宮城県仙台市青葉区中央1-3-1 アエル17階
TEL 0120-14-1150
――――――――――――――――――――――――――――――――

このメール配信に心当たりのない方は、
メールアドレスが誤って入力された可能性がありますので、
破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。
ご返信いただいてもご回答できかねますので、ご了承ください。

EOF;

//subject用
$now = date("Ymd");
$birth = $year . sprintf('%02d', $month) . sprintf('%02d', $day);
$age = floor(($now-$birth)/10000);
$tmp_div="";
if($school_div_id == "大学院卒（博士）"){
	$tmp_div = "院卒博";
}else if($school_div_id =="大学院卒（修士）"){
	$tmp_div = "院卒修";
}else if($school_div_id =="大学卒"){
	$tmp_div = "大学卒";
}else if($school_div_id =="高専卒"){
	$tmp_div = "高専卒";
}else if($school_div_id =="短大卒"){
	$tmp_div = "短大卒";
}else if($school_div_id =="専門各種学校卒"){
	$tmp_div = "専門卒";
}else if($school_div_id =="その他"){
	$tmp_div = "その他";
}else if($school_div_id =="不問"){
}


//通知メール設定
$order_mail_subject = "【お知り合い紹介キャンペーン(PC)】 " . $age ."歳 " . $company_number . "社 " . $tmp_div . " " . $sex . " "  . $shimei   ;
$order_mail_to[] = array('touroku@hurex.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('tosa@smt-net.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('tosa@smt-net.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('info@smt-net.co.jp', '追加宛先');
$order_body= <<< EOF
ご担当者様
ホームページより、 $shimei 様のエントリーがありました。
HRBCにログインし、エントリー情報をご確認下さい。

[電話番号]
$tel1

[メールアドレス]
$mail1

EOF;

//項目チェック用
$validation->set_rules('agree','個人情報保護方針','required');
$validation->set_rules('shimei','氏名','required|max_length[25]');
//$validation->set_rules('kana','フリガナ','required|max_length[25]');
$validation->set_rules('year','年','required');
$validation->set_rules('month','月','required');
$validation->set_rules('day','日','required');
$validation->set_rules('sex','性別','required');
//$validation->set_rules('zip','郵便番号','required|max_length[8]|alpha_dash');
$validation->set_rules('pref','都道府県','required');
//$validation->set_rules('address','住所','required|max_length[50]');
$validation->set_rules('tel1','電話1','max_length[15]');
$validation->set_rules('tel2','電話2','max_length[15]');
$validation->set_rules('mail1','メール1','valid_email|max_length[50]');
$validation->set_rules('mail2','メール2','valid_email|max_length[50]');
//$validation->set_rules('renraku','連絡事項','max_length[10000]');
$validation->set_rules('school_div_id','最終学歴','');
//$validation->set_rules('school','学校名','max_length[30]');
//$validation->set_rules('division','学部・学科名','max_length[30]');
//$validation->set_rules('zaiseki_year_from','在籍年','');
//$validation->set_rules('zaiseki_year_from','在籍月','');
//$validation->set_rules('zaiseki_month_to','在籍年','');
//$validation->set_rules('zaiseki_month_from','在籍月','');
$validation->set_rules('company_number','経験社数','');
//$validation->set_rules('jokyo','就業状況','');
$validation->set_rules('job1','経験職種','');
//$validation->set_rules('jigyo_naiyo','事業内容','max_length[10000]');
//$validation->set_rules('jigyo_naiyo2','事業内容2','max_length[10000]');
//$validation->set_rules('jigyo_naiyo3','事業内容3','max_length[10000]');
$validation->set_rules('company_name','企業名','');
$validation->set_rules('biz1','業種','');
$validation->set_rules('income','年収','');
$validation->set_rules('shugyo_keitai','就業形態','');
$validation->set_rules('expectarea1','勤務地（第1希望','required');
$validation->set_rules('expectarea2','勤務地（第2希望','');
$validation->set_rules('expectarea3','勤務地（第3希望','');
//$validation->set_rules('yakushoku','役職','');
/*
$validation->set_rules('shugyo_year_from','就業年','');
$validation->set_rules('shugyo_month_from','就業月','');
$validation->set_rules('shugyo_year_to','就業年','');
$validation->set_rules('shugyo_month_to','就業月','');
$validation->set_rules('shugyo_naiyo','就業内容','max_length[10000]');
$validation->set_rules('shugyo_naiyo2','就業内容2','max_length[10000]');
$validation->set_rules('shugyo_naiyo3','就業内容3','max_length[10000]');
$validation->set_rules('kibo_biz1','希望業種','');
*/
$validation->set_rules('comment','その他','max_length[10000]');
$validation->set_rules('kibo_job1','希望職種','');
//$validation->set_rules('kibo_pref1','希望勤務地','required');
$validation->set_rules('kibo_income','希望年収','');
$validation->set_rules('howto','弊社を知ったきっかけ','');
$validation->set_rules('toefl','TOEFL','numeric');
$validation->set_rules('toeic','TOEIC','numeric');
$validation->set_rules('kibo_income','希望年収','numeric');



//ログファイル出力 on or off
$log = "off";

?>