<?php
    require_once 'UtilLogger.php';

    class SendMail
    {
        const ERROR_TO_ADDRESS        = "fujiura@fits-inc.jp";
        const ERROR_FROM_ADDRESS      = "syserr";
        const SALESFORECE_ERROR_TITLE = "[HRBC->Salesforce] Salesforce upsert error occured.";
        const HRBC_ERROR_TITLE        = "[HRBC->Salesforce] HRBC read error occured.";
        const OTHER_ERROR_TITLE       = "[HRBC->Salesforce] Unknown error occured.";

        public static function sendSalesforceError(string $message)
        {
            self::sendError(self::SALESFORCE_ERROR_TITLE, $message);
        }

        public static function sendHrbcError(string $message)
        {
            self::sendError(self::HRBC_ERROR_TITLE, $message);
        }

        public static function sendOtherError(string $message)
        {
            self::sendError(self::OTHER_ERROR_TITLE, $message);
        }

        public static function sendError(string $title, string $message)
        {
            $errorFromAddress = self::ERROR_FROM_ADDRESS;
            $headers = "From: $errorFromAddress\r\nReturn-Path: $errorFromAddress";
            self::send(self::ERROR_TO_ADDRESS, $title, $message, $headers);
        }

        /**
         * メールを送信する
         *
         * @param string $address 送信先メールアドレス
         * @param string $title   メールタイトル
         * @param string $headers メールヘッダー
         * @param string $message メール本文
         * @return void
         */
        public static function send(string $address, string $title, string $message, string $headers): void
        {
            mb_send_mail($address, $title, $message, $headers);
        }
    }
