<?php
    require_once "UtilLogger.php";
    require_once 'SendMail.php';

    class UtilCurl
    {
        /**
         * Curl 実行のエラーを検知してメールを送信する
         *
         * @param $handle Curl のハンドル
         * @return void
         */
        public function checkError($handle)
        {
            //エラー検証
            if (curl_errno($handle)) {
                $message = "ErrorNo: " . curl_errno($handle) . " ErrorMessage: " . curl_error($handle);
                UtilLogger::error($message);
                SendMail::sendOtherError($message);
            }
        }

        /**
         * curl を使用して指定URLからXMLを取得する
         *
         * @param string $url リクエスト先URL
         * @param array $header リクエスト時に付与するheader
         * @return SimpleXMLElement 取得したレスポンスのXMLオブジェクト
         */
        public function getXml(string $url, array $header = []) : SimpleXMLElement
        {
            UtilLogger::debug("getXml {$url}");
            
            $handle = curl_init();
            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);  // 戻り値を文字列で
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false); // 証明無視
            if (count($header) > 0) {
                curl_setopt($handle, CURLOPT_HTTPHEADER, $header);
            }
            $response = curl_exec($handle);
            // エラー検証
            self::checkError($handle);
            curl_close($handle);

            // xml パース
            $xml = simplexml_load_string($response);
            return $xml;
        }

        /**
          * curl を使用して指定URLからテキストを取得する
         *
         * @param string $url リクエスト先URL
         * @param array $header リクエスト時に付与するheader
         * @return String
         */
        public function getText(string $url, array $header = []) : String
        {
            UtilLogger::debug("getText {$url}");

            $handle = curl_init();
            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);  // 戻り値を文字列で
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false); // 証明無視
            if (count($header) > 0) {
                curl_setopt($handle, CURLOPT_HTTPHEADER, $header);
            }
            $response = curl_exec($handle);
            self::checkError($handle);
            curl_close($handle);

            return $response;
        }

        /**
         * curl を使用して指定URLからXMLを取得する
         *
         * @param string $url リクエスト先URL
         * @param string $body リクエスト時に付与するbody
         * @param array $header リクエスト時に付与するheader
         * @return DOMDocument 取得したレスポンスのXMLオブジェクト
         */
        public function postRequest(string $url, string $body, array $header = []): DOMDocument
        {
            UtilLogger::debug("postRequest {$url}");

            $handle = curl_init();
            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_POST, true);
            curl_setopt($handle, CURLOPT_POSTFIELDS, $body);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true); //戻り値を文字列で
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false); //証明無視

            if (count($header) > 0) {
                curl_setopt($handle, CURLOPT_HTTPHEADER, $header);
            }
            $response = curl_exec($handle);
            //エラー検証
            self::checkError($handle);
            curl_close($handle);

            $dom = new DOMDocument;
            $dom->loadXML($response);
            return $dom;
        }
    }
