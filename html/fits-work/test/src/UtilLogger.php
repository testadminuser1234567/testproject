<?php
    class UtilLogger
    {
        const LEVEL_ERROR = 0;
        const LEVEL_WARN  = 1;
        const LEVEL_INFO  = 2;
        const LEVEL_DEBUG = 3;

        private static $defaultLevel = self::LEVEL_DEBUG;

        /**
         * ログを出力する
         *
         * @param string $message
         * @param integer $level
         * @return void
         */
        private static function outputLog(string $message, int $level)
        {
            if ($level > self::$defaultLevel) {
                // ログを出力しない
                return;
            }

            $label = [
                self::LEVEL_ERROR => '[ERROR]',
                self::LEVEL_WARN =>  '[WARN]',
                self::LEVEL_INFO =>  '[INFO]',
                self::LEVEL_DEBUG => '[DEBUG]',
            ][$level];

            $filename = date("Ymd") . '.log';

            date_default_timezone_set('Asia/Tokyo');
            $now = new DateTimeImmutable();
            $timestamp = $now->format("H:i:s.v");
            $logString = "{$timestamp} {$label} {$message}\n";
            file_put_contents("../log/" . $filename, $logString, FILE_APPEND);
        }

        /**
         * ERROR ログを出力する
         *
         * @param string $message
         * @return void
         */
        public static function error(string $message)
        {
            self::outputLog($message, self::LEVEL_ERROR);
        }

        /**
         * WARN ログを出力する
         *
         * @param string $message
         * @return void
         */
        public static function warn(string $message)
        {
            self::outputLog($message, self::LEVEL_WARN);
        }

        /**
         * INFO ログを出力する
         *
         * @param string $message
         * @return void
         */
        public static function info(string $message)
        {
            self::outputLog($message, self::LEVEL_INFO);
        }

        /**
         * DEBUG ログを出力する
         *
         * @param string $message
         * @return void
         */
        public static function debug(string $message)
        {
            self::outputLog($message, self::LEVEL_DEBUG);
        }
    }
