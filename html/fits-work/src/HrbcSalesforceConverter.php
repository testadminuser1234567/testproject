<?php
    require_once 'UtilCsv.php';
    require_once 'UtilCurl.php';
    require_once 'UtilLogger.php';
    require_once 'SendMail.php';

    class HrbcSalesforceConverter
    {
        const APP_ID = "725d6f07ddaede98231a01f7783a78ef";
        const SECRET = "9ea5f52d29da05f55f9fee47cd0f55b9bba28ebe9c64d8d3a51bdc7a0d11713d";
        const HRBC_URL = "https://api-hrbc-jp.porterscloud.com/";
        const OAUTH_URL = "v1/oauth";
        const TOKEN_URL = "v1/token";
        const PROCESS_URL = "v1/process";
        const RESUME_URL = "v1/resume";

        // データが 200件以上ある場合に、次にリクエストする件数
        // 200件 より少なくすることで、次のリクエストまでに更新された場合の取りこぼしを防ぐ
        const NEXT_REQUEST_RANGE = 190;

        // リクエスト処理を担当する UtilCurl クラスのインスタンスにまかせる
        private $curl;

        /**
         * コンストラクタ
         */
        public function __construct()
        {
            $this->curl = new UtilCurl();
        }

        /**
         * パラメータつきURLを生成する
         *
         * @param string $baseUrl ベースとなるURL
         * @param string $apiUrl APIごとのURL
         * @param array $param APIリクエスト時のパラメータ
         * @return string
         */
        public function buildUrl(string $baseUrl, string $apiUrl, array $param) : string
        {
            if (count($param) > 0) {
                // HRBCのパラメータ仕様に従い、エンコードしない
                return urldecode("{$baseUrl}{$apiUrl}?" . http_build_query($param));
            } else {
                return "{$baseUrl}{$apiUrl}";
            }
        }

        /**
         * OAUTHリクエストのURL生成
         *
         * @param array $param リクエストパラメータ
         * @return string
         */
        public function buildOauthUrl(array $param) : string
        {
            return self::buildUrl(self::HRBC_URL, self::OAUTH_URL, $param);
        }

        /**
         * TOKENリクエストのURL生成
         *
         * @param array $param リクエストパラメータ
         * @return string
         */
        public function buildTokenUrl(array $param):string
        {
            return self::buildUrl(self::HRBC_URL, self::TOKEN_URL, $param);
        }

        /**
         * プロセスリクエストのURL生成
         *
         * @param array $param リクエストパラメータ
         * @return string
         */
        public function buildProcessUrl(array $param):string
        {
            return self::buildUrl(self::HRBC_URL, self::PROCESS_URL, $param);
        }

        /**
         * レジュメリクエストのURL生成
         *
         * @param array $param リクエストパラメータ
         * @return string
         */
        public function buildResumeUrl(array $param):string
        {
            return self::buildUrl(self::HRBC_URL, self::RESUME_URL, $param);
        }

        /**
         * メイン処理
         *
         * @return int 処理対象 ID の件数
         */
        public function run() : int
        {
            date_default_timezone_set('Asia/Tokyo');
            // データの取得範囲（現在時刻を基準とし、65分前から5分後）を計算しておく
            $date = new DateTimeImmutable();
            // のちにURLエンコードを行わないため、スペースは%20にエンコードしておく
            $encodeSpace = "%20";
            // HRBCはAPI経由だと9時間前の時刻で可動するため、リクエスト時に9時間マイナスオフセットを設定する
            $hrbcOffset = new DateInterval("PT9H");
            $startDate = $date->sub($hrbcOffset)->sub(new DateInterval("PT65M"))->format("Y/m/d{$encodeSpace}H:i:s");
            $endDate = $date->sub($hrbcOffset)->add(new DateInterval("PT5M"))->format("Y/m/d{$encodeSpace}H:i:s");

            // OAUTH のコードを取得し、コードからトークンを取得してヘッダを生成
            $code = self::getOauthCode(self::APP_ID);
            $token = self::getOauthToken(self::APP_ID, self::SECRET, $code);
            $headers = [
                "X-porters-hrbc-oauth-token:{$token}"
            ];

            // 直近1時間に更新があったレジュメ、プロセスに紐づく ID のリストを取得
            $idFromResume = self::getResumeIdFromResume($startDate, $endDate, $headers);
            UtilLogger::info("ID from resume count is ". count($idFromResume));
            $idFromProcess = self::getResumeIdFromProcess($startDate, $endDate, $headers);
            UtilLogger::info("ID from process count is ". count($idFromProcess));
            if (count($idFromResume) === 0 && count($idFromProcess) === 0) {
                UtilLogger::info("Convert is stopped because ID list is empty");
                return 0;
            }

            // 2つのIDリストをマージして重複を削除
            $uniqueIdList = array_values(array_unique(array_merge($idFromProcess, $idFromResume)));

            // 大量データの場合は HRBC がエラーを返すため、2000件ずつに分割して処理する
            // TODO:メソッドへの切り出しを検討
            $resume = [];
            $process = [];
            $offset = 0;
            for ($offset = 0; $offset < count($uniqueIdList) + 2000; $offset += 2000) {
                // 2000 件ずつ ID を切り出す
                $subIdList = array_slice($uniqueIdList, $offset, 2000);
                $uniqueIdParameter = implode($subIdList, ":");
                // レジュメを取得
                $resume = array_merge($resume, self::getResume($uniqueIdParameter, $headers));
                // プロセスを取得
                $process = array_merge($process, self::getProcess($uniqueIdParameter, $headers));
            }
            // CSVデータを出力
            self::exportCsv($uniqueIdList, $resume, $process);
            return count($uniqueIdList);
        }

        /**
         * レジュメIDに紐づくレジュメデータを取得する
         *
         * @param string $id
         * @param array $resumeList レジュメデータのリスト
         * @return array レジュメデータ
         */
        public function getResumeById(string $id, array $resumeList) : array
        {
            foreach ($resumeList as $resume) {
                if ($resume["id"] === $id) {
                    return $resume;
                }
            }
            return [];
        }

        /**
         * レジュメIDに紐づくプロセスをリストから複数を取得する
         *
         * @param string $id
         * @param array $processList
         * @return array プロセスの 2次元配列
         */
        public function getProcessById(string $id, array $processList) : array
        {
            // 条件に該当するプロセスのリスト
            $matchProcess = [];
            // 該当すればリストに追加する
            foreach ($processList as $process) {
                if ($process["id"] === $id) {
                    $matchProcess[] = $process;
                }
            }
            return $matchProcess;
        }

        /**
         * CSVデータを出力する
         *
         * @param [type] $idList
         * @param [type] $resumeList
         * @param [type] $processList
         * @return void
         */
        public function exportCsv($idList, $resumeList, $processList)
        {
            $fp = fopen(UtilCsv::CSV_PATH, "w");
            if ($fp) {
                // ヘッダを出力
                fputcsv($fp, [
                    "hrbcResumeId__C",
                    "hrbcResumeMailmag__C",
                    "LastName",
                    "Company",
                    "hrbcPrefecture__C",
                    "hrbcResumePhase__C",
                    "hrbcResumePhaseDate__C",
                    "Email",
                    "hrbcResumeInterviewer__C",
                    "hrbcResumeBarker__C",
                    "hrbcResumeComprehensiveEvaluation__C",
                    "hrbcResumeUrgency__C",
                    "hrbcResumeInterviewDate__C",
                    "hrbcResumeDesiredWorkRegion__C",
                    "hrbcResumeMyPageRegistrationDate__C",
                    "hrbcProcessClient1__C",
                    "hrbcProcessJobPosition1__C",
                    "hrbcProcessJobPhase1__C",
                    "hrbcProcessPhaseDate1__C",
                    "hrbcProcessPhase1__C",
                    "hrbcProcessMypage1__C",
                    "hrbcProcessJobId1__C",
                    "hrbcProcessClient2__C",
                    "hrbcProcessJobPosition2__C",
                    "hrbcProcessJobPhase2__C",
                    "hrbcProcessPhaseDate2__C",
                    "hrbcProcessPhase2__C",
                    "hrbcProcessMypage2__C",
                    "hrbcProcessJobId2__C",
                    "hrbcProcessClient3__C",
                    "hrbcProcessJobPosition3__C",
                    "hrbcProcessJobPhase3__C",
                    "hrbcProcessPhaseDate3__C",
                    "hrbcProcessPhase3__C",
                    "hrbcProcessMypage3__C",
                    "hrbcProcessJobId3__C",
                    "hrbcTempUpperPhase__C",
                ]);

                // CSV の中身を出力
                foreach ($idList as $id) {
                    $resume = self::getResumeById($id, $resumeList);
                    if (count($resume) < 1) {
                        UtilLogger::warn("resume is empty");
                        continue;
                    }
                    $process = self::getProcessById($id, $processList);
                    fputcsv($fp, [
                        $id,
                        self::getMailMagaText($resume["mailmag"]),
                        $resume["name"],
                        "Company",
                        $resume["prefecture"],
                        $resume["phase"],
                        self::DateTimeExchangeISO8601Format(self::hrbcOffset($resume["phasedate"])),
                        $resume["mail"],
                        $resume["interviewer"],
                        $resume["barker"],
                        $resume["comprehensiveevaluation"],
                        $resume["urgency"],
                        self::DateExchangeISO8601Format($resume["interviewdate"]),
                        $resume["desiredworkregion"],
                        self::DateTimeExchangeISO8601Format(self::hrbcOffset($resume["mypageregistrationdate"])),
                        // プロセスは 0 件以上ある場合のみ値を取得
                        count($process) > 0 ? $process[0]["client"] : "",
                        count($process) > 0 ? $process[0]["job"] : "",
                        count($process) > 0 ? $process[0]["jobphase"] : "",
                        count($process) > 0 ? self::DateTimeExchangeISO8601Format(self::hrbcOffset($process[0]["phasedate"])) : "",
                        count($process) > 0 ? $process[0]["phase"] : "",
                        count($process) > 0 ? $process[0]["mypage"] : "",
                        count($process) > 0 ? $process[0]["jobid"] : "",
                        count($process) > 1 ? $process[1]["client"] : "",
                        count($process) > 1 ? $process[1]["job"] : "",
                        count($process) > 1 ? $process[1]["jobphase"] : "",
                        count($process) > 1 ? self::DateTimeExchangeISO8601Format(self::hrbcOffset($process[1]["phasedate"])) : "",
                        count($process) > 1 ? $process[1]["phase"] : "",
                        count($process) > 1 ? $process[1]["mypage"] : "",
                        count($process) > 1 ? $process[1]["jobid"] : "",
                        count($process) > 2 ? $process[2]["client"] : "",
                        count($process) > 2 ? $process[2]["job"] : "",
                        count($process) > 2 ? $process[2]["jobphase"] : "",
                        count($process) > 2 ? self::DateTimeExchangeISO8601Format(self::hrbcOffset($process[2]["phasedate"])) : "",
                        count($process) > 2 ? $process[2]["phase"] : "",
                        count($process) > 2 ? $process[2]["mypage"] : "",
                        count($process) > 2 ? $process[2]["jobid"] : "",
                        self::getTempUpperPhase($process),
                    ]);
                }
            }
            fclose($fp);
        }

        /**
         * エラーの有無を確認し、エラーがあればメールを送信する
         */
        public function checkError(SimpleXMLElement $xml) {
            if ($xml->Error !== null && (string)$xml->Error !== "" && (string)$xml->Error !== "0") {
                $errorCode = $xml->Error;
                $message = $xml->Message;
                SendMail::sendHrbcError("HRBCからのデータ取得でエラーが発生しました\nコード：${errorCode}\nエラーメッセージ：${message}");
            }
        }

        /**
         * OAUTH認証用のコードを取得
         *
         * @param string $appId
         * @return string 認証用のコード
         */
        public function getOauthCode(string $appId) : string
        {
            $url = self::buildOauthUrl([
                "response_type" => "code_direct",
                "app_id" => $appId,
            ]);
            $response = $this->curl->getXml($url);
            self::checkError($response);
            $code = (string)$response->Code;
            return $code;
        }

        /**
         * OAUTH認証用のトークンを取得
         *
         * @param string $appId
         * @param string $secret
         * @param string $code
         * @return string トークン情報
         */
        public function getOauthToken(string $appId, string $secret, string $code) : string
        {
            $url = self::buildTokenUrl([
                "app_id" => $appId,
                "secret" => $secret,
                "grant_type" => "oauth_code",
                "code" => $code,
            ]);
            $response = $this->curl->getXml($url);
            self::checkError($response);
            $token = (string)$response->AccessToken;
            return $token;
        }

        /**
         * 一定期間以内に更新されたレジュメのIDを取得
         *
         * @param [type] $startDate 期間の開始日時
         * @param [type] $endDate 期間の終了日時
         * @param [type] $headers リクエストヘッダ
         * @return array レジュメのIDのリスト
         */
        public function getResumeIdFromResume($startDate, $endDate, $headers) : array
        {
            $idFromResume = [];

            // TODO:更新日時で並び替えする
            $url = self::buildResumeUrl([
                "partition" => "2012",
                "field" => "Resume.P_Id",
                "count" => "200",
                "condition"  => "Resume.P_UpdateDate:ge={$startDate},Resume.P_UpdateDate:le={$endDate},",
            ]);

            $start = 0;
            // 取得件数が200件の場合は、それ以上のデータが存在する可能性があるため
            // 再度リクエストを行う
            do {
                $response = $this->curl->getXml($url . "&start={$start}", $headers);
                self::checkError($response);
                foreach ($response->Item as $Item) {
                    $idFromResume[] = (string)$Item->{'Resume.P_Id'};
                }
                $start += self::NEXT_REQUEST_RANGE;
            } while ((string)$response->attributes()->Count === "200");

            return $idFromResume;
        }

        /**
         * 一定期間以内に更新されたプロセスのIDを取得
         *
         * @param [type] $startDate 期間の開始日時
         * @param [type] $endDate 期間の終了日時
         * @param [type] $headers リクエストヘッダ
         * @return array レジュメのIDのリスト
         */
        public function getResumeIdFromProcess($startDate, $endDate, $headers) : array
        {
            $idFromProcess = [];

            // TODO:更新日時で並び替えする
            $url = self::buildProcessUrl([
                "partition" => "2012",
                "field" => "Process.P_Resume",
                "count" => "200",
                "condition"  => "Process.P_UpdateDate:ge={$startDate},Process.P_UpdateDate:le={$endDate},",
            ]);

            $start = 0;
            // 取得件数が200件の場合は、それ以上のデータが存在する可能性があるため
            // 再度リクエストを行う
            do {
                $response = $this->curl->getXml($url . "&start={$start}", $headers);
                self::checkError($response);
                foreach ($response->Item as $Item) {
                    $idFromProcess[] = (string)$Item->{'Process.P_Resume'}->Resume->{'Resume.P_Id'};
                }
                $start += self::NEXT_REQUEST_RANGE;
            } while ((string)$response->attributes()->Count === "200");

            return $idFromProcess;
        }

        /**
         * IDに紐づくレジュメ情報を返す
         *
         * @param string $idParameter コロン区切りのIDリスト
         * @param array $headers リクエストヘッダ
         * @return array レジュメ情報（連想配列）の配列
         */
        public function getResume(string $idParameter, array $headers) : array
        {
            $mailmag = "Resume.U_AC5A0D55E86D3363238C61F123390E"; // メルマガ（受信設定）
            $prefecture = "Resume.U_7F3DF7F61EEF0679AA432646777832"; // 都道府県
            $interviewer = "Resume.U_5598122582348E10065B3A9B2FCC79"; // インタビュー担当者
            $barker = "Resume.U_7C79BBCF39E0D849E54929A8D70B06"; // 呼び込み担当者
            $comprehensiveevaluation = "Resume.U_BC07D210E9789C7E4C16092C2718F0"; // 【1】総合評価
            $urgency = "Resume.U_DF0E1BF1E942DE5B44759F145F26CC";  // 【2】緊急度
            $interviewdate = "Resume.U_7A82F71961342ABC309E82F3CB4549"; // インタビュー日
            $mypageregistrationdate = "Resume.U_E7DFF5F197913246BD6A40336802D4"; // マイページ登録日時

            // 返すべきレジュメ情報のリスト
            $resumeList = [];

            // 連携したい（CSVに出力したい）フィールドをパラメータで指定
            $url = self::buildResumeUrl([
                "partition" => "2012",
                "field" => "Resume.P_Id,Resume.P_Candidate(Person.P_Name,Person.P_Mail),{$mailmag},{$prefecture},Resume.P_Phase,Resume.P_PhaseDate,{$interviewer},{$barker},{$comprehensiveevaluation},{$urgency},{$interviewdate},Resume.P_ExpectArea,{$mypageregistrationdate}",
                "count" => "200",
                "condition"  => "Resume.P_Id:or={$idParameter}",
            ]);

            $start = 0;
            do {
                $response = $this->curl->getXml($url . "&start={$start}", $headers);
                self::checkError($response);
                foreach ($response->Item as $Item) {
                    //希望勤務地の文字列結合
                    $ExpectArea = "";
                    foreach ($Item->{"Resume.P_ExpectArea"}->children() as $Area) {
                        if ((strlen($ExpectArea) > 0)) {
                            $ExpectArea .= "，";
                        }
                        $ExpectArea .= (string)$Area->{"Option.P_Name"};
                    }

                    // 取得したデータをNULLチェックしつつ連想配列に格納
                    $resume = [
                        "id" => (string)$Item->{"Resume.P_Id"},
                        "name" => (string)$Item->{"Resume.P_Candidate"}->{"Candidate"}->{"Person.P_Name"},
                        "mail" => (string)$Item->{"Resume.P_Candidate"}->{"Candidate"}->{"Person.P_Mail"},
                        "mailmag" => $Item->{"{$mailmag}"}->children()[0] ? (string)$Item->{"{$mailmag}"}->children()[0]->{"Option.P_Name"} : "",
                        "prefecture" => $Item->{"{$prefecture}"}->children()[0] ? (string)$Item->{"{$prefecture}"}->children()[0]->{"Option.P_Name"} : "",
                        "phase" => $Item->{"Resume.P_Phase"}->children()[0] ? (string)$Item->{"Resume.P_Phase"}->children()[0]->{"Option.P_Name"} : "",
                        "phasedate" => $Item->{"Resume.P_PhaseDate"} ? (string)$Item->{"Resume.P_PhaseDate"} : "",
                        "interviewer" => $Item->{"{$interviewer}"}->children()[0] ? (string)$Item->{"{$interviewer}"}->children()[0]->{"Option.P_Name"} : "",
                        "barker" => $Item->{"{$barker}"}->children()[0] ? (string)$Item->{"{$barker}"}->children()[0]->{"Option.P_Name"} : "",
                        "comprehensiveevaluation" => $Item->{"{$comprehensiveevaluation}"}->children()[0] ? (string)$Item->{"{$comprehensiveevaluation}"}->children()[0]->{"Option.P_Name"} : "",
                        "urgency" => $Item->{"{$urgency}"}->children()[0] ? (string)$Item->{"{$urgency}"}->children()[0]->{"Option.P_Name"} : "",
                        "interviewdate" => $Item->{"{$interviewdate}"} ? (string)$Item->{"{$interviewdate}"} : "",
                        "desiredworkregion" => $ExpectArea,
                        "mypageregistrationdate" =>$Item->{"{$mypageregistrationdate}"} ? (string)$Item->{"{$mypageregistrationdate}"} : "",
                    ];
                    $resumeList[] = $resume;
                }
                $start += self::NEXT_REQUEST_RANGE;
            } while ((string)$response->attributes()->Count === "200");
            return $resumeList;
        }

        /**
         * IDに紐づくプロセス情報を返す
         *
         * @param string $idParameter コロン区切りのIDリスト
         * @param array $headers リクエストヘッダ
         * @return array プロセス情報（連想配列）の配列
         */
        public function getProcess(string $idParameter, array $headers) : array
        {
            // 返すべきレジュメ情報のリスト
            $processList = [];

            $url = self::buildProcessUrl([
                "partition" => "2012",
                "field" => "Process.P_Resume,Process.P_Client(Client.P_Name),Process.P_Job(Job.P_Id,Job.P_Position,Job.P_Phase),Process.P_PhaseDate,Process.P_Phase,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11",
                "count" => "200",
                "condition"  => "Process.P_Resume:or={$idParameter}",
                "order" => "Process.P_UpdateDate:desc",
            ]);

            $start = 0;
            do {
                $response = $this->curl->getXml($url . "&start={$start}", $headers);
                self::checkError($response);
                foreach ($response->Item as $Item) {
                    $process = [
                        "id" => (string)$Item->{"Process.P_Resume"}->{"Resume"}->{"Resume.P_Id"},
                        "client" => (string)$Item->{"Process.P_Client"}->{"Client"}->{"Client.P_Name"},
                        "job" => (string)$Item->{"Process.P_Job"}->{"Job"}->{"Job.P_Position"},
                        "jobphase" => $Item->{"Process.P_Job"}->{"Job"}->{"Job.P_Phase"}->children[0] ? (string)$Item->{"Process.P_Job"}->{"Job"}->{"Job.P_Phase"}->children()[0]->{"Option.P_Name"} : "",
                        "phasedate" => $Item->{"Process.P_PhaseDate"} ? (string)$Item->{"Process.P_PhaseDate"} : "",
                        "phase" => $Item->{"Process.P_Phase"}->children()[0] ? (string)$Item->{"Process.P_Phase"}->children()[0]->{"Option.P_Name"} : "",
                        "optionid" => $Item->{"Process.P_Phase"}->children()[0] ? (string)$Item->{"Process.P_Phase"}->children()[0]->{"Option.P_Id"} : "",
                        "mypage" => $Item->{"Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"}->children()[0] ? (string)$Item->{"Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"}->children()[0]->{"Option.P_Name"} : "",
                        "jobid" =>  (string)$Item->{"Process.P_Job"}->{"Job"}->{"Job.P_Id"},
                    ];
                    $processList[] = $process;
                }
                $start += self::NEXT_REQUEST_RANGE;
            } while ((string)$response->attributes()->Count === "200");
            return $processList;
        }

        /**
         * ジョブ打診以降のプロセス数を計算して返す
         *
         * @param array $processList　同じ人物のプロセスのリスト
         * @return integer ジョブ打診以降のプロセス数
         */
        public function getTempUpperPhase(array $processList): int
        {
            //ジョブ打診以降のオプションIDリスト
            $optionIdList = [
                "15",       //応募承諾
                "16",       //書類推薦
                "17",       //面接（一次）
                "18",       //面接（二次）
                "19",       //面接（最終）
                "10419",    //内定
                "10420",    //入社予定
                "10422",    //入社
                "11122",    //応募承諾
                "11123",    //書類OK
                "11124",    //面接（一次）OK
                "11125",    //面接（二次）OK
                "11126",    //面接（三次）
                "11127",    //面接（三次）OK
                "11172",    //クローズ
                "11173",    //クローズ
            ];
            $cnt = 0;

            foreach ($processList as $process) {
                // フェーズIDがジョブ打診以降のフェーズに含まれるならカウントアップ
                if (in_array($process["optionid"], $optionIdList)) {
                    $cnt++;
                }
            }
            return $cnt;
        }
        /**
         * DATE型をISO8601へフォーマット変換　
         *
         * @param [type] $date　phaseDate
         * @return string   変換後のDATE文字列
         */
        public function DateTimeExchangeISO8601Format($date): string
        {
            if ($date == "") {
                return $date;
            }
            $datetime = new DateTimeImmutable($date);
            $date_iso8601 = $datetime->format('Y-m-d\TH:i:sO');
            return $date_iso8601;
        }
        /**
         * 日付にフォーマット
         *
         * @param [type] $date　インタビュー日
         * @return string
         */
        public function DateExchangeISO8601Format($date): string
        {
            if ($date == "") {
                return $date;
            }
            $datetime = new DateTimeImmutable($date);
            $date_iso8601 = $datetime->format('Y-m-d');
            return $date_iso8601;
        }

        /**
         * メルマガ区分のテキスト更新
         *
         * @param string $mailmaga　メルマガのテキスト
         * @return string   更新後メルマガのテキスト
         */
        public function getMailMagaText(string $mailmaga): string
        {
            if ($mailmaga == "") {
                return "配信する";
            }
            return "配信しない";
        }

        /**
         * HRBC から取得できる時間は9時間前になっているため
         * 9時間プラスのオフセットを設定する
         *
         * @param [type] $date
         * @return string 9時間プラスされた時刻
         */
        public function hrbcOffset($date): string
        {
            //空の場合、現在時刻を生成しないように
            if ($date == "") {
                return $date;
            }
            $datetime = new DateTimeImmutable($date);
            $hrbcOffset = new DateInterval("PT9H");
            return $datetime->add($hrbcOffset)->format('Y-m-d\TH:i:sO');
        }
    }
