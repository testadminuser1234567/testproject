<?php
    require_once 'HrbcSalesforceConverter.php';
    require_once 'SalesforceUpsert.php';
    require_once 'UtilCsv.php';
    require_once 'UtilLogger.php';
    require_once 'SendMail.php';

    UtilLogger::info("batch start");

    // 大量データ更新時のメモリ枯渇対策
    ini_set("memory_limit", "512M");

    // メール送信用の設定
    mb_language("Japanese");
    mb_internal_encoding("UTF-8");

    try {
        $hrbcSalesforceConverter = new HrbcSalesforceConverter();
        $idCount = $hrbcSalesforceConverter->run();

        if ($idCount === 0) {
            UtilLogger::info("Skip Upsert because ID count is 0");
        } else {
            UtilLogger::info("Upsert ID count is ". $idCount);
            $salesforceUpsert = new SalesforceUpsert();
            $salesforceUpsert->run();
        }
    } catch (Exception $e) {
        UtilLogger::error($e->getMessage());
        $message = $e->getMessage();
        SendMail::sendOtherError("その他のエラーが発生しました\nエラーメッセージ：$message");
    }

    UtilCsv::deleteCsv();
    UtilLogger::info("batch end");
