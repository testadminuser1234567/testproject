<?php
    require_once 'UtilCsv.php';
    require_once 'UtilLogger.php';
    require_once 'HrbcSalesforceConverter.php';
    require_once 'SendMail.php';

    class SalesforceUpsert
    {
        const SALESFORCE_LOGINURL = "https://login.salesforce.com/services/Soap/u/45.0";
        const BATCH_WAIT_TIMER = 30;

        // リクエスト処理を担当する UtilCurl クラスのインスタンスにまかせる
        private $curl;

        /**
         * コンストラクタ
         */
        public function __construct()
        {
            $this->curl = new UtilCurl();
        }
        
        /**
         * Undocumented function
         *
         * @param string $baseUrl ベースとなるURL
         * @param string $apiUrl APIごとのURL
         * @param array $param APIリクエスト時のパラメータ
         * @return string
         */
        public function buildUrl(string $baseUrl, string $apiUrl) : string
        {
            return "{$baseUrl}{$apiUrl}";
        }

        /**
         * LoginリクエストのURL生成
         *
         * @param array $param リクエストパラメータ
         * @return string
         */
        public function getSessionIdAndServerUrl()
        {
            $url = self::SALESFORCE_LOGINURL;
            $body = '<?xml version="1.0" encoding="utf-8" ?>
            <env:Envelope xmlns:xsd="http://www.w3.org/2001/XMLSchema"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:env="http://schemas.xmlsoap.org/soap/envelope/">
              <env:Body>
                <n1:login xmlns:n1="urn:partner.soap.sforce.com">
                  <n1:username>ishibashi@hurex.co.jp</n1:username>
                  <n1:password>Yw7LbJF3kra3eaYCzCpWz0hTeyRVvPOo3</n1:password>
                </n1:login>
              </env:Body>
            </env:Envelope>';

            $headers = [
                "Content-Type: text/xml",
                "SOAPAction: login",
            ];

            $response = $this->curl->postRequest($url, $body, $headers);
            if (is_null($response->getElementsByTagName('serverUrl')->item(0))) {
                UtilLogger::error($response->textContent);
                return;
            }
            $serverUrl = (string)$response->getElementsByTagName('serverUrl')->item(0)->nodeValue;
            $sessionId = (string)$response->getElementsByTagName('sessionId')->item(0)->nodeValue;
            return [
                'serverUrl' => $serverUrl,
                'sessionId' => $sessionId,
            ];
        }

        /**
         * インスタンスURLの生成
         *
         * @param [type] $url
         * @return string
         */
        public function getInstanceUrl($url) : string
        {
            $value = [];
            $match = preg_match('/https:\/\/.*?\//', $url, $value);
            return $match ? $value[0]: "";
        }

        /**
         * ジョブの作成
         *
         * @param [type] $instanceUrl
         * @param [type] $sessionId
         * @return string
         */
        public function createJob($instanceUrl, $sessionId) : string
        {
            $url = "{$instanceUrl}services/async/45.0/job";
            
            $headers = [
                'Content-Type: text/xml',
                'SOAPAction: ""',
                "X-SFDC-Session: {$sessionId}",
            ];

            $body = '<?xml version="1.0" encoding="UTF-8"?>
            <jobInfo xmlns="http://www.force.com/2009/06/asyncapi/dataload">
                <operation>upsert</operation>
                <object>Lead</object>
                <externalIdFieldName>HRBCRESUMEID__C</externalIdFieldName>
                <contentType>CSV</contentType>
            </jobInfo>';

            $response = $this->curl->postRequest($url, $body, $headers);
            $idNode = $response->getElementsByTagName('id')->item(0);
            if ($idNode === null) {
                $jobId = "";
                self::outputLog($response->textContent);
            } else {
                $jobId = (string)$idNode->nodeValue;
            }
            return $jobId;
        }

        public function outputLog(string $message)
        {
            $filename = date("Ymd") . '.log';

            date_default_timezone_set('Asia/Tokyo');
            $now = new DateTimeImmutable();
            $timestamp = $now->format("H:i:s.v");
            file_put_contents("log/" . $filename, $timestamp . " " . $message . "\n", FILE_APPEND);
        }

        /**
         * バッチの追加
         *
         * @param string $instanceUrl
         * @param string $sessionId
         * @param string $jobId
         * @return string
         */
        public function createBatch(string $instanceUrl, string $sessionId, string $jobId) : string
        {
            $url = "{$instanceUrl}services/async/45.0/job/{$jobId}/batch";

            $headers = [
                'Content-Type: text/csv',
                'SOAPAction: ""',
                "X-SFDC-Session: {$sessionId}",
            ];

            $body = file_get_contents(UtilCsv::CSV_PATH, "r");

            $response = $this->curl->postRequest($url, $body, $headers);
            $batchId = (string)$response->getElementsByTagName('id')->item(0)->nodeValue;

            return $batchId;
        }

        /**
         * ジョブの終了
         *
         * @param string $instanceUrl
         * @param string $sessionId
         * @param string $jobId
         * @return void
         */
        public function closeJob(string $instanceUrl, string $sessionId, string $jobId)
        {
            $url = "{$instanceUrl}services/async/45.0/job/{$jobId}";

            $headers = [
                'Content-Type: text/xml',
                'SOAPAction: ""',
                "X-SFDC-Session: {$sessionId}",
            ];

            $body = '<?xml version="1.0" encoding="UTF-8"?>
            <jobInfo xmlns="http://www.force.com/2009/06/asyncapi/dataload">
                <state>Closed</state>
            </jobInfo>';

            $response = $this->curl->postRequest($url, $body, $headers);
        }

        /**
         * バッチ情報の取得
         *
         * @param string $instanceUrl
         * @param string $sessionId
         * @param string $jobId
         * @param string $batchId
         * @return void
         */
        public function getBatchStatus(string $instanceUrl, string $sessionId, string $jobId, string $batchId)
        {
            $url = "{$instanceUrl}services/async/45.0/job/{$jobId}/batch/{$batchId}";

            $headers = [
                "X-SFDC-Session: {$sessionId}",
            ];

            $response = $this->curl->getXml($url, $headers);
        }
        /**
         * バッチ結果を取得する
         *
         * @param string $instanceUrl
         * @param string $sessionId
         * @param string $jobId
         * @param string $batchId
         * @return void
         */
        public function getBatchResult(string $instanceUrl, string $sessionId, string $jobId, string $batchId): string
        {
            $url = "{$instanceUrl}services/async/45.0/job/{$jobId}/batch/{$batchId}/result";
            
            $headers = [
                "X-SFDC-Session: {$sessionId}",
            ];

            $response = $this->curl->getText($url, $headers);
            return $response;
        }

        public function run()
        {
            // ログインリクエスト
            $login = self::getSessionIdAndServerUrl();
            $instanceUrl = self::getInstanceUrl($login['serverUrl']);

            // ジョブの作成
            $jobId = self::createJob($instanceUrl, $login['sessionId']);

            if ($jobId === "") {
                UtilLogger::error("jobId is empty");
                return;
            }

            // バッチの作成
            $batchId = self::createBatch($instanceUrl, $login['sessionId'], $jobId);

            // ジョブの終了
            self::closeJob($instanceUrl, $login['sessionId'], $jobId);

            // バッチ状況の取得
            self::getBatchStatus($instanceUrl, $login['sessionId'], $jobId, $batchId);

            // バッチ結果の取得
            // バッチ結果を待つためにsleepする
            sleep(self::BATCH_WAIT_TIMER);
            $result = self::getBatchResult($instanceUrl, $login['sessionId'], $jobId, $batchId);
            UtilLogger::info($result);

            // エラーの有無を確認する
            self::checkError($result);
        }

        // エラーが発生していたらメール通知する
        public function checkError(string $result) {
            if (strpos($result, "<exceptionCode>InvalidBatch</exceptionCode>") !== false) {
                return;
            }

            $ids = UtilCsv::readIds();
            $lines = str_getcsv($result, "\n");
            $errors = "";

            // 先頭から要素を削除（CSVヘッダを削除）
            array_shift($ids);
            array_shift($lines);

            foreach ($lines as $index => $line) {
                $csv = str_getcsv($line, ",");
                $errorDetail = $csv[3];
                if ($errorDetail !== "") {
                    $resumeId = $ids[$index];
                    $errors .= "レジュメID ${resumeId} で更新エラーが発生しました\nエラー内容：${errorDetail}\n\n";
                }
            }
            if ($errors !== "") {
                SendMail::sendSalesforceError($errors);
            }
        }
    }
