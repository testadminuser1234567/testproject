<?php
    require_once "UtilLogger.php";

    class UtilCsv
    {
        // CSV の出力パス
        const CSV_PATH = "../tmp/tmp.csv";

        public static function deleteCsv()
        {
            if (file_exists(self::CSV_PATH)) {
                unlink(self::CSV_PATH);
            } else {
                UtilLogger::debug(self::CSV_PATH . " doesn't exist");
            }
        }

        public static function readIds() : array
        {
            if (file_exists(self::CSV_PATH)) {
                $content = file_get_contents(self::CSV_PATH);
                $lines = explode("\n", $content);
                $ids = array_map(function($line) {
                    return explode(",", $line)[0];
                }, $lines);
                return $ids;
            } else {
                UtilLogger::debug(self::CSV_PATH . " doesn't exist");
            }
        }
    }
