/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.language = 'ja';   
	config.filebrowserBrowseUrl = '/syskanri/kcfinder/browse.php?type=files';
	config.filebrowserImageBrowseUrl = '/syskanri/kcfinder/browse.php?type=images';
//	config.filebrowserFlashBrowseUrl = '/system/system/kcfinder/browse.php?type=flash';
	config.filebrowserUploadUrl = '/syskanri/kcfinder/upload.php?type=files';
	config.filebrowserImageUploadUrl = '/syskanri/kcfinder/upload.php?type=images';
//	config.filebrowserFlashUploadUrl = '/system/kcfinder/upload.php?type=flash';
	CKEDITOR.config.width = '1000px';
	CKEDITOR.config.height = '600px';
	CKEDITOR.config.toolbar = [
//		['Source','-','Save','NewPage','Preview','-','Templates']
//		,['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print','SpellChecker']
//		,['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat']
//		,['Form','Checkbox','Radio','TextField','Textarea','Select','Button','ImageButton','HiddenField']
//		,'/'
//		,['Bold','Italic','Underline','Strike','-','Subscript','Superscript']
//		,['NumberedList','BulletedList','-','Outdent','Indent','Blockquote']
//		,['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
//		,['Link','Unlink','Anchor']
//		,['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak']
//		,'/'
//		,['Styles','Format','Font','FontSize']
//		,['TextColor','BGColor']
//		,['ShowBlocks']
['Bold','Italic','Underline','Strike','-','Subscript','Superscript']
,['NumberedList','BulletedList','-','Outdent','Indent','Blockquote']
,['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock']
,['Link','Unlink','Anchor']
,['Image','Table','HorizontalRule']
,['Format','FontSize','TextColor','BGColor']
,['Source']
	];
	CKEDITOR.config.width = '600px';
	CKEDITOR.config.height = '300px';
	CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
};
