<?php
/**
 *      @desc CMS integration code: CodeIgniter
 *   @package KCFinder-CodeIgniter
 *   @version 0.3
 *    @author Tiger Fok <tiger@tiger-workshop.com>
 * @copyright 2007-2015 Tiger-Workshop
 *   @license http://opensource.org/licenses/GPL-3.0 GPLv3
 *   @license http://opensource.org/licenses/LGPL-3.0 LGPLv3
 *      @link http://tiger-workshop.com
 */
class CodeIgniter {
    static function getSession() {
        session_start();
        define('ENVIRONMENT', 'production');
        define('BASEPATH', $_SERVER["DOCUMENT_ROOT"] .'/kanri/system/');
        define('APPPATH', $_SERVER["DOCUMENT_ROOT"] .'/kanri/application/');
        require(APPPATH . 'config/config.php');
        require(APPPATH . 'config/database.php');
        require(BASEPATH . 'core/Common.php');        
        require(BASEPATH . 'database/DB.php');
        
        $database = DB($db['default']);
        
        if (!isset($_COOKIE['ci_session'])) return true;
        
        $database->where('id', $_COOKIE['ci_session']);
        $query = $database->get('ci_sessions');
        $result = $query->row();
        if ($result->login_status == 1) {
		$_SESSION['KCFINDER']['disabled'] = false;
        }else{
		$_SESSION['KCFINDER']['disabled'] = true;
	}
    }
}
CodeIgniter::getSession();