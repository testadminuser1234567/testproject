var showCoords;

function imageFormat(id, file, target, display_target, type, selected, url, module, resizemodule, display_target2, display_target3, jcrop_target, resize_anc, resize_main){
	$("#" + display_target2).empty();
	var image ="";
	var imageUrl="";
	showCoords = "showCoords";
	if(jcrop_target == "jcrop_target1"){
		showCoords= "showCoords1";
	}else if(jcrop_target == "jcrop_target2"){
		showCoords = "showCoords2";
	}else if(jcrop_target == "jcrop_target3"){
		showCoords = "showCoords3";
	}

	$('#' + id).upload(module, {filename:file},function(res) {
		var tmp = res.substring(0,2);
		if(tmp!="ok"){
			alert(res);
			return false;
		}else{
			image = res.substring(2);
			imageUrl = url + image;
			$("#" + display_target).empty();
			$("#" + display_target).append('<img src="' + imageUrl + '" id="' + jcrop_target +'">');
			$("#" + display_target2).append('<script type="text/javascript">$(document).ready(function(){$("#' + jcrop_target + '").Jcrop({onChange: ' + showCoords + ',onSelect: ' + showCoords + ',aspectRatio: 4 / 3});});</script><br/><a href="javascript:void(0)" onclick="delImage(\'' + target + '\',\'' + display_target + '\',\'' + display_target2 + '\',\'' + display_target3 + '\')">画像削除</a>　　<a href="javascript:void(0);" onclick="resizeImage(\''+ image + '\', \'' + resizemodule + '\' , \'' + url + '\', \'' + display_target + '\', \'' + display_target2 + '\', \'' + target + '\', \'' + display_target3 + '\', \'' + jcrop_target + '\', \'' + resize_anc + '\', \'' + resize_main + '\');" id="' + resize_anc +'">リサイズ</a>　横幅：<span id="x_' + jcrop_target + '"></span>　縦幅：<span id="y_' + jcrop_target + '"></span>　（リサイズ横幅:　<span id="xt2_' + jcrop_target + '"></span>　リサイズ縦幅:　<span id="yt2_' + jcrop_target + '"></span>）');
			$("#" + target).val(image);
		}
        }, 'text');
}

function delImage(target, display_target, display_target2, display_target3){
	$("#" + display_target).empty();
	$("#" + display_target2).empty();
	$("#" + display_target3).remove();
	$("#" + target).val('');
}


function resizeImage(img, module, url, display_target, display_target2, target, display_target3, jcrop_target, resize_anc, resize_main){
	$("#" + display_target2).empty();
	$("#" + display_target3).remove();
	var x1;
	var y1;
	var w;
	var h;
	var image ="";
	var imageUrl="";
	var tmp_id = "tmp_image";
	showCoords = "showCoords";
	if(jcrop_target == "jcrop_target1"){
		tmp_id= "tmp_image";
		showCoords = "showCoords1";
		x1 = $("#x1").val();
		y1 = $("#y1").val();
		w = $("#w").val();
		h = $("#h").val();
	}else if(jcrop_target == "jcrop_target2"){
		tmp_id = "tmp_image2";
		showCoords = "showCoords2";
		x1 = $("#x2").val();
		y1 = $("#y2").val();
		w = $("#w2").val();
		h = $("#h2").val();
	}else if(jcrop_target == "jcrop_target3"){
		tmp_id = "tmp_image3";
		showCoords = "showCoords3";
		x1 = $("#x3").val();
		y1 = $("#y3").val();
		w = $("#w3").val();
		h = $("#h3").val();
	}
	$.ajax({
		type: "POST",
		url: module,
		data: {
			'x1': x1,
			'y1': y1,
			'w': w,
			'h': h,
			'image_thumb':img
		},
		success: function(res){
			var tmp = res.substring(0,2);
			if(tmp!="ok"){
				alert(res);
				return false;
			}else{
				image = res.substring(2);
				imageUrl = url + image;
				$("#" + display_target).after('<div id="' + display_target3 + '" style="margin-top:20px;"><img src="' + imageUrl + '" id="' + tmp_id + '"><br/><a href="javascript:void(0);" onclick="setResize(\'' + target + '\', \'' + imageUrl + '\', \'' + image + '\', \'' + display_target3 + '\', \'' + jcrop_target + '\', \'' + resize_anc + '\', \'' + resize_main + '\', \'' + display_target + '\')" id="' + resize_main +'">この画像を使用</a>　<label>横幅： <span id="xt_' + jcrop_target + '"></span></label>　<label>縦幅： <span id="yt_' + jcrop_target + '"></span>　（リサイズ横幅:　<span id="xt2_' + jcrop_target + '"></span>　リサイズ縦幅:　<span id="yt2_' + jcrop_target + '"></span>）</label></div>');
				$("#" + display_target2).append('<script type="text/javascript">$(document).ready(function(){$("#' + jcrop_target  + '").Jcrop({onChange: ' + showCoords + ',onSelect: ' + showCoords + ',aspectRatio: 4 / 3});});</script><br/><a href="javascript:void(0)" onclick="delImage(\'' + target + '\',\'' + display_target + '\', \'' + display_target2 + '\',\'' + display_target3 + '\')">画像削除</a>　　<a href="javascript:void(0);" onclick="resizeImage(\''+ img + '\', \'' + module + '\' , \'' + url + '\', \'' + display_target + '\', \'' + display_target2 + '\', \'' + target + '\', \'' + display_target3 + '\', \'' + jcrop_target + '\', \'' + resize_anc + '\', \'' + resize_main + '\');" id="' + resize_anc +'">リサイズ</a>');
			}
		}
	});
}

function setResize(target, image_url, image, display_target3, jcrop_target, resize_anc, resize_main, display_target){
	$("#" + target).val(image);
//	$("#image1_tmp img").attr("src",image);
	$("#" + display_target).empty();
	$("#" + resize_main).empty();
	$("#" + resize_anc).empty();
	$("#" + display_target3).val(image_url);
}



function x_check() {
	var t_x = $("#jcrop_target1").width();
	var t_tx = $("#tmp_image").width();

	$("#x_jcrop_target1").text(t_x);
	$("#xt_jcrop_target1").text(t_tx);
        setTimeout("x_check()", 1000); //100+αmsごとに比べる
}
function y_check() {
	var t_y = $("#jcrop_target1").height();
	var t_ty = $("#tmp_image").height();

	$("#y_jcrop_target1").text(t_y);
	$("#yt_jcrop_target1").text(t_ty);
        setTimeout("y_check()", 1000); //100+αmsごとに比べる
}
function x2_check() {
	var t_x2 = $("#jcrop_target2").width();
	var t_tx2 = $("#tmp_image2").width();

	$("#x_jcrop_target2").text(t_x2);
	$("#xt_jcrop_target2").text(t_tx2);
        setTimeout("x2_check()", 1000); //100+αmsごとに比べる
}
function y2_check() {
	var t_y2 = $("#jcrop_target2").height();
	var t_ty2 = $("#tmp_image2").height();

	$("#y_jcrop_target2").text(t_y2);
	$("#yt_jcrop_target2").text(t_ty2);
        setTimeout("y2_check()", 1000); //100+αmsごとに比べる
}
function x3_check() {
	var t_x3 = $("#jcrop_target3").width();
	var t_tx3 = $("#tmp_image3").width();

	$("#x_jcrop_target3").text(t_x3);
	$("#xt_jcrop_target3").text(t_tx3);
        setTimeout("x3_check()", 1000); //100+αmsごとに比べる
}
function y3_check() {
	var t_y3 = $("#jcrop_target3").height();
	var t_ty3 = $("#tmp_image3").height();

	$("#y_jcrop_target3").text(t_y3);
	$("#yt_jcrop_target3").text(t_ty3);
        setTimeout("y3_check()", 1000); //100+αmsごとに比べる
}

setTimeout("x_check()", 1000);
setTimeout("y_check()", 1000);
setTimeout("x2_check()", 1000);
setTimeout("y2_check()", 1000);
setTimeout("x3_check()", 1000);
setTimeout("y3_check()", 1000);