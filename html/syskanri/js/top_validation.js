$(function(){
	$("form").submit(function(){
		var err = "";
		var err2 =  "";

		var disp_flg1 = 0;
		var disp_flg2 = 0;
		var display0 = $("input[name='display[0]']:checked").val();
		var display1 = $("input[name='display[1]']:checked").val();
		var display2 = $("input[name='display[2]']:checked").val();
		var display3 = $("input[name='display[3]']:checked").val();
		var display4 = $("input[name='display[4]']:checked").val();

		if(display0 == 1){
			disp_flg1++;
			if($("#heading0").val() =="") err2 += "緑化推進委員会とはの見出しを入力して下さい。\n";
			if($("#discription0").val() =="") err2 += "緑化推進委員会とはの説明文を入力して下さい。\n";
		}else if(display0 == 2){
			disp_flg2++;
			if($("#heading0").val() =="") err2 += "緑化推進委員会とはの見出しを入力して下さい。\n";
			if($("#discription0").val() =="") err2 += "緑化推進委員会とはの説明文を入力して下さい。\n";
		}
		if(display1 == 1){
			disp_flg1++;
			if($("#heading1").val() =="") err2 += "緑の募金の見出しを入力して下さい。\n";
			if($("#discription1").val() =="") err2 += "緑の募金の説明文を入力して下さい。\n";
		}else if(display1 == 2){
			disp_flg2++;
			if($("#heading1").val() =="") err2 += "緑の募金の見出しを入力して下さい。\n";
			if($("#discription1").val() =="") err2 += "緑の募金の説明文を入力して下さい。\n";
		}
		if(display2 == 1){
			disp_flg1++;
			if($("#heading2").val() =="") err2 += "助成事業の見出しを入力して下さい。\n";
			if($("#discription2").val() =="") err2 += "助成事業の説明文を入力して下さい。\n";
		}else if(display2 == 2){
			disp_flg2++;
			if($("#heading2").val() =="") err2 += "助成事業の見出しを入力して下さい。\n";
			if($("#discription2").val() =="") err2 += "助成事業の説明文を入力して下さい。\n";
		}
		if(display3 == 1){
			disp_flg1++;
			if($("#heading3").val() =="") err2 += "緑の少年団の見出しを入力して下さい。\n";
			if($("#discription3").val() =="") err2 += "緑の少年団の説明文を入力して下さい。\n";
		}else if(display3 == 2){
			disp_flg2++;
			if($("#heading3").val() =="") err2 += "緑の少年団の見出しを入力して下さい。\n";
			if($("#discription3").val() =="") err2 += "緑の少年団の説明文を入力して下さい。\n";
		}
		if(display4 == 1){
			disp_flg1++;
			if($("#heading4").val() =="") err2 += "お知らせ・募集の見出しを入力して下さい。\n";
			if($("#discription4").val() =="") err2 += "お知らせ・募集の説明文を入力して下さい。\n";
		}else if(display4 == 2){
			disp_flg2++;
			if($("#heading4").val() =="") err2 += "お知らせ・募集の見出しを入力して下さい。\n";
			if($("#discription4").val() =="") err2 += "お知らせ・募集の説明文を入力して下さい。\n";
		}

		if(disp_flg1 <=0){
			err += "TOP左側の指定がありません。\n";
		}
		if(disp_flg1 > 1){
			err += "TOP左側が2つ以上指定されています。\n";
		}

		if(disp_flg2 <=0){
			err += "TOP右側の指定がありません。\n";
		}
		if(disp_flg2 > 1){
			err += "TOP右側が2つ以上指定されています。\n";
		}

		if(err){
			alert(err);
			return false;
		}
		if(err2){
			alert(err2);
			return false;
		}
viewport();

	});
});