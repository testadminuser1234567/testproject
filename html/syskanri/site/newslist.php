<?php
date_default_timezone_set('Asia/Tokyo');
include ('site_config.php');

include ('Pagination.php');

$paginate = new Pagination();
$today = date('Y-m-d 00:00:00');

//トピックの一覧取得
$sql = "SELECT COUNT(*) as cnt FROM articles where del = :del";
$stmt = $dbh->prepare($sql);
$stmt->execute(array('del' => 0));
$count = $stmt->fetch(PDO::FETCH_NUM);
/*
/  ページネーション
*/
$limit = 20;
$page = $_GET['page'];
$page = htmlspecialchars($page, ENT_QUOTES, 'UTF-8');
if(!is_numeric($page)){
	$page = 1;
}
$config["page"] = $page;
$config["base_url"] = "index.html";
$config["cur_page"] = $page;
$config["per_page"] = $limit;
$config["total_rows"] = $count[0];
$config["first_link"] = "&laquo";
$config["last_link"] = "&raquo";
$config['num_links'] = 3;
$paginate->initialize($config);
//ページネーションのリンク取得
$pagination = $paginate->create_links();
//データ用オフセット取得
$offset = $paginate->get_offset();

//トピックの公開取得
$sql_list = "SELECT * FROM articles where del = :del order by create_date desc, updated desc limit $offset, $limit";
$stmt = $dbh->prepare($sql_list);
$stmt->execute(array('del' => 0));
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$news[] = $result;
}