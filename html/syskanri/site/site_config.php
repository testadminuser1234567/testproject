<?php

require_once($_SERVER["DOCUMENT_ROOT"] .  '/../hrbc/site/config.php');

//PDO
$dsn = 'mysql:dbname=' . dbDatabase .';host=' . dbHostname . ';port=' . dbPort;
$user = dbUsername;
$password = dbPassword;

try{
	$dbh = new PDO($dsn, $user, $password,
		array(
		     	PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET `utf8`"
		));

}catch (PDOException $e){
    print('Error:'.$e->getMessage());
    die();
}


