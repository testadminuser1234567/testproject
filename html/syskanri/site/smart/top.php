<?php
date_default_timezone_set('Asia/Tokyo');
include ($_SERVER["DOCUMENT_ROOT"] . '/syskanri/site/site_config.php');


//本日日付
$today=date('Y-m-d');


//新着情報一覧取得
$news=array();
$sql = "SELECT * FROM articles where del=:del order by create_date desc, updated desc limit 0, 6";
$stmt = $dbh->prepare($sql);
$stmt->execute(array('del'=>0));
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$news[] = $result;
}