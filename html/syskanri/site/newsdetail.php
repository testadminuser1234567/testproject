<?php
include ('site_config.php');

$id = $_GET['id'];
$id = htmlspecialchars($id, ENT_QUOTES, 'UTF-8');
if(!is_numeric($id)){
	$id="";
}

//トピックの詳細取得
$sql = "SELECT * FROM articles where del = :del and id = :id";
$stmt = $dbh->prepare($sql);
$stmt->execute(array('del' => 0, 'id' => $id));

$result = $stmt->fetch(PDO::FETCH_ASSOC);