<?php
class MY_Controller extends CI_Controller {

	public $clean;

	function __construct()
	{
		$cache_path = unserialize(CACHE_LIMITER);
		if(!empty($cache_path)){
			foreach($cache_path as $k => $v){
				if(preg_match("/$v/",$_SERVER["REQUEST_URI"])){
					session_cache_limiter('private_no_expire');
					session_cache_expire(0);
				}
			}
		}

		parent::__construct();

		//xss対策
		$purifierdir = $_SERVER['DOCUMENT_ROOT'] . kanriDirectory . '/htmlpurifier/library/HTMLPurifier.auto.php'; 
		require_once($purifierdir);
		$config = HTMLPurifier_Config::createDefault();
		$config->set('Core.Encoding', 'UTF-8');
		$config->set('HTML.Doctype', 'XHTML 1.0 Transitional');
		$config->set('Attr.AllowedFrameTargets', array('_blank','_self'));
		$config->set('HTML.Trusted', true);

//		$config->set('HTML.Allowed', 'pre');
		$this->clean = new HTMLPurifier($config);

		$this->_login_check();
	}

	////////////////////////////////////////////////////////////////
	// ログインチェック
	////////////////////////////////////////////////////////////////
	function _login_check(){
		$key = $this->session->userdata('authenticated');
		$login_user = $this->session->userdata('login_user');

		if(empty($key) || empty($login_user)){
			if(!preg_match('/auth\/login/',$_SERVER["REQUEST_URI"]) && !preg_match('/auth\/logout/',$_SERVER["REQUEST_URI"]) && !preg_match('/auth\/forget_password/',$_SERVER["REQUEST_URI"]) && !preg_match('/auth\/create_password/',$_SERVER["REQUEST_URI"]) && !preg_match('/auth\/send_mail/',$_SERVER["REQUEST_URI"]) && !preg_match('/auth\/new_password/',$_SERVER["REQUEST_URI"])){
				//セッションの破棄
				$this->session->unset_userdata('authenticated');
				$this->session->unset_userdata('login_user');

				redirect('auth/login');
			}
		}else{
			$return = $this->db->get_where('auths', array('login_hash' => $key))->row();

			//ログイン済
			if($return->userid == $login_user && $return->login_hash == $key){
				$this->session->set_userdata('user_id', $return->id);

				//kcfinder用2時間有効
				$kcfinder = strtotime(date("Y-m-d H:i:s",strtotime("+2 hour")));
				$ci_session = $_COOKIE['ci_session'];
				$data = array(
					'kcfinder' => $kcfinder
				);
				$this->db->where('id', $ci_session);
				$this->db->update('ci_sessions', $data); 

			//未ログイン
			}else{
				$this->session->unset_userdata('authenticated');
				$this->session->unset_userdata('login_user');

				redirect('auth/login');
			}
		}
	}

	////////////////////////////////////////////////////////////////
	// csrf　発行
	////////////////////////////////////////////////////////////////
	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	////////////////////////////////////////////////////////////////
	// csrf チェック
	////////////////////////////////////////////////////////////////
	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	////////////////////////////////////////////////////////////////
	// ファイルアップロード
	////////////////////////////////////////////////////////////////
	function fileUpload($image, $cnt, $files, $dir, $require , $filename, $overwrite)
	{
		$result=array();
		$upDir = UploadDirectory . $dir;
		//ファイルアップロード
		if ( $files['userfile']['size'][$cnt] != 0){
			if (!file_exists($upDir)) {
				if ( !mkdir( $upDir ) ) {
					echo "ディレクトリ作成の権限がありません。<br>管理者に問い合わせてください。";
					exit;
				}
			}

			$_FILES['userfile']['name']= $files['userfile']['name'][$cnt];
			$_FILES['userfile']['type']= $files['userfile']['type'][$cnt];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$cnt];
			$_FILES['userfile']['error']= $files['userfile']['error'][$cnt];
			$_FILES['userfile']['size']= $files['userfile']['size'][$cnt];

			$fn = $_FILES["userfile"]['name'];
			$ext = substr($fn, strrpos($fn, '.') + 1);
			if(empty($filename)){
				$filename = date('Ymdhis') . "_" . mt_rand() . "." .$ext;
			}
			$config['upload_path'] = $upDir;
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['max_size']	= '10000000';
			$config['file_name']	= $filename;
			$config['overwrite'] = $overwrite;

			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload())
			{
				$result = array('error' => $this->upload->display_errors());
			}
			else
			{
				$result = array('upload_data' => $this->upload->data());
			}
		//ファイル変更なし
		}else if(!empty($image)){
			$result[] = $upDir . $image;
		}else{
			//$result[0] = "";
			if($require == 1){
				// エラー処理
				$result['error'] = "画像を選択して下さい。";
			}else{
				$result="";
			}
		}

		return $result;
	}

	////////////////////////////////////////////////////////////////
	// リサイズ
	////////////////////////////////////////////////////////////////
	function resizeImage($source_image, $width, $height, $thumb_marker, $master_dim)
	{
		$config['source_image']	= $source_image;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']	= $width;
		$config['height']	= $height;
		$config['thumb_marker']	= $thumb_marker;
		$config['master_dim']	= $master_dim;

		$this->load->library('image_lib', $config); 

		$this->image_lib->initialize($config);
		$ret = $this->image_lib->resize();
		unset($config);
		$this->image_lib->clear();
	}
}
