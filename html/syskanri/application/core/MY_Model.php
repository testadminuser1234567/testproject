<?php
class MY_Model extends CI_Model {

	/**
	 * table name
	 * 
	 * @var string
	 */
	protected $_table;

	/**
	 * constructor
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * save
	 * 
	 * @return integer 
	 */
	public function save($id = null, $data = null) {
		//idなしはinsert 
		if(empty($id)){
			$ret = $this->db->insert($this->_table, $data);
			if ($ret === FALSE) {
				return FALSE;
			}
			return $this->db->insert_id();
		}else{
			$ret = $this->db->update($this->_table, $data, array('id' => $id));
			if ($ret === FALSE) {
				return FALSE;
			}
			return $ret;
		}
	}
}
