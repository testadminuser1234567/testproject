<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {

	protected $page_limit = 20;

	//タイトル
	protected $sub_title = "ユーザー";

	//segment
	protected $segment = "auth";


	function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->library(array('form_validation'));
		$this->load->helper(array('url'));
	}

	// redirect if needed, otherwise display the user list
	function index()
	{
		$this->login();
	}

	//////////////////////////////////////////////////////////
	//ログイン
	//////////////////////////////////////////////////////////
	function login()
	{
		//POSTデータの取得
		$posts = $this->input->post();

		//エラーメッセージ格納用
		$errors = "";

		//ログイン処理
		if(!empty($posts)){
			$username = $this->clean->purify($posts['username']);
			$password = $this->clean->purify($posts['password']);

			if (!strlen($username)) {
				$errors .= 'ユーザIDを入力してください<br />';
			}

			if (!strlen($password)) {
				$errors .= 'パスワードを入力してください<br />';
			}

			//エラーがなければユーザの照合
			if (empty($errors)) {
				//DBにアクセス
				$pass = hash_hmac("sha256", $password, Salt);
				$user = hash_hmac("sha256", $username, Salt);
				$salt = hash_hmac("sha256", Salt, Salt);
				$pass_unit = $user . $pass . $salt;
				$return = $this->db->get_where('auths', array('userid' => $username, 'password' => $pass_unit))->row();

				//postのユーザとDBが違う場合は失敗
				if(empty($return->userid) || $return->userid != $username){
					$errors .= 'ユーザIDかパスワードが不正です<br />';

					//data設定
					$data["render"] = "auth/login";
					$data["clean"] = $this->clean;
					$data["msg"] = $errors;
					$data["segment"] = $this->segment;
			 		$this->load->view("template", $data);
				//成功
				}else{
					//ログイン情報を記憶した場合は値保存
					if(!empty($posts['loginkey']) && $this->clean->purify($posts['loginkey'])==='on'){
						$val = md5(uniqid(mt_rand(), TRUE));
						$data = array(
							'loginkey' => $loginkey
						);
						$this->db->where('id', $return->id);
						$this->db->update('auths', $data); 
					}
					//ログインOK
					$login_hash = hash_hmac("sha256", date("Ymd").$return->userid, Salt);
					$last_login = date('Y-m-d h:i:s');
					$user_agent = $_SERVER['HTTP_USER_AGENT'];
					$ip = $_SERVER["REMOTE_ADDR"];

					$data = array(
						'login_hash' => $login_hash,
						'last_login' => $last_login,
						'user_agent' => $user_agent,
						'ip' => $ip
					);
					$this->db->where('id', $return->id);
					$this->db->update('auths', $data); 

					//kcfinder用2時間有効
					$kcfinder = strtotime(date("Y-m-d H:i:s",strtotime("+2 hour")));
					$ci_session = $_COOKIE['ci_session'];
					$data = array(
						'kcfinder' => $kcfinder
					);
					$this->db->where('id', $ci_session);
					$this->db->update('ci_sessions', $data); 

					$sessionData = array(
						'authenticated'  => hash_hmac("sha256", date("Ymd").$return->userid, Salt),
						'login_user'  => $return->userid,
						'login_id'  => $return->id,
						'login_role'  => $return->role
					);
					$this->session->set_userdata($sessionData);

					if($return->id == 1){
						redirect(Home);
					}else if($return->id == 2){
						redirect(HRBC);
					}
				}
			//IDかパスワードが入力されていない場合
			} else {
				//data設定
				$data["render"] = "auth/login";
				$data["clean"] = $this->clean;
				$data["msg"] = $errors;
				$data["segment"] = $this->segment;
				$this->load->view("template", $data);
			}
		//初期画面
		}else{
			$errors .= $this->session->flashdata('msg');;
			//data設定
			$data["render"] = "auth/login";
			$data["clean"] = $this->clean;
			$data["msg"] = $errors;
			$data["segment"] = $this->segment;
			$this->load->view("template", $data);
		}
	}

	//////////////////////////////////////////////////////////
	//ログアウト
	//////////////////////////////////////////////////////////
	function logout()
	{		
		//login_status delete
		$sess = $_COOKIE['ci_session'];
		$this->db->where('id', $sess);
		$this->db->delete('ci_sessions'); 
	        
		// log the user out
		$this->session->unset_userdata('authenticated');
		$this->session->unset_userdata('login_user');
		$this->session->unset_userdata('login_id');
		$this->session->unset_userdata('login_role');

		// redirect them to the login page
		redirect('auth/login');
	}

	//////////////////////////////////////////////////////////
	//一覧出力
	//////////////////////////////////////////////////////////
	function home()
	{
		$role = $this->session->userdata('login_role');
		$login_id = $this->session->userdata('login_id');

		$posts = $this->input->post();

		$msg = $this->session->userdata('msg');
		$this->session->unset_userdata('msg');

		$this->load->library("pagination");

		if($role==1){
			$param = array("del"=>0);
		}else{
			$param = array("del"=>0, "id"=>$login_id);
		}

		//Pagination
		$config['base_url'] = base_url() . $this->uri->segment(1) . "/" . $this->uri->segment(2);
		$config["total_rows"] = $this->db->get_where("auths", $param)->num_rows();
 		$config["per_page"] = $this->page_limit;
 		$config["num_links"] = 2;
		$this->pagination->initialize($config);

		//data設定
		$data["contents"] = $this->db->order_by('created desc, updated desc')->get_where("auths",  $param, $config["per_page"], $this->uri->segment(3)); ;
		$data["render"] = "auth/home";
		$data["clean"] = $this->clean;
		$data["csrf"] = $this->_get_csrf_nonce();
		$data["msg"] = $msg;
		$data["login_id"] = $login_id;
		$data["role"] = $role;
		$data["segment"] = $this->segment;

 		$this->load->view("template", $data);
	}

	//////////////////////////////////////////////////////////
	//新規登録
	//////////////////////////////////////////////////////////
	function regist()
	{
		$role = $this->session->userdata('login_role');
		if($role != 1)
		{
			$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
			redirect("auth/login");
		}

		$posts = $this->input->post();
		
		//idが設定されていて戻るじゃない場合は編集
		if(!empty($posts['id']) && empty($posts['mode'])){
			$data['auth'] = $this->db->get_where('auths', array('id' => $posts['id']))->row();
		}else{
			$data['auth'] = (object) $posts;
			//idがない場合は空データ設定
			if(empty($data['auth']->id)){
				$data['auth']->id = "";
			}
		}

		$error = "";

		//data設定
		$data["render"] = "auth/regist";
		$data["clean"] = $this->clean;
		$data["csrf"] = $this->_get_csrf_nonce();
		$data["msg"] = "";
		$data["segment"] = $this->segment;

 		$this->load->view("template", $data);
	}

	//////////////////////////////////////////////////////////
	//新規登録処理
	//////////////////////////////////////////////////////////
	function add()
	{
		$role = $this->session->userdata('login_role');
		if($role != 1)
		{
			$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
			redirect("auth/login");
		}

		//CSRFチェック
		if ($this->_valid_csrf_nonce() === FALSE)
		{
			$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
			redirect("auth/home");
		}

		//バリデーション実行
		$error = "";
		$error = $this->setValidation();

		$posts = $this->input->post();

		//同一IDとメールアドレスチェック
		$sameId = $this->db->get_where('auths', array('userid' => $posts['userid'], 'del'=>0))->row();
		$sameEmail = $this->db->get_where('auths', array('email' => $posts['email'], 'del'=>0))->row();
		if($sameId) $error .= "<p>登録済みのIDです。</p>";
		if($sameEmail) $error .= "<p>登録済みのメールアドレスです。</p>";

		if($error !== ""){
			//data設定
			$data["render"] = "auth/regist";
			$data["clean"] = $this->clean;
			$data["csrf"] = $this->_get_csrf_nonce();
			$data["msg"] = $error;
			$data["segment"] = $this->segment;

	 		$this->load->view("template", $data);
		}else{
			if(empty($posts['id'])){
				$data['created'] = date('Y-m-d');
			}
			$data['id'] = $posts['id'];
			$data['userid'] = $posts['userid'];
			$pass = hash_hmac("sha256", $posts['password'], Salt);
			$user = hash_hmac("sha256", $posts['userid'], Salt);
			$salt = hash_hmac("sha256", Salt, Salt);
			$pass_unit = $user . $pass . $salt;
			$data['password'] = $pass_unit;
			$data['email'] = $posts['email'];

			$this->load->model('auths_model');
			$ret = $this->auths_model->save($data['id'], $data);
		
			if(empty($ret)){
				//エラー処理
			}else{
				//登録成功
				$this->session->set_flashdata(array("msg"=>"登録が完了しました。"));
				redirect("auth/home");
			}
		}

	}

	//////////////////////////////////////////////////////////
	//バリデーション
	//////////////////////////////////////////////////////////
	private function setValidation($validation=null)
	{
		$errors ="";
		$this->form_validation->set_rules("userid", "ユーザーID","required|min_length[4]|max_length[12]");
		$this->form_validation->set_rules("password", "パスワード","required|min_length[12]|max_length[20]");
		$this->form_validation->set_rules("email", "メールアドレス","required|valid_email");

		if($this->form_validation->run() == FALSE){
			$errors = validation_errors();
		}
		return $errors;
	}

	//////////////////////////////////////////////////////////
	//更新
	//////////////////////////////////////////////////////////
	function edit()
	{
		$posts = $this->input->post();

		$login_id = $this->session->userdata('login_id');

		$role = $this->session->userdata('login_role');

		//マスターと自分以外は編集不可
		if($role != 1)
		{
			if($login_id != $posts['id']){
				$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
				redirect("auth/home");
			}
		}
		
		//idが設定されていて戻るじゃない場合は編集
		if(!empty($posts['id']) && empty($posts['mode'])){
			$data['auth'] = $this->db->get_where('auths', array('id' => $posts['id']))->row();
		}else{
			$data['auth'] = (object) $posts;
			//idがない場合は空データ設定
			if(empty($data['auth']->id)){
				$data['auth']->id = "";
			}
		}

		$error = "";

		//data設定
		$data["render"] = "auth/edit";
		$data["clean"] = $this->clean;
		$data["csrf"] = $this->_get_csrf_nonce();
		$data["msg"] = "";
		$data["segment"] = $this->segment;

 		$this->load->view("template", $data);
	}

	//////////////////////////////////////////////////////////
	//ユーザー情報更新
	//////////////////////////////////////////////////////////
	function update()
	{
		$posts = $this->input->post();

		$login_id = $this->session->userdata('login_id');

		$role = $this->session->userdata('login_role');

		//マスターと自分以外は編集不可
		if($role != 1)
		{
			if($login_id != $posts['id']){
				$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
				redirect("auth/home");
			}
		}

		//CSRFチェック
		if ($this->_valid_csrf_nonce() === FALSE)
		{
			$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
			redirect("auth/home");
		}

		//バリデーション実行
		$error = "";
		$error = $this->setUpdateValidation();

		$posts = $this->input->post();

		//同一メールアドレスチェック
		$sameEmail = $this->db->get_where('auths', array('id !=' => $posts['id'], 'email' => $posts['email'], 'del'=>0))->row();
		if($sameEmail) $error .= "<p>登録済みのメールアドレスです。</p>";

		if($error !== ""){
			//data設定
			$data["auth"] =  (object) $posts;
			$data["render"] = "auth/edit";
			$data["clean"] = $this->clean;
			$data["csrf"] = $this->_get_csrf_nonce();
			$data["msg"] = $error;
			$data["segment"] = $this->segment;

	 		$this->load->view("template", $data);
		}else{
			$data['id'] = $posts['id'];
			$data['email'] = $posts['email'];

			$this->load->model('auths_model');
			$ret = $this->auths_model->save($data['id'], $data);
		
			if(empty($ret)){
				//エラー処理
			}else{
				//登録成功
				$this->session->set_flashdata(array("msg"=>"登録が完了しました。"));
				redirect("auth/home");
			}
		}

	}

	//////////////////////////////////////////////////////////
	//バリデーション
	//////////////////////////////////////////////////////////
	private function setUpdateValidation($validation=null)
	{
		$errors ="";
		$this->form_validation->set_rules("email", "メールアドレス","required|valid_email");

		if($this->form_validation->run() == FALSE){
			$errors = validation_errors();
		}
		return $errors;
	}

	//////////////////////////////////////////////////////////
	//パスワード変更
	//////////////////////////////////////////////////////////
	function change_password()
	{
		$posts = $this->input->post();

		$login_id = $this->session->userdata('login_id');

		$role = $this->session->userdata('login_role');

		//マスターと自分以外は編集不可
		if($role != 1)
		{
			if($login_id != $posts['id']){
				$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
				redirect("auth/home");
			}
		}
		
		//idが設定されていて戻るじゃない場合は編集
		if(!empty($posts['id']) && empty($posts['mode'])){
			$data['auth'] = $this->db->get_where('auths', array('id' => $posts['id']))->row();
		}else{
			$data['auth'] = (object) $posts;
			//idがない場合は空データ設定
			if(empty($data['auth']->id)){
				$data['auth']->id = "";
			}
		}

		$error = "";

		//data設定
		$data["render"] = "auth/change_password";
		$data["clean"] = $this->clean;
		$data["csrf"] = $this->_get_csrf_nonce();
		$data["msg"] = "";
		$data["segment"] = $this->segment;

 		$this->load->view("template", $data);
	}

	//////////////////////////////////////////////////////////
	//パスワード更新処理
	//////////////////////////////////////////////////////////
	public function reset_password($code = NULL)
	{
		//CSRFチェック
		if ($this->_valid_csrf_nonce() === FALSE)
		{
			$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
			redirect("auth/home");
		}

		//バリデーション実行
		$error = "";
		$error = $this->setPasswordValidation();

		$posts = $this->input->post();

		$login_id = $this->session->userdata('login_id');

		$role = $this->session->userdata('login_role');

		//マスターと自分以外は編集不可
		if($role != 1)
		{
			if($login_id != $posts['id']){
				$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
				redirect("auth/home");
			}
		}

		if($error !== ""){
			//data設定
			$data["auth"] =  (object) $posts;
			$data["render"] = "auth/change_password";
			$data["clean"] = $this->clean;
			$data["csrf"] = $this->_get_csrf_nonce();
			$data["msg"] = $error;
			$data["segment"] = $this->segment;

	 		$this->load->view("template", $data);
		}else{
			//パスワード再発行
			$pass = hash_hmac("sha256", $posts['password'], Salt);
			$user = hash_hmac("sha256", $posts['userid'], Salt);
			$salt = hash_hmac("sha256", Salt, Salt);
			$pass_unit = $user . $pass . $salt;
			$data['password'] = $pass_unit;

			$this->load->model('auths_model');
			$ret = $this->auths_model->save($posts['id'], $data);
		
			if(empty($ret)){
				//エラー処理
			}else{
				//登録成功
				$this->session->set_flashdata(array("msg"=>"パスワードの更新が完了しました。"));
				redirect("auth/home");
			}
		}

	}

	//////////////////////////////////////////////////////////
	//パスワードバリデーション
	//////////////////////////////////////////////////////////
	private function setPasswordValidation($validation=null)
	{

		$errors ="";
		$this->form_validation->set_rules("password", "パスワード","required|min_length[12]|max_length[20]");

		if($this->form_validation->run() == FALSE){
			$errors = validation_errors();
		}
		return $errors;
	}

	//////////////////////////////////////////////////////////
	//登録情報削除
	//////////////////////////////////////////////////////////
	public function delete()
	{
		$posts = $this->input->post();

		//CSRFチェック
		if ($this->_valid_csrf_nonce() === FALSE)
		{
			$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
			redirect("article/home");
		}

		$posts = $this->input->post();

		$login_id = $this->session->userdata('login_id');
		$role = $this->session->userdata('login_role');

		//マスター以外は編集不可
		if($role != 1)
		{
			$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
			redirect("auth/home");
		}

		//マスターIDの場合エラー
		if($posts['id']==1){
			$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
			redirect("auth/home");
		}

		if($posts['id'] && is_numeric($posts['id'])){
			$data = array(
			               'del' => 1
		            );
			$this->db->where('id', $posts['id']);
			$ret = $this->db->update('auths', $data); 

			if($ret){
				//削除成功
				$this->session->set_flashdata(array("msg"=>"削除が完了しました。"));
				redirect("auth/home");
			}else{
				$this->session->set_flashdata(array("msg"=>"削除に失敗しました。"));
				redirect("auth/home");
			}
		}else{
			redirect("auth/login");
		}
	}

	//////////////////////////////////////////////////////////
	//パスワード忘れ
	//////////////////////////////////////////////////////////
	function forget_password()
	{
		$error="";
		$data["render"] = "auth/forget_password";
		$data["clean"] = $this->clean;
		$data["csrf"] = $this->_get_csrf_nonce();
		$data["msg"] = $error;
		$data["segment"] = $this->segment;

		$this->load->view("template", $data);
	}

	//////////////////////////////////////////////////////////
	//パスワード再発行メール
	//////////////////////////////////////////////////////////
	function send_mail()
	{
		$posts = $this->input->post();

		//バリデーション実行
		$error = "";
		$error = $this->setSendmailValidation();

		$sameEmail="";
		if(empty($error)){
			$sameEmail = $this->db->get_where('auths', array('email' => $posts['email'], 'del'=>0))->row();

			if($sameEmail){
				$token = $this->generatePassword();
				//DBにforget_passwordトークン登録
				$data = array(
					'forget_password' => $token,
					'forget_password_limit' => strtotime("tomorrow")
			            );
				$this->db->where('email', $posts['email']);
				$ret = $this->db->update('auths', $data);

				$url = base_url() . "auth/create_password/?re=" . $token;

				$this->load->library('email');

				$this->email->from('info@smt-net.co.jp', '管理者');
				$this->email->to($posts['email']); 

				$this->email->subject('パスワード再発行メール');
				$this->email->message("以下のURLにアクセスしてください。\n" . $url);	

				$this->email->send();

				$error.="<p>パスワード再発行メールを送信しました。</p>";
			}else{
				$error.="<p>メールアドレスが存在しません。</p>";
			}
		}

		//data設定
		$data["auth"] =  (object) $posts;
		$data["render"] = "auth/forget_password";
		$data["clean"] = $this->clean;
		$data["csrf"] = $this->_get_csrf_nonce();
		$data["msg"] = $error;
		$data["segment"] = $this->segment;

 		$this->load->view("template", $data);
	}

	//////////////////////////////////////////////////////////
	//パスワードバリデーション
	//////////////////////////////////////////////////////////
	private function setSendmailValidation($validation=null)
	{

		$errors ="";
		$this->form_validation->set_rules("email", "メールアドレス","required|valid_email");

		if($this->form_validation->run() == FALSE){
			$errors = validation_errors();
		}
		return $errors;
	}

	//////////////////////////////////////////////////////////
	//パスワード再発行用のトークン
	//////////////////////////////////////////////////////////
	private function generatePassword ()
	{
		$len = 16;
		srand ( (double) microtime () * 1000000);
		$seed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		$pass = "";
		while ($len--) {
			$pos = rand(0,61);
			$pass .= $seed[$pos];
		}
		return $pass;
	}

	//////////////////////////////////////////////////////////
	//パスワード再発行画面
	//////////////////////////////////////////////////////////
	function create_password()
	{
		$code = $this->input->get('re', TRUE);
		$data['auth'] = $this->db->get_where('auths', array('forget_password' => $code, 'del'=>0))->row();

		if(empty($data['auth'])){
			redirect("auth/login");
		}

		$error="";
		$data["render"] = "auth/create_password";
		$data["clean"] = $this->clean;
		$data["csrf"] = $this->_get_csrf_nonce();
		$data["msg"] = $error;
		$data["segment"] = $this->segment;

		$this->load->view("template", $data);
	}

	//////////////////////////////////////////////////////////
	//パスワード再発行
	//////////////////////////////////////////////////////////
	function new_password()
	{
		//CSRFチェック
		if ($this->_valid_csrf_nonce() === FALSE)
		{
			$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
			redirect("article/home");
		}

		$posts = $this->input->post();

		//バリデーション実行
		$error = "";
		$error = $this->setNewPasswordValidation();

		if($posts['password'] != $posts['password_check']){
			$error.="<p>パスワードが一致しません。</p>";
		}

		$user = $this->db->get_where('auths', array('id' => $posts['id'], 'del'=>0))->row();
		$now = strtotime("now");

		if($user->forget_password_limit < $now){
			$this->session->set_flashdata(array("msg"=>"有効期限が切れました。再度パスワードの再発行の手続きをして下さい。"));
			redirect("article/home");
		}

		if($error !== ""){
			//data設定
			$data["auth"] =  (object) $posts;
			$data["render"] = "auth/create_password";
			$data["clean"] = $this->clean;
			$data["csrf"] = $this->_get_csrf_nonce();
			$data["msg"] = $error;
			$data["segment"] = $this->segment;

	 		$this->load->view("template", $data);
		}else{
			//パスワード再発行
			$pass = hash_hmac("sha256", $posts['password'], Salt);
			$user = hash_hmac("sha256", $posts['userid'], Salt);
			$salt = hash_hmac("sha256", Salt, Salt);
			$pass_unit = $user . $pass . $salt;
			$data['password'] = $pass_unit;
			$data['forget_password_limit'] = "";
			$data['forget_password'] = "";

			$this->load->model('auths_model');
			$ret = $this->auths_model->save($posts['id'], $data);
		
			if(empty($ret)){
				//エラー処理
			}else{
				//登録成功
				$this->session->set_flashdata(array("msg"=>"パスワードの更新が完了しました。"));
				redirect("auth/login");
			}
		}

	}

	//////////////////////////////////////////////////////////
	//バリデーション
	//////////////////////////////////////////////////////////
	private function setNewPasswordValidation($validation=null)
	{
		$errors ="";
		$this->form_validation->set_rules("password", "パスワード","required|min_length[12]|max_length[20]");
		$this->form_validation->set_rules("password_check", "パスワード(確認用）","required|min_length[12]|max_length[20]");

		if($this->form_validation->run() == FALSE){
			$errors = validation_errors();
		}
		return $errors;
	}
}
