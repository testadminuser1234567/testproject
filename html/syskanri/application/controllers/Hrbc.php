<?php defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('memory_limit', '512M');
class Hrbc extends MY_Controller {

	protected $page_limit = 20;

	protected $hrbc_limit = 200;
	protected $hrbc_job_url = "https://api-hrbc-jp.porterscloud.com/v1/job";
	protected $hrbc_client_url = "https://api-hrbc-jp.porterscloud.com/v1/client";
	protected $hrbc_recruiter_url = "https://api-hrbc-jp.porterscloud.com/v1/recruiter";
	protected $hrbc_option_url = "https://api-hrbc-jp.porterscloud.com/v1/option";
	protected $hrbc_user_url = "https://api-hrbc-jp.porterscloud.com/v1/user";

	function __construct()
	{
		parent::__construct();
	}

	//////////////////////////////////////////////////////////
	//一覧出力
	//////////////////////////////////////////////////////////
	function home()
	{
		$posts = $this->input->post();

		$msg = $this->session->userdata('msg');
		$this->session->unset_userdata('msg');

		//メンテナンス
		$maintenance = $this->db->get_where("maintenances", array("id"=>1))->row();
		$data["maintenance"] = $maintenance;

		//data設定
		$data["render"] = "hrbc/home";
		$data["clean"] = $this->clean;
		$data["csrf"] = $this->_get_csrf_nonce();
		$data["msg"] = $msg;

 		$this->load->view("template", $data);
	}

	//////////////////////////////////////////////////////////
	//JOB更新
	//////////////////////////////////////////////////////////
	function job()
	{
		//同時実行防止
		$fpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/tmp/job.txt";
		if(!file_exists($fpass)){
			$fp = fopen($fpass, "w");
		}else{
			//ホームに戻る
			$msg="処理中です<br />暫くしてから実行して下さい。";
			$msg = $this->session->set('msg', $msg);
			$this->request->redirect('hrbc/home');
		}
				
		$msg="";
		$page=1;
		$offset=0;
		$limit=$this->hrbc_limit;
		$jobs=array();
		$prefectures=array();
		$jobcategories=array();
		$employments=array();
		$tenkins=array();
		$actives=array();

		$counter1=0;
		$counter2=0;
		$counter3=0;
		$counter4=0;
		$counter5=0;
		$counter6=0;
  
		$ret = $this->getJobInfo($page, $offset, $limit, $jobs, $prefectures, $jobcategories, $employments, $tenkins, $actives, $counter1, $counter2, $counter3, $counter4, $counter4, "", "");

		//企業と業種情報取得成功
		if(!$ret){
			$msg = "正常終了しました。";
		}else{
			$msg = $ret;
		}

		//同時実行防止ファイルの削除
		unlink($fpass);

		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");

	}

	//////////////////////////////////////////////////////////
	//JOB更新（期間抽出）
	//////////////////////////////////////////////////////////
	function joblimit()
	{
		$posts = $this->input->post();

		if(empty($posts['start']) || empty($posts['end'])){
			//ホームに戻る
			$msg="抽出期間を設定してください。";
			$this->session->set_userdata(array("msg"=>$msg));
			redirect("hrbc/home");
		}else{
			$msg="";

			$startAry = explode("/", $posts['start']);
			if(is_array($startAry) && !empty($startAry[0]) && !empty($startAry[1]) && !empty($startAry[2])){
				if( !checkdate($startAry[1], $startAry[2], $startAry[0]) ){
					$msg.="抽出開始日の形式が違います<br>";
				}
			}else{
				$msg.="抽出開始日の形式が違います<br>";
			}

			$endAry = explode("/", $posts['end']);
			if(is_array($endAry) && !empty($endAry[0]) && !empty($endAry[1]) && !empty($endAry[2])){
				if( !checkdate($endAry[1], $endAry[2], $endAry[0]) ){
					$msg.="抽出終了日の形式が違います<br>";
				}
			}else{
				$msg.="抽出終了日の形式が違います<br>";
			}

			if($msg){
				$this->session->set_userdata(array("msg"=>$msg));
				redirect("hrbc/home");
			}

		}

		//同時実行防止
		$fpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/tmp/job.txt";
		if(!file_exists($fpass)){
			$fp = fopen($fpass, "w");
		}else{
			//ホームに戻る
			$msg="処理中です<br />暫くしてから実行して下さい。";
			$this->session->set_userdata(array("msg"=>$msg));
			redirect("hrbc/home");
		}

		$msg="";
		$page=1;
		$offset=0;
		$limit=$this->hrbc_limit;
		$jobs=array();
		$prefectures=array();
		$jobcategories=array();
		$employments=array();
		$tenkins=array();
		$actives=array();

		$counter1=0;
		$counter2=0;
		$counter3=0;
		$counter4=0;
		$counter5=0;
		$counter6=0;
  
		$ret = $this->getJobInfo($page, $offset, $limit, $jobs, $prefectures, $jobcategories, $employments, $tenkins,$actives, $counter1, $counter2, $counter3, $counter4, $counter5, $counter6, $posts['start'], $posts['end']);

		//企業と業種情報取得成功
		if(!$ret){
			$msg = "正常終了しました。";
		}else{
			$msg = $ret;
		}

		//同時実行防止ファイルの削除
		unlink($fpass);

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//ファイルクリア　※エラーでファイルが残った場合の対応
	//////////////////////////////////////////////////////////
	function jobclear()
	{
		//同時実行防止
		$fpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/tmp/job.txt";

		//同時実行防止ファイルの削除
		if(file_exists($fpass)){
			unlink($fpass);
		}
		$msg="対応が完了しました。<br />「更新する」をクリックして、データを更新して下さい。";
		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//企業情報登録
	//////////////////////////////////////////////////////////
	function getJobInfo($page, $offset, $limit, $jobs, $prefectures, $jobcategories, $employments, $tenkins, $actives, $counter1, $counter2, $counter3, $counter4, $counter5, $counter6, $start, $end)
	{
		$ret="";
		//sortflg
		$sortflg=0;

		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];

		$flg=0;

		$url = $this->hrbc_job_url; 

		//送信パラメータ
		//日付指定
		if($start && $end){
			$sortflg=1;
/*
			$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'start'=>$offset, 'condition'=>'Job.P_UpdateDate:ge=' . $start . ' 00:00:00,Job.P_UpdateDate:le=' . $end . ' 23:59:59' , 'field'=>'Job.P_Id,Job.P_Client,Job.P_Recruiter,Job.P_Owner,Job.P_Phase,Job.P_Position,Job.P_JobCategorySummary,Job.U_3CDEFF847916046D6F31A0C068159A,Job.P_JobCategory,Job.P_Area,Job.P_AreaSummary,Job.U_7D6856433D7C07220E89FA639106EA,Job.U_214F0F8477FB3FE93933CEFD6EA9D4,Job.U_AF3F580CBFC5EAF062C7F1CC9C0AA2,Job.U_30949ADEF041A03A07DD9DE59D5D70,Job.P_WokingHours,Job.P_Holidays,Job.P_EmploymentType,Job.P_Benefits,Job.P_MinSalary,Job.P_MaxSalary,Job.P_SalarySummary,Job.U_0B5014D29A9929E44B19D60BFA5020,Job.U_4E207619FF687BD492D88F1A5B947B,Job.P_RegistrationDate,Job.P_UpdateDate,Job.U_ACTIVE'));
*/
            $url = $this->hrbc_job_url .  "?partition=". $partition . "&count=".$limit. "&start=".$offset."&condition=Job.P_UpdateDate:ge=" . $start . "%2000:00:00,Job.P_UpdateDate:le=" . $end ."%2023:59:59&field=Job.P_Id,Job.P_Client,Job.P_Recruiter,Job.P_Owner,Job.P_Phase,Job.P_Position,Job.P_JobCategorySummary,Job.U_3CDEFF847916046D6F31A0C068159A,Job.P_JobCategory,Job.P_Area,Job.P_AreaSummary,Job.U_7D6856433D7C07220E89FA639106EA,Job.U_214F0F8477FB3FE93933CEFD6EA9D4,Job.U_AF3F580CBFC5EAF062C7F1CC9C0AA2,Job.U_30949ADEF041A03A07DD9DE59D5D70,Job.P_WokingHours,Job.P_Holidays,Job.P_EmploymentType,Job.P_Benefits,Job.P_MinSalary,Job.P_MaxSalary,Job.P_SalarySummary,Job.U_0B5014D29A9929E44B19D60BFA5020,Job.U_4E207619FF687BD492D88F1A5B947B,Job.P_RegistrationDate,Job.P_UpdateDate,Job.U_ACTIVE";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);


		//全レコード
		}else{
/*
			$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'start'=>$offset , 'field'=>'Job.P_Id,Job.P_Client,Job.P_Recruiter,Job.P_Owner,Job.P_Phase,Job.P_Position,Job.P_JobCategorySummary,Job.U_3CDEFF847916046D6F31A0C068159A,Job.P_JobCategory,Job.P_Area,Job.P_AreaSummary,Job.U_7D6856433D7C07220E89FA639106EA,Job.U_214F0F8477FB3FE93933CEFD6EA9D4,Job.U_AF3F580CBFC5EAF062C7F1CC9C0AA2,Job.U_30949ADEF041A03A07DD9DE59D5D70,Job.P_WokingHours,Job.P_Holidays,Job.P_EmploymentType,Job.P_Benefits,Job.P_MinSalary,Job.P_MaxSalary,Job.P_SalarySummary,Job.U_0B5014D29A9929E44B19D60BFA5020,Job.U_4E207619FF687BD492D88F1A5B947B,Job.P_RegistrationDate,Job.P_UpdateDate,Job.U_ACTIVE'));
*/

            $url = $this->hrbc_job_url .  "?partition=". $partition . "&count=".$limit. "&start=".$offset."&field=Job.P_Id,Job.P_Client,Job.P_Recruiter,Job.P_Owner,Job.P_Phase,Job.P_Position,Job.P_JobCategorySummary,Job.U_3CDEFF847916046D6F31A0C068159A,Job.P_JobCategory,Job.P_Area,Job.P_AreaSummary,Job.U_7D6856433D7C07220E89FA639106EA,Job.U_214F0F8477FB3FE93933CEFD6EA9D4,Job.U_AF3F580CBFC5EAF062C7F1CC9C0AA2,Job.U_30949ADEF041A03A07DD9DE59D5D70,Job.P_WokingHours,Job.P_Holidays,Job.P_EmploymentType,Job.P_Benefits,Job.P_MinSalary,Job.P_MaxSalary,Job.P_SalarySummary,Job.U_0B5014D29A9929E44B19D60BFA5020,Job.U_4E207619FF687BD492D88F1A5B947B,Job.P_RegistrationDate,Job.P_UpdateDate,Job.U_ACTIVE";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);
		}


/*
		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/
		//HRBCジョブカウント
		$this->HrbcCountCheck("job");

//		$xml = file_get_contents($unit_url, false, $context);
//		$xml = simplexml_load_string($xml);

		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("job登録でエラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//HRBCから帰ってきたデータ件数　1件の対応用に取得
		$cnt = $xml->attributes()->Count;

		//データ設定
		$json = json_encode($xml);
		$arr = json_decode($json,true);

		if($cnt==0){
		}else if($cnt!=1){
			foreach($arr['Item'] as $k => $v){
				$jobs[$counter1]['job_id'] = $v['Job.P_Id'];
				$jobs[$counter1]['client_id'] = @$v['Job.P_Client']['Client']['Client.P_Id'];
				$jobs[$counter1]['job_title'] = @$v['Job.P_Position'];
				$jobs[$counter1]['recruiter_id'] = @$v['Job.P_Recruiter']['Recruiter']['Recruiter.P_Id'];
				$jobs[$counter1]['owner_id'] = @$v['Job.P_Owner']['User']['User.P_Id'];
				$jobs[$counter1]['summary'] = @$v['Job.P_JobCategorySummary'];
				$jobs[$counter1]['background'] = @$v['Job.U_3CDEFF847916046D6F31A0C068159A'];
				$jobs[$counter1]['ideal'] = @$v['Job.U_AF3F580CBFC5EAF062C7F1CC9C0AA2'];
				$jobs[$counter1]['keywords'] = @$v['Job.U_214F0F8477FB3FE93933CEFD6EA9D4'];
				$jobs[$counter1]['area_detail'] = @$v['Job.P_AreaSummary'];
				$jobs[$counter1]['experience'] = @$v['Job.U_AF3F580CBFC5EAF062C7F1CC9C0AA2'];
				$jobs[$counter1]['qualification'] = @$v['Job.U_30949ADEF041A03A07DD9DE59D5D70'];
				$jobs[$counter1]['worktime'] = @$v['Job.P_WokingHours'];
				$jobs[$counter1]['holiday'] = @$v['Job.P_Holidays'];
				$jobs[$counter1]['benefits'] = @$v['Job.P_Benefits'];
				$jobs[$counter1]['minsalary'] = @$v['Job.P_MinSalary'];
				$jobs[$counter1]['maxsalary'] = @$v['Job.P_MaxSalary'];
				$jobs[$counter1]['salary'] = @$v['Job.P_SalarySummary'];
				$jobs[$counter1]['publish'] = @$v['Job.U_0B5014D29A9929E44B19D60BFA5020'];
				$jobs[$counter1]['j_created'] = date('Y-m-d H:i:s', strtotime(@$v['Job.P_RegistrationDate'])+9*60*60);
				$jobs[$counter1]['j_updated']  = date('Y-m-d H:i:s', strtotime(@$v['Job.P_UpdateDate'])+9*60*60);

				//フェーズ
				foreach($v['Job.P_Phase'] as $t=>$u){
					$jobs[$counter1]['phase'] = $u['Option.P_Id'];
				}

				//公開
				foreach($v['Job.U_0B5014D29A9929E44B19D60BFA5020'] as $t2=>$u2){
					$jobs[$counter1]['publish'] = $u2['Option.P_Id'];
				}

				//都道府県
				$pName="";
				$pId="";
				foreach($v['Job.P_Area'] as $k2=>$v2){
					$prefectures[$counter2]['job_id'] = $v['Job.P_Id'];
					$prefectures[$counter2]['p_alias'] = $k2;
	
					
					foreach($v2 as $k3 => $v3){
						if($k3=="Option.P_Name") $pName .= $v3 . ",";
						if($k3=="Option.P_Id") $pId .= $v3 . ",";
					}
	
					$counter2++;
				}
				 $jobs[$counter1]['prefecture_name']= rtrim($pName, ",");
				 $jobs[$counter1]['prefecture_id']= rtrim($pId, ",");

				//業種
				$jcName="";
				$jcId="";
				foreach($v['Job.P_JobCategory'] as $k4=>$v4){
					$jobcategories[$counter3]['job_id'] = $v['Job.P_Id'];
					$jobcategories[$counter3]['jc_alias'] = $k4;
	
					foreach($v4 as $k5 => $v5){
						if($k5=="Option.P_Name") $jcName.=$v5.",";
						if($k5=="Option.P_Id") $jcId.=$v5.",";
					}
	
					$counter3++;
				}
				$jobs[$counter1]['jobcategory_name']= rtrim($jcName, ",");
				$jobs[$counter1]['jobcategory_id']= rtrim($jcId, ",");

				//雇用
				$eName="";
				$eId="";
				foreach($v['Job.P_EmploymentType'] as $k6=>$v6){
					$employments[$counter4]['job_id'] = $v['Job.P_Id'];
					$employments[$counter4]['e_alias'] = $k6;
	
					foreach($v6 as $k7 => $v7){
						if($k7=="Option.P_Name") $eName .= $v7 . ",";
						if($k7=="Option.P_Id") $eId .= $v7.",";
					}
	
					$counter4++;
				}
				 $jobs[$counter1]['employ_name']=rtrim($eName,",");
				 $jobs[$counter1]['employ_id']=rtrim($eId,",");


				foreach($v['Job.U_7D6856433D7C07220E89FA639106EA'] as $k7=>$v7){
					if($v7['Option.P_Id']=="10591"){
						$jobs[$counter1]['recommend'] = @$v7['Option.P_Id'];
					}
				}

				//転勤
				$tName="";
				$tId="";
				foreach($v['Job.U_4E207619FF687BD492D88F1A5B947B'] as $k8=>$v8){
					$tenkins[$counter5]['job_id'] = $v['Job.P_Id'];
					$tenkins[$counter5]['t_alias'] = $k8;
	
					foreach($v8 as $k9 => $v9){
						if($k9=="Option.P_Name") $tName .= $v9 . ",";
						if($k9=="Option.P_Id") $tId .= $v9.",";
					}
	
					$counter5++;
				}
				 $jobs[$counter1]['tenkin_name']=rtrim($tName,",");
				 $jobs[$counter1]['tenkin_id']=rtrim($tId,",");

				//注目求人
				$aName="";
				$aId="";
				foreach($v['Job.U_ACTIVE'] as $k10=>$v10){
					$actives[$counter6]['job_id'] = $v['Job.P_Id'];
					$actives[$counter6]['a_alias'] = $k10;
	
					foreach($v10 as $k11 => $v11){
						if($k11=="Option.P_Name") $aName = $v11;
						if($k11=="Option.P_Id") $aId = $v11;
					}
	
					$counter6++;
				}
				 $jobs[$counter1]['active_name']=rtrim($aName,",");
				 $jobs[$counter1]['active_flg']=rtrim($aId,",");

				$counter1++;
			}
			//1件のデータは返却の形式が異なるので個別対応
		}else{
			$jobs[$counter1]['job_id'] = $arr['Item']['Job.P_Id'];
			$jobs[$counter1]['client_id'] = $arr['Item']['Job.P_Client']['Client']['Client.P_Id'];
			$jobs[$counter1]['job_title'] = $arr['Item']['Job.P_Position'];
			$jobs[$counter1]['recruiter_id'] = $arr['Item']['Job.P_Recruiter']['Recruiter']['Recruiter.P_Id'];
			$jobs[$counter1]['owner_id'] = $arr['Item']['Job.P_Owner']['User']['User.P_Id'];
			$jobs[$counter1]['summary'] = $arr['Item']['Job.P_JobCategorySummary'];
			$jobs[$counter1]['ideal'] = $arr['Item']['Job.U_AF3F580CBFC5EAF062C7F1CC9C0AA2'];
			$jobs[$counter1]['background'] = $arr['Item']['Job.U_3CDEFF847916046D6F31A0C068159A'];
			$jobs[$counter1]['keywords'] = $arr['Item']['Job.U_214F0F8477FB3FE93933CEFD6EA9D4'];
			$jobs[$counter1]['area_detail'] = $arr['Item']['Job.P_AreaSummary'];
			$jobs[$counter1]['experience'] = $arr['Item']['Job.U_AF3F580CBFC5EAF062C7F1CC9C0AA2'];
			$jobs[$counter1]['qualification'] = $arr['Item']['Job.U_30949ADEF041A03A07DD9DE59D5D70'];
			$jobs[$counter1]['worktime'] = $arr['Item']['Job.P_WokingHours'];
			$jobs[$counter1]['holiday'] = $arr['Item']['Job.P_Holidays'];
			$jobs[$counter1]['employment_type'] = $arr['Item']['Job.P_EmploymentType'];
			$jobs[$counter1]['benefits'] = $arr['Item']['Job.P_Benefits'];
			$jobs[$counter1]['minsalary'] = $arr['Item']['Job.P_MinSalary'];
			$jobs[$counter1]['maxsalary'] = $arr['Item']['Job.P_MaxSalary'];
			$jobs[$counter1]['salary'] = @$arr['Item']['Job.P_SalarySummary'];
			$jobs[$counter1]['j_created'] = date('Y-m-d H:i:s', strtotime($arr['Item']['Job.P_RegistrationDate'])+9*60*60);
			$jobs[$counter1]['j_updated']  = date('Y-m-d H:i:s', strtotime($arr['Item']['Job.P_UpdateDate'])+9*60*60);

			foreach($arr['Item']['Job.P_Phase'] as $t=>$u){
				$jobs[$counter1]['phase'] = $u['Option.P_Id'];
			}

			foreach($arr['Item']['Job.U_0B5014D29A9929E44B19D60BFA5020'] as $t2=>$u2){
				$jobs[$counter1]['publish'] = $u2['Option.P_Id'];
			}

			//都道府県
			$pName="";
			$pId="";
			foreach($arr['Item']['Job.P_Area'] as $k2=>$v2){
				$prefectures[$counter2]['job_id'] = $arr['Item']['Job.P_Id'];
				$prefectures[$counter2]['p_alias'] = $k2;
	
				foreach($v2 as $k3 => $v3){
					if($k3=="Option.P_Name") $pName .= $v3 . ",";
					if($k3=="Option.P_Id") $pId .= $v3 . ",";
				}
	
				$counter2++;
			}
			 $jobs[$counter1]['prefecture_name']= rtrim($pName, ",");
			 $jobs[$counter1]['prefecture_id']= rtrim($pId, ",");

			//業種
			$jcName="";
			$jcId="";
			foreach($arr['Item']['Job.P_JobCategory'] as $k4=>$v4){
				$jobcategories[$counter3]['job_id'] = $arr['Item']['Job.P_Id'];
				$jobcategories[$counter3]['jc_alias'] = $k4;
	
				foreach($v4 as $k5 => $v5){
					if($k5=="Option.P_Name") $jcName .= $v5.",";
					if($k5=="Option.P_Id") $jcId .= $v5 . ",";
				}

				$counter3++;
			}
			$jobs[$counter1]['jobcategory_name']= rtrim($jcName, ",");
			$jobs[$counter1]['jobcategory_id']= rtrim($jcId, ",");

			//雇用
			$eName="";
			$eId="";
			foreach($arr['Item']['Job.P_EmploymentType'] as $k6=>$v6){
				$employments[$counter4]['job_id'] = $arr['Item']['Job.P_Id'];
				$employments[$counter4]['jc_alias'] = $k4;
	
				foreach($v6 as $k7 => $v7){
					if($k7=="Option.P_Name") $eName .= $v7 . ",";
					if($k7=="Option.P_Id") $eId .= $v7 . ",";
				}

				$counter4++;
			}
			 $jobs[$counter1]['employ_name']=rtrim($eName,",");
			 $jobs[$counter1]['employ_id']=rtrim($eId,",");

			foreach($arr['Item']['Job.U_7D6856433D7C07220E89FA639106EA'] as $k7=>$v7){
				if($v7['Option.P_Id']=="10591"){
					$jobs[$counter1]['recommend'] = @$v7['Option.P_Id'];
				}
			}

			//転勤
			$tName="";
			$tId="";
			foreach($arr['Item']['Job.U_4E207619FF687BD492D88F1A5B947B'] as $k8=>$v8){
				$tenkins[$counter5]['job_id'] = $arr['Item']['Job.P_Id'];
				$tenkins[$counter5]['jc_alias'] = $k4;
	
				foreach($v8 as $k9 => $v9){
					if($k9=="Option.P_Name") $tName .= $v9 . ",";
					if($k9=="Option.P_Id") $tId .= $v9 . ",";
				}

				$counter5++;
			}
			$jobs[$counter1]['tenkin_name']=rtrim($tName,",");
			$jobs[$counter1]['tenkin_id']=rtrim($tId,",");

			//注目求人
			$aName="";
			$aId="";
			foreach($arr['Item']['Job.U_ACTIVE'] as $k10=>$v10){
				$actives[$counter6]['job_id'] = $arr['Item']['Job.P_Id'];
				$actives[$counter6]['a_alias'] = $k10;
	
				foreach($v10 as $k11 => $v11){
					if($k11=="Option.P_Name") $aName = $v11;
					if($k11=="Option.P_Id") $aId = $v11;
				}
	
				$counter6++;
			}
			 $jobs[$counter1]['active_name']=rtrim($aName,",");
			 $jobs[$counter1]['active_flg']=rtrim($aId,",");

			$counter1++;
		}

	
		//データ件数
		$total = $xml->attributes()->Total;
		if($total==0){
			$flg=1;
		}

		//ページ数
		$max = ceil($total / $limit);
		//総ページが１なら終了
		if($max==1){
			$flg=1;
		}

		//ページャーカウントアップ
		$page = $page + 1;
		//次に取得する件数
		$move_page = ($page) * $limit;


		//総ページ数を超えたら終了
		if($page > $max){
			$flg=1;
		//保険で100回超えたら終了
		}else if($page > 100){
			$flg=1;
		}

		//offset取得
		$offset = $page * $limit - $limit;
	
		if($flg!=1){
			$this->getJobInfo($page, $offset, $limit, $jobs, $prefectures, $jobcategories, $employments, $tenkins,$actives, $counter1, $counter2, $counter3, $counter4, $counter5, $counter6, $start, $end);
		}else{
			if($total==0){
				$ret = "データは0件でした";
			}else{
				//DB格納
				$ret = $this->setJobsInfo($jobs, $prefectures, $jobcategories, $employments, $tenkins,$actives, $total, $sortflg);
			}
		}
		return $ret;
	}

	///////////////////////////////////////////////////////////////////////////////
	// DB格納処理
	///////////////////////////////////////////////////////////////////////////////
	function setJobsInfo($jobs, $prefectures, $jobcategories, $employments, $tenkins,$actives, $total, $sortflg)
	{
		$ret="";

		//DB格納
		$dns = 'mysql:dbname=' . dbDatabase . ';host=' . dbHostname . ';port=' . dbPort;
		$user = dbUsername;
		$password = dbPassword;
		
		try{
			$pdo = new PDO($dns, $user, $password,
				array(
				     	PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET `utf8`"
				)
			);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		}catch (PDOException $e){
			print('Error:'.$e->getMessage());
			die();
		}

		//期間抽出の場合はインサートとアップデート
		if($sortflg==1){
			//////////////////////////////////////////////////////
			// job table挿入
			/////////////////////////////////////////////////////
			try {
				foreach($jobs as $k=>$v){
					$pdo->beginTransaction();
					$stmt = $pdo -> prepare("INSERT INTO jobs (job_id, job_title, active_flg,recruiter_id, owner_id, recommend, keywords, background, ideal, summary, area_detail, experience, qualification, worktime, holiday, benefits, minsalary, maxsalary, salary, prefecture_id, prefecture_name, jobcategory_id, jobcategory_name, employ_id, employ_name, tenkin_id, tenkin_name, client_id, phaze, publish, j_created, j_updated)  VALUES  (:job_id, :job_title, :active_flg, :recruiter_id, :owner_id, :recommend, :keywords, :background, :ideal, :summary, :area_detail, :experience, :qualification, :worktime, :holiday, :benefits, :minsalary, :maxsalary,:salary, :prefecture_id, :prefecture_name, :jobcategory_id, :jobcategory_name, :employ_id, :employ_name, :tenkin_id, :tenkin_name, :client_id, :phaze, :publish, :j_created, :j_updated) ON DUPLICATE KEY UPDATE job_title = :job_title, active_flg = :active_flg, recruiter_id =:recruiter_id, owner_id =:owner_id, recommend = :recommend, keywords = :keywords, background =  :background, ideal = :ideal, summary = :summary, area_detail =  :area_detail, experience = :experience, qualification = :qualification, worktime = :worktime, holiday = :holiday, benefits = :benefits, minsalary= :minsalary,maxsalary= :maxsalary, salary= :salary,prefecture_id= :prefecture_id,prefecture_name= :prefecture_name,jobcategory_id= :jobcategory_id,jobcategory_name= :jobcategory_name,employ_id= :employ_id,employ_name= :employ_name,tenkin_id= :tenkin_id,tenkin_name= :tenkin_name,client_id= :client_id,phaze= :phaze,publish= :publish,j_created= :j_created,j_updated= :j_updated");
					$stmt->bindParam(':job_id', $job_id, PDO::PARAM_INT);
					$stmt->bindParam(':job_title', $job_title, PDO::PARAM_STR);
					$stmt->bindParam(':active_flg', $active_flg, PDO::PARAM_INT);
					$stmt->bindParam(':recruiter_id', $recruiter_id, PDO::PARAM_STR);
					$stmt->bindParam(':owner_id', $owner_id, PDO::PARAM_STR);
					$stmt->bindParam(':recommend', $recommend, PDO::PARAM_INT);
					$stmt->bindParam(':background', $background, PDO::PARAM_STR);
					$stmt->bindParam(':ideal', $ideal, PDO::PARAM_STR);
					$stmt->bindParam(':keywords', $keywords, PDO::PARAM_STR);
					$stmt->bindParam(':summary', $summary, PDO::PARAM_STR);
					$stmt->bindParam(':area_detail', $area_detail, PDO::PARAM_STR);
					$stmt->bindParam(':experience', $experience, PDO::PARAM_STR);
					$stmt->bindParam(':qualification', $qualification, PDO::PARAM_STR);
					$stmt->bindParam(':worktime', $worktime, PDO::PARAM_STR);
					$stmt->bindParam(':holiday', $holiday, PDO::PARAM_STR);
					$stmt->bindParam(':benefits', $benefits, PDO::PARAM_STR);
					$stmt->bindParam(':minsalary', $minsalary, PDO::PARAM_STR);
					$stmt->bindParam(':maxsalary', $maxsalary, PDO::PARAM_STR);
					$stmt->bindParam(':salary', $salary, PDO::PARAM_STR);
					$stmt->bindParam(':prefecture_id', $prefecture_id, PDO::PARAM_STR);
					$stmt->bindParam(':prefecture_name', $prefecture_name, PDO::PARAM_STR);
					$stmt->bindParam(':jobcategory_id', $jobcategory_id, PDO::PARAM_STR);
					$stmt->bindParam(':jobcategory_name', $jobcategory_name, PDO::PARAM_STR);
					$stmt->bindParam(':employ_id', $employ_id, PDO::PARAM_STR);
					$stmt->bindParam(':employ_name', $employ_name, PDO::PARAM_STR);
					$stmt->bindParam(':tenkin_id', $tenkin_id, PDO::PARAM_STR);
					$stmt->bindParam(':tenkin_name', $tenkin_name, PDO::PARAM_STR);
					$stmt->bindParam(':client_id', $client_id, PDO::PARAM_INT);
					$stmt->bindParam(':phaze', $phaze, PDO::PARAM_INT);
					$stmt->bindParam(':publish', $publish, PDO::PARAM_INT);
					$stmt->bindParam(':j_created', $v['j_created'], PDO::PARAM_STR);
					$stmt->bindParam(':j_updated', $v['j_updated'], PDO::PARAM_STR);

					$job_id = empty($v['job_id']) ? "" : $v['job_id'];
					$job_title = empty($v['job_title']) ? "" : $v['job_title'];
	
					if(!empty($v['recommend'])){
						if(is_array($v['recommend'])){
							$tmprecommend="";
							foreach($v['recommend'] as $tmpk => $tmpv){
								$tmprecommend.=$tmpv;
							}
							$recommend = $tmprecommend;
						}else{
							$recommend = $v['recommend'];
						}
					}else{
						$recommend = 0;
					}

					if(!empty($v['keywords'])){
						if(is_array($v['keywords'])){
							$tmpkeywords="";
							foreach($v['keywords'] as $tmpk => $tmpv){
								$tmpkeywords.=$tmpv;
							}
							$keywords = $tmpkeywords;
						}else{
							$keywords = $v['keywords'];
						}
					}else{
						$keywords = "";
					}

					if(!empty($v['background'])){
						if(is_array($v['background'])){
							$tmpbackground="";
							foreach($v['background'] as $tmpk => $tmpv){
								$tmpbackground.=$tmpv;
							}
							$background = $tmpbackground;
						}else{
							$background = $v['background'];
						}
					}else{
						$background = "";
					}

					if(!empty($v['ideal'])){
						if(is_array($v['ideal'])){
							$tmpideal="";
							foreach($v['ideal'] as $tmpk => $tmpv){
								$tmpideal.=$tmpv;
							}
							$ideal = $tmpideal;
						}else{
							$ideal = $v['ideal'];
						}
					}else{
						$ideal = "";
					}

					if(!empty($v['summary'])){
						if(is_array($v['summary'])){
							$tmpsummary="";
							foreach($v['summary'] as $tmpk => $tmpv){
								$tmpsummary.=$tmpv;
							}
							$summary = $tmpsummary;
						}else{
							$summary = $v['summary'];
						}
					}else{
						$summary = "";
					}
	
					if(!empty($v['area_detail'])){
						if(is_array($v['area_detail'])){
						$tmparea_detail="";
						foreach($v['area_detail'] as $tmpk => $tmpv){
							$tmparea_detail.=$tmpv;
						}
						$area_detail = $tmparea_detail;
					}else{
						$area_detail = $v['area_detail'];
					}
					}else{
						$area_detail = "";
					}

					if(!empty($v['experience'])){
						if(is_array($v['experience'])){
							$tmpexperience="";
							foreach($v['experience'] as $tmpk => $tmpv){
								$tmpexperience.=$tmpv;
							}
							$experience = $tmpexperience;
						}else{
							$experience = $v['experience'];
						}
					}else{
						$experience = "";
					}

					if(!empty($v['qualification'])){
						if(is_array($v['qualification'])){
							$tmpqualification="";
							foreach($v['qualification'] as $tmpk => $tmpv){
								$tmpqualification.=$tmpv;
							}
							$qualification = $tmpqualification;
						}else{
							$qualification = $v['qualification'];
						}
					}else{
						$qualification = "";
					}

					if(!empty($v['worktime'])){
						if(is_array($v['worktime'])){
							$tmpworktime="";
							foreach($v['worktime'] as $tmpk => $tmpv){
								$tmpworktime.=$tmpv;
							}
							$worktime = $tmpworktime;
						}else{
							$worktime = $v['worktime'];
						}
					}else{
						$worktime = "";
					}

					if(!empty($v['holiday'])){
						if(is_array($v['holiday'])){
							$tmpholiday="";
							foreach($v['holiday'] as $tmpk => $tmpv){
								$tmpholiday.=$tmpv;
							}
							$holiday = $tmpholiday;
						}else{
							$holiday = $v['holiday'];
						}
					}else{
						$holiday = "";
					}
	
					if(!empty($v['benefits'])){
						if(is_array($v['benefits'])){
							$tmpbenefits="";
							foreach($v['benefits'] as $tmpk => $tmpv){
								$tmpbenefits.=$tmpv;
							}
							$benefits = $tmpbenefits;
						}else{
							$benefits = $v['benefits'];
						}
					}else{
						$benefits = "";
					}

					if(!empty($v['minsalary'])){
						if(is_array($v['minsalary'])){
							$tmpminsalary="";
							foreach($v['minsalary'] as $tmpk => $tmpv){
								$tmpminsalary.=$tmpv;
							}
							$minsalary = $tmpminsalary;
						}else{
							$minsalary = $v['minsalary'];
						}
					}else{
						$minsalary = 0;
					}

					if(!empty($v['maxsalary'])){
						if(is_array($v['maxsalary'])){
							$tmpmaxsalary="";
							foreach($v['maxsalary'] as $tmpk => $tmpv){
								$tmpmaxsalary.=$tmpv;
							}
							$maxsalary = $tmpmaxsalary;
						}else{
							$maxsalary = $v['maxsalary'];
						}
					}else{
						$maxsalary = 0;
					}

					if(!empty($v['salary'])){
						if(is_array($v['salary'])){
							$tmpsalary="";
							foreach($v['maxssalaryalary'] as $tmpk => $tmpv){
								$tmpsalary.=$tmpv;
							}
							$salary = $tmpsalary;
						}else{
							$salary = $v['salary'];
						}
					}else{
						$salary = "";
					}

					if(!empty($v['prefecture_id'])){
						if(is_array($v['prefecture_id'])){
							$tmpprefecture_id="";
							foreach($v['prefecture_id'] as $tmpk => $tmpv){
								$tmpprefecture_id.=$tmpv;
							}
							$prefecture_id = $tmpprefecture_id;
						}else{
							$prefecture_id = $v['prefecture_id'];
						}
					}else{
						$prefecture_id = "";
					}

					if(!empty($v['prefecture_name'])){
						if(is_array($v['prefecture_name'])){
							$tmpprefecture_name="";
							foreach($v['prefecture_name'] as $tmpk => $tmpv){
								$tmpprefecture_name.=$tmpv;
							}
							$prefecture_name = $tmpprefecture_name;
						}else{
							$prefecture_name = $v['prefecture_name'];
						}
					}else{
						$prefecture_name = "";
					}

					if(!empty($v['jobcategory_id'])){
						if(is_array($v['jobcategory_id'])){
							$tmpjobcategory_id="";
							foreach($v['jobcategory_id'] as $tmpk => $tmpv){
								$tmpjobcategory_id.=$tmpv;
							}
							$jobcategory_id = $tmpjobcategory_id;
						}else{
							$jobcategory_id = $v['jobcategory_id'];
						}
					}else{
						$jobcategory_id = "";
					}

					if(!empty($v['jobcategory_name'])){
						if(is_array($v['jobcategory_name'])){
							$tmpjobcategory_name="";
							foreach($v['jobcategory_name'] as $tmpk => $tmpv){
								$tmpjobcategory_name.=$tmpv;
							}
							$jobcategory_name = $tmpjobcategory_name;
						}else{
							$jobcategory_name = $v['jobcategory_name'];
						}
					}else{
						$jobcategory_name = "";
					}

					if(!empty($v['employ_id'])){
						if(is_array($v['employ_id'])){
							$tmpemploy_id="";
							foreach($v['employ_id'] as $tmpk => $tmpv){
								$tmpemploy_id.=$tmpv;
							}
							$employ_id = $tmpemploy_id;
						}else{
							$employ_id = $v['employ_id'];
						}
					}else{
						$employ_id = "";
					}

					if(!empty($v['employ_name'])){
						if(is_array($v['employ_name'])){
							$tmpemploy_name="";
							foreach($v['employ_name'] as $tmpk => $tmpv){
								$tmpemploy_name.=$tmpv;
							}
							$employ_name = $tmpemploy_name;
						}else{
							$employ_name = $v['employ_name'];
						}
					}else{
						$employ_name = "";
					}

					if(!empty($v['tenkin_id'])){
						if(is_array($v['tenkin_id'])){
							$tmptenkin_id="";
							foreach($v['tenkin_id'] as $tmpk => $tmpv){
								$tmptenkin_id.=$tmpv;
							}
							$tenkin_id = $tmptenkin_id;
						}else{
							$tenkin_id = $v['tenkin_id'];
						}
					}else{
						$tenkin_id = "";
					}

					if(!empty($v['tenkin_name'])){
						if(is_array($v['tenkin_name'])){
							$tmptenkin_name="";
							foreach($v['tenkin_name'] as $tmpk => $tmpv){
								$tmptenkin_name.=$tmpv;
							}
							$tenkin_name = $tmptenkin_name;
						}else{
							$tenkin_name = $v['tenkin_name'];
						}
					}else{
						$tenkin_name = "";
					}

					if(!empty($v['active_flg'])){
						if(is_array($v['active_flg'])){
							$tmpactive_flg="";
							foreach($v['active_flg'] as $tmpk => $tmpv){
								$tmpactive_flg.=$tmpv;
							}
							$active_flg = $tmpactive_flg;
						}else{
							$active_flg = $v['active_flg'];
						}
					}else{
						$active_flg = 0;
					}

					if(!empty($v['client_id'])){
						if(is_array($v['client_id'])){
							$tmpclient_id="";
							foreach($v['client_id'] as $tmpk => $tmpv){
								$tmpclient_id.=$tmpv;
							}
							$client_id = $tmpclient_id;
						}else{
							$client_id = $v['client_id'];
						}
					}else{
						$client_id = 0;
					}

					if(!empty($v['recruiter_id'])){
						if(is_array($v['recruiter_id'])){
							$tmprecruiter_id="";
							foreach($v['recruiter_id'] as $tmpk => $tmpv){
								$tmprecruiter_id.=$tmpv;
							}
							$recruiter_id = $tmprecruiter_id;
						}else{
							$recruiter_id = $v['recruiter_id'];
						}
					}else{
						$recruiter_id = 0;
					}

					if(!empty($v['owner_id'])){
						if(is_array($v['owner_id'])){
							$tmpowner_id="";
							foreach($v['owner_id'] as $tmpk => $tmpv){
								$tmpowner_id.=$tmpv;
							}
							$owner_id = $tmpowner_id;
						}else{
							$owner_id = $v['owner_id'];
						}
					}else{
						$owner_id = 0;
					}

					$phaze = empty($v['phase']) ? 0 : $v['phase'];
					$publish = empty($v['publish']) ? 0 : $v['publish'];
				

					$r = $stmt->execute();
					if (!$r) {
						throw new Exception('insert jobs 失敗');
					}
					//commit
					$pdo->commit();
				}
			} catch (PDOException $e) {
				//rollback
				$pdo->rollBack();
				print('Error:jobs' . " " .$e->getMessage());
				die();
			}

		//全件データ更新
		}else{
			$d = Date('Ymdhis');

			/////////////////////////////////////////////////////
			// 企業情報の設定
			/////////////////////////////////////////////////////
			$c_db = "jobs";
			$db = $c_db . "_" . $d;

			//////////////////////////////////////////////////////
			// job table作成
			/////////////////////////////////////////////////////
			$sql = "CREATE TABLE IF NOT EXISTS `" . $db . "` (
			  `job_id` int(11) NOT NULL,
			  `job_title` varchar(255) NOT NULL,
			  `active_flg` int(11) NOT NULL,
			  `recruiter_id` varchar(255) NOT NULL,
			  `owner_id` varchar(255) NOT NULL,
			  `recommend` int(11) NOT NULL,
			  `keywords` text NOT NULL,
			  `ideal` text NOT NULL,
			  `background` text NOT NULL,
			  `summary` text NOT NULL,
			  `area_detail` text NOT NULL,
			  `experience` text NOT NULL,
			  `qualification` text NOT NULL,
			  `worktime` text NOT NULL,
			  `holiday` text NOT NULL,
			  `benefits` text NOT NULL,
			  `minsalary` int(11) NOT NULL,
			  `maxsalary` int(11) NOT NULL,
			  `salary` text NOT NULL,
			  `prefecture_id` text NOT NULL,
			  `prefecture_name` text NOT NULL,
			  `jobcategory_id` text NOT NULL,
			  `jobcategory_name` text NOT NULL,
			  `employ_id` text NOT NULL,
			  `employ_name` text NOT NULL,
			  `tenkin_id` text NOT NULL,
			  `tenkin_name` text NOT NULL,
			  `client_id` int(11) NOT NULL,
			  `phaze` int(11) NOT NULL,
			  `publish` int(11) NOT NULL,
			  `j_created` datetime NOT NULL,
			  `j_updated` datetime NOT NULL,
			  PRIMARY KEY  (`job_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

			$stmt = $pdo -> prepare($sql);
			$r = $stmt -> execute();
			if (!$r) {
				throw new Exception('create 失敗');
			}

			//////////////////////////////////////////////////////
			// job table挿入
			/////////////////////////////////////////////////////
			try {
				foreach($jobs as $k=>$v){
					$pdo->beginTransaction();
					$stmt = $pdo -> prepare("INSERT INTO "  . $db . " (job_id, job_title, active_flg, recruiter_id, owner_id, recommend, keywords, background, ideal, summary, area_detail, experience, qualification, worktime, holiday, benefits, minsalary, maxsalary, salary, prefecture_id, prefecture_name, jobcategory_id, jobcategory_name, employ_id, employ_name, tenkin_id, tenkin_name, client_id, phaze, publish, j_created, j_updated) VALUES (:job_id, :job_title, :active_flg, :recruiter_id , :owner_id, :recommend, :keywords, :background, :ideal, :summary, :area_detail, :experience, :qualification, :worktime, :holiday, :benefits, :minsalary, :maxsalary, :salary, :prefecture_id, :prefecture_name, :jobcategory_id, :jobcategory_name, :employ_id, :employ_name, :tenkin_id, :tenkin_name, :client_id, :phaze, :publish, :j_created, :j_updated)");
					$stmt->bindParam(':job_id', $job_id, PDO::PARAM_INT);
					$stmt->bindParam(':job_title', $job_title, PDO::PARAM_STR);
					$stmt->bindParam(':active_flg', $active_flg, PDO::PARAM_INT);
					$stmt->bindParam(':recruiter_id', $recruiter_id, PDO::PARAM_STR);
					$stmt->bindParam(':owner_id', $owner_id, PDO::PARAM_STR);
					$stmt->bindParam(':recommend', $recommend, PDO::PARAM_INT);
					$stmt->bindParam(':background', $background, PDO::PARAM_STR);
					$stmt->bindParam(':ideal', $ideal, PDO::PARAM_STR);
					$stmt->bindParam(':keywords', $keywords, PDO::PARAM_STR);
					$stmt->bindParam(':summary', $summary, PDO::PARAM_STR);
					$stmt->bindParam(':area_detail', $area_detail, PDO::PARAM_STR);
					$stmt->bindParam(':experience', $experience, PDO::PARAM_STR);
					$stmt->bindParam(':qualification', $qualification, PDO::PARAM_STR);
					$stmt->bindParam(':worktime', $worktime, PDO::PARAM_STR);
					$stmt->bindParam(':holiday', $holiday, PDO::PARAM_STR);
					$stmt->bindParam(':benefits', $benefits, PDO::PARAM_STR);
					$stmt->bindParam(':minsalary', $minsalary, PDO::PARAM_STR);
					$stmt->bindParam(':maxsalary', $maxsalary, PDO::PARAM_STR);
					$stmt->bindParam(':salary', $salary, PDO::PARAM_STR);
					$stmt->bindParam(':prefecture_id', $prefecture_id, PDO::PARAM_STR);
					$stmt->bindParam(':prefecture_name', $prefecture_name, PDO::PARAM_STR);
					$stmt->bindParam(':jobcategory_id', $jobcategory_id, PDO::PARAM_STR);
					$stmt->bindParam(':jobcategory_name', $jobcategory_name, PDO::PARAM_STR);
					$stmt->bindParam(':employ_id', $employ_id, PDO::PARAM_STR);
					$stmt->bindParam(':employ_name', $employ_name, PDO::PARAM_STR);
					$stmt->bindParam(':tenkin_id', $tenkin_id, PDO::PARAM_STR);
					$stmt->bindParam(':tenkin_name', $tenkin_name, PDO::PARAM_STR);
					$stmt->bindParam(':client_id', $client_id, PDO::PARAM_INT);
					$stmt->bindParam(':phaze', $phaze, PDO::PARAM_INT);
					$stmt->bindParam(':publish', $publish, PDO::PARAM_INT);
					$stmt->bindParam(':j_created', $v['j_created'], PDO::PARAM_STR);
					$stmt->bindParam(':j_updated', $v['j_updated'], PDO::PARAM_STR);

					$job_id = empty($v['job_id']) ? "" : $v['job_id'];
					$job_title = empty($v['job_title']) ? "" : $v['job_title'];
	
					if(!empty($v['recommend'])){
						if(is_array($v['recommend'])){
							$tmprecommend="";
							foreach($v['recommend'] as $tmpk => $tmpv){
								$tmprecommend.=$tmpv;
							}
							$recommend = $tmprecommend;
						}else{
							$recommend = $v['recommend'];
						}
					}else{
						$recommend = 0;
					}

					if(!empty($v['keywords'])){
						if(is_array($v['keywords'])){
							$tmpkeywords="";
							foreach($v['keywords'] as $tmpk => $tmpv){
								$tmpkeywords.=$tmpv;
							}
							$keywords = $tmpkeywords;
						}else{
							$keywords = $v['keywords'];
						}
					}else{
						$keywords = "";
					}

					if(!empty($v['background'])){
						if(is_array($v['background'])){
							$tmpbackground="";
							foreach($v['background'] as $tmpk => $tmpv){
								$tmpbackground.=$tmpv;
							}
							$background = $tmpbackground;
						}else{
							$background = $v['background'];
						}
					}else{
						$background = "";
					}

					if(!empty($v['ideal'])){
						if(is_array($v['ideal'])){
							$tmpideal="";
							foreach($v['ideal'] as $tmpk => $tmpv){
								$tmpideal.=$tmpv;
							}
							$ideal = $tmpideal;
						}else{
							$ideal = $v['ideal'];
						}
					}else{
						$ideal = "";
					}

					if(!empty($v['summary'])){
						if(is_array($v['summary'])){
							$tmpsummary="";
							foreach($v['summary'] as $tmpk => $tmpv){
								$tmpsummary.=$tmpv;
							}
							$summary = $tmpsummary;
						}else{
							$summary = $v['summary'];
						}
					}else{
						$summary = "";
					}
	
					if(!empty($v['area_detail'])){
						if(is_array($v['area_detail'])){
						$tmparea_detail="";
						foreach($v['area_detail'] as $tmpk => $tmpv){
							$tmparea_detail.=$tmpv;
						}
						$area_detail = $tmparea_detail;
					}else{
						$area_detail = $v['area_detail'];
					}
					}else{
						$area_detail = "";
					}

					if(!empty($v['experience'])){
						if(is_array($v['experience'])){
							$tmpexperience="";
							foreach($v['experience'] as $tmpk => $tmpv){
								$tmpexperience.=$tmpv;
							}
							$experience = $tmpexperience;
						}else{
							$experience = $v['experience'];
						}
					}else{
						$experience = "";
					}

					if(!empty($v['qualification'])){
						if(is_array($v['qualification'])){
							$tmpqualification="";
							foreach($v['qualification'] as $tmpk => $tmpv){
								$tmpqualification.=$tmpv;
							}
							$qualification = $tmpqualification;
						}else{
							$qualification = $v['qualification'];
						}
					}else{
						$qualification = "";
					}

					if(!empty($v['worktime'])){
						if(is_array($v['worktime'])){
							$tmpworktime="";
							foreach($v['worktime'] as $tmpk => $tmpv){
								$tmpworktime.=$tmpv;
							}
							$worktime = $tmpworktime;
						}else{
							$worktime = $v['worktime'];
						}
					}else{
						$worktime = "";
					}

					if(!empty($v['holiday'])){
						if(is_array($v['holiday'])){
							$tmpholiday="";
							foreach($v['holiday'] as $tmpk => $tmpv){
								$tmpholiday.=$tmpv;
							}
							$holiday = $tmpholiday;
						}else{
							$holiday = $v['holiday'];
						}
					}else{
						$holiday = "";
					}
	
					if(!empty($v['benefits'])){
						if(is_array($v['benefits'])){
							$tmpbenefits="";
							foreach($v['benefits'] as $tmpk => $tmpv){
								$tmpbenefits.=$tmpv;
							}
							$benefits = $tmpbenefits;
						}else{
							$benefits = $v['benefits'];
						}
					}else{
						$benefits = "";
					}

					if(!empty($v['minsalary'])){
						if(is_array($v['minsalary'])){
							$tmpminsalary="";
							foreach($v['minsalary'] as $tmpk => $tmpv){
								$tmpminsalary.=$tmpv;
							}
							$minsalary = $tmpminsalary;
						}else{
							$minsalary = $v['minsalary'];
						}
					}else{
						$minsalary = 0;
					}

					if(!empty($v['maxsalary'])){
						if(is_array($v['maxsalary'])){
							$tmpmaxsalary="";
							foreach($v['maxsalary'] as $tmpk => $tmpv){
								$tmpmaxsalary.=$tmpv;
							}
							$maxsalary = $tmpmaxsalary;
						}else{
							$maxsalary = $v['maxsalary'];
						}
					}else{
						$maxsalary = 0;
					}

					if(!empty($v['salary'])){
						if(is_array($v['salary'])){
							$tmpsalary="";
							foreach($v['salary'] as $tmpk => $tmpv){
								$tmpsalary.=$tmpv;
							}
							$salary = $tmpsalary;
						}else{
							$salary = $v['salary'];
						}
					}else{
						$salary = 0;
					}

					if(!empty($v['prefecture_id'])){
						if(is_array($v['prefecture_id'])){
							$tmpprefecture_id="";
							foreach($v['prefecture_id'] as $tmpk => $tmpv){
								$tmpprefecture_id.=$tmpv;
							}
							$prefecture_id = $tmpprefecture_id;
						}else{
							$prefecture_id = $v['prefecture_id'];
						}
					}else{
						$prefecture_id = "";
					}

					if(!empty($v['prefecture_name'])){
						if(is_array($v['prefecture_name'])){
							$tmpprefecture_name="";
							foreach($v['prefecture_name'] as $tmpk => $tmpv){
								$tmpprefecture_name.=$tmpv;
							}
							$prefecture_name = $tmpprefecture_name;
						}else{
							$prefecture_name = $v['prefecture_name'];
						}
					}else{
						$prefecture_name = "";
					}

					if(!empty($v['jobcategory_id'])){
						if(is_array($v['jobcategory_id'])){
							$tmpjobcategory_id="";
							foreach($v['jobcategory_id'] as $tmpk => $tmpv){
								$tmpjobcategory_id.=$tmpv;
							}
							$jobcategory_id = $tmpjobcategory_id;
						}else{
							$jobcategory_id = $v['jobcategory_id'];
						}
					}else{
						$jobcategory_id = "";
					}

					if(!empty($v['jobcategory_name'])){
						if(is_array($v['jobcategory_name'])){
							$tmpjobcategory_name="";
							foreach($v['jobcategory_name'] as $tmpk => $tmpv){
								$tmpjobcategory_name.=$tmpv;
							}
							$jobcategory_name = $tmpjobcategory_name;
						}else{
							$jobcategory_name = $v['jobcategory_name'];
						}
					}else{
						$jobcategory_name = "";
					}

					if(!empty($v['employ_id'])){
						if(is_array($v['employ_id'])){
							$tmpemploy_id="";
							foreach($v['employ_id'] as $tmpk => $tmpv){
								$tmpemploy_id.=$tmpv;
							}
							$employ_id = $tmpemploy_id;
						}else{
							$employ_id = $v['employ_id'];
						}
					}else{
						$employ_id = "";
					}

					if(!empty($v['employ_name'])){
						if(is_array($v['employ_name'])){
							$tmpemploy_name="";
							foreach($v['employ_name'] as $tmpk => $tmpv){
								$tmpemploy_name.=$tmpv;
							}
							$employ_name = $tmpemploy_name;
						}else{
							$employ_name = $v['employ_name'];
						}
					}else{
						$employ_name = "";
					}

					if(!empty($v['tenkin_id'])){
						if(is_array($v['tenkin_id'])){
							$tmptenkin_id="";
							foreach($v['tenkin_id'] as $tmpk => $tmpv){
								$tmptenkin_id.=$tmpv;
							}
							$tenkin_id = $tmptenkin_id;
						}else{
							$tenkin_id = $v['tenkin_id'];
						}
					}else{
						$tenkin_id = "";
					}

					if(!empty($v['tenkin_name'])){
						if(is_array($v['tenkin_name'])){
							$tmptenkin_name="";
							foreach($v['tenkin_name'] as $tmpk => $tmpv){
								$tmptenkin_name.=$tmpv;
							}
							$tenkin_name = $tmptenkin_name;
						}else{
							$tenkin_name = $v['tenkin_name'];
						}
					}else{
						$tenkin_name = "";
					}

					if(!empty($v['active_flg'])){
						if(is_array($v['active_flg'])){
							$tmpactive_flg="";
							foreach($v['active_flg'] as $tmpk => $tmpv){
								$tmpactive_flg.=$tmpv;
							}
							$active_flg = $tmpactive_flg;
						}else{
							$active_flg = $v['active_flg'];
						}
					}else{
						$active_flg = 0;
					}

					if(!empty($v['client_id'])){
						if(is_array($v['client_id'])){
							$tmpclient_id="";
							foreach($v['client_id'] as $tmpk => $tmpv){
								$tmpclient_id.=$tmpv;
							}
							$client_id = $tmpclient_id;
						}else{
							$client_id = $v['client_id'];
						}
					}else{
						$client_id = 0;
					}

					if(!empty($v['recruiter_id'])){
						if(is_array($v['recruiter_id'])){
							$tmprecruiter_id="";
							foreach($v['recruiter_id'] as $tmpk => $tmpv){
								$tmprecruiter_id.=$tmpv;
							}
							$recruiter_id = $tmprecruiter_id;
						}else{
							$recruiter_id = $v['recruiter_id'];
						}
					}else{
						$recruiter_id = 0;
					}

					if(!empty($v['owner_id'])){
						if(is_array($v['owner_id'])){
							$tmpowner_id="";
							foreach($v['owner_id'] as $tmpk => $tmpv){
								$tmpowner_id.=$tmpv;
							}
							$owner_id = $tmpowner_id;
						}else{
							$owner_id = $v['owner_id'];
						}
					}else{
						$owner_id = 0;
					}

					$phaze = empty($v['phase']) ? 0 : $v['phase'];
					$publish = empty($v['publish']) ? 0 : $v['publish'];
				

					$r = $stmt->execute();
					if (!$r) {
						throw new Exception('insert '. $db .' 失敗');
					}
					//commit
					$pdo->commit();
				}
			} catch (PDOException $e) {
				//rollback
				$pdo->rollBack();
				print('Error:'.$db . " " .$e->getMessage() . "失敗");
				die();
			}

			//件数チェック
			$stmt = $pdo -> query("SELECT * FROM " . $db);
			$count = $stmt -> rowCount();
	
			if($total == $count){
				//古いDBを削除
				$sql = "DROP TABLE IF EXISTS " . $c_db . "_tmp";
				$pdo -> exec($sql);
	
				//既存のDBをtmpにリネーム
				$stmt = $pdo -> prepare("alter table " . $c_db . " rename to jobs_tmp");
				$stmt->execute();

				//今回作成したDBをリネーム
				$stmt = $pdo -> prepare("alter table " . $db . " rename to jobs");
				$res = $stmt->execute();
				if(!$res){
					$ret = "求人情報のデータ登録でエラーがありました。";
				}
			}else{
				$ret = "求人情報でエラーが発生しました。<br />" . "HRBC件数：" . $total . "<br />" . "insert件数：" . $count;
			}
	
			return $ret;
		}
	}

	//////////////////////////////////////////////////////////
	//Client
	//////////////////////////////////////////////////////////
	function client()
	{
		//同時実行防止
		$fpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/tmp/client.txt";
		if(!file_exists($fpass)){
			$fp = fopen($fpass, "w");
		}else{
			//ホームに戻る
			$msg="処理中です<br />暫くしてから実行して下さい。";
			$this->session->set_userdata(array("msg"=>$msg));
			redirect("hrbc/home");
		}

		$msg="";
		$page=1;
		$offset=0;
		$limit=$this->hrbc_limit;
		$clients=array();
		$industries=array();

		$counter1=0;
		$counter2=0;
  
		$ret = $this->getClientInfo($page, $offset, $limit, $clients, $industries, $counter1, $counter2, "", "");

		//企業と業種情報取得成功
		if(!$ret){
			$msg = "正常終了しました。";
		}else{
			$msg = $ret;
		}

		//同時実行防止ファイルの削除
		unlink($fpass);

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//Client 日付抽出
	//////////////////////////////////////////////////////////
	function clientlimit()
	{
		$posts = $this->input->post();

		if(empty($posts['start']) || empty($posts['end'])){
			//ホームに戻る
			$msg="抽出期間を設定してください。";
			$this->session->set_userdata(array("msg"=>$msg));
			redirect("hrbc/home");
		}else{
			$msg="";

			$startAry = explode("/", $posts['start']);
			if(is_array($startAry) && !empty($startAry[0]) && !empty($startAry[1]) && !empty($startAry[2])){
				if( !checkdate($startAry[1], $startAry[2], $startAry[0]) ){
					$msg.="抽出開始日の形式が違います<br>";
				}
			}else{
				$msg.="抽出開始日の形式が違います<br>";
			}

			$endAry = explode("/", $posts['end']);
			if(is_array($endAry) && !empty($endAry[0]) && !empty($endAry[1]) && !empty($endAry[2])){
				if( !checkdate($endAry[1], $endAry[2], $endAry[0]) ){
					$msg.="抽出終了日の形式が違います<br>";
				}
			}else{
				$msg.="抽出終了日の形式が違います<br>";
			}

			if($msg){
				$this->session->set_userdata(array("msg"=>$msg));
				redirect("hrbc/home");
			}

		}

		//同時実行防止
		$fpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/tmp/client.txt";
		if(!file_exists($fpass)){
			$fp = fopen($fpass, "w");
		}else{
			//ホームに戻る
			$msg="処理中です<br />暫くしてから実行して下さい。";
			$this->session->set_userdata(array("msg"=>$msg));
			redirect("hrbc/home");
		}

		$msg="";
		$page=1;
		$offset=0;
		$limit=$this->hrbc_limit;
		$clients=array();
		$industries=array();

		$counter1=0;
		$counter2=0;
  
		$ret = $this->getClientInfo($page, $offset, $limit, $clients, $industries, $counter1, $counter2, $posts['start'], $posts['end']);

		//企業と業種情報取得成功
		if(!$ret){
			$msg = "正常終了しました。";
		}else{
			$msg = $ret;
		}

		//同時実行防止ファイルの削除
		unlink($fpass);

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//ファイルクリア　※エラーでファイルが残った場合の対応
	//////////////////////////////////////////////////////////
	function clientclear()
	{				
		//同時実行防止
		$fpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/tmp/client.txt";

		//同時実行防止ファイルの削除
		if(file_exists($fpass)){
			unlink($fpass);
		}
		$msg="対応が完了しました。<br />「更新する」をクリックして、データを更新して下さい。";

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//企業情報登録
	//////////////////////////////////////////////////////////
	function getClientInfo($page, $offset, $limit, $clients, $industries, $counter1, $counter2 ,$start, $end)
	{

		$ret="";
		$sortflg=0;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

		$url = $this->hrbc_client_url; 

		//送信パラメータ
		//日付指定
		if($start && $end){
			$sortflg=1;
/*
			$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'start'=>$offset, 'condition'=>'Client.P_UpdateDate:ge=' . $start . ' 00:00:00,Client.P_UpdateDate:le=' . $end . ' 23:59:59' , 'field'=>'Client.P_Id,Client.P_Name,Client.U_73008663B01A568363BA767BEA7C3C,Client.U_00E035A31CE2CAC9B285D82E851161,Client.U_730F36F629417E6F7C2B9E089676CC,Client.P_UpdateDate,Client.P_Street,Client.U_FD0AD8514C0C84C08D7BFDC92AE81F,Client.U_B60F7A88A054DD8C52F0482F601C7F,Client.U_F492358E76837FD8ADEE2E132A16C0,Client.U_730F36F629417E6F7C2B9E089676CC,Client.U_47C67A9C1CAF4782E2FF8BD0AA4175'));
*/


            $url = $this->hrbc_client_url .  "?partition=". $partition . "&count=".$limit. "&start=".$offset."&condition=Client.P_UpdateDate:ge=" . $start . "%2000:00:00,Client.P_UpdateDate:le=" . $end ."%2023:59:59&field=Client.P_Id,Client.P_Name,Client.U_73008663B01A568363BA767BEA7C3C,Client.U_00E035A31CE2CAC9B285D82E851161,Client.U_730F36F629417E6F7C2B9E089676CC,Client.P_UpdateDate,Client.P_Street,Client.U_FD0AD8514C0C84C08D7BFDC92AE81F,Client.U_B60F7A88A054DD8C52F0482F601C7F,Client.U_F492358E76837FD8ADEE2E132A16C0,Client.U_730F36F629417E6F7C2B9E089676CC,Client.U_47C67A9C1CAF4782E2FF8BD0AA4175";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

		}else{
/*
			$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'start'=>$offset, 'field'=>'Client.P_Id,Client.P_Name,Client.U_73008663B01A568363BA767BEA7C3C,Client.U_00E035A31CE2CAC9B285D82E851161,Client.U_730F36F629417E6F7C2B9E089676CC,Client.P_UpdateDate,Client.P_Street,Client.U_FD0AD8514C0C84C08D7BFDC92AE81F,Client.U_B60F7A88A054DD8C52F0482F601C7F,Client.U_F492358E76837FD8ADEE2E132A16C0,Client.U_730F36F629417E6F7C2B9E089676CC,Client.U_47C67A9C1CAF4782E2FF8BD0AA4175'));
*/
            $url = $this->hrbc_client_url .  "?partition=". $partition . "&count=".$limit. "&start=".$offset."&field=Client.P_Id,Client.P_Name,Client.U_73008663B01A568363BA767BEA7C3C,Client.U_00E035A31CE2CAC9B285D82E851161,Client.U_730F36F629417E6F7C2B9E089676CC,Client.P_UpdateDate,Client.P_Street,Client.U_FD0AD8514C0C84C08D7BFDC92AE81F,Client.U_B60F7A88A054DD8C52F0482F601C7F,Client.U_F492358E76837FD8ADEE2E132A16C0,Client.U_730F36F629417E6F7C2B9E089676CC,Client.U_47C67A9C1CAF4782E2FF8BD0AA4175";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);


		}

/*
		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/

		//HRBCクライアントカウント
		$this->HrbcCountCheck("client");

//		$xml = file_get_contents($unit_url, false, $context);
//		$xml = simplexml_load_string($xml);
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//HRBCから帰ってきたデータ件数　1件の対応用に取得
		$cnt = $xml->attributes()->Count;

		//データ設定
		$json = json_encode($xml);
		$arr = json_decode($json,true);

		if($cnt==0){
		}else if($cnt!=1){
			foreach($arr['Item'] as $k => $v){
				$clients[$counter1]['c_id'] = $v['Client.P_Id'];
				$clients[$counter1]['client_title'] = $v['Client.P_Name'];
				$clients[$counter1]['naiyo'] = $v['Client.U_00E035A31CE2CAC9B285D82E851161'];
				$clients[$counter1]['employment'] = $v['Client.U_730F36F629417E6F7C2B9E089676CC'];
				$clients[$counter1]['street'] = $v['Client.P_Street'];
				$clients[$counter1]['jigyo'] = $v['Client.U_FD0AD8514C0C84C08D7BFDC92AE81F'];
				$clients[$counter1]['capital'] = $v['Client.U_B60F7A88A054DD8C52F0482F601C7F'];
				$clients[$counter1]['establish'] = $v['Client.U_F492358E76837FD8ADEE2E132A16C0'];
				$clients[$counter1]['url'] = $v['Client.U_47C67A9C1CAF4782E2FF8BD0AA4175'];
				$clients[$counter1]['c_updated']  = date('Y-m-d H:i:s', strtotime($v['Client.P_UpdateDate'])+9*60*60);

				//職種
				$iName="";
				$iId="";
				foreach($v['Client.U_73008663B01A568363BA767BEA7C3C'] as $k2=>$v2){
					$industries[$counter2]['client_id'] = $v['Client.P_Id'];
					$industries[$counter2]['alias'] = $k2;
					foreach($v2 as $k3 => $v3){
						if($k3=="Option.P_Name") $iName .= $v3.",";
						if($k3=="Option.P_Id") $iId .= $v3.",";
					}
					$counter2++;
				}
				$clients[$counter1]['industry_name']= rtrim($iName, ",");
				$clients[$counter1]['industry_id']= rtrim($iId, ",");

				$counter1++;
			}

		//1件のデータは返却の形式が異なるので個別対応
		}else{
			$clients[$counter1]['c_id'] = $arr['Item']['Client.P_Id'];
			$clients[$counter1]['client_title'] = $arr['Item']['Client.P_Name'];
			$clients[$counter1]['naiyo'] = $arr['Item']['Client.U_00E035A31CE2CAC9B285D82E851161'];
			$clients[$counter1]['employment'] = $arr['Item']['Client.U_730F36F629417E6F7C2B9E089676CC'];
			$clients[$counter1]['street'] = $arr['Item']['Client.P_Street'];
			$clients[$counter1]['jigyo'] = $arr['Item']['Client.U_FD0AD8514C0C84C08D7BFDC92AE81F'];
			$clients[$counter1]['capital'] = $arr['Item']['Client.U_B60F7A88A054DD8C52F0482F601C7F'];
			$clients[$counter1]['establish'] = $arr['Item']['Client.U_F492358E76837FD8ADEE2E132A16C0'];
			$clients[$counter1]['url'] = $arr['Item']['Client.U_47C67A9C1CAF4782E2FF8BD0AA4175'];

			$clients[$counter1]['c_updated']  = date('Y-m-d H:i:s', strtotime($arr['Item']['Client.P_UpdateDate'])+9*60*60);

			//職種
			$iName="";
			$iId="";
			foreach($arr['Item']['Client.U_73008663B01A568363BA767BEA7C3C'] as $k2=>$v2){
				$industries[$counter2]['client_id'] = $arr['Item']['Client.P_Id'];
				$industries[$counter2]['alias'] = $k2;
				foreach($v2 as $k3 => $v3){
					if($k3=="Option.P_Name") $iName = $v3.",";
					if($k3=="Option.P_Id") $iId = $v3.",";
				}
				$counter2++;
			}
			$clients[$counter1]['industry_name']= rtrim($iName, ",");
			$clients[$counter1]['industry_id']= rtrim($iId, ",");

			$counter1++;
		}
		
		//データ件数
		$total = $xml->attributes()->Total;
		if($total==0){
			$flg=1;
		}

		//ページ数
		$max = ceil($total / $limit);
		//総ページが１なら終了
		if($max==1){
			$flg=1;
		}

		//ページャーカウントアップ
		$page = $page + 1;
		//次に取得する件数
		$move_page = ($page) * $limit;


		//総ページ数を超えたら終了
		if($page > $max){
			$flg=1;
		//保険で100回超えたら終了
		}else if($page > 100){
			$flg=1;
		}

		//offset取得
		$offset = $page * $limit - $limit;
	
		if($flg!=1){
			$this->getClientInfo($page, $offset, $limit, $clients, $industries, $counter1, $counter2, $start, $end);
		}else{
			if($total==0){
				$ret = "データは0件でした";
			}else{
				//DB格納	
				$ret = $this->setClientsInfo($clients, $industries, $total, $sortflg);
			}
		}
		return $ret;
	}

	///////////////////////////////////////////////////////////////////////////////
	// DB格納処理
	///////////////////////////////////////////////////////////////////////////////
	function setClientsInfo($clients, $industries, $total, $sortflg)
	{
		$ret="";

		//DB格納
		$dns = 'mysql:dbname=' . dbDatabase . ';host=' . dbHostname . ';port=' . dbPort;
		$user = dbUsername;
		$password = dbPassword;
		
		try{
			$pdo = new PDO($dns, $user, $password,
				array(
				     	PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET `utf8`"
				)
			);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		}catch (PDOException $e){
			print('Error:'.$e->getMessage());
			die();
		}

		if($sortflg==1){
			//////////////////////////////////////////////////////
			// client table挿入
			/////////////////////////////////////////////////////
			try {
				foreach($clients as $k=>$v){
					$pdo->beginTransaction();
					$stmt = $pdo -> prepare("INSERT INTO clients (c_id, c_title, c_naiyo, c_employment, c_industry_id, c_industry_name, c_street, c_jigyo, c_capital, c_establish, c_url, c_updated) VALUES (:c_id, :c_title, :c_naiyo, :c_employment, :c_industry_id, :c_industry_name, :c_street, :c_jigyo, :c_capital, :c_establish, :c_url, :c_updated) ON DUPLICATE KEY UPDATE c_title = :c_title, c_naiyo = :c_naiyo, c_employment = :c_employment, c_industry_id = :c_industry_id, c_industry_name = :c_industry_name, c_street = :c_street, c_jigyo = :c_jigyo, c_capital = :c_capital, c_establish = :c_establish, c_url = :c_url, c_updated = :c_updated");
					$stmt->bindParam(':c_id', $c_id, PDO::PARAM_INT);
					$stmt->bindParam(':c_title', $client_title, PDO::PARAM_STR);
					$stmt->bindParam(':c_naiyo', $naiyo, PDO::PARAM_STR);
					$stmt->bindParam(':c_industry_id', $industry_id, PDO::PARAM_STR);
					$stmt->bindParam(':c_industry_name', $industry_name, PDO::PARAM_STR);
					$stmt->bindParam(':c_street', $street, PDO::PARAM_STR);
					$stmt->bindParam(':c_jigyo', $jigyo, PDO::PARAM_STR);
					$stmt->bindParam(':c_capital', $capital, PDO::PARAM_STR);
					$stmt->bindParam(':c_establish', $establish, PDO::PARAM_STR);
					$stmt->bindParam(':c_url', $url, PDO::PARAM_STR);
					$stmt->bindParam(':c_employment', $employment, PDO::PARAM_INT);
					$stmt->bindParam(':c_updated', $v['c_updated'], PDO::PARAM_INT);

					$c_id = empty($v['c_id']) ? "" : $v['c_id'];
					$client_title = empty($v['client_title']) ? "" : $v['client_title'];
	
					if(!empty($v['naiyo'])){
						if(is_array($v['naiyo'])){
							$tmpnaiyo="";
							foreach($v['naiyo'] as $tmpk => $tmpv){
								$tmpnaiyo.=$tmpv;
							}
							$naiyo = $tmpnaiyo;
						}else{
							$naiyo = $v['naiyo'];
						}
					}else{
						$naiyo = "";
					}
	
					if(!empty($v['employment'])){
						if(is_array($v['employment'])){
							$tmpemployment="";
							foreach($v['employment'] as $tmpk => $tmpv){
								if(!empty($tmpv)){
									$tmpemployment=$tmpv;
								}
							}
							$employment = $tmpemployment;
						}else{
							$employment = $v['employment'];
						}
					}else{
						$employment = "";
					}
	
					if(!empty($v['industry_id'])){
						if(is_array($v['industry_id'])){
							$tmpindustry_id="";
							foreach($v['industry_id'] as $tmpk => $tmpv){
								$tmpindustry_id.=$tmpv;
							}
							$industry_id = $tmpindustry_id;
						}else{
							$industry_id = $v['industry_id'];
						}
					}else{
						$industry_id = "";
					}
	
					if(!empty($v['industry_name'])){
						if(is_array($v['industry_name'])){
							$tmpindustry_name="";
							foreach($v['industry_name'] as $tmpk => $tmpv){
								$tmpindustry_name.=$tmpv;
							}
							$industry_name = $tmpindustry_name;
						}else{
							$industry_name = $v['industry_name'];
						}
					}else{
						$industry_name = "";
					}

					if(!empty($v['street'])){
						if(is_array($v['street'])){
							$tmpstreet="";
							foreach($v['street'] as $tmpk => $tmpv){
								$tmpstreet.=$tmpv;
							}
							$street = $tmpstreet;
						}else{
							$street = $v['street'];
						}
					}else{
						$street = "";
					}

					if(!empty($v['jigyo'])){
						if(is_array($v['jigyo'])){
							$tmpjigyo="";
							foreach($v['jigyo'] as $tmpk => $tmpv){
								$tmpjigyo.=$tmpv;
							}
							$jigyo = $tmpjigyo;
						}else{
							$jigyo = $v['jigyo'];
						}
					}else{
						$jigyo = "";
					}

					if(!empty($v['capital'])){
						if(is_array($v['capital'])){
							$tmpcapital="";
							foreach($v['capital'] as $tmpk => $tmpv){
								$tmpcapital.=$tmpv;
							}
							$capital = $tmpcapital;
						}else{
							$capital = $v['capital'];
						}
					}else{
						$capital = "";
					}

					if(!empty($v['establish'])){
						if(is_array($v['establish'])){
							$tmpestablish="";
							foreach($v['establish'] as $tmpk => $tmpv){
								$tmpestablish.=$tmpv;
							}
							$establish = $tmpestablish;
						}else{
							$establish = $v['establish'];
						}
					}else{
						$establish = "";
					}

					if(!empty($v['url'])){
						if(is_array($v['url'])){
							$tmpurl="";
							foreach($v['url'] as $tmpk => $tmpv){
								$tmpurl.=$tmpv;
							}
							$url = $tmpurl;
						}else{
							$url = $v['url'];
						}
					}else{
						$url = "";
					}

					$r = $stmt->execute();
					if (!$r) {
						throw new Exception('insert clients 失敗');
					}
					//commit
					$pdo->commit();
				}
			} catch (PDOException $e) {
				//rollback
				$pdo->rollBack();
				print('Error: clients' . " " .$e->getMessage());
				die();
			}
		}else{
			$d = Date('Ymdhis');

			/////////////////////////////////////////////////////
			// 企業情報の設定
			/////////////////////////////////////////////////////
			$c_db = "clients";
			$db = $c_db . "_" . $d;

			//////////////////////////////////////////////////////
			// client table作成
			/////////////////////////////////////////////////////
			$sql = "CREATE TABLE IF NOT EXISTS `" . $db . "` (
				`c_id` int(11) NOT NULL,
				`c_title` varchar(255) NOT NULL,
				`c_naiyo` longtext NOT NULL,
				`c_employment` varchar(255) NOT NULL,
				`c_industry_id` text NOT NULL,
				`c_industry_name` text NOT NULL,
				`c_street` text NOT NULL,
				`c_jigyo` text NOT NULL,
				`c_capital` text NOT NULL,
				`c_establish` text NOT NULL,
				`c_url` text NOT NULL,
				`c_updated` datetime NOT NULL,
				PRIMARY KEY  (`c_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
			$stmt = $pdo -> prepare($sql);
			$r = $stmt -> execute();
			if (!$r) {
				throw new Exception('create 失敗');
			}

			//////////////////////////////////////////////////////
			// client table挿入
			/////////////////////////////////////////////////////
			try {
				foreach($clients as $k=>$v){
					$pdo->beginTransaction();
					$stmt = $pdo -> prepare("INSERT INTO "  . $db . " (c_id, c_title, c_naiyo, c_employment, c_industry_id, c_industry_name, c_street, c_jigyo, c_capital, c_establish, c_url, c_updated) VALUES (:c_id, :c_title, :c_naiyo, :c_employment, :c_industry_id, :c_industry_name, :c_street, :c_jigyo, :c_capital, :c_establish, :c_url, :c_updated)");
					$stmt->bindParam(':c_id', $c_id, PDO::PARAM_INT);
					$stmt->bindParam(':c_title', $client_title, PDO::PARAM_STR);
					$stmt->bindParam(':c_naiyo', $naiyo, PDO::PARAM_STR);
					$stmt->bindParam(':c_industry_id', $industry_id, PDO::PARAM_STR);
					$stmt->bindParam(':c_industry_name', $industry_name, PDO::PARAM_STR);
					$stmt->bindParam(':c_street', $street, PDO::PARAM_STR);
					$stmt->bindParam(':c_jigyo', $jigyo, PDO::PARAM_STR);
					$stmt->bindParam(':c_capital', $capital, PDO::PARAM_STR);
					$stmt->bindParam(':c_establish', $establish, PDO::PARAM_STR);
					$stmt->bindParam(':c_url', $url, PDO::PARAM_STR);
					$stmt->bindParam(':c_employment', $employment, PDO::PARAM_STR);
					$stmt->bindParam(':c_updated', $v['c_updated'], PDO::PARAM_INT);

					$c_id = empty($v['c_id']) ? "" : $v['c_id'];
					$client_title = empty($v['client_title']) ? "" : $v['client_title'];
	
					if(!empty($v['naiyo'])){
						if(is_array($v['naiyo'])){
							$tmpnaiyo="";
							foreach($v['naiyo'] as $tmpk => $tmpv){
								$tmpnaiyo.=$tmpv;
							}
							$naiyo = $tmpnaiyo;
						}else{
							$naiyo = $v['naiyo'];
						}
					}else{
						$naiyo = "";
					}
	
					if(!empty($v['employment'])){
						if(is_array($v['employment'])){
							$tmpemployment="";
							foreach($v['employment'] as $tmpk => $tmpv){
								if(!empty($tmpv)){
									$tmpemployment=$tmpv;
								}
							}
							$employment = $tmpemployment;
						}else{
							$employment = $v['employment'];
						}
					}else{
						$employment = "";
					}
	
					if(!empty($v['industry_id'])){
						if(is_array($v['industry_id'])){
							$tmpindustry_id="";
							foreach($v['industry_id'] as $tmpk => $tmpv){
								$tmpindustry_id.=$tmpv;
							}
							$industry_id = $tmpindustry_id;
						}else{
							$industry_id = $v['industry_id'];
						}
					}else{
						$industry_id = "";
					}
	
					if(!empty($v['industry_name'])){
						if(is_array($v['industry_name'])){
							$tmpindustry_name="";
							foreach($v['industry_name'] as $tmpk => $tmpv){
								$tmpindustry_name.=$tmpv;
							}
							$industry_name = $tmpindustry_name;
						}else{
							$industry_name = $v['industry_name'];
						}
					}else{
						$industry_name = "";
					}

					if(!empty($v['street'])){
						if(is_array($v['street'])){
							$tmpstreet="";
							foreach($v['street'] as $tmpk => $tmpv){
								$tmpstreet.=$tmpv;
							}
							$street = $tmpstreet;
						}else{
							$street = $v['street'];
						}
					}else{
						$street = "";
					}

					if(!empty($v['jigyo'])){
						if(is_array($v['jigyo'])){
							$tmpjigyo="";
							foreach($v['jigyo'] as $tmpk => $tmpv){
								$tmpjigyo.=$tmpv;
							}
							$jigyo = $tmpjigyo;
						}else{
							$jigyo = $v['jigyo'];
						}
					}else{
						$jigyo = "";
					}

					if(!empty($v['capital'])){
						if(is_array($v['capital'])){
							$tmpcapital="";
							foreach($v['capital'] as $tmpk => $tmpv){
								$tmpcapital.=$tmpv;
							}
							$capital = $tmpcapital;
						}else{
							$capital = $v['capital'];
						}
					}else{
						$capital = "";
					}

					if(!empty($v['establish'])){
						if(is_array($v['establish'])){
							$tmpestablish="";
							foreach($v['establish'] as $tmpk => $tmpv){
								$tmpestablish.=$tmpv;
							}
							$establish = $tmpestablish;
						}else{
							$establish = $v['establish'];
						}
					}else{
						$establish = "";
					}

					if(!empty($v['url'])){
						if(is_array($v['url'])){
							$tmpurl="";
							foreach($v['url'] as $tmpk => $tmpv){
								$tmpurl.=$tmpv;
							}
							$url = $tmpurl;
						}else{
							$url = $v['url'];
						}
					}else{
						$url = "";
					}

					$r = $stmt->execute();
					if (!$r) {
						throw new Exception('insert '. $db .' 失敗');
					}
					//commit
					$pdo->commit();
				}
			} catch (PDOException $e) {
				//rollback
				$pdo->rollBack();
				print('Error:'.$db . " " .$e->getMessage());
				die();
			}

			//件数チェック
			$stmt = $pdo -> query("SELECT * FROM " . $db);
			$count = $stmt -> rowCount();
	
			if($total == $count){
				//古いDBを削除
				$sql = "DROP TABLE IF EXISTS " . $c_db . "_tmp";
				$pdo -> exec($sql);

				//既存のDBをtmpにリネーム
				$stmt = $pdo -> prepare("alter table " . $c_db . " rename to clients_tmp");
				$stmt->execute();

				//今回作成したDBをリネーム
				$stmt = $pdo -> prepare("alter table " . $db . " rename to clients");
				$res = $stmt->execute();
				if(!$res){
					$ret = "企業情報のデータ登録でエラーがありました。";
				}
			}else{
				$ret = "企業情報でエラーが発生しました。<br />" . "HRBC件数：" . $total . "<br />" . "insert件数：" . $count;
			}
			return $ret;
		}
	}

/////////////////////////////////////////////////////////////////////////
// 2017.03.10 リクルーター追加
/////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////
	//Rcruiter
	//////////////////////////////////////////////////////////
	function recruiter()
	{
		//同時実行防止
		$fpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/tmp/recruiter.txt";
		if(!file_exists($fpass)){
			$fp = fopen($fpass, "w");
		}else{
			//ホームに戻る
			$msg="処理中です<br />暫くしてから実行して下さい。";
			$this->session->set_userdata(array("msg"=>$msg));
			redirect("hrbc/home");
		}

		$msg="";
		$page=1;
		$offset=0;
		$limit=$this->hrbc_limit;
		$recruiters=array();
		$industries=array();

		$counter1=0;
		$counter2=0;
  
		$ret = $this->getRecruiterInfo($page, $offset, $limit, $recruiters, $industries, $counter1, $counter2, "", "");

		//企業と業種情報取得成功
		if(!$ret){
			$msg = "正常終了しました。";
		}else{
			$msg = $ret;
		}

		//同時実行防止ファイルの削除
		unlink($fpass);

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//Recruiter 日付抽出
	//////////////////////////////////////////////////////////
	function recruiterlimit()
	{
		$posts = $this->input->post();

		if(empty($posts['start']) || empty($posts['end'])){
			//ホームに戻る
			$msg="抽出期間を設定してください。";
			$this->session->set_userdata(array("msg"=>$msg));
			redirect("hrbc/home");
		}else{
			$msg="";

			$startAry = explode("/", $posts['start']);
			if(is_array($startAry) && !empty($startAry[0]) && !empty($startAry[1]) && !empty($startAry[2])){
				if( !checkdate($startAry[1], $startAry[2], $startAry[0]) ){
					$msg.="抽出開始日の形式が違います<br>";
				}
			}else{
				$msg.="抽出開始日の形式が違います<br>";
			}

			$endAry = explode("/", $posts['end']);
			if(is_array($endAry) && !empty($endAry[0]) && !empty($endAry[1]) && !empty($endAry[2])){
				if( !checkdate($endAry[1], $endAry[2], $endAry[0]) ){
					$msg.="抽出終了日の形式が違います<br>";
				}
			}else{
				$msg.="抽出終了日の形式が違います<br>";
			}

			if($msg){
				$this->session->set_userdata(array("msg"=>$msg));
				redirect("hrbc/home");
			}

		}

		//同時実行防止
		$fpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/tmp/recruiter.txt";
		if(!file_exists($fpass)){
			$fp = fopen($fpass, "w");
		}else{
			//ホームに戻る
			$msg="処理中です<br />暫くしてから実行して下さい。";
			$this->session->set_userdata(array("msg"=>$msg));
			redirect("hrbc/home");
		}

		$msg="";
		$page=1;
		$offset=0;
		$limit=$this->hrbc_limit;
		$recruiters=array();
		$industries=array();

		$counter1=0;
		$counter2=0;
  
		$ret = $this->getRecruiterInfo($page, $offset, $limit, $recruiters, $industries, $counter1, $counter2, $posts['start'], $posts['end']);

		//企業と業種情報取得成功
		if(!$ret){
			$msg = "正常終了しました。";
		}else{
			$msg = $ret;
		}

		//同時実行防止ファイルの削除
		unlink($fpass);

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//ファイルクリア　※エラーでファイルが残った場合の対応
	//////////////////////////////////////////////////////////
	function recruiterclear()
	{				
		//同時実行防止
		$fpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/tmp/recruiter.txt";

		//同時実行防止ファイルの削除
		if(file_exists($fpass)){
			unlink($fpass);
		}
		$msg="対応が完了しました。<br />「更新する」をクリックして、データを更新して下さい。";

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//企業情報登録
	//////////////////////////////////////////////////////////
	function getRecruiterInfo($page, $offset, $limit, $recruiters, $industries, $counter1, $counter2 ,$start, $end)
	{

		$ret="";
		$sortflg=0;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

		$url = $this->hrbc_recruiter_url; 

		//送信パラメータ
		//日付指定
		if($start && $end){
			$sortflg=1;
/*
			$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'start'=>$offset, 'condition'=>'Recruiter.P_UpdateDate:ge=' . $start . ' 00:00:00,Recruiter.P_UpdateDate:le=' . $end . ' 23:59:59' , 'field'=>'Recruiter.P_Id,Recruiter.P_Name,Recruiter.P_Reading,Recruiter.P_Division,Recruiter.P_Title,Recruiter.P_Client,Recruiter.P_Owner,Recruiter.P_Memo,Recruiter.P_Telephone,Recruiter.P_Street,Recruiter.P_Mail,Recruiter.U_55D3C65CE5558E4C67A81420C134C9,Recruiter.P_UpdateDate'));
*/

            $url = $this->hrbc_recruiter_url .  "?partition=". $partition . "&count=".$limit. "&start=".$offset."&condition=Recruiter.P_UpdateDate:ge=" . $start . "%2000:00:00,Recruiter.P_UpdateDate:le=" . $end ."%2023:59:59&field=Recruiter.P_Id,Recruiter.P_Name,Recruiter.P_Reading,Recruiter.P_Division,Recruiter.P_Title,Recruiter.P_Client,Recruiter.P_Owner,Recruiter.P_Memo,Recruiter.P_Telephone,Recruiter.P_Street,Recruiter.P_Mail,Recruiter.U_55D3C65CE5558E4C67A81420C134C9,Recruiter.P_UpdateDate";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);
		}else{
/*
			$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'start'=>$offset, 'field'=>'Recruiter.P_Id,Recruiter.P_Name,Recruiter.P_Reading,Recruiter.P_Division,Recruiter.P_Title,Recruiter.P_Client,Recruiter.P_Owner,Recruiter.P_Memo,Recruiter.P_Telephone,Recruiter.P_Street,Recruiter.P_Mail,Recruiter.U_55D3C65CE5558E4C67A81420C134C9,Recruiter.P_UpdateDate'));
*/

            $url = $this->hrbc_recruiter_url .  "?partition=". $partition . "&count=".$limit. "&start=".$offset . "&field=Recruiter.P_Id,Recruiter.P_Name,Recruiter.P_Reading,Recruiter.P_Division,Recruiter.P_Title,Recruiter.P_Client,Recruiter.P_Owner,Recruiter.P_Memo,Recruiter.P_Telephone,Recruiter.P_Street,Recruiter.P_Mail,Recruiter.U_55D3C65CE5558E4C67A81420C134C9,Recruiter.P_UpdateDate";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);


		}

/*
		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/
		//HRBCリクルーターカウント
		$this->HrbcCountCheck("recruiter");
//
//		$xml = file_get_contents($unit_url, false, $context);
//		$xml = simplexml_load_string($xml);
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//HRBCから帰ってきたデータ件数　1件の対応用に取得
		$cnt = $xml->attributes()->Count;

		//データ設定
		$json = json_encode($xml);
		$arr = json_decode($json,true);

		if($cnt==0){
		}else if($cnt!=1){
			foreach($arr['Item'] as $k => $v){
				$recruiters[$counter1]['r_id'] = $v['Recruiter.P_Id'];
				$recruiters[$counter1]['recruiters_client'] = $v['Recruiter.P_Client'];
				$recruiters[$counter1]['recruiters_title'] = $v['Recruiter.P_Name'];
				$recruiters[$counter1]['recruiters_kana'] = $v['Recruiter.P_Reading'];
				$recruiters[$counter1]['recruiters_division'] = $v['Recruiter.P_Division'];
				$recruiters[$counter1]['recruiters_owner'] = $v['Recruiter.P_Owner'];
				$recruiters[$counter1]['recruiters_memo'] = $v['Recruiter.P_Memo'];
				$recruiters[$counter1]['recruiters_tel'] = $v['Recruiter.P_Telephone'];
				$recruiters[$counter1]['recruiters_street'] = $v['Recruiter.P_Street'];
				$recruiters[$counter1]['recruiters_mail'] = $v['Recruiter.P_Mail'];
				$recruiters[$counter1]['r_updated']  = date('Y-m-d H:i:s', strtotime($v['Recruiter.P_UpdateDate'])+9*60*60);

				$counter1++;
			}

		//1件のデータは返却の形式が異なるので個別対応
		}else{
			$recruiters[$counter1]['r_id'] = $arr['Item']['Recruiter.P_Id'];
			$recruiters[$counter1]['recruiters_client'] = $arr['Item']['Recruiter.P_Client'];
			$recruiters[$counter1]['recruiters_title'] = $arr['Item']['Recruiter.P_Name'];
			$recruiters[$counter1]['recruiters_kana'] = $arr['Item']['Recruiter.P_Reading'];
			$recruiters[$counter1]['recruiters_division'] = $arr['Item']['Recruiter.P_Division'];
			$recruiters[$counter1]['recruiters_owner'] = $arr['Item']['Recruiter.P_Owner'];
			$recruiters[$counter1]['recruiters_memo'] = $arr['Item']['Recruiter.P_Memo'];
			$recruiters[$counter1]['recruiters_tel'] = $arr['Item']['Recruiter.P_Telephone'];
			$recruiters[$counter1]['recruiters_street'] = $arr['Item']['Recruiter.P_Street'];
			$recruiters[$counter1]['recruiters_mail'] = $arr['Item']['Recruiter.P_Mail'];
			$recruiters[$counter1]['r_updated']  = date('Y-m-d H:i:s', strtotime($arr['Item']['Recruiter.P_UpdateDate'])+9*60*60);

			$counter1++;
		}

		
		//データ件数
		$total = $xml->attributes()->Total;
		if($total==0){
			$flg=1;
		}

		//ページ数
		$max = ceil($total / $limit);
		//総ページが１なら終了
		if($max==1){
			$flg=1;
		}

		//ページャーカウントアップ
		$page = $page + 1;
		//次に取得する件数
		$move_page = ($page) * $limit;


		//総ページ数を超えたら終了
		if($page > $max){
			$flg=1;
		//保険で100回超えたら終了
		}else if($page > 100){
			$flg=1;
		}

		//offset取得
		$offset = $page * $limit - $limit;
	
		if($flg!=1){
			$this->getRecruiterInfo($page, $offset, $limit, $recruiters, $industries, $counter1, $counter2, $start, $end);
		}else{
			if($total==0){
				$ret = "データは0件でした";
			}else{
				//DB格納	
				$ret = $this->setRecruitersInfo($recruiters, $industries, $total, $sortflg);
			}
		}
		return $ret;
	}

	///////////////////////////////////////////////////////////////////////////////
	// DB格納処理
	///////////////////////////////////////////////////////////////////////////////
	function setRecruitersInfo($recruiters, $industries, $total, $sortflg)
	{
		$ret="";

		//DB格納
		$dns = 'mysql:dbname=' . dbDatabase . ';host=' . dbHostname . ';port=' . dbPort;
		$user = dbUsername;
		$password = dbPassword;
		
		try{
			$pdo = new PDO($dns, $user, $password,
				array(
				     	PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET `utf8`"
				)
			);
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		}catch (PDOException $e){
			print('Error:'.$e->getMessage());
			die();
		}

		if($sortflg==1){
			//////////////////////////////////////////////////////
			// recruiters table挿入
			/////////////////////////////////////////////////////
			try {
				foreach($recruiters as $k=>$v){
					$pdo->beginTransaction();
					$stmt = $pdo -> prepare("INSERT INTO recruiters (r_id, r_client, r_title, r_kana, r_division, r_owner, r_memo, r_tel, r_street, r_mail, r_updated) VALUES (:r_id, :r_client, :r_title, :r_kana, :r_division, :r_owner, :r_memo, :r_tel, :r_street, :r_mail, :r_updated) ON DUPLICATE KEY UPDATE r_title = :r_title, r_kana = :r_kana, r_division = :r_division, r_owner = :r_owner, r_memo = :r_memo, r_tel = :r_tel, r_street = :r_street, r_mail = :r_mail , r_updated = :r_updated");
					$stmt->bindParam(':r_id', $r_id, PDO::PARAM_INT);
					$stmt->bindParam(':r_client', $client, PDO::PARAM_STR);
					$stmt->bindParam(':r_title', $recruiter_title, PDO::PARAM_STR);
					$stmt->bindParam(':r_kana', $kana, PDO::PARAM_STR);
					$stmt->bindParam(':r_division', $division, PDO::PARAM_STR);
					$stmt->bindParam(':r_owner', $owner, PDO::PARAM_STR);
					$stmt->bindParam(':r_memo', $memo, PDO::PARAM_STR);
					$stmt->bindParam(':r_tel', $tel, PDO::PARAM_STR);
					$stmt->bindParam(':r_street', $street, PDO::PARAM_STR);
					$stmt->bindParam(':r_mail', $mail, PDO::PARAM_STR);
					$stmt->bindParam(':r_updated', $v['r_updated'], PDO::PARAM_INT);

					$r_id = empty($v['r_id']) ? "" : $v['r_id'];
					$recruiter_title = empty($v['recruiters_title']) ? "" : $v['recruiters_title'];

					//client
					if(!empty($v['recruiters_client']['Client']['Client.P_Id'])){
						$client = $v['recruiters_client']['Client']['Client.P_Id'];
					}else{
						$client = "";
					}

					//kana
					if(!empty($v['recruiters_kana'])){
						if(is_array($v['recruiters_kana'])){
							$tmpkana="";
							foreach($v['recruiters_kana'] as $tmpk => $tmpv){
								$tmpkana.=$tmpv;
							}
							$kana = $tmpkana;
						}else{
							$kana = $v['recruiters_kana'];
						}
					}else{
						$kana = "";
					}	

					//division
					if(!empty($v['recruiters_division'])){
						if(is_array($v['recruiters_division'])){
							$tmpdiv="";
							foreach($v['recruiters_division'] as $tmpk => $tmpv){
								$tmpdiv.=$tmpv;
							}
							$division = $tmpdiv;
						}else{
							$division = $v['recruiters_division'];
						}
					}else{
						$division = "";
					}	

					//owner
					if(!empty($v['recruiters_owner']['User']['User.P_Id'])){
						$owner = $v['recruiters_owner']['User']['User.P_Id'];
					}else{
						$owner = "";
					}

					//memo
					if(!empty($v['recruiters_memo'])){
						if(is_array($v['recruiters_memo'])){
							$tmpmemo="";
							foreach($v['recruiters_memo'] as $tmpk => $tmpv){
								$tmpmemo.=$tmpv;
							}
							$memo = $tmpmemo;
						}else{
							$memo = $v['recruiters_memo'];
						}
					}else{
						$memo = "";
					}

					//tel
					if(!empty($v['recruiters_tel'])){
						if(is_array($v['recruiters_tel'])){
							$tmptel="";
							foreach($v['recruiters_tel'] as $tmpk => $tmpv){
								$tmptel.=$tmpv;
							}
							$tel = $tmptel;
						}else{
							$tel = $v['recruiters_tel'];
						}
					}else{
						$tel = "";
					}

					//mail
					if(!empty($v['recruiters_mail'])){
						if(is_array($v['recruiters_mail'])){
							$tmpmail="";
							foreach($v['recruiters_mail'] as $tmpk => $tmpv){
								$tmpmail.=$tmpv;
							}
							$mail = $tmpmail;
						}else{
							$mail = $v['recruiters_mail'];
						}
					}else{
						$mail = "";
					}

					//street
					if(!empty($v['recruiters_street'])){
						if(is_array($v['recruiters_street'])){
							$tmpstreet="";
							foreach($v['recruiters_street'] as $tmpk => $tmpv){
								$tmpstreet.=$tmpv;
							}
							$street = $tmpstreet;
						}else{
							$street = $v['recruiters_street'];
						}
					}else{
						$street = "";
					}

					$r = $stmt->execute();
					if (!$r) {
						throw new Exception('insert recruiters 失敗');
					}
					//commit
					$pdo->commit();
				}
			} catch (PDOException $e) {
				//rollback
				$pdo->rollBack();
				print('Error: recruiters' . " " .$e->getMessage());
				die();
			}
		}else{
			$d = Date('Ymdhis');

			/////////////////////////////////////////////////////
			// 企業情報の設定
			/////////////////////////////////////////////////////
			$c_db = "recruiters";
			$db = $c_db . "_" . $d;

			//////////////////////////////////////////////////////
			// recruiters table作成
			/////////////////////////////////////////////////////
			$sql = "CREATE TABLE IF NOT EXISTS `" . $db . "` (
				`r_id` int(11) NOT NULL,
				`r_client` int(255) NOT NULL,
				`r_title` varchar(255) NOT NULL,
				`r_kana` varchar(255) NOT NULL,
				`r_division` varchar(255) NOT NULL,
				`r_owner` varchar(255) NOT NULL,
				`r_memo` longtext NOT NULL,
				`r_tel` varchar(255) NOT NULL,
				`r_mail` varchar(255) NOT NULL,
				`r_street` varchar(255) NOT NULL,
				`r_updated` datetime NOT NULL,
				PRIMARY KEY  (`r_id`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
			$stmt = $pdo -> prepare($sql);
			$r = $stmt -> execute();
			if (!$r) {
				throw new Exception('create 失敗');
			}

			//////////////////////////////////////////////////////
			// recruiters table挿入
			/////////////////////////////////////////////////////
			try {
				foreach($recruiters as $k=>$v){
					$pdo->beginTransaction();
					$stmt = $pdo -> prepare("INSERT INTO "  . $db . " (r_id, r_client, r_title, r_kana, r_division, r_owner, r_memo, r_tel, r_mail, r_street, r_updated) VALUES (:r_id, :r_client, :r_title, :r_kana, :r_division, :r_owner, :r_memo, :r_tel, :r_mail, :r_street,  :r_updated)");
					$stmt->bindParam(':r_id', $r_id, PDO::PARAM_INT);
					$stmt->bindParam(':r_client', $client, PDO::PARAM_STR);
					$stmt->bindParam(':r_title', $recruiter_title, PDO::PARAM_STR);
					$stmt->bindParam(':r_kana', $kana, PDO::PARAM_STR);
					$stmt->bindParam(':r_division', $division, PDO::PARAM_STR);
					$stmt->bindParam(':r_owner', $owner, PDO::PARAM_STR);
					$stmt->bindParam(':r_memo', $memo, PDO::PARAM_STR);
					$stmt->bindParam(':r_tel', $tel, PDO::PARAM_STR);
					$stmt->bindParam(':r_mail', $mail, PDO::PARAM_STR);
					$stmt->bindParam(':r_street', $street, PDO::PARAM_STR);
					$stmt->bindParam(':r_updated', $v['r_updated'], PDO::PARAM_INT);

					$r_id = empty($v['r_id']) ? "" : $v['r_id'];
					$recruiter_title = empty($v['recruiters_title']) ? "" : $v['recruiters_title'];

					//client
					if(!empty($v['recruiters_client']['Client']['Client.P_Id'])){
						$client = $v['recruiters_client']['Client']['Client.P_Id'];
					}else{
						$client = "";
					}	

					//kana
					if(!empty($v['recruiters_kana'])){
						if(is_array($v['recruiters_kana'])){
							$tmpkana="";
							foreach($v['recruiters_kana'] as $tmpk => $tmpv){
								$tmpkana.=$tmpv;
							}
							$kana = $tmpkana;
						}else{
							$kana = $v['recruiters_kana'];
						}
					}else{
						$kana = "";
					}	

					//division
					if(!empty($v['recruiters_division'])){
						if(is_array($v['recruiters_division'])){
							$tmpdiv="";
							foreach($v['recruiters_division'] as $tmpk => $tmpv){
								$tmpdiv.=$tmpv;
							}
							$division = $tmpdiv;
						}else{
							$division = $v['recruiters_division'];
						}
					}else{
						$division = "";
					}	

					//owner
					if(!empty($v['recruiters_owner']['User']['User.P_Id'])){
						$owner = $v['recruiters_owner']['User']['User.P_Id'];
					}else{
						$owner = "";
					}

					//memo
					if(!empty($v['recruiters_memo'])){
						if(is_array($v['recruiters_memo'])){
							$tmpmemo="";
							foreach($v['recruiters_memo'] as $tmpk => $tmpv){
								$tmpmemo.=$tmpv;
							}
							$memo = $tmpmemo;
						}else{
							$memo = $v['recruiters_memo'];
						}
					}else{
						$memo = "";
					}

					//tel
					if(!empty($v['recruiters_tel'])){
						if(is_array($v['recruiters_tel'])){
							$tmptel="";
							foreach($v['recruiters_tel'] as $tmpk => $tmpv){
								$tmptel.=$tmpv;
							}
							$tel = $tmptel;
						}else{
							$tel = $v['recruiters_tel'];
						}
					}else{
						$tel = "";
					}

					//mail
					if(!empty($v['recruiters_mail'])){
						if(is_array($v['recruiters_mail'])){
							$tmpmail="";
							foreach($v['recruiters_mail'] as $tmpk => $tmpv){
								$tmpmail.=$tmpv;
							}
							$mail = $tmpmail;
						}else{
							$mail = $v['recruiters_mail'];
						}
					}else{
						$mail = "";
					}

					//street
					if(!empty($v['recruiters_street'])){
						if(is_array($v['recruiters_street'])){
							$tmpstreet="";
							foreach($v['recruiters_street'] as $tmpk => $tmpv){
								$tmpstreet.=$tmpv;
							}
							$street = $tmpstreet;
						}else{
							$street = $v['recruiters_street'];
						}
					}else{
						$street = "";
					}


					$r = $stmt->execute();
					if (!$r) {
						throw new Exception('insert '. $db .' 失敗');
					}
					//commit
					$pdo->commit();
				}
			} catch (PDOException $e) {
				//rollback
				$pdo->rollBack();
				print('Error:'.$db . " " .$e->getMessage());
				die();
			}

			//件数チェック
			$stmt = $pdo -> query("SELECT * FROM " . $db);
			$count = $stmt -> rowCount();
	
			if($total == $count){
				//古いDBを削除
				$sql = "DROP TABLE IF EXISTS " . $c_db . "_tmp";
				$pdo -> exec($sql);

				//既存のDBをtmpにリネーム
				$stmt = $pdo -> prepare("alter table " . $c_db . " rename to recruiters_tmp");
				$stmt->execute();

				//今回作成したDBをリネーム
				$stmt = $pdo -> prepare("alter table " . $db . " rename to recruiters");
				$res = $stmt->execute();
				if(!$res){
					$ret = "企業担当情報のデータ登録でエラーがありました。";
				}
			}else{
				$ret = "企業担当情報でエラーが発生しました。<br />" . "HRBC件数：" . $total . "<br />" . "insert件数：" . $count;
			}
			return $ret;
		}
	}
////////////////////////////////////////////////
//リクルーター追加　ここまで
///////////////////////////////////////////////

	//////////////////////////////////////////////////////////
	//業種 Option.U_000892
	//////////////////////////////////////////////////////////
	public function industry()
	{				
		$msg="";
		$limit=$this->hrbc_limit;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

		$url = $this->hrbc_option_url; 

		//送信パラメータ
//		$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'alias'=>'Option.U_000892','enabled'=>1));

            $url = $this->hrbc_option_url .  "?partition=". $partition . "&count=".$limit. "&alias=Option.U_000892&enabled=1";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);


/*
		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/

		//HRBC業種カウント
		$this->HrbcCountCheck("gyoshu");

/*
		$xml = file_get_contents($unit_url, false, $context);
		$xml = simplexml_load_string($xml);
*/
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//データ設定
		$json = json_encode($xml);

		$fp = fopen($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/industry.json", "w" );
		fputs($fp, $json);
		$flag = 	fclose( $fp );

		if($flag){
			$msg = "正常終了しました。";
		}else{
			$msg = "更新に失敗しました";;
		}

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//職種 Option.P_JobCategory
	//////////////////////////////////////////////////////////
	function jobcategory()
	{				
		$msg="";
		$limit=$this->hrbc_limit;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

		$url = $this->hrbc_option_url; 

		//送信パラメータ
//		$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'alias'=>'Option.U_000714','enabled'=>1));

            $url = $this->hrbc_option_url .  "?partition=". $partition . "&count=".$limit. "&alias=Option.U_000714&enabled=1";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

/*
		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/
		//HRBC職種カウント
		$this->HrbcCountCheck("shokushu");

/*
		$xml = file_get_contents($unit_url, false, $context);
		$xml = simplexml_load_string($xml);
*/
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//データ設定
		$json = json_encode($xml);

		$fp = fopen($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/jobcategory.json", "w" );
		fputs($fp, $json);
		$flag = 	fclose( $fp );

		if($flag){
			$msg = "正常終了しました。";
		}else{
			$msg = "更新に失敗しました";;
		}

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//雇用形態　Option.P_EmploymentType
	//////////////////////////////////////////////////////////
	function employ()
	{				
		$msg="";
		$limit=$this->hrbc_limit;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

		$url = $this->hrbc_option_url; 

            $url = $this->hrbc_option_url .  "?partition=". $partition . "&count=".$limit. "&alias=Option.P_EmploymentType&enabled=1";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

/*
		//送信パラメータ
		$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'alias'=>'Option.P_EmploymentType','enabled'=>1));

		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/

		//HRBC雇用者カウント
		$this->HrbcCountCheck("employee");

//		$xml = file_get_contents($unit_url, false, $context);
//		$xml = simplexml_load_string($xml);
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//データ設定
		$json = json_encode($xml);

		$fp = fopen($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/employment.json", "w" );
		fputs($fp, $json);
		$flag = 	fclose( $fp );

		if($flag){
			$msg = "正常終了しました。";
		}else{
			$msg = "更新に失敗しました";;
		}

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//都道府県　Option.P_Area
	//////////////////////////////////////////////////////////
	function area()
	{				
		$msg="";
		$limit=$this->hrbc_limit;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

		$url = $this->hrbc_option_url; 

            $url = $this->hrbc_option_url .  "?partition=". $partition . "&count=".$limit. "&alias=Option.P_Area&enabled=1";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);
		//送信パラメータ
/*
		$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'alias'=>'Option.P_Area','enabled'=>1));

		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/
		//HRBCエリアカウント
		$this->HrbcCountCheck("area");

/*
		$xml = file_get_contents($unit_url, false, $context);
		$xml = simplexml_load_string($xml);
*/
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//データ設定
		$json = json_encode($xml);

		$fp = fopen($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/area.json", "w" );
		fputs($fp, $json);
		$flag = 	fclose( $fp );

		if($flag){
			$msg = "正常終了しました。";
		}else{
			$msg = "更新に失敗しました";;
		}

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}


	//////////////////////////////////////////////////////////
	//都道府県　U_000995
	//////////////////////////////////////////////////////////
	function prefecture()
	{				
		$msg="";
		$limit=$this->hrbc_limit;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

            $url = $this->hrbc_option_url .  "?partition=". $partition . "&count=".$limit. "&alias=Option.U_000995&enabled=1";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

/*
		$url = $this->hrbc_option_url; 

		//送信パラメータ
		$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'alias'=>'Option.U_000995','enabled'=>1));

		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/

		//HRBC都道府県カウント
		$this->HrbcCountCheck("pref");

/*
		$xml = file_get_contents($unit_url, false, $context);
		$xml = simplexml_load_string($xml);
*/
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//データ設定
		$json = json_encode($xml);

		$fp = fopen($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/pref.json", "w" );
		fputs($fp, $json);
		$flag = 	fclose( $fp );

		if($flag){
			$msg = "正常終了しました。";
		}else{
			$msg = "更新に失敗しました";;
		}

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}


	//////////////////////////////////////////////////////////
	//学歴　U_000165
	//////////////////////////////////////////////////////////
	function background()
	{				
		$msg="";
		$limit=$this->hrbc_limit;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

            $url = $this->hrbc_option_url .  "?partition=". $partition . "&count=".$limit. "&alias=Option.U_000165&enabled=1";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);
/*
		$url = $this->hrbc_option_url; 

		//送信パラメータ
		$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'alias'=>'Option.U_000165','enabled'=>1));

		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/
		//HRBCバックグラウンドカウント
		$this->HrbcCountCheck("background");

//		$xml = file_get_contents($unit_url, false, $context);
//		$xml = simplexml_load_string($xml);
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//データ設定
		$json = json_encode($xml);

		$fp = fopen($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/background.json", "w" );
		fputs($fp, $json);
		$flag = 	fclose( $fp );

		if($flag){
			$msg = "正常終了しました。";
		}else{
			$msg = "更新に失敗しました";;
		}

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//性別　P_Gender
	//////////////////////////////////////////////////////////
	public function gender()
	{				
		$msg="";
		$limit=$this->hrbc_limit;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

//		$url = $this->hrbc_option_url; 
            $url = $this->hrbc_option_url .  "?partition=". $partition . "&count=".$limit. "&alias=Option.P_Gender&enabled=1";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);
/*
		//送信パラメータ
		$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'alias'=>'Option.P_Gender','enabled'=>1));

		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/
		//HRBC性別カウント
		$this->HrbcCountCheck("gender");

//		$xml = file_get_contents($unit_url, false, $context);
//		$xml = simplexml_load_string($xml);
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//データ設定
		$json = json_encode($xml);

		$fp = fopen($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/gender.json", "w" );
		fputs($fp, $json);
		$flag = 	fclose( $fp );

		if($flag){
			$msg = "正常終了しました。";
		}else{
			$msg = "更新に失敗しました";;
		}

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//就業状況　P_WorkStatus
	//////////////////////////////////////////////////////////
	public function work()
	{				
		$msg="";
		$limit=$this->hrbc_limit;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

            $url = $this->hrbc_option_url .  "?partition=". $partition . "&count=".$limit. "&alias=Option.P_WorkStatus&enabled=1";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

/*
		$url = $this->hrbc_option_url; 

		//送信パラメータ
		$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'alias'=>'Option.P_WorkStatus','enabled'=>1));

		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/
		//HRBCワークステータスカウント
		$this->HrbcCountCheck("workstatus");

//		$xml = file_get_contents($unit_url, false, $context);
//		$xml = simplexml_load_string($xml);
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//データ設定
		$json = json_encode($xml);

		$fp = fopen($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/work.json", "w" );
		fputs($fp, $json);
		$flag = 	fclose( $fp );

		if($flag){
			$msg = "正常終了しました。";
		}else{
			$msg = "更新に失敗しました";;
		}

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//転勤可否　U_001098
	//////////////////////////////////////////////////////////
	function tenkin()
	{				
		$msg="";
		$limit=$this->hrbc_limit;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;


            $url = $this->hrbc_option_url .  "?partition=". $partition . "&count=".$limit. "&alias=Option.U_001098&enabled=1";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);
/*

		$url = $this->hrbc_option_url; 

		//送信パラメータ
		$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'alias'=>'Option.U_001098','enabled'=>1));

		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/
		//HRBC転勤カウント
		$this->HrbcCountCheck("tenkin");

/*
		$xml = file_get_contents($unit_url, false, $context);
		$xml = simplexml_load_string($xml);
*/
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//データ設定
		$json = json_encode($xml);

		$fp = fopen($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/tenkin.json", "w" );
		fputs($fp, $json);
		$flag = 	fclose( $fp );

		if($flag){
			$msg = "正常終了しました。";
		}else{
			$msg = "更新に失敗しました";;
		}

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//年収マイページ用　U_001345
	//////////////////////////////////////////////////////////
	function income()
	{				
		$msg="";
		$limit=$this->hrbc_limit;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

            $url = $this->hrbc_option_url .  "?partition=". $partition . "&count=".$limit. "&alias=Option.U_001345&enabled=1";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

/*
		$url = $this->hrbc_option_url; 

		//送信パラメータ
		$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'alias'=>'Option.U_001345','enabled'=>1));

		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/
		//HRBC収入カウント
		$this->HrbcCountCheck("income");

//		$xml = file_get_contents($unit_url, false, $context);
//		$xml = simplexml_load_string($xml);
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//データ設定
		$json = json_encode($xml);

		$fp = fopen($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/income.json", "w" );
		fputs($fp, $json);
		$flag = 	fclose( $fp );

		if($flag){
			$msg = "正常終了しました。";
		}else{
			$msg = "更新に失敗しました";;
		}

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//職種マイページCD用　Option.U_001356
	//////////////////////////////////////////////////////////
	function mypagejob()
	{				
		$msg="";
		$limit=$this->hrbc_limit;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

            $url = $this->hrbc_option_url .  "?partition=". $partition . "&count=".$limit. "&alias=Option.U_001356&enabled=1";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

/*
		$url = $this->hrbc_option_url; 

		//送信パラメータ
		$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'alias'=>'Option.U_001356','enabled'=>1));

		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/
		//HRBCマイページ職種カウント
		$this->HrbcCountCheck("shokushu_mypage");

//		$xml = file_get_contents($unit_url, false, $context);
//		$xml = simplexml_load_string($xml);
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//データ設定
		$json = json_encode($xml);

		$fp = fopen($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/jobmypagecd.json", "w" );
		fputs($fp, $json);
		$flag = 	fclose( $fp );

		if($flag){
			$msg = "正常終了しました。";
		}else{
			$msg = "更新に失敗しました";;
		}

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}

	//////////////////////////////////////////////////////////
	//職種マイページCD_TEST用　Option.U_001356
	//////////////////////////////////////////////////////////
	function mypagejob_test()
	{				
		$msg="";
		$limit=$this->hrbc_limit;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

		$url = $this->hrbc_option_url; 

		//送信パラメータ
		$data = http_build_query(array('partition'=>$partition, 'count'=>$limit , 'alias'=>'Option.U_011518','enabled'=>1));

		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);

		$xml = file_get_contents($unit_url, false, $context);
		$xml = simplexml_load_string($xml);
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//データ設定
		$json = json_encode($xml);

		$fp = fopen($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/jobmypagecd_test.json", "w" );
		fputs($fp, $json);
		$flag = 	fclose( $fp );

		if($flag){
			$msg = "正常終了しました。";
		}else{
			$msg = "更新に失敗しました";;
		}

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}


	//////////////////////////////////////////////////////////
	// ＪＯＢ担当ＵＳＥＲ
	//////////////////////////////////////////////////////////
	function mypageowner()
	{				
		$msg="";
		$limit=$this->hrbc_limit;

		//ファイルが無ければ再作成
		$cpass = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/token/cache.json";
		$this->getHrbcToken($cpass);

		//ファイル読込
		$json = file_get_contents($cpass, true);
		$arr = json_decode($json,true);
		$token = $arr['token'];
		$partition = $arr['partition'];
	
		$flg=0;

            $url = $this->hrbc_user_url .  "?request_type=1&partition=". $partition . "&count=".$limit. "&user_type=1";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

/*
		$url = $this->hrbc_user_url; 

		//送信パラメータ
		$data = http_build_query(array('request_type'=>1, 'partition'=>$partition, 'count'=>$limit , 'user_type'=>-1));

		$unit_url = $url . "?" . $data;

		$opts = array(
			'http'=>array(
				'method' => 'GET',
				'header' => "X-porters-hrbc-oauth-token: $token\r\n"
							. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
			)
		);
		$context = stream_context_create($opts);
*/

		//HRBCジョブ担当カウント
		$this->HrbcCountCheck("jobowner");

//		$xml = file_get_contents($unit_url, false, $context);
//		$xml = simplexml_load_string($xml);
		if($xml->Code!=0){
			$pErr=$xml->Code;
			print("エラーが発生しました。<br />コード：" . $pErr);
			exit;
		}

		//データ設定
		$json = json_encode($xml);

		$fp = fopen($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/jobowner.json", "w" );
		fputs($fp, $json);
		$flag =  fclose( $fp );

		if($flag){
			$msg = "正常終了しました。";
		}else{
			$msg = "更新に失敗しました";;
		}

		//ホームに戻る
		$this->session->set_userdata(array("msg"=>$msg));
		redirect("hrbc/home");
	}


    //////////////////////////////////////////////////////////
	//token取得用
	//////////////////////////////////////////////////////////
	private function getHrbcToken($cpass){
		//ファイルが無ければ再作成
		if(!file_exists($cpass)){
			require($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/conf/auth.php');
		}

		//ファイル時間20分ごとのcronでミスがあったとき
		$ftime = filemtime($cpass);
		$twminbf = strtotime( "-20 min" );
		if($ftime < $twminbf){
			require($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/conf/auth.php');
		}

		if(empty($token) || empty($partition)){
			require($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/conf/auth.php');
		}
	}

	//////////////////////////////////////////////////////////
	//登録処理
	//////////////////////////////////////////////////////////
	function maintenance()
	{
		$posts = $this->input->post();

		//バリデーション実行
		$error = "";
		$error = $this->setMaintenanceValidation();

		if($posts["start"] !== date("Y/m/d H:i:s", strtotime($posts["start"]))){
			$error.="<p>メンテナンス開始日の形式が異なります。</p>";
		}
		if($posts["end"] !== date("Y/m/d H:i:s", strtotime($posts["end"]))){
			$error.="<p>メンテナンス終了日の形式が異なります。</p>";
		}

		//オブジェクトへ変換
		$data['maintenance'] = (object)$posts;

		if($error !== ""){
			//data設定
			$data["render"] = "hrbc/home";
			$data["clean"] = $this->clean;
			$data["msg"] = $error;
			$data["csrf"] = $this->_get_csrf_nonce();

	 		$this->load->view("template", $data);
		}else{
			if($posts['id'] != 1){
				$this->session->set_userdata(array("msg"=>"不正な処理が行われました。"));
				redirect("hrbc/home");
			}
			$param['start'] = $posts['start'];
			$param['end'] = $posts['end'];

			$this->load->model('maintenances_model');
			$ret = $this->maintenances_model->save(1, $param);
		
			if(empty($ret)){
				//エラー処理
			}else{
				//登録成功
				$this->session->set_userdata(array("msg"=>"登録が完了しました。"));
				redirect("hrbc/home");
			}
		}

	}

	//////////////////////////////////////////////////////////
	//バリデーション
	//////////////////////////////////////////////////////////
	private function setMaintenanceValidation($validation=null)
	{

		$errors ="";
		$this->form_validation->set_rules("start", "メンテナンス開始日","required");
		$this->form_validation->set_rules("end", "メンテナンス終了日","required");

		if($this->form_validation->run() == FALSE){
			$errors = validation_errors();
		}
		return $errors;
	}

	//////////////////////////////////////////////////////////
	//HRBC未登録者チェック
	//////////////////////////////////////////////////////////
	function email()
	{
		$posts = $this->input->post();

		$search="";
		if(!empty($posts["search"])){
			$search = $posts["search"];
		}

		$param=array("del"=>0);

		if(!empty($search)){
			$param["hrbc_resume_id"] = $search;
		}

		$msg = $this->session->userdata('msg');
		$this->session->unset_userdata('msg');

		$this->load->library("pagination");

		//Pagination
		$config['base_url'] = base_url() . $this->uri->segment(1) . "/" . $this->uri->segment(2);
//		$config["total_rows"] = $this->db->get_where("members", array("activate"=>0))->num_rows();
		$config["total_rows"] = $this->db->get_where("members", $param)->num_rows();
 		$config["per_page"] = $this->page_limit;
 		$config["num_links"] = 2;
		$this->pagination->initialize($config);

		//data設定
//		$data["contents"] = $this->db->order_by('updated desc')->get_where("members", array("activate"=>0), $config["per_page"], $this->uri->segment(3));
//		$data["contents"] = $this->db->order_by('updated desc')->get_where("members_pre", array(), $config["per_page"], $this->uri->segment(3));
		$data["contents"] = $this->db->order_by('updated desc')->get_where("members", $param, $config["per_page"], $this->uri->segment(3));
		$data["render"] = "hrbc/email";
		$data["clean"] = $this->clean;
		$data["csrf"] = $this->_get_csrf_nonce();
		$data["msg"] = $msg;

 		$this->load->view("template", $data);
	}

	//////////////////////////////////////////////////////////
	//登録情報削除
	//////////////////////////////////////////////////////////
	public function maildel()
	{
		$posts = $this->input->post();

		//CSRFチェック
		if ($this->_valid_csrf_nonce() === FALSE)
		{
			$this->session->set_userdata(array("msg"=>"不正な処理が行われました。"));
			redirect("hrbc/email");
		}

		if($posts['id'] && is_numeric($posts['id'])){
			$data = array(
			               'del' => 1
		            );
			$this->db->where('id', $posts['id']);
			$ret = $this->db->update('members', $data); 

			if($ret){
				//削除成功
				$this->session->set_userdata(array("msg"=>"削除が完了しました。"));
				redirect("hrbc/email");
			}else{
				$this->session->set_userdata(array("msg"=>"削除に失敗しました。"));
				redirect("hrbc/email");
			}
		}else{
			redirect("auth/login");
		}
	}

	//////////////////////////////////////////////////////////
	// CSVダウンロード
	//////////////////////////////////////////////////////////
	function csvdownload()
	{

		$this->load->dbutil();
		$this->load->helper('download');

		$data_title = array('CpiID','PersonID','ResumeID','メールアドレス','ステータス', '更新日');
 
		// タイトル設定
		$csv = "";
		foreach ($data_title as $i => $value) {
		    $value = mb_convert_encoding($value, 'sjis-win', 'utf-8');
		    $csv .= ($i ? ',' : ''). $value;
		}
		$csv .= "\n"; // 改行コード
 
		// コンテンツ設定
//		$contents = $this->db->order_by('updated desc')->get_where("members", array("activate"=>0));
		$contents = $this->db->order_by('updated desc')->get_where("members", array("del"=>0));
		$error_data="";
		if(!empty($contents)){
			foreach ($contents->result()  as $val){
				if(!empty($val->email) && !empty($val->iv)){
					$iv = base64_decode($val->iv);
					$CryptTarget= base64_decode($val->email);
					$DecryptTarget_ = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, mailSalt, $CryptTarget, MCRYPT_MODE_CBC, $iv);
					$mail = $this->clean->purify( $DecryptTarget_);

					//status
					$status = mb_convert_encoding( $val->status , 'sjis-win', 'utf-8');
					$tmp_status="";
					if($status==1){
						$tmp_status = "メール認証離脱";
					}else if($status==2){
						$tmp_status = "STEP1離脱";
					}else if($status==3){
						$tmp_status = "登録完了";
					}

					if (preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/iD', $mail)){
						if(!empty($mail)){
							$id = mb_convert_encoding( $val->id , 'sjis-win', 'utf-8');

							$hrbc_person_id = mb_convert_encoding( $val->hrbc_person_id , 'sjis-win', 'utf-8');
							$hrbc_resume_id = mb_convert_encoding( $val->hrbc_resume_id , 'sjis-win', 'utf-8');
							
							$tmp_status = mb_convert_encoding( $tmp_status , 'sjis-win', 'utf-8');
							$email = mb_convert_encoding( $mail , 'sjis-win', 'utf-8');
							$updated = mb_convert_encoding( $val->updated , 'sjis-win', 'utf-8');
							$csv .= $id. "," . $hrbc_person_id."," . $hrbc_resume_id."," . $email. "," . $tmp_status.",". $updated. "\n";
						}
					}
				}else{

					$error_data .= $val->id . "\n";
				}
	
			}

			if(!empty($error_data)){
					$this->load->library('email');

					$this->email->from('mypage@hurex.jp', 'mypage');
					$this->email->to('tosa@smt-net.co.jp');

					$this->email->subject('Email Regist Error');
					$this->email->message($error_data);

					$this->email->send();

			}
			header("Content-Type: application/octet-stream; charset=sjis-win");
			header("Content-Disposition: attachment; filename=download.csv");
			echo $csv;
		}else{
			$this->session->set_userdata(array("msg"=>"対象データがありません"));
			redirect("hrbc/email");
		}

	}

	//////////////////////////////////////////////////////////
	//HRBC未登録者チェック
	//////////////////////////////////////////////////////////
	function emailerr()
	{
		$posts = $this->input->post();

		$search="";
		if(!empty($posts["search"])){
			$search = $posts["search"];
		}

		$msg = $this->session->userdata('msg');
		$this->session->unset_userdata('msg');

		$cnt=0;
		$contents = $this->db->order_by('updated desc')->get_where("members", array("del"=>0));
		if(!empty($contents)){
			foreach ($contents->result()  as $val){
				$iv = base64_decode($val->iv);
				$CryptTarget= base64_decode($val->email);
				$DecryptTarget_ = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, mailSalt, $CryptTarget, MCRYPT_MODE_CBC, $iv);
				$mail = $this->clean->purify( $DecryptTarget_);

				//status
				$status = mb_convert_encoding( $val->status , 'sjis-win', 'utf-8');
				$tmp_status="";
				if($status==1){
					$tmp_status = "メール認証離脱";
				}else if($status==2){
					$tmp_status = "STEP1離脱";
				}else if($status==3){
					$tmp_status = "登録完了";
				}

				if (!preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/iD', $mail)){
					$data["contents"][$cnt]["id"] = $val->id;
					$data["contents"][$cnt]["iv"] = $val->iv;
					$data["contents"][$cnt]["hrbc_person_id"] = $val->hrbc_person_id;
					$data["contents"][$cnt]["hrbc_resume_id"] = $val->hrbc_resume_id;
					
					$data["contents"][$cnt]["tmp_status"] = $tmp_status;
					$data["contents"][$cnt]["email"] = $mail;
					$data["contents"][$cnt]["updated"] = $val->updated;
					$cnt++;
				}
			}
		}

		$data["render"] = "hrbc/emailerr";
		$data["clean"] = $this->clean;
		$data["csrf"] = $this->_get_csrf_nonce();
		$data["msg"] = $msg;

 		$this->load->view("template", $data);
	}

	//////////////////////////////////////////////////////////
	// HRBCアクセスチェック
	//////////////////////////////////////////////////////////
	function HrbcCountCheck($param)
	{
		$this_month = date('Ym');
		$date = date('Y-m-d H:i:s');
		$ip = $_SERVER["REMOTE_ADDR"];
		$add = $date . " from " . $ip;
		$file = $_SERVER["DOCUMENT_ROOT"] . "/../count/" . $param . "_" . $this_month .".txt";
		if(!file_exists($file)){
			touch($file);
		}
		$fp = fopen( $file, "a+" );
		@fwrite($fp, $add . PHP_EOL);
		fclose( $fp );
	}
}

