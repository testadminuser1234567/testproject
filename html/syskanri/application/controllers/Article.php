<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends MY_Controller {

	protected $page_limit = 20;

	function __construct()
	{
		parent::__construct();
	}

	//////////////////////////////////////////////////////////
	//一覧出力
	//////////////////////////////////////////////////////////
	function home()
	{
		$posts = $this->input->post();

		$msg = $this->session->userdata('msg');
		$this->session->unset_userdata('msg');

		$this->load->library("pagination");

		//Pagination
		$config['base_url'] = base_url() . $this->uri->segment(1) . "/" . $this->uri->segment(2);
		$config["total_rows"] = $this->db->get_where("articles", array("del"=>0))->num_rows();
 		$config["per_page"] = $this->page_limit;
 		$config["num_links"] = 2;
		$this->pagination->initialize($config);

		//data設定
		$data["contents"] = $this->db->order_by('create_date desc, updated desc')->get_where("articles", array("del"=>0), $config["per_page"], $this->uri->segment(3));
		$data["render"] = "article/home";
		$data["clean"] = $this->clean;
		$data["csrf"] = $this->_get_csrf_nonce();
		$data["msg"] = $msg;

 		$this->load->view("template", $data);
	}

	//////////////////////////////////////////////////////////
	//登録情報入力
	//////////////////////////////////////////////////////////
	function regist()
	{
		$posts = $this->input->post();
		
		//idが設定されていて戻るじゃない場合は編集
		if(!empty($posts['id']) && empty($posts['mode'])){
			$data['article'] = $this->db->get_where('articles', array('id' => $posts['id']))->row();
		}else{
			$data['article'] = (object) $posts;
			//idがない場合は空データ設定
			if(empty($data['article']->id)){
				$data['article']->id = "";
			}
		}
				
		$error = "";

		//data設定
		$data["render"] = "article/regist";
		$data["clean"] = $this->clean;
		$data["csrf"] = $this->_get_csrf_nonce();
		$data["msg"] = "";

 		$this->load->view("template", $data);

	}

	//////////////////////////////////////////////////////////
	//登録情報確認
	//////////////////////////////////////////////////////////
	function confirm()
	{
		//バリデーション実行
		$error = "";
		$error = $this->setValidation();

		$posts = $this->input->post();

		//オブジェクトへ変換
		$data['article'] = (object)$posts;

		if($error !== ""){
			//data設定
			$data["render"] = "article/regist";
			$data["clean"] = $this->clean;
			$data["csrf"] = $this->_get_csrf_nonce();
			$data["msg"] = $error;

	 		$this->load->view("template", $data);
		}else{
			//data設定
			$data["render"] = "article/confirm";
			$data["clean"] = $this->clean;
			$data["csrf"] = $this->_get_csrf_nonce();
			$data["msg"] = $error;

			$this->load->view("template", $data);
		}
	}

	//////////////////////////////////////////////////////////
	//登録処理
	//////////////////////////////////////////////////////////
	function add()
	{
		//CSRFチェック
		if ($this->_valid_csrf_nonce() === FALSE)
		{
			$this->session->set_userdata(array("msg"=>"不正な処理が行われました。"));
			redirect("article/home");
		}

		//バリデーション実行
		$error = "";
		$error = $this->setValidation();

		$posts = $this->input->post();

		if($error !== ""){
			//data設定
			$data["render"] = "article/regist";
			$data["clean"] = $this->clean;
			$data["csrf"] = $this->_get_csrf_nonce();
			$data["msg"] = $error;

	 		$this->load->view("template", $data);
		}else{
			if(empty($posts['id'])){
				$data['created'] = date('Y-m-d');
			}
			$data['id'] = $posts['id'];
			$data['title'] = $posts['title'];
			$data['create_date'] = $posts['create_date'];
			$data['category'] = $posts['category'];
			$data['content'] = $posts['content'];

			$this->load->model('articles_model');
			$ret = $this->articles_model->save($data['id'], $data);
		
			if(empty($ret)){
				//エラー処理
			}else{
				//登録成功
				$this->session->set_userdata(array("msg"=>"登録が完了しました。"));
				redirect("article/home");
			}
		}

	}

	//////////////////////////////////////////////////////////
	//登録情報削除
	//////////////////////////////////////////////////////////
	public function delete()
	{
		$posts = $this->input->post();

		//CSRFチェック
		if ($this->_valid_csrf_nonce() === FALSE)
		{
			$this->session->set_userdata(array("msg"=>"不正な処理が行われました。"));
			redirect("article/home");
		}

		if($posts['id'] && is_numeric($posts['id'])){
			$data = array(
			               'del' => 1
		            );
			$this->db->where('id', $posts['id']);
			$ret = $this->db->update('articles', $data); 

			if($ret){
				//削除成功
				$this->session->set_userdata(array("msg"=>"削除が完了しました。"));
				redirect("article/home");
			}else{
				$this->session->set_userdata(array("msg"=>"削除に失敗しました。"));
				redirect("article/home");
			}
		}else{
			redirect("auth/login");
		}
	}

	//////////////////////////////////////////////////////////
	//バリデーション
	//////////////////////////////////////////////////////////
	private function setValidation($validation=null)
	{

		$errors ="";
		$this->form_validation->set_rules("create_date", "日付","required");
		$this->form_validation->set_rules("title", "タイトル","required");
		$this->form_validation->set_rules("content", "本文","required");

		if($this->form_validation->run() == FALSE){
			$errors = validation_errors();
		}
		return $errors;
	}
}

