<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends MY_Controller {

	protected $page_limit = 20;

	function __construct()
	{
		parent::__construct();
	}

	//////////////////////////////////////////////////////////
	//一覧出力
	//////////////////////////////////////////////////////////
	function home()
	{
		$posts = $this->input->post();

		$msg = $this->session->userdata('msg');
		$this->session->unset_userdata('msg');
		

		if($posts){
			$data['company'] = (object) $posts;
		}else{
			$data['company'] = $this->db->get_where('companies', array('id' => 1))->row();
		}
				
		$error = "";

		//data設定
		$data["render"] = "company/home";
		$data["clean"] = $this->clean;
		$data["csrf"] = $this->_get_csrf_nonce();
		$data["msg"] = $msg;

 		$this->load->view("template", $data);
	}

	//////////////////////////////////////////////////////////
	//登録情報確認
	//////////////////////////////////////////////////////////
	function confirm()
	{
		//バリデーション実行
		$error = "";
		$error = $this->setValidation();

		$posts = $this->input->post();

		//オブジェクトへ変換
		$data['company'] = (object)$posts;

		if($error !== ""){
			//data設定
			$data["render"] = "company/regist";
			$data["clean"] = $this->clean;
			$data["csrf"] = $this->_get_csrf_nonce();
			$data["msg"] = $error;

	 		$this->load->view("template", $data);
		}else{
			//data設定
			$data["render"] = "company/confirm";
			$data["clean"] = $this->clean;
			$data["csrf"] = $this->_get_csrf_nonce();
			$data["msg"] = $error;

			$this->load->view("template", $data);
		}
	}

	//////////////////////////////////////////////////////////
	//登録処理
	//////////////////////////////////////////////////////////
	function add()
	{
		//CSRFチェック
		if ($this->_valid_csrf_nonce() === FALSE)
		{
			$this->session->set_userdata(array("msg"=>"不正な処理が行われました。"));
			redirect("company/home");
		}

		//バリデーション実行
		$error = "";
		$error = $this->setValidation();

		$posts = $this->input->post();

		if($error !== ""){
			//data設定
			$data["render"] = "company/regist";
			$data["clean"] = $this->clean;
			$data["csrf"] = $this->_get_csrf_nonce();
			$data["msg"] = $error;

	 		$this->load->view("template", $data);
		}else{
			if(empty($posts['id'])){
				$data['created'] = date('Y-m-d');
			}
			$data['id'] = $posts['id'];
			$data['content'] = $posts['content'];

			$this->load->model('companies_model');
			$ret = $this->companies_model->save($data['id'], $data);
		
			if(empty($ret)){
				//エラー処理
			}else{
				//登録成功
				$this->session->set_userdata(array("msg"=>"登録が完了しました。"));
				redirect("company/home");
			}
		}

	}

	//////////////////////////////////////////////////////////
	//バリデーション
	//////////////////////////////////////////////////////////
	private function setValidation($validation=null)
	{

		$errors ="";
		$this->form_validation->set_rules("content", "内容","required");

		if($this->form_validation->run() == FALSE){
			$errors = validation_errors();
		}
		return $errors;
	}
}

