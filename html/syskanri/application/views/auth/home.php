<div class="title clearfix">
<div class="midasi">
<p class="tit">ユーザー管理 </p>
</div>
<div class="sibori">
<p class="page">
<?php echo $this->pagination->create_links(); ?>
</p>
</div>
</div>
<?php if($role==1):?>
<form method="post" action="<?php echo base_url();?>auth/regist">
<input type="submit" name="submit" value="新規登録する" style="margin-left:30px;width:150px;height:30px;margin-bottom:10px">
</form>
<?php endif;?>
<div class="toukou">
<div class="naiyo">
<p style="color:#FF0000;margin-top:10px;margin-bottom:10px"><?php echo $clean->purify($msg);?></p>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<th style="width:10%">ユーザーID</th>
<th style="width:8%">編集</th>
<th style="width:8%">パスワード変更</th>
<?php if($role==1):?>
<th style="width:8%">削除</th>
<?php endif;?>
</tr>
<?php foreach ($contents->result()  as $val): ?>
<tr>
<td><?php echo $clean->purify($val->userid);?></td>
<td>
<form action="<?php echo base_url();?>auth/edit/" method="post">
<input type="submit" name="submit" value="編集する" />
<?php echo form_hidden($csrf); ?>
<input type="hidden" name="id" value="<?php echo $clean->purify($val->id);?>" />
</form>
</td>
<td>
<form action="<?php echo base_url();?>auth/change_password/" method="post">
<input type="submit" name="submit" value="パスワードを変更する" />
<input type="hidden" name="id" value="<?php echo $clean->purify($val->id);?>" />
</form>
</td>
<?php if($role==1):?>
<td>
<?php if($val->role!=1):?>
<form action="<?php echo base_url();?>auth/delete/" method="post">
<input type="submit" name="submit" value="削除する" onclick="return confirm('削除してよろしいですか？')" />
<?php echo form_hidden($csrf); ?>
<input type="hidden" name="id" value="<?php echo $clean->purify($val->id);?>" />
</form>
<?php else:?>
<center>―</center>
<?php endif;?>
</td>
<?php endif;?>
</tr>
<?php endforeach; ?>
</table>
</div>
