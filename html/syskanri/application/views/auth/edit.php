<div class="title clearfix">
<p class="tit2">ユーザー情報を更新する</p>
<div class="naiyoToko">
<br/>
<p class="txt">▼下記の項目をご記入下さい。</p>
<div class="error"><?php echo $msg ?></div>
<form name="frm" action="<?php echo base_url();?>auth/update" method="post">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<th class="hissu">ユーザーID</th>
<td>
<?php echo $clean->purify(@$auth->userid);?>
<input type="hidden" name="userid" value="<?php echo $clean->purify(@$auth->userid);?>" />
</td>
</tr>
<tr>
<th class="hissu">メールアドレス</th>
<td>
<input name="email" type="text" id="email" value="<?php echo $clean->purify(@$auth->email);?>" />
</td>
</tr>
</table>
</div>
</div>

<div class="toukou noLine clearfix">
<p class="back"><a href="<?php echo base_url();?>auth/home">&lt;&lt;ユーザー一覧へ戻る</a></p>
<p class="b" style="text-align:center">
<input type="hidden" name="id" value="<?php echo @$auth->id;?>" />
<?php echo form_hidden($csrf); ?>
<input type="submit" value="登録する" style="width: 150px; height: 30px;font-weight:bold;" onclick="viewport();" />
</div>
</div>
</form>
