<div class="title clearfix">
<p class="tit2">ユーザー情報を登録する</p>
<div class="naiyoToko">
<br/>
<p class="txt">▼下記の項目をご記入下さい。</p>
<div class="error"><?php echo $msg ?></div>
<form name="frm" action="<?php echo base_url();?>auth/add" method="post">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<th class="hissu">ユーザーID</th>
<td>
<input name="userid" type="text" id="userid" value="<?php echo $clean->purify(@$auth->userid);?>" />　 <span style="color:#FF0000">※半角英数4文字以上12文字以下</span>
</td>
</tr>
<tr>
<th class="hissu">メールアドレス</th>
<td>
<input name="email" type="text" id="email" value="<?php echo $clean->purify(@$auth->email);?>" />
</td>
</tr>
<tr>
<th class="hissu">パスワード</th>
<td><input name="password" type="password" id="password" value="<?php echo $clean->purify(@$auth->password);?>" />　 <span style="color:#FF0000">※半角英数10文字以上20文字以下</span></td>
</tr>
<tr>
</table>
</div>
</div>

<div class="toukou noLine clearfix">
<p class="back"><a href="<?php echo base_url();?>auth/home">&lt;&lt;ユーザー一覧へ戻る</a></p>
<p class="b" style="text-align:center">
<input type="hidden" name="id" value="<?php echo @$auth->id;?>" />
<?php echo form_hidden($csrf); ?>
<input type="submit" value="登録する" style="width: 150px; height: 30px;font-weight:bold;" onclick="viewport();" />
</div>
</div>
</form>
