<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js" charset="utf8"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/loading.js" charset="utf8"></script>
<script src="<?php echo base_url();?>js/viewport.js" type="text/javascript" charset="utf8"></script>
<div class="title clearfix">
<p class="tit2">新着情報を投稿する</p>
<div class="naiyoToko">
<br/>
<p class="txt">▼下記の項目をご確認下さい。</p>
<table border="0" cellspacing="0" cellpadding="0">

<tr>
<th>投稿日</th>
<td><?php echo $clean->purify($article->create_date);?></td>
</tr>
<tr>
<th>タイトル</th>
<td><?php echo $clean->purify(@$article->title);?></td>
</tr>
<tr>
<th>カテゴリー</th>
<td>
<?php if($article->category==1):?>お知らせ<?php else:?>イベント<?php endif;?>
</td>
</tr>
<tr>
<th>本文</th>
<td><?php echo $clean->purify(@$article->content);?></td>
</tr>

</table>


</div>
</div>

<table style="margin:0 auto;margin-top:10px">
<tr>
<td>
<form name="frm" action="<?php echo base_url();?>article/regist" method="post" enctype="multipart/form-data">
<input type="hidden" name="mode" value="return" />
<input type="submit" value="内容を修正する" style="width: 150px; height: 30px;font-weight:bold;float:right" />
<input type="hidden" name="id" value="<?php echo $clean->purify(@$article->id);?>" />
<input type="hidden" name="create_date" value="<?php echo $clean->purify($article->create_date);?>" />
<input type="hidden" name="title" value="<?php echo $clean->purify($article->title);?>" />
<input type="hidden" name="category" value="<?php echo $clean->purify($article->category);?>" />
<input type="hidden" name="content" value='<?php echo $clean->purify($article->content);?>' />
</form>
</td>
<td>
<form name="frm" action="<?php echo base_url();?>article/add" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $clean->purify(@$article->id);?>" />
<input type="hidden" name="create_date" value="<?php echo $clean->purify($article->create_date);?>" />
<input type="hidden" name="title" value="<?php echo $clean->purify($article->title);?>" />
<input type="hidden" name="category" value="<?php echo $clean->purify($article->category);?>" />
<input type="hidden" name="content" value='<?php echo $clean->purify($article->content);?>' />
<?php echo form_hidden($csrf); ?>
<input type="submit" value="登録する" style="width: 150px; height: 30px; margin-left:10px; font-weight:bold;" />
</form>
</td>
</tr>
</table>
<br/>
</div>