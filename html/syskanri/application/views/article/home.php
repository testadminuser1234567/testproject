<div class="title clearfix">
<div class="midasi">
<p class="tit">新着情報 </p>
</div>
<div class="sibori">
<p class="page">
<?php echo $this->pagination->create_links(); ?>
</p>
</div>
</div>
<div class="toukou">
<p>
<form method="post" action="<?php echo base_url();?>article/regist"><input type="submit" name="submit" value="新規登録する" style="width:150px;height:30px;margin-left:10px"></form>
</p>
<div class="naiyo">
<p style="color:#FF0000;margin-top:10px;margin-bottom:10px"><?php echo $clean->purify($msg);?></p>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<th style="width:5%">投稿日</th>
<th style="width:30%">タイトル</th>
<th style="width:8%">編集</th>
<th style="width:8%">削除</th>
</tr>
<?php foreach ($contents->result()  as $val): ?>
<tr>
<td style="text-align:center"><?php echo str_replace('-','/',$clean->purify($val->create_date));?></td>
<td><?php echo $clean->purify($val->title);?></td>
<td>
<form action="<?php echo base_url();?>article/regist/" method="post">
<input type="submit" name="submit" value="編集する" />
<?php echo form_hidden($csrf); ?>
<input type="hidden" name="id" value="<?php echo $clean->purify($val->id);?>" />
</form>
</td>
<td>
<form action="<?php echo base_url();?>article/delete/" method="post">
<input type="submit" name="submit" value="削除する" onclick="return confirm('削除してよろしいですか？')" />
<?php echo form_hidden($csrf); ?>
<input type="hidden" name="id" value="<?php echo $clean->purify($val->id);?>" />
</form>
</td>
</tr>
<?php endforeach; ?>
</table>
</div>
