<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js" charset="utf8"></script>
<script src="<?php echo base_url();?>js/HolidayChk.js" type="text/javascript" charset="utf8"></script>
<script src="<?php echo base_url();?>js/calendar.js" type="text/javascript" charset="utf8"></script>
<script src="<?php echo base_url();?>js/chkListsubwin.js" type="text/javascript" charset="utf8"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/loading.js" charset="utf8"></script>
<script type="text/javascript" src="<?php echo base_url();?>ckeditor/ckeditor.js" charset="utf8"></script>
<div class="title clearfix">
<p class="tit2">新着情報を投稿する</p>
<div class="naiyoToko">
<br/>
<p class="txt">▼下記の項目をご記入下さい。</p>
<div class="error"><?php echo $clean->purify($msg); ?></div>
<form name="frm" action="<?php echo base_url();?>article/confirm" method="post" enctype="multipart/form-data">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<th class="hissu">日付</th>
<td>
<input name="create_date" type="text" id="create_date" value="<?php if(empty($article->create_date)){ echo date('Y/m/d'); }else{ echo str_replace("-","/",$clean->purify($article->create_date));};?>" />
<a href="javascript:void(0);" onClick="wrtCalendar(event,frm.create_date); return false;"><img class="calendar" src="<?php echo base_url();?>img/ico_cal.jpg" alt="カレンダー" /></a>
</td>
</tr>
<tr>
<th class="hissu">タイトル</th>
<td><input type="text" name="title" id="title" size="80" value="<?php echo $clean->purify(@$article->title);?>" /></td>
</tr>

<tr>
<th class="hissu">カテゴリー</th>
<td>
<select name="category">
<option value="1" <?php if(@$article->category==1):?>selected<?php endif;?>>お知らせ</option>
<option value="2" <?php if(@$article->category==2):?>selected<?php endif;?>>イベント</option>
</select>
</td>
</tr>

<tr>
<th class="hissu">本文</th>
<td>
<div id="wrapper">
<textarea class="ckeditor" style="width:20px" cols="10" id="ckeditor" name="content" rows="3"><?php echo $clean->purify(@$article->content);?></textarea>
</div>
<script>
CKEDITOR.replace( 'ckeditor', {
	extraPlugins: 'tableresize'
});
</script>
</td>
</tr>

</table>
<input name="category_id" type="hidden" id="category_id"  value="1"/>
</div>
</div>

<div class="toukou noLine clearfix">
<p class="back"><a href="<?php echo base_url();?>article/home">&lt;&lt;新着情報一覧へ戻る</a></p>
<p class="b" style="text-align:center">
<input type="hidden" name="id" value="<?php echo $clean->purify(@$article->id);?>" />
<input type="submit" value="内容を確認する" style="width: 150px; height: 30px;font-weight:bold;" onclick="viewport();" />
</div>
</div>
</form>
