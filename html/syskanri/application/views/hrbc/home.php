<script type="text/javascript" src="<?php echo base_url();?>js/loading.js" charset="utf8"></script>
<script src="<?php echo base_url();?>js/viewport.js" type="text/javascript" charset="utf8"></script>

<script type="text/javascript">
function disp(){
	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('更新処理を実行します')){
		viewport();
		return true;
	}else{
		return false;
	}
}
</script>
<div class="title clearfix">
<div class="midasi">
<p class="tit">HRBC管理 </p>
</div>
<div class="sibori">
<p class="page">
</p>
</div>
</div>
<div class="toukou">
<div class="naiyo">
<div style="color:#FF0000;margin-top:10px;margin-bottom:10px;color:#FF0000;"><?php echo $clean->purify($msg);?></div>

<h3 style="font-size:22px;font-weight:bold;">Job管理</h3>
<br />
<p>
求人検索のデータとマスターの管理になります。API負荷対策のため、対象のデータに更新があった場合のみ実行してください。
</p>
<br />
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<th style="width:20%;text-align:left">求人(Job)管理<br />（全データ）</th>
<td>
<?php echo form_open('hrbc/job', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '全データを更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
<td>

<?php echo form_open('hrbc/jobclear', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, 'クリアする', array('onclick'=>"return confirm('クリアを実施します');"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
</tr>
<tr>
<th style="width:8%;text-align:left">企業（Client)管理<br />（全データ）</th>
<td>
<?php echo form_open('hrbc/client', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '全データを更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
<td>

<?php echo form_open('hrbc/clientclear', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, 'クリアする', array('onclick'=>"return confirm('クリアを実施します')"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
</tr>

<tr>
<th style="width:8%;text-align:left">企業担当（Recruiter)管理<br />（全データ）</th>
<td>
<?php echo form_open('hrbc/recruiter', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '全データを更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
<td>

<?php echo form_open('hrbc/recruiterclear', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, 'クリアする', array('onclick'=>"return confirm('クリアを実施します')"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
</tr>



<?php //$ip = $_SERVER["REMOTE_ADDR"];?>
<?php //if($ip=="153.156.8.56"):?>
<tr>
<th style="width:20%;text-align:left">求人(Job)管理</th>
<td>
<?php echo form_open('hrbc/joblimit', array('name'=>'frm', 'method'=>'post'));?>
抽出期間 <input type="text" name="start" value="<?php echo date('Y/m/d',strtotime("-1 day"));?>"  />～<input type="text" name="end" value="<?php echo date('Y/m/d');?>" />
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();", 'class'=>"submit_button"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
<br />※2016/04/20の形式で入力してください。
</td>
<td>

<?php echo form_open('hrbc/jobclear', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, 'クリアする', array('onclick'=>"return confirm('クリアを実施します');"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
</tr>
<tr>
<th style="width:8%;text-align:left">企業（Client)管理</th>
<td>
<?php echo form_open('hrbc/clientlimit', array('name'=>'frm', 'method'=>'post'));?>
抽出期間 <input type="text" name="start" value="<?php echo date('Y/m/d',strtotime("-1 day"));?>"  />～<input type="text" name="end" value="<?php echo date('Y/m/d');?>" />
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();", 'class'=>"submit_button"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
<br />※2016/04/20の形式で入力してください。
</td>
<td>

<?php echo form_open('hrbc/clientclear', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, 'クリアする', array('onclick'=>"return confirm('クリアを実施します')"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
</tr>


<tr>
<th style="width:8%;text-align:left">企業担当（Recruiter)管理</th>
<td>
<?php echo form_open('hrbc/recruiterlimit', array('name'=>'frm', 'method'=>'post'));?>
抽出期間 <input type="text" name="start" value="<?php echo date('Y/m/d',strtotime("-1 day"));?>"  />～<input type="text" name="end" value="<?php echo date('Y/m/d');?>" />
<?php echo form_submit(NULL, '更新する', array('class'=>"submit_button", 'onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
<br />※2016/04/20の形式で入力してください。
</td>
<td>

<?php echo form_open('hrbc/recruiterclear', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, 'クリアする', array('onclick'=>"return confirm('クリアを実施します')"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
</tr>
<?php //endif;?>





<tr>
<th style="width:8%;text-align:left">業種マスター管理</th>
<td colspan="2">
<?php echo form_open('hrbc/industry', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>

</tr>
<tr>
<th style="width:8%;text-align:left">職種マスター管理</th>
<td colspan="2">
<?php echo form_open('hrbc/jobcategory', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
</tr>
<tr>
<th style="width:8%;text-align:left">雇用形態マスター管理</th>
<td colspan="2">
<?php echo form_open('hrbc/employ', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>

</tr>
<tr>
<th style="width:8%;text-align:left">勤務地マスター管理</th>
<td colspan="2">
<?php echo form_open('hrbc/area', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '更新する', array('onclick'=>"disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>

</tr>
</table>
<br />

<p style="color:#FF0000">
※
求人(Job)管理と企業（Client)管理は、複数人による重複更新と負荷回避のため同時に実行した場合はロックがかかります。<br />
「更新する」で処理中（ロック状態）になった場合は、「クリアする」をクリックして再度実行してください。<br />
他のユーザが実行中の可能性もあるので、必ず確認してから更新して下さい。
</p>
<br /><br />

<h3 style="font-size:22px;font-weight:bold;">ENTRY管理</h3>
<br />
<p>
エントリーフォームのマスターの管理になります。API負荷対策のため、対象のデータに更新があった場合のみ実行してください。
</p>
<br />
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<th style="width:20%;text-align:left">都道府県マスター管理</th>
<td colspan="2">
<?php echo form_open('hrbc/prefecture', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
<tr>
<th style="width:8%;text-align:left">学歴マスター管理</th>
<td colspan="2">
<?php echo form_open('hrbc/background', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>

</tr>
</tr>
<tr>
<th style="width:8%;text-align:left">性別マスター管理</th>
<td colspan="2">
<?php echo form_open('hrbc/gender', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>

</tr>
<tr>
<th style="width:8%;text-align:left">就業状況マスター管理</th>
<td colspan="2">
<?php echo form_open('hrbc/work', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
</tr>

<tr>
<th style="width:8%;text-align:left">転勤マスター管理</th>
<td colspan="2">
<?php echo form_open('hrbc/tenkin', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
</tr>

<tr>
<th style="width:8%;text-align:left">年収マスター管理</th>
<td colspan="2">
<?php echo form_open('hrbc/income', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
</tr>

<tr>
<th style="width:8%;text-align:left">職種　マイページCD用マスター管理</th>
<td colspan="2">
<?php echo form_open('hrbc/mypagejob', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
</tr>

</table>
<br />

<h3 style="font-size:22px;font-weight:bold;">マイページＪＯＢ担当マスター管理</h3>
<br />
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<th style="width:20%;text-align:left">マイページＪＯＢ担当マスター管理</th>
<td colspan="2">
<?php echo form_open('hrbc/mypageowner', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
</tr>
</table>
<br />


<h3 style="font-size:22px;font-weight:bold;">マイページメンテナンス管理</h3>
<br />
<p>
HRBCのメンテナンス期間を入力してください。
</p>
<br />
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<th style="width:20%;text-align:left">メンテナンス管理</th>
<td colspan="2">
<?php echo form_open('hrbc/maintenance', array('name'=>'frm', 'method'=>'post'));?>
メンテナンス期間 <input type="text" class="datetimepicker" name="start" value="<?php echo htmlspecialchars(str_replace("-","/",$maintenance->start), ENT_QUOTES, 'UTF-8');?>"  />～<input type="text" name="end" value="<?php echo htmlspecialchars(str_replace("-","/",$maintenance->end), ENT_QUOTES, 'UTF-8');?>" />
<?php echo form_submit(NULL, '更新する', array('onclick'=>"return disp();"));?>
<?php echo form_hidden("id",1);?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
<br />※2016/04/20 12:10:00の形式で入力してください。
</td>
<tr>
</table>
<br />


<h3 style="font-size:22px;font-weight:bold;">メールアドレス（マイページ未登録）管理</h3>
<br />
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<th style="width:20%;text-align:left">メールアドレス（マイページ未登録）管理</th>
<td colspan="2">
<?php echo form_open('hrbc/email', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '確認する');?>
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
<tr>
</table>
<br />


</div>

<style>
input.submit_button {
	background-color:#00942e;
	border:1px solid #2da547;
	color:#fff;
	text-align:center;
}
</style>