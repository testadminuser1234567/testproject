<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);
$job_ary=array();
if(!empty($tmpjob['Item'])){
	foreach($tmpjob['Item'] as $k=>$v){
		if(!empty($v['Item'])){
			foreach($v['Item'] as $k2=>$v2){
				$job_ary[] = $v2;
			}
		}
	}
	foreach ((array) $job_ary as $key => $value) {
		$sort[$key] = $value["Option.P_Order"];
	}
}
array_multisort($sort, SORT_ASC, $job_ary);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);
$tantou_job_ary=array();
if(!empty($tmptantoujob['Item'])){
	foreach($tmptantoujob['Item'] as $k=>$v){
		if(!empty($v['Item'])){
			foreach($v['Item'] as $k2=>$v2){
				$tantou_job_ary[] = $v2;
			}
		}
	}
	foreach ((array) $tantou_job_ary as $key => $value) {
		$tantou_sort[$key] = $value["Option.P_Order"];
	}
}
array_multisort($tantou_sort, SORT_ASC, $tantou_job_ary);

?>
<div class="title clearfix">
<div class="midasi">
<p class="tit">HRBCメールアドレス一覧 </p>
<p>
<a href="<?php echo base_url();?>hrbc/home">HRBC管理画面へ戻る</a>
</p>
</div>
<div class="sibori">
<p class="page">
<?php echo $this->pagination->create_links(); ?>
</p>
</div>
</div>
<div class="toukou">
<div class="naiyo">
<div>
<form method="post" action="<?php echo base_url();?>hrbc/csvdownload"><input type="submit" name="submit" value="CSVダウンロード" style="width:150px;height:30px;margin-left:10px"></form>
</div>
<div>
<form method="post" action="<?php echo base_url();?>hrbc/email" style="margin-top:20px;">
IDで検索
<input type="text" name="search" placeholder="　Resume ID" style="width:150px;height:30px;margin-left:10px">
<input type="submit" name="submit" value="検索する" style="width:150px;height:30px;margin-left:10px">
</form>

</div>
<p style="color:#FF0000;margin-top:10px;margin-bottom:10px"><?php echo $clean->purify($msg);?></p>
<table border="0" cellspacing="0" cellpadding="0" style="width:100%">
<tr>
<th style="width:1%">ID（CPI)</th>
<th style="width:1%">Resume ID</th>
<th style="width:1%">Person ID</th>
<th style="width:2%">メールアドレス</th>
<th style="width:1%">ステータス</th>
<th style="width:10%">希望職種</th>
<th style="width:3%">更新日</th>
<th style="width:3%">削除</th>
</tr>
<?php foreach ($contents->result()  as $val): ?>
<?php $iv = base64_decode($val->iv);?>
<?php $CryptTarget= base64_decode($val->email);?>
<?php $DecryptTarget_ = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, mailSalt, $CryptTarget, MCRYPT_MODE_CBC, $iv);?>
<?php
$mail = $clean->purify($DecryptTarget_);
if (preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/iD', $mail)):
?>
<tr>
<td><?php echo $clean->purify($val->id);?></td>
<td><?php echo $clean->purify($val->hrbc_resume_id);?></td>
<td><?php echo $clean->purify($val->hrbc_person_id);?></td>
<td style="text-align:center">
<?php echo $clean->purify($DecryptTarget_);?>
</td>
<td>
<?php if($val->status==1):?>
メール認証離脱
<?php elseif($val->status==2):?>
STEP1離脱
<?php elseif($val->status==3):?>
登録完了
<?php endif;?>
</td>
<td>
希望職種：
<?php
$kibou_jobs=explode(',',@$val->kibou_job);
?>
                    <?php if(!empty($job_ary)):?>
                        <?php foreach($job_ary as $k=>$v):?>
                                                <?php if(!empty(@$kibou_jobs)):?><?php foreach(@$kibou_jobs as $jk=>$jv):?><?php if($jv==$v["Option.P_Id"]):?><?php echo htmlspecialchars($v['Option.P_Name'],ENT_QUOTES,'UTF-8');?>　<?php endif;?><?php endforeach;?><?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
<br />


希望勤務地１：
<?php $tflg=0;?>
                    <?php if(!empty($tmparea['Item'])):?>
                        <?php foreach($tmparea['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                        <?php if(!empty($v2["Items"]['Item'])):?>
                                             <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
<?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                 <?php if(@$val->kibou_area1==$v3['Option.P_Id']):?><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>

<?php else:?>
<?php if($tflg==0):?>
                                                 <?php if(@$val->kibou_area1==$v2['Items']['Item']['Option.P_Id']):?><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
<?php $tflg=1;?>
<?php endif;?>
<?php endif;?>
                                             <?php endforeach;?>
                                        <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
<br />
希望勤務地２：
<?php $tflg=0;?>
                    <?php if(!empty($tmparea['Item'])):?>
                        <?php foreach($tmparea['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                        <?php if(!empty($v2["Items"]['Item'])):?>
                                             <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
<?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                 <?php if(@$val->kibou_area2==$v3['Option.P_Id']):?><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>

<?php else:?>
<?php if($tflg==0):?>
                                                 <?php if(@$val->kibou_area2==$v2['Items']['Item']['Option.P_Id']):?><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
<?php $tflg=1;?>
<?php endif;?>
<?php endif;?>
                                             <?php endforeach;?>
                                        <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
<br />
希望勤務地３：
<?php $tflg=0;?>
                    <?php if(!empty($tmparea['Item'])):?>
                        <?php foreach($tmparea['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                        <?php if(!empty($v2["Items"]['Item'])):?>
                                             <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
<?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                 <?php if(@$val->kibou_area3==$v3['Option.P_Id']):?><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>

<?php else:?>
<?php if($tflg==0):?>
                                                 <?php if(@$val->kibou_area3==$v2['Items']['Item']['Option.P_Id']):?><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
<?php $tflg=1;?>
<?php endif;?>
<?php endif;?>
                                             <?php endforeach;?>
                                        <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
<br />
希望年収：
                    <?php if(!empty($tmpincome['Item'])):?>
                        <?php foreach($tmpincome['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                        <?php if($v2["Option.P_Id"]==@$val->income):?><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
<br />
転勤：
<?php
$kibou_tenkins=explode(',',@$val->tenkin);
?>
                    <?php if(!empty($tmptenkin['Item'])):?>
                        <?php foreach($tmptenkin['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                        <?php if(!empty($kibou_tenkins)):?><?php if(in_array($v2['Option.P_Name'], @$kibou_tenkins, true)):?><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?><?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
</td>

<td><?php echo str_replace('-','/',$clean->purify($val->updated));?></td>

<td>

<?php echo form_open('hrbc/maildel', array('name'=>'frm', 'method'=>'post'));?>
<?php echo form_submit(NULL, '削除する', array('onclick'=>"return confirm('削除してよろしいですか？')"));?>
<input type="hidden" name="id" value="<?php echo $clean->purify($val->id);?>" />
<?php echo form_hidden($csrf); ?>
<?php echo form_close();?>
</td>
</tr>
<?php endif;?>
<?php endforeach; ?>
</table>
</div>
