<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js" charset="utf8"></script>
<script src="<?php echo base_url();?>js/HolidayChk.js" type="text/javascript" charset="utf8"></script>
<script src="<?php echo base_url();?>js/calendar.js" type="text/javascript" charset="utf8"></script>
<script src="<?php echo base_url();?>js/chkListsubwin.js" type="text/javascript" charset="utf8"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/loading.js" charset="utf8"></script>
<script type="text/javascript" src="<?php echo base_url();?>ckeditor/ckeditor.js" charset="utf8"></script>
<div class="title clearfix">
<p class="tit2">企業情報を投稿する</p>
<div class="naiyoToko">
<br/>
<p class="txt">▼下記の項目をご記入下さい。</p>
<div class="error"><?php echo $clean->purify($msg); ?></div>
<form name="frm" action="<?php echo base_url();?>company/confirm" method="post" enctype="multipart/form-data">
<table border="0" cellspacing="0" cellpadding="0">

<tr>
<th class="hissu">内容</th>
<td>
<div id="wrapper">
<textarea class="ckeditor" style="width:20px" cols="10" id="ckeditor" name="content" rows="3"><?php echo $clean->purify(@$company->content);?></textarea>
</div>
<script>
CKEDITOR.replace( 'ckeditor', {
	extraPlugins: 'tableresize'
});
</script>
</td>
</tr>

</table>
<input name="category_id" type="hidden" id="category_id"  value="1"/>
</div>
</div>

<div class="toukou noLine clearfix">
<p class="b" style="text-align:center">
<input type="hidden" name="id" value="<?php echo $clean->purify(@$company->id);?>" />
<input type="submit" value="内容を確認する" style="width: 150px; height: 30px;font-weight:bold;" onclick="viewport();" />
</div>
</div>
</form>
