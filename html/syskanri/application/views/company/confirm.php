<script type="text/javascript" src="<?php echo base_url();?>js/jquery.js" charset="utf8"></script>
<script type="text/javascript" src="<?php echo base_url();?>js/loading.js" charset="utf8"></script>
<script src="<?php echo base_url();?>js/viewport.js" type="text/javascript" charset="utf8"></script>
<div class="title clearfix">
<p class="tit2">企業情報を投稿する</p>
<div class="naiyoToko">
<br/>
<p class="txt">▼下記の項目をご確認下さい。</p>
<table border="0" cellspacing="0" cellpadding="0">

<tr>
<th>内容</th>
<td><?php echo $clean->purify(@$company->content);?></td>
</tr>

</table>


</div>
</div>

<table style="margin:0 auto;margin-top:10px">
<tr>
<td>
<form name="frm" action="<?php echo base_url();?>company/home" method="post" enctype="multipart/form-data">
<input type="hidden" name="mode" value="return" />
<input type="submit" value="内容を修正する" style="width: 150px; height: 30px;font-weight:bold;float:right" />
<input type="hidden" name="id" value="<?php echo $clean->purify(@$company->id);?>" />
<input type="hidden" name="content" value='<?php echo $clean->purify($company->content);?>' />
</form>
</td>
<td>
<form name="frm" action="<?php echo base_url();?>company/add" method="post" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $clean->purify(@$company->id);?>" />
<input type="hidden" name="content" value='<?php echo $clean->purify($company->content);?>' />
<?php echo $clean->purify(form_hidden($csrf)); ?>
<input type="submit" value="登録する" style="width: 150px; height: 30px; margin-left:10px; font-weight:bold;" />
</form>
</td>
</tr>
</table>
<br/>
</div>