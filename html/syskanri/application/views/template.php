<?php 
header("Content-type: text/html; charset=utf-8");
header("X-Content-Type-Options: nosniff");
header("X-FRAME-OPTIONS: SAMEORIGIN");
header("X-FRAME-OPTIONS: DENY");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="robots" content="noindex,nofollow">
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css" />
<title>更新システム</title>
</head>
<body>
<div id="header_system">
<p class="title">HUREX更新システム</p>
<?php if(!preg_match('/auth\/login/',$_SERVER["REQUEST_URI"]) && !preg_match('/auth\/logout/',$_SERVER["REQUEST_URI"])) :?>
<p class="btn" style="float:right">
<form method="post" action="<?php echo base_url();;?>auth/logout">
<input type="submit" name="btn" value="ログアウト" style="margin-top:10px;float:right;width: 100px; height: 30px;margin-right:15px;font-weight:bold" >
</form>
<input type="button" value="WEBを見る" style="margin-top:10px;float:right;width: 100px; height: 30px;margin-right:10px;font-weight:bold" onClick="window.open('/', '_blank')" />
</p>
<?php endif;?>
</div>
<?php if(!preg_match('/auth\/login/',$_SERVER["REQUEST_URI"]) && !preg_match('/auth\/logout/',$_SERVER["REQUEST_URI"]) && !preg_match('/auth\/forget_password/',$_SERVER["REQUEST_URI"]) && !preg_match('/auth\/create_password/',$_SERVER["REQUEST_URI"]) && !preg_match('/auth\/send_mail/',$_SERVER["REQUEST_URI"]) && !preg_match('/auth\/new_password/',$_SERVER["REQUEST_URI"])) :?>
<ul id="navi" class="clearfix">
<?php if(Kanri=="on"):?>
<li><a href="<?php echo site_url("auth/home");?>" <?php if(preg_match('/auth/',$_SERVER["REQUEST_URI"])) echo 'class="on"';?>>ユーザ管理</a></li>
<?php endif;?>
<li><a href="<?php echo site_url("article/home");?>" <?php if(preg_match('/article/',$_SERVER["REQUEST_URI"])) echo 'class="on"';?>>新着情報管理</a></li>
<li><a href="<?php echo site_url("company/home");?>" <?php if(preg_match('/company/',$_SERVER["REQUEST_URI"])) echo 'class="on"';?>>企業情報管理</a></li>
</ul>
<?php endif;?>
<div id="contents">
<?php $this->load->view($render);?>
</div>
</body>
</html>