<?php

session_cache_limiter('private_no_expire');
session_start();

include (dirname(__FILE__) . "/system/template/template.php");
include (dirname(__FILE__) . "/system/validation.php");
//クラス読み込み
$template = new template();
$validation = new Validation();

//セッションにデータを格納
setSession($_POST);

//セッションデータをname属性の名前に設定（ボディで使用するため
include (dirname(__FILE__) . "/system/set_session.php");

//コンフィグ読込
include (dirname(__FILE__) . "/config.php");

//送信
if($_POST["mode"] == "send"){
	include (dirname(__FILE__) . "/system/send.php");
//戻る
}else if($_POST["mode"] == "return"){
	$template->view(Index, $_POST, $err);
//確認処理
}else if($_POST["mode"] == "confirm"){
	//セッションにデータを格納
	setSession($posts);

	//エラーチェック
	$err = "";
	$err = $validation->run();

	if(empty($_POST["tel1"]) && empty($_POST['tel2'])){
		$err .= "電話は自宅もしくは携帯のどちらかは必ず御入力下さい。<br>";
	}
	if(empty($_POST["mail1"]) && empty($_POST['mail2'])){
		$err .= "メールは自宅もしくは携帯のどちらかは必ず御入力下さい。<br>";
	}



	//エラーがあったら初期画面へ
	if(!empty($err)){
		$template->view(Index, $_SESSION, $err);
	}else{
		//レジュメのアップロード
		include('system/fileuploads.php');
		$uploads = new Uploads();

		$upload_error="";

/*
		if(!empty($_POST['btnKakunin'])){
			//アップロード
			if(empty($_FILES['file1']['tmp_name']) && empty($_POST['tmp_file1'])){
				$upload_error .= "履歴書が選択されていません。<br/>";
			}
			if(empty($_FILES['file2']['tmp_name']) && empty($_POST['tmp_file2'])){
				$upload_error .= "職務経歴書が選択されていません。<br/>";
			}
		}
*/
/*
		if(empty($_FILES['file3']['tmp_name']) && empty($_POST['tmp_file3'])){
			$upload_error .= "職務経歴書が選択されていません。<br/>";
		}
*/

		if(empty($upload_error)){
			$img_config['upload_path'] = $_SERVER["DOCUMENT_ROOT"] . "/../ericsson/";
			$img_config['allowed_types'] = 'doc|xls|ppt|pptx|pdf|docx|xlsx|html|txt|htm';
			$img_config['max_size']	= '1000000';	
			$uploads->initialize($img_config);

			if(!empty($_FILES['file1']['tmp_name'])){
				if(!$uploads->do_upload('file1')){
					$tmp = array('error' => $uploads->display_errors());
					$upload_error .= $tmp["error"];
				}
				$tmpdata1 = array('file1' => $uploads->data());
			}

			if(!empty($_FILES['file2']['tmp_name'])){
				if(!$uploads->do_upload('file2')){
					$tmp = array('error' => $uploads->display_errors());
					$upload_error .= $tmp["error"];
				}
				$tmpdata2 = array('file2' => $uploads->data());
			}

			if(!empty($_FILES['file3']['tmp_name'])){
				if(!$uploads->do_upload('file3')){
					$tmp = array('error' => $uploads->display_errors());
					$upload_error .= $tmp["error"];
				}
				$tmpdata3 = array('file3' => $uploads->data());
			}

			if(!empty($_FILES['file4']['tmp_name'])){
				if(!$uploads->do_upload('file4')){
					$tmp = array('error' => $uploads->display_errors());
					$upload_error .= $tmp["error"];
				}
				$tmpdata4 = array('file4' => $uploads->data());
			}

			if(empty($upload_error)){
				//ファイル名の退避
				if(!empty($_FILES['file1']['tmp_name'])){
					$_SESSION['file1'] = $tmpdata1['file1']['file_name'];
				}else{
					$_SESSION['file1'] = $_POST['tmp_file1'];
				}
				if(!empty($_FILES['file2']['tmp_name'])){
					$_SESSION['file2'] = $tmpdata2['file2']['file_name'];
				}else{
					$_SESSION['file2'] = $_POST['tmp_file2'];
				}
				if(!empty($_FILES['file3']['tmp_name'])){
					$_SESSION['file3'] = $tmpdata3['file3']['file_name'];
				}else{
					$_SESSION['file3'] = $_POST['tmp_file3'];
				}
				if(!empty($_FILES['file4']['tmp_name'])){
					$_SESSION['file4'] = $tmpdata4['file4']['file_name'];
				}else{
					$_SESSION['file4'] = $_POST['tmp_file4'];
				}
				$template->view(Confirm, $_SESSION, "");
			}else{
				$template->view(Index, $_SESSION, $upload_error);
			}
		}else{
			$template->view(Index, $_SESSION, $upload_error);
		}
	}
//その他
}else{
	$data="";
	$err="";
	header("HTTP/1.1 200 ok");
	$url = Index;
	header("Location: $url");
	exit();
}

/*
/  セッションにデータ格納
*/
function setSession($posts){
	foreach($posts as $key => $val){
		unset($_SESSION[$key]);
		$_SESSION[$key] = $posts[$key];
	}
}
?>