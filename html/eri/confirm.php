<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="index,follow" />
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形" />
<meta name="description" content="東北地方の求人多数！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。" />
<meta http-equiv="imagetoolbar" content="no" />
<meta http-equiv="Content-Language" content="ja" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="JavaScript" />
<link rel="shortcut icon" href="/favicon.ico" />
<link href="../css/import.css" rel="stylesheet" type="text/css" media="all" />
<link href="form.css" rel="stylesheet" type="text/css" media="all" />
<link rel="canonical" href="https://www.hurex.jp/entry/form.html">
<title>ERICSSON｜東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<meta name="copyright" content="" />
<script type="text/javascript" src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<script type="text/javascript" src="../js/rollover.js"></script>
<!--[if lte IE 6]>   
<script type="text/javascript" src="../js/DD_belatedPNG_0.0.8a.js">   
</script>   
<script type="text/javascript">  /* EXAMPLE */  DD_belatedPNG.fix('img');</script>   
<![endif]-->
<script type="text/javascript" src="./form_validation.js"></script>
<script>
$(function(){
	$('.txtMoreBtn').click(function() {
		$(this).next().slideToggle();
	}).next().hide();
});
</script>
<!-- Facebook Conversion Code for ヒューレックス　リマーケ -->
<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', '6022076371230', {'value':'0.00','currency':'JPY'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none"
src="https://www.facebook.com/tr?ev=6022076371230&amp;cd[value]=0.00&amp;cd[
currency]=JPY&amp;noscript=1"
/></noscript>
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<!-- formPage start -->
<div id="formPage">
<!-- page start -->
<div class="page new" id="entryPage">
    <!-- -->
    <div id="readBox">
        <h1><img src="img/logo.png" width="73" height="65" alt="ERICSSON"/></h1>
        <h2 class="center">入力内容確認</h2>
    </div>
    <!-- -->
    <p><img src="../entry/img/step_02.png" alt=""/></p>
    <p>以下の内容で送信致します。</p>
    <?php require($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/site/setmaster.php');?>
    <?php require_once("system/esc.php");?>
    <table width="720">
        <tr>
            <th class="<?php if($shimei):?>ok<?php else:?>hissu<?php endif;?> ">氏名</th>
            <td><?php echo esc($shimei);?></td>
        </tr>
        <tr>
            <th class="<?php if($kana):?>ok<?php else:?>hissu<?php endif;?>">ふりがな</th>
            <td><?php echo esc($kana);?></td>
        </tr>
        <tr>
            <th class="<?php if($year && $month && $day):?>ok<?php else:?>hissu<?php endif;?>">生年月日</th>
            <td><?php echo esc($year);?>年<?php echo esc($month);?>月<?php echo esc($day);?>日 </td>
        </tr>
        <tr>
            <th class="<?php if($sex):?>ok<?php else:?>hissu<?php endif;?>">性別</th>
            <td><?php echo esc($sex);?></td>
        </tr>
        <tr>
            <th class="<?php if($pref):?>ok<?php else:?>hissu<?php endif;?>">住所</th>
            <td><?php echo esc($pref);?></td>
        </tr>
        <tr>
            <th class="<?php if($tel1):?>ok<?php else:?>hissu<?php endif;?>">電話</th>
            <td><?php echo esc($tel1);?></td>
        </tr>
        <tr>
            <th class="<?php if($mail1):?>ok<?php else:?>hissu<?php endif;?>">メール</th>
            <td><?php echo esc($mail1);?></td>
        </tr>
        <?php if(!empty(esc($school_div_id))):?>
        <tr>
            <th class="<?php if($school_div_id):?>ok<?php else:?>hissu<?php endif;?>">最終学歴</th>
            <td><?php echo esc($school_div_id);?></td>
        </tr>
        <?php endif;?>
        <?php if(!empty(esc($company_number)) || esc($company_number) == 0):?>
        <tr>
            <th class="<?php if($company_number):?>ok<?php else:?>hissu<?php endif;?>">経験社数</th>
            <td><?php if(esc($company_number)==5):?>
                5社以上
                <?php else:?>
                <?php echo esc($company_number);?>社
                <?php endif;?></td>
        </tr>
        <?php endif;?>
        <?php if(!empty(esc($qualification))):?>
        <tr>
            <th class="<?php if($qualification):?>ok<?php else:?>nini<?php endif;?>">免許･資格</th>
            <td><?php echo nl2br(esc($qualification));?></td>
        </tr>
        <?php endif;?>
        <tr>
            <th class="no">履歴書</th>
            <td><?php if($file1):?>
                選択されているファイル： <?php echo esc($file1);?>
                <?php endif;?></td>
        </tr>
        <tr>
            <th class="no ">職務経歴書</th>
            <td><?php if($file2):?>
        選択されているファイル： <?php echo esc($file2);?>
    <?php endif;?></td>
        </tr>
        <tr>
    <th valign="top" class="no">職務経歴記載欄</th>
    <td>
    <!-- keirekiBox start -->
    <div class="keirekiBox">
    <?php if(!empty(company1) || !empty($year1_from) || !empty($month1_from) || !empty($year1_to) || !empty($month1_to) || !empty($naiyo1)):?>
    <h3>直近1社目</h3>
    <table class="cnf">
        <tbody>
            <tr>
                <th>会社名</th>
                <td><?php echo esc($company1);?></td>
                </tr>
            <tr>
                <th>在職期間</th>
                <td><?php echo esc($year1_from);?>年<?php echo esc($month1_from);?>月
                    ～
                    <?php echo esc($year1_to);?>年<?php echo esc($month1_to);?>月
                    </td>
                </tr>
            <tr>
                <th>職務内容</th>
                <td><?php echo nl2br(esc($naiyo1));?></td>
                </tr>
            </tbody>
    </table>
    </div>
    <?php endif;?>
    <!-- keirekiBox end -->
    <!-- keirekiBox start -->
    <?php if(!empty(company2) || !empty($year2_from) || !empty($month2_from) || !empty($year2_to) || !empty($month2_to) || !empty($naiyo2)):?>
    <div class="keirekiBox">
        <h3>直近2社目</h3>
    <table class="cnf">
        <tbody>
            <tr>
                <th>会社名</th>
                <td><?php echo esc($company2);?></td>
                </tr>
            <tr>
                <th>在職期間</th>
                <td><?php echo esc($year2_from);?>年<?php echo esc($month2_from);?>月
                    ～
                    <?php echo esc($year2_to);?>年<?php echo esc($month2_to);?>月
                    </td>
                </tr>
            <tr>
                <th>職務内容</th>
                <td><?php echo nl2br(esc($naiyo2));?></td>
                </tr>
            </tbody>
    </table>
    </div>
    <?php endif;?>
    <!-- keirekiBox end -->
    <!-- keirekiBox start -->
    <?php if(!empty(company3) || !empty($year3_from) || !empty($month3_from) || !empty($year3_to) || !empty($month3_to) || !empty($naiyo3)):?>
    <div class="keirekiBox">
        <h3>直近3社目</h3>
    <table class="cnf">
        <tbody>
            <tr>
                <th>会社名</th>
                <td><?php echo esc($company3);?></td>
                </tr>
            <tr>
                <th>在職期間</th>
                <td><?php echo esc($year3_from);?>年<?php echo esc($month3_from);?>月
                    ～
                    <?php echo esc($year3_to);?>年<?php echo esc($month3_to);?>月
                    </td>
                </tr>
            <tr>
                <th>職務内容</th>
                <td><?php echo nl2br(esc($naiyo3));?></td>
                </tr>
            </tbody>
    </table>
    </div>
    <?php endif;?>
    <!-- keirekiBox end -->
    </td>
</tr>
</table>


    <!-- start -->
    <div class="btnSend">
        <form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data">
            <input name="btnSend" type="image" class="image" id="btnSend" value="送信する" onclick="window.onbeforeunload=null;" src="../entry/img/btn_send.png" alt="上記の内容で送信" width="502" height="63" />
            <input type="hidden" name="mode" value="send" />
            <input type="hidden" name="shimei" value="<?php echo esc($shimei);?>" />
            <input type="hidden" name="kana" value="<?php echo esc($kana);?>" />
            <input type="hidden" name="year" value="<?php echo esc($year);?>" />
            <input type="hidden" name="month" value="<?php echo esc($month);?>" />
            <input type="hidden" name="day" value="<?php echo esc($day);?>" />
            <input type="hidden" name="sex" value="<?php echo esc($sex);?>" />
            <input type="hidden" name="zip" value="<?php echo esc($zip);?>" />
            <input type="hidden" name="pref" value="<?php echo esc($pref);?>" />
            <input type="hidden" name="tel1" value="<?php echo esc($tel1);?>" />
            <input type="hidden" name="mail1" value="<?php echo esc($mail1);?>" />
            <input type="hidden" name="file1" value="<?php echo esc($file1);?>" />
            <input type="hidden" name="file2" value="<?php echo esc($file2);?>" />
            <input type="hidden" name="agree" value="<?php echo esc($agree);?>" />
            <input type="hidden" name="school_div_id" value="<?php echo esc($school_div_id);?>" />
            <input type="hidden" name="company_number" value="<?php echo esc($company_number);?>" />
            <input type="hidden" name="company1" value="<?php echo esc($company1);?>" />
            <input type="hidden" name="year1_from" value="<?php echo esc($year1_from);?>" />
            <input type="hidden" name="month1_from" value="<?php echo esc($month1_from);?>" />
            <input type="hidden" name="year1_to" value="<?php echo esc($year1_to);?>" />
            <input type="hidden" name="month1_to" value="<?php echo esc($month1_to);?>" />
            <input type="hidden" name="naiyo1" value="<?php echo esc($naiyo1);?>" />
            <input type="hidden" name="company2" value="<?php echo esc($company2);?>" />
            <input type="hidden" name="year2_from" value="<?php echo esc($year2_from);?>" />
            <input type="hidden" name="month2_from" value="<?php echo esc($month2_from);?>" />
            <input type="hidden" name="year2_to" value="<?php echo esc($year2_to);?>" />
            <input type="hidden" name="month2_to" value="<?php echo esc($month2_to);?>" />
            <input type="hidden" name="naiyo2" value="<?php echo esc($naiyo2);?>" />
            <input type="hidden" name="company3" value="<?php echo esc($company3);?>" />
            <input type="hidden" name="year3_from" value="<?php echo esc($year3_from);?>" />
            <input type="hidden" name="month3_from" value="<?php echo esc($month3_from);?>" />
            <input type="hidden" name="year3_to" value="<?php echo esc($year3_to);?>" />
            <input type="hidden" name="month3_to" value="<?php echo esc($month3_to);?>" />
            <input type="hidden" name="naiyo3" value="<?php echo esc($naiyo3);?>" />
            <input type="hidden" name="qualification" value="<?php echo esc($qualification);?>" />
        </form>
    </div>
    <div class="aCenter">
        <form id="entryForm" action="mail.php" method="post" enctype="multipart/form-data">
            <input type="hidden" name="mode" value="return" />
            <input type="hidden" name="shimei" value="<?php echo esc($shimei);?>" />
            <input type="hidden" name="kana" value="<?php echo esc($kana);?>" />
            <input type="hidden" name="year" value="<?php echo esc($year);?>" />
            <input type="hidden" name="month" value="<?php echo esc($month);?>" />
            <input type="hidden" name="day" value="<?php echo esc($day);?>" />
            <input type="hidden" name="sex" value="<?php echo esc($sex);?>" />
            <input type="hidden" name="zip" value="<?php echo esc($zip);?>" />
            <input type="hidden" name="pref" value="<?php echo esc($pref);?>" />
            <input type="hidden" name="tel1" value="<?php echo esc($tel1);?>" />
            <input type="hidden" name="mail1" value="<?php echo esc($mail1);?>" />
            <input type="hidden" name="file1" value="<?php echo esc($file1);?>" />
            <input type="hidden" name="file2" value="<?php echo esc($file2);?>" />
            <input type="hidden" name="agree" value="<?php echo esc($agree);?>" />
            <input type="hidden" name="school_div_id" value="<?php echo esc($school_div_id);?>" />
            <input type="hidden" name="company_number" value="<?php echo esc($company_number);?>" />
            <input type="hidden" name="company1" value="<?php echo esc($company1);?>" />
            <input type="hidden" name="year1_from" value="<?php echo esc($year1_from);?>" />
            <input type="hidden" name="month1_from" value="<?php echo esc($month1_from);?>" />
            <input type="hidden" name="year1_to" value="<?php echo esc($year1_to);?>" />
            <input type="hidden" name="month1_to" value="<?php echo esc($month1_to);?>" />
            <input type="hidden" name="naiyo1" value="<?php echo esc($naiyo1);?>" />
            <input type="hidden" name="company2" value="<?php echo esc($company2);?>" />
            <input type="hidden" name="year2_from" value="<?php echo esc($year2_from);?>" />
            <input type="hidden" name="month2_from" value="<?php echo esc($month2_from);?>" />
            <input type="hidden" name="year2_to" value="<?php echo esc($year2_to);?>" />
            <input type="hidden" name="month2_to" value="<?php echo esc($month2_to);?>" />
            <input type="hidden" name="naiyo2" value="<?php echo esc($naiyo2);?>" />
            <input type="hidden" name="company3" value="<?php echo esc($company3);?>" />
            <input type="hidden" name="year3_from" value="<?php echo esc($year3_from);?>" />
            <input type="hidden" name="month3_from" value="<?php echo esc($month3_from);?>" />
            <input type="hidden" name="year3_to" value="<?php echo esc($year3_to);?>" />
            <input type="hidden" name="month3_to" value="<?php echo esc($month3_to);?>" />
            <input type="hidden" name="naiyo3" value="<?php echo esc($naiyo3);?>" />
            <input type="hidden" name="qualification" value="<?php echo esc($qualification);?>" />
            <input type="image" value="戻る" src="../entry/img/btn_back.png" width="210" height="36" alt="戻る" onclick="window.onbeforeunload=null;" style="border:none;padding:0px;"/>
        </form>
    </div>
    <!--
<p class="aCenter">
<a href="javascript:history.back();"><img src="../entry/img/btn_back.png" width="210" height="36" onclick="window.onbeforeunload=null;" alt="入力画面に戻って修正する"/></a>
</p>
-->
</div>
<!-- page end -->
</div>
<!-- formPage end -->
<!-- リマーケティング タグの Google コード -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 963710423;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/963710423/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<!-- リマーケティングタグ Yahoo -->
<script type="text/javascript">
  (function () {
    var tagjs = document.createElement("script");
    var s = document.getElementsByTagName("script")[0];
    tagjs.async = true;
    tagjs.src = "//s.yjtag.jp/tag.js#site=1Wjm96V";
    s.parentNode.insertBefore(tagjs, s);
  }());
</script>
<noscript>
<iframe src="//b.yjtag.jp/iframe?c=1Wjm96V" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
</noscript>
<script src="//platform.twitter.com/oct.js" type="text/javascript"></script> <script type="text/javascript"> twttr.conversion.trackPid('l5c0l');</script>
<noscript>
<img height="1" width="1" style="display:none;" alt=""
src="https://analytics.twitter.com/i/adsct?txn_id=l5c0l&p_id=Twitter" /> <img height="1" width="1" style="display:none;" alt=""
src="//t.co/i/adsct?txn_id=l5c0l&p_id=Twitter" /></noscript>
<script type="text/javascript" language="javascript">
/* <![CDATA[ */
var yahoo_retargeting_id = 'W5O5K72Z6Z';
var yahoo_retargeting_label = '';
/* ]]> */
</script>
<script type="text/javascript" language="javascript" src="//b92.yahoo.co.jp/js/s_retargeting.js"></script>
<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
<p id="copyForm">&copy; 2017 ERICSSON</p>
</body>
</html>