<?php
//入力ページの設定
define('Index', 'index.html');
//確認ページの設定（使わない場合はこのまま）
define('Confirm', 'confirm.php');
//完了ページの設定
define('Finish', 'finish.php');

define('ENCODE', 'UTF-8');

//ユーザの名前設定
$unit_name = $shimei . "様";
//ユーザの宛先設定
if(!empty($mail1)){
	$email = $mail1;
}else{
	$email = $mail2;
}
$today = Date('Y年m月d日');
$send_mail="touroku@hurex.co.jp";
//$send_mail="tosa@smt-net.co.jp";

$birthday = $year . "年" . $month . "月" . $day . "日";
/*
$zaiseki = $zaiseki_year_from . "年" . $zaiseki_month_from . "月～" . $zaiseki_year_to . "年" . $zaiseki_month_to . "月";
$shugyo = $shugyo_year_from."年" .$shugyo_month_from . "月～" . $shugyo_year_to . "年" . $shugyo_month_to . "月";
$shugyo2 = $shugyo_year_from2."年" .$shugyo_month_from2 . "月～" . $shugyo_year_to2 . "年" . $shugyo_month_to2 . "月";
$shugyo3 = $shugyo_year_from3."年" .$shugyo_month_from3 . "月～" . $shugyo_year_to3 . "年" . $shugyo_month_to3 . "月";
*/
/*
$job1_mail = $job["$job1"];
$job2_mail = $job["$job2"];
$job3_mail = $job["$job3"];
$biz1_mail = $biz["$biz1"];
$biz2_mail = $biz["$biz2"];
$biz3_mail = $biz["$biz3"];
$kibo_biz1_mail = $biz["$kibo_biz1"];
$kibo_biz2_mail = $biz["$kibo_biz2"];
$kibo_biz3_mail = $biz["$kibo_biz3"];
$kibo_job1_mail = $job["$kibo_job1"];
$kibo_job2_mail = $job["$kibo_job2"];
$kibo_job3_mail = $job["$kibo_job3"];
*/

$tmp_company_number=$company_number;
if($company_number==5){
	$tmp_company_number="5社以上";
}else{
	$tmp_company_number.="社";
}

//サンクスメール設定
$thanks_mail_subject = "「エリクソン仙台」へご応募いただき、誠にありがとうございます。";
$thanks_mail_from = "touroku@hurex.co.jp";
//$thanks_mail_from = "tosa@smt-net.co.jp";
$thanks_mail_str = "ヒューレックス株式会社";
$thanks_body= <<< EOF
$shimei 様

この度は、「エリクソン仙台」の求人へご応募いただき、誠にありがとうございます。
いただいた情報を確認の上、2営業日以内に採用事務局よりご連絡させていただきますので、しばらくお待ちいただければ幸いです。

尚、エリクソン・ジャパン株式会社は、今回の募集活動において採用事務局の業務を「ヒューレックス株式会社」に委託しています。
ご不明な点などがございましたら、以下の連絡先までお問い合わせください。


【採用事務局 連絡先】
ヒューレックス株式会社
〒980-6117 
仙台市青葉区中央1-3-1 アエル17階
Tel.022-723-1770　（受付：9:00～21:00）
e-mail info@hurex.co.jp

――――――――――――――――――――――――――――――――

このメール配信に心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいても回答できかねますので、ご了承ください。

EOF;

//subject用
$now = date("Ymd");
$birth = $year . sprintf('%02d', $month) . sprintf('%02d', $day);
$age = floor(($now-$birth)/10000);
$tmp_div="";
if($school_div_id == "大学院卒（博士）"){
	$tmp_div = "院卒博";
}else if($school_div_id =="大学院卒（修士）"){
	$tmp_div = "院卒修";
}else if($school_div_id =="大学卒"){
	$tmp_div = "大学卒";
}else if($school_div_id =="高専卒"){
	$tmp_div = "高専卒";
}else if($school_div_id =="短大卒"){
	$tmp_div = "短大卒";
}else if($school_div_id =="専門各種学校卒"){
	$tmp_div = "専門卒";
}else if($school_div_id =="その他"){
	$tmp_div = "その他";
}else if($school_div_id =="不問"){
}


//通知メール設定
$order_mail_subject = "【ERICSSON(PC)】 " . $age ."歳 " . $company_number . "社 " . $tmp_div . " " . $sex . " "  . $shimei   ;
$order_mail_to[] = array('touroku@hurex.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('tosa@smt-net.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('tosa@smt-net.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('info@smt-net.co.jp', '追加宛先');

$order_body= <<< EOF
ご担当者様
ホームページより、 $shimei 様のエントリーがありました。

[氏名]
$shimei

[ふりがな]
$kana

[生年月日]
$birthday

[性別]
$sex

[住所]
$pref

[電話番号]
$tel1

[メールアドレス]
$mail1

[最終学歴]
$school_div_id

[経験社数]
$tmp_company_number

[免許・資格]
$qualification

[職務経歴書]
【直近1社目】
会社名： $company1
在職期間： $year1_from 年 $month1_from 月 ～ $year1_to 年 $month1_to 月
職務内容：
$naiyo1

【直近2社目】
会社名： $company2
在職期間： $year2_from 年 $month2_from 月 ～ $year2_to 年 $month2_to 月
職務内容：
$naiyo2

【直近3社目】
会社名： $company3
在職期間： $year3_from 年 $month3_from 月 ～ $year3_to 年 $month3_to 月
職務内容：
$naiyo3

EOF;

//項目チェック用
$validation->set_rules('agree','個人情報保護方針','required');
$validation->set_rules('shimei','氏名','required|max_length[25]');
$validation->set_rules('kana','フリガナ','required|max_length[25]');
$validation->set_rules('year','年','required');
$validation->set_rules('month','月','required');
$validation->set_rules('day','日','required');
$validation->set_rules('sex','性別','required');
$validation->set_rules('pref','都道府県','required');
$validation->set_rules('tel1','電話1','max_length[15]');
$validation->set_rules('mail1','メール1','valid_email|max_length[50]');
$validation->set_rules('school_div_id','最終学歴','');
$validation->set_rules('company_number','経験社数','');



//ログファイル出力 on or off
$log = "off";

?>