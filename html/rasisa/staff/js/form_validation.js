//エラー処理用
var data = {};
data.shimei    = "氏名";
data.kana     = "フリガナ";
data.year    = "年";
data.month    = "月";
data.day    = "日";
data.sex    = "性別";
data.zip      = "郵便番号";
data.pref      = "都道府県";
data.address      = "住所";
data.tel1       = "電話1";
data.mail1      = "メール1";
data.mail2      = "メール2";

$(function(){
	var tmp = "";
	$("form").submit(function(){
		
		//エラーの初期化
		$("div.error").remove();
		$("td").removeClass("error");
		
		//テキスト、テキストエリアのチェック
		$(":text, textarea").filter(".required").each(function(){
			if($(this).val()==""){
				tmp = $(this).attr("id");
				$(this).parent().append("<div class='error'>" + data[tmp] + "を入力してください。</div>");
			}

			//数値のチェック
			$(this).filter(".number").each(function(){
				if(isNaN($(this).val())){
					$(this).parent().append("<div class='error'>数値のみ選択可能です。</div>");
				}
			});
		
			//メールアドレスチェック
			$(this).filter(".mail").each(function(){
				if($(this).val() && !$(this).val().match(/.+@.+\..+/g)){
					$(this).parent().append("<div class='error'>メールアドレスの形式が異なります。</div>");
				}
			});
		
			//メールアドレス確認用
			$(this).filter(".mail_chk").each(function(){
				if($(this).val() && $(this).val() != $("input[name=" + $(this).attr("name").replace(/^(.+)_chk$/, "$1")+"]").val()){
					$(this).parent().append("<div class='error'>メールアドレスが一致しません。</div>");
				}
			});
		});
		

		//ラジオボタンのチェック
		$(":radio").filter(".required").each(function(){
			var tmp_name = $(this).attr("name");
			var radio_tmp = document.getElementsByName(tmp_name);
			var cnt = 0;
			tmp = tmp_name.replace("[]", "");
			for(var i=0;i<radio_tmp.length;i++) {
				if (radio_tmp[i].checked) {
					cnt = 1;
				}
			}
			if(cnt == 0){
				$(this).parent().append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
			}
		});
		
		//リストのチェック
		$("select").filter(".required").each(function(){
			if($("select[name="+$(this).attr("name")+"]").children(':selected').val() == ""){
				tmp = $(this).attr("id");
				$(this).parent().append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
			}
		});
		
		//チェックボックスのチェック
		$(".checkboxrequired").each(function(){
			if($(":checkbox:checked", this).size() == 0){
				tmp = $("input",this).attr("name");
				//name属性を配列にしているために[]を削除
				tmp = tmp.replace("[]", "");
				$(this).append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
			}
		});

		//環境依存文字簡易チェック
		$(":text, textarea").filter(".izon").each(function(){
			var show = $(this).attr("id");
			var text = $(this).val();
			var c_regP = "[①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳ⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩ㍉㌔㌢㍍㌘㌧㌃㌶㍑㍗㌍㌦㌣㌫㍊㌻㎜㎝㎞㎎㎏㏄㎡㍻〝〟№㏍℡㊤㊥㊦㊧㊨㈱㈲㈹㍾㍽㍼]";
			if(text.match(c_regP)){
				$(this).parent().append("<div class='error'>" + data[show] + "に環境依存文字が入力されています。</div>");
			}
		});
		
		//ファイルチェック
		file1 = $("#file1").val().length;
		if(file1 == 0 && $("#tmp_file1").val() == ""){
			$("#files").append("<div class='error'>キャリアシートを選択してください。</div>");
		}
		file2 = $("#file2").val().length;
		if(file2 == 0 && $("#tmp_file2").val() == ""){
			$("#files").append("<div class='error'>履歴書を選択してください。</div>");
		}
		file3 = $("#file3").val().length;
		if(file3 == 0 && $("#tmp_file3").val() == ""){
			$("#files").append("<div class='error'>職務経歴書を選択してください。</div>");
		}

		//エラー処理
		if($("div.error").size() > 0){
			$('html,body').animate({ scrollTop: $("div.error:first").offset().top-40}, 'slow');
			return false;
		}

	});
});