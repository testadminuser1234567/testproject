<?php
//入力ページの設定
define('Index', 'entry.html');
//確認ページの設定（使わない場合はこのまま）
define('Confirm', 'confirm.html');
//完了ページの設定
define('Finish', 'finish.html');

define('ENCODE', 'UTF-8');

//ユーザの名前設定
$unit_name = $shimei . "様";
//ユーザの宛先設定
$email = $mail1;
$today = Date('Y年m月d日');

$birthday = $year . "年" . $month . "月" . $day . "日";
//サンクスメール設定
$thanks_mail_subject = "派遣スタッフ仮登録を受け付けました。";
$thanks_mail_from = "rasisa@hurex.co.jp";
$thanks_mail_str = "Rasisa-ヒューレックス派遣部門";
$thanks_body= <<< EOF
この度は、ヒューレックスの人材派遣サービス「Rasisa」にご登録いただき、ありがとうございます。

Rasisaウェブサイトからの仮登録を受け付けました。

ご希望条件詳細の確認と、派遣スタッフ本登録手続きについて、
Rasisaの担当キャリアアドバイザーから追ってご連絡させていだたきます。

※このメールは、自動返信にてお送りしております。
　このメールにご返信いただいてもお問合せを受け付けられませんので、ご了承ください。


【送信内容】
氏名： $shimei
フリガナ： $kana
生年月日： $birthday
性別： $sex
郵便番号： $zip
都道府県： $pref
住所　　： $address
電話1　 ： $tel1
電話2　 ： $tel2
メール1 ： $mail1
メール2 ： $mail2

添付1： $file1
添付2： $file2
添付3： $file3


Rasisa派遣部門
ヒューレックス株式会社
http://www.hurex.jp/rasisa/
EOF;

//通知メール設定
$order_mail_subject = "派遣スタッフ登録がありました。";
$order_mail_to[] = array('rasisa@hurex.co.jp', 'ヒューレックス');
//$order_mail_to[] = array('tosa@smt-net.co.jp', 'ヒューレックス');
//$order_mail_to[] = array('info@smt-net.co.jp', '追加宛先');
$order_body= <<< EOF
このメールは自動送信でお送りしています。
以下のとおりフォームメールより送信がありました。

────────────────────────────
▼送信内容
────────────────────────────

氏名： $shimei
フリガナ： $kana
生年月日： $birthday
性別： $sex
郵便番号： $zip
都道府県： $pref
住所　　： $address
電話1　 ： $tel1
電話2　 ： $tel2
メール1 ： $mail1
メール2 ： $mail2

添付1： $file1
添付2： $file2
添付3： $file3
EOF;

//項目チェック用
$validation->set_rules('shimei','氏名','required');
$validation->set_rules('kana','フリガナ','required');
$validation->set_rules('year','年','required');
$validation->set_rules('month','月','required');
$validation->set_rules('day','日','required');
$validation->set_rules('sex','性別','required');
$validation->set_rules('zip','郵便番号','required');
$validation->set_rules('pref','都道府県','required');
$validation->set_rules('address','住所','required');
$validation->set_rules('tel1','電話1','required');
$validation->set_rules('tel2','電話2','');
$validation->set_rules('mail1','メール1','required|valid_email');
$validation->set_rules('mail2','メール2','valid_email');

//ログファイル出力 on or off
$log = "off";

?>