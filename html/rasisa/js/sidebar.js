$(window).load(function(){
	$(".anc").click(function () {
		index = $(".anc").index(this);
		$('.menuSub').eq(index).slideToggle(800);
	});
	$(".menuSub").hide();

	//現在のurlで一致したらパネルを開く
	if($(location).attr('href').match(/staff\/index.html/)){
		$(".anc").eq(0).click();
	}else if($(location).attr('href').match(/haken.html/)){
		$(".anc").eq(1).click();
	}else if($(location).attr('href').match(/about.html/)){
		$(".anc").eq(2).click();
	}
});