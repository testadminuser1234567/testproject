<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="ja" />
<meta name="author" content="Rasisa" />
<meta name="Description" content="仙台の派遣募集・紹介予定派遣・正社員求人探しはヒューレックスのRasisaで。厳選した求人をご紹介します。また、正社員としてキャリアアップするためのノウハウを提供。" />
<meta name="Keywords" content="派遣,募集,求人,仙台" />
<meta name="robots" content="index,follow" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="imagetoolbar" content="no" />
<link href="css/import.css" rel="stylesheet" type="text/css" media="all" />
<link rel="canonical" href="http://www.hurex.jp/rasisa/" /> 
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqueryAutoHeight.js"></script>
<script type="text/javascript">
window.onload= function(){
	jQuery(function($){
		$('div.boxLine').autoHeight({column:2, clear:1, height:'height'});
	});
}
</script>


<script type="text/javascript" src="js/swap.js"></script>

<script type="text/javascript">
var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-6405327-10']);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script');
			ga.type = 'text/javascript';
			ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(ga, s);
	})();
</script>

<title>仙台の派遣・紹介予定派遣・求人探しはヒューレックスのRasisa</title>

</head>
<body>

<!-- #BeginLibraryItem "/Library/header.lbi" -->
<!-- head -->
<div id="headCo">
<div id="Header">
<div id="headL">
<p><a href="http://www.hurex.jp/rasisa/"><img src="images/logo.gif" alt="Rasisa" /></a></p>
<h1>仙台の派遣・紹介予定派遣・正社員求人探しはヒューレックスのRasisa</h1>
</div>
<div id="headR">

<ul>
<li><a href="client/order.html">お問合せ</a>｜</li>
<li><a href="company/index.html">会社概要</a></li>
</ul>
</div>
</div>
</div>
<!-- /head --><!-- #EndLibraryItem --><!-- contets start -->
<div id="container">
<div id="contents" class="indexCo">

<h2 id="mainIndex"><img src="images/main.jpg" alt="あなたらしさを活かす" usemap="#main" id="Image1" /></h2>

<!-- right column -->
<div id="rightCo">
<div class="boxL boxLine">
<h3>Rasisaとは</h3>
<p>正社員求人専門の転職支援会社ヒューレックスが運営する人材派遣サイト。</p>
<p>「いつかは正社員に…」 <br />
「正社員になるステップとして…」 <br />
「自分らしいキャリアを築きたい」</p>
<p> そんな想いに応えるために、厳選した求人をご紹介します。また、正社員としてキャリアアップするためのノウハウを提供。正社員にもっとも近い派遣スタイル。それが“Rasisa”です。</p>
</div>
<div class="boxR boxLine">
<h3>Rasisaコンセプト</h3>
<ul>
<li>仕事を通じてスキルを身につける</li>
<li>正社員へのキャリアアップを目指す</li>
<li>紹介予定派遣制度の活用で短期間で正社員に</li>
<li>自分らしさを活かす仕事を</li>
<li>会社の「戦力」として、やりがいと成長を感じる仕事を</li>
</ul>
</div>
</div>
<map name="main" id="main">
<area shape="rect" coords="784,167,893,202" href="https://www.hurex.jp/rasisa/staff/entry.html" onmouseover="MM_swapImage('Image1','','images/main_02.jpg',1)" onmouseout="MM_swapImgRestore()" />
</map>

<!-- left column -->
<div id="leftCo">
<div id="menuL">
<h3>メニュー</h3>
<h4>お仕事をお探しの方へ</h4>
<ul class="menuMain">
<li><a href="staff/index.html">派遣のお仕事</a></li>
<li><a href="staff/haken.html">紹介予定派遣のお仕事</a></li>
</ul>


<h4>企業担当者様</h4>
<ul>
<li><a href="client/about.html">人材活用のご提案</a></li>
<li><a href="client/order.html">オーダーお問合せ</a></li>
</ul>
<h4>会社概要</h4>
<ul>
<li><a href="company/index.html">運営会社概要</a></li>
<li><a href="company/index.html#accessMap">アクセスマップ</a></li>
</ul>
</div>
</div>
<div id="up"><a href="#Header"><img src="images/up.gif" alt="ページの先頭へ戻る" /></a></div>
</div>
</div><!-- #BeginLibraryItem "/Library/footer.lbi" --><!-- footer -->
<hr id="hrF" />
<div id="footer">
<div class="boxL">
<address>
<a href="http://www.hurex.jp/" target="_blank"><img src="images/logo_hurex_f.gif" alt="HUREX" /></a>本社：〒980-6117 仙台市青葉区中央一丁目3-1 アエル17階<br />
TEL 022-723-1770 ／ FAX 022-723-1738
</address>
<p><a href="http://www.hurex.jp/" target="_blank">仙台の正社員求人・転職情報をお探しの方はこちら</a></p>
</div>
<ul>
<li><a href="company/pp.html">プライバシーポリシー</a>｜</li>
<li><a href="client/order.html">お問合せ</a>｜</li>
<li><a href="company/index.html">会社概要</a></li>
</ul>
</div>
<!-- end -->
<div id="copy">Copyright &copy; HUREX co.,ltd. All Rights Reserved.</div>
<!-- #EndLibraryItem --></body>
</html>
