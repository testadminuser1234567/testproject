<?php

class Validation
{
	var $error_string		= '';
	var $_error_array		= array();
	var $_rules				= array();
	var $_message			= array();
	var $_fields			= array();
	var $_error_messages	= array();
	var $_current_field  	= '';
	var $err="";

	public function __construct(){}

	public function set_rules($data, $japanese = '', $rules)
	{
		if ( ! is_array($data))
		{
			if ($rules == '')
				return;

			$data = array($data => $rules);
		}
	
		foreach ($data as $key => $val)
		{
			$this->_rules[$key] = $val;
			$this->_message[$key] = $japanese;
		}
	}


	public function run()
	{
		//postデータとruleが設定されていない場合
		if (count($_POST) == 0 OR count($this->_rules) == 0)
		{
			return FALSE;
		}

		//設定してあるrule分ループ
		foreach ($this->_rules as $field => $rules)
		{
			//設定項目の取得
			$ex = explode('|', $rules);
			//必須が設定されていない場合は続行
			if ( ! in_array('required', $ex, TRUE))
			{
				if ( ! isset($_POST[$field]) OR $_POST[$field] == '')
				{
					continue;
				}
			}
			//postデータが設定されていない場合
			if ( empty($_POST[$field]))
			{
				//必須項目の場合
				if (in_array('required', $ex))
				{
					//エラーの設定
					$line = $this->_message[$field] . "は必須項目です。<br/>";
				}
				$this->_error_array[$field] = $line;
				continue;
			}

			$this->_current_field = $field;


			foreach ($ex as $rule)
			{
				$param = FALSE;

				//maxlenght[5]等に対応
				if (preg_match("/(.*?)\[(.*?)\]/", $rule, $match))
				{
					$rule	= $match[1];
					$param	= $match[2];
				}

				//バリデーションメソッドが存在しない場合
				if ( ! method_exists($this, $rule))
				{
					//ファンクションが存在する場合
					if (function_exists($rule))
					{
						$_POST[$field] = $rule($_POST[$field]);
						$this->$field = $_POST[$field];
					}
											
					continue;
				}


				//各バリデーション
				$result = $this->$rule($_POST[$field], $param);


				if ($result !== TRUE)
				{
					$line = $this->_message[$field] . $result;

					$this->_error_array[$field] = $line;
					continue 2;
				}				
			}
			
		}

		$total_errors = count($this->_error_array);

		if ($total_errors == 0)
		{
			return false;
		}
		
		$error_data="";
		foreach ($this->_error_array as $key => $val)
		{
			$error_data .= $val; 
		}
		return $error_data;
	}
	
	//必須チェック
	function required($str)
	{
		if ( ! is_array($str))
		{
			return (trim($str) == '') ? "は必須項目です。<br/>" : TRUE;
		}
		else
		{
			return ( ! empty($str));
		}
	}
	
	//マッチング（おもにメールアドレスとか）
	function matches($str, $field)
	{
		$tmp_array = explode(',', $field);
		if ( empty($_POST[$tmp_array[0]]))
		{
			return $_POST[$tmp_array[0]] . "が入力されていません。<br/>";
		}
		return ($str !== $_POST[$tmp_array[0]]) ? "が" . $tmp_array[1] . "と一致しません。<br/>" : TRUE;
//		return ($str !== $_POST[$field]) ? "は" . $field . "と一致しません" : TRUE;
	}

	//マッチング(同じデータだったらエラーにする)
	function unmatches($str, $field)
	{
		$tmp_array = explode(',', $field);
		return ($str === $_POST[$tmp_array[0]]) ? "は" .$tmp_array[1] . "と同じ値を使用できません。<br/>" : TRUE;
//		return ($str === $_POST[$field]) ? "は" .$field . "と同じ値を使用できません。" : TRUE;
	}

	//日付比較（スタートとエンドの日があってるか）
	function datecomp($str, $field)
	{
		$tmp_array = explode(',', $field);
		$end = strtotime($str);
		$start = strtotime($_POST[$tmp_array[0]]);
		return ($start >= $end) ? "は" . $tmp_array[1] . "より後の日付を指定してください。<br/>" : TRUE;
	}

	//当日より前は指定不可とする
	function date($str)
	{
		$year = date('Y');
		$month = date('m');
		$day = date('d');
		$time = mktime(0,0,0,$month,$day,$year);
		$unixtime = strtotime($str);
		return ($time > $unixtime) ? "は当日より後を指定して下さい。<br/>" : TRUE;
	}


	//最少文字数
	function min_length($str, $val)
	{
		if (preg_match("/[^0-9]/", $val))
		{
			$msg = "文字数の設定が間違っています。<br/>";
			return $msg;
		}

		if (function_exists('mb_strlen'))
		{
			$msg = "は" . $val . "文字以上で入力してください。<br/>";
			return (mb_strlen($str) < $val) ? $msg : TRUE;		
		}
		
		$msg = "は" . $val . "文字以上で入力してください。<br/>";
		return (strlen($str) < $val) ? $msg : TRUE;
	}
	
	//最大文字数
	function max_length($str, $val)
	{
		if (preg_match("/[^0-9]/", $val))
		{
			$msg = "文字数の設定が間違っています。<br/>";
			return $msg;
		}
		
		if (function_exists('mb_strlen'))
		{
			$msg = "は" . $val . "文字以下で入力してください。<br/>";
			return (mb_strlen($str) > $val) ? $msg : TRUE;		
		}

		$msg = "は" . $val . "文字以下で入力してください。<br/>";
		return (strlen($str) > $val) ? $msg : TRUE;
	}
	

	//文字数一致
	function exact_length($str, $val)
	{
		if (preg_match("/[^0-9]/", $val))
		{
			$msg = "文字数の設定が間違っています。<br/>";
			return $msg;
		}
	
		if (function_exists('mb_strlen'))
		{
			$msg = "と文字列が一致しません。<br/>";
			return (mb_strlen($str) != $val) ? $msg : TRUE;		
		}

		$msg = "と文字列が一致しません。<br/>";
		return (strlen($str) != $val) ? $msg : TRUE;
	}
	
	//Emailチェック
	function valid_email($str)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? "のメールアドレス形式が違います。<br/>" : TRUE;
	}

	function valid_emails($str)
	{
		if (strpos($str, ',') === FALSE)
		{
			return $this->valid_email(trim($str));
		}
		
		foreach(explode(',', $str) as $email)
		{
			if (trim($email) != '' && $this->valid_email(trim($email)) === FALSE)
			{
				return "の形式が違います。<br/>";
			}
		}
		
		return TRUE;
	}

	//アルファベット
	function alpha($str)
	{
		return ( ! preg_match("/^([a-z])+$/i", $str)) ? "はアルファベットで入力してください。<br/>" : TRUE;
	}
	
	//アルファベットと数値
	function alpha_numeric($str)
	{
		return ( ! preg_match("/^([a-z0-9])+$/i", $str)) ? "はアルファベットと数値のみで入力してください。<br/>" : TRUE;
	}
	
	//アルファベットと数値とアンダーバーとダッシュ
	function alpha_dash($str)
	{
		return ( ! preg_match("/^([-a-z0-9_-])+$/i", $str)) ? "はアルファベット、数値、アンダーバー、ダッシュで入力してください。<br/>" : TRUE;
	}
	
	//数値チェック
	function numeric($str)
	{
		return (bool)preg_match( '/^[\-+]?[0-9]*\.?[0-9]+$/', $str);

	}

	//数値チェック
  	function is_numeric($str)
	{
		return ( ! is_numeric($str)) ? "は数値で入力してください。<br/>" : TRUE;
	} 

	//integerチェック
	function integer($str)
	{
		return (bool)preg_match( '/^[\-+]?[0-9]+$/', $str);
	}

	//自然数
	function is_natural($str)
	{   
   		return (bool)preg_match( '/^[0-9]+$/', $str);
	}

	//ゼロ以外の自然数
	function is_natural_no_zero($str)
	{   
		if ( ! preg_match( '/^[0-9]+$/', $str))
		{
			return FALSE;
		}
	
		if ($str == 0)
		{
			return FALSE;
		}

		return TRUE;
	}

	//Base64
	function valid_base64($str)
	{
		return (bool) ! preg_match('/[^a-zA-Z0-9\/\+=]/', $str);
	}

}
