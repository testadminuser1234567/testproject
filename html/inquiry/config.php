<?php
//入力ページの設定
define('Index', 'index.php');
//確認ページの設定（使わない場合はこのまま）
define('Confirm', 'confirm.php');
//完了ページの設定
define('Finish', 'finish.php');

define('ENCODE', 'UTF-8');

//ユーザの名前設定
$unit_name = $onamae . "様";
$email = $mail;

$send_from = "info@hurex.co.jp";

//サンクスメール設定
$thanks_mail_subject = "お問合せありがとうございました。【ヒューレックス株式会社】";
$thanks_mail_from = "info@hurex.co.jp";
$thanks_mail_str = "ヒューレックス株式会社";
$thanks_body= <<< EOF
この度はヒューレックス株式会社公式ホームページより
お問合せ頂きまして誠にありがとうございました。

下記内容にて受け付けましたので内容をご確認下さいませ。

――――――――――――――――――――
【送信内容】
御名前  ： $onamae
ふりがな： $kana
E-mail  ： $mail
電話番号： $tel
お問合せの種類： $kubun
弊社のサービスをどこでお知りになりましたか： $how
送信内容：
$naiyo

――――――――――――――――――――

内容確認の上、担当よりご連絡いたします。

どうぞ宜しくお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。
ご返信いただいてもご回答できかねますので、ご了承ください。


EOF;

//通知メール設定
$order_mail_subject = "【お問合せ】HPよりお問合せがありました。";
$order_mail_to[] = array('info@hurex.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('tosa@smt-net.co.jp', 'ヒューレックス');
//$order_mail_to[] = array('info@smt-net.co.jp', '追加宛先');
$order_body= <<< EOF
ホームページより
下記内容にてお問合せがありました。

────────────────────────────
▼送信内容
────────────────────────────

御名前  ： $onamae
ふりがな： $kana
E-mail  ： $mail
電話番号： $tel
お問合せの種類： $kubun
弊社のサービスをどこでお知りになりましたか： $how
送信内容：
$naiyo



EOF;

//項目チェック用
$validation->set_rules('onamae','御名前','required');
$validation->set_rules('mail','E-mail','required|valid_email');
$validation->set_rules('naiyo','送信内容','required');
$validation->set_rules('tel','電話番号','required');
$validation->set_rules('kubun','お問合せ項目','required');

//ログファイル出力 on or off
$log = "off";

?>