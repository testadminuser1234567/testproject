<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div><!-- .site-content -->
	
	<?php if(!is_front_page()) : ?>
	<div class="sidebar_wrap">
	<div class="sidebar_inner">
	<?php get_sidebar(); ?>
	</div>
	</div>
	<?php endif; ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
	<div class="ft_wrap">
		<div class="pagetop"><img src="<?php bloginfo( 'template_url' ); ?>/img/pagetop.png" alt="PAGETOP" /></div>
		<div class="ft_menu">
			<ul class="ft_menu_lists">
				<li class="ft_menu_li">
					<a class="ft_menu_a" href="<?php echo esc_url( home_url( '/' ) ); ?>knowhow/">転職ノウハウ･コラム</a>
				</li>
				<li class="ft_menu_li">
					<a class="ft_menu_a" href="<?php echo esc_url( home_url( '/' ) ); ?>voice/">転職成功者の声</a>
				</li>
				<li class="ft_menu_li">
					<a class="ft_menu_a" href="<?php echo esc_url( home_url( '/' ) ); ?>corporate/">採用をお考えの企業様へ</a>
				</li>
				<li class="ft_menu_li">
					<a class="ft_menu_a pc tab" href="https://www.hurex.jp/company/summary/" target="_blank">会社概要</a>
					<a class="ft_menu_a sp tab" href="https://www.hurex.jp/smart/company/summary/" target="_blank">会社概要</a>
				</li>
				<li class="ft_menu_li">
					<a class="ft_menu_a pc tab" href="https://www.hurex.jp/inquiry/" target="_blank">お問い合わせ</a>
					<a class="ft_menu_a sp tab" href="https://www.hurex.jp/smart/inquiry/" target="_blank">お問い合わせ</a>
				</li>
				<li class="ft_menu_li">
					<a class="ft_menu_a pc tab" href="https://www.hurex.jp/company/privacy-policy/" target="_blank">プライバシーポリシー</a>
					<a class="ft_menu_a sp tab" href="https://www.hurex.jp/smart/company/privacy-policy/" target="_blank">プライバシーポリシー</a>
				</li>
				<li class="ft_menu_li">
					<a class="ft_menu_a pc tab" href="https://www.hurex.jp/" target="_blank">HUREX</a>
					<a class="ft_menu_a sp tab" href="https://www.hurex.jp/smart/" target="_blank">HUREX</a>
				</li>
			</ul>
		</div>
		<p class="copy_r">（C）HUREX <?php //bloginfo( 'name' ); ?></p>
	</div>
	</footer><!-- .site-footer -->

</div><!-- .site -->

<?php wp_footer(); ?>

</body>
</html>
