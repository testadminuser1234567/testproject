<?php
require_once ( dirname(__FILE__).'/SearchJob.class.php' );
$listCount = SearchJob::getListCount();
$jobAll = SearchJob::getJobAll();
$incomeAll = SearchJob::getIncomeAll();
$prefAll = SearchJob::getPrefectureAll();

// 一覧データ取得
// list => StdObj
// rowmax
// pagemax
$obj = new SearchJob();
$obj->doSearch();
$data = $obj->getLists();

/**
 * Template Name: search-job
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Sixteen 1.0
 */
 
get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="search_job_wrap">
		<h2 class="title white">RECRUIT<span>求人検索</span></h2>
		<div class="search_job">
		<span class="search_job_btn more_btn">検索条件</span>
		<form action="/ibaraki/search-job" class="serach_job_box" name="searchForm" method="get">
			<table class="search_job_table">
				<tr>
					<th class="search_job_th">職種</th>
					<td class="search_job_td">
					<?php foreach ( $jobAll as $key => $value ) { ?>
					<?php 
						$selectedJob = $obj->getActiveSearchQuery('job', TRUE);
						foreach ( $selectedJob as $selectedValue ) {
							if ( $selectedValue == $key ) {
								$checked = 'checked';
								break;
							} else {
								$checked = '';
							}
						}
					?>
						<label class="job_label"><input type="checkbox" name="job[]" class="checkbox chk" value="<?php echo $key; ?>" <?php echo $checked; ?>><span class="job_label_p"><?php echo $value; ?></span></label>
					<?php } ?>
					</td>
				</tr>
				<tr>
					<th class="search_job_th">年収</th>
					<td class="search_job_td">
						<select name="year_income" class="search_input">
						<?php foreach ( $incomeAll as $key => $value ) { ?>
							<?php 
							if ( $obj->getActiveSearchQuery('year_income') == $key ) {
								$selected = 'selected';
							} else {
								$selected = '';
							} 
							?>
							<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value;?></option>
						<?php } ?>
						</select> 以上
					</td>
				</tr>
				<tr>
					<th class="search_job_th">地域</th>
					<td class="search_job_td">
						<select name="pref" class="search_input">
						<?php foreach ( $prefAll as $key => $value ) { ?>
						<?php 
							if ( $obj->getActiveSearchQuery('pref') == $key ) {
								$selected = 'selected';
							} else {
								$selected = '';
							} 
							?>
							<option value="<?php echo $key; ?>" <?php echo $selected; ?>><?php echo $value;?></option>
						<?php } ?>
						</select>	
					</td>
				</tr>
				<tr>
					<td colspan="2" align="right"><input type="submit" class="search_submit" value="検索"/></td>
				</tr>
			</table>
		</form>
		</div>
		</div>
		
		<div class="recruit_box_wrap bg_sub01">
			<h2 class="title search_job_title">RECRUIT LIST<span><?php if($_GET['pref'] == true){
	if($_GET['pref'] == '82'){
		echo('茨城県');
	}else{ 
		echo('全国');
	}
}
if($_GET['year_income'] == true){
	if($_GET['pref'] == false) echo '年収';
	echo $_GET['year_income'].'万円以上';
}
if($_GET['job'] == true){
	$url = $_SERVER['REQUEST_URI'];
	if(strstr($url,'10753')) echo '営業';
	if(strstr($url,'10775')) echo '経理･財務';
	if(strstr($url,'10773')) echo '法務･人事･総務･広報';
	if(strstr($url,'10777')) echo '一般事務';
	if(strstr($url,'10768')) echo '経営･管理職･企画･マーケティング';
	if(strstr($url,'10734')) echo '技術職(電気･電子･機械)';
	if(strstr($url,'10779')) echo '技術職(医療･化学･食品)';
	if(strstr($url,'10749')) echo '技術職(建築･土木･その他)';
	if(strstr($url,'10716')) echo 'ITエンジニア(システム開発･インフラ･SE)';
	if(strstr($url,'10727')) echo 'Web関連職･メディア･ゲーム･デザイン';
	if(strstr($url,'10770')) echo '専門職(士業･金融･不動産･コンサルタント)';
	if(strstr($url,'10760')) echo 'サービス･外食･販売';
	if(strstr($url,'10771')) echo '教育･保育･物流･購買･その他';
} 
if(($_GET['job'] == true && !empty ($_GET['job'][0])) || $_GET['pref'] == true || $_GET['year_income'] == true) echo 'の'; ?>求人一覧</span></h2>
			
			<div class="page_navi_wrap recruit_navi">
			<div class="page_navi page_navi_top">
				<div class="wp-pagenavi">
				<?php if ( count($data) > 0 ) {?>
				<span class="pages"><?php echo get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1; ?> / <?php echo $obj->getPageMax(); ?></span>
				<?php $obj->pager(); ?>
				<?php } else { ?>
				<span class="pages">0 / 0</span>
				<?php } ?>
				</div>
			</div>
			</div>
			<div class="recruit_box recruit_lists_box inner">
			
			<div class="pc_inner pc_min">
			<?php if ( count($data) > 0 ) {?>
			<ul class="recruit_lists">
				<?php foreach ( $data as $key => $value ) { ?>
				<?php //var_dump($value);?>
				<?php $count++; ?>
				 <li class="recruit_lists_li cid_<?php echo $count ?>">
					<div class="recruit_lists_li_inner <?php $cats = get_the_category();foreach($cats as $cat):if($cat->parent) echo $cat->slug; endforeach;?>">
					<p class="date">
					<?php if( $value->i_name == true ) : ?><span class="new_mark"><?php echo $value->i_name; ?></span><?php endif; ?>
					<span class="update"><?php echo $value->j_updated; ?></span> 更新</p>
					<h3 class="recruit_lists_title"><a href="https://www.hurex.jp/job-search/detail.html?id=<?php echo $value->job_id; ?>&argument=vgmWMMby&dmai=ibaraki_t_job" class="pc tab list01-titleText" target="_blank"><?php echo $value->job_title; ?></a><a href="https://www.hurex.jp/smart/job-search/detail.html?id=<?php echo $value->job_id; ?>&argument=vgmWMMby&dmai=ibaraki_t_job" class="sp tab list01-titleText" target="_blank"><?php echo $value->job_title; ?></a></h3>
					<table class="recruit_lists_table">
						<tr>
							<th class="recruit_lists_th">勤務地 : </th>
							<td class="recruit_lists_td"><span class="recruit_lists_p"><?php echo $value->prefecture_name; ?></span></td>
							<th class="recruit_lists_th">年収 : </th>
							<td class="recruit_lists_td"><span class="recruit_lists_p"><?php if( $value->minsalary > 400 ) echo $value->minsalary; ?><?php if( $value->maxsalary > 1 ) echo '～'.$value->maxsalary; ?>万</span></td>
						</tr>
						<tr>
							<th class="recruit_lists_th">雇用形態 : </th>
							<td class="recruit_lists_td" colspan="3"><span class="recruit_lists_p"><?php echo $value->employ_name; ?></span></th>
						</tr>
						<tr>
							<th class="recruit_lists_th">職種 : </th>
							<td class="recruit_lists_td" colspan="3"><span class="recruit_lists_p"><?php echo $value->jobcategory_name; ?></span></th>
						</tr>
					</table>
					<a class="recruit_lists_btn pc tab list02-moreButton" href="https://www.hurex.jp/job-search/detail.html?id=<?php echo $value->job_id; ?>&argument=vgmWMMby&dmai=ibaraki_t_job" target="_blank">詳細情報を見る</a>
					<a class="recruit_lists_btn sp tab list02-moreButton" href="https://www.hurex.jp/smart/job-search/detail.html?id=<?php echo $value->job_id; ?>&argument=vgmWMMby&dmai=ibaraki_t_job" target="_blank">詳細情報を見る</a>
				</div>
				</li>
				<?php } ?>

			</ul>
			<?php } else { ?>
			<p>データがありません</p>
			<?php } ?>
 		</div>
 		</div>
			
		<div class="page_navi_wrap recruit_navi page_navi_bottom_wrap recruit_navi_bottom">
		<div class="page_navi page_navi_bottom">
			<div class="wp-pagenavi">
			<?php if ( count($data) > 0 ) {?>
			<span class="pages"><?php echo get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1; ?> / <?php echo $obj->getPageMax(); ?></span>
			<?php $obj->pager(); ?>
			<?php } else { ?>
			<span class="pages">0 / 0</span>
			<?php } ?>
			</div>
			<img src="<?php bloginfo( 'template_url' ); ?>/img/pagetop02.png" class="pagetop02" />
		</div>
		</div>
	 		
 		
	</main>
 
</div>
 
<?php get_footer(); ?>
