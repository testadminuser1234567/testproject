<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if(is_front_page()) : ?>
		
			<div class="top_wrap con01">
			<div class="top_con inner">
				<img src="<?php bloginfo( 'template_url' ); ?>/img/top_sp2.jpg?v2" alt="あなたの「転職」をもっと明るく" class="sp" />
				<a href="https://www.hurex.jp/mypage/signup/regist?argument=vgmWMMby&dmai=ibaraki_t_apply" target="_blank" class="top_btn top02-mainImg"><img src="<?php bloginfo( 'template_url' ); ?>/img/top_btn_pc.png" alt="無料転職支援に申し込む" /></a>
			</div>
			</div>
        	
        	<div class="search_box_wrap bg_sub01 con02">
			<div class="search_box inner">
				<form method="get" name="searchpage" action="/ibaraki/search-job">
					<h2 class="title">SEARCH<span>茨城県の求人を検索</span></h2>
					<input type="hidden" name="pref" value="82" />
					<select class="b01 search_input" name="job[]">
						<option value="">職種</option>
						<option value="10753,10754,10755,10756,10757,10758,10765,10725">営業</option>
						<option value="10775">経理･財務</option>
						<option value="10773,10774,10776">法務･人事･総務･広報</option>
						<option value="10777,10762">一般事務</option>
						<option value="10768,10769">経営･管理職･企画･マーケティング</option>
						<option value="10734,10735,10736,10737,10738,10739,10740,10741,10742,10743,11290">技術職(電気･電子･機械)</option>
						<option value="10779,10780,10781,10782,10783">技術職(医療･化学･食品)</option>
						<option value="10749,10750,10751">技術職(建築･土木･その他)</option>
						<option value="10716,10717,10718,10719,10720,10721,10722,10723,10724">ITエンジニア(システム開発･インフラ･SE)</option>
						<option value="10727,10728,10729,10730,10731,10732">Web関連職･メディア･ゲーム･デザイン</option>
						<option value="10770">専門職(士業･金融･不動産･コンサルタント)</option>
						<option value="10760,10761,10762">サービス･外食･販売</option>
						<option value="10771,10766">教育･保育･物流･購買･その他 </option>
					</select>
					<select class="b02 search_input" name="year_income">
						<option value="">年収</option>
						<!--<option value="100">100万円</option>
						<option value="200">200万円</option>-->
						<option value="300">300万円</option>
						<option value="400">400万円</option>
						<option value="500">500万円</option>
						<option value="600">600万円</option>
						<option value="700">700万円</option>
						<option value="800">800万円</option>
						<option value="900">900万円</option>
						<option value="1000">1000万円</option>
					</select>
					<input id="submit" class="search_submit" name="submit" value="検索する" type="submit">
				</form>
			</div>
			</div>
			
			<div class="recruit_box_wrap bg_main con03">
			<div class="recruit_box inner">
				<h2 class="title">NEW ARRIVAL<span>新着求人</span></h2>
				<?php
				require_once ( dirname(__FILE__).'/get_new_job.php' );
				$obj = new GetNewJob();
				$tags = get_the_tags();
				$obj->getNew(6,82);
				?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>search-job/?pref=82" class="more_btn">もっと見る</a>
			</div>
			</div>
			
			<div id="about" class="ser_box_wrap bg_sub02 con04">
			<div class="ser_box inner">
				<img src="<?php bloginfo( 'template_url' ); ?>/img/ser_img_pc.jpg?v2" alt="無料転職支援に申し込む" class="ser_img" />
				<div class="ser_text">
					<img src="<?php bloginfo( 'template_url' ); ?>/img/ser_logo_pc.png" alt="茨城の転職.comとは？" class="ser_logo" />
					<h3>ヒューレックス株式会社が運営する<br class="pc"/>茨城県に特化した転職支援サービスです。</h3>
					<p>ヒューレックスは茨城県の銀行と提携しています。銀行の取引先である優良企業から継続的に求人情報が得られるため、他の転職エージェントや転職サイトには掲載されない<strong>ヒューレックスだけの独占求人情報が多数あります。</strong><br/>
					また、求人の数や質だけではなく、<strong>茨城の市場に精通したプロの転職コンサルタント</strong>が、茨城県で転職活動したいあなたを<strong>親身になってサポート</strong>いたします。</p>
					<a href="#flow" class="more_btn">転職支援サービスの流れ</a>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>service" class="more_btn b2">ヒューレックスの想い</a>
				</div>
			</div>
			</div>
			
			<div class="btn_box_wrap bg_sub03 con05">
			<div class="btn_box inner">
				<a href="https://www.hurex.jp/mypage/signup/regist?argument=vgmWMMby&dmai=ibaraki_t_apply" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/img/btn_pc.png" alt="無料転職支援に申し込む" /></a>
			</div>
			</div>
			
			<div id="flow" class="flow_box_wrap con06">
			<div class="flow_box inner">
				<h2 class="title">SERVICE FLOW<span>転職支援サービスの流れ</span></h2>
				<img src="<?php bloginfo( 'template_url' ); ?>/img/flow.png" class="flow_img" />
				<dl>
					<dt class="fl01"><span>1</span>無料転職支援に<br class="pc"/>申し込む</dt>
					<dd>まずは｢<a href="https://www.hurex.jp/mypage/signup/regist?argument=vgmWMMby&dmai=ibaraki_t_apply" target="_blank" class="top03-stepText">無料転職支援に申し込む</a>｣から必要事項を入力し、無料会員登録を行ってください。<br/>
					会員登録後、2営業日以内に当社からご連絡させていただきます｡</dd>
					<dt class="fl02"><span>2</span>キャリア<br class="pc"/>カウンセリング<br class="pc"/>（面談）</dt>
					<dd><strong>茨城の市場を熟知している当社の転職コンサルタント</strong>が、あなたのご経歴やこれまで培ってきたスキル、<strong>今後の要望</strong>などをお聞きし、最適なキャリアプランと求人情報をご紹介します。お仕事終わりの平日の夜や休日も対応可能です。直接の面談の他、お電話やWebチャットでの面談も承っております。</dd>
					<dt class="fl03"><span>3</span>マッチング<br class="pc"/>（求人紹介）</dt>
					<dd>ヒアリングさせていただいた経歴、スキルやご要望からマッチした求人をご紹介いたします。求人企業の仕事内容や待遇はもちろん、<strong>社内環境、社長・社員の人柄、部署の雰囲気といったさまざまな情報</strong>をお伝えします。</dd>
					<dt class="fl04"><span>4</span>面接・応募</dt>
					<dd><strong>履歴書・職務経歴書の書き方のアドバイス</strong>や<strong>面接対策</strong>を行います。履歴書・職務経歴書では「企業にあなたの魅力が伝わるように」、面接対策では「企業に自分自身の魅力を伝えられるように｣プロの転職コンサルタントがサポートいたします。</dd>
					<dt class="fl05"><span>5</span>転職成功<br class="pc"/>（内定）</dt>
					<dd>ヒューレックスでは<strong>入社準備のサポート</strong>や<strong>入社後のフォロー</strong>も実施しています。入社後は定期的にご連絡し、状況を確認させていただきます。<strong>何かお悩み事がある場合は、気兼ねなくご相談ください。</strong></dd>
				</dl>
			</div>
			</div>
			
			<div class="point_box_wrap bg_main con07">
			<div class="point_box inner">
				<h2 class="title">THREE REASONS<span>ヒューレックスが選ばれる3つの理由</span></h2>
				<ol>
					<li>
						<img src="<?php bloginfo( 'template_url' ); ?>/img/point01.png?v2" alt="茨城の地銀と提携しているから優良求人・非公開求人が多い" />
						<div>
							<p class="number">01</p>
							<h3>茨城の地銀と提携しているから<br/>優良求人・非公開求人が多い</h3>
							<p>ヒューレックスは<strong>茨城の地方銀行と提携</strong>をして独自のネットワークを構築しています。地方銀行の取引先である企業を紹介していただいているため、<strong>優良企業の求人を数多く保有</strong>しております。また、地銀と連携しながら企業と関係を築いてきたため、公開（掲載）されていない「<strong>非公開求人</strong>」や、ヒューレックスにしかない「<strong>独占求人</strong>」を多く紹介できるのも強みです。</p>
						</div>
					</li>
					<li>
						<img src="<?php bloginfo( 'template_url' ); ?>/img/point02.png?v2" alt="一人ひとりの将来を見据えた転職を提案" />
						<div>
							<p class="number">02</p>
							<h3>一人ひとりの<br/>将来を見据えた転職を提案</h3>
							<p>転職はこれからの人生を左右する重要な選択です。ヒューレックスでは、<strong>一人ひとりの想いや希望</strong>をくみ取りながら、スキルやキャリア、人柄や事情に合わせて、<strong>その人が最も成長・発展</strong>できるような企業や求人情報をご紹介します。そしてその企業で活躍し、成長することで、結果として自分の企業も発展し、報酬も上がる。そのような<strong>良い循環</strong>が起こることをも目指しています。</p>
						</div>
					</li>
					<li>
						<img src="<?php bloginfo( 'template_url' ); ?>/img/point03.png?v2" alt="経験豊富なコンサルタントがサポートするから内定を獲得できる" />
						<div>
							<p class="number">03</p>
							<h3>経験豊富なコンサルタントが<br/>サポートするから内定を獲得できる</h3>
							<p>ヒューレックスの転職コンサルタントは、<strong>さまざまな業界・職種に精通</strong>しているプロフェッショナルです。合否の実例や各社の傾向といった<strong>茨城に特化したノウハウを数多く蓄積</strong>しているため、ヒューレックスを通して転職活動をすることで<strong>他社よりも有利に</strong>転職活動を進められるでしょう。また、履歴書・職務経歴書の書き方や面接対策といった転職活動で発生する手間のサポートもお任せください。</p>
						</div>
					</li>
				</ol>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>voice" class="more_btn">転職成功者の声</a>
			</div>
			</div>
			
			<div class="btn_box_wrap bg_sub01 con08">
			<div class="btn_box inner">
				<a href="https://www.hurex.jp/mypage/signup/regist?argument=vgmWMMby&dmai=ibaraki_t_apply" target="_blank" class="top04-reasonUnder"><img src="<?php bloginfo( 'template_url' ); ?>/img/btn_pc.png" alt="無料転職支援に申し込む" /></a>
			</div>
			</div>
			
			<div class="consul_box_wrap bg_main con09">
			<div class="consul_box inner">
				<h2 class="title">CONSULTANTS<span>コンサルタント紹介</span></h2>
				<ul>
					<li>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>consultants">
						<img src="<?php bloginfo( 'template_url' ); ?>/img/consul01_pc.png" alt="国家資格キャリアコンサルタント保有,須賀川 敏哉" />
						<p>
							<span>コンサルタント</span>
							須賀川 敏哉
						</p>
					</a>
					</li>
					<li>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>consultants#yamagami">
						<img src="<?php bloginfo( 'template_url' ); ?>/img/consul02_pc.png" alt="山上 公平" />
						<p>
							<span>コンサルタント</span>
							山上 公平
						</p>
					</a>
					</li>
					<li>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>consultants#sawada">
						<img src="<?php bloginfo( 'template_url' ); ?>/img/consul03_pc.png" alt="澤田 聡一郎" />
						<p>
							<span>コンサルタント</span>
							澤田 聡一郎
						</p>
					</a>
					</li>
					<li>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>consultants#oshikiri">
						<img src="<?php bloginfo( 'template_url' ); ?>/img/consul04_pc.png" alt="押切 遥" />
						<p>
							<span>キャリアアドバイザー</span>
							押切 遥
						</p>
					</a>
					</li>
					<li>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>consultants#muroi">
						<img src="<?php bloginfo( 'template_url' ); ?>/img/consul05_pc.png" alt="室井 美穂" />
						<p>
							<span>キャリアアドバイザー</span>
							室井 美穂
						</p>
					</a>
					</li>
				</ul>
			</div>
			</div>
		
		<?php elseif(is_page('knowhow')) : ?>
		
		<section id="primary" class="content-area">
		<main id="main" class="site-main bg_sub01" role="main">
		<div class="post_box_wrap bg_sub02">
		
		<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			query_posts('posts_per_page=1&paged='.$paged);
			if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
			<div class="arc_title_wrap" style="background-image:url(<?php the_post_thumbnail_url( 'full' ); ?>)">
			<div class="arc_title">
				<h2 class="title white">KNOW-HOW<span>転職ノウハウ</span></h2>
			</div>
			</div>
			<?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>
			
			<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			query_posts('posts_per_page=20&paged='.$paged);
			$GLOBALS["cnt"] = 0;
			if (have_posts()) : ?>
			<div class="page_navi_wrap">
			<div class="page_navi page_navi_top">
				<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
			</div>
			</div>

			<div class="search_arc_wrap bg_sub06">
			<div class="search_arc inner">
				<span class="search_arc_btn more_btn">記事を絞り込む</span>
				<form method="get" action="<?php bloginfo( 'url' ); ?>" class="serach_arc_box">
					<?php wp_dropdown_categories('depth=0&orderby=name&hide_empty=1&show_option_all=カテゴリーを選択'); ?>
					<?php $tags = get_tags(); if ( $tags ) : ?>
						<select name='tag' id='tag' class="postform">
						<option value="" selected="selected">タグを選択</option>
						<?php foreach ( $tags as $tag ): ?>
						<option value="<?php echo esc_html( $tag->slug);  ?>"><?php echo esc_html( $tag->name ); ?></option>
						<?php endforeach; ?>
						</select>
					<?php endif; ?>
					<input name="s" id="s" type="text" class="serach_arc_text" placeholder="フリーワード検索" />
					<input id="submit" class="search_submit" type="submit" value="検索する" />
				</form>
			</div>
			</div>

			<div class="post_box pc_min inner">
			<div class="pc_inner">
				<header class="page-header">
					<?php
						the_archive_title( '<h2 class="title">ARTICLES<span>記事一覧(', ')</span></h2>' );
					?>
				</header><!-- .page-header -->

				<ul class="post_lists">
                   <?php
					$cat_now = get_the_category();
					$cat_now = $cat_now[0];
					$cat_name = $cat_now->name;
					$slug = $cat_now->name;
					$GLOBALS["cnt"] = 0;
					?>
					<?php while (have_posts()) : the_post(); ?>
					<li class="post_lists_li <?php echo "cnt_". ++$GLOBALS["cnt"]; if($GLOBALS["cnt"] > 0) echo " second"; ?>">
						<a href="<?php the_permalink(); ?>">
							<?php if( $GLOBALS["cnt"] < 3 ) : ?><span class="new_mark new_post">NEW</span><?php endif; ?>
							<div class="post_lists_img" style="background-image:url(<?php the_post_thumbnail_url( 'full' ); ?>)"></div>
						</a>
						<div class="post_lists_text">
							<p class="date gray"><a href="/category/<?php $cat = get_the_category(); $cat = $cat[0]; {echo $cat->slug;} ?>" class="post_lists_cat"><?php $cat = get_the_category(); $cat = $cat[0]; {echo $cat->cat_name;} ?></a><span class="post_lists_sp">　|　</span><?php echo the_modified_time('Y.m.d D') ?></p>
							<a href="<?php the_permalink(); ?>">
								<h3 class="post_li_title"><?php the_title(); ?></h3>
								<p class="post_p"><?php echo get_the_excerpt(); ?></p>
							</a>
							<div class="post_lists_tag">
							<?php the_tags('', ' '); ?>
							</div>
						</div>
					</li>
				   <?php endwhile; ?>
                
				</ul>
			</div>
			</div>

			<div class="page_navi_wrap page_navi_bottom_wrap">
			<div class="page_navi page_navi_bottom">
				<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
				<img src="<?php bloginfo( 'template_url' ); ?>/img/pagetop02.png" class="pagetop02" />
			</div>
			</div>
		
			<?php endif; wp_reset_query(); ?>
			
			</div>
			</main><!-- .site-main -->
		</section><!-- .content-area -->
		<?php else: ?>
		<div class="consul_box_wrap">
			<header class="entry-header bg_main">
				<?php the_title( '<h2 class="title">', '</h1>' ); ?>
			</header><!-- .entry-header -->

			<div class="post_box pc_min inner">
			<div class="pc_inner">
				<?php
				// Start the loop.
				while ( have_posts() ) : the_post();

					// Include the page content template.
					get_template_part( 'content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				// End the loop.
				endwhile;
				?>
			</div>
			</div>
		</div>
		
		<?php endif; ?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
