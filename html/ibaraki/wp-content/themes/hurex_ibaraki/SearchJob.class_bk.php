<?php

require_once(dirname(__FILE__).'/search-job-config.php');

/*
 * API操作
*/
class SearchJob {

	var $_page = 0;
	var $_rows = 0;

	var $_response = '';
	var $_data = array();
	var $_pagemax = 0;
	var $_rowmax = 0;

	/**
	 * 検索キーワード
	 * @param array $keyword
	 */
	public function doSearch($rows="", $pref="", $job="", $year_income="") {

		// 職種
		if( !empty($job) ){
			$jobs = $this->_queryParameter('job', TRUE);
		} else {
			$jobs = $this->_queryParameter('job', TRUE, $job);
		}

		// 都道府県
		if( !empty($pref) ){
			$pref = $this->_queryParameter('pref', FALSE, $pref);
		} else {
			$pref = $this->_queryParameter('pref', FALSE);
		}

		// 年収
		if( !empty($year_income) ){
			$yearIncome = $this->_queryParameter('year_income', FALSE, $year_income);
		} else {
			$yearIncome = $this->_queryParameter('year_income');
		}
			
		// 表示件数
		if ( !isset($_GET['rows']) && empty($rows) ) {
			$this->_rows = ENV_PAGENATION_DEFAULT_COUNT;
		} else
		if ( !empty($rows) ) {
			$this->_rows = $rows;
		} else {
			$this->_rows = $_GET['rows'];
		}

		$this->_page = get_query_var('paged');

		if ( !$this->_page ) {

			// デフォルト
			$this->_page = 1;

		}

		// リクエストパラメータ組み立て
		$queryArray = array();
		$queryArray[] = 'rows='.$this->_rows;
		$queryArray[] = 'page='.$this->_page;

		if ( $pref ) {
			$queryArray[] = 'pref='.$pref;
		}

		if ( $yearIncome ) {
			$queryArray[] = 'year_income='.$yearIncome;
		}

		if ( count( $jobs ) > 0 ) {
			foreach ( $jobs as $value ) {
				$queryArray[] = 'job[]='.$value;
			}
		}

		$query = implode('&', $queryArray);

		$basic = array(
				'User-Agent: My User Agent 1.0',
				'Authorization: Basic '.base64_encode(ENV_API_BASIC_ID.':'.ENV_API_BASIC_PWD)
		);
		$options = array('http' => array('header' => implode("\r\n", $basic ) ) );
		$response = file_get_contents(ENV_API_URL.'&'.$query, false, stream_context_create($options));
		$this->_response = json_decode($response);

	}

	/**
	 * 検索結果のデータを取得
	 */
	public function getLists() {
		return $this->_response->lists;
	}

	/**
	 * 最大行数
	 */
	public function getRowMax() {
		return $this->_response->rowmax[0];
	}

	/**
	 * 最大ページ数
	 */
	public function getPageMax() {
		return $this->_response->pagemax;
	}

	/**
	 * 検索条件をこねこねする
	 * @param unknown $key
	 */
	private function _queryParameter($key, $isArray = FALSE,$params=NULL) {

		if ( !isset($_GET[$key]) && empty($params) ) {
			if ( $isArray ) {
				$param = array();
			} else {
				$param = '';
			}
		}else
		if ( !empty($params) ) {
			$param = $params;
		} else {
			$param = $_GET[$key];
		}
		return $param;
	}

	/**
	 * 選択中の指定されたキーのコード取得
	 * @return unknown
	 */
	public function getActiveSearchQuery($key, $isArray = FALSE) {

		if ( !isset($_GET[$key]) ) {
			if ( $isArray ) {
				$param = array();
			} else {
				$param = '';
			}
		} else {
			$param = $_GET[$key];
		}

		return $param;

	}

	// ================ ページャー ================ //

	/**
	 * ページャー表示
	 * @param unknown $start
	 * @param unknown $end
	 */
	public function pager() {

		$big = 9999999999;
		$arg = array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'current' => max( 1, get_query_var('paged') ),
				'total'   => $this->getPageMax()
		);
		echo $this->_getPaginateLinks($arg);

	}

	/**
	 * n件〜n件
	 * @return string
	 */
	public function offset() {
		$page = get_query_var('paged');
		if ( !$page ) {
			$start = 1;
			$end = $this->_rows;
		} else {
			$page = $page - 1;
			$start = ($page * $this->_rows) + 1;
			$end = $page * ($this->_rows) + $this->_rows;
		}

		return $start.'件〜'.$end.'件';
	}

	/**
	 * ページリンク生成
	 * @param string $args
	 * @return void|multitype:string |string
	 */
	private function _getPaginateLinks( $args = '' ) {
		global $wp_query, $wp_rewrite;

		$pagenum_link = html_entity_decode( get_pagenum_link() );
		$url_parts    = explode( '?', $pagenum_link );
		$total   = isset( $wp_query->max_num_pages ) ? $wp_query->max_num_pages : 1;
		$current = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;

		$pagenum_link = trailingslashit( $url_parts[0] ) . '%_%';

		$format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
		$format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

		$defaults = array(
				'base' => $pagenum_link, // http://example.com/all_posts.php%_% : %_% is replaced by format (below)
				'format' => $format, // ?page=%#% : %#% is replaced by the page number
				'total' => $total,
				'current' => $current,
				'show_all' => false,
				'prev_next' => true,
				'prev_text' => __('&laquo; Previous'),
				'next_text' => __('Next &raquo;'),
				'end_size' => 1,
				'mid_size' => 2,
				'type' => 'plain',
				'add_args' => array(), // array of query args to add
				'add_fragment' => '',
				'before_page_number' => '',
				'after_page_number' => ''
		);

		$args = wp_parse_args( $args, $defaults );

		if ( ! is_array( $args['add_args'] ) ) {
			$args['add_args'] = array();
		}
		if ( isset( $url_parts[1] ) ) {
			$format = explode( '?', str_replace( '%_%', $args['format'], $args['base'] ) );
			$format_query = isset( $format[1] ) ? $format[1] : '';
			wp_parse_str( $format_query, $format_args );

			wp_parse_str( $url_parts[1], $url_query_args );

			foreach ( $format_args as $format_arg => $format_arg_value ) {
				unset( $url_query_args[ $format_arg ] );
			}
			$args['add_args'] = array_merge( $args['add_args'], urlencode_deep( $url_query_args ) );
		}

		$total = (int) $args['total'];
		if ( $total < 2 ) {
			return;
		}
		$current  = (int) $args['current'];
		$end_size = (int) $args['end_size']; // Out of bounds?  Make it the default.
		if ( $end_size < 1 ) {
			$end_size = 1;
		}
		$mid_size = (int) $args['mid_size'];
		if ( $mid_size < 0 ) {
			$mid_size = 2;
		}
		$add_args = $args['add_args'];
		$r = '';
		$page_links = array();
		$dots = false;

		if ( $args['prev_next'] && $current && 1 < $current ) {
			$link = str_replace( '%_%', 2 == $current ? '' : $args['format'], $args['base'] );
			$link = str_replace( '%#%', $current - 1, $link );
			if ( $add_args ){
				// 前へ
				$link .= $args['add_fragment'];

				$page_links[] = '<a class="prev page-numbers" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $args['prev_text'] . '</a>';
			}
		}
		for ( $n = 1; $n <= $total; $n++ ) {
			if ( $n == $current ) {
				// 現在ページ
				$page_links[] = "<span class='page-numbers current'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</span>";
				$dots = true;
			} else {
				if ( $args['show_all'] || ( $n <= $end_size || ( $current && $n >= $current - $mid_size && $n <= $current + $mid_size ) || $n > $total - $end_size ) ) {
					$link = str_replace( '%_%', 1 == $n ? '' : $args['format'], $args['base'] );
					$link = str_replace( '%#%', $n, $link );
					$link .= $args['add_fragment'];

					$page_links[] = "<a class='page-numbers' href='" . esc_url( apply_filters( 'paginate_links', $link ) ) . "'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</a>";
					$dots = true;
				} elseif ( $dots && ! $args['show_all'] ) {
					$page_links[] = '<span class="page-numbers dots">' . __( '&hellip;' ) . '</span>';
					$dots = false;
				}
			}
		}
		if ( $args['prev_next'] && $current && $current < $total ) {
			// 次へ
			$link = str_replace( '%_%', $args['format'], $args['base'] );
			$link = str_replace( '%#%', $current + 1, $link );
			$link .= $args['add_fragment'];

			$page_links[] = '<a class="next page-numbers" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $args['next_text'] . '</a>';
		}

		switch ( $args['type'] ) {
			case 'array' :
				return $page_links;

			case 'list' :
				$r .= "<ul class='page-numbers'>\n\t<li>";
				$r .= join("</li>\n\t<li>", $page_links);
				$r .= "</li>\n</ul>\n";
				break;

			default :
				$r = join("\n", $page_links);
				break;
		}
		return $r;
	}

	// ================ 設定項目 ================ //

	/**
	 * 一覧の表示件数
	 * @return multitype:string
	 */
	public static function getListCount() {
		$count = array();

		$count[] = '10';
		$count[] = '30';
		$count[] = '100';

		return $count;
	}

	/**
	 * 職種をすべて取得する
	 * @return multitype:string
	 */
	public static function getJobAll() {
		$jobAll = array();

		$jobAll["10753,10754,10755,10756,10757,10758,10765,10725"] = "営業";
		$jobAll["10775"] = "経理･財務";
		$jobAll["10773,10774,10776"] = "法務･人事･総務･広報";
		$jobAll["10777,10762"] = "一般事務";
		$jobAll["10768,10769"] = "経営･管理職･企画･マーケティング";
		$jobAll["10734,10735,10736,10737,10738,10739,10740,10741,10742,10743,11290"] = "技術職(電気･電子･機械)";
		$jobAll["10779,10780,10781,10782,10783"] = "技術職(医療･化学･食品)";
		$jobAll["10749,10750,10751"] = "技術職(建築･土木･その他)";
		$jobAll["10716,10717,10718,10719,10720,10721,10722,10723,10724"] = "ITエンジニア(システム開発･インフラ･SE)";
		$jobAll["10727,10728,10729,10730,10731,10732"] = "Web関連職･メディア･ゲーム･デザイン";
		$jobAll["10770"] = "専門職(士業･金融･不動産･コンサルタント)";
		$jobAll["10760,10761,10762"] = "サービス･外食･販売";
		$jobAll["10771,10766"] = "教育･保育･物流･購買･その他 ";

		return $jobAll;
	}

	/**
	 * すべての年収を取得する
	 */
	public static function getIncomeAll() {

		$income = array();
		$income["0"] = "選択してください";
		$income["100"] = "100万円";
		$income["200"] = "200万円";
		$income["300"] = "300万円";
		$income["400"] = "400万円";
		$income["500"] = "500万円";
		$income["600"] = "600万円";
		$income["700"] = "700万円";
		$income["800"] = "800万円";
		$income["900"] = "900万円";
		$income["1000"] = "1000万円";

		return $income;
	}

	/**
	 * すべての都道府県を取得する
	 * @return multitype:string
	 */
	public static function getPrefectureAll() {
		$pref = array();

		$pref["0"] = "選択してください";
		$pref["75"] = "北海道";
		$pref["76"] = "青森県";
		$pref["77"] = "岩手県";
		$pref["78"] = "宮城県";
		$pref["79"] = "秋田県";
		$pref["80"] = "山形県";
		$pref["81"] = "福島県";
		$pref["82"] = "茨城県";
		$pref["83"] = "栃木県";
		$pref["84"] = "群馬県";
		$pref["85"] = "埼玉県";
		$pref["86"] = "千葉県";
		$pref["87"] = "東京都";
		$pref["88"] = "神奈川県";
		$pref["89"] = "新潟県";
		$pref["90"] = "富山県";
		$pref["91"] = "石川県";
		$pref["92"] = "福井県";
		$pref["93"] = "山梨県";
		$pref["94"] = "長野県";
		$pref["95"] = "岐阜県";
		$pref["96"] = "静岡県";
		$pref["97"] = "愛知県";
		$pref["98"] = "三重県";
		$pref["99"] = "滋賀県";
		$pref["100"] = "京都府";
		$pref["101"] = "大阪府";
		$pref["102"] = "兵庫県";
		$pref["103"] = "奈良県";
		$pref["104"] = "和歌山県";
		$pref["105"] = "鳥取県";
		$pref["106"] = "島根県";
		$pref["107"] = "岡山県";
		$pref["108"] = "広島県";
		$pref["109"] = "山口県";
		$pref["110"] = "徳島県";
		$pref["111"] = "香川県";
		$pref["112"] = "愛媛県";
		$pref["113"] = "高知県";
		$pref["114"] = "福岡県";
		$pref["115"] = "佐賀県";
		$pref["116"] = "長崎県";
		$pref["117"] = "熊本県";
		$pref["118"] = "大分県";
		$pref["119"] = "宮崎県";
		$pref["120"] = "鹿児島県";
		$pref["121"] = "沖縄県";
		$pref["74"] = "海外";

		return $pref;

	}
}
