<?php
/**
 * The template for displaying Author bios
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<div class="author-info">
	<div class="author-avatar" style="background-image: url(<?php echo get_avatar_onlyurl($user_id); ?>)">
	<?php //echo get_avatar( get_the_author_meta('ID')); ?>
	</div><!-- .author-avatar -->

	<div class="author-description">
		<h2 class="author-head">この記事を書いた人</h2>
		<p class="author-position"><?php the_author_meta('position'); ?></p>
		<h3 class="author-title"><?php echo get_the_author(); ?></h3>

		<p class="author-bio">
			<?php the_author_meta( 'description' ); ?>
		</p><!-- .author-bio -->

	</div><!-- .author-description -->
</div><!-- .author-info -->
