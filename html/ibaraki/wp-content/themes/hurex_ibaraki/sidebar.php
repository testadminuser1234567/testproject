
	<div id="secondary" class="secondary">

		<div class="app_box_wrap bg_app">
			<a href="https://www.hurex.jp/mypage/signup/regist?argument=vgmWMMby&dmai=ibaraki_t_apply" target="_blank" class="more_btn app_more_btn">転職支援申込</a>
		</div>
		
		<div class="search_box_wrap search_box_side bg_white">
		<div class="search_box inner">
			<h2 class="title">SEARCH<span>茨城県の求人を検索</span></h2>
			<form method="get" name="searchpage" action="/ibaraki/search-job">
				<input type="hidden" name="pref" value="82" />
				<select class="b01 search_input" name="job[]">
					<option value="">職種</option>
					<option value="10753,10754,10755,10756,10757,10758,10765,10725">営業</option>
					<option value="10775">経理･財務</option>
					<option value="10773,10774,10776">法務･人事･総務･広報</option>
					<option value="10777,10762">一般事務</option>
					<option value="10768,10769">経営･管理職･企画･マーケティング</option>
					<option value="10734,10735,10736,10737,10738,10739,10740,10741,10742,10743,11290">技術職(電気･電子･機械)</option>
					<option value="10779,10780,10781,10782,10783">技術職(医療･化学･食品)</option>
					<option value="10749,10750,10751">技術職(建築･土木･その他)</option>
					<option value="10716,10717,10718,10719,10720,10721,10722,10723,10724">ITエンジニア(システム開発･インフラ･SE)</option>
					<option value="10727,10728,10729,10730,10731,10732">Web関連職･メディア･ゲーム･デザイン</option>
					<option value="10770">専門職(士業･金融･不動産･コンサルタント)</option>
					<option value="10760,10761,10762">サービス･外食･販売</option>
					<option value="10771,10766">教育･保育･物流･購買･その他 </option>
				</select>
				<select class="b02 search_input" name="year_income">
						<option value="">年収</option>
						<!--<option value="100">100万円</option>
						<option value="200">200万円</option>-->
						<option value="300">300万円</option>
						<option value="400">400万円</option>
						<option value="500">500万円</option>
						<option value="600">600万円</option>
						<option value="700">700万円</option>
						<option value="800">800万円</option>
						<option value="900">900万円</option>
						<option value="1000">1000万円</option>
					</select>
				<input id="submit" class="search_submit" name="submit" value="検索する" type="submit">
			</form>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>search-job/?pref=82" class="search_more_btn more_btn">もっと詳しく検索する</a>
		</div>
		</div>

		<div class="rank_box_wrap">
		<div class="rank_box inner">
			<h2 class="title">RANKING<span>よく読まれている記事</span></h2>
			<?php
			 $wpp_month = array (
			 'limit' => '5', 
			 'range' => 'monthly',
			 'order_by' => 'views',
			 'post_type' => 'post',
			 'stats_comments' => '0', 
			 'stats_views' => '0', 
			 'title_length' => '300', 
			 'thumbnail_width' => '300',
			 'thumbnail_height' => '182',
			'stats_date' => true,
			'stats_date_format' => 'Y.m.d',
			 'stats_category' => '1', 
			 'wpp_start' => '<ul class="rank_lists">',
			 'wpp_end=' => '</ul>',
			 'post_html' => '<li class="rank_li"><div class="rank_img">{thumb}</div><div class="rank_text"><p class="date">{stats}</p><h3 class="rank_title">{title}</h3></div></li>',
			);
			wpp_get_mostpopular($wpp_month); ?>
		</div>
		</div>
		
		<div class="tag_box_wrap bg_sub03">
		<div class="tag_box inner">
			<h2 class="title">TAGS<span>人気タグ</span></h2>
			<?php wp_tag_cloud('number=10&smallest=10&largest=10&order=RAND'); ?>
		</div>
		</div>

		<?php if(!is_front_page()) : ?>
		<div class="post_box_wrap ft_post bg_white">
		<div class="post_box inner">
			<h2 class="title">NEW<span>新着記事</span></h2>
			<ul class="post_lists">	
			<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				query_posts('posts_per_page=3');
				$GLOBALS["cnt"] = 0;
				if (have_posts()) : ?>
			   <?php
				$cat_now = get_the_category();
				$cat_now = $cat_now[0];
				$cat_name = $cat_now->name;
				$slug = $cat_now->name;
				?>
				<?php while (have_posts()) : the_post(); ?>
				<li class="post_lists_li <?php echo "cnt_". ++$GLOBALS["cnt"]; if($GLOBALS["cnt"] > 0) echo " second"; ?>">
					<?php if( $GLOBALS["cnt"] < 3 ) : ?><span class="new_mark new_post">NEW</span><?php endif; ?>
					<a href="<?php the_permalink(); ?>">
						<div class="post_lists_img" style="background-image:url(<?php the_post_thumbnail_url( 'full' ); ?>)"></div>

					</a>
					<div class="post_lists_text">
						<p class="date gray"><a href="/category/<?php $cat = get_the_category(); $cat = $cat[0]; {echo $cat->slug;} ?>" class="post_lists_cat"><?php $cat = get_the_category(); $cat = $cat[0]; {echo $cat->cat_name;} ?></a><span class="post_lists_sp">　|　</span><?php echo the_modified_time('Y.m.d D') ?></p>
						<a href="<?php the_permalink(); ?>">
							<h3 class="post_li_title"><?php the_title(); ?></h3>
						</a>
						<div class="post_lists_tag">
						<?php the_tags('', ' '); ?>
					</div>
				</li>
				<?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>
			</ul>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>knowhow" class="more_btn">もっと見る</a>
		</div>
		</div>
		<?php endif; ?>

	</div><!-- .secondary -->

