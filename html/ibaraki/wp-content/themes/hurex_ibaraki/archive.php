<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<?php
$cat_now = get_the_category();
$cat_now = $cat_now[0];
$cat_name = $cat_now->name;
$slug = $cat_now->name;
$GLOBALS["cnt"] = 0;
?>
	<section id="primary" class="content-area">
		<main id="main" class="site-main bg_sub01" role="main">
		<div class="post_box_wrap bg_sub02">
		
		<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			query_posts('posts_per_page=1&paged='.$paged);
			if (have_posts()) : ?>
			<?php while (have_posts()) : the_post(); ?>
			<div class="arc_title_wrap" style="background-image:url(<?php the_post_thumbnail_url( 'full' ); ?>)">
			<div class="arc_title">
				<h2 class="title white">KNOW-HOW<span>転職ノウハウ</span></h2>
			</div>
			</div>
			<?php endwhile; ?>
		<?php endif; wp_reset_query(); ?>

		
		<?php if ( have_posts() ) : ?>
		
		<div class="page_navi_wrap">
		<div class="page_navi page_navi_top">
			<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
		</div>
		</div>
		
		<div class="search_arc_wrap bg_sub06">
		<div class="search_arc inner">
			<span class="search_arc_btn more_btn">記事を絞り込む</span>
			<form method="get" action="<?php bloginfo( 'url' ); ?>" class="serach_arc_box">
				<?php wp_dropdown_categories('depth=0&orderby=name&hide_empty=1&show_option_all=カテゴリーを選択'); ?>
				<?php $tags = get_tags(); if ( $tags ) : ?>
					<select name='tag' id='tag' class="postform">
					<option value="" selected="selected">タグを選択</option>
					<?php foreach ( $tags as $tag ): ?>
					<option value="<?php echo esc_html( $tag->slug);  ?>"><?php echo esc_html( $tag->name ); ?></option>
					<?php endforeach; ?>
					</select>
				<?php endif; ?>
				<input name="s" id="s" type="text" class="serach_arc_text" placeholder="フリーワード検索" />
				<input id="submit" class="search_submit" type="submit" value="検索する" />
			</form>
		</div>
		</div>

		<div class="post_box pc_min inner">
		<div class="pc_inner">
			<header class="page-header">
				<?php
					the_archive_title( '<h2 class="title">ARTICLES<span>記事一覧(', ')</span></h2>' );
				?>
			</header><!-- .page-header -->

			<ul class="post_lists">
				<?php while (have_posts()) : the_post(); ?>
				<li class="post_lists_li <?php echo "cnt_". ++$GLOBALS["cnt"]; if($GLOBALS["cnt"] > 0) echo " second"; ?>">
					<a href="<?php the_permalink(); ?>">
						<?php if( $GLOBALS["cnt"] < 3 ) : ?><span class="new_mark new_post">NEW</span><?php endif; ?>
						<div class="post_lists_img" style="background-image:url(<?php the_post_thumbnail_url( 'full' ); ?>)"></div>
					</a>
					<div class="post_lists_text">
						<p class="date gray"><a href="/category/<?php $cat = get_the_category(); $cat = $cat[0]; {echo $cat->slug;} ?>" class="post_lists_cat"><?php $cat = get_the_category(); $cat = $cat[0]; {echo $cat->cat_name;} ?></a><span class="post_lists_sp">　|　</span><?php echo the_modified_time('Y.m.d D') ?></p>
						<a href="<?php the_permalink(); ?>">
							<h3 class="post_li_title"><?php the_title(); ?></h3>
							<p class="post_p"><?php echo get_the_excerpt(); ?></p>
						</a>
						<div class="post_lists_tag">
						<?php the_tags('', ' '); ?>
						</div>
					</div>
				</li>
			   <?php endwhile; ?>
			</ul>
		</div>
		</div>
		
		<div class="page_navi_wrap page_navi_bottom_wrap">
		<div class="page_navi page_navi_bottom">
			<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
			<img src="<?php bloginfo( 'template_url' ); ?>/img/pagetop02.png" class="pagetop02" />
		</div>
		</div>
		
			<?php else :
			get_template_part( 'content', 'none' );

		endif;
		?>

		</div>
		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php get_footer(); ?>
