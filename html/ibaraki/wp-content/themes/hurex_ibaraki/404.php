<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
header( "location: " . home_url() );
header("HTTP/1.1 301 Moved Permanently");
get_header(); ?>
	<div class="search_arc_wrap bg_sub06">
	<div class="search_arc inner">
		<span class="search_arc_btn more_btn">記事を絞り込む</span>
		<form method="get" action="<?php bloginfo( 'url' ); ?>" class="serach_arc_box">
			<?php wp_dropdown_categories('depth=0&orderby=name&hide_empty=1&show_option_all=カテゴリーを選択'); ?>
			<?php $tags = get_tags(); if ( $tags ) : ?>
				<select name='tag' id='tag' class="postform">
				<option value="" selected="selected">タグを選択</option>
				<?php foreach ( $tags as $tag ): ?>
				<option value="<?php echo esc_html( $tag->slug);  ?>"><?php echo esc_html( $tag->name ); ?></option>
				<?php endforeach; ?>
				</select>
			<?php endif; ?>
			<input name="s" id="s" type="text" class="serach_arc_text" placeholder="フリーワード検索" />
			<input id="submit" type="submit" class="search_submit" value="検索する" />
		</form>
	</div>
	</div>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="bg_sub02">
		<div class="post_box inner">
			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'twentyfifteen' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location', 'twentyfifteen' ); ?></p>

					<?php //get_search_form(); ?>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->
		</div>
		</div>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
