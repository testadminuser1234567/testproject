jQuery(document).ready( function() {
	
	var headerHeight = $('.site-header').outerHeight();
	var urlHash = location.hash;
	if(urlHash) {
		$('body,html').stop().scrollTop(0);
		setTimeout(function(){
			var target = $(urlHash);
			var position = target.offset().top - headerHeight;
			$('body,html').stop().animate({scrollTop:position}, 500);
		}, 100);
	}
	$('a[href^="#"]').click(function() {
		var href= $(this).attr("href");
		var target = $(href);
		var position = target.offset().top - headerHeight;
		$('body,html').stop().animate({scrollTop:position}, 500);   
	});
	
	//PCサイドバー
	if(jQuery(window).width()>768){
		hsize = $('.sidebar_wrap').height() + 200;
		jQuery('.pc_min').css('min-height', hsize + 'px');
	 }else if( jQuery(window).width()<768 ){
		jQuery('.pc_min').css('min-height', 'auto');
		 //メインメニュー
		jQuery('.menu,.overray').on('click',function(){
			jQuery('.menu').toggleClass('active');
			if(jQuery('.menu').hasClass('active')){
				jQuery('.drawer-nav').animate({'right':'0'},100);
				jQuery('body').css({'overflow':'hidden'});
				jQuery('.overray').css({'display':'block'});
			}else{
				jQuery('.drawer-nav').animate({'right':'-320px'},100);
				jQuery('body').css({'overflow':'auto'});
				jQuery('.overray').css({'display':'none'});
			}
		});
	}
	
	var $setElem = $('img'),
	pcName = '_pc',
	spName = '_sp',
	replaceWidth = 770;

	$setElem.each(function(){
		var $this = $(this);
		function imgSize(){
			var windowWidth = parseInt($(window).width());
			if(windowWidth >= replaceWidth) {
				$this.attr('src',$this.attr('src').replace('_sp', '_pc'));
			} else if(windowWidth < replaceWidth) {
				$this.attr('src',$this.attr('src').replace('_pc', '_sp'));
			}
		}
		$(window).resize(function(){imgSize();});
		imgSize();
   });
	
	//転職ノウハウメニュー
	jQuery('.know_menu_btn,.know_menu_close').click(function(){
		jQuery('.know_menu_btn').toggleClass('active');
		if(jQuery('.know_menu_btn').hasClass('active')){
			jQuery('.know_menu_wrap').velocity("slideDown", {duration: 300});
		}else{
			jQuery('.know_menu_wrap').velocity("slideUp", {duration: 300});
		}
	});
	
	jQuery(".know_cat_lists > li").each(function(i){
		var childElement = jQuery(this).children();
		if (childElement.length > 1) { 
			jQuery(childElement[0]).addClass("cat_btn");
			jQuery(this).find(".cat_btn").click(function(){
				jQuery(this).toggleClass('active');
				var ulTag = jQuery(this).next();
				ulTag.slideToggle('fast');
				return false;
			});
		}
	});
	
	//検索詳細
	jQuery('.search_arc_btn').click(function(){
		jQuery(this).toggleClass('active');
		if(jQuery(this).hasClass('active')){
			jQuery('.serach_arc_box').velocity("slideDown", {duration: 300});
		}else{
			jQuery('.serach_arc_box').velocity("slideUp", {duration: 300});
		}
	});
	
	//求人検索
	jQuery('.search_job_btn').click(function(){
		jQuery(this).toggleClass('active');
		if(jQuery(this).hasClass('active')){
			jQuery('.serach_job_box').velocity("slideDown", {duration: 300});
		}else{
			jQuery('.serach_job_box').velocity("slideUp", {duration: 300});
		}
	});
	
	//pagetop
	jQuery('.pagetop,.pagetop02').click(function(){
		jQuery('html,body').stop(true,false).velocity("scroll", { duration: 300 });
	});
});
$(window).load(function() {
	
	var ua = navigator.userAgent;
	var headerHeight = $('.site-header').outerHeight();
    //iOSのみ対応
    if(/iPhone/.test(ua) || /iPad/.test(ua)) {
        //ページをまたいだページ内リンクのハッシュをクエリパラメータに変換
        $('a[href*=#]').each(function(){
            var orgHref = $(this).attr('href');
            var index = orgHref.lastIndexOf("#");
            //通常のページ内リンクは対象外とする
            if(index != 0) {
                var hash = orgHref.substr(index).substr(1);
                var href = orgHref.substr(0,index);
                if(href.match(/\?/)) {
                    var connector = '&';
                } else {
                    var connector = '?';
                }
                $(this).attr('href',href + connector + 'js-hash=' + hash);
            }
        });
        //クエリパラメータに[js-hash]が存在したときの処理
        if(getQueryVariable('js-hash')) {
            var target = $('#' + getQueryVariable('js-hash'));
            offsetTop = target.offset().top - headerHeight;
            $('html, body').animate({scrollTop:offsetTop}, 500);
        }
    }

    //指定したクエリパラメータの値を返す
    function getQueryVariable(variable) {
      var query = window.location.search.substring(1);
      var vars = query.split("&");
      for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
          return pair[1];
        }
      }
        return(false);
    }
});
jQuery(window).resize(function () {
	//PCサイドバー
	if(jQuery(window).width()>768){
		hsize = $('.sidebar_wrap').height() + 200;
		jQuery('.pc_min').css('min-height', hsize + 'px');
	 }else if( jQuery(window).width()<768 ){
		jQuery('.pc_min').css('min-height', 'auto');
	}
});