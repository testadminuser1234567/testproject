jQuery(document).ready( function() {
	//メインメニュー
	jQuery('.menu').click(function(){
		jQuery('.menu').addClass('active');
		jQuery('.drawer-nav').animate({'right':'0'},100);
		jQuery('body').css({'overflow':'hidden'});
		jQuery('.overray').css({'display':'block'});
		jQuery('.menu_close').css({'display':'block'});
	});
	jQuery('.menu_close,.overray,.menu_lists a').click(function(){
		jQuery('.menu').removeClass('active');
		jQuery('.drawer-nav').animate({'right':'-320px'},100);
		jQuery('body').css({'overflow':'auto'});
		jQuery('.overray').css({'display':'none'});
		jQuery('.menu_close').css({'display':'none'});
	});
	return false;
});