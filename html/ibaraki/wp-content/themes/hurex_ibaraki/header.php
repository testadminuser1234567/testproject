<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width">
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
	<!-- End Google Tag Manager -->
	<?php $custom_fields = get_post_meta( $post->ID , 'ディスクリプション' , true ); if(empty( $custom_fields ) === false): ?>
    <meta name="description"  content="<?php echo $custom_fields; ?>" />
	<?php endif; ?>
	<link href="<?php bloginfo( 'template_url' ); ?>/img/favicon.ico" rel="shortcut icon" />
	<?php if(is_page('faq')) : ?><link rel="canonical" href="https://www.hurex.jp/questions/"><?php endif; ?>
	
<title><?php if(is_home()): ?><?php bloginfo('name'); ?><?php elseif(is_page('search-job')): ?>
<?php if($_GET['pref'] == true){
	if($_GET['pref'] == '82'){
		echo('茨城県');
	}else{ 
		echo('全国');
	}
}
if($_GET['year_income'] == true){
	if($_GET['pref'] == false) echo '年収';
	echo $_GET['year_income'].'万円以上';
}
if($_GET['job'] == true){
	$url = $_SERVER['REQUEST_URI'];
	if(strstr($url,'10753')) echo '営業';
	if(strstr($url,'10775')) echo '経理･財務';
	if(strstr($url,'10773')) echo '法務･人事･総務･広報';
	if(strstr($url,'10777')) echo '一般事務';
	if(strstr($url,'10768')) echo '経営･管理職･企画･マーケティング';
	if(strstr($url,'10734')) echo '技術職(電気･電子･機械)';
	if(strstr($url,'10779')) echo '技術職(医療･化学･食品)';
	if(strstr($url,'10749')) echo '技術職(建築･土木･その他)';
	if(strstr($url,'10716')) echo 'ITエンジニア(システム開発･インフラ･SE)';
	if(strstr($url,'10727')) echo 'Web関連職･メディア･ゲーム･デザイン';
	if(strstr($url,'10770')) echo '専門職(士業･金融･不動産･コンサルタント)';
	if(strstr($url,'10760')) echo 'サービス･外食･販売';
	if(strstr($url,'10771')) echo '教育･保育･物流･購買･その他';
} 
if(($_GET['job'] == true && !empty ($_GET['job'][0])) || $_GET['pref'] == true || $_GET['year_income'] == true) echo 'の'; ?>求人一覧 ｜ <?php bloginfo('name'); ?>
<?php elseif(is_single()): ?>
<?php wp_title(''); ?> ｜ <?php bloginfo('name'); ?>
<?php elseif(is_category()): ?>
<?php single_cat_title() ?>の記事一覧 ｜ <?php bloginfo('name'); ?>
<?php elseif(is_search()): ?>
検索結果 ｜ <?php bloginfo('name'); ?>
<?php else: ?>
<?php bloginfo('name'); ?>
<?php endif; ?></title>

	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script> 
	<![endif]-->
<?php wp_head(); ?>
	<script src="//cdn.jsdelivr.net/velocity/1.2.2/velocity.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,500|Oswald:600,700" rel="stylesheet">
	<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.smoothScroll.js?v2"></script>
	<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/common.js?v=v2"></script>
	<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/top.js"></script>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); echo '?' . filemtime( get_stylesheet_directory() . '/style.css'); ?>" type="text/css" />
	
	<!-- OGP -->
	<meta property="og:type" content="blog">
	<?php
	if (is_single()){
	if(have_posts()): while(have_posts()): the_post();
	//echo '<meta property="og:description" content="'.get_post_meta($post->ID, _aioseop_description, true).'">';echo "\n";
	endwhile; endif;
	echo '<meta property="og:title" content="'; the_title(); echo '">';echo "\n";
	echo '<meta property="og:url" content="'; the_permalink(); echo '">';echo "\n";//
	} else {
	//echo '<meta property="og:description" content="'; get_the_excerpt(); echo '">';echo "\n";
	echo '<meta property="og:title" content="'; bloginfo('name'); echo '">';echo "\n";
	echo '<meta property="og:url" content="'; bloginfo('url'); echo '">';echo "\n";
	}
	$str = $post->post_content;
	$searchPattern = '/<img.*?src=(["\'])(.+?)\1.*?>/i';
	if (is_single()){
	if (has_post_thumbnail()){
	$image_id = get_post_thumbnail_id();
	$image = wp_get_attachment_image_src( $image_id, 'full');
	echo '<meta property="og:image" content="'.$image[0].'">';echo "\n";
	} else if ( preg_match( $searchPattern, $str, $imgurl ) && !is_archive()) {
	echo '<meta property="og:image" content="'.$imgurl[2].'">';echo "\n";
	} else {
	echo '<meta property="og:image" content="'; bloginfo('template_url'); echo '/img/ogp.jpg">';echo "\n";
	}
	} else {
	echo '<meta property="og:image" content="'; bloginfo('template_url'); echo '/img/ogp.jpg">';echo "\n";
	}
	?>
	<?php if(is_single()): ?>
	<meta property="og:description" content="<?php echo post_custom('ディスクリプション'); ?>" />
	<?php endif; ?>
	<meta property="og:site_name" content="<?php bloginfo('name'); ?>">
	<!-- OGP -->
</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="hfeed site">

	<header id="masthead" class="site-header" role="banner">
		<div class="h_wrap">
			<h1 class="site_title pc"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo( 'template_url' ); ?>/img/logo_pc.png" class="site_title_img" alt="<?php bloginfo( 'name' ); ?>"></a></h1>
			<div class="drawer_header_wrap">
			<div class="drawer_header">
				<h1 class="site_title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php bloginfo( 'template_url' ); ?>/img/logo_sp.png" class="site_title_img" alt="<?php bloginfo( 'name' ); ?>"></a></h1>
				<ul class="menu_btn sp">
					<li class="menu_btn_li"><a href="https://www.hurex.jp/mypage/signup/regist?argument=vgmWMMby&dmai=ibaraki_t_apply" target="_blank" class="top01-globalNavi"><img src="<?php bloginfo( 'template_url' ); ?>/img/h_btn_pc.png" alt="無料転職支援に申し込む" /></a></li>
					<li class="menu_btn_li"><button type="button" class="drawer-toggle menu"></button><button type="button" class="menu_close sp"></button></li>
				</ul>
			</div>
			</div>
		</div>
		<div class="overray"></div>
		<nav class="drawer-nav nav">
			<ul class="menu_lists">
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>search-job?pref=82">求人検索<span>SEARCH</span></a></li>
				<li><a href="<?php if(is_front_page()) : ?>#about<?php else: ?><?php echo esc_url( home_url( '/' ) ); ?>#about<?php endif; ?>">サービス内容<span>SERVICE</span></a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>consultants">コンサルタント紹介<span>CONSULTANTS</span></a></li>
				<li class="sp"><a href="<?php echo esc_url( home_url( '/' ) ); ?>knowhow">転職ノウハウ・コラム<span>KNOW-HOW</span></a></li>
				<li class="sp"><a href="<?php echo esc_url( home_url( '/' ) ); ?>voice">転職成功者の声<span>VOICE</span></a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>tomorrow">いつか転職したい方<span>FOR YOUR TOMORROW</span></a></li>
				<li><a href="<?php echo esc_url( home_url( '/' ) ); ?>faq">よくあるご質問<span>FAQ</span></a></li>
			</ul>
			<ul class="h_btn">
				<li><img src="<?php bloginfo( 'template_url' ); ?>/img/h_tel.png" alt="無料転職支援サービス,受付 9:00～21:00,フリーダイヤル 0120-14-1150" /></li>
				<li><a href="https://www.hurex.jp/mypage/signup/regist?argument=vgmWMMby&dmai=ibaraki_t_apply" target="_blank" class="top01-globalNavi"><img src="<?php bloginfo( 'template_url' ); ?>/img/h_btn_pc.png" alt="無料転職支援に申し込む" /></a></li>
			</ul>
			<div class="add sp">
				<strong>電話でお問い合わせ</strong><br/>
				<a href="tel:0120141150"><img src="/ibaraki/wp-content/themes/hurex_ibaraki/img/icon_tel.png" class="icon_tel" alt="無料転職支援サービス,受付 9:00～21:00,フリーダイヤル 0120-14-1150"> <span class="tel">0120-14-1150</span></a><br/><span class="small">受付 9:00～21:00</span>
			</div>
		</nav>
	</header><!-- .site-header -->

	<div id="content" class="site-content drawer-contents">
