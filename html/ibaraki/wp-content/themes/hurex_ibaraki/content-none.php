<?php
/**
 * The template part for displaying a message that posts cannot be found
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
		
<div class="search_arc_wrap search_none bg_sub06">
<div class="search_arc inner">
	<span class="search_arc_btn more_btn">記事を絞り込む</span>
	<form method="get" action="<?php bloginfo( 'url' ); ?>" class="serach_arc_box">
		<?php wp_dropdown_categories('depth=0&orderby=name&hide_empty=1&show_option_all=カテゴリーを選択'); ?>
		<?php $tags = get_tags(); if ( $tags ) : ?>
			<select name='tag' id='tag' class="postform">
			<option value="" selected="selected">タグを選択</option>
			<?php foreach ( $tags as $tag ): ?>
			<option value="<?php echo esc_html( $tag->slug);  ?>"><?php echo esc_html( $tag->name ); ?></option>
			<?php endforeach; ?>
			</select>
		<?php endif; ?>
		<input name="s" id="s" type="text" class="serach_arc_text" placeholder="フリーワード検索" />
		<input id="submit" type="submit" class="search_submit" value="検索する" />
	</form>
</div>
</div>

<section class="no-results pc_min not-found">
<div class="post_box inner">
	<div class="pc_inner">
	<header class="page-header">
		<h1 class="page-title"><?php _e( 'Nothing Found', 'twentyfifteen' ); ?></h1>
	</header><!-- .page-header -->

	<div class="page-content">

		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'twentyfifteen' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'twentyfifteen' ); ?></p>
			<?php //get_search_form(); ?>

		<?php else : ?>

			<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'twentyfifteen' ); ?></p>
			<?php //get_search_form(); ?>

		<?php endif; ?>

	</div><!-- .page-content -->
	</div>
</div>
</section><!-- .no-results -->
