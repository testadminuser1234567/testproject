<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<?php
$cat_now = get_the_category();
$cat_now = $cat_now[0];
$cat_name = $cat_now->name;
$slug = $cat_now->name;
$GLOBALS["cnt"] = 0;
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="pc_min">
	<div class="single_post_img_wrap">
	<div class="post_lists_img single_post_lists_img" style="background-image:url(<?php the_post_thumbnail_url( 'full' ); ?>)"></div>
	</div>

	<div class="entry_wrap inner">
	<div class="pc_inner">
		<header class="entry_header">
			<p class="date gray"><a href="/category/<?php $cat = get_the_category(); $cat = $cat[0]; {echo $cat->slug;} ?>" class="post_lists_cat"><?php $cat = get_the_category(); $cat = $cat[0]; {echo $cat->cat_name;} ?></a>　|　<?php echo the_modified_time('Y.m.d D') ?></p>
			<?php
				if ( is_single() ) :
					the_title( '<h1 class="entry-title">', '</h1>' );
				else :
					the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
				endif;
			?>
		</header><!-- .entry-header -->

		<div class="share_btn pc tab">
			<ul class="share_lists">
				<li class="share_lists_li"><a href="http://www.facebook.com/share.php?u=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="share_fb share_a" target="_blank">Facebook</a></li>
				<li class="share_lists_li"><a href="http://twitter.com/share?url=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>&text=<?php echo urlencode(the_title("")); ?>&related=hurex_hurex" class="share_tw share_a" target="_blank">Twitter</a></li>
				<li class="share_lists_li"><a href="http://line.me/R/msg/text/?<?php the_title(); ?>%0D%0A<?php the_permalink(); ?>" rel="nofollow" class="share_li share_a" target="_blank">LINE</a></li>
			</ul>
		</div>


		<?php if(get_post_meta($post->ID,'アドバイザー',true) == '神谷貴宏'): ?>
		<div class="author_box pc tab">
		<div class="author-info author01">
			<div class="author-avatar w_kamiya"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">神谷 貴宏</h3>

				<p class="author-bio">
					3,000人以上の転職をサポートしてきたベテランコンサルタント。大手証券会社、半導体商社、総合人材サービス会社を経て転職支援会社へ入社。「心から満足できる出会い」をモットーに、日々業務に取り組んでいる。趣味はゴルフ。
				</p>
			</div>
			<a href="https://www.hurex.jp/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '須賀川敏哉'): ?>
		<div class="author_box pc tab">
		<div class="author-info author02">
			<div class="author-avatar w_sukagawa"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">須賀川 敏哉</h3>

				<p class="author-bio">
					大手証券会社、総合人材サービス会社を経て転職支援会社へ。人材業界で17年以上のキャリアを持つプロフェッショナル。幅広い職種・年齢に対応できるのが強みで、東北で数多くの転職成功実績を築いている。趣味はテニス、ドライブ。
				</p>
			</div>
			<a href="https://www.hurex.jp/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '浅野有史'): ?>
		<div class="author_box pc tab">
		<div class="author-info author03">
			<div class="author-avatar w_asano"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">浅野 有史</h3>

				<p class="author-bio">
					大手証券会社、地域金融機関、ベンチャーキャピタルを経て転職支援会社へ。宮城の製造業が集まる工業会の人材ネットワークを設立し、製造業と求職者をつなげるシステムを構築。親切丁寧なサービスを常に心がけている。趣味は食べ歩き。
				</p>
			</div>
			<a href="https://www.hurex.jp/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '千葉公介'): ?>
		<div class="author_box pc tab">
		<div class="author-info author04">
			<div class="author-avatar w_chiba"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">千葉 公介</h3>

				<p class="author-bio">
					首都圏のベンチャー証券会社、投資会社でヒト・モノ・カネの流れや企業経営について学ぶ。「生まれ故郷である東北の活性化に貢献したい」という想いをきっかけに、東北の転職支援会社に入社。趣味は車（特にF1）。
				</p>
			</div>
			<a href="https://www.hurex.jp/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '山上公平'): ?>
		<div class="author_box pc tab">
		<div class="author-info author05">
			<div class="author-avatar w_yamagami"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">山上 公平</h3>

				<p class="author-bio">
					インターネット求人広告会社で、中途採用支援や医療系の人材紹介事業に従事。人材紹介業の経験を活かし、転職支援会社へ入社する。転職希望者と共に悩み、共に考えるサポートを心がけている。趣味はゴルフと農作業。
				</p>
			</div>
			<a href="https://www.hurex.jp/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '大阪大祐'||get_post_meta($post->ID,'アドバイザー',true) == '大阪太祐'): ?>
		<div class="author_box pc tab">
		<div class="author-info author06">
			<div class="author-avatar w_osaka"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">大阪 太祐</h3>

				<p class="author-bio">
					東京の大手不動産でリテール営業を経験。30歳を節目に故郷の東北へUターンし、地元の転職支援会社へ入社。主に20～30代の若い人材のキャリアカウンセリングや、転職活動サポートを担当している。趣味はライブ鑑賞とスノーボード。
				</p>
			</div>
			<a href="https://www.hurex.jp/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '山崎伸也'): ?>
		<div class="author_box pc tab">
		<div class="author-info author07">
			<div class="author-avatar w_yamazaki"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">山崎 伸也</h3>

				<p class="author-bio">
					大手小売店業の店長、SVを経験し、その後は企画営業としてチェーン全体のオペレーション構築と社員教育を担当。現在は転職支援会社のコンサルタントとして、多くの転職相談に対応している。趣味は料理。
				</p>
			</div>
			<a href="https://www.hurex.jp/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '大西理'): ?>
		<div class="author_box pc tab">
		<div class="author-info author08">
			<div class="author-avatar w_onishi"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">大西 理</h3>

				<p class="author-bio">
					医療系商社で基幹病院を担当し、医師や経営・管理部門、各メーカーとの関係構築に努める。東北に貢献したいと考え、地元の人材紹介業へキャリアチェンジ。日夜、転職希望者と企業の最高の出会いを追及している。趣味はボルダリングとダーツ。
				</p>
			</div>
			<a href="https://www.hurex.jp/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '丸亀賢大'): ?>
		<div class="author_box pc tab">
		<div class="author-info author09">
			<div class="author-avatar w_marugame"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">丸亀 賢大</h3>

				<p class="author-bio">
					個別指導塾のスクールマネージャー、IT・Web業界に強い人材紹介会社でコンサルタントリーダーを経験。転職支援を通じて、東北を活性化させることが自身のミッションと考え日々業務に取り組んでいる。趣味は野球観戦。
				</p>
			</div>
			<a href="https://www.hurex.jp/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '高野翼'): ?>
		<div class="author_box pc tab">
		<div class="author-info author10">
			<div class="author-avatar w_takano"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">高野 翼</h3>

				<p class="author-bio">
					東京のTV番組制作会社、インターネットメディアといったマスコミ業界で研鑽を積む。その後、東北へのUターンをきっかけに人材紹介業へキャリアチェンジ。「心から満足できる出会い」を提供するため日々奮闘している。趣味は野球、サッカー。
				</p>
			</div>
			<a href="https://www.hurex.jp/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '坂本理沙'): ?>
		<div class="author_box pc tab">
		<div class="author-info author12">
			<div class="author-avatar w_sakamoto"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">坂本 理沙</h3>

				<p class="author-bio">
					金融、広告業で営業を経験。求人広告の営業として働く中で「採用側だけでなく、求職者の声も聞いた上で、双方を結びたい」と考えるようになり、転職支援会社へ入社。転職希望者を「より輝けるフィールドへ」導くべく、研鑽を積む。趣味はウィンタースポーツと登山。
				</p>
			</div>
			<a href="https://www.hurex.jp/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '菊池花澄美'): ?>
		<div class="author_box pc tab">
		<div class="author-info author12">
			<div class="author-avatar w_kikuchi"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">菊池 花澄美</h3>

				<p class="author-bio">
					前職は服飾デザイン会社で勤務。採用支援・転職支援を通じて、自身の故郷である東北を守りたいという想いから人材紹介業へ大きくキャリアチェンジ。現在は、転職活動で困っている求職者に寄り添ったサポートを心がけ、日々精進している。趣味は音楽。
				</p>
			</div>
			<a href="https://www.hurex.jp/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=ibaraki_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php endif; ?>
		
		<div class="app_box_wrap bg_sub04 app_box_content sp tab">
			<a href="https://www.hurex.jp/mypage/signup/regist?argument=vgmWMMby&dmai=ibaraki_t_apply" target="_blank" class="more_btn app_more_btn"><span>茨城に強い転職エージェントに</span><br/>転職支援を申し込む</a>
		</div>
		
		<script type="text/javascript">
			function openWindowAndPost(event){
				window.open('','formpost','width=600,height=400,scrollbars=yes');
				var form = event.parentNode;
				//alert(form.className);
				form.action = 'https://a06.hm-f.jp/index.php';
				form.target = 'formpost';
				form.method = 'post';
				form.submit();
			}
		</script>
		
		<div class="entry_content">
			<?php
				/* translators: %s: Name of current post */
				the_content( sprintf(
					__( 'Continue reading %s', 'twentyfifteen' ),
					the_title( '<span class="screen-reader-text">', '</span>', false )
				) );

				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				) );
			?>
		</div><!-- .entry-content -->
		
		<?php /*<div class="mail_box">
			<h3><span>転職に役立つノウハウ&<br class="sp"/>お役立ち情報を配信中！</span></h3>
			<form action="https://a06.hm-f.jp/index.php" method="post">
				<input type="hidden" name="charset" value="UTF-8" />
				<input type="hidden" name="action" value="R2" />
				<input type="hidden" name="a" value="741" />
				<input type="hidden" name="f" value="1587" />
				<input type="hidden" name="g" value="793" />
				<input type="text" name="mailaddr" class="add" value="" placeholder="例:sample@docomo.ne.jp" />
				<input type="submit" class="submit" value="購読する" onclick="openWindowAndPost(this);return false;" />
			</form>
		</div>*/ ?>

		<div class="single_tag">
		<?php //the_tags('', ' '); ?>
			<?php $tags = terms_orderby_term_order();if ($tags) {
				foreach($tags as $tag) {
					$tag_link = sprintf('<a class="tag-cloud-link" href="/tag/%s" rel="tag">%s</a>',$tag->slug,$tag->name);
					echo $tag_link." ";
				}
			}?>
		</div>
		
		<div class="share_btn sp tab">
			<ul class="share_lists">
				<li class="share_lists_li"><a href="http://www.facebook.com/share.php?u=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="share_fb share_a" target="_blank">Facebook</a></li>
				<li class="share_lists_li"><a href="http://twitter.com/share?url=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>&text=<?php echo urlencode(the_title("")); ?>&related=hurex_hurex" class="share_tw share_a" target="_blank">Twitter</a></li>
				<li class="share_lists_li"><a href="http://line.me/R/msg/text/?<?php the_title(); ?>%0D%0A<?php the_permalink(); ?>" rel="nofollow" class="share_li share_a" target="_blank">LINE</a></li>
			</ul>
		</div>
		
		<?php if(get_post_meta($post->ID,'アドバイザー',true) == '神谷貴宏'): ?>
		<div class="author_box sp tab">
		<div class="author-info author01">
			<div class="author-avatar w_kamiya"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">神谷 貴宏</h3>

				<p class="author-bio">
					3,000人以上の転職をサポートしてきたベテランコンサルタント。大手証券会社、半導体商社、総合人材サービス会社を経て転職支援会社へ入社。「心から満足できる出会い」をモットーに、日々業務に取り組んでいる。趣味はゴルフ。
				</p>
			</div>
			<a href="https://www.hurex.jp/smart/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '須賀川敏哉'): ?>
		<div class="author_box sp tab">
		<div class="author-info author02">
			<div class="author-avatar w_sukagawa"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">須賀川 敏哉</h3>

				<p class="author-bio">
					大手証券会社、総合人材サービス会社を経て転職支援会社へ。人材業界で17年以上のキャリアを持つプロフェッショナル。幅広い職種・年齢に対応できるのが強みで、東北で数多くの転職成功実績を築いている。趣味はテニス、ドライブ。
				</p>
			</div>
			<a href="https://www.hurex.jp/smart/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '浅野有史'): ?>
		<div class="author_box sp tab">
		<div class="author-info author03">
			<div class="author-avatar w_asano"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">浅野 有史</h3>

				<p class="author-bio">
					大手証券会社、地域金融機関、ベンチャーキャピタルを経て転職支援会社へ。宮城の製造業が集まる工業会の人材ネットワークを設立し、製造業と求職者をつなげるシステムを構築。親切丁寧なサービスを常に心がけている。趣味は食べ歩き。
				</p>
			</div>
			<a href="https://www.hurex.jp/smart/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '千葉公介'): ?>
		<div class="author_box sp tab">
		<div class="author-info author04">
			<div class="author-avatar w_chiba"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">千葉 公介</h3>

				<p class="author-bio">
					首都圏のベンチャー証券会社、投資会社でヒト・モノ・カネの流れや企業経営について学ぶ。「生まれ故郷である東北の活性化に貢献したい」という想いをきっかけに、東北の転職支援会社に入社。趣味は車（特にF1）。
				</p>
			</div>
			<a href="https://www.hurex.jp/smart/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '山上公平'): ?>
		<div class="author_box sp tab">
		<div class="author-info author05">
			<div class="author-avatar w_yamagami"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">山上 公平</h3>

				<p class="author-bio">
					インターネット求人広告会社で、中途採用支援や医療系の人材紹介事業に従事。人材紹介業の経験を活かし、転職支援会社へ入社する。転職希望者と共に悩み、共に考えるサポートを心がけている。趣味はゴルフと農作業。
				</p>
			</div>
			<a href="https://www.hurex.jp/smart/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '大阪大祐'||get_post_meta($post->ID,'アドバイザー',true) == '大阪太祐'): ?>
		<div class="author_box sp tab">
		<div class="author-info author06">
			<div class="author-avatar w_osaka"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">大阪 太祐</h3>

				<p class="author-bio">
					東京の大手不動産でリテール営業を経験。30歳を節目に故郷の東北へUターンし、地元の転職支援会社へ入社。主に20～30代の若い人材のキャリアカウンセリングや、転職活動サポートを担当している。趣味はライブ鑑賞とスノーボード。
				</p>
			</div>
			<a href="https://www.hurex.jp/smart/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '山崎伸也'): ?>
		<div class="author_box sp tab">
		<div class="author-info author07">
			<div class="author-avatar w_yamazaki"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">山崎 伸也</h3>

				<p class="author-bio">
					大手小売店業の店長、SVを経験し、その後は企画営業としてチェーン全体のオペレーション構築と社員教育を担当。現在は転職支援会社のコンサルタントとして、多くの転職相談に対応している。趣味は料理。
				</p>
			</div>
			<a href="https://www.hurex.jp/smart/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '大西理'): ?>
		<div class="author_box sp tab">
		<div class="author-info author08">
			<div class="author-avatar w_onishi"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">大西 理</h3>

				<p class="author-bio">
					医療系商社で基幹病院を担当し、医師や経営・管理部門、各メーカーとの関係構築に努める。東北に貢献したいと考え、地元の人材紹介業へキャリアチェンジ。日夜、転職希望者と企業の最高の出会いを追及している。趣味はボルダリングとダーツ。
				</p>
			</div>
			<a href="https://www.hurex.jp/smart/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '丸亀賢大'): ?>
		<div class="author_box sp tab">
		<div class="author-info author09">
			<div class="author-avatar w_marugame"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">丸亀 賢大</h3>

				<p class="author-bio">
					個別指導塾のスクールマネージャー、IT・Web業界に強い人材紹介会社でコンサルタントリーダーを経験。転職支援を通じて、東北を活性化させることが自身のミッションと考え日々業務に取り組んでいる。趣味は野球観戦。
				</p>
			</div>
			<a href="https://www.hurex.jp/smart/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '高野翼'): ?>
		<div class="author_box sp tab">
		<div class="author-info author10">
			<div class="author-avatar w_takano"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">高野 翼</h3>

				<p class="author-bio">
					東京のTV番組制作会社、インターネットメディアといったマスコミ業界で研鑽を積む。その後、東北へのUターンをきっかけに人材紹介業へキャリアチェンジ。「心から満足できる出会い」を提供するため日々奮闘している。趣味は野球、サッカー。
				</p>
			</div>
			<a href="https://www.hurex.jp/smart/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '坂本理沙'): ?>
		<div class="author_box sp tab">
		<div class="author-info author12">
			<div class="author-avatar w_sakamoto"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">坂本 理沙</h3>

				<p class="author-bio">
					金融、広告業で営業を経験。求人広告の営業として働く中で「採用側だけでなく、求職者の声も聞いた上で、双方を結びたい」と考えるようになり、転職支援会社へ入社。転職希望者を「より輝けるフィールドへ」導くべく、研鑽を積む。趣味はウィンタースポーツと登山。
				</p>
			</div>
			<a href="https://www.hurex.jp/smart/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php elseif(get_post_meta($post->ID,'アドバイザー',true) == '菊池花澄美'): ?>
		<div class="author_box sp tab">
		<div class="author-info author12">
			<div class="author-avatar w_kikuchi"></div>
			<div class="author-description">
				<h2 class="author-head">この記事を書いた人</h2>
				<p class="author-position">コンサルタント</p>
				<h3 class="author-title">菊池 花澄美</h3>

				<p class="author-bio">
					前職は服飾デザイン会社で勤務。採用支援・転職支援を通じて、自身の故郷である東北を守りたいという想いから人材紹介業へ大きくキャリアチェンジ。現在は、転職活動で困っている求職者に寄り添ったサポートを心がけ、日々精進している。趣味は音楽。
				</p>
			</div>
			<a href="https://www.hurex.jp/smart/service-introduction/consultants/?consultants&argument=vgmWMMby&dmai=sendai_t_consul&consultants" target="_blank" class="ditail">転職コンサルタントについて詳しく知る</a>
		</div>
		</div>
		<?php endif; ?>

		<?php if ( comments_open() || get_comments_number() ) : ?>
		<a href="#comment" class="more_btn line_more_btn">コメントを見る</a>
		<?php endif; // have_comments() ?>

		<div class="single_recruit_box bg_main">
			<h2 class="title">RECRUIT<span>おすすめ求人</span></h2>
			<?php $tags = terms_orderby_term_order();if ($tags) {
				$tags_name = "";
				foreach($tags as $tag) {
					if(!empty($tags_name)){
						$tags_name .= ' '.$tag->name;
					}else{
						$tags_name = $tag->name;
					}
				}
			}?>
			<?php
			require_once ( dirname(__FILE__).'/get_new_job.php' );
			$obj = new GetNewJob();
			//$obj->getNew(6,78,'"'.$tags_name.'"');
			$obj->getNew(12,82,$tags_name);
			?>
		</div>

		<div class="related_box bg_sub02">
			<h2 class="title">RELATED<span>関連記事</span></h2>
			<ul class="post_lists">	
			<?php
			   $post_id = get_the_ID();
				$GLOBALS["cnt"] = 0;
			   foreach((get_the_category()) as $cat) {
			   $cat_id = $cat->cat_ID ;
			   break ;
			   }
			   query_posts(
				  array(
				  'cat' => $cat_id,
				  'showposts' => 6,
				  'orderby' => rand,
				  'post__not_in' => array($post_id)
				  )
			   );
			?>
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<li class="post_lists_li second<?php echo " cid".$cat->cat_ID; ?> <?php echo "cnt_". ++$GLOBALS["cnt"]; if($GLOBALS["cnt"] > 2) echo " second"; ?>">
					<a href="<?php the_permalink(); ?>">
						<div class="post_lists_img" style="background-image:url(<?php the_post_thumbnail_url( 'full' ); ?>)"></div>
					</a>
					<div class="post_lists_text">
						<p class="date gray"><a href="/category/<?php $cat = get_the_category(); $cat = $cat[0]; {echo $cat->slug;} ?>" class="post_lists_cat"><?php $cat = get_the_category(); $cat = $cat[0]; {echo $cat->cat_name;} ?></a><span class="post_lists_sp">　|　</span><?php echo the_modified_time('Y.m.d D') ?></p>
						<a href="<?php the_permalink(); ?>">
							<h3 class="post_li_title"><?php the_title(); ?></h3>
							<p class="post_p"><?php echo get_the_excerpt(); ?></p>
						</a>
					</div>
				</li>
				<?php endwhile; ?>
			<?php endif; wp_reset_query(); ?>
			</ul>
		</div>

		<div class="share_btn">
			<ul class="share_lists">
				<li class="share_lists_li"><a href="http://www.facebook.com/share.php?u=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>" rel="nofollow" class="share_fb share_a" target="_blank">Facebook</a></li>
				<li class="share_lists_li"><a href="http://twitter.com/share?url=<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>&text=<?php echo urlencode(the_title("")); ?>&related=hurex_hurex" class="share_tw share_a" target="_blank">Twitter</a></li>
				<li class="share_lists_li"><a href="http://line.me/R/msg/text/?<?php the_title(); ?>%0D%0A<?php the_permalink(); ?>" rel="nofollow" class="share_li share_a" target="_blank">LINE</a></li>
			</ul>
		</div>
		<?php if ( comments_open() || get_comments_number() ) : ?>
		<div id="comment" class="comment_box bg_sub02">
			<h2 class="title">COMMENT<span>コメント</span></h2>
			<div class="comment_form">
			<?php comments_template(); ?>
			</div>
		</div>
		<?php endif; ?>
	</div>
	</div>
	</div>
</article><!-- #post-## -->
