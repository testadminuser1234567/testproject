<?php
require_once ( dirname(__FILE__).'/SearchJob.class.php' );
class GetNewJob {


	public function getNew($rows,$pref,$keywords="")
	{
		// 一覧データ取得
		// list => StdObj
		// rowmax
		// pagemax
		$count = 0;
		$obj = new SearchJob();
		$obj->doSearch($rows,$pref,"","",$keywords);
		$data = $obj->getLists();?>
		<ul class="recruit_lists">
 			<?php foreach ( $data as $key => $value ) { ?>
 			<?php $count++; ?>
 			 <li class="recruit_lists_li cid_<?php echo $count ?>">
 			 	<div class="recruit_lists_li_inner <?php $cats = get_the_category();foreach($cats as $cat):if($cat->parent) echo $cat->slug; endforeach;?><?php if( $active_flg == 1 ) echo ' pickup'; ?>">
				<p class="date">
				<?php if( $count < 3 ) : ?><span class="new_mark">NEW</span><?php endif; ?>
				<?php if( $active_flg == 1 ) : ?><span class="pickup_mark">注目</span><?php endif; ?>
				<?php echo $value->j_updated; ?> 更新</p>
				<h3 class="recruit_lists_title"><a href="https://www.hurex.jp/job-search/detail.html?id=<?php echo $value->job_id; ?>&job=" target="_blank"><?php echo $value->job_title; ?></a></h3>
				<table class="recruit_lists_table">
						<tr>
							<th class="recruit_lists_th">勤務地 : </th>
							<td class="recruit_lists_td"><span class="recruit_lists_p"><?php echo $value->prefecture_name; ?></span></td>
							<th class="recruit_lists_th">年収 : </th>
							<td class="recruit_lists_td"><span class="recruit_lists_p"><?php if( $value->minsalary > 400 ) echo $value->minsalary; ?><?php if( $value->maxsalary > 1 ) echo '～'.$value->maxsalary; ?>万</span></td>
						</tr>
						<tr>
							<th class="recruit_lists_th">雇用形態 : </th>
							<td class="recruit_lists_td" colspan="3"><span class="recruit_lists_p"><?php echo $value->employ_name; ?></span></th>
						</tr>
						<tr>
							<th class="recruit_lists_th">職種 : </th>
							<td class="recruit_lists_td" colspan="3"><span class="recruit_lists_p"><?php echo $value->jobcategory_name; ?></span></th>
						</tr>
					</table>
				<p class="recruit_lists_exp"><?php echo $value->qualification; ?></p>
				<a class="recruit_lists_btn" href="https://www.hurex.jp/job-search/detail.html?id=<?php echo $value->job_id; ?>&job=" target="_blank">詳細情報を見る</a>
			</div>
			<p class="p_offer"><span class="gray">提供 :</span> ヒューレックス株式会社</p>
 			</li>
 			<?php } ?>
 		</ul><?php
 	}
}