<?php
/**
 * The template for displaying comments
 *
 * The area of the page that contains both current comments
 * and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div class="comment_area">

	<?php if ( have_comments() ) : ?>
	<h2 class="comment_title">
		みんなのコメント<?php comments_number('(0)','(1)','(%)'); ?>
	</h2>

		<?php //twentyfifteen_comment_nav(); ?>

		<ol class="comment_list">
			<?php wp_list_comments('callback=mytheme_comment'); ?>
		</ol><!-- .comment-list -->

		<?php twentyfifteen_comment_nav(); ?>

	<?php endif; // have_comments() ?>

	<?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
		<p class="no-comments"><?php _e( 'Comments are closed.', 'twentyfifteen' ); ?></p>
	<?php endif; ?>

	<?php
	  $comments_args = array(
		'fields' => array(
		  'author' => '<input id="author" class="comment_author comment_input" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" placeholder="お名前"' . $aria_req . ' />',
		  'email'  => '<input id="email" class="comment_email comment_input" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr(  $commenter['comment_author_email'] ) . '" placeholder="メールアドレス"' . $aria_req . ' />',
		  'url'    => '',
		),
	  );

	  comment_form($comments_args);
	?>

</div><!-- .comments-area -->
