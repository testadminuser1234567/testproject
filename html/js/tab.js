$(function () {
    var tabContainer = $('div#searchPrefTab div.tabSub');
    tabContainer.hide().filter(':first').show();
    $('div#searchPrefTab ul#searchPrefTabNavi a').click(function () {
       tabContainer.hide();
       tabContainer.filter(this.hash).show();
       $('div#searchPrefTab ul#searchPrefTabNavi a').removeClass('selected');
       $(this).addClass('selected');
       return false;
    })
    .filter(':first').click();
});