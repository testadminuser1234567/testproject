$(function(){
	var fade = 1000;
	var time = 5000;
	var thumbfade = 10;
	var timer;
	var images = new Array();
	var thumb = $("#mainThumbnail ul li");
	var auto = 'on';
	var cnt=0;
	var max = 0;
	var opacity = 1;
	
	max = $(".mainImg").size();
	for(i=0;i<max;i++){
		images[i] = $(".mainImg").eq(i);
	}

	for(var i=1;i<max;i++){
		images[i].css({zIndex:'10',opacity:'0'});
	}
	images[0].css({zIndex:'20',opacitry:'0'});

	thumb.each(function(i){
		thumb.eq(i).css({opacity:'1'});
	});
	thumb.eq(0).css({opacity:'1'});
	thumb.eq(0).addClass('active');

	thumb.hover(function(){
		_idx = thumb.index(this);
		$(this).stop().animate({opacity:'1'},200);
	},function(){
		if(cnt != _idx) $(this).stop().animate({opacity:opacity},200);
	});

	thumb.click(function(){
		if(auto == 'on'){clearTimeout(timer);}

		var index = thumb.index(this);
		images[cnt].stop().animate({opacity:'0'},fade,function(){$(this).css({zIndex:'10'});});
		cnt = index;
		images[cnt].stop().animate({opacity:'1'},fade,function(){$(this).css({zIndex:'20'});});

		thumb.each(function(i){
			if(index == i){
				thumb.eq(i).css({opacity:'1'});
			}else{
				thumb.eq(i).css({opacity:opacity});
			}
		});

		$(this).addClass('active');
		$(this).siblings().removeClass('active');

		if(auto == 'on'){startTimer();}

	});

	function imageChange(){
		images[cnt].stop().animate({opacity:'0'},fade,function(){$(this).css({zIndex:'10'});});
		if(cnt == max-1){
			cnt=0;
		}else{
			cnt++;
		}

		thumb.each(function(i){
			if(cnt == i){
				thumb.eq(i).css({opacity:'1'});
				thumb.eq(i).addClass('active');
				thumb.eq(i).removeClass('active');
			}else{
				thumb.eq(i).css({opacity:opacity});
				thumb.eq(i).removeClass('active');
			}
		});

		images[cnt].stop().animate({opacity:'1'},fade,function(){
			$(this).css({zIndex:'20'});
			startTimer();
		});
	}

	if(auto == 'on'){
		function startTimer(){
			timer = setTimeout(function(){
				imageChange();
			},time);
		}
		startTimer();
	}

});
