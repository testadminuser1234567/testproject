<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="robots" content="noindex,nofollow">
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <link href="../css/import.css" rel="stylesheet" media="all">
    <title>HUREXマイページ</title>
    <script src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
    <script src="../js/jquery.page-scroller-308.js" charset="utf-8"></script>
    <script src="../js/rollover.js"></script>
    <script src="../js/common.js"></script>
    <script src="../js/manualNavi.js"></script>
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/css3-mediaqueries.js"></script>
    <![endif]-->
 <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6405327-10', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
<?php
$kanridir = "/mypage";
$baseUrl = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"]  . $kanridir ."/";

if( !defined("BaseDir") ){
	define("BaseDir",$baseDir);  
}

if( !defined("BaseUrl") ){
	define("BaseUrl",$baseUrl);  
}
?>
    <?php include($_SERVER["DOCUMENT_ROOT"] .'/mypage/include/header.php'); ?>
        <section class="co manual">
        <div class="inner">
            <h1><span class="line">ご利用マニュアル</span></h1>
            <!-- navi start -->
            <ul id="manualNavi" class="pc clearfix">
                <li><a href="index.php">メニューについて</a></li>
                <li><a href="p02.php">TOPページ</a></li>
                <li><a href="p03.php">求人検索</a></li>
                <li><a href="p04.php">気になる求人リスト</a></li>
                <li><a href="p05.php">希望条件保存</a></li>
                <li class="current"><a href="p06.php">応募状況</a></li>
                <li><a href="p07.php">プロフィール設定</a></li>
            </ul>
            <form name="sort_form" id="spMenu" class="sp">
                <select name="sort" onchange="dropsort()">
                    <option>▼お選び下さい</option>

                    <option value="index.php">メニューについて</option>
                    <option value="p02.php">TOPページ</option>
                    <option value="p03.php">求人検索</option>
                    <option value="p04.php">気になる求人リスト</option>
                    <option value="p05.php">希望条件保存</option>
                    <option value="p06.php">応募状況</option>
                    <option value="p07.php">プロフィール設定</option>
                </select>
            </form>
            <!-- navi end -->
            <h2>応募状況ページ</h2>
            <p>ご自身の応募された求人情報の一覧、及びその現状（フェーズ）を表示しています。</p>
            <p><img src="img/p06_img_01.jpg" width="100%" alt=""/></p>
            <ul class="list">
                <li class="na">[フェーズを絞り込む]を選択すると、そのフェーズに対応した求人情報が表示されます。<br>
                    <strong>【フェーズ一覧】</strong><br>
                ・求人詳細問い合わせ中：現在、問い合わせ中の求人情報を掲載します。<br>
                ・オファー：オファーを受けた求人情報を掲載します。<br>
                ・辞退：過去に辞退した求人情報を掲載します。<br>
                ・応募中：現在、応募中の求人情報を掲載します。<br>
                ・お見送り：過去にお見送りした求人情報を掲載します。<br>
                ・面接：面接の回答待ちの求人情報を掲載します。<br>
                ・内定：内定を受けた求人情報を掲載します。<br>
                ・入社予定：入社予定の求人情報を掲載します。<br>
                ・案件終了：フェーズが終了した求人情報を掲載します。</li>
                <li class="nb">表示されている求人情報について、あなたの現状（フェーズ）を表示しています。</li>
                <li class="nc">求人情報名をクリックすると詳細ページを閲覧することができます。</li>
            </ul>
            <h3>TOPページ応募状況との関連性</h3>
            <ol><li>TOPページで表示されるオファー（件数）は、フェーズ「オファー」を選択すると閲覧できます。</li>
                <li>TOPページで表示される応募（件数）は、フェーズ「応募中」を選択すると閲覧できます。</li>
            </ol>
            <h3>オファーを受けた求人について</h3>
            <p>オファーを受けた求人情報は、一覧ページ・詳細ページ共に下記ボタンが表示されます。</p>
            <p><img src="img/p06_img_02.jpg" width="100%" alt=""/></p>
            <ul class="point01">
                <li>[応募する] </li>
                <li>[辞退する]</li>
            </ul>
            <p>応募される場合は[応募する]をクリック、辞退される場合は[辞退する]をクリックしてください。</p>
            <!-- btnBox start -->
            <ul class="btnBox clearfix">
                <li class="prev"><a href="p05.php">前へ</a></li>
            <li class="close"><a href="javascript:;" onClick="window.close(); return false;">閉じる</a></li>
                <li class="next"><a href="p07.php">次へ</a></li>
            </ul>
            <!-- btnBox start -->
        </div>
    </section>
    <!-- co end -->
    <?php include($_SERVER["DOCUMENT_ROOT"] .'/mypage/include/footer.php'); ?>
</body>
</html>