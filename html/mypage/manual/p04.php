<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="robots" content="noindex,nofollow">
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <link href="../css/import.css" rel="stylesheet" media="all">
    <title>HUREXマイページ</title>
    <script src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
    <script src="../js/jquery.page-scroller-308.js" charset="utf-8"></script>
    <script src="../js/rollover.js"></script>
    <script src="../js/common.js"></script>
    <script src="../js/manualNavi.js"></script>
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/css3-mediaqueries.js"></script>
    <![endif]-->
 <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6405327-10', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
<?php
$kanridir = "/mypage";
$baseUrl = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"]  . $kanridir ."/";

if( !defined("BaseDir") ){
	define("BaseDir",$baseDir);  
}

if( !defined("BaseUrl") ){
	define("BaseUrl",$baseUrl);  
}
?>
    <?php include($_SERVER["DOCUMENT_ROOT"] .'/mypage/include/header.php'); ?>
        <section class="co manual">
        <div class="inner">
            <h1><span class="line">ご利用マニュアル</span></h1>
            <!-- navi start -->
            <ul id="manualNavi" class="pc clearfix">
                <li><a href="index.php">メニューについて</a></li>
                <li><a href="p02.php">TOPページ</a></li>
                <li><a href="p03.php">求人検索</a></li>
                <li class="current"><a href="p04.php">気になる求人リスト</a></li>
                <li><a href="p05.php">希望条件保存</a></li>
                <li><a href="p06.php">応募状況</a></li>
                <li><a href="p07.php">プロフィール設定</a></li>
            </ul>
            <form name="sort_form" id="spMenu" class="sp">
                <select name="sort" onchange="dropsort()">
                    <option>▼お選び下さい</option>

                    <option value="index.php">メニューについて</option>
                    <option value="p02.php">TOPページ</option>
                    <option value="p03.php">求人検索</option>
                    <option value="p04.php">気になる求人リスト</option>
                    <option value="p05.php">希望条件保存</option>
                    <option value="p06.php">応募状況</option>
                    <option value="p07.php">プロフィール設定</option>
                </select>
            </form>
            <!-- navi end -->
            <h2>気になる求人リストページ</h2>
            <p>ご自身で追加された気になる求人リストを確認することができます。</p>
            <h3>一覧ページ</h3>
            <p><img src="img/p04_img_01.jpg" width="100%" alt=""/></p>
            <ul class="list">
                <li class="na">クリックすると詳細ページを閲覧することができます。</li>
                <li class="nb">クリックするとエントリーできます。</li>
                <li class="nc">気になる求人リストから削除することができます。</li>
                <li class="nd">削除したい情報をチェックの上、まとめて削除することができます。</li>
            </ul>
            <h3>求人詳細ページ</h3>
            <p><img src="img/p04_img_02.jpg" width="100%" alt=""/></p>
            <ul class="list">
                <li class="ne">クリックするとエントリーできます。</li>
            </ul>
            <h3>エントリー後について</h3>
            <p>一覧ページ[応募する]、求人詳細ページ[この求人の詳細を聞く]をクリックすると、エントリーされます。<br>
                エントリーされた求人情報は応募状況ページ（フェーズ：応募中）に格納されます。</p>
                <h3>気になる求人への追加方法</h3>
            <ol>
                    <li>求人検索を行ってください。 </li>
                    <li>[この求人の詳細を開く]をクリックし、求人詳細ページを閲覧します。</li>
                    <li>求人詳細ぺージ下部にある[気になる求人リストに保存]をクリックすると追加されます。</li>
            </ol>
            <!-- btnBox start -->
            <ul class="btnBox clearfix">
                <li class="prev"><a href="p03.php">前へ</a></li>
            <li class="close"><a href="javascript:;" onClick="window.close(); return false;">閉じる</a></li>
                <li class="next"><a href="p05.php">次へ</a></li>
            </ul>
            <!-- btnBox start -->
        </div>
    </section>
    <!-- co end -->
    <?php include($_SERVER["DOCUMENT_ROOT"] .'/mypage/include/footer.php'); ?>
</body>
</html>