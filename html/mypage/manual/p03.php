<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="robots" content="noindex,nofollow">
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <link href="../css/import.css" rel="stylesheet" media="all">
    <title>HUREXマイページ</title>
    <script src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
    <script src="../js/jquery.page-scroller-308.js" charset="utf-8"></script>
    <script src="../js/rollover.js"></script>
    <script src="../js/common.js"></script>
    <script src="../js/manualNavi.js"></script>
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/css3-mediaqueries.js"></script>
    <![endif]-->
 <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6405327-10', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
<?php
$kanridir = "/mypage";
$baseUrl = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"]  . $kanridir ."/";

if( !defined("BaseDir") ){
	define("BaseDir",$baseDir);  
}

if( !defined("BaseUrl") ){
	define("BaseUrl",$baseUrl);  
}
?>
    <?php include($_SERVER["DOCUMENT_ROOT"] .'/mypage/include/header.php'); ?>
        <section class="co manual">
        <div class="inner">
            <h1><span class="line">ご利用マニュアル</span></h1>
            <!-- navi start -->
            <ul id="manualNavi" class="pc clearfix">
                <li><a href="index.php">メニューについて</a></li>
                <li><a href="p02.php">TOPページ</a></li>
                <li class="current"><a href="p03.php">求人検索</a></li>
                <li><a href="p04.php">気になる求人リスト</a></li>
                <li><a href="p05.php">希望条件保存</a></li>
                <li><a href="p06.php">応募状況</a></li>
                <li><a href="p07.php">プロフィール設定</a></li>
            </ul>
            <form name="sort_form" id="spMenu" class="sp">
                <select name="sort" onchange="dropsort()">
                    <option>▼お選び下さい</option>

                    <option value="index.php">メニューについて</option>
                    <option value="p02.php">TOPページ</option>
                    <option value="p03.php">求人検索</option>
                    <option value="p04.php">気になる求人リスト</option>
                    <option value="p05.php">希望条件保存</option>
                    <option value="p06.php">応募状況</option>
                    <option value="p07.php">プロフィール設定</option>
                </select>
            </form>
            <!-- navi end -->
            <h2>求人検索ページ</h2>
            <p>任意の条件を指定して求人検索を行います。</p>
            <p><img src="img/p03_img_01.jpg" width="100%" alt=""/></p>
            <h3>検索条件</h3>
            <ul class="point01">
                <li>職種・勤務地・年収・転勤・フリーワード</li>
            </ul>
            <h3>検索方法</h3>
            <ol>
                <li>MENUから[求人検索]をクリック</li><li>別画面に遷移しますので条件を入力の上、[この条件で検索する]をクリックしてください。</li>
                <li>選択された条件を満たす求人情報一覧が表示されます。</li>
                <li>詳細ページを閲覧する場合は[この求人の詳細を開く]をクリックしてください。</li>
            </ol>
            <!-- btnBox start -->
            <ul class="btnBox clearfix">
                <li class="prev"><a href="p02.php">前へ</a></li>
            <li class="close"><a href="javascript:;" onClick="window.close(); return false;">閉じる</a></li>
                <li class="next"><a href="p04.php">次へ</a></li>
            </ul>
            <!-- btnBox start -->
        </div>
    </section>
    <!-- co end -->
    <?php include($_SERVER["DOCUMENT_ROOT"] .'/mypage/include/footer.php'); ?>
</body>
</html>