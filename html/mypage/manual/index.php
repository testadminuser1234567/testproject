<html>

<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="robots" content="noindex,nofollow">
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="-1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <link href="../css/import.css" rel="stylesheet" media="all">
    <title>HUREXマイページ</title>
    <script src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
    <script src="../js/jquery.page-scroller-308.js" charset="utf-8"></script>
    <script src="../js/rollover.js"></script>
    <script src="../js/common.js"></script>
    <script src="../js/manualNavi.js"></script>
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/css3-mediaqueries.js"></script>
    <![endif]-->
    <script>
        ( function ( i, s, o, g, r, a, m ) {
            i[ 'GoogleAnalyticsObject' ] = r;
            i[ r ] = i[ r ] || function () {
                ( i[ r ].q = i[ r ].q || [] ).push( arguments )
            }, i[ r ].l = 1 * new Date();
            a = s.createElement( o ),
                m = s.getElementsByTagName( o )[ 0 ];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore( a, m )
        } )( window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga' );

        ga( 'create', 'UA-6405327-10', 'auto' );
        ga( 'send', 'pageview' );
    </script>
</head>

<body>
<?php
$kanridir = "/mypage";
$baseUrl = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"]  . $kanridir ."/";

if( !defined("BaseDir") ){
	define("BaseDir",$baseDir);  
}

if( !defined("BaseUrl") ){
	define("BaseUrl",$baseUrl);  
}
?>
    <?php include($_SERVER["DOCUMENT_ROOT"] .'/mypage/include/header.php'); ?>
    <section class="co manual">
        <div class="inner">
            <h1><span class="line">ご利用マニュアル</span></h1>
            <!-- navi start -->
            <ul id="manualNavi" class="pc clearfix">
                <li class="current"><a href="index.php">メニューについて</a>
                </li>
                <li><a href="p02.php">TOPページ</a>
                </li>
                <li><a href="p03.php">求人検索</a>
                </li>
                <li><a href="p04.php">気になる求人リスト</a>
                </li>
                <li><a href="p05.php">希望条件保存</a>
                </li>
                <li><a href="p06.php">応募状況</a>
                </li>
                <li><a href="p07.php">プロフィール設定</a>
                </li>
            </ul>
            <form name="sort_form" id="spMenu" class="sp">
                <select name="sort" onchange="dropsort()">
                    <option>▼お選び下さい</option>

                    <option value="index.php">メニューについて</option>
                    <option value="p02.php">TOPページ</option>
                    <option value="p03.php">求人検索</option>
                    <option value="p04.php">気になる求人リスト</option>
                    <option value="p05.php">希望条件保存</option>
                    <option value="p06.php">応募状況</option>
                    <option value="p07.php">プロフィール設定</option>
                </select>
            </form>
            <!-- navi end -->
            <h2>メニューについて</h2>
            <p><img src="img/index_img_01.jpg" width="100%" class="pc" alt=""/>
            <img src="img/index_img_01_sp.jpg" width="100%" class="sp" alt=""/>
            </p>
            <ul class="list">
                <li class="n01"><strong>求人検索</strong><br> 任意の条件を指定して求人検索が可能です。
                </li>
                <li class="n02"><strong>気になる求人リスト</strong><br> ご自身で追加された気になる求人リストを表示します。
                    <br> ※気になるリストから応募を行うと応募状況ページに格納されますのでご注意ください。
                </li>
                <li class="n03"><strong>希望条件保存</strong><br> マイページ登録時、もしくは登録後保存されたご自身の希望条件を確認・変更するページです。
                </li>
                <li class="n04"><strong>応募状況</strong><br> ご自身の応募された求人情報の現状を確認することが可能です。
                </li>
                <li class="n05"><strong>プロフィール設定</strong><br> 登録されたご自身のプロフィールを確認・変更することが可能です。
                </li>
            </ul>
            <!-- btnBox start -->
            <ul class="btnBox clearfix">
                <li class="close"><a href="javascript:;" onClick="window.close(); return false;">閉じる</a>
                </li>
                <li class="next"><a href="p02.php">次へ</a>
                </li>
            </ul>
            <!-- btnBox start -->
        </div>
    </section>
    <!-- co end -->
    <?php include($_SERVER["DOCUMENT_ROOT"] .'/mypage/include/footer.php'); ?>
</body>

</html>