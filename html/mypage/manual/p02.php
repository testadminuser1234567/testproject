<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="robots" content="noindex,nofollow">
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <link href="../css/import.css" rel="stylesheet" media="all">
    <title>HUREXマイページ</title>
    <script src="../js/jquery-1.6.4.min.js" charset="utf-8"></script>
    <script src="../js/jquery.page-scroller-308.js" charset="utf-8"></script>
    <script src="../js/rollover.js"></script>
    <script src="../js/common.js"></script>
    <script src="../js/manualNavi.js"></script>
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/css3-mediaqueries.js"></script>
    <![endif]-->
 <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-6405327-10', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
<?php
$kanridir = "/mypage";
$baseUrl = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"]  . $kanridir ."/";

if( !defined("BaseDir") ){
	define("BaseDir",$baseDir);  
}

if( !defined("BaseUrl") ){
	define("BaseUrl",$baseUrl);  
}
?>
    <?php include($_SERVER["DOCUMENT_ROOT"] .'/mypage/include/header.php'); ?>
        <section class="co manual">
        <div class="inner">
            <h1><span class="line">ご利用マニュアル</span></h1>
            <!-- navi start -->
            <ul id="manualNavi" class="pc clearfix">
                <li><a href="index.php">メニューについて</a></li>
                <li class="current"><a href="p02.php">TOPページ</a></li>
                <li><a href="p03.php">求人検索</a></li>
                <li><a href="p04.php">気になる求人リスト</a></li>
                <li><a href="p05.php">希望条件保存</a></li>
                <li><a href="p06.php">応募状況</a></li>
                <li><a href="p07.php">プロフィール設定</a></li>
            </ul>
            <form name="sort_form" id="spMenu" class="sp">
                <select name="sort" onchange="dropsort()">
                    <option>▼お選び下さい</option>

                    <option value="index.php">メニューについて</option>
                    <option value="p02.php">TOPページ</option>
                    <option value="p03.php">求人検索</option>
                    <option value="p04.php">気になる求人リスト</option>
                    <option value="p05.php">希望条件保存</option>
                    <option value="p06.php">応募状況</option>
                    <option value="p07.php">プロフィール設定</option>
                </select>
            </form>
            <!-- navi end -->
            <h2>TOPページ</h2>
            <ul class="list">
                <li class="n01"><strong>ページ上部表示エリア</strong></li>
            </ul>
            <p><img src="img/p02_img_01.jpg" width="100%" alt=""/></p>
            <ul class="list">
                <li class="na">ご自身で追加した気になる求人リストの件数を表示します。</li>
                <li class="nb">保存済の希望条件で求人情報を検索できます。</li>
                <li class="nc">現在の保存済の希望条件を確認・変更できます。</li>
                <li class="nd">オファーが届いている場合の通知領域です。</li>
            </ul>
            <ul class="list">
                <li class="n02"><strong>希望条件に合致する求人情報</strong><br>
保存済の「希望条件」を満たす求人情報を新着順に掲載しています。</li>
            </ul>
            <p><img src="img/p02_img_02.jpg" width="100%" alt=""/></p>
            <ul class="list">
                <li class="n03"><strong>応募状況</strong></li>
            </ul>
            <p><img src="img/p02_img_03.jpg" width="100%" alt=""/></p>
            <ul class="list">
                <li class="ne">あなたのへオファー件数、求人情報を掲載しています。</li>
                <li class="nf">お問い合わせ・応募された件数、求人情報を掲載しています。</li>
            </ul>
            <ul class="list">
                <li class="n04"><strong>地域と職種で検索</strong><br>
　「地域」「職種」「転勤可否」を指定して検索することができます。</li>
            </ul>
            <p><img src="img/p02_img_04.jpg" width="100%" alt=""/></p>
            <!-- btnBox start -->
            <ul class="btnBox clearfix">
                <li class="prev"><a href="index.php">前へ</a></li>
            <li class="close"><a href="javascript:;" onClick="window.close(); return false;">閉じる</a></li>
                <li class="next"><a href="p03.php">次へ</a></li>
            </ul>
            <!-- btnBox start -->
        </div>
    </section>
    <!-- co end -->
    <?php include($_SERVER["DOCUMENT_ROOT"] .'/mypage/include/footer.php'); ?>
</body>
</html>