    <!-- footLinks start -->
    <div id="footLinks">
        <p><a href="<?php echo BaseUrl;?>manual/page1" target="_blank">ご利用マニュアル</a> /  <a href="https://www.hurex.jp/company/privacy-policy/" target="_blank">個人情報の取り扱い</a> /  <br class="sp"><a href="https://www.hurex.jp/company/responsibility/" target="_blank">利用規約･免責事項</a> /  <a href="https://www.hurex.jp/inquiry/" target="_blank">お問い合わせ</a> /  <a href="<?php echo BaseUrl;?>user/logout">ログアウト</a></p>
            <div class="ssl sp clearfix">
                <div class="seal">
                    <script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hurex.jp&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=ja"></script>
                </div>
                <p>当サイトは、お客様のプライバシー保護のため、個人情報入力ページにおいてSSL暗号化通信を採用しています</p>
            </div>
    </div>
    <!-- footLinks end -->
<!-- footer start -->
    <footer id="footer">
        <div class="inner clearfix">
            <div id="footerCo" class="clearfix">
            <p class="logo"><img src="<?php echo BaseUrl;?>images/logo_hurex.png" width="140" height="42" alt="HUREX"/></p>
            </div>
            <div class="ssl pc clearfix">
                <div class="seal">
                    <script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hurex.jp&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=ja"></script>
                </div>
                <p>当サイトは、お客様のプライバシー保護のため、個人情報入力ページにおいてSSL暗号化通信を採用しています</p>
            </div>
        </div>
        <p class="copy">&copy; 2018 HUREX.INK</p>
    </footer>
    <!-- footer end -->