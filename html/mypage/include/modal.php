<?php
//HRBCマスターと連動

//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);
$job_ary=array();
$sort=array();
if(!empty($tmpjob['Item'])){
	foreach($tmpjob['Item'] as $k=>$v){
		if(!empty($v['Item'])){
			foreach($v['Item'] as $k2=>$v2){
				$job_ary[] = $v2;
			}
		}
	}
	foreach ((array) $job_ary as $key => $value) {
		$sort[$key] = $value["Option.P_Order"];
	}
}
array_multisort($sort, SORT_ASC, $job_ary);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);
$tantou_job_ary=array();
if(!empty($tmptantoujob['Item'])){
	foreach($tmptantoujob['Item'] as $k=>$v){
		if(!empty($v['Item'])){
			foreach($v['Item'] as $k2=>$v2){
				$tantou_job_ary[] = $v2;
			}
		}
	}
	foreach ((array) $tantou_job_ary as $key => $value) {
		$tantou_sort[$key] = $value["Option.P_Order"];
	}
}
array_multisort($tantou_sort, SORT_ASC, $tantou_job_ary);

?>
<div class="remodal" data-remodal-id="modal" data-remodal-options="hashTracking:false">
    <!-- searchBox start -->
    <div class="searchBox form">
        <p class="close remodal-close" data-remodal-action="close"><a href="javascript:;">閉じる</a>
        </p>
        <?php echo form_open('search/index/', array('method'=>'GET'));?>
            <h3>求人を探す</h3>
            <div class="box fl">
                <div class="inner clearfix">
                    <h4>職種</h4>
                    <div class="list">
                    <?php if(!empty($job_ary)):?>
                                    <ul class="full clearfix">
                        <?php foreach($job_ary as $k=>$v):?>
                                        <li class="<?php if($v['Option.P_Name']=="営業系"):?>ss<?php endif;?><?php if($v['Option.P_Name']=="管理部門/事務系"):?>ss no<?php endif;?>">
                                                <input type="checkbox" class="searchjoblabel<?php echo htmlspecialchars($v['Option.P_Id'],ENT_QUOTES,'UTF-8');?> searchjobchk checkbox searchjobcheckbox" name="job[]" value="<?php echo htmlspecialchars($v['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty($job)):?><?php foreach($job as $jk=>$jv):?><?php if($jv==$v["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?> id="cat_job0<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>">
                                            <label id="joblabel<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" for="cat_job0<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" class="searchcheckboxlabel searchjoblabel CheckBoxLabelClass searchjobnavichecklabel <?php if(!empty($job)):?><?php foreach($job as $jk=>$jv):?><?php if($jv==$v["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?>">
                                                <?php echo htmlspecialchars($v['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
                                            </label>

                                        </li>
                        <?php endforeach;?>
                                    </ul>
                    <?php endif;?>
                    </div>
                </div>
            </div>

            <div class="box fl">
                <div class="inner clearfix">
                    <h4>勤務地<span class="ss"></span></h4>
                    <div class="list">
<?php if(!empty($tmparea['Item'])):?>
<?php foreach($tmparea['Item'] as $k=>$v):?>
<?php if(!empty($v['Item'])):?>


<?php foreach($v['Item'] as $k2=>$v2):?>

<?php if($v2["Option.P_Name"]=="関東"):?>
                        <p class="category"><a href="javascript:;"><span>関東/上信越/北陸</span></a></p>
                        <div class="categoryBox kanto">
<?php endif;?>
<?php if($v2["Option.P_Name"]=="東海"):?>
			</div>
                        <p class="category"><a href="javascript:;"><span>関西/東海/中国</span></a></p>
                        <div class="categoryBox tokai">
<?php endif;?>
<?php if($v2["Option.P_Name"]=="四国"):?>
			</div>
                        <p class="category"><a href="javascript:;"><span>四国/九州/沖縄/海外</span></a></p>
                        <div class="categoryBox shikoku">
<?php endif;?>

<?php if($v2['Option.P_Name']!="勤務地不問"):?>
<ul class="clearfix">
<li>
<input type="checkbox" class="checkbox areacheckbox<?php if($k2-1==count($tmparea['Item'])):?> required<?php endif;?> searchareaallchk " name="pref[]" value="<?php echo htmlspecialchars($v2['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty($pref)):?><?php foreach($pref as $jk=>$jv):?><?php if($jv==$v2["Option.P_Id"]):?>checked<?php $flgs[]=htmlspecialchars($v2['Option.P_Alias'],ENT_QUOTES,'UTF-8');?><?php endif;?><?php endforeach;?><?php endif;?> id="<?php echo htmlspecialchars($v2['Option.P_Alias'],ENT_QUOTES,'UTF-8');?>">
<label for="<?php echo htmlspecialchars($v2['Option.P_Alias'],ENT_QUOTES,'UTF-8');?>" class="checkboxlabel arealabel CheckBoxLabelClass searchareaallnavichecklabel all <?php if(!empty($pref)):?><?php foreach($pref as $jk=>$jv):?><?php if($jv==$v2["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?>">
<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php if($v2['Option.P_Name']!="海外"):?>を全部選択する<?php endif;?>
</label>

<?php if(!empty($v2["Items"]['Item'])):?>
<ul class="area clearfix">

<?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
<li>
<?php if(!empty($v3["Option.P_Id"] )):?>
<input type="checkbox" class="checkbox areacheckbox<?php if($k3-1==count($tmparea['Item'])):?> required<?php endif;?> searchareachk" name="pref[]" value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty($pref)):?><?php foreach($pref as $jk2=>$jv2):?><?php if($jv2==$v3["Option.P_Id"]):?>checked<?php $flgs[]=htmlspecialchars($v2['Option.P_Alias'],ENT_QUOTES,'UTF-8');?><?php endif;?><?php endforeach;?><?php endif;?> id="<?php echo htmlspecialchars($v3['Option.P_Alias'],ENT_QUOTES,'UTF-8');?>">
<label for="<?php echo htmlspecialchars($v3['Option.P_Alias'],ENT_QUOTES,'UTF-8');?>" class="checkboxlabel arealabel CheckBoxLabelClass searchareanavichecklabel <?php if(!empty($pref)):?><?php foreach($pref as $jk2=>$jv2):?><?php if($jv2==$v3["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?>">
<?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
</label>
<?php endif;?>

</li>
<?php endforeach;?>
</ul>
<?php endif;?>
</li>
</ul>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>
</div><!-- category end -->

                    </div>
                </div>
            </div>
            <div class="box fl">
                <div class="inner clearfix">
                    <h4>年収</h4>
                    <div class="list">
                    <?php if(!empty($tmpincome['Item'])):?>
		    <select name="year_income" id="year_income">
				<option value="">指定なし</option>
                        <?php foreach($tmpincome['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                        <option value="<?php echo htmlspecialchars($v2["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" <?php if($v2["Option.P_Id"]==@$year_income):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
		    </select>
                    <?php endif;?>
                    </div>
                </div>
            </div>

            <div class="box fl">
                <div class="inner clearfix">
                    <h4>転勤</h4>
                    <div class="list">
                        <ul class="two clearftx"><li>
                        <label for="ari" class="radiolabel">
<input type="radio" class="radio" name="tenkin" id="ari" value="可" <?php if(@$tenkin=="可"):?>checked<?php endif;?>>
                            可</label></li>
                    <li>
                        <label for="nashi" class="radiolabel">
<input type="radio" class="radio" name="tenkin" id="nashi" value="不可" <?php if(@$tenkin=="不可"):?>checked<?php endif;?>>
                            不可</label></li>
                        </ul>
                    
                    </div>
                </div>
            </div>

            <div class="box fl">
                <div class="inner clearfix">
                    <h4>フリーワード</h4>
                    <div class="list">
                        <input name="keyword" type="text" class="txt" placeholder="入力例：正社員　経理"  style="width:100%;" value="<?php echo htmlspecialchars(@$keyword,ENT_QUOTES,'UTF-8');?>" />
<ul class="two clearftx">
    <li>
                        <label for="or" class="radiolabel">
<input type="radio" class="radio" name="keyword_flg" id="or" value="or" <?php if(@$keyword_flg=="or"):?>checked<?php endif;?>>
いずれかの文字を含む</label>
    </li>
         <li>           
                        <label for="and" class="radiolabel">
<input type="radio" class="radio" name="keyword_flg" id="and" value="and" <?php if(@$keyword_flg=="and"):?>checked<?php endif;?>>
すべての文字を含む</label>
    </li></ul>
                    
                    </div>
                </div>
            </div>
            <div class="box">
                <div class="inner clearfix">
                    <p class="clearBtn"><input type="button" value="全ての選択を解除する" class="clearForm">
                    </p>
                    <div class="aCenter">
                    <button class="btn"><span>この条件で検索する</span></button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- searchBox end -->
</div>
<script>
//チェンジイベント
$(function(){
document.onkeydown = function(e) {
    var keyCode = false;
 
    if (e) event = e;
 
    if (event) {
        if (event.keyCode) {
            keyCode = event.keyCode;
        } else if (event.which) {
            keyCode = event.which;
        }
    }
    if(keyCode==27){
	$("#wrapper").show();
    }
};

    $('ul input[type="checkbox"]').change(function(){
        if ($(this).is(':checked')) {
            $(this).parent().find('input[type="checkbox"]').prop('checked', true);
        }
        else {
            $(this).parent().find('input[type="checkbox"]').prop('checked', false);
            $(this).parents('li').each(function(){
                $(this).children('input[type="checkbox"]').prop('checked', false);
            });
        }
    });

    //form clear
    $(".clearForm").bind("click",function(){
	$(this.form).find("textarea, :text, select").val("").end().find(":checked").prop("checked", false);
    });
})
<?php if(!empty($flgs)):?>
<?php
$kanto=0;
$tokai=0;
$shikoku=0;
?>
window.onload=function(){
<?php foreach($flgs as $k => $v):?>
<?php if($v=="Option.P_AreaKanto" || $v=="Option.P_AreaJoshinetsuHokuriku"):?><?php $kanto=1;?><?php endif;?>
<?php if($v=="Option.P_AreaTokai" || $v=="Option.P_AreaKansai"):?><?php $tokai=1;?><?php endif;?>
<?php if($v=="Option.P_AreaChugoku" || $v=="Option.P_AreaShikoku" || $v=="Option.P_AreaKyushuOkinawa" || $v=="Option.P_AreaForeign"):?><?php $shikoku=1;?><?php endif;?>
<?php endforeach;?>
<?php endif;?>
<?php if($kanto==1):?>
$(".kanto").show();
<?php endif;?>
<?php if($tokai==1):?>
$(".tokai").show();
<?php endif;?>
<?php if($shikoku==1):?>
$(".shikoku").show();
<?php endif;?>
//}
</script>