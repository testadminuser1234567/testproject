   <!-- header start -->
<header id="header">
    <div class="inner clearfix">
        <p class="logo"><a href="<?php echo BaseUrl;?>user/home/"><img src="<?php echo BaseUrl;?>images/logo.png" width="217" height="35" alt="HUREXマイページ"/></a></p>
        <div id="headerContents" class="pc">
            <ul class="clearfix">
                <li><a href="https://www.hurex.jp/" target="_blank">HUREXホームページ</a></li>
                <li><a href="<?php echo BaseUrl;?>manual/page1" target="_blank">ご利用マニュアル</a></li>
            </ul>
            <p id="txtContact">サービスについてのお問い合わせ<span class="tel"><img src="<?php echo BaseUrl;?>images/ico_freedial.png" width="21" height="12" alt=""/>0120-14-1150</span></p>
        </div>
    </div>
</header>
   <!-- header end -->