            <?php
            //HRBCマスターと連動

            //職種の設定
            $tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
            $tmpjob = json_decode($tmpjob,true);
            $job_ary=array();
            if(!empty($tmpjob['Item'])){
                foreach($tmpjob['Item'] as $k=>$v){
                    if(!empty($v['Item'])){
                        foreach($v['Item'] as $k2=>$v2){
                            $job_ary[] = $v2;
                        }
                    }
                }
                foreach ((array) $job_ary as $key => $value) {
                    $sort[$key] = $value["Option.P_Order"];
                }
            }
            array_multisort($sort, SORT_ASC, $job_ary);

            $tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
            $tmppref = json_decode($tmppref,true);

            $tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
            $tmpgender = json_decode($tmpgender,true);

            $tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
            $tmpbackground = json_decode($tmpbackground,true);

            $tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
            $tmpwork = json_decode($tmpwork,true);

            $tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
            $tmparea = json_decode($tmparea,true);

            $tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
            $tmptenkin = json_decode($tmptenkin,true);

            ?>
<?php
$topUri = $_SERVER['REQUEST_URI'];
$commonClass="";
if(!preg_match("/mypage\/user\/home/", $topUri)){
	$commonClass=' class="common"';
}
?>
<!-- knoehowBox start -->
            <div id="knowhowBox">
                <h2><span>内定率UP!!<br class="pc">今スグ役立つ転職ノウハウ</span></h2>
                <ul>
                    <li><a href="https://www.hurex.jp/guide/" target="_blank" class="clearfix">
                        <p class="photo"><img src="<?php echo BaseUrl;?>images/knowhow_img_01.jpg" width="100%" alt=""/></p>
                        <div class="txt">
                        <span>事前準備から円満退社まで</span>
                            転職成功ガイド</div></a></li>
                    <li><a href="https://www.hurex.jp/column/resume-adviser/writing.html" target="_blank" class="clearfix">
                        <p class="photo"><img src="<?php echo BaseUrl;?>images/knowhow_img_02.jpg" width="100%" alt=""/></p>
                        <div class="txt">
                        <span>４つのポイントが重要！</span>
                            職務経歴書のコツ</div></a></li>
                    <li><a href="https://www.hurex.jp/success/" target="_blank" class="clearfix">
                        <p class="photo"><img src="<?php echo BaseUrl;?>images/knowhow_img_03.jpg" width="100%" alt=""/></p>
                        <div class="txt">
                        <span>転職して本当によかった！</span>
                            成功者インタビュー</div></a></li>
                    <li><a href="https://www.hurex.jp/campaign/" target="_blank" class="clearfix">
                        <p class="photo"><img src="<?php echo BaseUrl;?>images/knowhow_img_04.jpg" width="100%" alt=""/></p>
                        <div class="txt">
                        <span>お知り合いを紹介いただくと</span>
                            1万円プレゼント</div></a></li>
                </ul>
            </div>
            <!-- knowhowBox end -->
        </aside>
    </div>
    </div>
    <!-- co end -->
    <!-- btnTel start -->
            <div class="btnTel">
                <a href="tel:0120141150">
                    <p class="txt">HUREXのサービス内容などのお問い合わせ</p>
                    <p class="img"><img src="<?php echo BaseUrl;?>images/btn_tel.png" width="200" height="25" alt=""/></p>
                    <p class="txt">タップで電話がかかります</p>
                </a>
            </div>
    <!-- btnTel end -->
    <!-- footNavi start -->
    <div id="footNavi">
        <ul>
            <li class="n01"><a data-remodal-target="modal"><span>求人検索</span></a></li>
            <li class="n02"><a href="<?php echo BaseUrl;?>search/like/"><span>気になる求人リスト</span></a></li>
            <li class="n03"><a href="<?php echo BaseUrl;?>search/home/"><span>希望条件</span></a></li>
            <li class="n04"><a href="<?php echo BaseUrl;?>process/home/"><span>応募状況</span></a></li>
            <li class="n05"><a href="<?php echo BaseUrl;?>user/profile/"><span>プロフィール設定</span></a></li>
        </ul>
    </div>
    <!-- footNavi end -->
    <!-- footLinks start -->
    <div id="footLinks">
        <p><a href="<?php echo BaseUrl;?>manual/page1" target="_blank">ご利用マニュアル</a> /  <a href="https://www.hurex.jp/company/privacy-policy/" target="_blank">個人情報の取り扱い</a> /  <br class="sp"><a href="https://www.hurex.jp/company/responsibility/" target="_blank">利用規約･免責事項</a> /  <a href="https://www.hurex.jp/inquiry/" target="_blank">お問い合わせ</a> /  <br class="sp"><a href="<?php echo BaseUrl;?>user/logout">ログアウト</a> /  <a href="<?php echo BaseUrl;?>suspension/home" target="_blank">メールマガジン停止・退会</a></p>
            <div class="ssl sp clearfix">
                <div class="seal">
                    <script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hurex.jp&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=ja"></script>
                </div>
                <p>当サイトは、お客様のプライバシー保護のため、個人情報入力ページにおいてSSL暗号化通信を採用しています</p>
            </div>
    </div>
    <!-- footLinks end -->
<!-- footer start -->
    <footer id="footer">
        <div class="inner clearfix">
            <div id="footerCo" class="clearfix">
            <p class="logo"><img src="<?php echo BaseUrl;?>images/logo_hurex.png" width="140" height="42" alt="HUREX"/></p>
                <p class="btn"><a href="/"><span>HUREXホームページ</span></a></p>
            </div>
            <div class="ssl pc clearfix">
                <div class="seal">
                    <script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hurex.jp&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=ja"></script>
                </div>
                <p>当サイトは、お客様のプライバシー保護のため、個人情報入力ページにおいてSSL暗号化通信を採用しています</p>
            </div>
        </div>
        <p class="copy">&copy; 2018 HUREX.INK</p>
    </footer>
    <!-- footer end -->