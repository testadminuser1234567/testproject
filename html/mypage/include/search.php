<?php
//HRBCマスターと連動

//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);
$job_ary=array();
$sort=array();
if(!empty($tmpjob['Item'])){
	foreach($tmpjob['Item'] as $k=>$v){
		if(!empty($v['Item'])){
			foreach($v['Item'] as $k2=>$v2){
				$job_ary[] = $v2;
			}
		}
	}
	foreach ((array) $job_ary as $key => $value) {
		$sort[$key] = $value["Option.P_Order"];
	}
}
array_multisort($sort, SORT_ASC, $job_ary);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);
$tantou_job_ary=array();
if(!empty($tmptantoujob['Item'])){
	foreach($tmptantoujob['Item'] as $k=>$v){
		if(!empty($v['Item'])){
			foreach($v['Item'] as $k2=>$v2){
				$tantou_job_ary[] = $v2;
			}
		}
	}
	foreach ((array) $tantou_job_ary as $key => $value) {
		$tantou_sort[$key] = $value["Option.P_Order"];
	}
}
array_multisort($tantou_sort, SORT_ASC, $tantou_job_ary);

?>
<div class="box fl">
	<div class="inner clearfix">
	<h4>職種</h4>
	<div class="list">
                    <?php if(!empty($job_ary)):?>
                                    <ul class="clearfix">
                        <?php foreach($job_ary as $k=>$v):?>
                                        <li>
                                                <input type="checkbox" class="searchjoblabel<?php echo htmlspecialchars($v['Option.P_Id'],ENT_QUOTES,'UTF-8');?> searchjobchk checkbox searchjobcheckbox" name="job[]" value="<?php echo htmlspecialchars($v['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty($job)):?><?php foreach($job as $jk=>$jv):?><?php if($jv==$v["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?> id="cat_job0<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>">
                                            <label id="joblabel<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" for="cat_job0<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" class="searchcheckboxlabel searchjoblabel CheckBoxLabelClass searchjobnavichecklabel <?php if(!empty($job)):?><?php foreach($job as $jk=>$jv):?><?php if($jv==$v["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?>">
                                                <?php echo htmlspecialchars($v['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
                                            </label>

                                        </li>
                        <?php endforeach;?>
                                    </ul>
                    <?php endif;?>

	</div>
	</div>
</div>

<div class="box fl">
<div class="inner clearfix">
<h4>勤務地<span class="ss"></span></h4>
<div class="list">
<?php if(!empty($tmparea['Item'])):?>
<?php foreach($tmparea['Item'] as $k=>$v):?>
<?php if(!empty($v['Item'])):?>
<?php foreach($v['Item'] as $k2=>$v2):?>
<?php if($v2['Option.P_Name']!="勤務地不問"):?>
<ul class="clearfix">
<li>
<input type="checkbox" class="checkbox areacheckbox<?php if($k2-1==count($tmparea['Item'])):?> required<?php endif;?> searchareaallchk " name="pref[]" value="<?php echo htmlspecialchars($v2['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty($pref)):?><?php foreach($pref as $jk=>$jv):?><?php if($jv==$v2["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?> id="<?php echo htmlspecialchars($v2['Option.P_Alias'],ENT_QUOTES,'UTF-8');?>">
<label for="<?php echo htmlspecialchars($v2['Option.P_Alias'],ENT_QUOTES,'UTF-8');?>" class="checkboxlabel arealabel CheckBoxLabelClass searchareaallnavichecklabel <?php if(!empty($pref)):?><?php foreach($pref as $jk=>$jv):?><?php if($jv==$v2["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?>">
<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
</label>

<?php if(!empty($v2["Items"]['Item'])):?>
<ul class="clearfix">
<?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
<li>
<?php if(!empty($v3["Option.P_Id"] )):?>
<input type="checkbox" class="checkbox areacheckbox<?php if($k3-1==count($tmparea['Item'])):?> required<?php endif;?> searchareachk" name="pref[]" value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty($pref)):?><?php foreach($pref as $jk2=>$jv2):?><?php if($jv2==$v3["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?> id="<?php echo htmlspecialchars($v3['Option.P_Alias'],ENT_QUOTES,'UTF-8');?>">
<label for="<?php echo htmlspecialchars($v3['Option.P_Alias'],ENT_QUOTES,'UTF-8');?>" class="checkboxlabel arealabel CheckBoxLabelClass searchareanavichecklabel <?php if(!empty($pref)):?><?php foreach($pref as $jk2=>$jv2):?><?php if($jv2==$v3["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?>">
<?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
</label>
<?php endif;?>
</li>
<?php endforeach;?>
</ul>
<?php endif;?>
</li>
</ul>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>
</div>
</div>
</div>
<div class="box fl">
<div class="inner clearfix">
<h4>年収</h4>
<div class="list">
                    <?php if(!empty($tmpincome['Item'])):?>
		    <select name="year_income" id="year_income">
				<option value="">指定なし</option>
                        <?php foreach($tmpincome['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                        <option value="<?php echo htmlspecialchars($v2["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" <?php if($v2["Option.P_Id"]==@$year_income):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
		    </select>
                    <?php endif;?>
</div>
</div>
</div>

<div class="box fl">
<div class="inner clearfix">
<h4>転勤</h4>
<div class="list">
<label for="ari" class="radiolabel">
<input type="radio" class="radio" name="tenkin" id="ari" value="可" <?php if(@$tenkin=="可"):?>checked<?php endif;?>>
可</label>
<label for="nashi" class="radiolabel">
<input type="radio" class="radio" name="tenkin" id="nashi" value="不可" <?php if(@$tenkin=="不可"):?>checked<?php endif;?>>
不可</label>
</div>
</div>
</div>

<div class="box fl">
<div class="inner clearfix">
<h4>フリーワード</h4>
<div class="list">
<input name="keyword" type="text" class="txtM" placeholder="入力例：正社員　経理"  style="width:100%;" value="<?php echo htmlspecialchars(@$keyword,ENT_QUOTES,'UTF-8');?>" /><br />

<label for="or" class="radiolabel">
<input type="radio" class="radio" name="keyword_flg" id="or" value="or" <?php if(@$keyword_flg=="or"):?>checked<?php endif;?>>
いずれかの文字を含む</label>
<label for="and" class="radiolabel">
<input type="radio" class="radio" name="keyword_flg" id="and" value="and" <?php if(@$keyword_flg=="and"):?>checked<?php endif;?>>
すべての文字を含む</label>
</div>
</div>
</div>
<script>
//チェンジイベント
$(function(){
document.onkeydown = function(e) {
    var keyCode = false;
 
    if (e) event = e;
 
    if (event) {
        if (event.keyCode) {
            keyCode = event.keyCode;
        } else if (event.which) {
            keyCode = event.which;
        }
    }
    if(keyCode==27){
	$("#wrapper").show();
    }
};

    $('ul input[type="checkbox"]').change(function(){
        if ($(this).is(':checked')) {
            $(this).parent().find('input[type="checkbox"]').prop('checked', true);
        }
        else {
            $(this).parent().find('input[type="checkbox"]').prop('checked', false);
            $(this).parents('li').each(function(){
                $(this).children('input[type="checkbox"]').prop('checked', false);
            });
        }
    });

    //form clear
    $(".clearForm").bind("click",function(){
	$(this.form).find("textarea, :text, select").val("").end().find(":checked").prop("checked", false);
    });
})
</script>