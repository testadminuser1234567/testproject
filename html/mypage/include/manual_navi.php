    <script src="<?php echo base_url();?>js/manualNavi.js"></script>
            <!-- navi start -->
            <ul id="manualNavi" class="pc clearfix">
                <li<?php if($current=="page1"):?> class="current"<?php endif;?>><a href="<?php echo base_url();?>manual/page1">メニューについて</a>
                </li>
                <li<?php if($current=="page2"):?> class="current"<?php endif;?>><a href="<?php echo base_url();?>manual/page2">TOPページ</a>
                </li>
                <li<?php if($current=="page3"):?> class="current"<?php endif;?>><a href="<?php echo base_url();?>manual/page3">求人検索</a>
                </li>
                <li<?php if($current=="page4"):?> class="current"<?php endif;?>><a href="<?php echo base_url();?>manual/page4">気になる求人</a>
                </li>
                <li<?php if($current=="page5"):?> class="current"<?php endif;?>><a href="<?php echo base_url();?>manual/page5">希望条件</a>
                </li>
                <li<?php if($current=="page6"):?> class="current"<?php endif;?>><a href="<?php echo base_url();?>manual/page6">応募状況</a>
                </li>
                <li<?php if($current=="page7"):?> class="current"<?php endif;?>><a href="<?php echo base_url();?>manual/page7">プロフィール</a>
                </li>
            </ul>
            <form name="sort_form" id="spMenu" class="sp">
                <select name="sort" onchange="dropsort()">
                    <option value="">▼お選び下さい</option>

                    <option value="<?php echo base_url();?>manual/page1" <?php if($current=="page1"):?>selected<?php endif;?>>メニューについて</option>
                    <option value="<?php echo base_url();?>manual/page2" <?php if($current=="page2"):?>selected<?php endif;?>>TOPページ</option>
                    <option value="<?php echo base_url();?>manual/page3" <?php if($current=="page3"):?>selected<?php endif;?>>求人検索</option>
                    <option value="<?php echo base_url();?>manual/page4" <?php if($current=="page4"):?>selected<?php endif;?>>気になる求人リスト</option>
                    <option value="<?php echo base_url();?>manual/page5" <?php if($current=="page5"):?>selected<?php endif;?>>希望条件保存</option>
                    <option value="<?php echo base_url();?>manual/page6" <?php if($current=="page6"):?>selected<?php endif;?>>応募状況</option>
                    <option value="<?php echo base_url();?>manual/page7" <?php if($current=="page7"):?>selected<?php endif;?>>プロフィール設定</option>
                </select>
            </form>
            <!-- navi end -->
