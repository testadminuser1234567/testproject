<p id="menuBtn" class="sp"><a href="javascript:;"><span>MENU</span></a></p>
  <div id="menuBox">
   <!-- navi start -->
    <nav id="navi">
        <ul class="clearfix">
            <li class="navi01"><a href="<?php echo BaseUrl;?>user/home"><span>ホーム</span></a></li>
            <li class="navi02"><a href="javascript:;" data-remodal-target="modal"><span>求人検索</span></a></li>
            <li class="navi03"><a href="<?php echo BaseUrl;?>search/like/"><span>気になる求人</span></a>
            </li>
            <li class="navi04"><a href="<?php echo BaseUrl;?>condition/home/"><span>希望条件</span></a>
            </li>
            <li class="navi05"><a href="<?php echo BaseUrl;?>process/home/"><span>応募状況</span></a>
            </li>
            <li class="navi06"><a href="<?php echo BaseUrl;?>user/profile/"><span>プロフィール</span></a></li>
            <li class="sp point"><a href="<?php echo BaseUrl;?>manual/page1" target="_blank">ご利用マニュアル</a></li>
            <li class="sp point"><a href="https://www.hurex.jp/company/privacy-policy/" target="_blank">個人情報の取扱い</a></li>
            <li class="sp point"><a href="https://www.hurex.jp/company/responsibility/" target="_blank">利用規約/免責事項</a></li>
            <li class="sp point"><a href="https://www.hurex.jp/inquiry/" target="_blank">お問い合わせ</a></li>
            <li class="sp point logout"><a href="<?php echo BaseUrl;?>user/logout/">ログアウト</a></li>
        </ul>
    </nav>
    <!-- navi end -->
<p class="btnMenuClose sp"><a href="javascript:;">× 閉じる</a></p>
</div>
<!-- footerNavi start -->
    <div id="footerNavi">
        <ul>
            <li class="navi01"><a href="<?php echo BaseUrl;?>user/home"><span>ホーム</span></a></li>
            <li class="navi02"><a href="javascript:;" data-remodal-target="modal"><span>求人検索</span></a></li>
            <li class="navi03"><a href="<?php echo BaseUrl;?>search/like/"><span>気になる求人</span></a>
            </li>
            <li class="navi04"><a href="<?php echo BaseUrl;?>condition/home/"><span>希望条件</span></a>
            </li>
            <li class="navi05"><a href="<?php echo BaseUrl;?>process/home/"><span>応募状況</span></a>
            </li>
        </ul>
    </div>
<!-- footerNavi end -->