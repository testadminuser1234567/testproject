//エラー処理用
var data = {};
data.company_name1="会社名１";
data.tantou_job1="担当職種１";
data.start_year1="開始年１";
data.start_month1="開始月１";
data.company_name2="会社名２";
data.tantou_job2="担当職種２";
data.start_year2="開始年２";
data.start_month2="開始月２";
data.company_name3="会社名３";
data.tantou_job3="担当職種３";
data.start_year3="開始年３";
data.start_month3="開始月３";
data.kiboujob="希望職種";
data.income="希望年収";
data.tenkin="転勤可否";
data.earnings="現在の年収";
data.kibouarea1="希望勤務地：第１希望";
data.kibouarea2="希望勤務地：第２希望";
data.kibouarea3="希望勤務地：第３希望";

data.agree = "個人情報保護方針の同意にチェックをお願いいたします。";

$(function(){

	//////////////////////////////////////////////////////
	//　return 戻るボタンで戻ったときの背景対策
	//////////////////////////////////////////////////////
	if($("#company_name1").val()){
		$("#company_name1").css("background-color","#FFF");
	}
	if($("#company_name2").val()){
		$("#company_name2").css("background-color","#FFF");
	}
	if($("#company_name3").val()){
		$("#company_name3").css("background-color","#FFF");
	}
	if($("#start_year1").val()){
		$("#start_year1").css("background-color","#FFF");
	}
	if($("#start_month1").val()){
		$("#start_month1").css("background-color","#FFF");
	}
	if($("#end_year1").val()){
		$("#end_year1").css("background-color","#FFF");
	}
	if($("#end_month1").val()){
		$("#end_month1").css("background-color","#FFF");
	}
	if($("#start_year2").val()){
		$("#start_year2").css("background-color","#FFF");
	}
	if($("#start_month2").val()){
		$("#start_month2").css("background-color","#FFF");
	}
	if($("#end_year2").val()){
		$("#end_year2").css("background-color","#FFF");
	}
	if($("#end_month2").val()){
		$("#end_month2").css("background-color","#FFF");
	}
	if($("#start_year3").val()){
		$("#start_year3").css("background-color","#FFF");
	}
	if($("#start_month3").val()){
		$("#start_month3").css("background-color","#FFF");
	}
	if($("#end_year3").val()){
		$("#end_year3").css("background-color","#FFF");
	}
	if($("#end_month3").val()){
		$("#end_month3").css("background-color","#FFF");
	}

	if($("#earnings").val()){
		$("#earnings").css("background-color","#FFF");
	}
	if($("#tenkin").val()){
		$("#tenkin").css("background-color","#FFF");
	}
	if($("#kiboujob").val()){
		$("#kiboujob").css("background-color","#FFF");
	}
	if($("#income").val()){
		$("#income").css("background-color","#FFF");
	}
	if($("#tantou_job1").val()){
		$("#tantou_job1").css("background-color","#FFF");
	}
	if($("#tantou_job2").val()){
		$("#tantou_job2").css("background-color","#FFF");
	}
	if($("#tantou_job3").val()){
		$("#tantou_job3").css("background-color","#FFF");
	}


	//////////////////////////////////////////////////////
	// return ここまで
	//////////////////////////////////////////////////////


	//氏名
	$("input#company_name1").blur(function() {
		if($("#company_name1").val()){
			$(".company_name1").removeClass("hissu");
			$(".company_name1").addClass("ok");
			$("#company_name1").css("background-color","#FFF");
		}else{
			$(".company_name1").removeClass("ok");
			$(".company_name1").addClass("hissu");
			$("#company_name1").css("background-color","#f8f8f8");
		}
	});

	$("input#company_name2").blur(function() {
		if($("#company_name2").val()){
			$(".company_name2").removeClass("");
			$(".company_name2").addClass("ok");
			$("#company_name2").css("background-color","#FFF");
		}else{
			$(".company_name2").removeClass("ok");
			$(".company_name2").addClass("hissu");
			$("#company_name2").css("background-color","#f8f8f8");
		}
	});

	$("input#company_name3").blur(function() {
		if($("#company_name3").val()){
			$(".company_name3").removeClass("");
			$(".company_name3").addClass("ok");
			$("#company_name3").css("background-color","#FFF");
		}else{
			$(".company_name3").removeClass("ok");
			$(".company_name3").addClass("hissu");
			$("#company_name3").css("background-color","#f8f8f8");
		}
	});

	//在籍期間１
	$("select#start_year1").change(function() {
		if($("#start_year1").val() && $("#start_month1").val()){
			$(".start_year1").removeClass("hissu");
			$(".start_year1").addClass("ok");
			$("#start_year1").css("background-color","#FFF");
		}else{
			$(".start_year1").removeClass("ok");
			$(".start_year1").addClass("hissu");
			$("#start_year1").css("background-color","#f8f8f8");
		}
		if($("#start_year1").val() && $("#start_month1").val() && $("#end_year1").val() && $("#end_month1").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month1").val())-1;
			em = parseInt($("#end_month1").val())-1;
			var start_date1 = new Date($("#start_year1").val(), sm, 1);
			var end_date1 = new Date($("#end_year1").val(), em, 1);
			if(start_date1 > end_date1){
				$("#start_month1").parent().append("<div class='error datechk'>勤務開始日は勤務終了日よりも前の日付にして下さい。</div>");
				//$(".start_year1").removeClass("ok");
				//$(".end_year1").removeClass("ok");
			}
		}
	});
	$("select#start_month1").change(function() {
		if($("#start_year1").val() && $("#start_month1").val()){
			$(".start_year1").removeClass("hissu");
			$(".start_year1").addClass("ok");
			$("#start_month1").css("background-color","#FFF");
		}else{
			$(".start_year1").removeClass("ok");
			$(".start_year1").addClass("hissu");
			$("#start_month1").css("background-color","#f8f8f8");
		}
		if($("#start_year1").val() && $("#start_month1").val() && $("#end_year1").val() && $("#end_month1").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month1").val())-1;
			em = parseInt($("#end_month1").val())-1;
			var start_date1 = new Date($("#start_year1").val(), sm, 1);
			var end_date1 = new Date($("#end_year1").val(), em, 1);
			if(start_date1 > end_date1){
				$("#start_month1").parent().append("<div class='error datechk'>勤務開始日は勤務終了日よりも前の日付にして下さい。</div>");
			//	$(".start_year1").removeClass("ok");
				//$(".end_year1").removeClass("ok");
			}
		}

	});
	$("select#end_year1").change(function() {
		if($("#end_year1").val() && $("#end_month1").val()){
			$(".end_year1").addClass("ok");
			$("#end_year1").css("background-color","#FFF");
		}else{
			$(".end_year1").removeClass("ok");
			$("#end_year1").css("background-color","#f8f8f8");
		}
		if($("#end_year1").val()){
			$("#end_year1").css("background-color","#FFF");
			if($("#end_month1").val()){
				$("#end_month1").css("background-color","#FFF");
			}
		}
		if($("#start_year1").val() && $("#start_month1").val() && $("#end_year1").val() && $("#end_month1").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month1").val())-1;
			em = parseInt($("#end_month1").val())-1;
			var start_date1 = new Date($("#start_year1").val(), sm, 1);
			var end_date1 = new Date($("#end_year1").val(), em, 1);
			if(start_date1 > end_date1){
				$("#end_month1").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year1").removeClass("ok");
				//$(".end_month1").removeClass("ok");
			}
		}
	});
	$("select#end_month1").change(function() {
		if($("#end_year1").val() && $("#end_month1").val()){
			$(".end_year1").addClass("ok");
			$("#end_month1").css("background-color","#FFF");
		}else{
			$(".end_year1").removeClass("ok");
			$("#end_month1").css("background-color","#f8f8f8");
		}
		if($("#end_month1").val()){
			$("#end_month1").css("background-color","#FFF");
			if($("#end_year1").val()){
				$("#end_year1").css("background-color","#FFF");
			}
		}
		
		if($("#start_year1").val() && $("#start_month1").val() && $("#end_year1").val() && $("#end_month1").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month1").val())-1;
			em = parseInt($("#end_month1").val())-1;
			var start_date1 = new Date($("#start_year1").val(), sm, 1);
			var end_date1 = new Date($("#end_year1").val(), em, 1);
			if(start_date1 > end_date1){
				$("#end_month1").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year1").removeClass("ok");
				//$(".end_month1").removeClass("ok");
			}
		}
	});

	//在籍期間２
	$("select#start_year2").change(function() {
		if($("#start_year2").val() && $("#start_month2").val()){
			$(".start_year2").addClass("ok");
			$("#start_year2").css("background-color","#FFF");
		}else{
			$(".start_year2").removeClass("ok");
			$("#start_year2").css("background-color","#f8f8f8");
		}
		if($("#start_year2").val()){
			$("#start_year2").css("background-color","#FFF");
			if($("#start_month2").val()){
				$("#start_month2").css("background-color","#FFF");
			}
		}
		if($("#start_year2").val() && $("#start_month2").val() && $("#end_year2").val() && $("#end_month2").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month2").val())-1;
			em = parseInt($("#end_month2").val())-1;
			var start_date2 = new Date($("#start_year2").val(), sm, 1);
			var end_date2 = new Date($("#end_year2").val(), em, 1);
			if(start_date2 > end_date2){
				$("#start_month2").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year2").removeClass("ok");
			}
		}
	});
	$("select#start_month2").change(function() {
		if($("#start_year2").val() && $("#start_month2").val()){
			$(".start_year2").addClass("ok");
			$("#start_month2").css("background-color","#FFF");
		}else{
			$(".start_year2").removeClass("ok");
			$("#start_month2").css("background-color","#f8f8f8");
		}
		if($("#start_month2").val()){
			$("#start_month2").css("background-color","#FFF");
			if($("#start_year2").val()){
				$("#start_year2").css("background-color","#FFF");
			}
		}
		if($("#start_year2").val() && $("#start_month2").val() && $("#end_year2").val() && $("#end_month2").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month2").val())-1;
			em = parseInt($("#end_month2").val())-1;
			var start_date2 = new Date($("#start_year2").val(), sm, 1);
			var end_date2 = new Date($("#end_year2").val(), em, 1);
			if(start_date2 > end_date2){
				$("#start_month2").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year2").removeClass("ok");
			}
		}
	});
	$("select#end_year2").change(function() {
		if($("#end_year2").val() && $("#end_month2").val()){
			$(".end_year2").addClass("ok");
			$("#end_year2").css("background-color","#FFF");
		}else{
			$(".end_year2").removeClass("ok");
			$("#end_year2").css("background-color","#f8f8f8");
		}
		if($("#end_year2").val()){
			$("#end_year2").css("background-color","#FFF");
			if($("#end_month2").val()){
				$("#end_month2").css("background-color","#FFF");
			}
		}
		if($("#start_year2").val() && $("#start_month2").val() && $("#end_year2").val() && $("#end_month2").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month2").val())-1;
			em = parseInt($("#end_month2").val())-1;
			var start_date2 = new Date($("#start_year2").val(), sm, 1);
			var end_date2 = new Date($("#end_year2").val(), em, 1);
			if(start_date2 > end_date2){
				$("#end_month2").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year2").removeClass("ok");
			}
		}
	});
	$("select#end_month2").change(function() {
		if($("#end_year2").val() && $("#end_month2").val()){
			$(".end_year2").addClass("ok");
			$("#end_month2").css("background-color","#FFF");
		}else{
			$(".end_year2").removeClass("ok");
			$("#end_month2").css("background-color","#f8f8f8");
		}
		if($("#end_month2").val()){
			$("#end_month2").css("background-color","#FFF");
			if($("#end_year2").val()){
				$("#end_year2").css("background-color","#FFF");
			}
		}
		if($("#start_year2").val() && $("#start_month2").val() && $("#end_year2").val() && $("#end_month2").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month2").val())-1;
			em = parseInt($("#end_month2").val())-1;
			var start_date2 = new Date($("#start_year2").val(), sm, 1);
			var end_date2 = new Date($("#end_year2").val(), em, 1);
			if(start_date2 > end_date2){
				$("#end_month2").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year2").removeClass("ok");
			}
		}
	});

	//在籍期間３
	$("select#start_year3").change(function() {
		if($("#start_year3").val() && $("#start_month3").val()){
			$(".start_year3").addClass("ok");
			$("#start_year3").css("background-color","#FFF");
		}else{
			$(".start_year3").removeClass("ok");
			$("#start_year3").css("background-color","#f8f8f8");
		}
		if($("#start_year3").val()){
			$("#start_year3").css("background-color","#FFF");
			if($("#start_month3").val()){
				$("#start_month3").css("background-color","#FFF");
			}
		}
		if($("#start_year3").val() && $("#start_month3").val() && $("#end_year3").val() && $("#end_month3").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month3").val())-1;
			em = parseInt($("#end_month3").val())-1;
			var start_date3 = new Date($("#start_year3").val(), sm, 1);
			var end_date3 = new Date($("#end_year3").val(), em, 1);
			if(start_date3 > end_date3){
				$("#start_month3").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year3").removeClass("ok");
			}
		}
	});
	$("select#start_month3").change(function() {
		if($("#start_year3").val() && $("#start_month3").val()){
			$(".start_year3").addClass("ok");
			$("#start_month3").css("background-color","#FFF");
		}else{
			$(".start_year3").removeClass("ok");
			$("#start_month3").css("background-color","#f8f8f8");
		}
		if($("#start_month3").val()){
			$("#start_month3").css("background-color","#FFF");
			if($("#start_year3").val()){
				$("#start_year3").css("background-color","#FFF");
			}
		}
		if($("#start_year3").val() && $("#start_month3").val() && $("#end_year3").val() && $("#end_month3").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month3").val())-1;
			em = parseInt($("#end_month3").val())-1;
			var start_date3 = new Date($("#start_year3").val(), sm, 1);
			var end_date3 = new Date($("#end_year3").val(), em, 1);
			if(start_date3 > end_date3){
				$("#start_month3").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year3").removeClass("ok");
			}
		}
	});
	$("select#end_year3").change(function() {
		if($("#end_year3").val() && $("#end_month3").val()){
			$(".end_year3").addClass("ok");
			$("#end_year3").css("background-color","#FFF");
		}else{
			$(".end_year3").removeClass("ok");
			$("#end_year3").css("background-color","#f8f8f8");
		}
		if($("#end_year3").val()){
			$("#end_year3").css("background-color","#FFF");
			if($("#end_month3").val()){
				$("#end_month3").css("background-color","#FFF");
			}
		}
		if($("#start_year3").val() && $("#start_month3").val() && $("#end_year3").val() && $("#end_month3").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month3").val())-1;
			em = parseInt($("#end_month3").val())-1;
			var start_date3 = new Date($("#start_year3").val(), sm, 1);
			var end_date3 = new Date($("#end_year3").val(), em, 1);
			if(start_date3 > end_date3){
				$("#end_month3").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
			//	$(".end_year3").removeClass("ok");
			}
		}
	});
	$("select#end_month3").change(function() {
		if($("#end_year3").val() && $("#end_month3").val()){
			$(".end_year3").addClass("ok");
			$("#end_month3").css("background-color","#FFF");
		}else{
			$(".end_year3").removeClass("ok");
			$("#end_month3").css("background-color","#f8f8f8");
		}
		if($("#end_month3").val()){
			$("#end_month3").css("background-color","#FFF");
			if($("#end_year3").val()){
				$("#end_year3").css("background-color","#FFF");
			}
		}
		if($("#start_year3").val() && $("#start_month3").val() && $("#end_year3").val() && $("#end_month3").val()){
			$(".start_year3").removeClass("ok");
			$(".datechk").remove();
			sm = parseInt($("#start_month3").val())-1;
			em = parseInt($("#end_month3").val())-1;
			var start_date3 = new Date($("#start_year3").val(), sm, 1);
			var end_date3 = new Date($("#end_year3").val(), em, 1);
			if(start_date3 > end_date3){
				$("#end_month3").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
			//	$(".end_year3").removeClass("ok");
			}
		}
	});

	//希望勤務地１
	$("select#kibouarea1").change(function() {
		if($("#kibouarea1").val()){
			$(".kibouarea1").removeClass("hissu");
			$(".kibouarea1").addClass("ok");
			$(".kibouarea1").attr("src", "/mypage/images/ico_ok.png");
		}else{
			$(".kibouarea1").removeClass("ok");
			$(".kibouarea1").addClass("hissu");
			$(".kibouarea1").attr("src", "/mypage/images/ico_hissu.png");
		}
	});

	//希望勤務地２
	$("select#kibouarea2").change(function() {
		if($("#kibouarea2").val()){
			$(".kibouarea2").removeClass("hissu");
			$(".kibouarea2").addClass("ok");
			$(".kibouarea2").attr("src", "/mypage/images/ico_ok.png");
		}else{
			$(".kibouarea2").removeClass("ok");
			$(".kibouarea2").addClass("");
			$(".kibouarea2").attr("src", "/mypage/images/ico_nini.png");
		}
	});

	//希望勤務地３
	$("select#kibouarea3").change(function() {
		if($("#kibouarea3").val()){
			$(".kibouarea3").removeClass("hissu");
			$(".kibouarea3").addClass("ok");
			$(".kibouarea3").attr("src", "/mypage/images/ico_ok.png");
		}else{
			$(".kibouarea3").removeClass("ok");
			$(".kibouarea3").addClass("");
			$(".kibouarea3").attr("src", "/mypage/images/ico_nini.png");
		}
	});



	$(":radio").change(function(){
		var tmp_name = $(this).attr("name");
		var radio_tmp = document.getElementsByName(tmp_name);
		var cnt = 0;
		tmp = tmp_name.replace("[]", "");
		for(var i=0;i<radio_tmp.length;i++) {
			if (radio_tmp[i].checked) {
				cnt = 1;
			}
		}
		if(cnt == 0){
			$(this).parent().css("background-color","#f8f8f8");
		}else{
			$(this).parent().css("background-color","#fff");
		}
	});

	$("select").change(function(){
		cls = $(this).attr("class");
		if(cls.match(/required/)){
			tmpId = $(this).attr("id");
			if($(this).val()){
				$(this).css("background-color","#FFF");
			}else{
				$(this).css("background-color","#f8f8f8");
			}
		}
	});

	$("select[name='renraku1']").change(function(){
		renraku1 = $("select[name='renraku1']").val();
		renraku2 = $("select[name='renraku2']").val();
		if(!renraku1 && !renraku2){
			$("#renraku1").css("background-color","#f8f8f8");
			$("#renraku2").css("background-color","#f8f8f8");
		}else{
			$("#renraku1").css("background-color","#FFF");
			$("#renraku2").css("background-color","#FFF");
		}
	});
	$("select[name='renraku2']").change(function(){
		renraku1 = $("select[name='renraku1']").val();
		renraku2 = $("select[name='renraku2']").val();
		if(!renraku1 && !renraku2){
			$("#renraku1").css("background-color","#f8f8f8");
			$("#renraku2").css("background-color","#f8f8f8");
		}else{
			$("#renraku1").css("background-color","#FFF");
			$("#renraku2").css("background-color","#FFF");
		}
	});


	$("select[name='kibo_jiki_year']").change(function(){
		kibo_jiki_year = $("select[name='kibo_jiki_year']").val();
		kibo_jiki_month = $("select[name='kibo_jiki_month']").val();
		kibou_jiki = $("#kibou_jiki").prop('checked');

		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month)){
			$("#kibo_jiki_year").css("background-color","#f8f8f8");
			$("#kibo_jiki_month").css("background-color","#f8f8f8");
			$("#kb").css("background-color","#fffafa");
		}else{
			$("#kibo_jiki_year").css("background-color","#FFF");
			$("#kibo_jiki_month").css("background-color","#FFF");
			$("#kb").css("background-color","#FFF");
		}
	});

	$("select[name='kibo_jiki_month']").change(function(){
		kibo_jiki_year = $("select[name='kibo_jiki_year']").val();
		kibo_jiki_month = $("select[name='kibo_jiki_month']").val();
		kibou_jiki = $("#kibou_jiki").prop('checked');

		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month)){
			$("#kibo_jiki_year").css("background-color","#f8f8f8");
			$("#kibo_jiki_month").css("background-color","#f8f8f8");
			$("#kb").css("background-color","#fffafa");
		}else{
			$("#kibo_jiki_year").css("background-color","#FFF");
			$("#kibo_jiki_month").css("background-color","#FFF");
			$("#kb").css("background-color","#FFF");
		}
	});

	$("#kibou_jiki").change(function(){
		kibo_jiki_year = $("select[name='kibo_jiki_year']").val();
		kibo_jiki_month = $("select[name='kibo_jiki_month']").val();
		kibou_jiki = $("#kibou_jiki").prop('checked');

		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month)){
			$("#kibo_jiki_year").css("background-color","#f8f8f8");
			$("#kibo_jiki_month").css("background-color","#f8f8f8");
			$("#kb").css("background-color","#fffafa");
		}else{
			$("#kibo_jiki_year").css("background-color","#FFF");
			$("#kibo_jiki_month").css("background-color","#FFF");
			$("#kb").css("background-color","#FFF");
		}
	});

	//希望職種
    //職種の一括チェック
    $('.joballchk').change(function(){
                    if ($(this).is(':checked')) {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', true);
	                $(this).next(".joballnavichecklabel").addClass('checked');
	                $(this).parent().find(".jobnavichecklabel").addClass('checked');
                    }
                    else {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', false);
	                $(this).next(".joballnavichecklabel").removeClass('checked');
                        $(this).parents('li').each(function(){
                            $(this).children('input[type="checkbox"]').prop('checked', false);
                        });
	                $(this).parent().find(".jobnavichecklabel").removeClass('checked');
                    }

		    //チェックされているかの確認
	    	    cnt = $('.jobcheckbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".jobcheckbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".kiboujob").removeClass("");
			$(".kiboujob").addClass("ok");
			$(".kiboujob").attr("src", "/mypage/images/ico_ok.png");
	    	    }else{
			$(".kiboujob").removeClass("ok");
			$(".kiboujob").addClass("");
			$(".kiboujob").attr("src", "/mypage/images/ico_hissu.png");
	    	    }
    });
    $('.jobchk').click(function () {
        var idx = $(".jobchk").index(this);
        var cnt = $('.jobchk').length;
	if(!$(this).is(':checked')){
		cls = $(this).attr('class');
		term = cls.split( ' ' );
		label_id = term[0];
                $("#"+label_id).removeClass('checked');
	}

        for (i = 0; i < cnt; i++) {
            if ($(".jobchk").eq(i).prop("checked")) {
                $(".jobnavichecklabel").eq(i).addClass('checked');
            } else {
                $(".jobnavichecklabel").eq(i).removeClass('checked');
            }
        }
		    //チェックされているかの確認
	    	    cnt = $('.jobcheckbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".jobcheckbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".kiboujob").removeClass("");
			$(".kiboujob").addClass("ok");
			$(".kiboujob").attr("src", "/mypage/images/ico_ok.png");
	    	    }else{
			$(".kiboujob").removeClass("ok");
			$(".kiboujob").addClass("");
			$(".kiboujob").attr("src", "/mypage/images/ico_hissu.png");
	    	    }

    });

    //担当職種１
    $('.tantoujob1allchk').change(function(){
                    if ($(this).is(':checked')) {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', true);
	                $(this).next(".tantoujob1allnavichecklabel").addClass('checked');
	                $(this).parent().find(".tantoujob1navichecklabel").addClass('checked');
                    }
                    else {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', false);
	                $(this).next(".tantoujob1allnavichecklabel").removeClass('checked');
                        $(this).parents('li').each(function(){
                            $(this).children('input[type="checkbox"]').prop('checked', false);
                        });
	                $(this).parent().find(".tantoujob1navichecklabel").removeClass('checked');
                    }

		    //チェックされているかの確認
	    	    cnt = $('.tantoujob1checkbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".tantoujob1checkbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".tantou_job1").removeClass("");
			$(".tantou_job1").addClass("ok");
	    	    }else{
			$(".tantou_job1").removeClass("ok");
			$(".tantou_job1").addClass("");
	    	    }
    });

    //担当職種２
    $('.tantoujob2allchk').change(function(){
                    if ($(this).is(':checked')) {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', true);
	                $(this).next(".tantoujob2allnavichecklabel").addClass('checked');
	                $(this).parent().find(".tantoujob2navichecklabel").addClass('checked');
                    }
                    else {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', false);
	                $(this).next(".tantoujob2allnavichecklabel").removeClass('checked');
                        $(this).parents('li').each(function(){
                            $(this).children('input[type="checkbox"]').prop('checked', false);
                        });
	                $(this).parent().find(".tantoujob2navichecklabel").removeClass('checked');
                    }

		    //チェックされているかの確認
	    	    cnt = $('.tantoujob2checkbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".tantoujob2checkbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".tantou_job2").removeClass("");
			$(".tantou_job2").addClass("ok");
	    	    }else{
			$(".tantou_job2").removeClass("ok");
			$(".tantou_job2").addClass("");
	    	    }
    });

    //担当職種３
    $('.tantoujob3allchk').change(function(){
                    if ($(this).is(':checked')) {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', true);
	                $(this).next(".tantoujob3allnavichecklabel").addClass('checked');
	                $(this).parent().find(".tantoujob3navichecklabel").addClass('checked');
                    }
                    else {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', false);
	                $(this).next(".tantoujob3allnavichecklabel").removeClass('checked');
                        $(this).parents('li').each(function(){
                            $(this).children('input[type="checkbox"]').prop('checked', false);
                        });
	                $(this).parent().find(".tantoujob3navichecklabel").removeClass('checked');
                    }

		    //チェックされているかの確認
	    	    cnt = $('.tantoujob3checkbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".tantoujob3checkbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".tantou_job3").removeClass("");
			$(".tantou_job3").addClass("ok");
	    	    }else{
			$(".tantou_job3").removeClass("ok");
			$(".tantou_job3").addClass("");
	    	    }
    });



    //転勤
    $('.tenkincheckbox').click(function () {
		    //チェックされているかの確認
	    	    cnt = $('.tenkincheckbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".tenkincheckbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".tenkin").removeClass("");
			$(".tenkin").addClass("ok");
			$(".tenkin").attr("src", "/mypage/images/ico_ok.png");

	    	    }else{
			$(".tenkin").removeClass("ok");
			$(".tenkin").addClass("hissu");
			$(".tenkin").attr("src", "/mypage/images/ico_hissu.png");
	    	    }

    });


    //収入の切り替え
	$("select#income").change(function() {
		if($("#income").val()){
			$(".income").removeClass("hissu");
			$(".income").addClass("ok");
			$(".income").attr("src", "/mypage/images/ico_ok.png");
		}else{
			$(".income").removeClass("ok");
			$(".income").addClass("hissu");
			$("#income").css("background-color","#f8f8f8");
			$(".income").attr("src", "/mypage/images/ico_hissu.png");
		}
	});

	$("select#earnings").change(function() {
		if($("#earnings").val()){
			$(".earnings").removeClass("hissu");
			$(".earnings").addClass("ok");
		}else{
			$(".earnings").removeClass("ok");
			$(".earnings").addClass("hissu");
			$("#earnings").css("background-color","#f8f8f8");
		}
	});

	$("#agreeChk").change(function(){
		agr = $("#agreeChk").prop('checked');
		if(agr){
			$("#btnKakunin").attr("src","img/btn_check_on.png");
	                       $("#agr").addClass('checked');
		}else{
			$("#btnKakunin").attr("src","img/btn_check_off.png");
	                       $("#agr").removeClass('checked');
		}
/*
		if(agr==0){
			$(this).parent().css("background-color","#fffafa");
		}else{
			$(this).parent().css("background-color","#fff");
		}
*/
	});
});

function check(param){

	window.onbeforeunload=null;

//$(function(){
	var tmp = "";
//	$("form").submit(function(){
		//エラーの初期化
		$("div.error").remove();
		$("div.mailerr").remove();

		$("td").removeClass("error");
		
		//テキスト、テキストエリアのチェック
		$(":text, textarea").filter(".required").each(function(){
			if($(this).val()==""){
				tmp = $(this).attr("id");
				$(this).parent().append("<div class='error'>" + data[tmp] + "を入力してください。</div>");
			}

			//数値のチェック
			$(this).filter(".number").each(function(){
				if(isNaN($(this).val())){
					$(this).parent().append("<div class='error'>数値のみ選択可能です。</div>");
				}
			});

		
/*
			//メールアドレスチェック
			$(this).filter(".mail").each(function(){
				if($(this).val() && !$(this).val().match(/.+@.+\..+/g)){
					$(this).parent().append("<div class='error'>メールアドレスの形式が異なります。</div>");
				}
			});
		
			//メールアドレス確認用
			$(this).filter(".mail_chk").each(function(){
				if($(this).val() && $(this).val() != $("input[name=" + $(this).attr("name").replace(/^(.+)_chk$/, "$1")+"]").val()){
					$(this).parent().append("<div class='error'>メールアドレスが一致しません。</div>");
				}
			});
*/
		});
		

		//ラジオボタンのチェック
		$(":radio").filter(".required").each(function(){
			var tmp_name = $(this).attr("name");
			var radio_tmp = document.getElementsByName(tmp_name);
			var cnt = 0;
			tmp = tmp_name.replace("[]", "");
			for(var i=0;i<radio_tmp.length;i++) {
				if (radio_tmp[i].checked) {
					cnt = 1;
				}
			}
			if(cnt == 0){
				$("#"+tmp_name+"_err").parent().append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
			}
		});
		
		//リストのチェック
		$("select").filter(".required").each(function(){
			if($("select[name="+$(this).attr("name")+"]").children(':selected').val() == ""){
				tmp = $(this).attr("id");
				$(this).parent().append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
			}
		});
		
		//チェックボックスのチェック
		$(".checkboxrequired").each(function(){
			if($(":checkbox:checked", this).size() == 0){
				tmp = $("input",this).attr("name");
				//name属性を配列にしているために[]を削除
				tmp = tmp.replace("[]", "");
				if(tmp=="agree"){
					$(this).append("<div class='error'>" + data[tmp] + "</div>");
				}else{
					$(this).append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
				}
			}
		});

		//環境依存文字簡易チェック
		$(":text, textarea").filter(".izon").each(function(){
			var show = $(this).attr("id");
			var text = $(this).val();
			var c_regP = "[①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳ⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩ㍉㌔㌢㍍㌘㌧㌃㌶㍑㍗㌍㌦㌣㌫㍊㌻㎜㎝㎞㎎㎏㏄㎡㍻〝〟№㏍℡㊤㊥㊦㊧㊨㈱㈲㈹㍾㍽㍼]";
			if(text.match(c_regP)){
				$(this).parent().append("<div class='error'>" + data[show] + "に環境依存文字が入力されています。</div>");
			}
		});
		

		//日付チェック
		if($("#start_year1").val() && $("#start_month1").val() && $("#end_year1").val() && $("#end_month1").val()){
			sm = parseInt($("#start_month1").val())-1;
			em = parseInt($("#end_month1").val())-1;
			var start_date1 = new Date($("#start_year1").val(), sm, 1);
			var end_date1 = new Date($("#end_year1").val(), em, 1);
			if(start_date1 > end_date1){
				$("#end_month1").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
			//	$(".end_year1").removeClass("ok");
			}
		}

		if($("#start_year2").val() && $("#start_month2").val() && $("#end_year2").val() && $("#end_month2").val()){
			sm = parseInt($("#start_month2").val())-1;
			em = parseInt($("#end_month2").val())-1;
			var start_date2 = new Date($("#start_year2").val(), sm, 1);
			var end_date2 = new Date($("#end_year2").val(), em, 1);
			if(start_date2 > end_date2){
				$("#end_month2").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
			//	$(".end_year2").removeClass("ok");
			}
		}

		if($("#start_year3").val() && $("#start_month3").val() && $("#end_year3").val() && $("#end_month3").val()){
			sm = parseInt($("#start_month3").val())-1;
			em = parseInt($("#end_month3").val())-1;
			var start_date3 = new Date($("#start_year3").val(), sm, 1);
			var end_date3 = new Date($("#end_year3").val(), em, 1);
			if(start_date3 > end_date3){
				$("#end_month3").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
			//	$(".end_year3").removeClass("ok");
			}
		}
		e1 = $("#kibouarea1").val();
		e2 = $("#kibouarea2").val();
		e3 = $("#kibouarea3").val();

		if(e1 && e2){
			if(e1 == e2){
				$("#kibouarea1").parent().append("<div class='error'>勤務地（第1希望）と勤務地（第2希望）は別の都道府県を選択してください。</div>");
			}
		}
		if(e1 && e3){
			if(e1 == e3){
				$("#kibouarea1").parent().append("<div class='error'>勤務地（第1希望）と勤務地（第3希望）は別の都道府県を選択してください。</div>");
			}
		}
		if(e2 && e3){
			if(e2 == e3){
				$("#kibouarea2").parent().append("<div class='error'>勤務地（第2希望）と勤務地（第3希望）は別の都道府県を選択してください。</div>");
			}
		}

		//エラー処理
		if($("div.error").size() > 0){
			$('html,body').animate({ scrollTop: $("div.error:first").offset().top-150}, 'slow');
			return false;
		}else{
			return true;
		}

//	});
//});
}