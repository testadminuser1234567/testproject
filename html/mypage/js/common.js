$(function(){
var ua = navigator.userAgent;
	if (ua.indexOf('iPhone') < 0 && ua.indexOf('Android') < 0) {
		$('a[href^="tel:"]').css('cursor', 'default').click(function (event) {
			event.preventDefault();
		});
	}
});

$(function(){
	$('.searchBox p.category').click(function() {
		$(this).next().slideToggle();
	}).next().hide();
});

// sp用ナビゲーション

$(function(){
    $("p#menuBtn").click(function(){
        $("div#menuBox").fadeToggle();
    });
    $("p.btnMenuClose").click(function(){
        $("div#menuBox").fadeToggle();
	})
});


$(function () {
    //    var checkbox = $('input[type="checkbox"]');
    var radio = $('input[type="radio"]');

    radioChecked();
    radio.on('click', function () {
        radioChecked();
    });


    //チェックボックスがチェックされているか（モーダルの戻るボタンの挙動）
    var cnt = $('.chk').length;
    var checked = 0;
    for (i = 0; i < cnt; i++) {
        if ($(".chk").eq(i).prop("checked")) {
            checked++;
            $(".navichecklabel").eq(i).addClass('checked');
        }
    }

    // チェックボックス
    $('.chk').click(function () {
        var idx = $(".chk").index(this);
        var cnt = $('.chk').length;
        for (i = 0; i < cnt; i++) {
            if ($(".chk").eq(i).prop("checked")) {
                $(".navichecklabel").eq(i).addClass('checked');
            } else {
                $(".navichecklabel").eq(i).removeClass('checked');
            }
        }
    });

    //職種の一括チェック
    $('.searchjoballchk').change(function(){
                    if ($(this).is(':checked')) {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', true);
	                $(this).next(".searchjoballnavichecklabel").addClass('checked');
	                $(this).parent().find(".searchjobnavichecklabel").addClass('checked');
                    }
                    else {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', false);
	                $(this).next(".searchjoballnavichecklabel").removeClass('checked');
                        $(this).parents('li').each(function(){
                            $(this).children('input[type="checkbox"]').prop('checked', false);
                        });
	                $(this).parent().find(".searchjobnavichecklabel").removeClass('checked');
                    }

    });
    $('.searchjobchk').click(function () {
        var idx = $(".searchjobchk").index(this);
        var cnt = $('.searchjobchk').length;
	if(!$(this).is(':checked')){
		cls = $(this).attr('class');
		term = cls.split( ' ' );
		label_id = term[0];
                $("#"+label_id).removeClass('checked');
	}

        for (i = 0; i < cnt; i++) {
            if ($(".searchjobchk").eq(i).prop("checked")) {
                $(".searchjobnavichecklabel").eq(i).addClass('checked');
            } else {
                $(".searchjobnavichecklabel").eq(i).removeClass('checked');
            }
        }
    });

    //都道府県の一括チェック
    $('.searchareaallchk').change(function(){
                    if ($(this).is(':checked')) {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', true);
	                $(this).next(".searchareaallnavichecklabel").addClass('checked');
	                $(this).parent().find(".searchareanavichecklabel").addClass('checked');
                    }
                    else {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', false);
	                $(this).next(".searchareaallnavichecklabel").removeClass('checked');
                        $(this).parents('li').each(function(){
                            $(this).children('input[type="checkbox"]').prop('checked', false);
                        });
	                $(this).parent().find(".searchareanavichecklabel").removeClass('checked');
                    }
    });
    $('.searchareachk').click(function () {
        var idx = $(".searchareachk").index(this);
        var cnt = $('.searchareachk').length;
	if(!$(this).is(':checked')){
		cls = $(this).attr('class');
		term = cls.split( ' ' );
		label_id = term[0];
                $("#"+label_id).removeClass('checked');
	}

        for (i = 0; i < cnt; i++) {
            if ($(".searchareachk").eq(i).prop("checked")) {
                $(".searchareanavichecklabel").eq(i).addClass('checked');
            } else {
                $(".searchareanavichecklabel").eq(i).removeClass('checked');
            }
        }
    });

    //収入の切り替え

    $("#year_income").change(function () {
        if ($("#year_income").val()) {
            $("#year_income").addClass("checked");
        } else {
            $("#year_income").removeClass("checked");
        }
    });

    function radioChecked() {
        radio.each(function () {
            if ($(this).prop('checked')) {
                $(this).parent().addClass('checked');
            } else {
                $(this).parent().removeClass('checked');
            }
        });
    }

//チェンジイベント
$(function(){
    $('ul input[type="checkbox"]').change(function(){
        if ($(this).is(':checked')) {
            $(this).parent().find('input[type="checkbox"]').prop('checked', true);
        }
        else {
            $(this).parent().find('input[type="checkbox"]').prop('checked', false);
            $(this).parents('li').each(function(){
                $(this).children('input[type="checkbox"]').prop('checked', false);
            });
        }
    });

    //form clear
    $(".clearForm").bind("click",function(){
	$(".searchjoballnavichecklabel").removeClass('checked');
	$(".searchjobnavichecklabel").removeClass('checked');
	$(".searchareaallnavichecklabel").removeClass('checked');
	$(".searchareanavichecklabel").removeClass('checked');
	$("#year_income").removeClass("checked");
        radio.each(function () {
                $(this).parent().removeClass('checked');
        });
	$(this.form).find("textarea, :text, select").val("").end().find(":checked").prop("checked", false);
    });

})


    $(document).on("open", ".remodal", function () {
        console.log("open");
    });
    $(document).on("opened", ".remodal", function () {
        console.log("opened");
    });
    $(document).on("close", ".remodal", function (e) {
        console.log('close' + (e.reason ? ", reason: " + e.reason : ''));
    });
    $(document).on("closed", ".remodal", function (e) {
        console.log('closed' + (e.reason ? ', reason: ' + e.reason : ''));
    });
    $(document).on("confirm", ".remodal", function () {
        console.log("confirm");
    });
    $(document).on("cancel", ".remodal", function () {
        console.log("cancel");
    });




});

