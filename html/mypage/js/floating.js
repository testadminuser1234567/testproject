$(function() {

	var fl = $('#floating');
    var flHeight = $('#floating').outerHeight();

if($('#floating').length){
	var flTop = fl.offset().top+300;
	var showFlag = false;
	
	
	$(window).scroll(function () {
		var winTop = $(this).scrollTop();
		if (winTop >= flTop) {
			if (showFlag == false) {
				showFlag = true;
				fl.addClass('fixed').stop().animate({'top' : '0px'}, 200);
		                $('body').css('padding-top',flHeight)
			}
		} else if (winTop <= flTop) {
			if (showFlag) {
				showFlag = false;
				
                
                		if (window.matchMedia('screen and (min-width:768px)').matches) { 
                		        fl.stop().animate({'top' : -(flHeight)}, 200, function(){
        					fl.removeClass('fixed');
        					$('body').css('padding-top','0')
                		        });
				}else{
					fl.stop().animate({'top' : -(flHeight)}, 200, function(){
						fl.removeClass('fixed');
						$('body').css('padding-top','0')
					});
	        	        }
			}
		}
	});
}
	
	
});