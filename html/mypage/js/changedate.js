function changedate() {
	// フォームと各年月日のname属性を指定
	// ・フォーム
	// document.name属性;
	// ・年月日
	// formN.name属性;
	var formN	= document.form;
	var tYear = document.getElementById("year");
	var tMonth	= document.getElementById("month");
	var tDays	= document.getElementById("day");

	var selectY = tYear.options[tYear.selectedIndex].value;
	var selectM = tMonth.options[tMonth.selectedIndex].value;
	var selectD = tDays.options[tDays.selectedIndex].value;

	tDays.length = 0;
	for (var i = 1;i <= 31;i++) {
		var dateObj = new Date(selectY, selectM - 1, i);
		if (dateObj.getFullYear()	== selectY &&
			dateObj.getMonth() + 1	== selectM &&
			dateObj.getDate()		== i) {
			tDays.options[i] = new Option(i, i);
			if (selectD == i) {
				tDays.options[i].selected = true;
			}
		}
	}
	tDays.removeChild(tDays.options[0]);
	if (selectD > tDays.length) {
		tDays.options[tDays.length - 1].selected = true;
	}
}
