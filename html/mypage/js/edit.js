var viewTextMode = 0;
$(function(){
	var iframe = '<iframe width="700px" height="500px" id="edit"></iframe>';
	document.getElementById('textArea').style.display = "none";
	document.getElementById('textArea').insertAdjacentHTML("afterEnd", iframe);
	
	var content = document.getElementById('textArea').value;
	var doc = document.getElementById("edit").contentWindow.document;

	doc.open();
	doc.write(content);
	doc.close();

	doc.body.contentEditable = true;
	doc.designMode = "on";

	$("form").submit(function(){
		var data = document.getElementById("edit").contentWindow.document.body.innerHTML;
		var rep = data.replace(/<div>/g,'');
		var rep2 = rep.replace(/<\u002fdiv>/g,'<br>');
		$("#textArea").val(rep2);
	});
}) 

function urlFormat(target, type, selected){
	if(!$("#url").val()){
		alert("urlを入力してください。");
		return false;
	}

	var url = $("#url").val();
	var doc = document.getElementById(target);
	doc.contentWindow.focus();
	doc.contentWindow.document.execCommand(type,false,url);

	
}

function imageFormat(target, type, selected, url, module){
	var image ="";
	var imageUrl="";
	$('#upload1').upload(module, function(res) {
		var tmp = res.substring(0,2);
		if(tmp!="ok"){
			alert(res);
			return false;
		}else{
			image = res.substring(2);
			imageUrl = url + "/uploads/" + image;
			var doc = document.getElementById(target);
			doc.contentWindow.focus();
			doc.contentWindow.document.execCommand(type,false,imageUrl);		}
        }, 'text');
}