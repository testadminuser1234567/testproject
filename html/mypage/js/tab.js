$(function () {
    var tabContainer = $('div#tab div.tabSub');
    tabContainer.hide().filter(':first').show();
    $('div#tab ul#tabNavi a').click(function () {
       tabContainer.hide();
       tabContainer.filter(this.hash).show();
       $('div#tab ul#tabNavi a').removeClass('selected');
       $(this).addClass('selected');
       return false;
    })
    .filter(':first').click();
});