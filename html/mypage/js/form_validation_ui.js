$(function(){
    $(window).on('beforeunload', function() {
        return "このページから移動してもよろしいですか？";
    });
});

//エラー処理用
var data = {};
data.shimei    = "氏名";
data.kana     = "ふりがな";
data.year    = "年";
data.month    = "月";
data.day    = "日";
data.sex    = "性別";
data.zip      = "郵便番号";
data.pref      = "住所";
data.address      = "住所";
data.tel1       = "電話";
data.mail1      = "メール";
data.mail2      = "メール（携帯）";
data.agree = "個人情報保護方針の同意にチェックをお願い致します。";
data.kibo_pref1="希望勤務地";
data.school_div_id="最終学歴";
data.school="学校名";
data.company_number="経験社数";
data.jokyo="就業状況";
data.kibo_job1="希望職種";
data.expectarea1="勤務地（第1希望）";
data.school_name="学校名/学部/学科";
data.comment="ご希望の参加日";
data.kiboubi="ご希望の参加日";
data.kibouarea1="希望勤務地：第１希望";
data.kibouarea2="希望勤務地：第２希望";
data.kibouarea3="希望勤務地：第３希望";


$(function(){

	//////////////////////////////////////////////////////
	//　return 戻るボタンで戻ったときの背景対策
	//////////////////////////////////////////////////////
	if($("#shimei").val()){
		$("#shimei").css("background-color","#FFF");
	}
	if($("#kana").val()){
		$("#kana").css("background-color","#FFF");
	}
	if($("#year").val()){
		$("#year").css("background-color","#FFF");
	}
	if($("#month").val()){
		$("#month").css("background-color","#FFF");
	}
	if($("#day").val()){
		$("#day").css("background-color","#FFF");
	}
	cnt_tmp = $('.sexradio').length;
	checked_tmp = 0;
	for(i=0;i<cnt_tmp;i++){
		if($(".sexradio").eq(i).prop("checked")){
			 checked_tmp++;
		}
	}
	if (checked_tmp > 0) {
		$(".sexlabel").css("background-color","#FFF");
	}
	if($("#pref").val()){
		$("#pref").css("background-color","#FFF");
	}
	if($("#tel1").val()){
		$("#tel1").css("background-color","#FFF");
	}
	if($("#mail1").val()){
		$("#mail1").css("background-color","#FFF");
	}
	if($("#school_name").val()){
		$("#school_name").css("background-color","#FFF");
	}
	if($("#comment").val()){
		$("#comment").css("background-color","#FFF");
	}
	if($("#password").val()){
		$("#password").css("background-color","#FFF");
	}
	if($("#password_check").val()){
		$("#password_check").css("background-color","#FFF");
	}
	cnt_tmp = $('.gakurekiradio').length;
	checked_tmp = 0;
	for(i=0;i<cnt_tmp;i++){
		if($(".gakurekiradio").eq(i).prop("checked")){
			 checked_tmp++;
		}
	}
	if (checked_tmp > 0) {
		$(".gakurekilabel").css("background-color","#FFF");
	}
	cnt_tmp = $('.compradio').length;
	checked_tmp = 0;
	for(i=0;i<cnt_tmp;i++){
		if($(".compradio").eq(i).prop("checked")){
			 checked_tmp++;
		}
	}
	if (checked_tmp > 0) {
		$(".companylabel").css("background-color","#FFF");
	}
	cnt_tmp = $('.jokyoradio').length;
	checked_tmp = 0;
	for(i=0;i<cnt_tmp;i++){
		if($(".jokyoradio").eq(i).prop("checked")){
			 checked_tmp++;
		}
	}
	if (checked_tmp > 0) {
		$(".jokyolabel").css("background-color","#FFF");
	}
	if($("#expectarea1").val()){
		$("#expectarea1").css("background-color","#FFF");
	}
	//////////////////////////////////////////////////////
	// return ここまで
	//////////////////////////////////////////////////////

	//参加日
	$("textarea#comment").blur(function() {
		if($("#comment").val()){
			$(".comment").removeClass("");
			$(".comment").addClass("ok");
			$(".comment").attr("src", "/mypage/images/ico_ok.png");
			$("#comment").css("background-color","#FFF");
		}else{
			$(".comment").removeClass("ok");
			$(".comment").addClass("");
			$(".comment").attr("src", "/mypage/images/ico_nini.png");
			$("#comment").css("background-color","#fffafa");
		}
	});

	//パスワード
	$("input#password").blur(function() {
		$("div.password_err").remove();
		pwd = $("#password").val();
		if(pwd){
			if(!pwd.match(/^(?=.*?[a-z])(?=.*?\d)[a-z\d]{8,20}$/i)){
				$("#password_err").append("<div class='password_err error'>入力エラー：パスワードは8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</div>");
				$(".password").removeClass("ok");
				$(".password").addClass("hissu");
				$(".password").attr("src", "/mypage/images/ico_hissu.png");
				$("#password").css("background-color","#fffafa");
			}else{
				$(".password").removeClass("hissu");
				$(".password").addClass("ok");
				$(".password").attr("src", "/mypage/images/ico_ok.png");
				$("#password").css("background-color","#FFF");
			}
		}else{
			$("#password_err").append("<div class='password_err error'>入力エラー：パスワードを入力してください。</div>");
			$(".password").removeClass("ok");
			$(".password").addClass("hissu");
			$(".password").attr("src", "/mypage/images/ico_hissu.png");
			$("#password").css("background-color","#fffafa");
		}
	});
	$("input#password_check").blur(function() {
		$("div.password_check_err").remove();
		pwd = $("#password").val();
		pwd_check = $("#password_check").val();
		if(pwd_check){
			if(!pwd_check.match(/^(?=.*?[a-z])(?=.*?\d)[a-z\d]{8,20}$/i)){
				$("#password_check_err").append("<div class='password_check_err error'>入力エラー：パスワード(確認）は8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</div>");
				$(".password_check").removeClass("ok");
				$(".password_check").addClass("hissu");
				$(".password_check").attr("src", "/mypage/images/ico_hissu.png");
				$("#password_check").css("background-color","#fffafa");
			}else if(pwd != pwd_check){
				$("#password_check_err").append("<div class='password_check_err error'>入力エラー：パスワードとパスワード（確認）の入力値が異なります。</div>");
				$(".password_check").removeClass("ok");
				$(".password_check").addClass("hissu");
				$(".password_check").attr("src", "/mypage/images/ico_hissu.png");
				$("#password_check").css("background-color","#fffafa");
			}else{
				$(".password_check").removeClass("hissu");
				$(".password_check").addClass("ok");
				$(".password_check").attr("src", "/mypage/images/ico_ok.png");
				$("#password_check").css("background-color","#FFF");
			}
		}else{
			$("#password_check_err").append("<div class='password_check_err error'>入力エラー：パスワード（確認）を入力してください。</div>");
			$(".password_check").removeClass("ok");
			$(".password_check").addClass("hissu");
			$(".password_check").attr("src", "/mypage/images/ico_hissu.png");
			$("#password_check").css("background-color","#fffafa");
		}
	});


	//////////////////////////////////////////////////////////////////

	$(":radio").change(function(){
		var tmp_name = $(this).attr("name");
		var radio_tmp = document.getElementsByName(tmp_name);
		var cnt = 0;
		tmp = tmp_name.replace("[]", "");
		for(var i=0;i<radio_tmp.length;i++) {
			if (radio_tmp[i].checked) {
				cnt = 1;
			}
		}
		if(cnt == 0){
			$(this).parent().css("background-color","#fffafa");
		}else{
			$(this).parent().css("background-color","#fff");
		}
	});

	$("select").change(function(){
		cls = $(this).attr("class");
		if(cls.match(/required/)){
			tmpId = $(this).attr("id");
			if($(this).val()){
				$(this).css("background-color","#FFF");
			}else{
				$(this).css("background-color","#fffafa");
			}
		}
	});

	$("#agreeChk").change(function(){
		agr = $("#agreeChk").prop('checked');
		if(agr){
			$("#btnKakunin").attr("src","img/btn_check_on.png");
	                       $("#agr").addClass('checked');
		}else{
			$("#btnKakunin").attr("src","img/btn_check_off.png");
	                       $("#agr").removeClass('checked');
		}
	});
});

function check(param){


	window.onbeforeunload=null;

//$(function(){
	var tmp = "";
//	$("form").submit(function(){
		//エラーの初期化
		$("div.error").remove();
		$("div.mailerr").remove();

		$("td").removeClass("error");
		
		//テキスト、テキストエリアのチェック
		$(":text, textarea").filter(".required").each(function(){
			if($(this).val()==""){
				tmp = $(this).attr("id");
				$(this).parent().append("<div class='error'>" + data[tmp] + "を入力してください。</div>");
			}

			//数値のチェック
			$(this).filter(".number").each(function(){
				if(isNaN($(this).val())){
					$(this).parent().append("<div class='error'>数値のみ選択可能です。</div>");
				}
			});

		
/*
			//メールアドレスチェック
			$(this).filter(".mail").each(function(){
				if($(this).val() && !$(this).val().match(/.+@.+\..+/g)){
					$(this).parent().append("<div class='error'>メールアドレスの形式が異なります。</div>");
				}
			});
		
			//メールアドレス確認用
			$(this).filter(".mail_chk").each(function(){
				if($(this).val() && $(this).val() != $("input[name=" + $(this).attr("name").replace(/^(.+)_chk$/, "$1")+"]").val()){
					$(this).parent().append("<div class='error'>メールアドレスが一致しません。</div>");
				}
			});
*/
		});
		
		//電話番号
		if(!$("#tel1").val()){
			$("#tel1").parent().append("<div class='error'>電話番号を入力下さい。</div>");
		}

		//パスワード
		if(!$("#password").val()){
			$("#password").parent().append("<div class='error'>パスワードを入力してください。</div>");

		}
		if(!$("#password_check").val()){
			$("#password_check").parent().append("<div class='error'>パスワード（確認用）を入力してください。</div>");

		}

		if($("#password").val() && $("#password_check").val()){

			if($("#password").val() != $("#password_check").val()){
				$("#password").parent().append("<div class='error'>パスワードが異なります。</div>");
			}
		}

		pwd = $("#password").val();
		if(pwd){
			if(!pwd.match(/^(?=.*?[a-z])(?=.*?\d)[a-z\d]{8,20}$/i)){
			        $("#password").parent().append("<div class='error'></div>");
			}
		}

/*
		//メールアドレス
		if($("#mail1").val()=="" && $("#mail2").val()==""){
			$("#mail1").parent().append("<div class='error'>自宅もしくは携帯のどちらかは必ず御入力下さい。</div>");
		}
*/
		if($("#mail1").val()==""){
			$("div.mailerr").remove();
			$("#mailerr").append("<div class='mailerr error'>入力エラー：メールアドレスを入力してください。</div>");
			//$("#mail1").parent().append("<div class='error'>メールアドレスを入力して下さい。</div>");
		}else{
			$("div.mailerr").remove();
				if($("#mail1").val() && !$("#mail1").val().match(/.+@.+\..+/g)){
					$("#mailerr").append("<div class='mailerr error'>入力エラー：メールアドレスの形式が異なります。</div>");
					//$("#mail1").parent().append("<div class='error'>メールアドレスの形式が異なります。</div>");
				}
		}


		//ラジオボタンのチェック
		$(":radio").filter(".required").each(function(){
			var tmp_name = $(this).attr("name");
			var radio_tmp = document.getElementsByName(tmp_name);
			var cnt = 0;
			tmp = tmp_name.replace("[]", "");
			for(var i=0;i<radio_tmp.length;i++) {
				if (radio_tmp[i].checked) {
					cnt = 1;
				}
			}
			if(cnt == 0){
				$("#"+tmp_name+"_err").parent().append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
			}
		});
		
		//リストのチェック
		$("select").filter(".required").each(function(){
			if($("select[name="+$(this).attr("name")+"]").children(':selected').val() == ""){
				tmp = $(this).attr("id");
				$(this).parent().append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
			}
		});
		
		//チェックボックスのチェック
		$(".checkboxrequired").each(function(){
			if($(":checkbox:checked", this).size() == 0){
				tmp = $("input",this).attr("name");
				//name属性を配列にしているために[]を削除
				tmp = tmp.replace("[]", "");
				if(tmp=="agree"){
					$(this).append("<div class='error'>" + data[tmp] + "</div>");
				}else{
					$(this).append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
				}
			}
		});

		//環境依存文字簡易チェック
		$(":text, textarea").filter(".izon").each(function(){
			var show = $(this).attr("id");
			var text = $(this).val();
			var c_regP = "[①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳ⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩ㍉㌔㌢㍍㌘㌧㌃㌶㍑㍗㌍㌦㌣㌫㍊㌻㎜㎝㎞㎎㎏㏄㎡㍻〝〟№㏍℡㊤㊥㊦㊧㊨㈱㈲㈹㍾㍽㍼]";
			if(text.match(c_regP)){
				$(this).parent().append("<div class='error'>" + data[show] + "に環境依存文字が入力されています。</div>");
			}
		});
		
		renraku1 = $("select[name='renraku1']").val();
		renraku2 = $("select[name='renraku2']").val();
		if(!renraku1 && !renraku2){
				$("#renraku1").parent().append("<div class='error'>連絡希望時間を選択して下さい。</div>");
		}

		kibo_jiki_year = $("select[name='kibo_jiki_year']").val();
		kibo_jiki_month = $("select[name='kibo_jiki_month']").val();
//		kibo_jiki_day = $("select[name='kibo_jiki_day']").val();
		kibou_jiki = $("#kibou_jiki").prop('checked');

//		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month || !kibo_jiki_day)){
		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month)){
				$("#kibo_jiki_year").parent().append("<div class='error'>転職希望時期を入力して下さい。</div>");
		}

		e1 = $("#kibouarea1").val();
		e2 = $("#kibouarea2").val();
		e3 = $("#kibouarea3").val();

		if(e1 && e2){
			if(e1 == e2){
				$("#kibouarea1").parent().append("<div class='error'>勤務地（第1希望）と勤務地（第2希望）は別の都道府県を選択してください。</div>");
			}
		}
		if(e1 && e3){
			if(e1 == e3){
				$("#kibouarea1").parent().append("<div class='error'>勤務地（第1希望）と勤務地（第3希望）は別の都道府県を選択してください。</div>");
			}
		}
		if(e2 && e3){
			if(e2 == e3){
				$("#kibouarea2").parent().append("<div class='error'>勤務地（第2希望）と勤務地（第3希望）は別の都道府県を選択してください。</div>");
			}
		}
		//エラー処理
		if($("div.error").size() > 0){
			$('html,body').animate({ scrollTop: $("div.error:first").offset().top-150}, 'slow');
			return false;
		}else{
			return true;
		}

//	});
//});
}