//エラー処理用
var data = {};
data.shimei    = "氏名";
data.kana     = "ふりがな";
data.year    = "年";
data.month    = "月";
data.day    = "日";
data.sex    = "性別";
data.zip      = "郵便番号";
data.pref      = "住所";
data.address      = "住所";
data.tel1       = "電話";
data.mail1      = "メール";
data.mail2      = "メール（携帯）";
data.agree = "個人情報保護方針の同意にチェックをお願い致します。";
data.kibo_pref1="希望勤務地";
data.school_div_id="最終学歴";
data.school="学校名";
data.company_number="経験社数";
data.jokyo="就業状況";
data.kibo_job1="希望職種";
data.expectarea1="勤務地（第1希望）";
data.school_name="学校名/学部/学科";
data.company_name1="会社名１";
data.tantou_job1="担当職種１";
data.start_year1="開始年１";
data.start_month1="開始月１";
data.company_name2="会社名２";
data.tantou_job2="担当職種２";
data.start_year2="開始年２";
data.start_month2="開始月２";
data.company_name3="会社名３";
data.tantou_job3="担当職種３";
data.start_year3="開始年３";
data.start_month3="開始月３";
data.kiboujob="希望職種";
data.income="希望年収";
data.tenkin="転勤";
data.earnings="現在の年収";
data.kibouarea1="希望勤務地：第１希望";
data.kibouarea2="希望勤務地：第２希望";
data.kibouarea3="希望勤務地：第３希望";


$(function(){

	//////////////////////////////////////////////////////
	//　return 戻るボタンで戻ったときの背景対策
	//////////////////////////////////////////////////////
	if($("#shimei").val()){
		$("#shimei").css("background-color","#FFF");
	}
	if($("#kana").val()){
		$("#kana").css("background-color","#FFF");
	}
	if($("#year").val()){
		$("#year").css("background-color","#FFF");
	}
	if($("#month").val()){
		$("#month").css("background-color","#FFF");
	}
	if($("#day").val()){
		$("#day").css("background-color","#FFF");
	}
	cnt_tmp = $('.sexradio').length;
	checked_tmp = 0;
	for(i=0;i<cnt_tmp;i++){
		if($(".sexradio").eq(i).prop("checked")){
			 checked_tmp++;
		}
	}
	if (checked_tmp > 0) {
		$(".sexlabel").css("background-color","#FFF");
	}
	if($("#pref").val()){
		$("#pref").css("background-color","#FFF");
	}
	if($("#tel1").val()){
		$("#tel1").css("background-color","#FFF");
	}
	if($("#mail1").val()){
		$("#mail1").css("background-color","#FFF");
	}
	if($("#school_name").val()){
		$("#school_name").css("background-color","#FFF");
	}
	if($("#password").val()){
		$("#password").css("background-color","#FFF");
	}
	if($("#password_check").val()){
		$("#password_check").css("background-color","#FFF");
	}
	if($("#company_name1").val()){
		$("#company_name1").css("background-color","#FFF");
	}
	if($("#company_name2").val()){
		$("#company_name2").css("background-color","#FFF");
	}
	if($("#company_name3").val()){
		$("#company_name3").css("background-color","#FFF");
	}
	if($("#start_year1").val()){
		$("#start_year1").css("background-color","#FFF");
	}
	if($("#start_month1").val()){
		$("#start_month1").css("background-color","#FFF");
	}
	if($("#end_year1").val()){
		$("#end_year1").css("background-color","#FFF");
	}
	if($("#end_month1").val()){
		$("#end_month1").css("background-color","#FFF");
	}
	if($("#start_year2").val()){
		$("#start_year2").css("background-color","#FFF");
	}
	if($("#start_month2").val()){
		$("#start_month2").css("background-color","#FFF");
	}
	if($("#end_year2").val()){
		$("#end_year2").css("background-color","#FFF");
	}
	if($("#end_month2").val()){
		$("#end_month2").css("background-color","#FFF");
	}
	if($("#start_year3").val()){
		$("#start_year3").css("background-color","#FFF");
	}
	if($("#start_month3").val()){
		$("#start_month3").css("background-color","#FFF");
	}
	if($("#end_year3").val()){
		$("#end_year3").css("background-color","#FFF");
	}
	if($("#end_month3").val()){
		$("#end_month3").css("background-color","#FFF");
	}

	if($("#earnings").val()){
		$("#earnings").css("background-color","#FFF");
	}
	cnt_tmp = $('.gakurekiradio').length;
	checked_tmp = 0;
	for(i=0;i<cnt_tmp;i++){
		if($(".gakurekiradio").eq(i).prop("checked")){
			 checked_tmp++;
		}
	}
	if (checked_tmp > 0) {
		$(".gakurekilabel").css("background-color","#FFF");
	}
	cnt_tmp = $('.compradio').length;
	checked_tmp = 0;
	for(i=0;i<cnt_tmp;i++){
		if($(".compradio").eq(i).prop("checked")){
			 checked_tmp++;
		}
	}
	if (checked_tmp > 0) {
		$(".companylabel").css("background-color","#FFF");
	}
	cnt_tmp = $('.jokyoradio').length;
	checked_tmp = 0;
	for(i=0;i<cnt_tmp;i++){
		if($(".jokyoradio").eq(i).prop("checked")){
			 checked_tmp++;
		}
	}
	if (checked_tmp > 0) {
		$(".jokyolabel").css("background-color","#FFF");
	}
	if($("#expectarea1").val()){
		$("#expectarea1").css("background-color","#FFF");
	}
	//////////////////////////////////////////////////////
	// return ここまで
	//////////////////////////////////////////////////////


	//氏名
	$("input#shimei").blur(function() {
		if($("#shimei").val()){
			$(".shimei").removeClass("hissu");
			$(".shimei").addClass("ok");
			$("#shimei").css("background-color","#FFF");
		}else{
			$(".shimei").removeClass("ok");
			$(".shimei").addClass("hissu");
			$("#shimei").css("background-color","#fffafa");
		}
	});

	//かな
	$("input#kana").blur(function() {
		if($("#kana").val()){
			$(".kana").removeClass("hissu");
			$(".kana").addClass("ok");
			$("#kana").css("background-color","#FFF");
		}else{
			$(".kana").removeClass("ok");
			$(".kana").addClass("hissu");
			$("#kana").css("background-color","#fffafa");
		}
	});

	//生年月日
	$("select#year").change(function() {
		$("div.hidukeerr").remove();
		if($("#year").val() && $("#month").val() && $("#day").val()){

			if(ValidDate($('#year').val(),$('#month').val(),$('#day').val())){
				$(".birth").removeClass("hissu");
				$(".birth").addClass("ok");
				$("#year").css("background-color","#FFF");
			}else{
				$(".birth").removeClass("ok");
				$(".birth").addClass("hissu");
				$("#year").css("background-color","#fffafa");
			        $("#day").parent().append("<div class='error hidukeerr'>日付が正しくありません</div>");
			}
		}else{
			$(".birth").removeClass("ok");
			$(".birth").addClass("hissu");
			$("#year").css("background-color","#fffafa");
		}
	});
	$("select#month").change(function() {
		$("div.hidukeerr").remove();
		if($("#year").val() && $("#month").val() && $("#day").val()){
			if(ValidDate($('#year').val(),$('#month').val(),$('#day').val())){
				$(".birth").removeClass("hissu");
				$(".birth").addClass("ok");
				$("#month").css("background-color","#FFF");
			}else{
				$(".birth").removeClass("ok");
				$(".birth").addClass("hissu");
				$("#month").css("background-color","#fffafa");
			        $("#day").parent().append("<div class='error hidukeerr'>日付が正しくありません</div>");
			}
		}else{
			$(".birth").removeClass("ok");
			$(".birth").addClass("hissu");
			$("#month").css("background-color","#fffafa");
		}
	});
	$("select#day").change(function() {
		$("div.hidukeerr").remove();
		if($("#year").val() && $("#month").val() && $("#day").val()){
			if(ValidDate($('#year').val(),$('#month').val(),$('#day').val())){
				$(".birth").removeClass("hissu");
				$(".birth").addClass("ok");
				$("#day").css("background-color","#FFF");
			}else{
				$(".birth").removeClass("ok");
				$(".birth").addClass("hissu");
				$("#day").css("background-color","#fffafa");
			        $("#day").parent().append("<div class='error hidukeerr'>日付が正しくありません</div>");
			}
		}else{
			$(".birth").removeClass("ok");
			$(".birth").addClass("hissu");
			$("#day").css("background-color","#fffafa");
		}
	});

	//性別
	$('.sexradio').click(function(){
	    var idx = $(".sexradio").index(this);
	    var cnt = $('.sexradio').length;
	    var checked = 0;
	    for(i=0;i<cnt;i++){
		if($(".sexradio").eq(i).prop("checked")){
			 checked++;
		}
	    }
	    if (checked > 0) {
			$(".sex").removeClass("hissu");
			$(".sex").addClass("ok");
			$(".sexlabel").css("background-color","#FFF");
	    }else{
			$(".sex").removeClass("ok");
			$(".sex").addClass("hissu");
			$(".sexlabel").css("background-color","#fffafa");
	    }
	})

	//現住所
	$("select#pref").change(function() {
		if($("#pref").val()){
			$(".pref").removeClass("hissu");
			$(".pref").addClass("ok");
		}else{
			$(".pref").removeClass("ok");
			$(".pref").addClass("hissu");
		}
	});

	//電話
	$("input#tel1").blur(function() {
		if($("#tel1").val()){
			$(".tel1").removeClass("hissu");
			$(".tel1").addClass("ok");
			$("#tel1").css("background-color","#FFF");
		}else{
			$(".tel1").removeClass("ok");
			$(".tel1").addClass("hissu");
			$("#tel1").css("background-color","#fffafa");
		}
	});

	//学部名
	$("input#school_name").blur(function() {
		if($("#school_name").val()){
			$(".school_name").removeClass("hissu");
			$(".school_name").addClass("ok");
			$("#school_name").css("background-color","#FFF");
		}else{
			$(".school_name").removeClass("ok");
			$(".school_name").addClass("hissu");
			$("#school_name").css("background-color","#fffafa");
		}
	});

	//メール
	$("input#mail1").blur(function() {
		$("div.mailerr").remove();
		if($("#mail1").val()){
			if($("#mail1").val() && !$("#mail1").val().match(/.+@.+\..+/g)){
				$("#mailerr").append("<div class='mailerr error'>入力エラー：メールアドレスの形式が異なります。</div>");
				$(".mail1").removeClass("ok");
				$(".mail1").addClass("hissu");
				$("#mail1").css("background-color","#fffafa");
			}else{
				$(".mail1").removeClass("hissu");
				$(".mail1").addClass("ok");
				$("#mail1").css("background-color","#FFF");
			}
		}else{
				$("#mailerr").append("<div class='mailerr error'>入力エラー：メールアドレスを入力してください。</div>");
			$(".mail1").removeClass("ok");
			$(".mail1").addClass("hissu");
			$("#mail1").css("background-color","#fffafa");
		}
	});

	//パスワード
	$("input#password").blur(function() {
		$("div.password_err").remove();
		pwd = $("#password").val();
		if(pwd){
			if(!pwd.match(/^(?=.*?[a-z])(?=.*?\d)[a-z\d]{8,20}$/i)){
				$("#password_err").append("<div class='password_err error'>入力エラー：パスワードは8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</div>");
				$(".password").removeClass("ok");
				$(".password").addClass("nini");
				$("#password").css("background-color","#fffafa");
			}else{
				$(".password").removeClass("nini");
				$(".password").addClass("ok");
				$("#password").css("background-color","#FFF");
			}
		}else{
			$("#password_err").append("<div class='password_err error'>入力エラー：パスワードを入力してください。</div>");
			$(".password").removeClass("ok");
			$(".password").addClass("nini");
			$("#password").css("background-color","#fffafa");
		}
	});
	$("input#password_check").blur(function() {
		$("div.password_check_err").remove();
		pwd = $("#password").val();
		pwd_check = $("#password_check").val();
		if(pwd_check){
			if(!pwd_check.match(/^(?=.*?[a-z])(?=.*?\d)[a-z\d]{8,20}$/i)){
				$("#password_check_err").append("<div class='password_check_err error'>入力エラー：パスワード（確認）は8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</div>");
				$(".password_check").removeClass("ok");
				$(".password_check").addClass("nini");
				$("#password_check").css("background-color","#fffafa");
			}else if(pwd != pwd_check){
				$("#password_check_err").append("<div class='password_check_err error'>入力エラー：パスワードとパスワード（確認）の入力値が異なります。</div>");
				$(".password_check").removeClass("ok");
				$(".password_check").addClass("nini");
				$("#password_check").css("background-color","#fffafa");
			}else{
				$(".password_check").removeClass("nini");
				$(".password_check").addClass("ok");
				$("#password_check").css("background-color","#FFF");
			}
		}else{
			$("#password_check_err").append("<div class='password_check_err error'>入力エラー：パスワード（確認）を入力してください。</div>");
			$(".password_check").removeClass("ok");
			$(".password_check").addClass("nini");
			$("#password_check").css("background-color","#fffafa");
		}
	});


	//学歴
	$('.gakurekiradio').click(function(){
	    var idx = $(".gakurekiradio").index(this);
	    var cnt = $('.gakurekiradio').length;
	    var checked = 0;
	    for(i=0;i<cnt;i++){
		if($(".gakurekiradio").eq(i).prop("checked")){
			 checked++;
		}
	    }
	    if (checked > 0) {
			$(".school_div_id").removeClass("hissu");
			$(".school_div_id").addClass("ok");
			$(".gakurekilabel").css("background-color","#fff");
	    }else{
			$(".school_div_id").removeClass("ok");
			$(".school_div_id").addClass("hissu");
			$(".gakurekilabel").css("background-color","#fffafa");
	    }
	})

	//経験社数
	$('.compradio').click(function(){
	    var idx = $(".compradio").index(this);
	    var cnt = $('.compradio').length;
	    var checked = 0;
	    for(i=0;i<cnt;i++){
		if($(".compradio").eq(i).prop("checked")){
			 checked++;
		}
	    }
	    if (checked > 0) {
			$(".company_number").removeClass("hissu");
			$(".company_number").addClass("ok");
			$(".companylabel").css("background-color","#fff");
	    }else{
			$(".company_number").removeClass("ok");
			$(".company_number").addClass("hissu");
			$(".companylabel").css("background-color","#fffafa");
	    }
	})

	//就業状況
	$('.jokyoradio').click(function(){
	    var idx = $(".jokyoradio").index(this);
	    var cnt = $('.jokyoradio').length;
	    var checked = 0;
	    for(i=0;i<cnt;i++){
		if($(".jokyoradio").eq(i).prop("checked")){
			 checked++;
		}
	    }
	    if (checked > 0) {
			$(".jokyo").removeClass("hissu");
			$(".jokyo").addClass("ok");
			$(".jokyolabel").css("background-color","#fff");
	    }else{
			$(".jokyo").removeClass("ok");
			$(".jokyo").addClass("hissu");
			$(".jokyolabel").css("background-color","#fffafa");
	    }
	})

	//勤務地１
	$("select#expectarea1").change(function() {
		if($("#expectarea1").val()){
			$(".expectarea1").removeClass("hissu");
			$(".expectarea1").addClass("ok");
			$("#expectarea1").css("background-color","#fff");
		}else{
			$(".expectarea1").removeClass("ok");
			$(".expectarea1").addClass("hissu");
			$("#expectarea1").css("background-color","#fffafa");
		}
	});

	//勤務地２
	$("select#expectarea2").change(function() {
		if($("#expectarea2").val()){
			$(".expectarea2").removeClass("");
			$(".expectarea2").addClass("ok");
			$("#expectarea2").css("background-color","#fff");
		}else{
			$(".expectarea2").removeClass("ok");
			$(".expectarea2").addClass("");
//			$("#expectarea2").css("background-color","#fffafa");
		}
	});

	//勤務地3
	$("select#expectarea3").change(function() {
		if($("#expectarea3").val()){
			$(".expectarea3").removeClass("");
			$(".expectarea3").addClass("ok");
			$("#expectarea3").css("background-color","#fff");
		}else{
			$(".expectarea3").removeClass("ok");
			$(".expectarea3").addClass("");
//			$("#expectarea3").css("background-color","#fffafa");
		}
	});

	//希望質問
	$("textarea#comment").blur(function() {
		if($("#comment").val()){
			$(".comment").removeClass("");
			$(".comment").addClass("ok");
		}else{
			$(".comment").removeClass("ok");
			$(".comment").addClass("");
		}
	});

	$("input#company_name1").blur(function() {
		if($("#company_name1").val()){
			$(".company_name1").removeClass("hissu");
			$(".company_name1").addClass("ok");
			$("#company_name1").css("background-color","#FFF");
		}else{
			$(".company_name1").removeClass("ok");
			$(".company_name1").addClass("hissu");
			$("#company_name1").css("background-color","#f8f8f8");
		}
	});

	$("input#company_name2").blur(function() {
		if($("#company_name2").val()){
			$(".company_name2").removeClass("");
			$(".company_name2").addClass("ok");
			$("#company_name2").css("background-color","#FFF");
		}else{
			$(".company_name2").removeClass("ok");
			$(".company_name2").addClass("hissu");
			$("#company_name2").css("background-color","#f8f8f8");
		}
	});

	$("input#company_name3").blur(function() {
		if($("#company_name3").val()){
			$(".company_name3").removeClass("");
			$(".company_name3").addClass("ok");
			$("#company_name3").css("background-color","#FFF");
		}else{
			$(".company_name3").removeClass("ok");
			$(".company_name3").addClass("hissu");
			$("#company_name3").css("background-color","#f8f8f8");
		}
	});

	//在籍期間１
	$("select#start_year1").change(function() {
		if($("#start_year1").val() && $("#start_month1").val()){
			$(".start_year1").removeClass("hissu");
			$(".start_year1").addClass("ok");
			$("#start_year1").css("background-color","#FFF");
		}else{
			$(".start_year1").removeClass("ok");
			$(".start_year1").addClass("hissu");
			$("#start_year1").css("background-color","#f8f8f8");
		}
		if($("#start_year1").val() && $("#start_month1").val() && $("#end_year1").val() && $("#end_month1").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month1").val())-1;
			em = parseInt($("#end_month1").val())-1;
			var start_date1 = new Date($("#start_year1").val(), sm, 1);
			var end_date1 = new Date($("#end_year1").val(), em, 1);
			if(start_date1 > end_date1){
				$("#start_month1").parent().append("<div class='error datechk'>勤務開始日は勤務終了日よりも前の日付にして下さい。</div>");
				//$(".start_year1").removeClass("ok");
				//$(".end_year1").removeClass("ok");
			}
		}
	});
	$("select#start_month1").change(function() {
		if($("#start_year1").val() && $("#start_month1").val()){
			$(".start_year1").removeClass("hissu");
			$(".start_year1").addClass("ok");
			$("#start_month1").css("background-color","#FFF");
		}else{
			$(".start_year1").removeClass("ok");
			$(".start_year1").addClass("hissu");
			$("#start_month1").css("background-color","#f8f8f8");
		}
		if($("#start_year1").val() && $("#start_month1").val() && $("#end_year1").val() && $("#end_month1").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month1").val())-1;
			em = parseInt($("#end_month1").val())-1;
			var start_date1 = new Date($("#start_year1").val(), sm, 1);
			var end_date1 = new Date($("#end_year1").val(), em, 1);
			if(start_date1 > end_date1){
				$("#start_month1").parent().append("<div class='error datechk'>勤務開始日は勤務終了日よりも前の日付にして下さい。</div>");
			//	$(".start_year1").removeClass("ok");
				//$(".end_year1").removeClass("ok");
			}
		}

	});
	$("select#end_year1").change(function() {
		if($("#end_year1").val() && $("#end_month1").val()){
			$(".end_year1").addClass("ok");
			$("#end_year1").css("background-color","#FFF");
		}else{
			$(".end_year1").removeClass("ok");
			$("#end_year1").css("background-color","#f8f8f8");
		}
		if($("#end_year1").val()){
			$("#end_year1").css("background-color","#FFF");
			if($("#end_month1").val()){
				$("#end_month1").css("background-color","#FFF");
			}
		}
		if($("#start_year1").val() && $("#start_month1").val() && $("#end_year1").val() && $("#end_month1").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month1").val())-1;
			em = parseInt($("#end_month1").val())-1;
			var start_date1 = new Date($("#start_year1").val(), sm, 1);
			var end_date1 = new Date($("#end_year1").val(), em, 1);
			if(start_date1 > end_date1){
				$("#end_month1").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year1").removeClass("ok");
				//$(".end_month1").removeClass("ok");
			}
		}
	});
	$("select#end_month1").change(function() {
		if($("#end_year1").val() && $("#end_month1").val()){
			$(".end_year1").addClass("ok");
			$("#end_month1").css("background-color","#FFF");
		}else{
			$(".end_year1").removeClass("ok");
			$("#end_month1").css("background-color","#f8f8f8");
		}
		if($("#end_month1").val()){
			$("#end_month1").css("background-color","#FFF");
			if($("#end_year1").val()){
				$("#end_year1").css("background-color","#FFF");
			}
		}
		
		if($("#start_year1").val() && $("#start_month1").val() && $("#end_year1").val() && $("#end_month1").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month1").val())-1;
			em = parseInt($("#end_month1").val())-1;
			var start_date1 = new Date($("#start_year1").val(), sm, 1);
			var end_date1 = new Date($("#end_year1").val(), em, 1);
			if(start_date1 > end_date1){
				$("#end_month1").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year1").removeClass("ok");
				//$(".end_month1").removeClass("ok");
			}
		}
	});

	//在籍期間２
	$("select#start_year2").change(function() {
		if($("#start_year2").val() && $("#start_month2").val()){
			$(".start_year2").addClass("ok");
			$("#start_year2").css("background-color","#FFF");
		}else{
			$(".start_year2").removeClass("ok");
			$("#start_year2").css("background-color","#f8f8f8");
		}
		if($("#start_year2").val()){
			$("#start_year2").css("background-color","#FFF");
			if($("#start_month2").val()){
				$("#start_month2").css("background-color","#FFF");
			}
		}
		if($("#start_year2").val() && $("#start_month2").val() && $("#end_year2").val() && $("#end_month2").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month2").val())-1;
			em = parseInt($("#end_month2").val())-1;
			var start_date2 = new Date($("#start_year2").val(), sm, 1);
			var end_date2 = new Date($("#end_year2").val(), em, 1);
			if(start_date2 > end_date2){
				$("#start_month2").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year2").removeClass("ok");
			}
		}
	});
	$("select#start_month2").change(function() {
		if($("#start_year2").val() && $("#start_month2").val()){
			$(".start_year2").addClass("ok");
			$("#start_month2").css("background-color","#FFF");
		}else{
			$(".start_year2").removeClass("ok");
			$("#start_month2").css("background-color","#f8f8f8");
		}
		if($("#start_month2").val()){
			$("#start_month2").css("background-color","#FFF");
			if($("#start_year2").val()){
				$("#start_year2").css("background-color","#FFF");
			}
		}
		if($("#start_year2").val() && $("#start_month2").val() && $("#end_year2").val() && $("#end_month2").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month2").val())-1;
			em = parseInt($("#end_month2").val())-1;
			var start_date2 = new Date($("#start_year2").val(), sm, 1);
			var end_date2 = new Date($("#end_year2").val(), em, 1);
			if(start_date2 > end_date2){
				$("#start_month2").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year2").removeClass("ok");
			}
		}
	});
	$("select#end_year2").change(function() {
		if($("#end_year2").val() && $("#end_month2").val()){
			$(".end_year2").addClass("ok");
			$("#end_year2").css("background-color","#FFF");
		}else{
			$(".end_year2").removeClass("ok");
			$("#end_year2").css("background-color","#f8f8f8");
		}
		if($("#end_year2").val()){
			$("#end_year2").css("background-color","#FFF");
			if($("#end_month2").val()){
				$("#end_month2").css("background-color","#FFF");
			}
		}
		if($("#start_year2").val() && $("#start_month2").val() && $("#end_year2").val() && $("#end_month2").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month2").val())-1;
			em = parseInt($("#end_month2").val())-1;
			var start_date2 = new Date($("#start_year2").val(), sm, 1);
			var end_date2 = new Date($("#end_year2").val(), em, 1);
			if(start_date2 > end_date2){
				$("#end_month2").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year2").removeClass("ok");
			}
		}
	});
	$("select#end_month2").change(function() {
		if($("#end_year2").val() && $("#end_month2").val()){
			$(".end_year2").addClass("ok");
			$("#end_month2").css("background-color","#FFF");
		}else{
			$(".end_year2").removeClass("ok");
			$("#end_month2").css("background-color","#f8f8f8");
		}
		if($("#end_month2").val()){
			$("#end_month2").css("background-color","#FFF");
			if($("#end_year2").val()){
				$("#end_year2").css("background-color","#FFF");
			}
		}
		if($("#start_year2").val() && $("#start_month2").val() && $("#end_year2").val() && $("#end_month2").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month2").val())-1;
			em = parseInt($("#end_month2").val())-1;
			var start_date2 = new Date($("#start_year2").val(), sm, 1);
			var end_date2 = new Date($("#end_year2").val(), em, 1);
			if(start_date2 > end_date2){
				$("#end_month2").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year2").removeClass("ok");
			}
		}
	});

	//在籍期間３
	$("select#start_year3").change(function() {
		if($("#start_year3").val() && $("#start_month3").val()){
			$(".start_year3").addClass("ok");
			$("#start_year3").css("background-color","#FFF");
		}else{
			$(".start_year3").removeClass("ok");
			$("#start_year3").css("background-color","#f8f8f8");
		}
		if($("#start_year3").val()){
			$("#start_year3").css("background-color","#FFF");
			if($("#start_month3").val()){
				$("#start_month3").css("background-color","#FFF");
			}
		}
		if($("#start_year3").val() && $("#start_month3").val() && $("#end_year3").val() && $("#end_month3").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month3").val())-1;
			em = parseInt($("#end_month3").val())-1;
			var start_date3 = new Date($("#start_year3").val(), sm, 1);
			var end_date3 = new Date($("#end_year3").val(), em, 1);
			if(start_date3 > end_date3){
				$("#start_month3").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year3").removeClass("ok");
			}
		}
	});
	$("select#start_month3").change(function() {
		if($("#start_year3").val() && $("#start_month3").val()){
			$(".start_year3").addClass("ok");
			$("#start_month3").css("background-color","#FFF");
		}else{
			$(".start_year3").removeClass("ok");
			$("#start_month3").css("background-color","#f8f8f8");
		}
		if($("#start_month3").val()){
			$("#start_month3").css("background-color","#FFF");
			if($("#start_year3").val()){
				$("#start_year3").css("background-color","#FFF");
			}
		}
		if($("#start_year3").val() && $("#start_month3").val() && $("#end_year3").val() && $("#end_month3").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month3").val())-1;
			em = parseInt($("#end_month3").val())-1;
			var start_date3 = new Date($("#start_year3").val(), sm, 1);
			var end_date3 = new Date($("#end_year3").val(), em, 1);
			if(start_date3 > end_date3){
				$("#start_month3").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
				//$(".end_year3").removeClass("ok");
			}
		}
	});
	$("select#end_year3").change(function() {
		if($("#end_year3").val() && $("#end_month3").val()){
			$(".end_year3").addClass("ok");
			$("#end_year3").css("background-color","#FFF");
		}else{
			$(".end_year3").removeClass("ok");
			$("#end_year3").css("background-color","#f8f8f8");
		}
		if($("#end_year3").val()){
			$("#end_year3").css("background-color","#FFF");
			if($("#end_month3").val()){
				$("#end_month3").css("background-color","#FFF");
			}
		}
		if($("#start_year3").val() && $("#start_month3").val() && $("#end_year3").val() && $("#end_month3").val()){
			$(".datechk").remove();
			sm = parseInt($("#start_month3").val())-1;
			em = parseInt($("#end_month3").val())-1;
			var start_date3 = new Date($("#start_year3").val(), sm, 1);
			var end_date3 = new Date($("#end_year3").val(), em, 1);
			if(start_date3 > end_date3){
				$("#end_month3").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
			//	$(".end_year3").removeClass("ok");
			}
		}
	});
	$("select#end_month3").change(function() {
		if($("#end_year3").val() && $("#end_month3").val()){
			$(".end_year3").addClass("ok");
			$("#end_month3").css("background-color","#FFF");
		}else{
			$(".end_year3").removeClass("ok");
			$("#end_month3").css("background-color","#f8f8f8");
		}
		if($("#end_month3").val()){
			$("#end_month3").css("background-color","#FFF");
			if($("#end_year3").val()){
				$("#end_year3").css("background-color","#FFF");
			}
		}
		if($("#start_year3").val() && $("#start_month3").val() && $("#end_year3").val() && $("#end_month3").val()){
			$(".start_year3").removeClass("ok");
			$(".datechk").remove();
			sm = parseInt($("#start_month3").val())-1;
			em = parseInt($("#end_month3").val())-1;
			var start_date3 = new Date($("#start_year3").val(), sm, 1);
			var end_date3 = new Date($("#end_year3").val(), em, 1);
			if(start_date3 > end_date3){
				$("#end_month3").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
			//	$(".end_year3").removeClass("ok");
			}
		}
	});

	//////////////////////////////////////////////////////////////////

	$('select#kibo_jiki_year').change(function() {
		if($("select[name='kibo_jiki_year']").val()){
			$("#kibou_jiki").attr("checked", false);
		}
	});
	$('select#kibo_jiki_month').change(function() {
		if($("select[name='kibo_jiki_month']").val()){
			$("#kibou_jiki").attr("checked", false);
		}
	});
/*
	$('select#kibo_jiki_day').change(function() {
		if($("select[name='kibo_jiki_day']").val()){
			$("#kibou_jiki").attr("checked", false);
		}
	});
*/

	$("#kibou_jiki").click(function() {
		$("select#kibo_jiki_year").val("");
		$("select#kibo_jiki_month").val("");
//		$("select#kibo_jiki_day").val("");
	});


	//色の制御
/*
	$(":text, textarea").keyup(function(){
		cls = $(this).attr("class");
		if(cls.match(/required/)){
			tmpId = $(this).attr("id");
			if($(this).val()){
				$(this).css("background-color","#FFF");
			}else{
				$(this).css("background-color","#fffafa");
			}
		}
	});
*/
	$(":radio").change(function(){
		var tmp_name = $(this).attr("name");
		var radio_tmp = document.getElementsByName(tmp_name);
		var cnt = 0;
		tmp = tmp_name.replace("[]", "");
		for(var i=0;i<radio_tmp.length;i++) {
			if (radio_tmp[i].checked) {
				cnt = 1;
			}
		}
		if(cnt == 0){
			$(this).parent().css("background-color","#fffafa");
		}else{
			$(this).parent().css("background-color","#fff");
		}
	});

	$("select").change(function(){
		cls = $(this).attr("class");
		if(cls.match(/required/)){
			tmpId = $(this).attr("id");
			if($(this).val()){
				$(this).css("background-color","#FFF");
			}else{
				$(this).css("background-color","#fffafa");
			}
		}
	});

	$("select[name='renraku1']").change(function(){
		renraku1 = $("select[name='renraku1']").val();
		renraku2 = $("select[name='renraku2']").val();
		if(!renraku1 && !renraku2){
			$("#renraku1").css("background-color","#fffafa");
			$("#renraku2").css("background-color","#fffafa");
		}else{
			$("#renraku1").css("background-color","#FFF");
			$("#renraku2").css("background-color","#FFF");
		}
	});
	$("select[name='renraku2']").change(function(){
		renraku1 = $("select[name='renraku1']").val();
		renraku2 = $("select[name='renraku2']").val();
		if(!renraku1 && !renraku2){
			$("#renraku1").css("background-color","#fffafa");
			$("#renraku2").css("background-color","#fffafa");
		}else{
			$("#renraku1").css("background-color","#FFF");
			$("#renraku2").css("background-color","#FFF");
		}
	});


	$("select[name='kibo_jiki_year']").change(function(){
		kibo_jiki_year = $("select[name='kibo_jiki_year']").val();
		kibo_jiki_month = $("select[name='kibo_jiki_month']").val();
		kibou_jiki = $("#kibou_jiki").prop('checked');

		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month)){
			$("#kibo_jiki_year").css("background-color","#fffafa");
			$("#kibo_jiki_month").css("background-color","#fffafa");
			$("#kb").css("background-color","#fffafa");
		}else{
			$("#kibo_jiki_year").css("background-color","#FFF");
			$("#kibo_jiki_month").css("background-color","#FFF");
			$("#kb").css("background-color","#FFF");
		}
	});

	$("select[name='kibo_jiki_month']").change(function(){
		kibo_jiki_year = $("select[name='kibo_jiki_year']").val();
		kibo_jiki_month = $("select[name='kibo_jiki_month']").val();
		kibou_jiki = $("#kibou_jiki").prop('checked');

		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month)){
			$("#kibo_jiki_year").css("background-color","#fffafa");
			$("#kibo_jiki_month").css("background-color","#fffafa");
			$("#kb").css("background-color","#fffafa");
		}else{
			$("#kibo_jiki_year").css("background-color","#FFF");
			$("#kibo_jiki_month").css("background-color","#FFF");
			$("#kb").css("background-color","#FFF");
		}
	});

	$("#kibou_jiki").change(function(){
		kibo_jiki_year = $("select[name='kibo_jiki_year']").val();
		kibo_jiki_month = $("select[name='kibo_jiki_month']").val();
		kibou_jiki = $("#kibou_jiki").prop('checked');

		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month)){
			$("#kibo_jiki_year").css("background-color","#fffafa");
			$("#kibo_jiki_month").css("background-color","#fffafa");
			$("#kb").css("background-color","#fffafa");
		}else{
			$("#kibo_jiki_year").css("background-color","#FFF");
			$("#kibo_jiki_month").css("background-color","#FFF");
			$("#kb").css("background-color","#FFF");
		}
	});

    //担当職種１
    $('.tantoujob1allchk').change(function(){
                    if ($(this).is(':checked')) {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', true);
	                $(this).next(".tantoujob1allnavichecklabel").addClass('checked');
	                $(this).parent().find(".tantoujob1navichecklabel").addClass('checked');
                    }
                    else {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', false);
	                $(this).next(".tantoujob1allnavichecklabel").removeClass('checked');
                        $(this).parents('li').each(function(){
                            $(this).children('input[type="checkbox"]').prop('checked', false);
                        });
	                $(this).parent().find(".tantoujob1navichecklabel").removeClass('checked');
                    }

		    //チェックされているかの確認
	    	    cnt = $('.tantoujob1checkbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".tantoujob1checkbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".tantou_job1").removeClass("");
			$(".tantou_job1").addClass("ok");
	    	    }else{
			$(".tantou_job1").removeClass("ok");
			$(".tantou_job1").addClass("");
	    	    }
    });

    //担当職種２
    $('.tantoujob2allchk').change(function(){
                    if ($(this).is(':checked')) {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', true);
	                $(this).next(".tantoujob2allnavichecklabel").addClass('checked');
	                $(this).parent().find(".tantoujob2navichecklabel").addClass('checked');
                    }
                    else {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', false);
	                $(this).next(".tantoujob2allnavichecklabel").removeClass('checked');
                        $(this).parents('li').each(function(){
                            $(this).children('input[type="checkbox"]').prop('checked', false);
                        });
	                $(this).parent().find(".tantoujob2navichecklabel").removeClass('checked');
                    }

		    //チェックされているかの確認
	    	    cnt = $('.tantoujob2checkbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".tantoujob2checkbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".tantou_job2").removeClass("");
			$(".tantou_job2").addClass("ok");
	    	    }else{
			$(".tantou_job2").removeClass("ok");
			$(".tantou_job2").addClass("");
	    	    }
    });

    //担当職種３
    $('.tantoujob3allchk').change(function(){
                    if ($(this).is(':checked')) {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', true);
	                $(this).next(".tantoujob3allnavichecklabel").addClass('checked');
	                $(this).parent().find(".tantoujob3navichecklabel").addClass('checked');
                    }
                    else {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', false);
	                $(this).next(".tantoujob3allnavichecklabel").removeClass('checked');
                        $(this).parents('li').each(function(){
                            $(this).children('input[type="checkbox"]').prop('checked', false);
                        });
	                $(this).parent().find(".tantoujob3navichecklabel").removeClass('checked');
                    }

		    //チェックされているかの確認
	    	    cnt = $('.tantoujob3checkbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".tantoujob3checkbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".tantou_job3").removeClass("");
			$(".tantou_job3").addClass("ok");
	    	    }else{
			$(".tantou_job3").removeClass("ok");
			$(".tantou_job3").addClass("");
	    	    }
    });

	$("select#earnings").change(function() {
		if($("#earnings").val()){
			$(".earnings").removeClass("hissu");
			$(".earnings").addClass("ok");
		}else{
			$(".earnings").removeClass("ok");
			$(".earnings").addClass("hissu");
			$("#earnings").css("background-color","#f8f8f8");
		}
	});

	//希望職種
    //職種の一括チェック
    $('.joballchk').change(function(){
                    if ($(this).is(':checked')) {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', true);
	                $(this).next(".joballnavichecklabel").addClass('checked');
	                $(this).parent().find(".jobnavichecklabel").addClass('checked');
                    }
                    else {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', false);
	                $(this).next(".joballnavichecklabel").removeClass('checked');
                        $(this).parents('li').each(function(){
                            $(this).children('input[type="checkbox"]').prop('checked', false);
                        });
	                $(this).parent().find(".jobnavichecklabel").removeClass('checked');
                    }

		    //チェックされているかの確認
	    	    cnt = $('.jobcheckbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".jobcheckbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".kiboujob").removeClass("");
			$(".kiboujob").addClass("ok");
	    	    }else{
			$(".kiboujob").removeClass("ok");
			$(".kiboujob").addClass("");
	    	    }
    });
    $('.jobchk').click(function () {
        var idx = $(".jobchk").index(this);
        var cnt = $('.jobchk').length;
	if(!$(this).is(':checked')){
		cls = $(this).attr('class');
		term = cls.split( ' ' );
		label_id = term[0];
                $("#"+label_id).removeClass('checked');
	}

        for (i = 0; i < cnt; i++) {
            if ($(".jobchk").eq(i).prop("checked")) {
                $(".jobnavichecklabel").eq(i).addClass('checked');
            } else {
                $(".jobnavichecklabel").eq(i).removeClass('checked');
            }
        }
		    //チェックされているかの確認
	    	    cnt = $('.jobcheckbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".jobcheckbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".kiboujob").removeClass("");
			$(".kiboujob").addClass("ok");
	    	    }else{
			$(".kiboujob").removeClass("ok");
			$(".kiboujob").addClass("");
	    	    }

    });

    //転勤
    $('.tenkincheckbox').click(function () {
		    //チェックされているかの確認
	    	    cnt = $('.tenkincheckbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".tenkincheckbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".tenkin").removeClass("");
			$(".tenkin").addClass("ok");
	    	    }else{
			$(".tenkin").removeClass("ok");
			$(".tenkin").addClass("");
	    	    }

    });

    //都道府県の一括チェック
    $('.areaallchk').change(function(){
                    if ($(this).is(':checked')) {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', true);
	                $(this).next(".areaallnavichecklabel").addClass('checked');
	                $(this).parent().find(".areanavichecklabel").addClass('checked');
                    }
                    else {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', false);
	                $(this).next(".areaallnavichecklabel").removeClass('checked');
                        $(this).parents('li').each(function(){
                            $(this).children('input[type="checkbox"]').prop('checked', false);
                        });
	                $(this).parent().find(".areanavichecklabel").removeClass('checked');
                    }
		    //チェックされているかの確認
	    	    cnt = $('.areacheckbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".areacheckbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".kibouarea").removeClass("");
			$(".kibouarea").addClass("ok");
	    	    }else{
			$(".kibouarea").removeClass("ok");
			$(".kibouarea").addClass("");
	    	    }
    });
    $('.areachk').click(function () {
        var idx = $(".areachk").index(this);
        var cnt = $('.areachk').length;
	if(!$(this).is(':checked')){
		cls = $(this).attr('class');
		term = cls.split( ' ' );
		label_id = term[0];
                $("#"+label_id).removeClass('checked');
	}

        for (i = 0; i < cnt; i++) {
            if ($(".areachk").eq(i).prop("checked")) {
                $(".areanavichecklabel").eq(i).addClass('checked');
            } else {
                $(".areanavichecklabel").eq(i).removeClass('checked');
            }
        }
		    //チェックされているかの確認
	    	    cnt = $('.areacheckbox').length;
	            checked = 0;
	            for(i=0;i<cnt;i++){
		        if($(".areacheckbox").eq(i).prop("checked")){
			     checked++;
   		        }
	            }
	            if (checked > 0) {
			$(".kibouarea").removeClass("");
			$(".kibouarea").addClass("ok");
	    	    }else{
			$(".kibouarea").removeClass("ok");
			$(".kibouarea").addClass("");
	    	    }
    });

    //収入の切り替え
	$("input#income").blur(function() {
		if($("#income").val()){
			$(".income").removeClass("");
			$(".income").addClass("ok");
			$("#income").css("background-color","#FFF");
		}else{
			$(".income").removeClass("ok");
			$(".income").addClass("");
			$("#income").css("background-color","#fffafa");
		}
	});

	$("#agreeChk").change(function(){
		agr = $("#agreeChk").prop('checked');
		if(agr){
			$("#btnKakunin").attr("src","img/btn_check_on.png");
	                       $("#agr").addClass('checked');
		}else{
			$("#btnKakunin").attr("src","img/btn_check_off.png");
	                       $("#agr").removeClass('checked');
		}
/*
		if(agr==0){
			$(this).parent().css("background-color","#fffafa");
		}else{
			$(this).parent().css("background-color","#fff");
		}
*/
	});
});

function check(param){


	window.onbeforeunload=null;

//$(function(){
	var tmp = "";
//	$("form").submit(function(){
		//エラーの初期化
		$("div.error").remove();
		$("div.mailerr").remove();

		$("td").removeClass("error");
		
		//テキスト、テキストエリアのチェック
		$(":text, textarea").filter(".required").each(function(){
			if($(this).val()==""){
				tmp = $(this).attr("id");
				$(this).parent().append("<div class='error'>" + data[tmp] + "を入力してください。</div>");
			}

			//数値のチェック
			$(this).filter(".number").each(function(){
				if(isNaN($(this).val())){
					$(this).parent().append("<div class='error'>数値のみ選択可能です。</div>");
				}
			});

		
/*
			//メールアドレスチェック
			$(this).filter(".mail").each(function(){
				if($(this).val() && !$(this).val().match(/.+@.+\..+/g)){
					$(this).parent().append("<div class='error'>メールアドレスの形式が異なります。</div>");
				}
			});
		
			//メールアドレス確認用
			$(this).filter(".mail_chk").each(function(){
				if($(this).val() && $(this).val() != $("input[name=" + $(this).attr("name").replace(/^(.+)_chk$/, "$1")+"]").val()){
					$(this).parent().append("<div class='error'>メールアドレスが一致しません。</div>");
				}
			});
*/
		});
		
		//電話番号
		if(!$("#tel1").val()){
			$("#tel1").parent().append("<div class='error'>電話番号を入力下さい。</div>");
		}

		pwd = $("#password").val();
		pwd_chk = $("#password_check").val();
		if(pwd){
			if(!pwd.match(/^(?=.*?[a-z])(?=.*?\d)[a-z\d]{8,20}$/i)){
			        $("#password").parent().append("<div class='error'></div>");
			}
		}
		if(pwd_chk){
			if(!pwd_check.match(/^(?=.*?[a-z])(?=.*?\d)[a-z\d]{8,20}$/i)){
			        $("#password_check").parent().append("<div class='error'></div>");
			}
		}

		//パスワード
		if(pwd && pwd_chk){
			if(pwd != pwd_chk){
				$("#password").parent().append("<div class='error'>パスワードが異なります。</div>");
			}
		}


		if($("#year").val() && $("#month").val() && $("#day").val()){
			if(ValidDate($('#year').val(),$('#month').val(),$('#day').val())){
				$(".birth").removeClass("hissu");
				$(".birth").addClass("ok");
				$("#day").css("background-color","#FFF");
			}else{
			        $("#day").parent().append("<div class='error hidukeerr'>日付が正しくありません</div>");
			}
		}

/*
		//メールアドレス
		if($("#mail1").val()=="" && $("#mail2").val()==""){
			$("#mail1").parent().append("<div class='error'>自宅もしくは携帯のどちらかは必ず御入力下さい。</div>");
		}
*/
		if($("#mail1").val()==""){
			$("div.mailerr").remove();
			$("#mailerr").append("<div class='mailerr error'>入力エラー：メールアドレスを入力してください。</div>");
			//$("#mail1").parent().append("<div class='error'>メールアドレスを入力して下さい。</div>");
		}else{
			$("div.mailerr").remove();
				if($("#mail1").val() && !$("#mail1").val().match(/.+@.+\..+/g)){
					$("#mailerr").append("<div class='mailerr error'>入力エラー：メールアドレスの形式が異なります。</div>");
					//$("#mail1").parent().append("<div class='error'>メールアドレスの形式が異なります。</div>");
				}
		}


		//ラジオボタンのチェック
		$(":radio").filter(".required").each(function(){
			var tmp_name = $(this).attr("name");
			var radio_tmp = document.getElementsByName(tmp_name);
			var cnt = 0;
			tmp = tmp_name.replace("[]", "");
			for(var i=0;i<radio_tmp.length;i++) {
				if (radio_tmp[i].checked) {
					cnt = 1;
				}
			}
			if(cnt == 0){
				$("#"+tmp_name+"_err").parent().append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
			}
		});
		
		//リストのチェック
		$("select").filter(".required").each(function(){
			if($("select[name="+$(this).attr("name")+"]").children(':selected').val() == ""){
				tmp = $(this).attr("id");
				$(this).parent().append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
			}
		});
		
		//チェックボックスのチェック
		$(".checkboxrequired").each(function(){
			if($(":checkbox:checked", this).size() == 0){
				tmp = $("input",this).attr("name");
				//name属性を配列にしているために[]を削除
				tmp = tmp.replace("[]", "");
				if(tmp=="agree"){
					$(this).append("<div class='error'>" + data[tmp] + "</div>");
				}else{
					$(this).append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
				}
			}
		});

		//環境依存文字簡易チェック
		$(":text, textarea").filter(".izon").each(function(){
			var show = $(this).attr("id");
			var text = $(this).val();
			var c_regP = "[①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳ⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩ㍉㌔㌢㍍㌘㌧㌃㌶㍑㍗㌍㌦㌣㌫㍊㌻㎜㎝㎞㎎㎏㏄㎡㍻〝〟№㏍℡㊤㊥㊦㊧㊨㈱㈲㈹㍾㍽㍼]";
			if(text.match(c_regP)){
				$(this).parent().append("<div class='error'>" + data[show] + "に環境依存文字が入力されています。</div>");
			}
		});
		
		//日付チェック
		if($("#start_year1").val() && $("#start_month1").val() && $("#end_year1").val() && $("#end_month1").val()){
			sm = parseInt($("#start_month1").val())-1;
			em = parseInt($("#end_month1").val())-1;
			var start_date1 = new Date($("#start_year1").val(), sm, 1);
			var end_date1 = new Date($("#end_year1").val(), em, 1);
			if(start_date1 > end_date1){
				$("#end_month1").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
			//	$(".end_year1").removeClass("ok");
			}
		}

		if($("#start_year2").val() && $("#start_month2").val() && $("#end_year2").val() && $("#end_month2").val()){
			sm = parseInt($("#start_month2").val())-1;
			em = parseInt($("#end_month2").val())-1;
			var start_date2 = new Date($("#start_year2").val(), sm, 1);
			var end_date2 = new Date($("#end_year2").val(), em, 1);
			if(start_date2 > end_date2){
				$("#end_month2").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
			//	$(".end_year2").removeClass("ok");
			}
		}

		if($("#start_year3").val() && $("#start_month3").val() && $("#end_year3").val() && $("#end_month3").val()){
			sm = parseInt($("#start_month3").val())-1;
			em = parseInt($("#end_month3").val())-1;
			var start_date3 = new Date($("#start_year3").val(), sm, 1);
			var end_date3 = new Date($("#end_year3").val(), em, 1);
			if(start_date3 > end_date3){
				$("#end_month3").parent().append("<div class='error datechk'>勤務終了日は勤務開始日よりも後の日付にして下さい。</div>");
			//	$(".end_year3").removeClass("ok");
			}
		}

		renraku1 = $("select[name='renraku1']").val();
		renraku2 = $("select[name='renraku2']").val();
		if(!renraku1 && !renraku2){
				$("#renraku1").parent().append("<div class='error'>連絡希望時間を選択して下さい。</div>");
		}

		kibo_jiki_year = $("select[name='kibo_jiki_year']").val();
		kibo_jiki_month = $("select[name='kibo_jiki_month']").val();
//		kibo_jiki_day = $("select[name='kibo_jiki_day']").val();
		kibou_jiki = $("#kibou_jiki").prop('checked');

//		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month || !kibo_jiki_day)){
		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month)){
				$("#kibo_jiki_year").parent().append("<div class='error'>転職希望時期を入力して下さい。</div>");
		}

		e1 = $("#expectarea1").val();
		e2 = $("#expectarea2").val();
		e3 = $("#expectarea3").val();

		if(e1 && e2){
			if(e1 == e2){
				$("#expectarea1").parent().append("<div class='error'>勤務地（第1希望）と勤務地（第2希望）は別の都道府県を選択してください。</div>");
			}
		}
		if(e1 && e3){
			if(e1 == e3){
				$("#expectarea1").parent().append("<div class='error'>勤務地（第1希望）と勤務地（第3希望）は別の都道府県を選択してください。</div>");
			}
		}
		if(e2 && e3){
			if(e2 == e3){
				$("#expectarea2").parent().append("<div class='error'>勤務地（第2希望）と勤務地（第3希望）は別の都道府県を選択してください。</div>");
			}
		}
		//エラー処理
		if($("div.error").size() > 0){
			$('html,body').animate({ scrollTop: $("div.error:first").offset().top-150}, 'slow');
			return false;
		}else{
			return true;
		}

//	});
//});
}

	function ValidDate(y,m,d) {
	    dt=new Date(y,m-1,d);
	    return(dt.getFullYear()==y && dt.getMonth()==m-1 && dt.getDate()==d);
	}