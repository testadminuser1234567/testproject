<?php
class MY_Controller extends CI_Controller {

    public $clean;

    //公開条件
    private $phaze = 6;
    private $publish = 11103;
    /*
    private $phaze = 11203;
    private $publish = 11104;
    */

    function __construct()
    {
        //キャッシュ対策戻るボタン
        $cache_path = unserialize(CACHE_LIMITER);
        if(!empty($cache_path)){
            foreach($cache_path as $k => $v){
                if(preg_match("/$v/",$_SERVER["REQUEST_URI"])){
                    session_cache_expire(0);
                    session_cache_limiter('private_no_expire');
                }
            }
        }

        //xss対策
        $purifierdir = $_SERVER['DOCUMENT_ROOT'] . purifierDirectory . '/htmlpurifier/library/HTMLPurifier.auto.php';
        require_once($purifierdir);
        $config = HTMLPurifier_Config::createDefault();
        $config->set('Core.Encoding', 'UTF-8');
        $config->set('HTML.Doctype', 'XHTML 1.0 Transitional');
        $config->set('Attr.AllowedFrameTargets', array('_blank','_self'));
        $config->set('HTML.Trusted', true);
        $this->clean = new HTMLPurifier($config);

        parent::__construct();

        //メンテナンスチェック
        $return = $this->db->get_where("maintenances", array('id' => 1))->row();
        $start = $return->start;
        $end = $return->end;
        $today = date('Y/m/d H:i:s');
        if (strtotime(date('Y-m-d H:i:s')) >= strtotime($start) && strtotime(date('Y-m-d H:i:s')) <= strtotime($end)){
            $controller = $this->uri->segment(1);
            if($controller != "maintenance"){
                redirect("maintenance/home");
            }
        }
    }

    ////////////////////////////////////////////////////////////////
    // curl
    ///////////////////////////////////////////////////////////////////////////////
    /**
     * Curl 実行のエラーを検知してメールを送信する
     *
     * @param $handle Curl のハンドル
     * @return void
     */
    public function checkError($handle)
    {
        //エラー検証
        if (curl_errno($handle)) {
            $message = "ErrorNo: " . curl_errno($handle) . " ErrorMessage: " . curl_error($handle);
            UtilLogger::error($message);
            SendMail::sendOtherError($message);
        }
    }

    /**
     * curl を使用して指定URLからXMLを取得する
     *
     * @param string $url リクエスト先URL
     * @param array $header リクエスト時に付与するheader
     * @return SimpleXMLElement 取得したレスポンスのXMLオブジェクト
     */
    public function getXml( $url, array $header = [])
    {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);  // 戻り値を文字列で
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false); // 証明無視
        if (count($header) > 0) {
            curl_setopt($handle, CURLOPT_HTTPHEADER, $header);
        }
        $response = curl_exec($handle);
        // エラー検証
        self::checkError($handle);
        curl_close($handle);

        // xml パース
        $xml = simplexml_load_string($response);
        return $xml;
    }

    /**
     * curl を使用して指定URLからテキストを取得する
     *
     * @param string $url リクエスト先URL
     * @param array $header リクエスト時に付与するheader
     * @return String
     */
    public function getText( $url, array $header = [])
    {
        UtilLogger::debug("getText {$url}");

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);  // 戻り値を文字列で
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false); // 証明無視
        if (count($header) > 0) {
            curl_setopt($handle, CURLOPT_HTTPHEADER, $header);
        }
        $response = curl_exec($handle);
        self::checkError($handle);
        curl_close($handle);

        return $response;
    }

    /**
     * curl を使用して指定URLからXMLを取得する
     *
     * @param string $url リクエスト先URL
     * @param string $body リクエスト時に付与するbody
     * @param array $header リクエスト時に付与するheader
     * @return DOMDocument 取得したレスポンスのXMLオブジェクト
     */
    public function postRequest( $url,  $body, array $header = [])
    {
        UtilLogger::debug("postRequest {$url}");

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $body);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true); //戻り値を文字列で
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false); //証明無視

        if (count($header) > 0) {
            curl_setopt($handle, CURLOPT_HTTPHEADER, $header);
        }
        $response = curl_exec($handle);
        //エラー検証
        self::checkError($handle);
        curl_close($handle);

        $dom = new DOMDocument;
        $dom->loadXML($response);
        return $dom;
    }

    ////////////////////////////////////////////////////////////////
    // ログインチェック
    ////////////////////////////////////////////////////////////////
    function _login_check(){
        //hurexからのジョブオファーがあってログイン前の場合の対策
        $redirectParam = "";
        $controller = $this->uri->segment(1);
        $action = $this->uri->segment(2);
        $job = $this->uri->segment(3);
        if($controller=="search" && $action == "detail"){
            if($job){
                $redirectParam = "?j=" . htmlspecialchars($job, ENT_QUOTES,'UTF-8');
            }
        }

        //IPアドレス取得
        $ip = $_SERVER["REMOTE_ADDR"];
        $remote_host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
        $script_url =  $_SERVER["REQUEST_URI"];
        $referer = $_SERVER['HTTP_REFERER'];
        $agent = $_SERVER['HTTP_USER_AGENT'];

        $login_id = $this->session->userdata('login_id');
        $login_name = $this->session->userdata('login_name');

        //IDが未設定の場合はcookieにキーが保存されているかチェック
        $last_login = 0; //ログイン履歴に残す（１の場合）
        if(empty($login_id)){

            $login_cookie="";
            if(!empty($_COOKIE["loginkey"])) {
                $login_cookie = $_COOKIE["loginkey"];
            }
            if($login_cookie){
                $return = $this->db->get_where("members", array('loginkey' => $login_cookie, 'del' => 0))->row();

                if(!empty($return) && !empty($return->id)){
                    $login_id = $return->id;
                    //ログイン履歴に残すパターン
                    //$last_login = 1;
                }else{
                    //ログファイルに保存
                    $user = $this->db->get_where("members", array('id' => $login_id, 'del'=>0))->row();
                    $resume_id = $user->hrbc_resume_id;
                    $person_id = $user->hrbc_person_id;
                    $log_data = array();
                    $log_data["status"] = "error1";
                    $log_data["id"] = $login_id;
                    $log_data["resume"] = $resume_id;
                    $log_data["person"] = $person_id;
                    $log_data["ip"] = $ip;
                    $log_data["host"] = $remote_host;
                    $log_data["script"] = $script_url;
                    $log_data["referer"] = $referer;
                    $log_data["agent"] = $agent;
                    $log_data["date"] =  date('YmdHis');
                    //$log_data = "error1," . $login_id . "," . $resume_id . "," . $person_id . "," . $ip . "," . $remote_host . "," . $script_url . "," . $referer . "," . $_SERVER['HTTP_USER_AGENT'] . "," . date('YmdHis');
                    //$this->writeLog($log_data, "loginCheck1 login session finish");

                    $this->session->sess_destroy();
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
                    redirect("user/finishall" . $redirectParam);
                }
            }else{
            }
        }

        if(empty($login_id)){
            //ログファイルに保存
            $user = $this->db->get_where("members", array('id' => $login_id, 'del'=>0))->row();
            $resume_id = $user->hrbc_resume_id;
            $person_id = $user->hrbc_person_id;
            $log_data = array();
            $log_data["status"] = "error2";
            $log_data["id"] = $login_id;
            $log_data["resume"] = $resume_id;
            $log_data["person"] = $person_id;
            $log_data["ip"] = $ip;
            $log_data["host"] = $remote_host;
            $log_data["script"] = $script_url;
            $log_data["referer"] = $referer;
            $log_data["agent"] = $agent;
            $log_data["date"] =  date('YmdHis');
            //$log_data = "error2," . $login_id . "," . $resume_id . "," . $person_id . "," . $ip . "," . $remote_host . "," . $script_url . "," . $referer . "," . $_SERVER['HTTP_USER_AGENT'] . "," . date('YmdHis');
            //$this->writeLog($log_data, "loginCheck2 loginID session finish");

            $this->session->sess_destroy();
            //$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect("user/finishall" . $redirectParam);
        }

        //名前が空の場合はHRBCから取得
        if(empty($login_name)){
            $user = $this->db->get_where("members", array('id' => $login_id, 'del'=>0))->row();
            $resume_id = $user->hrbc_resume_id;
            $person_id = $user->hrbc_person_id;

            //HRBC
            $result = $this->getHrbcMaster();

            ///////////////////////////////////////////////////
            //person取得
            ///////////////////////////////////////////////////
            /*
            $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Id:eq=' . $person_id));
            $url = "https://api-hrbc-jp.porterscloud.com/v1/candidate?" . $url;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);

            $xml = file_get_contents($url, false, $context);
            $xml = simplexml_load_string($xml);
            */
            $url = $this->hrbcUrl .  "candidate?partition=". $result["partition"] . "&count=1&start=0&condition=Person.P_Id:eq=".$person_id;
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);


            if($xml->Code!=0){
                $pErr=$xml->Code;

                $subject = "【ユーザ名取得に失敗　code:user01】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                $message = "ユーザ名取得に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id . " error:" . $pErr;
                $this->errMail($subject, $message);

                print("エラーが発生しました。<br />コード：" . $pErr);
                exit;
            }

            //データ設定
            $json = json_encode($xml);
            $person = json_decode($json,true);
            $login_name = $person["Item"]["Person.P_Name"];

            $sessionData = array(
                'login_name' => $login_name
            );
            $this->session->set_userdata($sessionData);
        }

        if(empty($login_name)){
            //ログファイルに保存
            $user = $this->db->get_where("members", array('id' => $login_id, 'del'=>0))->row();
            $resume_id = $user->hrbc_resume_id;
            $person_id = $user->hrbc_person_id;
            $log_data = array();
            $log_data["status"] = "error3";
            $log_data["id"] = $login_id;
            $log_data["resume"] = $resume_id;
            $log_data["person"] = $person_id;
            $log_data["ip"] = $ip;
            $log_data["host"] = $remote_host;
            $log_data["script"] = $script_url;
            $log_data["referer"] = $referer;
            $log_data["agent"] = $agent;
            $log_data["date"] =  date('YmdHis');
//            $log_data = "error3," . $login_id . "," . $resume_id . "," . $person_id . "," . $ip . "," . $remote_host . "," . $script_url . "," . $referer . "," . $_SERVER['HTTP_USER_AGENT'] . "," . date('YmdHis');
            //$this->writeLog($log_data, "loginCheck1 login_name can not find");

            $this->session->sess_destroy();
            //$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect("user/finishall" . $redirectParam);
        }

        $sessionData = array(
            'login_id' => $login_id
        );
        $this->session->set_userdata($sessionData);

        $login_id = $this->session->userdata('login_id');
        $login_name = $this->session->userdata('login_name');

        /////////////////////////////////////////////////////////////////////////////////////////
        //ログイン履歴に残す
        /////////////////////////////////////////////////////////////////////////////////////////
        if($last_login==1){
            $this->_login_check();
            $login_id = $this->session->userdata('login_id');

            ////////////////////////////////////////////////////////////////////////////////////////////
            //HRBCデータ取得
            ////////////////////////////////////////////////////////////////////////////////////////////
            $result = $this->getHrbcMaster();

            $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
            $resume_id = $user->hrbc_resume_id;
            $person_id = $user->hrbc_person_id;

            ////////////////////////////////////////////////////////////////////////////////////////////
            //Resume のログイン履歴登録
            ////////////////////////////////////////////////////////////////////////////////////////////
            /*
            $param = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Id:eq='.$resume_id, 'field'=>'Resume.P_Owner,Resume.U_CFD727A34F74C45C5786B706A1F90F', 'order'=>'Resume.P_Id:desc'));
            $url =$this->hrbcUrl .  "resume?" . $param;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);

            if($xml = @file_get_contents($url, false, $context)){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 lastLogin"));
                            redirect("err/error_404");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 lastLogin"));
                            redirect("err/error_404");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other lastLogin"));
                            redirect("err/error_404");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout lastLogin"));
                    redirect("err/error_404");
                }
            }
            $xml = simplexml_load_string($xml);
            */
            $url = $this->hrbcUrl .  "resume?partition=". $result["partition"] . "&count=1&start=0&condition=Resume.P_Id:eq=".$resume_id."&field=Resume.P_Owner,Resume.U_CFD727A34F74C45C5786B706A1F90F&order=Resume.P_Id:desc";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

            if($xml->Code!=0){
                $pErr=$xml->Code;
                //エラーメール送信
                $subject = "【resumeログイン履歴登録失敗　code:user01】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                $message = "ログイン履歴登録に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                $this->errMail($subject, $message);

                $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
                //redirect("err/error_404");
                //exit;

            }else{
                //HRBCから帰ってきたデータ件数　1件の対応用に取得
                $cnt = $xml->attributes()->Count;

                //データ設定
                $json = json_encode($xml);
                $data["resume"] = json_decode($json,true);

                $now = date('Y/m/d H:i:s');
                $now_hrbc = date("Y/m/d H:i:s",strtotime($now. "-9 hour"));
                $login_log = $data["resume"]["Item"]["Resume.U_CFD727A34F74C45C5786B706A1F90F"];
                $first_login_flg = 0;
                if(empty($login_log)){
                    $login_log="";
                }else{
                    $first_login_flg = 1;
                }
                $login_log = $now . "&#x0A;" .  $login_log;
                $owner_id = $data["resume"]["Item"]["Resume.P_Owner"]["User"]["User.P_Id"];

                ///////////////////////////////////////////////////////////
                // resume に登録
                ///////////////////////////////////////////////////////////
                $url = $this->hrbcUrl . "resume?partition=". $result["partition"];

                //※レジュメにメールとTELを入れるとエラーになる
                //1回でもログインした場合
                if($first_login_flg==1){
                    $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>' . $owner_id . '</Resume.P_Owner><Resume.P_Candidate>'.$person_id.'</Resume.P_Candidate><Resume.U_CFD727A34F74C45C5786B706A1F90F>' . $login_log . '</Resume.U_CFD727A34F74C45C5786B706A1F90F></Item></Resume>';
                }else{
                    $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>' . $owner_id . '</Resume.P_Owner><Resume.P_Candidate>'.$person_id.'</Resume.P_Candidate><Resume.U_CFD727A34F74C45C5786B706A1F90F>' . $login_log . '</Resume.U_CFD727A34F74C45C5786B706A1F90F></Item></Resume>';
                }

                $header = array(
                    "Content-Type: application/xml; charset=UTF-8",
                    "X-porters-hrbc-oauth-token: " . $result['token']
                );

                $options = array('http' => array(
                    'method' => 'POST',
                    'content' => $xml,
                    'header' => implode("\r\n", $header)
                ));
                if($xml = @file_get_contents($url, false, stream_context_create($options))){
                    //ここにデータ取得が成功した時の処理
                }else{
                    //エラー処理
                    if(count($http_response_header) > 0){
                        //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                        $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                        //エラーの判別
                        switch($status_code[1]){
                            //404エラーの場合
                            case 404:
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 lastLogin2"));
                                redirect($this->segment."/login");
                                break;

                            //500エラーの場合
                            case 500:
                                $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 lastLogin2"));
                                redirect($this->segment."/login");
                                break;

                            //その他のエラーの場合
                            default:
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other lastLogin2"));
                                redirect($this->segment."/login");
                        }
                    }else{
                        //タイムアウトの場合 or 存在しないドメインだった場合
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout lastLogin2"));
                        redirect($this->segment."/login");
                    }
                }

                //xml解析
                $rErr="";
                $rId = "";
                $xml = simplexml_load_string($xml);

                $error="";
                if($xml->Code!=0){
                    $subject = "【resumeログイン履歴登録失敗　code:user02】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                    $message = "ログイン履歴登録に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                    $this->errMail($subject, $message);
                    $rErr=$xml->Code;
                }else{
                    $rCode="";
                    $json = json_encode($xml);
                    $arr = json_decode($json,true);
                    foreach($arr['Item'] as $k => $v){
                        if($k=="Id"){
                            $rId = $v;
                        }
                        if($k=="Code"){
                            $rErr = $v;
                        }
                    }
                }
                if(!$rId || $rErr){
                    $subject = "【resumeログイン履歴登録失敗　code:user03】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                    $message = "ログイン履歴登録に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id . " error:" . $rErr;
                    $this->errMail($subject, $message);
                    $error.= "エラーが発生しました。(Resume: ".$rErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
                }
            }
        }

        //お気に入りの件数
        $sql = "SELECT job_id from likes where member_id = " . $login_id;
        $likeData = $this->db->query($sql);
        $likeCnt = count($likeData->result());
        $this->session->set_userdata('like_cnt', $likeCnt);

        //ログイン済かチェック
        if(empty($login_id)){
            //ログファイルに保存
            $user = $this->db->get_where("members", array('id' => $login_id, 'del'=>0))->row();
            $resume_id = $user->hrbc_resume_id;
            $person_id = $user->hrbc_person_id;
            $log_data = array();
            $log_data["status"] = "error4";
            $log_data["id"] = $login_id;
            $log_data["resume"] = $resume_id;
            $log_data["person"] = $person_id;
            $log_data["ip"] = $ip;
            $log_data["host"] = $remote_host;
            $log_data["script"] = $script_url;
            $log_data["referer"] = $referer;
            $log_data["agent"] = $agent;
            $log_data["date"] =  date('YmdHis');
            //$log_data = "error4," . $login_id . "," . $resume_id . "," . $person_id . "," . $ip . "," . $remote_host . "," . $script_url . "," . $referer . "," . $_SERVER['HTTP_USER_AGENT'] . "," . date('YmdHis');
            $this->writeLog($log_data, "ng4");

            //セッションの破棄
            $this->session->unset_userdata('login_id');
            $this->session->unset_userdata('login_name');

            redirect('user/finishall' . $redirectParam);
        }else{
            $return = $this->db->get_where('members', array('id' => $login_id, 'del' => 0))->row();

            if(!empty($return)){
                //ログイン済
                if($return->id == $login_id){
                    //ログファイルに保存
                    $user = $this->db->get_where("members", array('id' => $login_id, 'del'=>0))->row();
                    $resume_id = $user->hrbc_resume_id;
                    $person_id = $user->hrbc_person_id;
                    $log_data = array();
                    $log_data["status"] = "ok";
                    $log_data["id"] = $login_id;
                    $log_data["resume"] = $resume_id;
                    $log_data["person"] = $person_id;
                    $log_data["ip"] = $ip;
                    $log_data["host"] = $remote_host;
                    $log_data["script"] = $script_url;
                    $log_data["referer"] = $referer;
                    $log_data["agent"] = $agent;
                    $log_data["date"] =  date('YmdHis');
                    //$log_data = "ok," . $login_id . "," . $resume_id . "," . $person_id . "," . $ip . "," . $remote_host . "," . $script_url . "," . $referer . "," . $_SERVER['HTTP_USER_AGENT'] . "," . date('YmdHis');
                    $this->writeLog($log_data , "ok");

                    $this->session->set_userdata('login_id', $return->id);
                    $this->session->set_userdata('login_name', $login_name);
                    //未ログイン
                }else{
                    //ログファイルに保存
                    $user = $this->db->get_where("members", array('id' => $login_id, 'del'=>0))->row();
                    $resume_id = $user->hrbc_resume_id;
                    $person_id = $user->hrbc_person_id;
                    $log_data = array();
                    $log_data["status"] = "error5";
                    $log_data["id"] = $login_id;
                    $log_data["resume"] = $resume_id;
                    $log_data["person"] = $person_id;
                    $log_data["ip"] = $ip;
                    $log_data["host"] = $remote_host;
                    $log_data["script"] = $script_url;
                    $log_data["referer"] = $referer;
                    $log_data["agent"] = $agent;
                    $log_data["date"] =  date('YmdHis');
                    //$log_data = "error5," . $login_id . "," . $resume_id . "," . $person_id . "," . $ip . "," . $remote_host . "," . $script_url . "," . $referer . "," . $_SERVER['HTTP_USER_AGENT'] . "," . date('YmdHis');
                    $this->writeLog($log_data, "ng5");

                    $this->session->unset_userdata('login_id');
                    $this->session->unset_userdata('login_name');

                    redirect('user/finishall' . $redirectParam);
                }
            }else{
                //ログファイルに保存
                $user = $this->db->get_where("members", array('id' => $login_id, 'del'=>0))->row();
                $resume_id = $user->hrbc_resume_id;
                $person_id = $user->hrbc_person_id;
                $log_data = array();
                $log_data["status"] = "error6";
                $log_data["id"] = $login_id;
                $log_data["resume"] = $resume_id;
                $log_data["person"] = $person_id;
                $log_data["ip"] = $ip;
                $log_data["host"] = $remote_host;
                $log_data["script"] = $script_url;
                $log_data["referer"] = $referer;
                $log_data["agent"] = $agent;
                $log_data["date"] =  date('YmdHis');
                //$log_data = "error6," . $login_id . "," . $resume_id . "," . $person_id . "," . $ip . "," . $remote_host . "," . $script_url . "," . $referer . "," . $_SERVER['HTTP_USER_AGENT'] . "," . date('YmdHis');
                $this->writeLog($log_data, "ng6");

                $this->session->unset_userdata('login_id');
                $this->session->unset_userdata('login_name');

                redirect('user/finishall' . $redirectParam);
            }
        }
    }

    //////////////////////////////////////////////////////////////////
    // ログ
    //////////////////////////////////////////////////////////////////
    function writeLog($log_data, $msg){
        //$dir = $_SERVER["DOCUMENT_ROOT"]."/../mypage_log/";
        //$file_name = $dir . date('Ymd') . "_data.txt";

        $data = array(
            'status' => $log_data["status"],
            'user_id' => $log_data["id"],
            'resume_id' => $log_data["resume"],
            'person_id' => $log_data["person"],
            'ip' => $log_data["ip"],
            'host' => $log_data["host"],
            'script' => $log_data["script"],
            'referer' => $log_data["referer"],
            'agent' => $log_data["agent"],
            'updated' => date('Y-m-d H:i:s')
        );

        $ret = $this->db->insert("logs", $data);
        if($msg != "ok"){
            if (empty($ret)) {
                $error = $this->db->error();
                $data = print_r($data, TRUE);
                $info = print_r($error, TRUE);
                $subject = "【code:log DB " . $msg . "】";
                $message = $data . $info;
                $this->errMail($subject, $message);
            }
        }

        /*
        $a = fopen($file_name, "a");
        $log_data = $log_data . "\r\n";
        @fwrite($a, $log_data);
        fclose($a);
        */
    }

    //////////////////////////////////////////////////////////////////
    // HRBC登録のログ
    //////////////////////////////////////////////////////////////////
    function writeRegistLog($tmp, $xml, $part){

        $errCode = "";
        $unit="";
        $json = json_encode($xml);
        $arr = json_decode($json,true);
        $cFlg = 0;

        if($xml->Code!=0){
            $rt = 1;
        }else{
            $rt = 2;
        }

        foreach($arr['Item'] as $k => $v){
            $unit .= $k . ":" . $v . "\n";
            if($k=="Code"){
                $errCode = $v;
                $cFlg = 1;
            }
        }

        if(empty($cFlg)){
            $errCode = $xml->Code;
        }

        $subject = "【HRBC更新】" . $part . " : " . $errCode . " : " . $rt;
        $message = $tmp . " 【return】" . $unit;
        $this->errMail($subject, $message);
    }


    //////////////////////////////////////////////////////////////////
    // Person取得
    //////////////////////////////////////////////////////////////////
    function getPerson($pid){
        $datas=array();

        $result = $this->getHrbcMaster();

        ///////////////////////////////////////////////////
        //person取得
        ///////////////////////////////////////////////////
        /*
        $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Id:eq=' . $pid));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/candidate?" . $url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist"));
                        //redirect($this->segment."/regist");
                        //break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist"));
                        //redirect($this->segment."/regist");
                        //break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist"));
                        //redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist"));
                //redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "candidate?partition=". $result["partition"] . "&count=1&start=0&condition=Person.P_Id:eq=".$pid;
        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);


        if($xml->Code!=0){
            $pErr=$xml->Code;

            $subject = "【user基本情報編集に失敗　code:getPerson】";
            $message = "user基本情報編集に失敗しました。". $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            //redirect("err/error_404");
            //exit;

        }

        //データ設定
        $json = json_encode($xml);
        $person = json_decode($json,true);

        $datas["name"] = $person["Item"]["Person.P_Name"];

        if(!empty($person["Item"]["Person.P_Reading"])){
            $datas["kana"] = $person["Item"]["Person.P_Reading"];
        }else{
            $datas["kana"] = "";
        }
        $datas["tel1"] = "";
        if(empty($person["Item"]["Person.P_Telephone"])){
            if(!empty($person["Item"]["Person.P_Mobile"])){
                $datas["tel1"] = $person["Item"]["Person.P_Mobile"];
            }else{
                $datas["tel1"] = "";
            }
        }else{
            $datas["tel1"] = $person["Item"]["Person.P_Telephone"];
        }

        $datas["mail1"] = $person["Item"]["Person.P_Mail"];

        $datas["id"] = $pid;

        return $datas;
    }

    //////////////////////////////////////////////////////////////////
    // Resume取得
    //////////////////////////////////////////////////////////////////
    function getResume($rid){

        $datas=array();

        $result = $this->getHrbcMaster();

        ////////////////////////////////////////////////////////////////
        //candidate Idからresume取得
        ///////////////////////////////////////////////////////////////
        /*
        $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Id:eq=' . $rid));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/resume?" . $url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist2"));
                        //redirect($this->segment."/regist");
                        //break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist2"));
                        //redirect($this->segment."/regist");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist2"));
                        redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist2"));
                redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "resume?partition=". $result["partition"] . "&count=1&start=0&condition=Resume.P_Id:eq=".$rid;
        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

        if($xml->Code!=0){
            $pErr=$xml->Code;

            $subject = "【user基本情報編集に失敗　code:getResume】";
            $message = "user基本情報編集に失敗しました。 " . $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            //redirect("err/error_404");
            //exit;
        }

        //データ設定
        $json = json_encode($xml);
        $resume = json_decode($json,true);

        if(!empty($resume["Item"]["Resume.P_DateOfBirth"])){
            $birthday = $resume["Item"]["Resume.P_DateOfBirth"];
            $datas["birthday"] =  $birthday;

            $now = date("Ymd");
            $birthday = str_replace("/", "", $birthday);//ハイフンを除去しています。
            $datas["age"] =  floor(($now-$birthday)/10000).'歳';

        }else{
            $datas["birthday"]="";
            $datas["age"]="";
        }

        if(!empty($resume["Item"]["Resume.P_ChangeJobsCount"])){
            $datas["company_number"] = $resume["Item"]["Resume.P_ChangeJobsCount"];
        }else{
            $datas["company_number"] = "";
        }

        $tmpgender="";
        if(!empty($resume["Item"]["Resume.P_Gender"])){
            $tmpgender = $resume["Item"]["Resume.P_Gender"];
        }
        if(!empty($tmpgender)){
            foreach($tmpgender as $k=>$v){
                $datas["sex"] = $v["Option.P_Name"];
            }
        }else{
            $datas["sex"] = "";
        }

        $tmppref="";
        if(!empty($resume["Item"]["Resume.U_7F3DF7F61EEF0679AA432646777832"])){
            $tmppref = $resume["Item"]["Resume.U_7F3DF7F61EEF0679AA432646777832"];
        }
        if(!empty($tmppref)){
            foreach($tmppref as $k=>$v){
                $datas["pref"] = $v["Option.P_Name"];
            }
        }else{
            $datas["pref"] = "";
        }

        $tmpschool="";
        if(!empty($resume["Item"]["Resume.U_673275A4DA445C9B23AC18BDD7C4DC"])){
            $tmpschool = $resume["Item"]["Resume.U_673275A4DA445C9B23AC18BDD7C4DC"];
        }
        if(!empty($tmpschool)){
            foreach($tmpschool as $k=>$v){
                $datas["school_div_id"] = $v["Option.P_Name"];
            }
        }else{
            $datas["school_div_id"] = "";
        }

        $tmpschool_name="";
        if(!empty($resume["Item"]["Resume.U_85DEA8FA18EF17830FD8A036EC95A4"])){
            $tmpschool_name = $resume["Item"]["Resume.U_85DEA8FA18EF17830FD8A036EC95A4"];
        }
        if(!empty($tmpschool_name)){
            $datas["school_name"] = $tmpschool_name;
        }else{
            $datas["school_name"] = "";
        }

        $tmpjokyo="";
        if(!empty($resume["Item"]["Resume.P_CurrentStatus"])){
            $tmpjokyo = $resume["Item"]["Resume.P_CurrentStatus"];
        }
        if(!empty($tmpjokyo)){
            foreach($tmpjokyo as $k=>$v){
                $datas["jokyo"] = $v["Option.P_Name"];
            }
        }else{
            $datas["jokyo"] = "";
        }

        if(!empty($resume["Item"]["Resume.U_91DBD8B96BAAD6B33B538CF2494377"])){
            $datas["kana"] = $resume["Item"]["Resume.U_91DBD8B96BAAD6B33B538CF2494377"];
        }else{
            $datas["kana"] =  "";
        }

        if(!empty($resume["Item"]["Resume.U_5F7FC5144BA471117A376B60BC8D90"])){
            $datas["comment"] = $resume["Item"]["Resume.U_5F7FC5144BA471117A376B60BC8D90"];
        }else{
            $datas["comment"] =  "";
        }

        if(!empty($resume["Item"]["Resume.U_13C45F7FFAFC1AB430E6038C76125A"])){
            $datas["addinfo"] = $resume["Item"]["Resume.U_13C45F7FFAFC1AB430E6038C76125A"];
        }else{
            $datas["addinfo"] =  "";
        }

        $jobCateoryInfo = $this->getJobCategoryInfo();
        $jobTantouCateoryInfo = $this->getTantouJobCategoryInfo();

        //職種
        $datas["kiboujob"]="";
        $tmpkiboujob = array();
        if (!empty($resume['Item']['Resume.U_14551B3DE0AAA0F98964734D422811'])) {
            foreach ($resume['Item']['Resume.U_14551B3DE0AAA0F98964734D422811'] as $k => $v) {
                foreach($jobCateoryInfo as $k2 => $v2){
                    if($v["Option.P_Name"] == $k2){
                        $tmpkiboujob[] = $v2;
                    }
                }
            }
        }
        if(is_array($tmpkiboujob)){
            $tmpkiboujob = array_unique($tmpkiboujob);
            if(!empty($tmpkiboujob)){
                foreach($tmpkiboujob as $k=>$v){
                    $datas["kiboujob"] .= $v . ",";
                }
            }
            $datas["kiboujob"] = rtrim($datas["kiboujob"], ",");
        }

        //都道府県
        $datas["kibouarea1"]="";
        if (!empty($resume['Item']['Resume.U_F3312A78116CAA30201FFFFC4CBC33'])) {
            foreach ($resume['Item']['Resume.U_F3312A78116CAA30201FFFFC4CBC33'] as $k => $v) {
                $datas["kibouarea1"] .= $v["Option.P_Name"] . ",";

            }
            $datas["kibouarea1"] = rtrim($datas["kibouarea1"], ",");
        }else{
            $datas["kibouarea1"]="";
        }

        $datas["kibouarea2"]="";
        if (!empty($resume['Item']['Resume.U_3CA45BBA1FBA2B4D0F2E7A6D25DD7D'])) {
            foreach ($resume['Item']['Resume.U_3CA45BBA1FBA2B4D0F2E7A6D25DD7D'] as $k => $v) {
                $datas["kibouarea2"] .= $v["Option.P_Name"] . ",";

            }
            $datas["kibouarea2"] = rtrim($datas["kibouarea2"], ",");
        }else{
            $datas["kibouarea2"]="";
        }

        $datas["kibouarea3"]="";
        if (!empty($resume['Item']['Resume.U_EF1BA8D1B218E6A4F11BF5EA910E36'])) {
            foreach ($resume['Item']['Resume.U_EF1BA8D1B218E6A4F11BF5EA910E36'] as $k => $v) {
                $datas["kibouarea3"] .= $v["Option.P_Name"] . ",";

            }
            $datas["kibouarea3"] = rtrim($datas["kibouarea3"], ",");
        }else{
            $datas["kibouarea3"]="";
        }

        //年収
        $datas["kibouincome"]="";
        if (!empty($resume['Item']['Resume.U_83F3DDBA6DEC80CE66E21A04631FA7'])) {
            foreach ($resume['Item']['Resume.U_83F3DDBA6DEC80CE66E21A04631FA7'] as $k => $v) {
                $datas["kibouincome"] .= $v["Option.P_Name"] . ",";

            }
            $datas["kibouincome"] = rtrim($datas["kibouincome"], ",");
        }else{
            $datas["kibouincome"]="";
        }

        $datas["earnings"]="";
        if (!empty($resume['Item']['Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504'])) {
            foreach ($resume['Item']['Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504'] as $k => $v) {
                $datas["earnings"] .= $v["Option.P_Name"] . ",";

            }
            $datas["earnings"] = rtrim($datas["earnings"], ",");
        }else{
            $datas["earnings"]="";
        }

        //転勤
        $datas["tenkin"]="";
        if (!empty($resume['Item']['Resume.U_EC012BD105993E9656D208951323F4'])) {
            foreach ($resume['Item']['Resume.U_EC012BD105993E9656D208951323F4'] as $k => $v) {
                $datas["tenkin"] .= $v["Option.P_Name"] . ",";

            }
            $datas["tenkin"] = rtrim($datas["tenkin"], ",");
        }else{
            $datas["tenkin"]="";
        }

        //担当職種
        $datas["tantou_jobs1"]="";
        $tmptantoujob1 = array();
        if (!empty($resume['Item']['Resume.U_90CE972D1600F17EDCEC4661728935'])) {
            foreach ($resume['Item']['Resume.U_90CE972D1600F17EDCEC4661728935'] as $k => $v) {
                foreach($jobTantouCateoryInfo as $k2 => $v2){
                    if($v["Option.P_Name"] == $k2){
                        $tmptantoujob1[] = $v2;
                    }
                }
            }
        }
        if(is_array($tmptantoujob1)){
            $tmptantoujob1 = array_unique($tmptantoujob1);
            if(!empty($tmptantoujob1)){
                foreach($tmptantoujob1 as $k=>$v){
                    $datas["tantou_jobs1"] .= $v . ",";
                }
            }
            $datas["tantou_jobs1"] = rtrim($datas["tantou_jobs1"], ",");
        }

        $datas["tantou_jobs2"]="";
        $tmptantoujob2 = array();
        if (!empty($resume['Item']['Resume.U_910B68CCF4A2F064E0059737A883C0'])) {
            foreach ($resume['Item']['Resume.U_910B68CCF4A2F064E0059737A883C0'] as $k => $v) {
                foreach($jobTantouCateoryInfo as $k2 => $v2){
                    if($v["Option.P_Name"] == $k2){
                        $tmptantoujob2[] = $v2;
                    }
                }
            }
        }
        if(is_array($tmptantoujob2)){
            $tmptantoujob2 = array_unique($tmptantoujob2);
            if(!empty($tmptantoujob2)){
                foreach($tmptantoujob2 as $k=>$v){
                    $datas["tantou_jobs2"] .= $v . ",";
                }
            }
            $datas["tantou_jobs2"] = rtrim($datas["tantou_jobs2"], ",");
        }

        $datas["tantou_jobs3"]="";
        $tmptantoujob3 = array();
        if (!empty($resume['Item']['Resume.U_7245CA44510F223C359FE2D7BC66DB'])) {
            foreach ($resume['Item']['Resume.U_7245CA44510F223C359FE2D7BC66DB'] as $k => $v) {
                foreach($jobTantouCateoryInfo as $k2 => $v2){
                    if($v["Option.P_Name"] == $k2){
                        $tmptantoujob3[] = $v2;
                    }
                }
            }
        }
        if(is_array($tmptantoujob3)){
            $tmptantoujob3 = array_unique($tmptantoujob3);
            if(!empty($tmptantoujob3)){
                foreach($tmptantoujob3 as $k=>$v){
                    $datas["tantou_jobs3"] .= $v . ",";
                }
            }
            $datas["tantou_jobs3"] = rtrim($datas["tantou_jobs3"], ",");
        }

        $datas["company_name1"]="";
        $datas["company_name2"]="";
        $datas["company_name3"]="";
        if(!empty($resume['Item']['Resume.U_9A9AD75E2CF88361F40E91D2B53876'])){
            $datas["company_name1"] = $resume['Item']['Resume.U_9A9AD75E2CF88361F40E91D2B53876'];
        }

        if(!empty($resume['Item']['Resume.U_AE99DF32764C633CAE8A79EDC68490'])){
            $datas["company_name2"] = $resume['Item']['Resume.U_AE99DF32764C633CAE8A79EDC68490'];
        }

        if(!empty($resume['Item']['Resume.U_C12A1CD0372B2FECA9055E0302F59D'])){
            $datas["company_name3"] = $resume['Item']['Resume.U_C12A1CD0372B2FECA9055E0302F59D'];
        }

        $datas["start_date1"]="";
        if(!empty($resume['Item']['Resume.U_12C7C4EC761C78DB864D9AD0FA73ED'])){
            $datas["start_date1"] = $resume['Item']['Resume.U_12C7C4EC761C78DB864D9AD0FA73ED'];
        }
        $datas["start_date2"]="";
        if(!empty($resume['Item']['Resume.U_7CCBD503A17CBD06078DB0907BEC9B'])){
            $datas["start_date2"] = $resume['Item']['Resume.U_7CCBD503A17CBD06078DB0907BEC9B'];
        }
        $datas["start_date3"]="";
        if(!empty($resume['Item']['Resume.U_F65492E72A71583EE72F032C73885A'])){
            $datas["start_date3"] = $resume['Item']['Resume.U_F65492E72A71583EE72F032C73885A'];
        }

        $datas["end_date1"]="";
        if(!empty($resume['Item']['Resume.U_4679EE290064C24E5E593F5772732D'])){
            $datas["end_date1"] = $resume['Item']['Resume.U_4679EE290064C24E5E593F5772732D'];
        }
        $datas["end_date2"]="";
        if(!empty($resume['Item']['Resume.U_FD0444A71EB7C820DC1455C4358EE8'])){
            $datas["end_date2"] = $resume['Item']['Resume.U_FD0444A71EB7C820DC1455C4358EE8'];
        }
        $datas["end_date3"]="";
        if(!empty($resume['Item']['Resume.U_F6662BA5A33D671A4516974181F5F0'])){
            $datas["end_date3"] = $resume['Item']['Resume.U_F6662BA5A33D671A4516974181F5F0'];
        }

        $datas["tenshoku_kiboubi"]="";
        if(!empty($resume['Item']['Resume.U_E7A1F795DE4A2C4F94EB1847006899'])){
            $datas["tenshoku_kiboubi"] = $resume['Item']['Resume.U_E7A1F795DE4A2C4F94EB1847006899'];
        }

        $datas["id"] = $rid;

        return $datas;
    }

    //////////////////////////////////////////////////////////////////
    // HRBCからJOB担当取得
    //////////////////////////////////////////////////////////////////
    function getJobOwner($job)
    {
        $result = $this->getHrbcMaster();

        /*
        $hrbc_url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Job.P_Id=' . $job, 'field'=>'Job.P_Id,Job.P_Owner'));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/job?" . $hrbc_url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 newpassword ui"));
                        //redirect($this->segment."/regist");
                        //break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 newpassword ui"));
                        //redirect($this->segment."/regist");
                        //break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other newpassword ui"));
                        //redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout newpassword ui"));
                //redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "job?partition=". $result["partition"] . "&count=1&start=0&field=Job.P_Id,Job.P_Owner&condition=Job.P_Id=".$job;

        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

        if($xml->Code!=0){
            $pErr=$xml->Code;
            $subject = "【code:mycontroller1】" . $pErr;
            $message = $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            //redirect("err/error_404");
            //exit;
        }

        //データ設定
        $json = json_encode($xml);
        $jobData = json_decode($json, true);

        $userId = $jobData["Item"]["Job.P_Owner"]["User"]["User.P_Id"];

        //////////////////////////////////////////////////////////////////////////////
        // User
        //////////////////////////////////////////////////////////////////////////////
        /*
        $hrbc_url = http_build_query(array('partition'=>$result["partition"], 'count'=>200 , 'start'=>0, 'request_type'=>1, 'user_type'=>-1));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/user?" . $hrbc_url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 newpassword ui"));
                        redirect($this->segment."/regist");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 newpassword ui"));
                        redirect($this->segment."/regist");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other newpassword ui"));
                        redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout newpassword ui"));
                redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "user?partition=". $result["partition"] . "&count=200&start=0&request_type=1&user_type=-1";

        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

        if($xml->Code!=0){
            $pErr=$xml->Code;
            $subject = "【code:mycontroller2】" . $pErr;
            $message = $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            //redirect("err/error_404");
            //exit;
        }
        //データ設定
        $json = json_encode($xml);
        $userData = json_decode($json, true);

        $userName="";
        if(!empty($userData)){
            foreach($userData as $k => $v){
                if(!empty($v)){
                    foreach (($v) as $k2 => $v2) {
                        if(!empty($v2["User.P_Name"])){
                            if($v2["User.P_Id"] == $userId){
                                $userName = $v2["User.P_Name"];
                            }
                        }
                    }
                }
            }
        }
        return $userName;
    }

    ///////////////////////////////////////////////////////////////
    // job情報の取得
    ///////////////////////////////////////////////////////////////
    function getJobInfo($param)
    {
        if(is_array($param)){
            $param = $param[1];
        }
        $content = array();
        if(!empty($param)){
            if(is_numeric($param)){
                //データ取得
//                $sql = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = ". HrbcPhaze . " and j.publish = ". HrbcPublish ." and j.job_id = " . $param . " group by j.job_id";
                $sql = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.job_id = " . $param . " group by j.job_id";
                $contents = $this->db->query($sql);
                if(!empty($contents->result()[0])){
                    $contents = $contents->result()[0];
                }

                if(!empty($contents->job_id)){
                    $content = $contents;
                }
            }
        }
        return $contents;
    }

    ///////////////////////////////////////////////////////////////
    // リクルート情報取得
    ///////////////////////////////////////////////////////////////
    function getRecruitInfo($content)
    {
        //企業担当の取得recruiterから
        $esc_clientid = $this->db->escape($content->client_id);
        $sql = "SELECT * from recruiters where r_client = " . $esc_clientid;
        $query = $this->db->query($sql);
        $recruiter = $query->result();

        return $recruiter;
    }

    ////////////////////////////////////////////////////////////////////////
    // JOBCategory 大項目の情報取得用
    ////////////////////////////////////////////////////////////////////////
    function getJobCategoryInfo()
    {
        $masters = $this->getHrbcMaster();
        $jobCategoryInfo = array();
        foreach($masters["jobcategory"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"]["Item"])){
                        foreach($v2['Items']['Item'] as $k3=>$v3) {
                            $jobCategoryInfo[$v3["Option.P_Name"]] = $v2["Option.P_Name"];
                        }
                    }
                }
            }
        }
        return $jobCategoryInfo;
    }

    ////////////////////////////////////////////////////////////////////////
    // 担当JOBの一覧
    ////////////////////////////////////////////////////////////////////////
    function getTantouJobCategoryInfo()
    {
        $masters = $this->getHrbcMaster();
        $jobTantouCategoryInfo = array();
        foreach($masters["jobmypagecd"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    $jobTantouCategoryInfo[$v2["Option.P_Name"]] = $v2["Option.P_Name"];
                }
            }
        }
        return $jobTantouCategoryInfo;
    }

    //////////////////////////////////////////////////////////////////////////
    // Jobから応募のプロセス登録
    //////////////////////////////////////////////////////////////////////////
    function setProcess($param , $person , $resume, $job, $recruit)
    {
        $result = $this->getHrbcMaster();

        $url =$this->hrbcUrl .  "process?partition=".$result["partition"];
        $now = date('Y/m/d H:i:s');
        $phasedate = date("Y/m/d H:i:s", strtotime($now . "-9 hour"));

        $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>-1</Process.P_Id><Process.P_Owner>6</Process.P_Owner><Process.P_Client>' . $job->client_id . '</Process.P_Client><Process.P_Recruiter>' . $recruit[0]->r_id . '</Process.P_Recruiter><Process.P_Job>' . $param . '</Process.P_Job><Process.P_Candidate>' . $person . '</Process.P_Candidate><Process.P_Resume>' . $resume . '</Process.P_Resume><Process.P_Phase><Option.U_001342 /></Process.P_Phase><Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11><Option.U_001311 /></Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11><Process.P_PhaseDate>' . $phasedate . '</Process.P_PhaseDate></Item></Process>';
        $tmp_xml = $xml;

        $header = array(
            "Content-Type: application/xml; charset=UTF-8",
            "X-porters-hrbc-oauth-token: " . $result['token']
        );

        $options = array('http' => array(
            'method' => 'POST',
            'content' => $xml,
            'header' => implode("\r\n", $header)
        ));

        if($xml = @file_get_contents($url, false, stream_context_create($options))){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 search apply "));
                        //redirect($this->segment."/index");
                        //break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 search apply"));
                        //redirect($this->segment."/index");
                        //break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other search apply"));
                        //redirect($this->segment."/index");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout search apply"));
                //redirect($this->segment."/index");
            }
        }
        $jErr="";
        $processId = "";
        $hrbcErr="";
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $arr = json_decode($json,true);

        $this->writeRegistLog($tmp_xml, $xml, "mycontroller_setprocess");

        if($xml->Code!=0) {
            if($arr["Code"]==301){
                $processId = "error301";
            }
            $hrbcErr = $xml->Code;
        }else{
            $jCode="";
            foreach($arr['Item'] as $k => $v){
                if($k=="Id"){
                    $processId = $v;
                }
                if($k=="Code"){
                    $jErr = $v;
                }
            }
        }
        $error="";
        if (!$processId) {
            $subject = "【process登録失敗】JOBID: " . $job->client_id;
            $message = "resumeId: " . $resume . " " . " personId: " . $person . " error: " . $hrbcErr;

            $this->errHurexMail($subject, $message);
        }
        return $processId;
    }

    //////////////////////////////////////////////////////////////////
    // Process取得
    //////////////////////////////////////////////////////////////////
    function getProcess($rid, $job){
        $datas=array();

        $result = $this->getHrbcMaster();

        ///////////////////////////////////////////////////
        //プロセス取得
        ///////////////////////////////////////////////////
        /*
        $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Process.P_Resume:eq=' . $rid . ",Process.P_Job:eq=" . $job->job_id));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/process?" . $url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist"));
                        redirect($this->segment."/regist");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist"));
                        redirect($this->segment."/regist");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist"));
                        redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist"));
                redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "process?partition=". $result["partition"] . "&count=1&start=0&condition=Process.P_Resume:eq=".$rid.",Process.P_Job:eq=".$job->job_id;

        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);


        if($xml->Code!=0){
            $pErr=$xml->Code;

            $subject = "【getProcess取得に失敗　code:getPerson】";
            $message = "getProcess取得に失敗しました。". $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            //redirect("err/error_404");
            //exit;

        }

        //データ設定
        $json = json_encode($xml);
        $process = json_decode($json,true);

        $process_id = $process["Item"]["Process.P_Id"];

        return $process_id;
    }

    //////////////////////////////////////////////////////////////////
    // Process取得
    //////////////////////////////////////////////////////////////////
    function getProcessInfo($rid, $job){
        $datas=array();

        $result = $this->getHrbcMaster();

        ///////////////////////////////////////////////////
        //プロセス取得
        ///////////////////////////////////////////////////
        /*
        $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Process.P_Resume:eq=' . $rid . ",Process.P_Job:eq=" . $job->job_id));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/process?" . $url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist"));
                        redirect($this->segment."/regist");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist"));
                        redirect($this->segment."/regist");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist"));
                        redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist"));
                redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "process?partition=". $result["partition"] . "&count=1&start=0&condition=Process.P_Resume:eq=".$rid.",Process.P_Job:eq=".$job->job_id;

        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

        if($xml->Code!=0){
            $pErr=$xml->Code;

            $subject = "【getProcessInfo取得に失敗　code:getPerson】";
            $message = "getProcessInfo取得に失敗しました。". $pErr . "\n" . "rid:" . $rid . " jobid:" . $job;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            //redirect("err/error_404");
            //exit;

        }

        //データ設定
        $json = json_encode($xml);
        $process = json_decode($json,true);

        $process = $process["Item"];

        return $process;
    }

    //////////////////////////////////////////////////////////////////
    // User取得
    //////////////////////////////////////////////////////////////////
    function getUser($uid){
        $datas=array();

        $result = $this->getHrbcMaster();

        ///////////////////////////////////////////////////
        //user
        ///////////////////////////////////////////////////
        /*
        $hrbc_url = http_build_query(array('partition'=>$result["partition"], 'count'=>200 , 'start'=>0, 'request_type'=>1, 'user_type'=>-1));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/user?" . $hrbc_url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 newpassword ui"));
                        redirect($this->segment."/regist");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 newpassword ui"));
                        redirect($this->segment."/regist");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other newpassword ui"));
                        redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout newpassword ui"));
                redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "user?partition=". $result["partition"] . "&count=200&start=0&request_type=1&user_type=-1";
        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);


        if($xml->Code!=0){
            $pErr=$xml->Code;
            $subject = "【code:mycontroller2】" . $pErr;
            $message = $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            //redirect("err/error_404");
            //exit;
        }
        //データ設定
        $json = json_encode($xml);
        $userData = json_decode($json, true);

        return $userData;
    }

    //////////////////////////////////////////////////////////
    //エントリーエラーメール
    //////////////////////////////////////////////////////////
    function entryErrMail($subject, $shimei, $mail1, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $company_name1, $company_name2, $company_name3, $tantou_job1, $tantou_job2, $tantou_job3, $start1, $start2, $start3, $end1, $end2, $end3, $earnings)
    {
        //エラー処理
        $message = <<< EOF
{unwrap}
マイページ新規登録のエラーがありました。

[氏名]
$shimei

[メールアドレス]
$mail1

[希望JOB]
$tmp_kibou_job

[希望収入]
$kibou_income

[転勤]
$tmp_tenkin

[直近会社１]
$company_name1
$tantou_job1
$start1 $end1

[直近会社２]
$company_name2
$tantou_job2
$start3 $end2

[直近会社３]
$company_name3
$tantou_job3
$start3 $end3

[現在の収入]
$earnings

{/unwrap}
EOF;
        $this->errMail($subject, $message);
    }

    //////////////////////////////////////////////////////////
    //　signupメール
    //////////////////////////////////////////////////////////
    function signupMail($to, $url)
    {
        $subject ="【HUREX株式会社】 会員仮登録完了メール";
        $this->signupSendMail($subject, $to, $url);
    }
    //////////////////////////////////////////////////////////
    //エントリー1サンクスメール 転職支援サービス
    //////////////////////////////////////////////////////////
    function signupSendMail($subject, $to, $url)
    {

        $message= <<< EOF
{unwrap}
転職支援会社 ヒューレックスへ会員登録をしていただき、
ありがとうございます。

利用規約に同意され会員登録を完了するには、
引き続き以下のＵＲＬにアクセスして認証手続きをお願いいたします。

■URL:
$url

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　  仙台市青葉区中央1-3-1 アエル17階
　E-mail　hrx@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■事業承継推進機構株式会社（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。



＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
{/unwrap}
EOF;

        $this->signupThanksMail($subject, $message, $to);
    }

    //////////////////////////////////////////////////////////
    //　エントリー1メール送信　転職支援サービス STEP1
    //////////////////////////////////////////////////////////
    function sendMailStep1($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $kibouarea1, $kibouarea2, $kibouarea3, $tenshoku_kiboubi, $age , $agent, $mailTo, $unit_url, $naiyo, $add)
    {
        if(empty($naiyo)){
            $subject ="【支援申込(" . $agent . ")  基本情報入力完了】 " . $age ." " . $tmp_company_number. " " . $school_div_id . " " . $sex . " "  . $shimei;
        }else{
            $subject ="【" . $naiyo . "(" . $agent . ") 基本情報入力完了 】 " . $add . $age ." " . $tmp_company_number. " " . $school_div_id . " " . $sex . " "  . $shimei;
        }
        $this->orderMailStep1($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $kibouarea1, $kibouarea2, $kibouarea3, $tenshoku_kiboubi, $age , $agent, $mailTo, $unit_url, $naiyo);

        //サンクスメール
        /*
        if($mailTo){
            $subject = "【" . $shimei . " 様】転職支援サービスへお申し込みいただき、ありがとうございます／ヒューレックス株式会社";
            $this->entry1ThanksMail($subject, $mailTo, $shimei);
        }
        */
    }

    //////////////////////////////////////////////////////////
    //　エントリー1メール送信　転職支援サービス STEP2
    //////////////////////////////////////////////////////////
    function sendMailStep2($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $tenshoku_kiboubi, $job_id, $unit_url, $age , $agent, $mailTo, $text, $add)
    {
        if(empty($text)){
            $subject ="【支援申込(" . $agent . ")  登録完了】 " . $age ." " . $tmp_company_number. " " . $school_div_id . " " . $sex . " "  . $shimei;
        }else{
            $subject ="【" . $text . "(" . $agent . ") 登録完了 】 " . $add . $age ." " . $tmp_company_number. " " . $school_div_id . " " . $sex . " "  . $shimei;
        }

        $this->orderMailStep2($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $tenshoku_kiboubi, $job_id, $unit_url, $age , $agent, $mailTo, $text);

        //サンクスメール
        if($mailTo){
            $subject = $shimei . " 様／登録完了のご連絡（ヒューレックス株式会社）";
            $this->entryCompThanksMail($subject, $mailTo, $shimei);
        }
    }

    //////////////////////////////////////////////////////////
    //エントリー完了サンクスメール
    //////////////////////////////////////////////////////////
    function entryCompThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
この度はヒューレックスのマイページにご登録いただきありがとうございます。
会員登録が完了いたしました。

求人の詳細閲覧やお問い合わせ、お気に入りへの登録が簡単に行えるようになります。
─・─・─・─・─・─・─・─・─・─・─・─
マイページにログインして求人に応募してみる
→　https://www.hurex.jp/mypage/user/login
─・─・─・─・─・─・─・─・─・─・─・─


【期間限定！】
今スグ使える！[職種別] 職務経歴書のテンプレートをプレゼント中！
↓職務経歴書のダウンロードはコチラ↓
https://www.hurex.jp/column/resume-adviser/sample.html?utm_source=mThankyouMail&utm_medium=email&utm_campaign=20171211resumeDL 


☆弊社転職アドバイザーよる、失敗しない転職ノウハウを公開中！☆

▼【保存版】履歴書を書く前に覚えておきたい基礎知識まとめ
https://goo.gl/UnC14d

▼【完全版】 プロが教える！応募で差がつく職務経歴書の書き方
https://goo.gl/Lp4HQu

▼【転職編】あなたは大丈夫？面接時の持ち物とマナー
https://goo.gl/BRtzx8

【地方を元気にすることが私たちのミッション】
私たちヒューレックスの役目は「地域経済の活性化に貢献して、地方を元気にすること」です。
そのために私たちが行っているのが、採用支援と転職支援。
企業には良い人材を、人材には良い企業を紹介し、地域経済を盛り上げています。

私たちは、全国の各地域に特化した求人をご紹介しております。大きな特徴は、企業開拓力。数多くの地元企業をクライアントに持つ銀行と提携することで、転職サイトや他の転職支援会社では扱っていない優良求人を紹介することができます。

ただ求人を紹介だけではなく、応募から内定までに発生する「手間」や「めんどう」もサポートいたします。

一人ひとりの気持ちに寄り添ったサポートで、あなたの転職を成功へと導きます。

今後ともヒューレックス株式会社をよろしくお願いいたします。


━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

ヒューレックス株式会社
[web] https://www.hurex.jp/

TEL：0120-14-1150（代表）
MAIL：hrx@hurex.co.jp

仙台本社　：宮城県仙台市青葉区中央1-3-1アエル17階
東京本社　：東京都中央区八重洲1-9-13八重洲ヤヨイビル5階
名古屋支社：愛知県名古屋市中村区名駅4-14-8名駅あさひビル6階
関西支社　：大阪府大阪市淀川区西中島5-5-15新大阪セントラルタワー南館10階
福岡支社　：福岡県福岡市博多区博多駅中央街8-27第16岡部ビル7階
沖縄支社　：沖縄県那覇市久米2-3-15COI那覇ビル5階

━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━

{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    //////////////////////////////////////////////////////////
    //　エントリー1メール送信　転職支援サービス STEP2 uiターン
    //////////////////////////////////////////////////////////
    function sendMailStep2_ui($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $tenshoku_kiboubi, $job_id, $unit_url, $age , $agent, $mailTo, $text, $add,$param)
    {
        if(empty($text)){
            $subject ="【支援申込(" . $agent . ")  登録完了】 " . $age ." " . $tmp_company_number. " " . $school_div_id . " " . $sex . " "  . $shimei;
        }else{
            $subject ="【" . $text . "(" . $agent . ") 登録完了 】 " . $add . $age ." " . $tmp_company_number. " " . $school_div_id . " " . $sex . " "  . $shimei;
        }

        $this->orderMailStep2($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $tenshoku_kiboubi, $job_id, $unit_url, $age , $agent, $mailTo, $text);

        /*
        if($mailTo){
            $subject = "【" . $shimei . " 様】東北Ｕ・Ｉターン転職相談会 in 東京へお申し込みいただき、ありがとうございます／ヒューレックス株式会社";
            $this->entryCompThanksMail_ui($subject, $mailTo, $shimei);
        }
        */
        //サンクスメール
        if($mailTo){
            if($param=="j"){
                $subject = "【" . $shimei . " 様】東北Ｕ・Ｉターン転職相談会 in 東京へお申し込みいただき、ありがとうございます／ヒューレックス株式会社";
                $this->entry2ThanksMail($subject, $mailTo,$shimei);
            }else{
                $subject = "【" . $shimei . " 様】年末年始Uターン&東北転職相談会へお申し込みいただき、ありがとうございます／ヒューレックス株式会社";
//                $subject = "【" . $shimei . " 様】お盆休みUターン&地元転職相談会へお申し込みいただき、ありがとうございます／ヒューレックス株式会社";
                $this->entry2ThanksMail2($subject, $mailTo,$shimei);
            }
        }
    }

    //////////////////////////////////////////////////////////
    //エントリー完了サンクスメール uiターン 使わない
    //////////////////////////////////////////////////////////
    function entryCompThanksMail_ui($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

お世話になっております。
転職支援会社 ヒューレックスでございます。

この度は、弊社ホームページより東北Ｕ・Ｉターン転職相談会 in 東京へお申し込みいただき、誠にありがとうございました。

担当が確認後、改めてご連絡させていただきます。
時期によっては、お時間が掛かることがございますので、ご了承ください。

ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱いおよび管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　 仙台市青葉区中央1-3-1 アエル17階
　E-mail　hrx@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
https://www.hurex.jp/
　■マリッジパートナーズ
https://www.mpartners.jp/
　■事業承継推進機構株式会社（AOBA）
http://aoba-east.jp/

【facebook page】
　■ヒューレックス
http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。

{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    //////////////////////////////////////////////////////////
    //エントリー1サンクスメール 転職支援サービス 未使用
    //////////////////////////////////////////////////////////
    function entry1ThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

お世話になっております。
転職支援会社 ヒューレックスでございます。

この度は、弊社ホームページより転職支援サービスへお申し込みいただき、誠にありがとうございます。

今後ご転職活動を支援させていただきますので、どうぞ宜しくお願い致します。

マイページが開設されましたので、以下のURLよりアクセスしてください。

マイページでは、
・ご登録いただいた希望条件に合致した求人のご検索
・気になる求人の保存
・求人詳細への問合せ
・求人へのご応募
・選考が進んでいる求人の進行管理
等が可能です。

ぜひご活用ください。

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【マイページURL】
　　▼　　▼　　▼　　▼　　▼
https://www.hurex.jp/mypage/user/login/
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱いおよび管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　  仙台市青葉区中央1-3-1 アエル17階
　E-mail　hrx@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■事業承継推進機構株式会社（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。



＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    //////////////////////////////////////////////////////////
    //　エントリー2メール送信 新規登録者　UIターン
    //////////////////////////////////////////////////////////
    function sendMail2_new($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number, $jokyo, $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $tmp_kibou_area, $unit_url,$age, $agent, $param, $person, $resume, $naiyo, $mailTo)
    {
        $subject = "【" . $naiyo . " (" . $agent . ")】" . $age . "歳 " . $tmp_company_number . " " . $school_div_id . " " . $sex . " " . $shimei;
        $this->entry2OrderMail($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number, $jokyo, $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $tmp_kibou_area, $unit_url);

        //サンクスメール
        if($mailTo){
            $subject = "【" . $shimei . " 様】東北Ｕ・Ｉターン転職相談会 in 東京へお申し込みいただき、ありがとうございます／ヒューレックス株式会社";
            $this->entry2ThanksMail($subject, $mailTo, $shimei);
        }
    }

    //////////////////////////////////////////////////////////
    //　エントリー2メール送信　既存登録者
    //////////////////////////////////////////////////////////
    function sendMail2($agent, $param, $person, $resume, $comment, $naiyo)
    {
        $subject = "【既登録者" . $naiyo . " (" . $agent . ")】" . $person["name"] . " " . $person["tel1"];
        $person_url = "{unwrap}Person: https://hrbc-jp.porterscloud.com/can/search/id/" . $person["id"] . "#/dv/1:" . $person["id"] . "/{/unwrap}";
        $resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5#/dv/17:" . $resume["id"] ."/{/unwrap}";
        //$resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5{/unwrap}";
        //$unit_url = $person_url . "\n" . $resume_url;
        $unit_url = $resume_url;
        $message = $naiyo . "　登録通知" . "\n" . $comment . "\n" . $unit_url;
        $this->successMail($subject, $message);

        //サンクスメール
        if($person["mail1"]){
            if($param=="j"){
                $subject = "【" . $person["name"] . " 様】東北Ｕ・Ｉターン転職相談会 in 東京へお申し込みいただき、ありがとうございます／ヒューレックス株式会社";
                $this->entry2ThanksMail($subject, $person["mail1"],$person["name"]);
            }else{
                $subject = "【" . $person["name"] . " 様】年末年始Uターン&地元転職相談会へお申し込みいただき、ありがとうございます／ヒューレックス株式会社";
                $this->entry2ThanksMail2($subject, $person["mail1"],$person["name"]);
            }
        }
    }

    //////////////////////////////////////////////////////////
    //エントリー2サンクスメール UIターン
    //////////////////////////////////////////////////////////
    function entry2ThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

お世話になっております。
転職支援会社 ヒューレックスでございます。

この度は、弊社ホームページより東北Ｕ・Ｉターン転職相談会 in 東京へお申し込みいただき、誠にありがとうございました。

担当が確認後、改めてご連絡させていただきます。
時期によっては、お時間が掛かることがございますので、ご了承ください。

ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱いおよび管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　 仙台市青葉区中央1-3-1 アエル17階
　E-mail　hrx@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
https://www.hurex.jp/
　■マリッジパートナーズ
https://www.mpartners.jp/
　■事業承継推進機構株式会社（AOBA）
http://aoba-east.jp/

【facebook page】
　■ヒューレックス
http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。
{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    //////////////////////////////////////////////////////////
    //エントリー2サンクスメール 長期休暇
    //////////////////////////////////////////////////////////
    function entry2ThanksMail2($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

お世話になっております。
転職支援会社 ヒューレックスでございます。

この度は、弊社ホームページより年末年始Uターン&地元転職相談会へお申し込みいただき、誠にありがとうございました。

担当が確認後、改めてご連絡させていただきます。
時期によっては、お時間が掛かることがございますので、ご了承ください。

ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱いおよび管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　 仙台市青葉区中央1-3-1 アエル17階
　E-mail　hrx@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
https://www.hurex.jp/
　■マリッジパートナーズ
https://www.mpartners.jp/
　■事業承継推進機構株式会社（AOBA）
http://aoba-east.jp/

【facebook page】
　■ヒューレックス
http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。



＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    //////////////////////////////////////////////////////////
    //　エントリー４メール送信 新規登録者　求人詳細を聞く
    //////////////////////////////////////////////////////////
    function sendMail4_new($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $job_id, $unit_url, $age , $agent, $mailTo)
    {
        $user = $this->getJobOwner($job_id);

        $subject ="【求人問合 (" . $agent . ")】" . $age ."歳 " . $tmp_company_number . " " . $school_div_id . " " . $sex . " "  . $shimei;
        $this->entry3OrderMail($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $job_id, $unit_url, $age , $agent, $mailTo);

        //サンクスメール
        if($mailTo){
            $subject = "【" . $shimei . " 様】求人へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $this->entry4newThanksMail($subject, $mailTo, $shimei);
        }
    }

    //////////////////////////////////////////////////////////
    //エントリー4サンクスメール 新規登録者のjob詳細を聞く
    //////////////////////////////////////////////////////////
    function entry4newThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

お世話になっております。
転職支援会社 ヒューレックスでございます。

この度は、弊社ホームページより求人へお問い合わせいただき、誠にありがとうございます。
ご経験やご希望条件が、企業が定める求人の応募要件と合致されていない場合は、求人の詳細に関してお伝えできない場合がございます。
ご了承いただきますようお願い致します。

また、マイページが開設されましたので、以下のURLよりアクセスしてください。

マイページでは、
・ご登録いただいた希望条件に合致した求人のご検索
・気になる求人の保存
・求人詳細への問合せ
・求人へのご応募
・選考が進んでいる求人の進行管理
等が可能です。

ぜひご活用ください。

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【マイページURL】
　　▼　　▼　　▼　　▼　　▼
https://www.hurex.jp/mypage/user/login/
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱いおよび管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　  仙台市青葉区中央1-3-1 アエル17階
　E-mail　hrx@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■事業承継推進機構株式会社（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。



＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    //////////////////////////////////////////////////////////
    //　エントリー４メール送信　既存登録者　求人詳細を聞く
    //////////////////////////////////////////////////////////
    function sendMail4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo, $add)
    {
        //JOB担当
        $user = $this->getJobOwner($jobinfo->job_id);

        //$subject ="【既登録者 (" . $agent . ")：求人詳細問合わせ】JOB:" . $param . " " . $person["name"] . " " . $person["tel1"];
        $subject ="■■問合わせ■■　JOB担当:" . $add["job_owner"] . " （打診担当:" . $add["process_owner"] . "）" . $add["client_name"];
        $person_url = "Person: https://hrbc-jp.porterscloud.com/can/search/id/" . $person["id"] . "#/dv/1:" . $person["id"] . "/";
        //$resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5/{/unwrap}";
        $resume_url  ="\n\n【キャンディデイトBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5#/dv/17:" . $resume["id"] ."/";

        $job_url ="\n\n【応募JobBCリンク】" . "\n" .  "https://hrbc-jp.porterscloud.com/job/search/id/".$param. "?menu_id=3";

        $process_url ="\n\n【選考プロセスBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $resume["id"] . "#/dv/7:" . $processId . "/";
        $unit_url = "\n" . $resume_url . "\n" . $job_url . "\n" . $process_url;

        $kana="";
        if(!empty($person["kana"])){
            $kana  = $person["kana"];
        }else if(!empty($resume["kana"])){
            $kana = $resume["kana"];
        }

        $message = "マイページより求人詳細問合せがありました。" . "\n\n[氏名]\n" . $person["name"] . "\n\n[ふりがな]\n" . $kana . "\n\n[生年月日]\n" . $resume["birthday"] . "\n\n[経験社数]\n" .$resume["company_number"] . "社" . "\n\n[Jobの担当者]\n" . $user . "\n\n[企業名]\n" . $jobinfo->c_title . "\n\n[ポジション名]\n". $jobinfo->job_title . "\n" . $unit_url;
        $this->successMail($subject, $message);

        /*
        //サンクスメール
        if($person["mail1"]){
            $subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . " "  . $jobinfo->c_title . "へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $this->entry4ThanksMail($subject, $person["mail1"], $person["name"]);
        }
        */
    }

    //////////////////////////////////////////////////////////
    //　エントリー４メール送信　既存登録者　登録済み to hurex
    //////////////////////////////////////////////////////////
    function sendMail4_alreay($agent, $param, $person, $resume, $processId, $recruits, $jobinfo, $process_id)
    {
        //JOB担当
        $user = $this->getJobOwner($jobinfo->job_id);

        $subject ="【登録済み求人詳細問い合わせ (" . $agent . ")：既登録者】JOB:" . $param . " " . $person["name"] . " " . $person["tel1"];
        $person_url = "Person: https://hrbc-jp.porterscloud.com/can/search/id/" . $person["id"] . "#/dv/1:" . $person["id"] . "/";
        //$resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5/{/unwrap}";
        $resume_url  ="Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5#/dv/17:" . $resume["id"] ."/";

        $job_url ="\n\n【応募JobBCリンク】" . "\n" .  "https://hrbc-jp.porterscloud.com/job/search/id/".$param. "?menu_id=3";
        $process_url ="\n\n【選考プロセスBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $resume["id"] . "#/dv/7:" . $process_id . "/";
        $unit_url = "\n" . $resume_url . "\n" . $job_url . "\n" . $process_url;

        $kana="";
        if(!empty($person["kana"])){
            $kana  = $person["kana"];
        }else if(!empty($resume["kana"])){
            $kana = $resume["kana"];
        }

        $message = "登録済み求人へマイページより求人詳細問合せがありました。" . "\n\n[氏名]\n" . $person["name"] . "\n\n[ふりがな]\n" . $kana . "\n\n[生年月日]\n" . $resume["birthday"] . "\n\n[経験社数]\n" .$resume["company_number"] . "社" . "\n\n[Jobの担当者]\n" . $user . "\n\n[企業名]\n" . $jobinfo->c_title . "\n\n[ポジション名]\n". $jobinfo->job_title . "\n" . $unit_url;
        $this->successMail($subject, $message);

        //サンクスメール
        if($person["mail1"]){
//            $subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . " "  . $jobinfo->c_title . "お問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $subject = "【" . $person["name"] . " 様】 JOB" . $jobinfo->job_id . "：お問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $this->alreadyThanksMail($subject, $person["mail1"], $person["name"]);
        }
    }

    //////////////////////////////////////////////////////////
    //　エントリー４サンクスメール送信　既存登録者　求人詳細を聞く
    //////////////////////////////////////////////////////////
    function sendMailThanks4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo)
    {
        //サンクスメール
        if($person["mail1"]){
//            $subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . " "  . $jobinfo->c_title . "へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . "お問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $this->entry4ThanksMail($subject, $person["mail1"], $person["name"]);
        }
    }

    //////////////////////////////////////////////////////////
    //エントリー4サンクスメール 既登録者のjob詳細を聞く
    //////////////////////////////////////////////////////////
    function entry4ThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

お世話になっております。
転職支援会社 ヒューレックスでございます。

この度は、マイページより求人へお問い合わせいただき、誠にありがとうございます。
ご経験やご希望条件が、企業が定める求人の応募要件と合致されていない場合は、求人の詳細に関してお伝えできない場合がございます。
ご了承いただきますようお願い致します。

確認の上のご回答となるため、ご回答までお時間をいただくことがございますので、ご了承ください。

ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱いおよび管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　  仙台市青葉区中央1-3-1 アエル17階
　E-mail　hrx@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■事業承継推進機構株式会社（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。
{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    //////////////////////////////////////////////////////////
    //　マイページ既存登録者が登録済みのジョブに応募したときのサンクス
    //////////////////////////////////////////////////////////
    function alreadyThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

お世話になっております。
転職支援会社 ヒューレックスでございます。

この度は、弊社取り扱い求人へお問い合わせいただき、誠にありがとうございました。

今回の求人は、以前お問い合わせいただいたことがあるか、既にご案内差し上げたことのある求人でございました。

一度、マイページ上『応募状況』よりご確認ください。

なお、お心当たりのない方は下記までご連絡いただければ幸いです。

■ マイページ　ヘルプデスク
E-mail　hrx@hurex.co.jp


ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱い及び管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　 仙台市青葉区中央1-3-1 アエル17階
　E-mail　hrx@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
https://www.hurex.jp/
　■マリッジパートナーズ
https://www.mpartners.jp/
　■東日本事業承継推進機構（AOBA）
http://aoba-east.jp/

【facebook page】
　■ヒューレックス
http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
https://www.facebook.com/marriagepartners
────────────────────

このメールにお心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。
{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }


    //////////////////////////////////////////////////////////
    //　エントリー5 プロセスからの応募
    //////////////////////////////////////////////////////////
    function sendMail5($agent, $param, $person, $resume, $jobinfo, $unit_url, $to, $add)
    {
        //JOB担当
        $user = $this->getJobOwner($jobinfo->job_id);

//        $subject ="【既登録者 (" . $agent . ")：Job打診からの申込】JOB:" . $param . " " . $person["name"] . " " . $person["tel1"];
        $subject ="■■応募承諾■■　JOB担当:" . $add["job_owner"] . " （打診担当:" . $add["process_owner"] . "）" . $add["client_name"];

        $kana="";
        if(!empty($person["kana"])){
            $kana  = $person["kana"];
        }else if(!empty($resume["kana"])){
            $kana = $resume["kana"];
        }

        $message = "マイページからJob打診からの応募がありました。" . "\n\n[氏名]\n" . $person["name"] . "\n\n[ふりがな]\n" . $kana . "\n\n[生年月日]\n" . $resume["birthday"] . "\n\n[経験社数]\n" .$resume["company_number"] . "社" . "\n\n[Jobの担当者]\n" . $user . "\n\n[企業名]\n" . $jobinfo->c_title . "\n\n[ポジション名]\n". $jobinfo->job_title . "\n\n" . $unit_url;
        $this->successMailProcess($subject, $message, $to);

        //サンクスメール
        if($person["mail1"]){
            //$subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . " "  . $jobinfo->c_title . "へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $subject = "【" . $person["name"] . " 様】求人へご応募いただき、ありがとうございます。／ヒューレックス株式会社";
            //$this->entry4ThanksMail($subject, $person["mail1"], $person["name"]);
            $this->jobdashinThanksMail($subject, $person["mail1"], $person["name"], $jobinfo);
        }

    }

    //////////////////////////////////////////////////////////
    //　エントリー６　お気に入りからの応募
    //////////////////////////////////////////////////////////
    function sendMail6($agent, $param, $person, $resume, $processId, $recruits, $jobinfo, $add)
    {
        //JOB担当
        $user = $this->getJobOwner($jobinfo->job_id);

//        $subject ="【既登録者 (" . $agent . ")：お気に入りからの申込】JOB:" . $param . " " . $person["name"] . " " . $person["tel1"];
        $subject ="■■問合わせ■■　JOB担当:" . $add["job_owner"] . " （打診担当:" . $add["process_owner"] . "）" . $add["client_name"];

        $person_url = "Person: https://hrbc-jp.porterscloud.com/can/search/id/" . $person["id"] . "#/dv/1:" . $person["id"] . "/";

        //$resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5/{/unwrap}";
        $resume_url  ="\n\n【キャンディデイトBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5#/dv/17:" . $resume["id"] ."/";

        $job_url ="\n\n【応募JobBCリンク】" . "\n" .  "https://hrbc-jp.porterscloud.com/job/search/id/".$param. "?menu_id=3";
        $process_url ="\n\n【選考プロセスBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $resume["id"] . "#/dv/7:" . $processId . "/";
        $unit_url = $resume_url . "\n" . $job_url . "\n" . $process_url;

        $kana="";
        if(!empty($person["kana"])){
            $kana  = $person["kana"];
        }else if(!empty($resume["kana"])){
            $kana = $resume["kana"];
        }

        $message = "お気に入りからお問い合わせがありました。" . "\n\n[氏名]\n" . $person["name"] . "\n\n[ふりがな]\n" . $kana . "\n\n[生年月日]\n" . $resume["birthday"] . "\n\n[経験社数]\n" .$resume["company_number"] . "社" . "\n\n[Jobの担当者]\n" . $user . "\n\n[企業名]\n" . $jobinfo->c_title . "\n\n[ポジション名]\n". $jobinfo->job_title . "" . $unit_url;
        $this->successMail($subject, $message);

        /*
        //サンクスメール
        if($person["mail1"]){
            $subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . " "  . $jobinfo->c_title . "へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $this->entry4ThanksMail($subject, $person["mail1"], $person["name"]);
        }
        */
    }

    //////////////////////////////////////////////////////////
    //　エントリー7 JOB打診の辞退
    //////////////////////////////////////////////////////////
    function sendMail7($agent, $param, $person, $resume, $jobinfo, $unit_url, $to, $add)
    {
        //JOB担当
        $user = $this->getJobOwner($jobinfo->job_id);

        //$subject ="【既登録者 (" . $agent . ")：Job打診からの辞退】JOB:" . $param . " " . $person["name"] . " " . $person["tel1"];
        $subject ="■■本人NG■■　JOB担当:" . $add["job_owner"] . " （打診担当:" . $add["process_owner"] . "）" . $add["client_name"];

        $kana="";
        if(!empty($person["kana"])){
            $kana  = $person["kana"];
        }else if(!empty($resume["kana"])){
            $kana = $resume["kana"];
        }

        $message = "マイページからJob打診の辞退がありました。" . "\n\n[氏名]\n" . $person["name"] . "\n\n[ふりがな]\n" . $kana . "\n\n[生年月日]\n" . $resume["birthday"] . "\n\n[経験社数]\n" .$resume["company_number"] . "社" . "\n\n[Jobの担当者]\n" . $user . "\n\n[企業名]\n" . $jobinfo->c_title . "\n\n[ポジション名]\n". $jobinfo->job_title . "\n\n" . $unit_url;
        $this->successMailProcess($subject, $message, $to);

        /*
        //サンクスメール
        if($person["mail1"]){
            $subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . " "  . $jobinfo->c_title . "へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $this->entry4ThanksMail($subject, $person["mail1"], $person["name"]);
        }
        */
    }

    //////////////////////////////////////////////////////////
    //　エントリー8 JOB打診の辞退理由
    //////////////////////////////////////////////////////////
    function sendMail8($agent, $param, $person, $resume, $jobinfo, $unit_url, $to, $add)
    {
        //JOB担当
        $user = $this->getJobOwner($jobinfo->job_id);

        //$subject ="【既登録者 (" . $agent . ")：Job打診からの辞退】JOB:" . $param . " " . $person["name"] . " " . $person["tel1"];
        $subject ="■■本人NG：理由■■　JOB担当:" . $add["job_owner"] . " （打診担当:" . $add["process_owner"] . "）" . $add["client_name"];

        $kana="";
        if(!empty($person["kana"])){
            $kana  = $person["kana"];
        }else if(!empty($resume["kana"])){
            $kana = $resume["kana"];
        }

        $message = "マイページからJob打診の辞退理由が登録されました。" . "\n\n[氏名]\n" . $person["name"] . "\n\n[ふりがな]\n" . $kana . "\n\n[Jobの担当者]\n" . $user . "\n\n[企業名]\n" . $jobinfo->c_title . "\n\n[ポジション名]\n". $jobinfo->job_title . "\n\n" . $unit_url;
        $this->successMailProcess($subject, $message, $to);

        /*
        //サンクスメール
        if($person["mail1"]){
            $subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . " "  . $jobinfo->c_title . "へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $this->entry4ThanksMail($subject, $person["mail1"], $person["name"]);
        }
        */
    }

    //////////////////////////////////////////////////////////
    // オーダーメール STEP1
    //////////////////////////////////////////////////////////
    function orderMailStep1($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $kibouarea1, $kibouarea2, $kibouarea3, $tenshoku_kiboubi, $age , $agent, $mailTo, $unit_url, $naiyo)
    {
        if(!empty($naiyo)){
            $comment_midashi = "希望参加日";
        }else{
            $comment_midashi = "その他";
        }
        $message = <<< EOF
{unwrap}
ご担当者様
ホームページより、 $shimei 様のエントリーがありました。
HRBCにログインし、エントリー情報をご確認下さい。

[氏名]
$shimei

[ふりがな]
$kana

[生年月日]
$birthday

[性別]
$sex

[現住所]
$pref

[電話番号]
$tel1

[最終学歴]
$school_div_id

[学校名]
$school_name

[経験社数]
$tmp_company_number

[就業状況]
$jokyo

[転職希望日]
$tenshoku_kiboubi

[$comment_midashi]
$comment

[希望勤務地１]
$kibouarea1

[希望勤務地２]
$kibouarea2

[希望勤務地３]
$kibouarea3

{/unwrap}
$unit_url
EOF;
        $this->entryOrderMail($subject, $message);
    }

    //////////////////////////////////////////////////////////
    // オーダーメール STEP2
    //////////////////////////////////////////////////////////
    function orderMailStep2($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $tenshoku_kiboubi, $job_id, $unit_url, $age , $agent, $mailTo)
    {
        if(!empty($text)){
            $comment_midashi = "希望参加日";
        }else{
            $comment_midashi = "その他";
        }
        $message = <<< EOF
{unwrap}
ご担当者様
ホームページより、 $shimei 様のエントリーがありました。
HRBCにログインし、エントリー情報をご確認下さい。

[氏名]
$shimei

[ふりがな]
$kana

[生年月日]
$birthday

[性別]
$sex

[現住所]
$pref

[電話番号]
$tel1

[最終学歴]
$school_div_id

[学校名]
$school_name

[経験社数]
$tmp_company_number

[就業状況]
$jokyo

[転職希望日]
$tenshoku_kiboubi

[$comment_midashi]
$comment

[希望職種]
$tmp_kibou_job

[希望年収]
$kibou_income

[転勤]
$tmp_tenkin

[希望勤務地１]
$kibouarea1

[希望勤務地２]
$kibouarea2

[希望勤務地３]
$kibouarea3

[直近の会社１]
$company_name1
$tantou_jobs1
$start_date1 ～ $end_date1

[直近の会社２]
$company_name2
$tantou_jobs2
$start_date2 ～ $end_date2

[直近の会社３]
$company_name3
$tantou_jobs3
$start_date3 ～ $end_date3

[現在の年収]
$current_earnings

{/unwrap}
$unit_url
EOF;

        $this->entryOrderMail($subject, $message);
    }

    //////////////////////////////////////////////////////////
    //エントリー2オーダーメール UIターン
    //////////////////////////////////////////////////////////
    function entry2OrderMail($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name,$tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $tmp_kibou_area, $unit_url)
    {
        $message = <<< EOF
{unwrap}
ご担当者様
ホームページより、 $shimei 様のエントリーがありました。
HRBCにログインし、エントリー情報をご確認下さい。

[氏名]
$shimei

[ふりがな]
$kana

[生年月日]
$birthday

[性別]
$sex

[現住所]
$pref

[電話番号]
$tel1

[最終学歴]
$school_div_id

[学校名]
$school_name

[経験社数]
$tmp_company_number

[就業状況]
$jokyo

[ご希望の参加日]
$comment

[希望職種]
$tmp_kibou_job

[年収]
$kibou_income

[転勤]
$tmp_tenkin

[希望地域]
$tmp_kibou_area

{/unwrap}
$unit_url
EOF;



        $this->entryOrderMail($subject, $message);
    }

    //////////////////////////////////////////////////////////
    //エントリー3オーダーメール JOB詳細を聞く
    //////////////////////////////////////////////////////////
    function entry3OrderMail($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $job_id, $unit_url, $age , $agent, $mailTo)
    {
        $message = <<< EOF
{unwrap}
ご担当者様
ホームページより、 $shimei 様のエントリーがありました。
HRBCにログインし、エントリー情報をご確認下さい。

[氏名]
$shimei

[ふりがな]
$kana

[生年月日]
$birthday

[性別]
$sex

[現住所]
$pref

[電話番号]
$tel1

[最終学歴]
$school_div_id

[学校名]
$school_name

[経験社数]
$tmp_company_number

[就業状況]
$jokyo

[その他]
$comment

[JOB]
$job_id

[希望職種]
$tmp_kibou_job

[希望年収]
$kibou_income

[転勤]
$tmp_tenkin

[希望勤務地１]
$kibouarea1

[希望勤務地２]
$kibouarea2

[希望勤務地３]
$kibouarea3

[直近の会社１]
$company_name1  
$tantou_jobs1
$start_date1 ～ $end_date1

[直近の会社２]
$company_name2
$tantou_jobs2
$start_date2 ～ $end_date2

[直近の会社３]
$company_name3
$tantou_jobs3
$start_date3 ～ $end_date3

[現在の年収]
$current_earnings

{/unwrap}
$unit_url
EOF;



        $this->entryOrderMail($subject, $message);
    }

    //////////////////////////////////////////////////////////
    //エントリー4オーダーメール JOB詳細を聞く 既登録者
    //////////////////////////////////////////////////////////
    function entry4OrderMail($subject, $shimei, $birthday, $company_number, $tantou, $client, $job_title ,$jobid, $unit_url)
    {
        $message = <<< EOF
{unwrap}
マイページより求人詳細問合せがありました。
$shimei
$birthday
$company_number
$tantou
$client
$job_title
{/unwrap}
$unit_url
EOF;



        $this->entryOrderMail($subject, $message);
    }

    //////////////////////////////////////////////////////////
    //ジョブ打診　サンクスメール
    //////////////////////////////////////////////////////////
    function jobdashinThanksMail($subject, $mailTo, $shimei, $jobinfo)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

この度は、弊社の取り扱い求人にご応募いただき、誠にありがとうございます。

＜ご応募いただいた求人＞
[企業名]
$jobinfo->c_title
[ポジション名]
$jobinfo->job_title

たしかにご希望を承りました。
この求人の担当コンサルタントが応募の手続きを進めてまいります。
進捗がありましたら、随時ご報告させていただきますので、少しお時間をいただければと存じます。
尚、応募にあたっての書類準備等がある場合は、後ほど担当コンサルタントからご連絡させていただきます。
以上、今後とも、どうぞ宜しくお願い申し上げます。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　  仙台市青葉区中央1-3-1 アエル17階
　E-mail　hrx@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■事業承継推進機構株式会社（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。
{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    //////////////////////////////////////////////////////////
    //メルマガ停止メール
    //////////////////////////////////////////////////////////
    function merumagaMail($subject , $user, $person_datas, $unit_url)
    {
        $name = $person_datas["name"];
        $message = <<< EOF
{unwrap}
マイページよりメールマガジン停止の要望を受信しました。
メールマガジン配信停止の処理をお願いします。

[氏名]
$name

$unit_url
{/unwrap}
EOF;
        $this->merumagaFinishMail($subject, $message);

        //サンクスメール
        if(!empty($person_datas["mail1"])){
            $subject = "メールマガジンの停止を受け付けました";
            $this->merumagaThanksMail($subject, $person_datas["mail1"], $person_datas["name"]);
        }
    }

    //////////////////////////////////////////////////////////
    //活動停止メール
    //////////////////////////////////////////////////////////
    function suspensionMail($subject , $user, $person_datas, $unit_url, $jiki, $reason)
    {
        $name = $person_datas["name"];
        $message = <<< EOF
{unwrap}
マイページより活動中止を承りました。
活動中止の処理をお願いします。

[氏名]
$name

[理由]
$reason

[活動再開時期]
$jiki

$unit_url
{/unwrap}
EOF;
        $this->suspensionFinishMail($subject, $message);

        //サンクスメール
        if(!empty($person_datas["mail1"])){
            $subject = "活動中止を受け付けました";
            $this->suspensionThanksMail($subject, $person_datas["mail1"], $person_datas["name"]);
        }
    }

    //////////////////////////////////////////////////////////
    //退会メール
    //////////////////////////////////////////////////////////
    function cancelMail($subject , $user, $person_datas, $unit_url, $reason, $comment)
    {
        $name = $person_datas["name"];
        $message = <<< EOF
{unwrap}
マイページより退会希望を承りました。
退会の処理をお願いします。

[氏名]
$name

[理由]
$reason
$comment

$unit_url
{/unwrap}
EOF;
        $this->cancelFinishMail($subject, $message);

        //サンクスメール
        if(!empty($person_datas["mail1"])){
            $subject = "退会を受け付けました";
            $this->cancelThanksMail($subject, $person_datas["mail1"], $person_datas["name"]);
        }
    }

    ////////////////////////////////////////////////////////////////
    // user agent
    ////////////////////////////////////////////////////////////////
    function getUserAgent()
    {
        $this->load->library('user_agent');
        $agent="";
        if ($this->agent->is_mobile()) {
            $agent = "SP";
        }else{
            $agent = "PC";
        }
        return $agent;
    }

    ////////////////////////////////////////////////////////////////
    // メールアドレス暗号化初期値
    ////////////////////////////////////////////////////////////////
    function mail_crypt_iv($email)
    {
        //POSTされた値
        $TargetStr = $email;

        //暗号化・復元用のIVキーを作成
        //指定した暗号モードの組み合わせに属する IV の大きさ
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);

        //暗号モードに対するIVのサイズに合わせたキーを生成
        $iv = random_bytes($iv_size);

        return $iv;
    }
    ////////////////////////////////////////////////////////////////
    // メールアドレス暗号化
    ////////////////////////////////////////////////////////////////
    function mail_crypt($email, $iv)
    {
        //POSTされた値
        $TargetStr = $email;

        //暗号化・復元用のソルト
        $passphrase = mailSalt;

        //暗号化を実施
        $CryptTarget = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $passphrase, $TargetStr, MCRYPT_MODE_CBC, $iv);

        return $CryptTarget;

    }


    ////////////////////////////////////////////////////////////////
    // メールアドレス復元
    ////////////////////////////////////////////////////////////////
    function mail_decrypt($base64_Iv,$base64_CryptTarget )
    {
        //復元したいデータとそのIVキーをデータベースから引き出してBase64でデコード
        $iv = base64_decode($base64_Iv );
        $CryptTarget= base64_decode($base64_CryptTarget );

        //暗号化・復元用のソルトを指定
        $passphrase = mailSalt;

        //復元
        $DecryptTarget_ = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $passphrase, $CryptTarget, MCRYPT_MODE_CBC, $iv);

        return $DecryptTarget_;
    }

    //////////////////////////////////////////////////////////
    //パスワード再発行用のトークン
    //////////////////////////////////////////////////////////
    function generatePassword ()
    {
        $len = 24;
        srand ( (double) microtime () * 1000000);
        $seed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        $pass = "";
        while ($len--) {
            $pos = rand(0,61);
            $pass .= $seed[$pos];
        }
        //CPIのDB重複チェック
        $ret = $this->checkForgetPassword($pass);
        if(!$ret) {
            return $pass;
        }else{
            $this->generatePassword();
        }
    }

    //////////////////////////////////////////////////////////
    //CPIのDB重複チェック
    //////////////////////////////////////////////////////////
    function checkForgetPassword($pass)
    {
        $ret = $this->db->get_where("members", array('forget_password' => $pass, 'del' => 0))->row();
        return $ret;
    }

    ////////////////////////////////////////////////////////////////
    // csrf　発行
    ////////////////////////////////////////////////////////////////
    function _get_csrf_nonce()
    {
        $this->load->helper('string');
        $key   = random_string('alnum', 8);
        $value = random_string('alnum', 20);
        $this->session->set_flashdata('csrfkey', $key);
        $this->session->set_flashdata('csrfvalue', $value);

        return array($key => $value);
    }

    ////////////////////////////////////////////////////////////////
    // csrf チェック
    ////////////////////////////////////////////////////////////////
    function _valid_csrf_nonce()
    {
        if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
            $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    ////////////////////////////////////////////////////////////////
    // sessionにID保持
    ////////////////////////////////////////////////////////////////
    function setLoginId($id)
    {
        $this->load->helper(array('cookie'));
        //IDが未設定の場合はcookieにキーが保存されているかチェック
        if(empty($id)){
            $login_cookie="";
            if(!empty($_COOKIE["loginkey"])) {
                $login_cookie = $_COOKIE["loginkey"];
            }
            if($login_cookie){
                $return = $this->db->get_where("members", array('loginkey' => $login_cookie, 'del' => 0))->row();
                if($return->id){
                    $id = $return->id;
                }else{
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
                    //redirect("user/home");
                }
            }
        }

        if(empty($id)){
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            //redirect($this->segment."/login");
        }

        $sessionData = array(
            'login_id' => $id
        );
        $this->session->set_userdata($sessionData);
    }

    ////////////////////////////////////////////////////////////////
    // エラーメール送信
    ////////////////////////////////////////////////////////////////
    function errMail($subject, $message)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle);
        $this->email->to(kanriMail);
//        $this->email->to('mypage@hurex.co.jp.test-google-a.com');
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }
    }

    ////////////////////////////////////////////////////////////////
    // エラーメール送信
    ////////////////////////////////////////////////////////////////
    function errHurexMail($subject, $message)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
//        $this->email->to(hurexMailTo, addMailTo);
        $this->email->to(
            array(hurexMailTo, addMailTo)
        );
//        $this->email->to('mypage@hurex.co.jp.test-google-a.com');
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }
    }

    ////////////////////////////////////////////////////////////////
    // メール送信
    ////////////////////////////////////////////////////////////////
    function successMail($subject, $message)
    {
        $to = hurexMailTo . "," . addMailTo;
        $title = $subject;
        $content  = $message;
        $from = "From: " . hurexMailFrom . "\r\n";
        $from .= "Return-Path: ". returnPath;
        $send_mail = mb_send_mail($to, $title, $content, $from);
        if (!$send_mail) {
            $this->errMail($subject, $message);
        }
        /*
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
//        $this->email->to(hurexMailTo, addMailTo);
        $this->email->to(
            array(hurexMailTo, addMailTo)
        );
        $this->email->subject($subject);

        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }
        */
    }

    ////////////////////////////////////////////////////////////////
    // メール送信 オファーの申し込み、辞退の場合はジョブ担当へ
    ////////////////////////////////////////////////////////////////
    function successMailProcess($subject, $message, $to)
    {
        $to = hurexMailTo . "," . addMailTo;
        $title = $subject;
        $content  = $message;
        $from = "From: " . hurexMailFrom . "\r\n";
        $from .= "Return-Path: ". returnPath;
        $send_mail = mb_send_mail($to, $title, $content, $from);
        if (!$send_mail) {
            $this->errMail($subject, $message);
        }
        /*
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
//        $this->email->to($to, addMailTo);
        $this->email->to(
            array(hurexMailTo, addMailTo)
        );
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }
        */
    }

    ////////////////////////////////////////////////////////////////
    // signupメール送信
    ////////////////////////////////////////////////////////////////
    function signupThanksMail($subject, $message, $to)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }

    }

    ////////////////////////////////////////////////////////////////
    // マイページ登録サンクスメール
    ////////////////////////////////////////////////////////////////
    function entryThanksMail($subject, $message, $mailTo)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
        $this->email->to($mailTo);
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }

    }

    ////////////////////////////////////////////////////////////////
    // マイページ登録orderメール
    ////////////////////////////////////////////////////////////////
    function entryOrderMail($subject, $message)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
//        $this->email->to(hurexMailTo, addMailTo);
        $this->email->to(
            array(hurexMailTo, addMailTo)
        );
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }

    }

    ////////////////////////////////////////////////////////////////
    // メルマガ停止 orderメール
    ////////////////////////////////////////////////////////////////
    function merumagaFinishMail($subject, $message)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
//        $this->email->to(hurexMailTo, addMailTo);
        $this->email->to(
            array(hurexMailTo, addMailTo)
        );
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }
    }

    //////////////////////////////////////////////////////////
    //メルマガサンクス（使うかわからない）
    //////////////////////////////////////////////////////////
    function merumagaThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

お世話になっております。
ヒューレックス株式会社でございます。

この度は、メールマガジンを停止されるとのこと、承知いたしました。

システムの更新までにタイムラグが生じる都合上、
本メールをお送りさせていただいた後、上記手続き完了までに、
弊社よりご案内をお送りしてしまう可能性がございますが、
何卒ご容赦くださいませ。

※ 配信再開をご希望の場合、 hrx@hurex.co.jp までご連絡ください。

今後もヒューレックスエージェントサービスをご利用頂きますよう、
よろしくお願いいたします。

＜＜ 個人情報の取り扱いに関して ＞＞
ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱いおよび管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　E-mail　hrx@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■事業承継推進機構株式会社（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。

{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    ////////////////////////////////////////////////////////////////
    // 活動中止 orderメール
    ////////////////////////////////////////////////////////////////
    function suspensionFinishMail($subject, $message)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
//        $this->email->to(hurexMailTo, addMailTo);
        $this->email->to(
            array(hurexMailTo, addMailTo)
        );
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }
    }

    //////////////////////////////////////////////////////////
    //活動中止サンクス（使うかわからない）
    //////////////////////////////////////////////////////////
    function suspensionThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

お世話になっております。
ヒューレックス株式会社でございます。

この度は転職活動を中止されるとのこと承知いたしました。

システムの更新までにタイムラグが生じる都合上、
本メールをお送りさせていただいた後、上記手続き完了までに、
弊社よりご案内をお送りしてしまう可能性がございますが、
何卒ご容赦くださいませ。

※ 配信再開をご希望の場合、hrx@hurex.co.jp までご連絡ください。

今後もヒューレックスエージェントサービスをご利用頂きますよう、
よろしくお願いいたします。

＜＜ 個人情報の取り扱いに関して ＞＞
ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱いおよび管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　E-mail　hrx@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■事業承継推進機構株式会社（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。

{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    ////////////////////////////////////////////////////////////////
    // 退会 orderメール
    ////////////////////////////////////////////////////////////////
    function cancelFinishMail($subject, $message)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
//        $this->email->to(hurexMailTo, addMailTo);
        $this->email->to(
            array(hurexMailTo, addMailTo)
        );
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }
    }

    //////////////////////////////////////////////////////////
    //退会サンクス（使うかわからない）
    //////////////////////////////////////////////////////////
    function cancelThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

いつも大変お世話になっております。
ヒューレックス株式会社です。

この度はご連絡をいただき
誠にありがとうございました。

また、これまで弊社のサービスをご利用いただきまして
ありがとうございました。

こちらの連絡をもちまして、
弊社サービスの終了と退会手続きをとらせていただきます。

今後の $shimei 様の
益々のご活躍を心よりお祈り申し上げます。

今後もお困りごとがございましたら、
少しでもお力になれますよう、精一杯努めさせていただきますので
些細なことでも是非ご相談ください。

それでは、今後とも何卒よろしくお願い申し上げます。

ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
ヒューレックス株式会社
　tel. 0120-14-1150　　fax. 022-723-1738
　email. hrx@hurex.co.jp
ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
【Group web site】
　■ ヒューレックス　http://www.hurex.jp/
　■ マリッジパートナーズ　http://www.mpartners.jp/
　■ 事業承継推進機構（AOBA）　http://aoba-east.jp/
ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー


{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    //////////////////////////////////////////////////////////
    //HRBC基本情報
    //////////////////////////////////////////////////////////
    public function getHrbcMaster(){
        $dir = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/";
        include($dir . "system_entry/mimes.php");

        $error="";
        //ファイルが無ければ再作成
        if(!file_exists($dir . 'token/cache.json')) {
            $this->setHrbcBasicData();
            //require($dir . 'conf/auth.php');
        }

        //ファイル時間20分ごとのcronでミスがあったとき 念のため19分
        $ftime = filemtime($dir . 'token/cache.json');
        $twminbf = strtotime( "-19 min" );
        if($ftime < $twminbf){
            $this->setHrbcBasicData();
            //require($dir . 'conf/auth.php');
        }

        $cachefile = file_get_contents($dir . 'token/cache.json', true);
        $tmp_arr = json_decode($cachefile,true);

        $tmp_token = $tmp_arr['token'];
        $tmp_partition = $tmp_arr['partition'];
        if(empty($tmp_token) || empty($tmp_partition)){
            $this->setHrbcBasicData();
            //require($dir . 'conf/auth.php');
        }

        $datas=array();
        //ファイル読込
        if($json = @file_get_contents($dir . 'token/cache.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("err/error_hrbc");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("err/error_hrbc");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("err/error_hrbc");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました１。Err:timeout"));
                redirect("err/error_hrbc");
            }
        }
        //$json = file_get_contents($dir . 'token/cache.json', true);
        $arr = json_decode($json,true);
        $token = $arr['token'];
        $partition = $arr['partition'];
        $datas["token"] = $token;
        $datas["partition"] = $partition;

        //HRBCマスターと連動
        if($tmppref = @file_get_contents($dir. 'pref.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました２。Err:timeout"));
                redirect("/user/home");
            }
        }
        //$tmppref = file_get_contents($dir. 'pref.json', true);
        $tmppref = json_decode($tmppref,true);
        $datas["pref"] = $tmppref;

        if($tmpgender = @file_get_contents($dir. 'gender.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました３。Err:timeout"));
                redirect("user/home");
            }
        }
        //$tmpgender = file_get_contents($dir . 'gender.json', true);
        $tmpgender = json_decode($tmpgender,true);
        $datas["gender"] = $tmpgender;

        if($tmpbackground = @file_get_contents($dir. 'background.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました４。Err:timeout"));
                redirect("user/home");
            }
        }
        //$tmpbackground = file_get_contents($dir . 'background.json', true);
        $tmpbackground = json_decode($tmpbackground,true);
        $datas["background"] = $tmpbackground;

        if($tmpwork = @file_get_contents($dir. 'work.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました５。Err:timeout"));
                redirect("user/home");
            }
        }
        //$tmpwork = file_get_contents($dir . 'work.json', true);
        $tmpwork = json_decode($tmpwork,true);
        $datas["work"] = $tmpwork;

        if($tmparea = @file_get_contents($dir. 'area.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました６。Err:timeout"));
                redirect("user/home");
            }
        }
        //$tmparea = file_get_contents($dir. 'area.json', true);
        $tmparea = json_decode($tmparea,true);
        $datas["area"] = $tmparea;

        if($tmptenkin = @file_get_contents($dir. 'tenkin.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました７。Err:timeout"));
                redirect("user/home");
            }
        }
        //$tmptenkin = file_get_contents($dir. 'tenkin.json', true);
        $tmptenkin = json_decode($tmptenkin,true);
        $datas["tenkin"] = $tmptenkin;

        if($tmpcategory = @file_get_contents($dir. 'jobcategory.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました８。Err:timeout"));
                redirect("user/home");
            }
        }
        //$tmpcategory = file_get_contents($dir. 'jobcategory.json', true);
        $tmpcategory = json_decode($tmpcategory,true);
        $datas["jobcategory"] = $tmpcategory;


        if($tmpincome = @file_get_contents($dir. 'income.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました９。Err:timeout"));
                redirect("user/home");
            }
        }
        $tmpincome = json_decode($tmpincome,true);
        $datas["income"] = $tmpincome;

        if($tmptantoujob = @file_get_contents($dir. 'jobmypagecd.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました１０。Err:timeout"));
                redirect("user/home");
            }
        }
        $tmptantoujob = json_decode($tmptantoujob,true);
        $datas["jobmypagecd"] = $tmptantoujob;

        //担当職種テスト用（本番では使わない）
        if($tmptantoujob_test = @file_get_contents($dir. 'jobmypagecd_test.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました１０。Err:timeout"));
                redirect("user/home");
            }
        }
        $tmptantoujob_test = json_decode($tmptantoujob_test,true);
        $datas["jobmypagecd_test"] = $tmptantoujob_test;

        return $datas;
    }

    //////////////////////////////////////////////////////////
    //HRBC基本情報設定
    //////////////////////////////////////////////////////////
    function setHrbcBasicData()
    {
        // リクエストを行うURLの指定
        $url = "https://api-hrbc-jp.porterscloud.com/v1/oauth";
        $url = $url .  "?app_id=725d6f07ddaede98231a01f7783a78ef&response_type=code_direct";
        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8'),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);
        $json = json_encode($xml);
        $jsonD = json_decode($json,true);
        $code = $jsonD["Code"];

        //送信パラメータ
        /*
        $param = array('app_id'=>'725d6f07ddaede98231a01f7783a78ef','response_type'=>'code_direct');

        $data = http_build_query($param);

        $unit_url = $url . "?" . $data;

        $opts = array(
            'http'=>array(
                'method' => 'GET'
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($unit_url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404"));
                        redirect("err/error_hrbc");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("err/error_hrbc");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("err/error_hrbc");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました１１。Err:timeout"));
                redirect("err/error_hrbc");
            }
        }
        //$xml = file_get_contents($unit_url, false, $context);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $jsonD = json_decode($json,true);
        $code = $jsonD["Code"];
        */

        //////////////////////////////////////////////////////////////////////
        //// アクセストークン
        ////////////////////////////////////////////////////////////////////////

        $url = "https://api-hrbc-jp.porterscloud.com/v1/token";
        $url = $url .  "?code=".$code."&grant_type=oauth_code&app_id=725d6f07ddaede98231a01f7783a78ef&secret=9ea5f52d29da05f55f9fee47cd0f55b9bba28ebe9c64d8d3a51bdc7a0d11713d";
        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8'),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

//$xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $jsonD = json_decode($json,true);
        $token = $jsonD['AccessToken'];
        /*
        //送信パラメータ
        $param = array('code'=>$code, 'grant_type'=>'oauth_code','app_id'=>'725d6f07ddaede98231a01f7783a78ef','secret'=>'9ea5f52d29da05f55f9fee47cd0f55b9bba28ebe9c64d8d3a51bdc7a0d11713d');

        $data = http_build_query($param);

        $unit_url = $url . "?" . $data;

        $opts = array(
            'http'=>array(
                'method' => 'GET',
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($unit_url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404"));
                        redirect("err/error_hrbc");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("err/error_hrbc");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("err/error_hrbc");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout"));
                redirect("err/error_hrbc");
            }
        }
        //$xml = file_get_contents($unit_url, false, $context);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $jsonD = json_decode($json,true);
        $token = $jsonD['AccessToken'];
        */

        //////////////////////////////////////////////////////////////////////
        //// パーティション
        ////////////////////////////////////////////////////////////////////////

        //$url = "https://api-hrbc-jp.porterscloud.com/v1/partition";
        $url = "https://api-hrbc-jp.porterscloud.com/v1/partition?request_type=1";
        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

        $json = json_encode($xml);
        $jsonD = json_decode($json,true);
        $partitionId = $jsonD['Item']['Partition.P_Id'];
        /*
        //送信パラメータ
        $param = array('request_type'=>1);

        $data = http_build_query($param);

        $unit_url = $url . "?" . $data;

        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: $token\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        //xml解析
        if($xml = @file_get_contents($unit_url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404"));
                        redirect("err/error_hrbc");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("err/error_hrbc");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("err/error_hrbc");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout"));
                redirect("err/error_hrbc");
            }
        }
        //$xml = file_get_contents($unit_url, false, $context);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $jsonD = json_decode($json,true);
        $partitionId = $jsonD['Item']['Partition.P_Id'];
        */

        //////////////////////////////////////////////////////////////////////
        //// cache化
        ////////////////////////////////////////////////////////////////////////
        $arrs = array(
            "token" => $token,
            "partition" => $partitionId
        );

        $json = json_encode($arrs);


        $cachFile = BaseDir . "/../hrbc/token/cache.json";
        /*
        if(file_exists($cachFile)){
            unlink($cachFile);
        }
        */
        $fp = fopen($cachFile, "w" );
        fputs($fp, $json);
        fclose( $fp );
    }

    ////////////////////////////////////////////////////////////////
    // ファイルアップロード
    ////////////////////////////////////////////////////////////////
    function fileUpload($image, $cnt, $files, $dir, $require , $filename, $overwrite, $allow)
    {
        $result=array();
        $upDir = UploadDirectory . $dir;
        //ファイルアップロード
        if ( $files['userfile']['size'][$cnt] != 0){
            if (!file_exists($upDir)) {
                if ( !mkdir( $upDir ) ) {
                    echo "ディレクトリ作成の権限がありません。<br>管理者に問い合わせてください。";
                    exit;
                }
            }

            $_FILES['userfile']['name']= $files['userfile']['name'][$cnt];
            $_FILES['userfile']['type']= $files['userfile']['type'][$cnt];
            $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$cnt];
            $_FILES['userfile']['error']= $files['userfile']['error'][$cnt];
            $_FILES['userfile']['size']= $files['userfile']['size'][$cnt];

            $fn = $_FILES["userfile"]['name'];
            $ext = substr($fn, strrpos($fn, '.') + 1);
            if(empty($filename)){
                $filename = date('Ymdhis') . "_" . mt_rand() . "." .$ext;
            }
            $config['upload_path'] = $upDir;
            $config['allowed_types'] = $allow;
            $config['max_size']	= '10000000';
            $config['file_name']	= $filename;
            $config['overwrite'] = $overwrite;

            $this->upload->initialize($config);

            if ( ! $this->upload->do_upload())
            {
                $result = array('error' => $this->upload->display_errors());
            }
            else
            {
                $result = array('upload_data' => $this->upload->data());
            }
            //ファイル変更なし
        }else if(!empty($image)){
            $result[] = $upDir . $image;
        }else{
            //$result[0] = "";
            if($require == 1){
                // エラー処理
                $result['error'] = "ファイルを選択して下さい。";
            }else{
                $result="";
            }
        }

        return $result;
    }

    //////////////////////////////////////////////////////////
    //ソート
    //////////////////////////////////////////////////////////
    function sortChange($id, $category_id, $sort, $segment, $database)
    {
        if(empty($id)){
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。エラーコード９"));
            redirect($segment . "/home/".$category_id);
        }
        if(empty($category_id)){
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。エラーコード１０"));
            redirect($segment ."/home/".$category_id);
        }
        if(empty($sort)){
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。エラーコード１２"));
            redirect($segment . "/home/".$category_id);
        }

        //現在のソート番号取得
        $data = $this->db->get_where($database, array('id' => $id, 'del'=>0))->row();
        $nowSort = $data->sort;

        //ソートアップかダウンから対象データ取得
        if($sort=="down"){
            $data2 = $this->db->order_by('sort asc')->get_where($database, array("del"=>0, "category_id"=>$category_id, "sort >" =>  $nowSort));

            if(empty($data2->num_rows())){
                $this->session->set_flashdata(array("msg"=>"対象のデータが存在しません。エラーコード１３"));
                redirect($segment . "/home/".$category_id);
            }

            //データの入れ替え（前後を逆にする）
            $update_param1 = array('sort' => $data2->result('array')[0]['sort']);
            $this->db->where('id', $id);
            $this->db->update($database, $update_param1);

            $update_param2 = array('sort' => $nowSort);
            $this->db->where('id', $data2->result('array')[0]['id']);
            $this->db->update($database, $update_param2);

        }else if($sort=="up"){
            $down = $nowSort - 1;
            $data2 = $this->db->order_by('sort desc')->get_where($database, array("del"=>0, "category_id"=>$category_id, "sort <" => $nowSort));

            if(empty($data2->num_rows())){
                $this->session->set_flashdata(array("msg"=>"対象のデータが存在しません。エラーコード１４"));
                redirect($segment . "/home/".$category_id);
            }

            //データの入れ替え（前後を逆にする）
            $update_param1 = array('sort' => $data2->result('array')[0]['sort']);
            $this->db->where('id', $id);
            $this->db->update($database, $update_param1);

            $update_param2 = array('sort' => $nowSort);
            $this->db->where('id', $data2->result('array')[0]['id']);
            $this->db->update($database, $update_param2);
        }

        $this->session->set_flashdata(array("msg"=>"並び替えが完了しました。"));
        redirect($segment."/home/".$category_id);
    }

    //////////////////////////////////////////////////////////
    //カテゴリーソート
    //////////////////////////////////////////////////////////
    function category_sortChange($id, $sort, $segment, $database)
    {
        if(empty($id)){
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。エラーコード９"));
            redirect($segment . "/home/");
        }
        if(empty($sort)){
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。エラーコード１２"));
            redirect($segment . "/home/");
        }

        //現在のソート番号取得
        $data = $this->db->get_where($database, array('category_id' => $id, 'category_del'=>0))->row();
        $nowSort = $data->category_sort;

        //ソートアップかダウンから対象データ取得
        if($sort=="down"){
            $data2 = $this->db->order_by('category_sort asc')->get_where($database, array("category_del"=>0, "category_sort >" =>  $nowSort));

            if(empty($data2->num_rows())){
                $this->session->set_flashdata(array("msg"=>"対象のデータが存在しません。エラーコード１３"));
                redirect($segment . "/home/");
            }

            //データの入れ替え（前後を逆にする）
            $update_param1 = array('category_sort' => $data2->result('array')[0]['category_sort']);
            $this->db->where('category_id', $id);
            $this->db->update($database, $update_param1);

            $update_param2 = array('category_sort' => $nowSort);
            $this->db->where('category_id', $data2->result('array')[0]['category_id']);
            $this->db->update($database, $update_param2);

        }else if($sort=="up"){
            $down = $nowSort - 1;
            $data2 = $this->db->order_by('category_sort desc')->get_where($database, array("category_del"=>0,  "category_sort <" => $nowSort));

            if(empty($data2->num_rows())){
                $this->session->set_flashdata(array("msg"=>"対象のデータが存在しません。エラーコード１４"));
                redirect($segment . "/home/");
            }

            //データの入れ替え（前後を逆にする）
            $update_param1 = array('category_sort' => $data2->result('array')[0]['category_sort']);
            $this->db->where('category_id', $id);
            $this->db->update($database, $update_param1);

            $update_param2 = array('category_sort' => $nowSort);
            $this->db->where('category_id', $data2->result('array')[0]['category_id']);
            $this->db->update($database, $update_param2);
        }

        $this->session->set_flashdata(array("msg"=>"並び替えが完了しました。"));
        redirect($segment."/home/");
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    //デバイス判定　PC SP
    ////////////////////////////////////////////////////////////////////////////////////////////
    function getDevice(){
        $ua = $_SERVER['HTTP_USER_AGENT'];// ユーザエージェントを取得
        if((strpos($ua, 'iPhone') !== false) || (strpos($ua, 'iPod') !== false) ||(strpos($ua, 'Android') !== false)){
            $agent = "SP";
        }else{
            $agent = "PC";
        }
        return $agent;
    }

    /////////////////////////////////////////////////////////////////////////
    // job検索(サイトのサイドバーに表示用）
    /////////////////////////////////////////////////////////////////////////
    function jobSearch($job, $pref, $tenkin, $income, $p_job_id, $taishoku, $limit)
    {
        ////////////////////////////////////////////////////////////
        // パラメータ設定
        //////////////////////////////////////////////////////////////
        //業種
        $bizSql = " left join clients as c on j.client_id = c.c_id ";
        $bizSql2 = ", group_concat(distinct c.c_industry_name separator '|') as i_name, c.c_title, c.c_id";

        //職種
        $tmpjob="";

        //$jobSearch;
        $jobcnt=0;
        $jobSearch="";
        $jcParam="";
        $jccnt=0;
        $jobs=array();
        $tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
        $tmpjob = json_decode($tmpjob,true);
        if(!empty($job)){
            if(!empty($tmpjob['Item'])){
                foreach($tmpjob['Item'] as $k=>$v){
                    if(!empty($v['Item'])){
                        foreach($v['Item'] as $k2=>$v2){
                            foreach($job as $k3 => $v3){
                                if($v2["Option.P_Id"]==$v3){
                                    if(!empty($v2["Items"]["Item"])){
                                        foreach($v2["Items"]["Item"] as $k4 => $v4){
                                            $jobs[] = $v4["Option.P_Id"];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(!empty($jobs)){
                foreach($jobs as $k=>$v){
                    if(empty($jobSearch)) {
                        $jobSearch.=" and ( ";
                    }else{
                        $jobSearch.=" or ";
                    }
                    $val = $this->db->escape($v);
                    $jobSearch .= " FIND_IN_SET(" . $val . ",j.jobcategory_id)";
                }
            }
        }
        if($jobSearch) $jobSearch .= " ) ";

        //勤務地
        $tmppref=array();
        $prefSearch="";
        $prefParam="";
        $prefcnt=0;
        if(!empty($pref)){
            foreach($pref as $k=>$v){
                if(empty($prefSearch)) {
                    $prefSearch.=" and ( ";
                }else{
                    $prefSearch.=" or ";
                }
                $val = $this->db->escape($v);
                $prefSearch .= " FIND_IN_SET(" . $val . ",j.prefecture_id)";
            }
        }
        if($prefSearch) $prefSearch .= " ) ";

        //転勤
        $tenkinSearch="";
        if(!empty($tenkin)){
            $val = $this->db->escape($tenkin);
            $tenkinSearch = " and j.tenkin_id = " . $val . " ";
        }

        //給与
        $tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
        $tmpincome = json_decode($tmpincome,true);
        if(!empty($income)){
            if (!empty($tmpincome['Item'])) {
                foreach ($tmpincome['Item'] as $k => $v) {
                    if (!empty($v['Item'])) {
                        foreach ($v['Item'] as $k2 => $v2) {
                            if($income == $v2["Option.P_Id"]){
                                $income = $v2["Option.P_Name"];
                            }
                        }
                    }
                }
            }
        }

        $incomeSearch="";
        if(!empty($income)){
            $min="";
            $max="";
            $income = str_replace("万円", "", $income);
            $income = str_replace("以上", "", $income);
            $income = str_replace("以下", "", $income);
            $split_income = explode("～",$income);
            if($split_income[0]==200 && empty($split_income[1])){
                $min = 0;
                $max = $split_income[0];
            }else if($split_income[0]==1001 && empty($split_income[1])){
                $min = $split_income[0];
                $max = 99999999999999999;
            }else{
                $min = $split_income[0];
                $max = $split_income[1];
            }
            $min = $this->db->escape($min);
            $max = $this->db->escape($max);
            $incomeSearch = " and j.maxsalary >= " . $min . " and j.minsalary <= " . $max . " ";
        }

        $process_param="";

        $tmp_p_job_id = "";
        if(!empty($p_job_id)){
            foreach ($p_job_id as $k=>$v){
                $tmp_p_job_id.=$v . ",";
            }
            $tmp_p_job_id = rtrim($tmp_p_job_id,",");
        }
        if($tmp_p_job_id){
            $process_param = " and (j.publish = " .HrbcPublish. " or (j.publish = ". HrbcCloseJob . " and j.job_id in (" . $tmp_p_job_id . "))) ";
        }else{
            $process_param = " and j.publish = " .HrbcPublish;
        }

        //退職を外す
        $taishoku_param="";
        if($taishoku){
            $taishoku_param = " and j.job_id not in (" . $taishoku . ") ";
        }

        ////////////////////////////////////////////////////////////////////
        //// db抽出
        //////////////////////////////////////////////////////////////////////
        $datas = array();
        $sql = "SELECT j.job_id, j.job_title, j.client_id, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.j_updated, j.prefecture_name, j.jobcategory_name, j.employ_name ,j.tenkin_name" . $bizSql2 . " FROM jobs as j" . $bizSql . " where j.phaze = " . HrbcPhaze . $process_param .$taishoku_param . $jobSearch . $prefSearch . $tenkinSearch . $incomeSearch . " group by j.job_id order by j.j_updated desc limit 0, $limit";
        $datas = $this->db->query($sql);

//        echo $this->db->last_query();
        return $datas;
    }

    /////////////////////////////////////////////////////////////////////////
    // job検索件数用
    /////////////////////////////////////////////////////////////////////////
    function jobCount($job, $pref, $tenkin, $income, $p_job_id, $taishoku)
    {
        ////////////////////////////////////////////////////////////
        // パラメータ設定
        //////////////////////////////////////////////////////////////
        //業種
        $bizSql = " left join clients as c on j.client_id = c.c_id ";
        $bizSql2 = ", group_concat(distinct c.c_industry_name separator '|') as i_name, c.c_title, c.c_id";

        //職種
        $tmpjob="";

        //$jobSearch;
        $jobcnt=0;
        $jobSearch="";
        $jcParam="";
        $jccnt=0;
        $jobs=array();
        $tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
        $tmpjob = json_decode($tmpjob,true);
        if(!empty($job)){
            if(!empty($tmpjob['Item'])){
                foreach($tmpjob['Item'] as $k=>$v){
                    if(!empty($v['Item'])){
                        foreach($v['Item'] as $k2=>$v2){
                            foreach($job as $k3 => $v3){
                                if($v2["Option.P_Id"]==$v3){
                                    if(!empty($v2["Items"]["Item"])){
                                        foreach($v2["Items"]["Item"] as $k4 => $v4){
                                            $jobs[] = $v4["Option.P_Id"];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(!empty($jobs)){
                foreach($jobs as $k=>$v){
                    if(empty($jobSearch)) {
                        $jobSearch.=" and ( ";
                    }else{
                        $jobSearch.=" or ";
                    }
                    $val = $this->db->escape($v);
                    $jobSearch .= " FIND_IN_SET(" . $val . ",j.jobcategory_id)";
                }
            }
        }
        if($jobSearch) $jobSearch .= " ) ";

        //勤務地
        $tmppref=array();
        $prefSearch="";
        $prefParam="";
        $prefcnt=0;
        if(!empty($pref)){
            foreach($pref as $k=>$v){
                if(empty($prefSearch)) {
                    $prefSearch.=" and ( ";
                }else{
                    $prefSearch.=" or ";
                }
                $val = $this->db->escape($v);
                $prefSearch .= " FIND_IN_SET(" . $val . ",j.prefecture_id)";
            }
        }
        if($prefSearch) $prefSearch .= " ) ";

        //転勤
        $tenkinSearch="";
        if(!empty($tenkin)){
            $val = $this->db->escape($tenkin);
            $tenkinSearch = " and j.tenkin_id = " . $val . " ";
        }

        //給与
        $tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
        $tmpincome = json_decode($tmpincome,true);
        if(!empty($income)){
            if (!empty($tmpincome['Item'])) {
                foreach ($tmpincome['Item'] as $k => $v) {
                    if (!empty($v['Item'])) {
                        foreach ($v['Item'] as $k2 => $v2) {
                            if($income == $v2["Option.P_Id"]){
                                $income = $v2["Option.P_Name"];
                            }
                        }
                    }
                }
            }
        }

        $incomeSearch="";
        if(!empty($income)){
            $min="";
            $max="";
            $income = str_replace("万円", "", $income);
            $income = str_replace("以上", "", $income);
            $income = str_replace("以下", "", $income);
            $split_income = explode("～",$income);
            if($split_income[0]==200 && empty($split_income[1])){
                $min = 0;
                $max = $split_income[0];
            }else if($split_income[0]==1001 && empty($split_income[1])){
                $min = $split_income[0];
                $max = 99999999999999999;
            }else{
                $min = $split_income[0];
                $max = $split_income[1];
            }
            $min = $this->db->escape($min);
            $max = $this->db->escape($max);
            $incomeSearch = " and j.maxsalary >= " . $min . " and j.maxsalary <= " . $max . " ";
        }

        $process_param="";

        $tmp_p_job_id = "";
        if(!empty($p_job_id)){
            foreach ($p_job_id as $k=>$v){
                $tmp_p_job_id.=$v . ",";
            }
            $tmp_p_job_id = rtrim($tmp_p_job_id,",");
        }
        if($tmp_p_job_id){
            $process_param = " and (j.publish = " .HrbcPublish. " or (j.publish = ". HrbcCloseJob . " and j.job_id in (" . $tmp_p_job_id . "))) ";
        }else{
            $process_param = " and j.publish = " .HrbcPublish;
        }


        //退職を外す
        $taishoku_param="";
        if($taishoku){
            $taishoku_param = " and j.job_id not in (" . $taishoku . ") ";
        }

        ////////////////////////////////////////////////////////////////////
        //// db抽出
        //////////////////////////////////////////////////////////////////////
        $datas = array();
        $sql = "SELECT j.job_id, j.job_title, j.client_id, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.j_updated, j.prefecture_name, j.jobcategory_name, j.employ_name ,j.tenkin_name" . $bizSql2 . " FROM jobs as j" . $bizSql . " where j.phaze = " . HrbcPhaze . $process_param .$taishoku_param . $jobSearch . $prefSearch . $tenkinSearch . $incomeSearch . " group by j.job_id order by j.j_updated desc";
        $datas = $this->db->query($sql);
        return $datas;
    }


    ////////////////////////////////////////////////////////////////////
    //// 閲覧履歴
    //////////////////////////////////////////////////////////////////////
    function getLooks($login_id){
        $looks = array();
        $mem_id = $this->db->escape($login_id);
        $sql = "SELECT l.job_id, j.job_title FROM `looks` as l left join jobs as j on l.job_id = j.job_id WHERE l.member_id = " . $mem_id . "  order by l.updated desc limit 0, 30";
        $tmp_looks = $this->db->query($sql);
        $looks = $tmp_looks->result();

        $looks = (array)$looks;

        $tmp = [];
        $datas = [];
        foreach ($looks as $look){
            if (!in_array($look->job_id, $tmp)) {
                $tmp[] = $look->job_id;
                $datas[] = $look;
            }
        }
        $datas = array_slice($datas,0,4);

        return $datas;

    }

    ////////////////////////////////////////////////////////////////////
    //// 閲覧履歴
    //////////////////////////////////////////////////////////////////////
    function getTenshokuKiboubi($data){
        $tnsk="";
        if($data=="すぐに"){
            $tnsk=date("Y/m/d",strtotime("+30 day"));
        }else if($data=="3ヶ月後"){
            $tnsk=date("Y/m/d",strtotime("+90 day"));
        }else if($data=="半年後"){
            $tnsk=date("Y/m/d",strtotime("+180 day"));
        }else if($data=="1年後"){
            $tnsk=date("Y/m/d",strtotime("+365 day"));
        }else if($data=="未定（情報収集段階）"){
            $tnsk="情報収集段階";
        }else if($data=="未定（良いところがあれば転職したい）"){
            $tnsk="良いところがあれば";
        }
        return  $tnsk;
    }

    ////////////////////////////////////////////////////////////////
    // リサイズ
    ////////////////////////////////////////////////////////////////
    function resizeImage($source_image, $width, $height, $thumb_marker, $master_dim)
    {
        $config['source_image']	= $source_image;
        $config['create_thumb'] = TRUE;
        $config['maintain_ratio'] = TRUE;
        $config['width']	= $width;
        $config['height']	= $height;
        $config['thumb_marker']	= $thumb_marker;
        $config['master_dim']	= $master_dim;

        $this->load->library('image_lib', $config);

        $this->image_lib->initialize($config);
        $ret = $this->image_lib->resize();
        unset($config);
        $this->image_lib->clear();
    }
}
