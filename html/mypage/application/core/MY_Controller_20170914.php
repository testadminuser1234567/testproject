<?php
class MY_Controller extends CI_Controller {

	public $clean;

    //公開条件
    private $phaze = 6;
    private $publish = 11103;
    /*
    private $phaze = 11203;
    private $publish = 11104;
    */

	function __construct()
	{
        //キャッシュ対策戻るボタン
		$cache_path = unserialize(CACHE_LIMITER);
		if(!empty($cache_path)){
			foreach($cache_path as $k => $v){
				if(preg_match("/$v/",$_SERVER["REQUEST_URI"])){
                    session_cache_expire(0);
                    session_cache_limiter('private_no_expire');
				}
			}
		}

        //xss対策
        $purifierdir = $_SERVER['DOCUMENT_ROOT'] . purifierDirectory . '/htmlpurifier/library/HTMLPurifier.auto.php';
        require_once($purifierdir);
        $config = HTMLPurifier_Config::createDefault();
        $config->set('Core.Encoding', 'UTF-8');
        $config->set('HTML.Doctype', 'XHTML 1.0 Transitional');
        $config->set('Attr.AllowedFrameTargets', array('_blank','_self'));
        $config->set('HTML.Trusted', true);
        $this->clean = new HTMLPurifier($config);

		parent::__construct();

        //メンテナンスチェック
        $return = $this->db->get_where("maintenances", array('id' => 1))->row();
        $start = $return->start;
        $end = $return->end;
        $today = date('Y/m/d H:i:s');
        if (strtotime(date('Y-m-d H:i:s')) >= strtotime($start) && strtotime(date('Y-m-d H:i:s')) <= strtotime($end)){
            $controller = $this->uri->segment(1);
            if($controller != "maintenance"){
                redirect("maintenance/home");
            }
        }
    }

	////////////////////////////////////////////////////////////////
	// ログインチェック
	////////////////////////////////////////////////////////////////
	function _login_check(){

        //hurexからのジョブオファーがあってログイン前の場合の対策
        $param = "";
        $controller = $this->uri->segment(1);
        $action = $this->uri->segment(2);
        $job = $this->uri->segment(3);
        if($controller=="search" && $action == "detail"){
            if($job){
                $param = "?j=" . htmlspecialchars($job, ENT_QUOTES,'UTF-8');
            }
        }


        $login_id = $this->session->userdata('login_id');
        $login_name = $this->session->userdata('login_name');

        //IDが未設定の場合はcookieにキーが保存されているかチェック
        $last_login = 0; //ログイン履歴に残す（１の場合）
        if(empty($login_id)){
            $login_cookie="";
            if(!empty($_COOKIE["loginkey"])) {
                $login_cookie = $_COOKIE["loginkey"];
            }
            if($login_cookie){
                $return = $this->db->get_where("members", array('loginkey' => $login_cookie, 'del' => 0))->row();

                if(!empty($return) && !empty($return->id)){
                    $login_id = $return->id;
                    //ログイン履歴に残すパターン
                    //$last_login = 1;
                }else{
                    $this->session->sess_destroy();
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
                    redirect("user/login".$param);
                }
            }else{

            }
        }

        if(empty($login_id)){
            $this->session->sess_destroy();
            //$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect("user/login".$param);
        }

        //名前が空の場合はHRBCから取得
        if(empty($login_name)){
            $user = $this->db->get_where("members", array('id' => $login_id, 'del'=>0))->row();
            $resume_id = $user->hrbc_resume_id;
            $person_id = $user->hrbc_person_id;

            //HRBC
            $result = $this->getHrbcMaster();

            ///////////////////////////////////////////////////
            //person取得
            ///////////////////////////////////////////////////
            $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Id:eq=' . $person_id));
            $url = "https://api-hrbc-jp.porterscloud.com/v1/candidate?" . $url;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);

            $xml = file_get_contents($url, false, $context);
            $xml = simplexml_load_string($xml);
            if($xml->Code!=0){
                $pErr=$xml->Code;

                $subject = "【ユーザ名取得に失敗　code:user01】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                $message = "ユーザ名取得に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id . " error:" . $pErr;
                $this->errMail($subject, $message);

                print("エラーが発生しました。<br />コード：" . $pErr);
                exit;
            }

            //データ設定
            $json = json_encode($xml);
            $person = json_decode($json,true);
            $login_name = $person["Item"]["Person.P_Name"];

            $sessionData = array(
                'login_name' => $login_name
            );
            $this->session->set_userdata($sessionData);
        }

        if(empty($login_name)){
            $this->session->sess_destroy();
            //$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect("user/login".$param);
        }

        $sessionData = array(
            'login_id' => $login_id
        );
        $this->session->set_userdata($sessionData);

        $login_id = $this->session->userdata('login_id');
        $login_name = $this->session->userdata('login_name');

        /////////////////////////////////////////////////////////////////////////////////////////
        //ログイン履歴に残す
        /////////////////////////////////////////////////////////////////////////////////////////
        if($last_login==1){
            $this->_login_check();
            $login_id = $this->session->userdata('login_id');

            ////////////////////////////////////////////////////////////////////////////////////////////
            //HRBCデータ取得
            ////////////////////////////////////////////////////////////////////////////////////////////
            $result = $this->getHrbcMaster();

            $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
            $resume_id = $user->hrbc_resume_id;
            $person_id = $user->hrbc_person_id;

            ////////////////////////////////////////////////////////////////////////////////////////////
            //Resume のログイン履歴登録
            ////////////////////////////////////////////////////////////////////////////////////////////
            $param = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Id:eq='.$resume_id, 'field'=>'Resume.P_Owner,Resume.U_CFD727A34F74C45C5786B706A1F90F', 'order'=>'Resume.P_Id:desc'));
            $url =$this->hrbcUrl .  "resume?" . $param;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);

            if($xml = @file_get_contents($url, false, $context)){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 lastLogin"));
                            redirect($this->segment."/lastLogin");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 lastLogin"));
                            redirect($this->segment."/lastLogin");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other lastLogin"));
                            redirect($this->segment."/lastLogin");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout lastLogin"));
                    redirect($this->segment."/lastLogin");
                }
            }
            $xml = simplexml_load_string($xml);

            if($xml->Code!=0){
                $pErr=$xml->Code;
                //エラーメール送信
                $subject = "【resumeログイン履歴登録失敗　code:user01】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                $message = "ログイン履歴登録に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                $this->errMail($subject, $message);

                $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
                redirect("err/error_404");
                //exit;

            }else{
                //HRBCから帰ってきたデータ件数　1件の対応用に取得
                $cnt = $xml->attributes()->Count;

                //データ設定
                $json = json_encode($xml);
                $data["resume"] = json_decode($json,true);

                $now = date('Y/m/d H:i:s');
                $now_hrbc = date("Y/m/d H:i:s",strtotime($now. "-9 hour"));
                $login_log = $data["resume"]["Item"]["Resume.U_CFD727A34F74C45C5786B706A1F90F"];
                $first_login_flg = 0;
                if(empty($login_log)){
                    $login_log="";
                }else{
                    $first_login_flg = 1;
                }
                $login_log = $now . "&#x0A;" .  $login_log;
                $owner_id = $data["resume"]["Item"]["Resume.P_Owner"]["User"]["User.P_Id"];

                ///////////////////////////////////////////////////////////
                // resume に登録
                ///////////////////////////////////////////////////////////
                $url = $this->hrbcUrl . "resume?partition=". $result["partition"];

                //※レジュメにメールとTELを入れるとエラーになる
                //1回でもログインした場合
                if($first_login_flg==1){
                    $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>' . $owner_id . '</Resume.P_Owner><Resume.P_Candidate>'.$person_id.'</Resume.P_Candidate><Resume.U_CFD727A34F74C45C5786B706A1F90F>' . $login_log . '</Resume.U_CFD727A34F74C45C5786B706A1F90F></Item></Resume>';
                }else{
                    $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>' . $owner_id . '</Resume.P_Owner><Resume.P_Candidate>'.$person_id.'</Resume.P_Candidate><Resume.U_CFD727A34F74C45C5786B706A1F90F>' . $login_log . '</Resume.U_CFD727A34F74C45C5786B706A1F90F></Item></Resume>';
                }

                $header = array(
                    "Content-Type: application/xml; charset=UTF-8",
                    "X-porters-hrbc-oauth-token: " . $result['token']
                );

                $options = array('http' => array(
                    'method' => 'POST',
                    'content' => $xml,
                    'header' => implode("\r\n", $header)
                ));
                if($xml = @file_get_contents($url, false, stream_context_create($options))){
                    //ここにデータ取得が成功した時の処理
                }else{
                    //エラー処理
                    if(count($http_response_header) > 0){
                        //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                        $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                        //エラーの判別
                        switch($status_code[1]){
                            //404エラーの場合
                            case 404:
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 lastLogin2"));
                                redirect($this->segment."/lastLogin");
                                break;

                            //500エラーの場合
                            case 500:
                                $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 lastLogin2"));
                                redirect($this->segment."/lastLogin");
                                break;

                            //その他のエラーの場合
                            default:
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other lastLogin2"));
                                redirect($this->segment."/lastLogin");
                        }
                    }else{
                        //タイムアウトの場合 or 存在しないドメインだった場合
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout lastLogin2"));
                        redirect($this->segment."/lastLogin");
                    }
                }

                //xml解析
                $rErr="";
                $rId = "";
                $xml = simplexml_load_string($xml);

                $error="";
                if($xml->Code!=0){
                    $subject = "【resumeログイン履歴登録失敗　code:user02】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                    $message = "ログイン履歴登録に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                    $this->errMail($subject, $message);
                    $rErr=$xml->Code;
                }else{
                    $rCode="";
                    $json = json_encode($xml);
                    $arr = json_decode($json,true);
                    foreach($arr['Item'] as $k => $v){
                        if($k=="Id"){
                            $rId = $v;
                        }
                        if($k=="Code"){
                            $rErr = $v;
                        }
                    }
                }
                if(!$rId || $rErr){
                    $subject = "【resumeログイン履歴登録失敗　code:user03】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                    $message = "ログイン履歴登録に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id . " error:" . $rErr;
                    $this->errMail($subject, $message);
                    $error.= "エラーが発生しました。(Resume: ".$rErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
                }
            }
        }

        //お気に入りの件数
        $sql = "SELECT job_id from likes where member_id = " . $login_id;
        $likeData = $this->db->query($sql);
        $likeCnt = count($likeData->result());
        $this->session->set_userdata('like_cnt', $likeCnt);

        //ログイン済かチェック
		if(empty($login_id)){
            //セッションの破棄
            $this->session->unset_userdata('login_id');
            $this->session->unset_userdata('login_name');

            redirect('user/login' . $param);
		}else{
			$return = $this->db->get_where('members', array('id' => $login_id, 'del' => 0))->row();

            if(!empty($return)){
                //ログイン済
                if($return->id == $login_id){
                    $this->session->set_userdata('user_id', $return->id);
                    $this->session->set_userdata('login_name', $login_name);
                    //未ログイン
                }else{
                    $this->session->unset_userdata('login_id');
                    $this->session->unset_userdata('login_name');

                    redirect('user/login' . $param);
                }
            }else{
                $this->session->unset_userdata('login_id');
                $this->session->unset_userdata('login_name');

                redirect('user/login' . $param);
            }
		}
	}

    //////////////////////////////////////////////////////////////////
    // Person取得
    //////////////////////////////////////////////////////////////////
    function getPerson($pid){
        $datas=array();

        $result = $this->getHrbcMaster();

        ///////////////////////////////////////////////////
        //person取得
        ///////////////////////////////////////////////////
        $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Id:eq=' . $pid));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/candidate?" . $url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist"));
                        redirect($this->segment."/regist");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist"));
                        redirect($this->segment."/regist");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist"));
                        redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist"));
                redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);
        if($xml->Code!=0){
            $pErr=$xml->Code;

            $subject = "【user基本情報編集に失敗　code:getPerson】";
            $message = "user基本情報編集に失敗しました。". $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404");
            //exit;

        }

        //データ設定
        $json = json_encode($xml);
        $person = json_decode($json,true);

        $datas["name"] = $person["Item"]["Person.P_Name"];

        if(!empty($person["Item"]["Person.P_Reading"])){
            $datas["kana"] = $person["Item"]["Person.P_Reading"];
        }else{
            $datas["kana"] = "";
        }
        $datas["tel1"] = "";
        if(empty($person["Item"]["Person.P_Telephone"])){
            if(!empty($person["Item"]["Person.P_Mobile"])){
                $datas["tel1"] = $person["Item"]["Person.P_Mobile"];
            }else{
                $datas["tel1"] = "";
            }
        }else{
            $datas["tel1"] = $person["Item"]["Person.P_Telephone"];
        }

        $datas["mail1"] = $person["Item"]["Person.P_Mail"];

        $datas["id"] = $pid;

        return $datas;
    }

    //////////////////////////////////////////////////////////////////
    // Resume取得
    //////////////////////////////////////////////////////////////////
    function getResume($rid){

        $datas=array();

        $result = $this->getHrbcMaster();

        ////////////////////////////////////////////////////////////////
        //candidate Idからresume取得
        ///////////////////////////////////////////////////////////////
        $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Id:eq=' . $rid));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/resume?" . $url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist2"));
                        redirect($this->segment."/regist");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist2"));
                        redirect($this->segment."/regist");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist2"));
                        redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist2"));
                redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);

        if($xml->Code!=0){
            $pErr=$xml->Code;

            $subject = "【user基本情報編集に失敗　code:getResume】";
            $message = "user基本情報編集に失敗しました。 " . $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404");
            //exit;
        }

        //データ設定
        $json = json_encode($xml);
        $resume = json_decode($json,true);

        if(!empty($resume["Item"]["Resume.P_DateOfBirth"])){
            $birthday = $resume["Item"]["Resume.P_DateOfBirth"];
            $datas["birthday"] =  $birthday;

            $now = date("Ymd");
            $birthday = str_replace("/", "", $birthday);//ハイフンを除去しています。
            $datas["age"] =  floor(($now-$birthday)/10000).'歳';

        }else{
            $datas["birthday"]="";
            $datas["age"]="";
        }

        if(!empty($resume["Item"]["Resume.P_ChangeJobsCount"])){
            $datas["company_number"] = $resume["Item"]["Resume.P_ChangeJobsCount"];
        }else{
            $datas["company_number"] = "";
        }

        $tmpgender="";
        if(!empty($resume["Item"]["Resume.P_Gender"])){
            $tmpgender = $resume["Item"]["Resume.P_Gender"];
        }
        if(!empty($tmpgender)){
            foreach($tmpgender as $k=>$v){
                $datas["sex"] = $v["Option.P_Name"];
            }
        }else{
            $datas["sex"] = "";
        }

        $tmppref="";
        if(!empty($resume["Item"]["Resume.U_7F3DF7F61EEF0679AA432646777832"])){
            $tmppref = $resume["Item"]["Resume.U_7F3DF7F61EEF0679AA432646777832"];
        }
        if(!empty($tmppref)){
            foreach($tmppref as $k=>$v){
                $datas["pref"] = $v["Option.P_Name"];
            }
        }else{
            $datas["pref"] = "";
        }

        $tmpschool="";
        if(!empty($resume["Item"]["Resume.U_673275A4DA445C9B23AC18BDD7C4DC"])){
            $tmpschool = $resume["Item"]["Resume.U_673275A4DA445C9B23AC18BDD7C4DC"];
        }
        if(!empty($tmpschool)){
            foreach($tmpschool as $k=>$v){
                $datas["school_div_id"] = $v["Option.P_Name"];
            }
        }else{
            $datas["school_div_id"] = "";
        }

        $tmpschool_name="";
        if(!empty($resume["Item"]["Resume.U_85DEA8FA18EF17830FD8A036EC95A4"])){
            $tmpschool_name = $resume["Item"]["Resume.U_85DEA8FA18EF17830FD8A036EC95A4"];
        }
        if(!empty($tmpschool_name)){
            $datas["school_name"] = $tmpschool_name;
        }else{
            $datas["school_name"] = "";
        }

        $tmpjokyo="";
        if(!empty($resume["Item"]["Resume.P_CurrentStatus"])){
            $tmpjokyo = $resume["Item"]["Resume.P_CurrentStatus"];
        }
        if(!empty($tmpjokyo)){
            foreach($tmpjokyo as $k=>$v){
                $datas["jokyo"] = $v["Option.P_Name"];
            }
        }else{
            $datas["jokyo"] = "";
        }

        if(!empty($resume["Item"]["Resume.U_91DBD8B96BAAD6B33B538CF2494377"])){
            $datas["kana"] = $resume["Item"]["Resume.U_91DBD8B96BAAD6B33B538CF2494377"];
        }else{
            $datas["kana"] =  "";
        }

        if(!empty($resume["Item"]["Resume.U_5F7FC5144BA471117A376B60BC8D90"])){
            $datas["comment"] = $resume["Item"]["Resume.U_5F7FC5144BA471117A376B60BC8D90"];
        }else{
            $datas["comment"] =  "";
        }

        if(!empty($resume["Item"]["Resume.U_13C45F7FFAFC1AB430E6038C76125A"])){
            $datas["addinfo"] = $resume["Item"]["Resume.U_13C45F7FFAFC1AB430E6038C76125A"];
        }else{
            $datas["addinfo"] =  "";
        }

        $jobCateoryInfo = $this->getJobCategoryInfo();
        $jobTantouCateoryInfo = $this->getTantouJobCategoryInfo();

        //職種
        $datas["kiboujob"]="";
        $tmpkiboujob = array();
        if (!empty($resume['Item']['Resume.U_14551B3DE0AAA0F98964734D422811'])) {
            foreach ($resume['Item']['Resume.U_14551B3DE0AAA0F98964734D422811'] as $k => $v) {
                foreach($jobCateoryInfo as $k2 => $v2){
                    if($v["Option.P_Name"] == $k2){
                        $tmpkiboujob[] = $v2;
                    }
                }
            }
        }
        if(is_array($tmpkiboujob)){
            $tmpkiboujob = array_unique($tmpkiboujob);
            if(!empty($tmpkiboujob)){
                foreach($tmpkiboujob as $k=>$v){
                    $datas["kiboujob"] .= $v . ",";
                }
            }
            $datas["kiboujob"] = rtrim($datas["kiboujob"], ",");
        }

        //都道府県
        $datas["kibouarea1"]="";
        if (!empty($resume['Item']['Resume.U_F3312A78116CAA30201FFFFC4CBC33'])) {
            foreach ($resume['Item']['Resume.U_F3312A78116CAA30201FFFFC4CBC33'] as $k => $v) {
                $datas["kibouarea1"] .= $v["Option.P_Name"] . ",";

            }
            $datas["kibouarea1"] = rtrim($datas["kibouarea1"], ",");
        }else{
            $datas["kibouarea1"]="";
        }

        $datas["kibouarea2"]="";
        if (!empty($resume['Item']['Resume.U_3CA45BBA1FBA2B4D0F2E7A6D25DD7D'])) {
            foreach ($resume['Item']['Resume.U_3CA45BBA1FBA2B4D0F2E7A6D25DD7D'] as $k => $v) {
                $datas["kibouarea2"] .= $v["Option.P_Name"] . ",";

            }
            $datas["kibouarea2"] = rtrim($datas["kibouarea2"], ",");
        }else{
            $datas["kibouarea2"]="";
        }

        $datas["kibouarea3"]="";
        if (!empty($resume['Item']['Resume.U_EF1BA8D1B218E6A4F11BF5EA910E36'])) {
            foreach ($resume['Item']['Resume.U_EF1BA8D1B218E6A4F11BF5EA910E36'] as $k => $v) {
                $datas["kibouarea3"] .= $v["Option.P_Name"] . ",";

            }
            $datas["kibouarea3"] = rtrim($datas["kibouarea3"], ",");
        }else{
            $datas["kibouarea3"]="";
        }

        //年収
        $datas["kibouincome"]="";
        if (!empty($resume['Item']['Resume.U_83F3DDBA6DEC80CE66E21A04631FA7'])) {
            foreach ($resume['Item']['Resume.U_83F3DDBA6DEC80CE66E21A04631FA7'] as $k => $v) {
                $datas["kibouincome"] .= $v["Option.P_Name"] . ",";

            }
            $datas["kibouincome"] = rtrim($datas["kibouincome"], ",");
        }else{
            $datas["kibouincome"]="";
        }

        $datas["earnings"]="";
        if (!empty($resume['Item']['Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504'])) {
            foreach ($resume['Item']['Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504'] as $k => $v) {
                $datas["earnings"] .= $v["Option.P_Name"] . ",";

            }
            $datas["earnings"] = rtrim($datas["earnings"], ",");
        }else{
            $datas["earnings"]="";
        }

        //転勤
        $datas["tenkin"]="";
        if (!empty($resume['Item']['Resume.U_EC012BD105993E9656D208951323F4'])) {
            foreach ($resume['Item']['Resume.U_EC012BD105993E9656D208951323F4'] as $k => $v) {
                $datas["tenkin"] .= $v["Option.P_Name"] . ",";

            }
            $datas["tenkin"] = rtrim($datas["tenkin"], ",");
        }else{
            $datas["tenkin"]="";
        }

        //担当職種
        $datas["tantou_jobs1"]="";
        $tmptantoujob1 = array();
        if (!empty($resume['Item']['Resume.U_90CE972D1600F17EDCEC4661728935'])) {
            foreach ($resume['Item']['Resume.U_90CE972D1600F17EDCEC4661728935'] as $k => $v) {
                foreach($jobTantouCateoryInfo as $k2 => $v2){
                    if($v["Option.P_Name"] == $k2){
                        $tmptantoujob1[] = $v2;
                    }
                }
            }
        }
        if(is_array($tmptantoujob1)){
            $tmptantoujob1 = array_unique($tmptantoujob1);
            if(!empty($tmptantoujob1)){
                foreach($tmptantoujob1 as $k=>$v){
                    $datas["tantou_jobs1"] .= $v . ",";
                }
            }
            $datas["tantou_jobs1"] = rtrim($datas["tantou_jobs1"], ",");
        }

        $datas["tantou_jobs2"]="";
        $tmptantoujob2 = array();
        if (!empty($resume['Item']['Resume.U_910B68CCF4A2F064E0059737A883C0'])) {
            foreach ($resume['Item']['Resume.U_910B68CCF4A2F064E0059737A883C0'] as $k => $v) {
                foreach($jobTantouCateoryInfo as $k2 => $v2){
                    if($v["Option.P_Name"] == $k2){
                        $tmptantoujob2[] = $v2;
                    }
                }
            }
        }
        if(is_array($tmptantoujob2)){
            $tmptantoujob2 = array_unique($tmptantoujob2);
            if(!empty($tmptantoujob2)){
                foreach($tmptantoujob2 as $k=>$v){
                    $datas["tantou_jobs2"] .= $v . ",";
                }
            }
            $datas["tantou_jobs2"] = rtrim($datas["tantou_jobs2"], ",");
        }

        $datas["tantou_jobs3"]="";
        $tmptantoujob3 = array();
        if (!empty($resume['Item']['Resume.U_7245CA44510F223C359FE2D7BC66DB'])) {
            foreach ($resume['Item']['Resume.U_7245CA44510F223C359FE2D7BC66DB'] as $k => $v) {
                foreach($jobTantouCateoryInfo as $k2 => $v2){
                    if($v["Option.P_Name"] == $k2){
                        $tmptantoujob3[] = $v2;
                    }
                }
            }
        }
        if(is_array($tmptantoujob3)){
            $tmptantoujob3 = array_unique($tmptantoujob3);
            if(!empty($tmptantoujob3)){
                foreach($tmptantoujob3 as $k=>$v){
                    $datas["tantou_jobs3"] .= $v . ",";
                }
            }
            $datas["tantou_jobs3"] = rtrim($datas["tantou_jobs3"], ",");
        }

        $datas["company_name1"]="";
        $datas["company_name2"]="";
        $datas["company_name3"]="";
        if(!empty($resume['Item']['Resume.U_9A9AD75E2CF88361F40E91D2B53876'])){
            $datas["company_name1"] = $resume['Item']['Resume.U_9A9AD75E2CF88361F40E91D2B53876'];
        }

        if(!empty($resume['Item']['Resume.U_AE99DF32764C633CAE8A79EDC68490'])){
            $datas["company_name2"] = $resume['Item']['Resume.U_AE99DF32764C633CAE8A79EDC68490'];
        }

        if(!empty($resume['Item']['Resume.U_C12A1CD0372B2FECA9055E0302F59D'])){
            $datas["company_name3"] = $resume['Item']['Resume.U_C12A1CD0372B2FECA9055E0302F59D'];
        }

        $datas["start_date1"]="";
        if(!empty($resume['Item']['Resume.U_12C7C4EC761C78DB864D9AD0FA73ED'])){
            $datas["start_date1"] = $resume['Item']['Resume.U_12C7C4EC761C78DB864D9AD0FA73ED'];
        }
        $datas["start_date2"]="";
        if(!empty($resume['Item']['Resume.U_7CCBD503A17CBD06078DB0907BEC9B'])){
            $datas["start_date2"] = $resume['Item']['Resume.U_7CCBD503A17CBD06078DB0907BEC9B'];
        }
        $datas["start_date3"]="";
        if(!empty($resume['Item']['Resume.U_F65492E72A71583EE72F032C73885A'])){
            $datas["start_date3"] = $resume['Item']['Resume.U_F65492E72A71583EE72F032C73885A'];
        }

        $datas["end_date1"]="";
        if(!empty($resume['Item']['Resume.U_4679EE290064C24E5E593F5772732D'])){
            $datas["end_date1"] = $resume['Item']['Resume.U_4679EE290064C24E5E593F5772732D'];
        }
        $datas["end_date2"]="";
        if(!empty($resume['Item']['Resume.U_FD0444A71EB7C820DC1455C4358EE8'])){
            $datas["end_date2"] = $resume['Item']['Resume.U_FD0444A71EB7C820DC1455C4358EE8'];
        }
        $datas["end_date3"]="";
        if(!empty($resume['Item']['Resume.U_F6662BA5A33D671A4516974181F5F0'])){
            $datas["end_date3"] = $resume['Item']['Resume.U_F6662BA5A33D671A4516974181F5F0'];
        }

        $datas["id"] = $rid;

        return $datas;
    }

    //////////////////////////////////////////////////////////////////
    // HRBCからJOB担当取得
    //////////////////////////////////////////////////////////////////
    function getJobOwner($job)
    {
        $result = $this->getHrbcMaster();

        $hrbc_url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Job.P_Id=' . $job, 'field'=>'Job.P_Id,Job.P_Owner'));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/job?" . $hrbc_url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 newpassword ui"));
                        redirect($this->segment."/regist");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 newpassword ui"));
                        redirect($this->segment."/regist");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other newpassword ui"));
                        redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout newpassword ui"));
                redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);
        if($xml->Code!=0){
            $pErr=$xml->Code;
            $subject = "【code:mycontroller1】" . $pErr;
            $message = $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404");
            exit;
        }

        //データ設定
        $json = json_encode($xml);
        $jobData = json_decode($json, true);

        $userId = $jobData["Item"]["Job.P_Owner"]["User"]["User.P_Id"];

        //////////////////////////////////////////////////////////////////////////////
        // User
        //////////////////////////////////////////////////////////////////////////////
        $hrbc_url = http_build_query(array('partition'=>$result["partition"], 'count'=>200 , 'start'=>0, 'request_type'=>1, 'user_type'=>-1));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/user?" . $hrbc_url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 newpassword ui"));
                        redirect($this->segment."/regist");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 newpassword ui"));
                        redirect($this->segment."/regist");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other newpassword ui"));
                        redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout newpassword ui"));
                redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);
        if($xml->Code!=0){
            $pErr=$xml->Code;
            $subject = "【code:mycontroller2】" . $pErr;
            $message = $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404");
            exit;
        }
        //データ設定
        $json = json_encode($xml);
        $userData = json_decode($json, true);

        $userName="";
        if(!empty($userData)){
            foreach($userData as $k => $v){
                if(!empty($v)){
                    foreach (($v) as $k2 => $v2) {
                        if(!empty($v2["User.P_Name"])){
                            if($v2["User.P_Id"] == $userId){
                                $userName = $v2["User.P_Name"];
                            }
                        }
                    }
                }
            }
        }
        return $userName;
    }

    ///////////////////////////////////////////////////////////////
    // job情報の取得
    ///////////////////////////////////////////////////////////////
    function getJobInfo($param)
    {
        if(is_array($param)){
            $param = $param[1];
        }
        $content = array();
        if(!empty($param)){
            if(is_numeric($param)){
                //データ取得
//                $sql = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = ". HrbcPhaze . " and j.publish = ". HrbcPublish ." and j.job_id = " . $param . " group by j.job_id";
                $sql = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.job_id = " . $param . " group by j.job_id";
                $contents = $this->db->query($sql);
                if(!empty($contents->result()[0])){
                    $contents = $contents->result()[0];
                }

                if(!empty($contents->job_id)){
                    $content = $contents;
                }
            }
        }
        return $contents;
    }

    ///////////////////////////////////////////////////////////////
    // リクルート情報取得
    ///////////////////////////////////////////////////////////////
    function getRecruitInfo($content)
    {
        //企業担当の取得recruiterから
        $esc_clientid = $this->db->escape($content->client_id);
        $sql = "SELECT * from recruiters where r_client = " . $esc_clientid;
        $query = $this->db->query($sql);
        $recruiter = $query->result();

        return $recruiter;
    }

    ////////////////////////////////////////////////////////////////////////
    // JOBCategory 大項目の情報取得用
    ////////////////////////////////////////////////////////////////////////
    function getJobCategoryInfo()
    {
        $masters = $this->getHrbcMaster();
        $jobCategoryInfo = array();
        foreach($masters["jobcategory"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"]["Item"])){
                        foreach($v2['Items']['Item'] as $k3=>$v3) {
                            $jobCategoryInfo[$v3["Option.P_Name"]] = $v2["Option.P_Name"];
                        }
                    }
                }
            }
        }
        return $jobCategoryInfo;
    }

    ////////////////////////////////////////////////////////////////////////
    // 担当JOBの一覧
    ////////////////////////////////////////////////////////////////////////
    function getTantouJobCategoryInfo()
    {
        $masters = $this->getHrbcMaster();
        $jobTantouCategoryInfo = array();
        foreach($masters["jobmypagecd"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    $jobTantouCategoryInfo[$v2["Option.P_Name"]] = $v2["Option.P_Name"];
                }
            }
        }
        return $jobTantouCategoryInfo;
    }

    //////////////////////////////////////////////////////////////////////////
    // Jobから応募のプロセス登録
    //////////////////////////////////////////////////////////////////////////
    function setProcess($param , $person , $resume, $job, $recruit)
    {
        $result = $this->getHrbcMaster();

        $url =$this->hrbcUrl .  "process?partition=".$result["partition"];
        $now = date('Y/m/d H:i:s');
        $phasedate = date("Y/m/d H:i:s", strtotime($now . "-9 hour"));

        $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>-1</Process.P_Id><Process.P_Owner>6</Process.P_Owner><Process.P_Client>' . $job->client_id . '</Process.P_Client><Process.P_Recruiter>' . $recruit[0]->r_id . '</Process.P_Recruiter><Process.P_Job>' . $param . '</Process.P_Job><Process.P_Candidate>' . $person . '</Process.P_Candidate><Process.P_Resume>' . $resume . '</Process.P_Resume><Process.P_Phase><Option.U_001342 /></Process.P_Phase><Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11><Option.U_001311 /></Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11><Process.P_PhaseDate>' . $phasedate . '</Process.P_PhaseDate></Item></Process>';

        $header = array(
            "Content-Type: application/xml; charset=UTF-8",
            "X-porters-hrbc-oauth-token: " . $result['token']
        );

        $options = array('http' => array(
            'method' => 'POST',
            'content' => $xml,
            'header' => implode("\r\n", $header)
        ));

        if($xml = @file_get_contents($url, false, stream_context_create($options))){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 search apply "));
                        redirect($this->segment."/index");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 search apply"));
                        redirect($this->segment."/index");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other search apply"));
                        redirect($this->segment."/index");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout search apply"));
                redirect($this->segment."/index");
            }
        }
        $jErr="";
        $processId = "";
        $hrbcErr="";
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $arr = json_decode($json,true);

        if($xml->Code!=0) {
            if($arr["Code"]==301){
                $processId = "error301";
            }
            $hrbcErr = $xml->Code;
        }else{
            $jCode="";
            foreach($arr['Item'] as $k => $v){
                if($k=="Id"){
                    $processId = $v;
                }
                if($k=="Code"){
                    $jErr = $v;
                }
            }
        }
        $error="";
        if (!$processId) {
            $subject = "【process登録失敗】JOBID: " . $job->client_id;
            $message = "resumeId: " . $resume . " " . " personId: " . $person;
            $this->errHurexMail($subject, $message);
        }
        return $processId;
    }

    //////////////////////////////////////////////////////////////////
    // Process取得
    //////////////////////////////////////////////////////////////////
    function getProcess($rid, $job){
        $datas=array();

        $result = $this->getHrbcMaster();

        ///////////////////////////////////////////////////
        //person取得
        ///////////////////////////////////////////////////
        $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Process.P_Resume:eq=' . $rid . ",Process.P_Job:eq=" . $job->job_id));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/process?" . $url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist"));
                        redirect($this->segment."/regist");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist"));
                        redirect($this->segment."/regist");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist"));
                        redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist"));
                redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);
        if($xml->Code!=0){
            $pErr=$xml->Code;

            $subject = "【getProcess取得に失敗　code:getPerson】";
            $message = "getProcess取得に失敗しました。". $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404");
            //exit;

        }

        //データ設定
        $json = json_encode($xml);
        $process = json_decode($json,true);

        $process_id = $process["Item"]["Process.P_Id"];

        return $process_id;
    }

    //////////////////////////////////////////////////////////////////
    // Process取得
    //////////////////////////////////////////////////////////////////
    function getProcessInfo($rid, $job){
        $datas=array();

        $result = $this->getHrbcMaster();

        ///////////////////////////////////////////////////
        //person取得
        ///////////////////////////////////////////////////
        $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Process.P_Resume:eq=' . $rid . ",Process.P_Job:eq=" . $job->job_id));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/process?" . $url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist"));
                        redirect($this->segment."/regist");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist"));
                        redirect($this->segment."/regist");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist"));
                        redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist"));
                redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);
        if($xml->Code!=0){
            $pErr=$xml->Code;

            $subject = "【getProcessInfo取得に失敗　code:getPerson】";
            $message = "getProcessInfo取得に失敗しました。". $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404");
            //exit;

        }

        //データ設定
        $json = json_encode($xml);
        $process = json_decode($json,true);

        $process = $process["Item"];

        return $process;
    }

    //////////////////////////////////////////////////////////////////
    // User取得
    //////////////////////////////////////////////////////////////////
    function getUser($uid){
        $datas=array();

        $result = $this->getHrbcMaster();

        ///////////////////////////////////////////////////
        //person取得
        ///////////////////////////////////////////////////
        $hrbc_url = http_build_query(array('partition'=>$result["partition"], 'count'=>200 , 'start'=>0, 'request_type'=>1, 'user_type'=>-1));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/user?" . $hrbc_url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 newpassword ui"));
                        redirect($this->segment."/regist");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 newpassword ui"));
                        redirect($this->segment."/regist");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other newpassword ui"));
                        redirect($this->segment."/regist");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout newpassword ui"));
                redirect($this->segment."/regist");
            }
        }
        $xml = simplexml_load_string($xml);
        if($xml->Code!=0){
            $pErr=$xml->Code;
            $subject = "【code:mycontroller2】" . $pErr;
            $message = $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404");
            exit;
        }
        //データ設定
        $json = json_encode($xml);
        $userData = json_decode($json, true);

        return $userData;
    }

    //////////////////////////////////////////////////////////
    //エントリーエラーメール
    //////////////////////////////////////////////////////////
    function entryErrMail($subject, $shimei, $mail1, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $company_name1, $company_name2, $company_name3, $tantou_job1, $tantou_job2, $tantou_job3, $start1, $start2, $start3, $end1, $end2, $end3, $earnings)
    {
        //エラー処理
        $message = <<< EOF
{unwrap}
マイページ新規登録のエラーがありました。

[氏名]
$shimei

[メールアドレス]
$mail1

[希望JOB]
$tmp_kibou_job

[希望収入]
$kibou_income

[転勤]
$tmp_tenkin

[直近会社１]
$company_name1
$tantou_job1
$start1 $end1

[直近会社２]
$company_name2
$tantou_job2
$start3 $end2

[直近会社３]
$company_name3
$tantou_job3
$start3 $end3

[現在の収入]
$earnings

{/unwrap}
EOF;
        $this->errMail($subject, $message);
    }

    //////////////////////////////////////////////////////////
    //　signupメール
    //////////////////////////////////////////////////////////
    function signupMail($to, $url)
    {
        $subject ="【HUREX株式会社】 会員仮登録完了メール";
        $this->signupSendMail($subject, $to, $url);
    }
    //////////////////////////////////////////////////////////
    //エントリー1サンクスメール 転職支援サービス
    //////////////////////////////////////////////////////////
    function signupSendMail($subject, $to, $url)
    {

        $message= <<< EOF
{unwrap}
転職支援会社 ヒューレックスへ会員登録をしていただき、
ありがとうございます。

利用規約に同意され会員登録を完了するには、
引き続き以下のＵＲＬにアクセスして認証手続きをお願いいたします。

■URL:
$url

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　  仙台市青葉区中央1-3-1 アエル17階
　E-mail　ca@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■東日本事業承継推進機構（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。



＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
{/unwrap}
EOF;

        $this->signupThanksMail($subject, $message, $to);
    }

    //////////////////////////////////////////////////////////
    //　エントリー1メール送信　転職支援サービス STEP1
    //////////////////////////////////////////////////////////
    function sendMailStep1($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $kibouarea1, $kibouarea2, $kibouarea3, $age , $agent, $mailTo, $unit_url, $naiyo, $add)
    {
        if(empty($naiyo)){
            $subject ="【支援申込(" . $agent . ")  基本情報入力完了】 " . $age ." " . $tmp_company_number. " " . $school_div_id . " " . $sex . " "  . $shimei;
        }else{
            $subject ="【" . $naiyo . "(" . $agent . ") 基本情報入力完了 】 " . $add . $age ." " . $tmp_company_number. " " . $school_div_id . " " . $sex . " "  . $shimei;
        }
        $this->orderMailStep1($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $kibouarea1, $kibouarea2, $kibouarea3, $age , $agent, $mailTo, $unit_url, $naiyo);

        //サンクスメール
        /*
        if($mailTo){
            $subject = "【" . $shimei . " 様】転職支援サービスへお申し込みいただき、ありがとうございます／ヒューレックス株式会社";
            $this->entry1ThanksMail($subject, $mailTo, $shimei);
        }
        */
    }

    //////////////////////////////////////////////////////////
    //　エントリー1メール送信　転職支援サービス STEP2
    //////////////////////////////////////////////////////////
    function sendMailStep2($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $job_id, $unit_url, $age , $agent, $mailTo, $text, $add)
    {
        if(empty($text)){
            $subject ="【支援申込(" . $agent . ")  登録完了】 " . $age ." " . $tmp_company_number. " " . $school_div_id . " " . $sex . " "  . $shimei;
        }else{
            $subject ="【" . $text . "(" . $agent . ") 登録完了 】 " . $add . $age ." " . $tmp_company_number. " " . $school_div_id . " " . $sex . " "  . $shimei;
        }

        $this->orderMailStep2($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $job_id, $unit_url, $age , $agent, $mailTo, $text);

        //サンクスメール
        /*
        if($mailTo){
            $subject = "【" . $shimei . " 様】転職支援サービスへお申し込みいただき、ありがとうございます／ヒューレックス株式会社";
            $this->entry1ThanksMail($subject, $mailTo, $shimei);
        }
        */
    }

    //////////////////////////////////////////////////////////
    //エントリー1サンクスメール 転職支援サービス
    //////////////////////////////////////////////////////////
    function entry1ThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

お世話になっております。
転職支援会社 ヒューレックスでございます。

この度は、弊社ホームページより転職支援サービスへお申し込みいただき、誠にありがとうございます。

今後ご転職活動を支援させていただきますので、どうぞ宜しくお願い致します。

マイページが開設されましたので、以下のURLよりアクセスしてください。

マイページでは、
・ご登録いただいた希望条件に合致した求人のご検索
・気になる求人の保存
・求人詳細への問合せ
・求人へのご応募
・選考が進んでいる求人の進行管理
等が可能です。

ぜひご活用ください。

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【マイページURL】
　　▼　　▼　　▼　　▼　　▼
https://www.hurex.jp/mypage/user/login/
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱いおよび管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　  仙台市青葉区中央1-3-1 アエル17階
　E-mail　ca@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■東日本事業承継推進機構（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。



＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    //////////////////////////////////////////////////////////
    //　エントリー2メール送信 新規登録者　UIターン
    //////////////////////////////////////////////////////////
    function sendMail2_new($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number, $jokyo, $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $tmp_kibou_area, $unit_url,$age, $agent, $param, $person, $resume, $naiyo, $mailTo)
    {
        $subject = "【" . $naiyo . " (" . $agent . ")】" . $age . "歳 " . $tmp_company_number . " " . $school_div_id . " " . $sex . " " . $shimei;
        $this->entry2OrderMail($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number, $jokyo, $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $tmp_kibou_area, $unit_url);

        //サンクスメール
        if($mailTo){
            $subject = "【" . $shimei . " 様】東北Ｕ・Ｉターン転職相談会 in 東京へお申し込みいただき、ありがとうございます／ヒューレックス株式会社";
            $this->entry2ThanksMail($subject, $mailTo, $shimei);
        }
    }

    //////////////////////////////////////////////////////////
    //　エントリー2メール送信　既存登録者
    //////////////////////////////////////////////////////////
    function sendMail2($agent, $param, $person, $resume, $comment, $naiyo)
    {
        $subject = "【既登録者" . $naiyo . " (" . $agent . ")】" . $person["name"] . " " . $person["tel1"];
        $person_url = "{unwrap}Person: https://hrbc-jp.porterscloud.com/can/search/id/" . $person["id"] . "#/dv/1:" . $person["id"] . "/{/unwrap}";
        $resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5#/dv/17:" . $resume["id"] ."/";
        //$resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5{/unwrap}";
        //$unit_url = $person_url . "\n" . $resume_url;
        $unit_url = $resume_url;
        $message = $naiyo . "　登録通知" . "\n" . $comment . "\n" . $unit_url;
        $this->successMail($subject, $message);

        //サンクスメール
        if($person["mail1"]){
            $subject = "【" . $person["name"] . " 様】東北Ｕ・Ｉターン転職相談会 in 東京へお申し込みいただき、ありがとうございます／ヒューレックス株式会社";
            $this->entry2ThanksMail($subject, $person["mail1"],$person["name"]);
        }
    }

    //////////////////////////////////////////////////////////
    //エントリー2サンクスメール UIターン
    //////////////////////////////////////////////////////////
    function entry2ThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

お世話になっております。
転職支援会社 ヒューレックスでございます。

この度は、弊社ホームページより東北Ｕ・Ｉターン転職相談会 in 東京へお申し込みいただき、誠にありがとうございました。

今後ご転職活動を支援させていただきますので、どうぞ宜しくお願い致します。

さて、弊社では毎週数多くの方から東北Ｕ・Ｉターン転職相談会 in 東京へのお申し込みを頂戴しておりますが、定数に限りがある関係上、すぐに具体的な求人情報をご案内できる方から優先的にご予約を受け付けさせていただいております。

そのため、まずはこれまでのご経験やご希望条件を確認させていただき、ご紹介できる求人を探して参りたいと思います。

簡単な入力フォームを用意しておりますので、下記URLよりアクセスしていただき、ご入力をお願い致します。

ご入力された内容の確認が取れましたら、改めて担当より連絡いたします。

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【ご経歴・ご希望条件入力フォーム】
　　▼　　▼　　▼　　▼　　▼
https://business.form-mailer.jp/fms/831589fa42879

※お申し込み時に履歴書や職務経歴書を添付された方は、ご入力いただかなくて結構です。
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

また、マイページが開設されましたので、以下のURLよりアクセスしてください。

マイページでは、
・ご登録いただいた希望条件に合致した求人のご検索
・気になる求人の保存
・求人詳細への問合せ
・求人へのご応募
・選考が進んでいる求人の進行管理
等が可能です。

ぜひご活用ください。

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【マイページURL】
　　▼　　▼　　▼　　▼　　▼
https://www.hurex.jp/mypage/user/login/
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝


ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱いおよび管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　  仙台市青葉区中央1-3-1 アエル17階
　E-mail　ca@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■東日本事業承継推進機構（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。



＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    //////////////////////////////////////////////////////////
    //　エントリー４メール送信 新規登録者　求人詳細を聞く
    //////////////////////////////////////////////////////////
    function sendMail4_new($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $job_id, $unit_url, $age , $agent, $mailTo)
    {
        $user = $this->getJobOwner($job_id);

        $subject ="【求人問合 (" . $agent . ")】" . $age ."歳 " . $tmp_company_number . " " . $school_div_id . " " . $sex . " "  . $shimei;
        $this->entry3OrderMail($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $job_id, $unit_url, $age , $agent, $mailTo);

        //サンクスメール
        if($mailTo){
            $subject = "【" . $shimei . " 様】求人へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $this->entry4newThanksMail($subject, $mailTo, $shimei);
        }
    }

    //////////////////////////////////////////////////////////
    //エントリー4サンクスメール 新規登録者のjob詳細を聞く
    //////////////////////////////////////////////////////////
    function entry4newThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

お世話になっております。
転職支援会社 ヒューレックスでございます。

この度は、弊社ホームページより求人へお問い合わせいただき、誠にありがとうございます。
ご経験やご希望条件が、企業が定める求人の応募要件と合致されていない場合は、求人の詳細に関してお伝えできない場合がございます。
ご了承いただきますようお願い致します。

また、マイページが開設されましたので、以下のURLよりアクセスしてください。

マイページでは、
・ご登録いただいた希望条件に合致した求人のご検索
・気になる求人の保存
・求人詳細への問合せ
・求人へのご応募
・選考が進んでいる求人の進行管理
等が可能です。

ぜひご活用ください。

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【マイページURL】
　　▼　　▼　　▼　　▼　　▼
https://www.hurex.jp/mypage/user/login/
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱いおよび管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　  仙台市青葉区中央1-3-1 アエル17階
　E-mail　ca@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■東日本事業承継推進機構（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。



＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    //////////////////////////////////////////////////////////
    //　エントリー４メール送信　既存登録者　求人詳細を聞く
    //////////////////////////////////////////////////////////
    function sendMail4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo, $add)
    {
        //JOB担当
        $user = $this->getJobOwner($jobinfo->job_id);

        //$subject ="【既登録者 (" . $agent . ")：求人詳細問合わせ】JOB:" . $param . " " . $person["name"] . " " . $person["tel1"];
        $subject ="■■問合わせ■■　JOB担当:" . $add["job_owner"] . " （打診担当:" . $add["process_owner"] . "）" . $add["client_name"];
        $person_url = "{unwrap}Person: https://hrbc-jp.porterscloud.com/can/search/id/" . $person["id"] . "#/dv/1:" . $person["id"] . "/{/unwrap}";
        //$resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5/{/unwrap}";
        $resume_url  ="{unwrap}\n\n【キャンディデイトBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5#/dv/17:" . $resume["id"] ."/{/unwrap}";

        $job_url ="{unwrap}\n\n【応募JobBCリンク】" . "\n" .  "https://hrbc-jp.porterscloud.com/job/search/id/".$param. "?menu_id=3{/unwrap}";

        $process_url ="{unwrap}\n\n【選考プロセスBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $resume["id"] . "#/dv/7:" . $processId . "/{/unwrap}";
        $unit_url = "\n" . $resume_url . "\n" . $job_url . "\n" . $process_url;

        $kana="";
        if(!empty($person["kana"])){
            $kana  = $person["kana"];
        }else if(!empty($resume["kana"])){
            $kana = $resume["kana"];
        }

        $message = "マイページより求人詳細問合せがありました。" . "\n\n[氏名]\n" . $person["name"] . "\n\n[ふりがな]\n" . $kana . "\n\n[生年月日]\n" . $resume["birthday"] . "\n\n[経験社数]\n" .$resume["company_number"] . "社" . "\n\n[Jobの担当者]\n" . $user . "\n\n[企業名]\n" . $jobinfo->c_title . "\n\n[ポジション名]\n". $jobinfo->job_title . "\n" . $unit_url;
        $this->successMail($subject, $message);

        /*
        //サンクスメール
        if($person["mail1"]){
            $subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . " "  . $jobinfo->c_title . "へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $this->entry4ThanksMail($subject, $person["mail1"], $person["name"]);
        }
        */
    }

    //////////////////////////////////////////////////////////
    //　エントリー４メール送信　既存登録者　登録済み to hurex
    //////////////////////////////////////////////////////////
    function sendMail4_alreay($agent, $param, $person, $resume, $processId, $recruits, $jobinfo, $process_id)
    {
        //JOB担当
        $user = $this->getJobOwner($jobinfo->job_id);

        $subject ="【登録済み求人詳細問い合わせ (" . $agent . ")：既登録者】JOB:" . $param . " " . $person["name"] . " " . $person["tel1"];
        $person_url = "{unwrap}Person: https://hrbc-jp.porterscloud.com/can/search/id/" . $person["id"] . "#/dv/1:" . $person["id"] . "/{/unwrap}";
        //$resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5/{/unwrap}";
        $resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5#/dv/17:" . $resume["id"] ."/{/unwrap}";

        $job_url ="{unwrap}\n\n【応募JobBCリンク】" . "\n" .  "https://hrbc-jp.porterscloud.com/job/search/id/".$param. "?menu_id=3{/unwrap}";
        $process_url ="{unwrap}\n\n【選考プロセスBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $resume["id"] . "#/dv/7:" . $process_id . "/{/unwrap}";
        $unit_url = "\n" . $resume_url . "\n" . $job_url . "\n" . $process_url;

        $kana="";
        if(!empty($person["kana"])){
            $kana  = $person["kana"];
        }else if(!empty($resume["kana"])){
            $kana = $resume["kana"];
        }

        $message = "登録済み求人へマイページより求人詳細問合せがありました。" . "\n\n[氏名]\n" . $person["name"] . "\n\n[ふりがな]\n" . $kana . "\n\n[生年月日]\n" . $resume["birthday"] . "\n\n[経験社数]\n" .$resume["company_number"] . "社" . "\n\n[Jobの担当者]\n" . $user . "\n\n[企業名]\n" . $jobinfo->c_title . "\n\n[ポジション名]\n". $jobinfo->job_title . "\n" . $unit_url;
        $this->successMail($subject, $message);

        /*
        //サンクスメール
        if($person["mail1"]){
            $subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . " "  . $jobinfo->c_title . "へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $this->entry4ThanksMail($subject, $person["mail1"], $person["name"]);
        }
        */
    }

    //////////////////////////////////////////////////////////
    //　エントリー４サンクスメール送信　既存登録者　求人詳細を聞く
    //////////////////////////////////////////////////////////
    function sendMailThanks4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo)
    {
        //サンクスメール
        if($person["mail1"]){
//            $subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . " "  . $jobinfo->c_title . "へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . "お問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $this->entry4ThanksMail($subject, $person["mail1"], $person["name"]);
        }
    }

    //////////////////////////////////////////////////////////
    //エントリー4サンクスメール 既登録者のjob詳細を聞く
    //////////////////////////////////////////////////////////
    function entry4ThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

お世話になっております。
転職支援会社 ヒューレックスでございます。

この度は、マイページより求人へお問い合わせいただき、誠にありがとうございます。
ご経験やご希望条件が、企業が定める求人の応募要件と合致されていない場合は、求人の詳細に関してお伝えできない場合がございます。
ご了承いただきますようお願い致します。

確認の上のご回答となるため、ご回答までお時間をいただくことがございますので、ご了承ください。

ヒューレックスでは、個人情報保護の観点から、個人情報に関する適切な取り扱いおよび管理に努めております。

お申し込みいただいた個人情報は、職業紹介の目的にのみ使用し、他の目的で使用することはございません。

━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　  仙台市青葉区中央1-3-1 アエル17階
　E-mail　ca@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■東日本事業承継推進機構（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。
{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }

    //////////////////////////////////////////////////////////
    //　エントリー5 プロセスからの応募
    //////////////////////////////////////////////////////////
    function sendMail5($agent, $param, $person, $resume, $jobinfo, $unit_url, $to, $add)
    {
        //JOB担当
        $user = $this->getJobOwner($jobinfo->job_id);

//        $subject ="【既登録者 (" . $agent . ")：Job打診からの申込】JOB:" . $param . " " . $person["name"] . " " . $person["tel1"];
        $subject ="■■応募承諾■■　JOB担当:" . $add["job_owner"] . " （打診担当:" . $add["process_owner"] . "）" . $add["client_name"];

        $kana="";
        if(!empty($person["kana"])){
            $kana  = $person["kana"];
        }else if(!empty($resume["kana"])){
            $kana = $resume["kana"];
        }

        $message = "マイページからJob打診からの応募がありました。" . "\n\n[氏名]\n" . $person["name"] . "\n\n[ふりがな]\n" . $kana . "\n\n[生年月日]\n" . $resume["birthday"] . "\n\n[経験社数]\n" .$resume["company_number"] . "社" . "\n\n[Jobの担当者]\n" . $user . "\n\n[企業名]\n" . $jobinfo->c_title . "\n\n[ポジション名]\n". $jobinfo->job_title . "\n\n" . $unit_url;
        $this->successMailProcess($subject, $message, $to);

        //サンクスメール
        if($person["mail1"]){
            //$subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . " "  . $jobinfo->c_title . "へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $subject = "【" . $person["name"] . " 様】求人へご応募いただき、ありがとうございます。／ヒューレックス株式会社";
            //$this->entry4ThanksMail($subject, $person["mail1"], $person["name"]);
            $this->jobdashinThanksMail($subject, $person["mail1"], $person["name"]);
        }

    }

    //////////////////////////////////////////////////////////
    //　エントリー６　お気に入りからの応募
    //////////////////////////////////////////////////////////
    function sendMail6($agent, $param, $person, $resume, $processId, $recruits, $jobinfo, $add)
    {
        //JOB担当
        $user = $this->getJobOwner($jobinfo->job_id);

//        $subject ="【既登録者 (" . $agent . ")：お気に入りからの申込】JOB:" . $param . " " . $person["name"] . " " . $person["tel1"];
        $subject ="■■問合わせ■■　JOB担当:" . $add["job_owner"] . " （打診担当:" . $add["process_owner"] . "）" . $add["client_name"];

        $person_url = "{unwrap}Person: https://hrbc-jp.porterscloud.com/can/search/id/" . $person["id"] . "#/dv/1:" . $person["id"] . "/{/unwrap}";

        //$resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5/{/unwrap}";
        $resume_url  ="{unwrap}\n\n【キャンディデイトBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume["id"] ."?menu_id=5#/dv/17:" . $resume["id"] ."/{/unwrap}";

        $job_url ="{unwrap}\n\n【応募JobBCリンク】" . "\n" .  "https://hrbc-jp.porterscloud.com/job/search/id/".$param. "?menu_id=3{/unwrap}";
        $process_url ="{unwrap}\n\n【選考プロセスBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $resume["id"] . "#/dv/7:" . $processId . "/{/unwrap}";
        $unit_url = $resume_url . "\n" . $job_url . "\n" . $process_url;

        $kana="";
        if(!empty($person["kana"])){
            $kana  = $person["kana"];
        }else if(!empty($resume["kana"])){
            $kana = $resume["kana"];
        }

        $message = "お気に入りから応募がありました。" . "\n\n[氏名]\n" . $person["name"] . "\n\n[ふりがな]\n" . $kana . "\n\n[生年月日]\n" . $resume["birthday"] . "\n\n[経験社数]\n" .$resume["company_number"] . "社" . "\n\n[Jobの担当者]\n" . $user . "\n\n[企業名]\n" . $jobinfo->c_title . "\n\n[ポジション名]\n". $jobinfo->job_title . "" . $unit_url;
        $this->successMail($subject, $message);

        /*
        //サンクスメール
        if($person["mail1"]){
            $subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . " "  . $jobinfo->c_title . "へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $this->entry4ThanksMail($subject, $person["mail1"], $person["name"]);
        }
        */
    }

    //////////////////////////////////////////////////////////
    //　エントリー7 JOB打診の辞退
    //////////////////////////////////////////////////////////
    function sendMail7($agent, $param, $person, $resume, $jobinfo, $unit_url, $to, $add)
    {
        //JOB担当
        $user = $this->getJobOwner($jobinfo->job_id);

        //$subject ="【既登録者 (" . $agent . ")：Job打診からの辞退】JOB:" . $param . " " . $person["name"] . " " . $person["tel1"];
        $subject ="■■本人NG■■　JOB担当:" . $add["job_owner"] . " （打診担当:" . $add["process_owner"] . "）" . $add["client_name"];

        $kana="";
        if(!empty($person["kana"])){
            $kana  = $person["kana"];
        }else if(!empty($resume["kana"])){
            $kana = $resume["kana"];
        }

        $message = "マイページからJob打診の辞退がありました。" . "\n\n[氏名]\n" . $person["name"] . "\n\n[ふりがな]\n" . $kana . "\n\n[生年月日]\n" . $resume["birthday"] . "\n\n[経験社数]\n" .$resume["company_number"] . "社" . "\n\n[Jobの担当者]\n" . $user . "\n\n[企業名]\n" . $jobinfo->c_title . "\n\n[ポジション名]\n". $jobinfo->job_title . "\n\n" . $unit_url;
        $this->successMailProcess($subject, $message, $to);

        /*
        //サンクスメール
        if($person["mail1"]){
            $subject = "【" . $person["name"] . " 様】Jobid:　" . $jobinfo->job_id . " "  . $jobinfo->c_title . "へお問い合わせいただき、ありがとうございます。／ヒューレックス株式会社";
            $this->entry4ThanksMail($subject, $person["mail1"], $person["name"]);
        }
        */
    }

    //////////////////////////////////////////////////////////
    // オーダーメール STEP1
    //////////////////////////////////////////////////////////
    function orderMailStep1($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $kibouarea1, $kibouarea2, $kibouarea3, $age , $agent, $mailTo, $unit_url, $naiyo)
    {
        if(!empty($naiyo)){
            $comment_midashi = "希望参加日";
        }else{
            $comment_midashi = "その他";
        }
        $message = <<< EOF
{unwrap}
ご担当者様
ホームページより、 $shimei 様のエントリーがありました。
HRBCにログインし、エントリー情報をご確認下さい。

[氏名]
$shimei

[ふりがな]
$kana

[生年月日]
$birthday

[性別]
$sex

[現住所]
$pref

[電話番号]
$tel1

[最終学歴]
$school_div_id

[学校名]
$school_name

[経験社数]
$tmp_company_number

[就業状況]
$jokyo

[$comment_midashi]
$comment

[希望勤務地１]
$kibouarea1

[希望勤務地２]
$kibouarea2

[希望勤務地３]
$kibouarea3

{/unwrap}
$unit_url
EOF;
        $this->entryOrderMail($subject, $message);
    }

    //////////////////////////////////////////////////////////
    // オーダーメール STEP2
    //////////////////////////////////////////////////////////
    function orderMailStep2($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $job_id, $unit_url, $age , $agent, $mailTo)
    {
        if(!empty($text)){
            $comment_midashi = "希望参加日";
        }else{
            $comment_midashi = "その他";
        }
        $message = <<< EOF
{unwrap}
ご担当者様
ホームページより、 $shimei 様のエントリーがありました。
HRBCにログインし、エントリー情報をご確認下さい。

[氏名]
$shimei

[ふりがな]
$kana

[生年月日]
$birthday

[性別]
$sex

[現住所]
$pref

[電話番号]
$tel1

[最終学歴]
$school_div_id

[学校名]
$school_name

[経験社数]
$tmp_company_number

[就業状況]
$jokyo

[$comment_midashi]
$comment

[希望職種]
$tmp_kibou_job

[希望年収]
$kibou_income

[転勤]
$tmp_tenkin

[希望勤務地１]
$kibouarea1

[希望勤務地２]
$kibouarea2

[希望勤務地３]
$kibouarea3

[直近の会社１]
$company_name1
$tantou_jobs1
$start_date1 ～ $end_date1

[直近の会社２]
$company_name2
$tantou_jobs2
$start_date2 ～ $end_date2

[直近の会社３]
$company_name3
$tantou_jobs3
$start_date3 ～ $end_date3

[現在の年収]
$current_earnings

{/unwrap}
$unit_url
EOF;

        $this->entryOrderMail($subject, $message);
    }

    //////////////////////////////////////////////////////////
    //エントリー2オーダーメール UIターン
    //////////////////////////////////////////////////////////
    function entry2OrderMail($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name,$tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $tmp_kibou_area, $unit_url)
    {
        $message = <<< EOF
{unwrap}
ご担当者様
ホームページより、 $shimei 様のエントリーがありました。
HRBCにログインし、エントリー情報をご確認下さい。

[氏名]
$shimei

[ふりがな]
$kana

[生年月日]
$birthday

[性別]
$sex

[現住所]
$pref

[電話番号]
$tel1

[最終学歴]
$school_div_id

[学校名]
$school_name

[経験社数]
$tmp_company_number

[就業状況]
$jokyo

[ご希望の参加日]
$comment

[希望職種]
$tmp_kibou_job

[年収]
$kibou_income

[転勤]
$tmp_tenkin

[希望地域]
$tmp_kibou_area

{/unwrap}
$unit_url
EOF;



        $this->entryOrderMail($subject, $message);
    }

    //////////////////////////////////////////////////////////
    //エントリー3オーダーメール JOB詳細を聞く
    //////////////////////////////////////////////////////////
    function entry3OrderMail($subject, $shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibou_income, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $job_id, $unit_url, $age , $agent, $mailTo)
    {
        $message = <<< EOF
{unwrap}
ご担当者様
ホームページより、 $shimei 様のエントリーがありました。
HRBCにログインし、エントリー情報をご確認下さい。

[氏名]
$shimei

[ふりがな]
$kana

[生年月日]
$birthday

[性別]
$sex

[現住所]
$pref

[電話番号]
$tel1

[最終学歴]
$school_div_id

[学校名]
$school_name

[経験社数]
$tmp_company_number

[就業状況]
$jokyo

[その他]
$comment

[JOB]
$job_id

[希望職種]
$tmp_kibou_job

[希望年収]
$kibou_income

[転勤]
$tmp_tenkin

[希望勤務地１]
$kibouarea1

[希望勤務地２]
$kibouarea2

[希望勤務地３]
$kibouarea3

[直近の会社１]
$company_name1
$tantou_jobs1
$start_date1 ～ $end_date1

[直近の会社２]
$company_name2
$tantou_jobs2
$start_date2 ～ $end_date2

[直近の会社３]
$company_name3
$tantou_jobs3
$start_date3 ～ $end_date3

[現在の年収]
$current_earnings

{/unwrap}
$unit_url
EOF;



        $this->entryOrderMail($subject, $message);
    }

    //////////////////////////////////////////////////////////
    //エントリー4オーダーメール JOB詳細を聞く 既登録者
    //////////////////////////////////////////////////////////
    function entry4OrderMail($subject, $shimei, $birthday, $company_number, $tantou, $client, $job_title ,$jobid, $unit_url)
    {
        $message = <<< EOF
{unwrap}
マイページより求人詳細問合せがありました。
$shimei
$birthday
$company_number
$tantou
$client
$job_title
{/unwrap}
$unit_url
EOF;



        $this->entryOrderMail($subject, $message);
    }

    //////////////////////////////////////////////////////////
    //ジョブ打診　サンクスメール
    //////////////////////////////////////////////////////////
    function jobdashinThanksMail($subject, $mailTo, $shimei)
    {

        $message= <<< EOF
{unwrap}
$shimei 様

この度は、弊社の取り扱い求人にご応募いただき、誠にありがとうございます。

たしかにご希望を承りました。
この求人の担当コンサルタントが応募の手続きを進めてまいります。
進捗がありましたら、随時ご報告させていただきますので、少しお時間をいただければと存じます。
尚、応募にあたっての書類準備等がある場合は、後ほど担当コンサルタントからご連絡させていただきます。
以上、今後とも、どうぞ宜しくお願い申し上げます。


━━━━━━━━━━━━━━━━━━━━
　ヒューレックス株式会社

　〒980-6117
　  仙台市青葉区中央1-3-1 アエル17階
　E-mail　ca@hurex.co.jp
　tel.0120-14-1150／fax.022-723-1738
────────────────────
【Group web site】
　■ヒューレックス
    https://www.hurex.jp/
　■マリッジパートナーズ
    https://www.mpartners.jp/
　■東日本事業承継推進機構（AOBA）
    http://aoba-east.jp/

【facebook page】
　■ヒューレックス
    http://www.facebook.com/hurex.japan
　■マリッジパートナーズ
    https://www.facebook.com/marriagepartners
────────────────────

このメールに心当たりのない方は、メールアドレスが誤って入力された可能性がありますので、破棄していただきますようお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。ご返信いただいてもご対応できかねますので、ご了承ください。
{/unwrap}
EOF;

        $this->entryThanksMail($subject, $message, $mailTo);
    }


    ////////////////////////////////////////////////////////////////
    // user agent
    ////////////////////////////////////////////////////////////////
    function getUserAgent()
    {
        $this->load->library('user_agent');
        $agent="";
        if ($this->agent->is_mobile()) {
            $agent = "SP";
        }else{
            $agent = "PC";
        }
        return $agent;
    }

    ////////////////////////////////////////////////////////////////
    // メールアドレス暗号化初期値
    ////////////////////////////////////////////////////////////////
    function mail_crypt_iv($email)
    {
        //POSTされた値
        $TargetStr = $email;

        //暗号化・復元用のIVキーを作成
        //指定した暗号モードの組み合わせに属する IV の大きさ
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);

        //暗号モードに対するIVのサイズに合わせたキーを生成
        $iv = random_bytes($iv_size);

        return $iv;
    }
    ////////////////////////////////////////////////////////////////
    // メールアドレス暗号化
    ////////////////////////////////////////////////////////////////
    function mail_crypt($email, $iv)
    {
        //POSTされた値
        $TargetStr = $email;

        //暗号化・復元用のソルト
        $passphrase = mailSalt;

        //暗号化を実施
        $CryptTarget = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $passphrase, $TargetStr, MCRYPT_MODE_CBC, $iv);

        return $CryptTarget;

    }


    ////////////////////////////////////////////////////////////////
    // メールアドレス復元
    ////////////////////////////////////////////////////////////////
    function mail_decrypt($base64_Iv,$base64_CryptTarget )
    {
        //復元したいデータとそのIVキーをデータベースから引き出してBase64でデコード
        $iv = base64_decode($base64_Iv );
        $CryptTarget= base64_decode($base64_CryptTarget );

        //暗号化・復元用のソルトを指定
        $passphrase = mailSalt;

        //復元
        $DecryptTarget_ = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $passphrase, $CryptTarget, MCRYPT_MODE_CBC, $iv);

        return $DecryptTarget_;
    }

    //////////////////////////////////////////////////////////
    //パスワード再発行用のトークン
    //////////////////////////////////////////////////////////
    function generatePassword ()
    {
        $len = 16;
        srand ( (double) microtime () * 1000000);
        $seed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        $pass = "";
        while ($len--) {
            $pos = rand(0,61);
            $pass .= $seed[$pos];
        }
        return $pass;
    }

    ////////////////////////////////////////////////////////////////
	// csrf　発行
	////////////////////////////////////////////////////////////////
	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	////////////////////////////////////////////////////////////////
	// csrf チェック
	////////////////////////////////////////////////////////////////
	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

    ////////////////////////////////////////////////////////////////
    // sessionにID保持
    ////////////////////////////////////////////////////////////////
	function setLoginId($id)
    {
        $this->load->helper(array('cookie'));
        //IDが未設定の場合はcookieにキーが保存されているかチェック
        if(empty($id)){
            $login_cookie="";
            if(!empty($_COOKIE["loginkey"])) {
                $login_cookie = $_COOKIE["loginkey"];
            }
            if($login_cookie){
                $return = $this->db->get_where("members", array('loginkey' => $login_cookie, 'del' => 0))->row();
                if($return->id){
                    $id = $return->id;
                }else{
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
                    redirect($this->segment."/login");
                }
            }
        }

        if(empty($id)){
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/login");
        }

        $sessionData = array(
            'login_id' => $id
        );
        $this->session->set_userdata($sessionData);
    }

    ////////////////////////////////////////////////////////////////
    // エラーメール送信
    ////////////////////////////////////////////////////////////////
    function errMail($subject, $message)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle);
        $this->email->to(kanriMail);
//        $this->email->to('mypage@hurex.co.jp.test-google-a.com');
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }
    }

    ////////////////////////////////////////////////////////////////
    // エラーメール送信
    ////////////////////////////////////////////////////////////////
    function errHurexMail($subject, $message)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
//        $this->email->to(hurexMailTo, addMailTo);
        $this->email->to(
            array(hurexMailTo, addMailTo)
        );
//        $this->email->to('mypage@hurex.co.jp.test-google-a.com');
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }
    }

    ////////////////////////////////////////////////////////////////
    // メール送信
    ////////////////////////////////////////////////////////////////
    function successMail($subject, $message)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
//        $this->email->to(hurexMailTo, addMailTo);
        $this->email->to(
            array(hurexMailTo, addMailTo)
        );
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }

    }

    ////////////////////////////////////////////////////////////////
    // メール送信 オファーの申し込み、辞退の場合はジョブ担当へ
    ////////////////////////////////////////////////////////////////
    function successMailProcess($subject, $message, $to)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
//        $this->email->to($to, addMailTo);
        $this->email->to(
            array(hurexMailTo, addMailTo)
        );
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }

    }

    ////////////////////////////////////////////////////////////////
    // signupメール送信
    ////////////////////////////////////////////////////////////////
    function signupThanksMail($subject, $message, $to)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }

    }

    ////////////////////////////////////////////////////////////////
    // マイページ登録サンクスメール
    ////////////////////////////////////////////////////////////////
    function entryThanksMail($subject, $message, $mailTo)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
        $this->email->to($mailTo);
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }

    }

    ////////////////////////////////////////////////////////////////
    // マイページ登録orderメール
    ////////////////////////////////////////////////////////////////
    function entryOrderMail($subject, $message)
    {
        $this->load->library('email');
        $this->email->from(hurexMailFrom, hurexMailTitle, returnPath);
//        $this->email->to(hurexMailTo, addMailTo);
        $this->email->to(
            array(hurexMailTo, addMailTo)
        );
        $this->email->subject($subject);
        $this->email->message($message);
        if ( ! $this->email->send())
        {
            $message .= $this->email->print_debugger();
            $this->errMail($subject, $message);
        }

    }

    //////////////////////////////////////////////////////////
    //HRBC基本情報
    //////////////////////////////////////////////////////////
    public function getHrbcMaster(){
        $dir = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/";
        include($dir . "system_entry/mimes.php");

        $error="";
        //ファイルが無ければ再作成
        if(!file_exists($dir . 'token/cache.json')) {
            $this->setHrbcBasicData();
            //require($dir . 'conf/auth.php');
        }

        //ファイル時間20分ごとのcronでミスがあったとき
        $ftime = filemtime($dir . 'token/cache.json');
        $twminbf = strtotime( "-20 min" );
        if($ftime < $twminbf){
            $this->setHrbcBasicData();
            //require($dir . 'conf/auth.php');
        }

        if(empty($token) || empty($partition)){
            $this->setHrbcBasicData();
            //require($dir . 'conf/auth.php');
        }

        $datas=array();
        //ファイル読込
        if($json = @file_get_contents($dir . 'token/cache.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("err/error_hrbc");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("err/error_hrbc");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("err/error_hrbc");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました１。Err:timeout"));
                redirect("err/error_hrbc");
            }
        }
        //$json = file_get_contents($dir . 'token/cache.json', true);
        $arr = json_decode($json,true);
        $token = $arr['token'];
        $partition = $arr['partition'];
        $datas["token"] = $token;
        $datas["partition"] = $partition;

        //HRBCマスターと連動
        if($tmppref = @file_get_contents($dir. 'pref.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました２。Err:timeout"));
                redirect("/user/home");
            }
        }
        //$tmppref = file_get_contents($dir. 'pref.json', true);
        $tmppref = json_decode($tmppref,true);
        $datas["pref"] = $tmppref;

        if($tmpgender = @file_get_contents($dir. 'gender.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました３。Err:timeout"));
                redirect("user/home");
            }
        }
        //$tmpgender = file_get_contents($dir . 'gender.json', true);
        $tmpgender = json_decode($tmpgender,true);
        $datas["gender"] = $tmpgender;

        if($tmpbackground = @file_get_contents($dir. 'background.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました４。Err:timeout"));
                redirect("user/home");
            }
        }
        //$tmpbackground = file_get_contents($dir . 'background.json', true);
        $tmpbackground = json_decode($tmpbackground,true);
        $datas["background"] = $tmpbackground;

        if($tmpwork = @file_get_contents($dir. 'work.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました５。Err:timeout"));
                redirect("user/home");
            }
        }
        //$tmpwork = file_get_contents($dir . 'work.json', true);
        $tmpwork = json_decode($tmpwork,true);
        $datas["work"] = $tmpwork;

        if($tmparea = @file_get_contents($dir. 'area.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました６。Err:timeout"));
                redirect("user/home");
            }
        }
        //$tmparea = file_get_contents($dir. 'area.json', true);
        $tmparea = json_decode($tmparea,true);
        $datas["area"] = $tmparea;

        if($tmptenkin = @file_get_contents($dir. 'tenkin.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました７。Err:timeout"));
                redirect("user/home");
            }
        }
        //$tmptenkin = file_get_contents($dir. 'tenkin.json', true);
        $tmptenkin = json_decode($tmptenkin,true);
        $datas["tenkin"] = $tmptenkin;

        if($tmpcategory = @file_get_contents($dir. 'jobcategory.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました８。Err:timeout"));
                redirect("user/home");
            }
        }
        //$tmpcategory = file_get_contents($dir. 'jobcategory.json', true);
        $tmpcategory = json_decode($tmpcategory,true);
        $datas["jobcategory"] = $tmpcategory;


        if($tmpincome = @file_get_contents($dir. 'income.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました９。Err:timeout"));
                redirect("user/home");
            }
        }
        $tmpincome = json_decode($tmpincome,true);
        $datas["income"] = $tmpincome;

        if($tmptantoujob = @file_get_contents($dir. 'jobmypagecd.json', true)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"システムエラーが発生しました。Err:404"));
                        redirect("user/home");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("user/home");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("user/home");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"タイムアウトになりました１０。Err:timeout"));
                redirect("user/home");
            }
        }
        $tmptantoujob = json_decode($tmptantoujob,true);
        $datas["jobmypagecd"] = $tmptantoujob;

        return $datas;
    }

    //////////////////////////////////////////////////////////
    //HRBC基本情報設定
    //////////////////////////////////////////////////////////
    function setHrbcBasicData()
    {
        // リクエストを行うURLの指定
        $url = "https://api-hrbc-jp.porterscloud.com/v1/oauth";

        //送信パラメータ
        $param = array('app_id'=>'725d6f07ddaede98231a01f7783a78ef','response_type'=>'code_direct');

        $data = http_build_query($param);

        $unit_url = $url . "?" . $data;

        $opts = array(
            'http'=>array(
                'method' => 'GET'
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($unit_url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404"));
                        redirect("err/error_hrbc");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("err/error_hrbc");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("err/error_hrbc");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました１１。Err:timeout"));
                redirect("err/error_hrbc");
            }
        }
        //$xml = file_get_contents($unit_url, false, $context);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $jsonD = json_decode($json,true);
        $code = $jsonD["Code"];

        //////////////////////////////////////////////////////////////////////
        //// アクセストークン
        ////////////////////////////////////////////////////////////////////////

        $url = "https://api-hrbc-jp.porterscloud.com/v1/token";

        //送信パラメータ
        $param = array('code'=>$code, 'grant_type'=>'oauth_code','app_id'=>'725d6f07ddaede98231a01f7783a78ef','secret'=>'9ea5f52d29da05f55f9fee47cd0f55b9bba28ebe9c64d8d3a51bdc7a0d11713d');

        $data = http_build_query($param);

        $unit_url = $url . "?" . $data;

        $opts = array(
            'http'=>array(
                'method' => 'GET',
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($unit_url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404"));
                        redirect("err/error_hrbc");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("err/error_hrbc");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("err/error_hrbc");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout"));
                redirect("err/error_hrbc");
            }
        }
        //$xml = file_get_contents($unit_url, false, $context);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $jsonD = json_decode($json,true);
        $token = $jsonD['AccessToken'];

        //////////////////////////////////////////////////////////////////////
        //// パーティション
        ////////////////////////////////////////////////////////////////////////

        $url = "https://api-hrbc-jp.porterscloud.com/v1/partition";

        //送信パラメータ
        $param = array('request_type'=>1);

        $data = http_build_query($param);

        $unit_url = $url . "?" . $data;

        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: $token\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        //xml解析
        if($xml = @file_get_contents($unit_url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404"));
                        redirect("err/error_hrbc");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                        redirect("err/error_hrbc");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                        redirect("err/error_hrbc");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout"));
                redirect("err/error_hrbc");
            }
        }
        //$xml = file_get_contents($unit_url, false, $context);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $jsonD = json_decode($json,true);
        $partitionId = $jsonD['Item']['Partition.P_Id'];

        //////////////////////////////////////////////////////////////////////
        //// cache化
        ////////////////////////////////////////////////////////////////////////
        $arrs = array(
            "token" => $token,
            "partition" => $partitionId
        );

        $json = json_encode($arrs);


        $cachFile = BaseDir . "/../hrbc/token/cache.json";
        /*
        if(file_exists($cachFile)){
            unlink($cachFile);
        }
        */
        $fp = fopen($cachFile, "w" );
        fputs($fp, $json);
        fclose( $fp );
    }

	////////////////////////////////////////////////////////////////
	// ファイルアップロード
	////////////////////////////////////////////////////////////////
	function fileUpload($image, $cnt, $files, $dir, $require , $filename, $overwrite, $allow)
	{
		$result=array();
		$upDir = UploadDirectory . $dir;
		//ファイルアップロード
		if ( $files['userfile']['size'][$cnt] != 0){
			if (!file_exists($upDir)) {
				if ( !mkdir( $upDir ) ) {
					echo "ディレクトリ作成の権限がありません。<br>管理者に問い合わせてください。";
					exit;
				}
			}

			$_FILES['userfile']['name']= $files['userfile']['name'][$cnt];
			$_FILES['userfile']['type']= $files['userfile']['type'][$cnt];
			$_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$cnt];
			$_FILES['userfile']['error']= $files['userfile']['error'][$cnt];
			$_FILES['userfile']['size']= $files['userfile']['size'][$cnt];

			$fn = $_FILES["userfile"]['name'];
			$ext = substr($fn, strrpos($fn, '.') + 1);
			if(empty($filename)){
				$filename = date('Ymdhis') . "_" . mt_rand() . "." .$ext;
			}
			$config['upload_path'] = $upDir;
			$config['allowed_types'] = $allow;
			$config['max_size']	= '10000000';
			$config['file_name']	= $filename;
			$config['overwrite'] = $overwrite;

			$this->upload->initialize($config);

			if ( ! $this->upload->do_upload())
			{
				$result = array('error' => $this->upload->display_errors());
			}
			else
			{
				$result = array('upload_data' => $this->upload->data());
			}
		//ファイル変更なし
		}else if(!empty($image)){
			$result[] = $upDir . $image;
		}else{
			//$result[0] = "";
			if($require == 1){
				// エラー処理
				$result['error'] = "ファイルを選択して下さい。";
			}else{
				$result="";
			}
		}

		return $result;
	}

	//////////////////////////////////////////////////////////
	//ソート
	//////////////////////////////////////////////////////////
	function sortChange($id, $category_id, $sort, $segment, $database)
	{
		if(empty($id)){
			$this->session->set_userdata(array("msg"=>"不正な処理が行われました。エラーコード９"));
			redirect($segment . "/home/".$category_id);
		}
		if(empty($category_id)){
			$this->session->set_userdata(array("msg"=>"不正な処理が行われました。エラーコード１０"));
			redirect($segment ."/home/".$category_id);
		}
		if(empty($sort)){
			$this->session->set_userdata(array("msg"=>"不正な処理が行われました。エラーコード１２"));
			redirect($segment . "/home/".$category_id);
		}

		//現在のソート番号取得
		$data = $this->db->get_where($database, array('id' => $id, 'del'=>0))->row();
		$nowSort = $data->sort;
	
		//ソートアップかダウンから対象データ取得
		if($sort=="down"){
			$data2 = $this->db->order_by('sort asc')->get_where($database, array("del"=>0, "category_id"=>$category_id, "sort >" =>  $nowSort));

			if(empty($data2->num_rows())){
				$this->session->set_userdata(array("msg"=>"対象のデータが存在しません。エラーコード１３"));
				redirect($segment . "/home/".$category_id);
			}

			//データの入れ替え（前後を逆にする）
			$update_param1 = array('sort' => $data2->result('array')[0]['sort']);
			$this->db->where('id', $id);
			$this->db->update($database, $update_param1); 

			$update_param2 = array('sort' => $nowSort);
			$this->db->where('id', $data2->result('array')[0]['id']);
			$this->db->update($database, $update_param2); 

		}else if($sort=="up"){
			$down = $nowSort - 1;
			$data2 = $this->db->order_by('sort desc')->get_where($database, array("del"=>0, "category_id"=>$category_id, "sort <" => $nowSort));

			if(empty($data2->num_rows())){
				$this->session->set_userdata(array("msg"=>"対象のデータが存在しません。エラーコード１４"));
				redirect($segment . "/home/".$category_id);
			}

			//データの入れ替え（前後を逆にする）
			$update_param1 = array('sort' => $data2->result('array')[0]['sort']);
			$this->db->where('id', $id);
			$this->db->update($database, $update_param1); 

			$update_param2 = array('sort' => $nowSort);
			$this->db->where('id', $data2->result('array')[0]['id']);
			$this->db->update($database, $update_param2); 
		}

		$this->session->set_userdata(array("msg"=>"並び替えが完了しました。"));
		redirect($segment."/home/".$category_id);
	}

	//////////////////////////////////////////////////////////
	//カテゴリーソート
	//////////////////////////////////////////////////////////
	function category_sortChange($id, $sort, $segment, $database)
	{
		if(empty($id)){
			$this->session->set_userdata(array("msg"=>"不正な処理が行われました。エラーコード９"));
			redirect($segment . "/home/");
		}
		if(empty($sort)){
			$this->session->set_userdata(array("msg"=>"不正な処理が行われました。エラーコード１２"));
			redirect($segment . "/home/");
		}

		//現在のソート番号取得
		$data = $this->db->get_where($database, array('category_id' => $id, 'category_del'=>0))->row();
		$nowSort = $data->category_sort;
	
		//ソートアップかダウンから対象データ取得
		if($sort=="down"){
			$data2 = $this->db->order_by('category_sort asc')->get_where($database, array("category_del"=>0, "category_sort >" =>  $nowSort));

			if(empty($data2->num_rows())){
				$this->session->set_userdata(array("msg"=>"対象のデータが存在しません。エラーコード１３"));
				redirect($segment . "/home/");
			}

			//データの入れ替え（前後を逆にする）
			$update_param1 = array('category_sort' => $data2->result('array')[0]['category_sort']);
			$this->db->where('category_id', $id);
			$this->db->update($database, $update_param1); 

			$update_param2 = array('category_sort' => $nowSort);
			$this->db->where('category_id', $data2->result('array')[0]['category_id']);
			$this->db->update($database, $update_param2); 

		}else if($sort=="up"){
			$down = $nowSort - 1;
			$data2 = $this->db->order_by('category_sort desc')->get_where($database, array("category_del"=>0,  "category_sort <" => $nowSort));

			if(empty($data2->num_rows())){
				$this->session->set_userdata(array("msg"=>"対象のデータが存在しません。エラーコード１４"));
				redirect($segment . "/home/");
			}

			//データの入れ替え（前後を逆にする）
			$update_param1 = array('category_sort' => $data2->result('array')[0]['category_sort']);
			$this->db->where('category_id', $id);
			$this->db->update($database, $update_param1); 

			$update_param2 = array('category_sort' => $nowSort);
			$this->db->where('category_id', $data2->result('array')[0]['category_id']);
			$this->db->update($database, $update_param2); 
		}

		$this->session->set_userdata(array("msg"=>"並び替えが完了しました。"));
		redirect($segment."/home/");
	}

    ////////////////////////////////////////////////////////////////////////////////////////////
    //デバイス判定　PC SP
    ////////////////////////////////////////////////////////////////////////////////////////////
    function getDevice(){
        $ua = $_SERVER['HTTP_USER_AGENT'];// ユーザエージェントを取得
        if((strpos($ua, 'iPhone') !== false) || (strpos($ua, 'iPod') !== false) ||(strpos($ua, 'Android') !== false)){
            $agent = "SP";
        }else{
            $agent = "PC";
        }
        return $agent;
    }

	////////////////////////////////////////////////////////////////
	// リサイズ
	////////////////////////////////////////////////////////////////
	function resizeImage($source_image, $width, $height, $thumb_marker, $master_dim)
	{
		$config['source_image']	= $source_image;
		$config['create_thumb'] = TRUE;
		$config['maintain_ratio'] = TRUE;
		$config['width']	= $width;
		$config['height']	= $height;
		$config['thumb_marker']	= $thumb_marker;
		$config['master_dim']	= $master_dim;

		$this->load->library('image_lib', $config); 

		$this->image_lib->initialize($config);
		$ret = $this->image_lib->resize();
		unset($config);
		$this->image_lib->clear();
	}
}
