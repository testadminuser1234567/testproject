<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Maintenance extends MY_Controller {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * 404ページ
     */
    public function home() {
        $data = array();
 
        /**
         * データベースから値を取得する等、
         * 必要な処理を記述してください。
         */
        $return = $this->db->get_where("maintenances", array('id' => 1))->row();
        $data["start"] = $return->start;
        $data["end"] = $return->end;

        $data["render"] = "maintenance/home";
        $this->load->view("template_nologin", $data);
    }
 
}