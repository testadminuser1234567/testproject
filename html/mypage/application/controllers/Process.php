<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Process extends MY_Controller
{

    protected $page_limit = 10000000;

    //タイトル
    protected $sub_title = "プロセス";

    //segment
    protected $segment = "process";

    //database
    protected $database = "members";
    protected $databaseJob = "jobs";
    protected $databaseClient = "clients";

    //画像がonの時 許可ファイル
    protected $allow = 'doc|xls|ppt|pptx|pdf|docx|xlsx|html|txt|htm';

    //SNSのURL
    protected $fbloginUrl = "";
    protected $googleUrl = "";

    //HRBC URL
    protected $hrbcUrl = "https://api-hrbc-jp.porterscloud.com/v1/";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('form_validation', 'encrypt'));
        $this->load->helper(array('url'));
    }

    // redirect if needed, otherwise display the user list
    function index()
    {
        $this->home();
    }

    //////////////////////////////////////////////////////////
    //一覧出力
    //////////////////////////////////////////////////////////
    function home()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        //対象のID取得
        $ret = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $person_id = $ret->hrbc_person_id;
        $resume_id = $ret->hrbc_resume_id;

        $posts = $this->input->post();

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $this->load->library("pagination");

        $param = array("del" => 0, "id" => $login_id);

        ////////////////////////////////////////////////////////////////////////////////////////////
        //HRBCデータ取得
        ////////////////////////////////////////////////////////////////////////////////////////////
        $result = $this->getHrbcMaster();
        $data = array();

        ////////////////////////////////////////////////////////////////////////////////////////////
        //Process
        ////////////////////////////////////////////////////////////////////////////////////////////
        //Option.U_001311 マイページ反映のデータを抽出
        /*
        $param = http_build_query(array('partition' => $result["partition"], 'count' => 200, 'start' => 0, 'condition' => 'Process.P_Resume:eq=' . $resume_id . '' , 'field' => 'Process.P_Id,Process.P_Job,Process.P_Owner,Process.P_Recruiter,Process.P_Resume, Process.U_4E29163A766BC0D6CA7D9B1157A614,Process.P_Phase,Process.P_PhaseMemo,Process.P_Close,Process.P_PhaseDate,Process.P_Client,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.P_UpdateDate,Process.U_186596AC430DB9AF87A142A6A47DC7,Process.U_419EC7AD9BE7E1A377DF8B6D0115A1,Process.U_EFEE750C9B6F3D74F06F8E821E1C79,Process.U_1C50E7D9F0AD650BF5159E1BE0DF6D,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.U_5D69124062C230BB6DEC5F4D812C57,Process.U_C10D749E15C59A371D293282096F5B,Process.U_3E781B18BA915F475A1FA174CAAD69,Process.P_UpdateDate', 'order' => 'Process.P_PhaseDate:desc'));
//        $param = http_build_query(array('partition' => $result["partition"], 'count' => 200, 'start' => 0, 'condition' => 'Process.P_Resume:eq=' . $resume_id . ',Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11:or=Option.U_001311' , 'field' => 'Process.P_Id,Process.P_Job,Process.P_Owner,Process.P_Recruiter,Process.P_Resume, Process.U_4E29163A766BC0D6CA7D9B1157A614,Process.P_Phase,Process.P_PhaseMemo,Process.P_Close,Process.P_PhaseDate,Process.P_Client,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.P_UpdateDate,Process.U_186596AC430DB9AF87A142A6A47DC7,Process.U_419EC7AD9BE7E1A377DF8B6D0115A1,Process.U_EFEE750C9B6F3D74F06F8E821E1C79,Process.U_1C50E7D9F0AD650BF5159E1BE0DF6D,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.U_5D69124062C230BB6DEC5F4D812C57,Process.U_C10D749E15C59A371D293282096F5B,Process.U_3E781B18BA915F475A1FA174CAAD69,Process.P_UpdateDate', 'order' => 'Process.P_PhaseDate:desc'));
        $url = $this->hrbcUrl . "process?" . $param;
        $opts = array(
            'http' => array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 process"));
                        redirect($this->segment."/home/");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 process"));
                        redirect($this->segment."/home/");
                        break;

                    //その他のエラーの場合
                    default:
                        //$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other process"));
                        //redirect($this->segment."/home/");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout process"));
                redirect($this->segment."/home/");
            }
        }
        $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "process?partition=". $result["partition"] . "&count=200&start=0&field=Process.P_Id,Process.P_Job,Process.P_Owner,Process.P_Recruiter,Process.P_Resume,Process.U_4E29163A766BC0D6CA7D9B1157A614,Process.P_Phase,Process.P_PhaseMemo,Process.P_Close,Process.P_PhaseDate,Process.P_Client,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.P_UpdateDate,Process.U_186596AC430DB9AF87A142A6A47DC7,Process.U_419EC7AD9BE7E1A377DF8B6D0115A1,Process.U_EFEE750C9B6F3D74F06F8E821E1C79,Process.U_1C50E7D9F0AD650BF5159E1BE0DF6D,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.U_5D69124062C230BB6DEC5F4D812C57,Process.U_C10D749E15C59A371D293282096F5B,Process.U_3E781B18BA915F475A1FA174CAAD69,Process.P_UpdateDate&condition=Process.P_Resume:eq=".$resume_id."&order=Process.P_PhaseDate:desc";

        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

        if ($xml->Code != 0) {
            $pErr = $xml->Code;
            //print("抽出処理でエラーが発生しました。<br />コード：" . $pErr);
            $subject = "【code:process1】" . $pErr;
            $message = $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404/");

            //exit;
        }

        //HRBCから帰ってきたデータ件数　1件の対応用に取得
        $cnt = $xml->attributes()->Count;

        //データ設定
        $json = json_encode($xml);
        $processes = json_decode($json, true);

        $tmp=array();
        $i = 0;
        if ($cnt == 0) {
        } else if ($cnt != 1) {
            //複数データ
            foreach ($processes["Item"] as $val) {
                if(empty($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"][0]["Option.U_001312"])){
                    foreach ($val["Process.P_Phase"] as $k => $v) {
                        $opname = $v["Option.P_Name"];
                        $opId = $v["Option.P_Id"];
                        if ($opname == "求人詳細問合せ") {
                            $phaze = "求人詳細問合せ中";
                            $phazeFlg = 0;
                            $clientNameDisplay = "非公開";
                            /* 20180702 第２ローンチで削除
                        } else if ($opname == "WEBエントリー") {
                            $phaze = "応募中";
                            $phazeFlg = 0;
                            $clientNameDisplay = "非公開";
                            */
                        } else if ($opname == "JOB打診" || $opname == "JOB打診（弱）" || $opname == "JOB打診【強！】") {
                            $phaze = "オファー";
                            $phazeFlg = 1;
                            $clientNameDisplay = "公開";
                            /* 20180702 第２ローンチで削除
                        } else if ($opname == "JOB打診（社名非公開）") {
                            $phaze = "オファー";
                            $phazeFlg = 2;
                            $clientNameDisplay = "非公開";
                            */
                        } else if ($opname == "本人NG" || $opname == "内定辞退" || $opname=="面接辞退") {
                            $phaze = "辞退";
                            $phazeFlg = 0;
                            $clientNameDisplay = "公開";
                            /* 20180702 第２ローンチで削除
                        } else if ($opname == "本人NG（社名非公開）") {
                            $phaze = "辞退";
                            $phazeFlg = 0;
                            $clientNameDisplay = "非公開";
                            */
                            /* 20180702 第２ローンチで「応募承諾（書類待ち）、社内NG予定削除
                        } else if ($opname == "応募承諾" || $opname == "応募承諾（書類待ち）"  || $opname == "書類推薦" || $opname == "社内NG予定") {
                            */
                        } else if ($opname == "応募承諾"  || $opname == "応募承諾（書類待ち）"  || $opname == "書類推薦") {
                            $phaze = "応募中";
                            $phazeFlg = 0;
                            $clientNameDisplay = "公開";
                            /* 20180702 第２ローンチで削除
                        } else if ($opname == "社内NG予定（社名非公開）") {
                            $phaze = "応募中";
                            $phazeFlg = 0;
                            $clientNameDisplay = "非公開";
                            */
                        } else if ($opname == "社内NG" || $opname == "書類NG" || $opname == "面接NG") {
                            $phaze = "見送り";
                            $phazeFlg = 0;
                            $clientNameDisplay = "公開";
                            /* 20180702 第２ローンチ「社内NG（社名非公開）」で削除
                        } else if ($opname == "社内NG（社名非公開）" || $opname == "JM対応用 社内NG（社名非公開）") {
                            */
                        } else if ($opname == "JM対応用 社内NG（社名非公開）" || $opname == "社内NG（社名非公開）") {
                            $phaze = "見送り";
                            $phazeFlg = 0;
                            $clientNameDisplay = "非公開";
                            /* 20180702 第２ローンチ「面接（一次）OK」、「面接（二次）OK」、「面接（三次）OK」削除
                        } else if ($opname == "書類OK" || $opname == "面接（一次）" || $opname == "面接（一次）OK" || $opname == "面接（二次）" || $opname == "面接（二次）OK" || $opname == "面接（三次）" || $opname == "面接（三次）OK" || $opname == "面接（最終）") {
                            */
                        } else if ($opname == "書類OK" || $opname == "面接（一次）" || $opname == "面接（二次）" || $opname == "面接（三次）" || $opname == "面接（最終）") {
                            $phaze = "面接";
                            $phazeFlg = 0;
                            $clientNameDisplay = "公開";
                        } else if ($opname == "内定" || $opname=="入社") {
                            $phaze = "内定";
                            $phazeFlg = 0;
                            $clientNameDisplay = "公開";
                            /* 20180702 第２ローンチで「入社予定」削除　入社が内定
                        } else if ($opname == "入社予定" || $opname == "入社") {
                        } else if ( $opname == "入社") {
                            $phaze = "ご入社決定";
                            $phazeFlg = 0;
                            $clientNameDisplay = "公開";
                            */
                            /* 20180702 第２ローンチで追加 */
                        } else if ( $opname == "クローズ" || $opname == "JOBクローズ" || $opname == "期限切れ処理") {
                            $phaze = "募集終了";
                            $phazeFlg = 0;
                            $clientNameDisplay = "公開";
                            /* 20180702 第２ローンチで「入社予定」削除
                        } else if ($opname == "JOBクローズ" || $opname == "期限切れ処理") {
                            $phaze = "案件終了";
                            $phazeFlg = 0;
                            $clientNameDisplay = "公開";
                        } else if ($opname == "JOBクローズ（社名非公開）" || $opname == "期限切れ処理（社名非公開）") {
                            $phaze = "案件終了";
                            $phazeFlg = 0;
                            $clientNameDisplay = "非公開";
                            */
                        } else {
                            $phaze = "";
                            $clientNameDisplay = "非公開";
                        }
                    }

                    $jobId = $val["Process.P_Job"]["Job"]["Job.P_Id"];
                    $clientId = $val["Process.P_Client"]["Client"]["Client.P_Id"];
                    if (!empty($phaze)) {
                        //ジョブと企業の設定
                        $job = $this->db->get_where($this->databaseJob, array('job_id' => $jobId))->row();
                        $client = $this->db->get_where($this->databaseClient, array('c_id' => $clientId))->row();
                        $job = (array)$job;
                        $client = (array)$client;

                        //職種の設定
                        if($json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                            //ここにデータ取得が成功した時の処理
                        }else{
                            //エラー処理
                            if(count($http_response_header) > 0){
                                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                                //エラーの判別
                                switch($status_code[1]){
                                    //404エラーの場合
                                    case 404:
                                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 home2"));
                                        redirect($this->segment."/home/");
                                        break;

                                    //500エラーの場合
                                    case 500:
                                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 home2"));
                                        redirect($this->segment."/home/");
                                        break;

                                    //その他のエラーの場合
                                    default:
                                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other home2"));
                                        redirect($this->segment."/home/");
                                }
                            }else{
                                //タイムアウトの場合 or 存在しないドメインだった場合
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout home2"));
                                redirect($this->segment."/home/");
                            }
                        }
                        $tmpJobcategory = json_decode($json2, true);
                        $jobCategoryName = "";
                        $jobCategoryId = "";
                        foreach ($tmpJobcategory['Item'] as $jck => $jcv) {
                            if (!empty($jcv["Item"])) {
                                foreach ($jcv['Item'] as $jck2 => $jcv2) {
                                    foreach ($jcv2['Items'] as $jck3 => $jcv3) {
                                        foreach ($jcv3 as $jck4 => $jcv4) {
                                            if ($job["jobcategory_id"] == $jcv4['Option.P_Id']) {
                                                $jobCategoryName = htmlspecialchars($jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                                $jobCategoryId = htmlspecialchars($jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //マイページに反映するチェック
                        $mypageCheck = "";
                        $mypageText = "";
                        if (!empty($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                            foreach ($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $mk => $mv) {
                                foreach($mv as $mk2 => $mv2){
                                    if(!empty($mv2)){
                                        $mypageCheck = $mv2["Option.P_Id"];
                                        $mypageText = $mv2["Option.P_Name"];
                                    }
                                }
                            }
                        }

                        //企業名表示/非表示
                        /*
                        $clientNameCheck = "";
                        $clientNameDisplay = "";
                        if (!empty($val["Process.U_C10D749E15C59A371D293282096F5B"])) {
                            foreach ($val["Process.U_C10D749E15C59A371D293282096F5B"] as $mk => $mv) {
                                if (!empty($mv)) {
                                    $clientNameCheck = $mv["Option.P_Id"];
                                    $clientNameDisplay = $mv["Option.P_Name"];
                                }
                            }
                        }
                        */

                        $tmp["process"][$i]["processId"] = $val["Process.P_Id"];
                        $tmp["process"][$i]["mypageCheck"] = $mypageCheck;
                        $tmp["process"][$i]["clientNameDisplay"] = $clientNameDisplay;
                        $tmp["process"][$i]["mypageText"] = $mypageText;
                        $tmp["process"][$i]["phaze"] = $phaze;
                        $tmp["process"][$i]["opname"] = $opname;
                        $tmp["process"][$i]["phazeId"] = $opId;
                        $tmp["process"][$i]["phazeFlg"] = $phazeFlg;
                        $tmp["process"][$i]["job"] = $job;
                        $tmp["process"][$i]["client"] = $client;
                        $tmpdate = "";
                        if ($val["Process.P_PhaseDate"]) {
                            $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_PhaseDate"])));
                        }
                        $tmp["process"][$i]["phasedate"] = $tmpdate;
                        $tmp["process"][$i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_UpdateDate"][0])));
                        $tmp["process"][$i]["jobcategoryName"] = $jobCategoryName;
                        $tmp["process"][$i]["jobcategoryId"] = $jobCategoryId;
                        $i++;
                    }
                }
            }
            //1件のデータは返却の形式が異なるので個別対応
        } else {
            foreach ($processes["Item"]["Process.P_Phase"] as $k => $v) {
                if(empty($v["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"][0]["Option.U_001312"])){
                    $opname = $v["Option.P_Name"];
                    $opId = $v["Option.P_Id"];
                    if ($opname == "求人詳細問合せ") {
                        $phaze = "求人詳細問合せ中";
                        $phazeFlg = 0;
                        $clientNameDisplay = "非公開";
                        /* 20180702 第２ローンチで削除
                    } else if ($opname == "WEBエントリー") {
                        $phaze = "応募中";
                        $phazeFlg = 0;
                        $clientNameDisplay = "非公開";
                        */
                    } else if ($opname == "JOB打診" || $opname == "JOB打診（弱）" || $opname == "JOB打診【強！】") {
                        $phaze = "オファー";
                        $phazeFlg = 1;
                        $clientNameDisplay = "公開";
                        /* 20180702 第２ローンチで削除
                    } else if ($opname == "JOB打診（社名非公開）") {
                        $phaze = "オファー";
                        $phazeFlg = 2;
                        $clientNameDisplay = "非公開";
                        */
                    } else if ($opname == "本人NG" || $opname == "内定辞退" || $opname=="面接辞退") {
                        $phaze = "辞退";
                        $phazeFlg = 0;
                        $clientNameDisplay = "公開";
                        /* 20180702 第２ローンチで削除
                    } else if ($opname == "本人NG（社名非公開）") {
                        $phaze = "辞退";
                        $phazeFlg = 0;
                        $clientNameDisplay = "非公開";
                        */
                        /* 20180702 第２ローンチで「応募承諾（書類待ち）、社内NG予定削除
                    } else if ($opname == "応募承諾" || $opname == "応募承諾（書類待ち）"  || $opname == "書類推薦" || $opname == "社内NG予定") {
                        */
                    } else if ($opname == "応募承諾"   || $opname == "応募承諾（書類待ち）" || $opname == "書類推薦") {
                        $phaze = "応募中";
                        $phazeFlg = 0;
                        $clientNameDisplay = "公開";
                        /* 20180702 第２ローンチで削除
                    } else if ($opname == "社内NG予定（社名非公開）") {
                        $phaze = "応募中";
                        $phazeFlg = 0;
                        $clientNameDisplay = "非公開";
                        */
                    } else if ($opname == "社内NG" || $opname == "書類NG" || $opname == "面接NG") {
                        $phaze = "見送り";
                        $phazeFlg = 0;
                        $clientNameDisplay = "公開";
                        /* 20180702 第２ローンチ「社内NG（社名非公開）」で削除
                    } else if ($opname == "社内NG（社名非公開）" || $opname == "JM対応用 社内NG（社名非公開）") {
                        */
                    } else if ($opname == "JM対応用 社内NG（社名非公開）" || $opname == "社内NG（社名非公開）") {
                        $phaze = "見送り";
                        $phazeFlg = 0;
                        $clientNameDisplay = "非公開";
                        /* 20180702 第２ローンチ「面接（一次）OK」、「面接（二次）OK」、「面接（三次）OK」削除
                    } else if ($opname == "書類OK" || $opname == "面接（一次）" || $opname == "面接（一次）OK" || $opname == "面接（二次）" || $opname == "面接（二次）OK" || $opname == "面接（三次）" || $opname == "面接（三次）OK" || $opname == "面接（最終）") {
                        */
                    } else if ($opname == "書類OK" || $opname == "面接（一次）" || $opname == "面接（二次）" || $opname == "面接（三次）" || $opname == "面接（最終）") {
                        $phaze = "面接";
                        $phazeFlg = 0;
                        $clientNameDisplay = "公開";
                    } else if ($opname == "内定" || $opname=="入社") {
                        $phaze = "内定";
                        $phazeFlg = 0;
                        $clientNameDisplay = "公開";
                        /* 20180702 第２ローンチで「入社予定」削除　入社を内定に入れる
                    } else if ($opname == "入社予定" || $opname == "入社") {
                    } else if ( $opname == "入社") {
                        $phaze = "ご入社決定";
                        $phazeFlg = 0;
                        $clientNameDisplay = "公開";
                        */
                        /* 20180702 第２ローンチで追加 */
                    } else if ( $opname == "クローズ" || $opname == "JOBクローズ" || $opname == "期限切れ処理") {
                        $phaze = "募集終了";
                        $phazeFlg = 0;
                        $clientNameDisplay = "公開";
                        /* 20180702 第２ローンチで「入社予定」削除
                    } else if ($opname == "JOBクローズ" || $opname == "期限切れ処理") {
                        $phaze = "案件終了";
                        $phazeFlg = 0;
                        $clientNameDisplay = "公開";
                    } else if ($opname == "JOBクローズ（社名非公開）" || $opname == "期限切れ処理（社名非公開）") {
                        $phaze = "案件終了";
                        $phazeFlg = 0;
                        $clientNameDisplay = "非公開";
                        */
                    } else {
                        $phaze = "";
                        $clientNameDisplay = "非公開";
                    }
                }
                $jobId = $processes["Item"]["Process.P_Job"]["Job"]["Job.P_Id"];
                $clientId = $processes["Item"]["Process.P_Client"]["Client"]["Client.P_Id"];
                if (!empty($phaze)) {

                    //ジョブと企業の設定
                    $job = $this->db->get_where($this->databaseJob, array('job_id' => $jobId))->row();
                    $client = $this->db->get_where($this->databaseClient, array('c_id' => $clientId))->row();
                    $job = (array)$job;
                    $client = (array)$client;

                    //職種の設定
                    if($json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                        //ここにデータ取得が成功した時の処理
                    }else{
                        //エラー処理
                        if(count($http_response_header) > 0){
                            //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                            $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                            //エラーの判別
                            switch($status_code[1]){
                                //404エラーの場合
                                case 404:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 home3"));
                                    redirect($this->segment."/home/");
                                    break;

                                //500エラーの場合
                                case 500:
                                    $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 home3"));
                                    redirect($this->segment."/home/");
                                    break;

                                //その他のエラーの場合
                                default:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other home3"));
                                    redirect($this->segment."/home/");
                            }
                        }else{
                            //タイムアウトの場合 or 存在しないドメインだった場合
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout home3"));
                            redirect($this->segment."/home/");
                        }
                    }
                    $tmpJobcategory = json_decode($json2, true);
                    $jobCategoryName = "";
                    $jobCategoryId = "";
                    if (!empty($tmpJobcategory["Item"])) {
                        foreach ($tmpJobcategory['Item'] as $jck => $jcv) {
                            if (!empty($jcv["Item"])) {
                                foreach ($jcv['Item'] as $jck2 => $jcv2) {
                                    foreach ($jcv2['Items'] as $jck3 => $jcv3) {
                                        foreach ($jcv3 as $jck4 => $jcv4) {
                                            if ($job["jobcategory_id"] == $jcv4['Option.P_Id']) {
                                                $jobCategoryName = htmlspecialchars($jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                                $jobCategoryId = htmlspecialchars($jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //マイページに反映するチェック
                    $mypageCheck = "";
                    $mypageText = "";
                    if (!empty($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                        foreach ($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $mk => $mv) {
                            if (!empty($mv)) {
                                foreach ($mv as $mk2 => $mv2) {
                                    $mypageCheck = $mv2["Option.P_Id"];
                                    $mypageText = $mv2["Option.P_Name"];
                                }
                            }
                        }
                    }

                    //企業名表示/非表示
                    /*
                    $clientNameCheck = "";
                    $clientName = "";
                    $clientNameDisplay="";
                    if (!empty($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"])) {
                        foreach ($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"] as $mk => $mv) {
                            if (!empty($mv)) {
                                    $clientNameCheck = $mv["Option.P_Id"];
                                    $clientNameDisplay = $mv["Option.P_Name"];
                            }
                        }
                    }
                    */

                    $tmp["process"][$i]["processId"] = $processes["Item"]["Process.P_Id"];
                    $tmp["process"][$i]["mypageCheck"] = $mypageCheck;
                    $tmp["process"][$i]["clientNameDisplay"] = $clientNameDisplay;
                    $tmp["process"][$i]["mypageText"] = $mypageText;
                    $tmp["process"][$i]["phaze"] = $phaze;
                    $tmp["process"][$i]["opname"] = $opname;
                    $tmp["process"][$i]["phazeId"] = $opId;
                    $tmp["process"][$i]["phazeFlg"] = $phazeFlg;
                    $tmp["process"][$i]["job"] = $job;
                    $tmp["process"][$i]["client"] = $client;
                    $tmpdate="";
                    if(!empty($processes["Item"]["Process.P_PhaseDate"])){
                        $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_PhaseDate"])));
                    }
                    $tmp["process"][$i]["phasedate"] = $tmpdate;
                    $tmp["process"][$i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_UpdateDate"][0])));
                    $tmp["process"][$i]["jobcategoryName"] = $jobCategoryName;
                    $tmp["process"][$i]["jobcategoryId"] = $jobCategoryId;
                    $i++;
                }
            }
        }

        //ソート処理
        $sort="";
        $page_sort="";
        if($this->uri->segment(3)){
            $sort = $this->uri->segment(3);
            /*
            $sort = urldecode($sort);
            $page_sort = "/" . $this->uri->segment(3);

            $tmp_datas=array();
            if(!empty($tmp["process"])){
                foreach ($tmp["process"] as $k => $v) {
                    $sort_phase = $v["phaze"];
                    if ($sort_phase == $sort) {
                        $tmp_datas[] = $v;
                    }
                }
            }
            $data["process"] = $tmp_datas;
            */
            $data["process"] = $tmp["process"];
            $data["sort"] = $sort;
        }else{
            if(!empty($tmp)){
                $data["process"] = $tmp["process"];
            }else{
                $data["process"] = array();
            }
        }


        //ページネーション用
        $total = 0;
        $offset=0;
        $paging="";
        $total = count($data["process"]);

        //pagination
        $config['base_url'] = base_url() . $this->uri->segment(1) . "/" . $this->uri->segment(2) . $page_sort;
        $config["total_rows"] = $total;
        $config["per_page"] = $this->page_limit;
        $config["num_links"] = 2;
        $config['full_tag_open'] = '<ul class="clearfix">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><span class="current">';
        $config['cur_tag_close'] = '</span></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['query_string_segment'] ="p";
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $paging = $this->pagination->create_links();

        if(!empty($_GET["p"])){
            $offset = $_GET["p"];
        }
        if(empty($offset)){
            $offset = 0;
        }else if(!is_numeric($offset)){
            $offset = 0;
        }

        //件数範囲用
        $offset = $offset + 1;
        $hani = $offset + $this->page_limit - 1;
        if($hani >= $total){
            $hani = $total;
        }

        $data["processes"]=array();
        if(!empty($data["process"])){
            for($i=$offset-1;$i<$hani;$i++){
                $data["processes"][$i] = $data["process"][$i];
            }
        }

        if(!empty($data["processes"])){
            foreach($data["processes"] as  $k => $v){
                //マイページに反映のデータのみ抽出
                if($v["mypageCheck"]!=11312){
                    if($v["phaze"]=="オファー"){
                        $data["offer"][] = $v;
                    }else if($v["phaze"]=="求人詳細問合せ中"){
                        $data["contact"][] = $v;
                    }else if($v["phaze"]=="辞退"){
                        $data["refuse"][] = $v;
                    }else if($v["phaze"]=="応募中"){
                        $data["apply"][] = $v;
                    }else if($v["phaze"]=="見送り"){
                        $data["sendoff"][] = $v;
                    }else if($v["phaze"]=="面接"){
                        $data["selection"][] = $v;
                    }else if($v["phaze"]=="内定"){
                        $data["appointment"][] = $v;
                    }else if($v["phaze"]=="ご入社決定"){
                        $data["decision"][] = $v;
                    }else if($v["phaze"]=="募集終了"){
                        $data["finish"][] = $v;
                    }
                }
            }
        }

        //フェーズオープンで自社サイトへの公開が非公開のデータ抽出（ＪＯＢ打診のものだけＪＯＢに表示）
        $p_job_id = "";
        $taishoku="";
        if(!empty($processes["Item"])){
            foreach ($processes["Item"] as $pk => $pv){
                foreach ($pv["Process.P_Phase"] as $pk2 => $pv2) {
                    $opname = $pv2["Option.P_Name"];
                    $opId = $pv2["Option.P_Id"];

                    //退職は非表示
                    if ($opname == "退職") {
                        $taishoku .= $pv["Process.P_Job"]["Job"]["Job.P_Id"] . ",";
                    }
                    //if ($opname == "JOB打診" || $opname == "JOB打診（弱）" || $opname == "JOB打診【強！】" || $opname == "JOB打診（社名非公開）") {
                    $p_job_id .= $pv["Process.P_Job"]["Job"]["Job.P_Id"] . ",";
                    //}
                }
//                $p_job_id .= $pv["Process.P_Job"]["Job"]["Job.P_Id"] . ",";
            }
        }
        $p_job_id = rtrim($p_job_id,",");
        $taishoku = rtrim($taishoku,",");


        ////////////////////////////////////////////////////////////////////
        //// 閲覧履歴
        //////////////////////////////////////////////////////////////////////
        $looks = $this->getLooks($login_id);
        $data["looks"] = $looks;

        //data設定
        $data["render"] = $this->segment . "/home";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["paging"] = $paging;
        $data["total"] = $total;
        $data["offset"] = $offset;
        $data["hani"] = $hani;
        $data["login_id"] = $login_id;
        $data["segment"] = $this->segment;
        $data["phaze"] = HrbcPhaze;
        $data["publish"] = HrbcPublish;
        $data["closeJob"] = HrbcCloseJob;
        $data["sort"] = $sort;
        $data["body_id"] = "list";


        $this->load->view("template_search", $data);
    }

    //////////////////////////////////////////////////////////
    //JOB打診からの申し込み
    //////////////////////////////////////////////////////////
    function apply()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $posts = $this->input->post();

        $jobid = $this->input->post("job_id");
        $clientid = $this->input->post("client_id");
        $clientname = $this->input->post("client_name");

        //job情報の取得
        $esc_jobid = $this->db->escape($jobid);
        $sql = "SELECT j.job_id, j.job_title,j.recruiter_id, j.owner_id, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = ". HrbcPhaze . " and j.job_id = " . $esc_jobid . " group by j.job_id";

        $jobs = $this->db->query($sql);
        $job = $jobs->result()[0];

        //job担当取得
        $tmpjobowner = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobowner.json', true);
        $tmpjobowner = json_decode($tmpjobowner,true);

        $jobowner_mail = "";
        $jobowner_flg = 0;
        if(!empty($tmpjobowner)){
            foreach ($tmpjobowner as $k => $v) {
                if(!empty($v)){
                    foreach($v as $k2 => $v2){
                        if(!empty($v2) && is_array($v2)){
                            if(!empty($v2["User.P_Id"]) && $v2["User.P_Id"]==$job->owner_id){
                                $jobowner_mail = $v2["User.P_Mail"];
                            }
                        }
                    }
                }
            }
        }
        if(empty($jobowner_mail)){
            $jobowner_flg = 1;
        }

        //User情報取得　Person取得のため
        $esc_userid = $this->db->escape($login_id);
        $sql = "SELECT * From members where del = 0 and id = " . $esc_userid;

        $users = $this->db->query($sql);
        $user = $users->result()[0];
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        ////////////////////////////////////////////////////////////////////////////////////////////
        //デバイス判定　PC SP
        ////////////////////////////////////////////////////////////////////////////////////////////
        $agent = $this->getDevice();

        ////////////////////////////////////////////////////////////////////////////////////////////
        //Processアップデート
        ////////////////////////////////////////////////////////////////////////////////////////////
        $result = $this->getHrbcMaster();
        $data = array();

        $url = $this->hrbcUrl . "process?partition=" . $result["partition"];
//        $now = date('Y/m/d H:i:s');
        $tmpDate = date("Y/m/d H:i:s", strtotime($posts["phasedate"] . "-9 hour"));
//        $now = date("Y/m/d H:i:s", strtotime($tmpDate . "+1 second"));
        $now = date("Y/m/d H:i:s",strtotime("-9 hour"));

        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>' . $posts["processId"] . '</Process.P_Id><Process.P_Phase><Option.U_001122 /></Process.P_Phase><Process.P_PhaseDate>' . $now . '</Process.P_PhaseDate></Item></Process>';
        $tmp_xml = $xml;

        /*
        $header = array(
            "Content-Type: application/xml; charset=UTF-8",
            "X-porters-hrbc-oauth-token: " . $result['token']
        );

        $options = array('http' => array(
            'method' => 'POST',
            'content' => $xml,
            'header' => implode("\r\n", $header)
        ));

        if($xml = @file_get_contents($url, false, stream_context_create($options))){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 apply"));
                        redirect($this->segment."/home/");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 apply"));
                        redirect($this->segment."/home/");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other apply"));
                        redirect($this->segment."/home/");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout apply"));
                redirect($this->segment."/home/");
            }
        }
        */
        $pErr = "";
        $pId = "";
        $hrbcErr = "";
        $options = array
        (
            CURLOPT_URL            => $url,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $xml,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result['token']),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

        $this->writeRegistLog($tmp_xml, $xml, "process01");

        if ($xml->Code != 0) {
            $hrbcErr = $xml->Code;
        } else {
            $pCode = "";
            $json = json_encode($xml);
            $arr = json_decode($json, true);
            foreach ($arr['Item'] as $k => $v) {
                if ($k == "Id") {
                    $pId = $v;
                }
                if ($k == "Code") {
                    $pErr = $v;

                }
            }
        }

        $error = "";
        if (!$pId || $pErr) {
//            $error.= "エラーが発生しました。(Process: " . $pErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
        }
        if ($hrbcErr) {
            $error .= "エラーが発生しました。(Process: " . $hrbcErr . " time:" . $now . ")<br />画面をメモして管理者へ問い合わせて下さい。";
        }

        if (empty($error)) {
            //Email送信
            //$resume_url  ="[キャンディデイトBCリンク]" . "\n" .  "https://hrbc-jp.porterscloud.com/resume/search/id/".$resume_id."?menu_id=5";
            $resume_url  ="[キャンディデイトBCリンク]" . "\n" .  "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id ."?menu_id=5#/dv/17:" . $resume_id ."/";

            $job_url = "\n【応募JobBCリンク】". "\n" . "https://hrbc-jp.porterscloud.com/job/search/id/" . $jobid . "?menu_id=3";
            $process_url = "\n【選考プロセスBCリンク】". "\n". "https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $resume_id . "#/dv/7:" . $pId . "/";
            $unit_url  =$resume_url . "\n" . $job_url . "\n" . $process_url;

            $person_datas = $this->getPerson($person_id);
            $resume_datas = $this->getResume($resume_id);

            $subject = "【既登録者(".$agent.")：Job打診からの申込】 JOB:". $jobid . " Client:" . $clientname  . " " . $person_datas["tel1"];

            if($jobowner_flg == 1){
                //エラーの場合
                $mailto = hurexErrMailTo;
            }else{
                $mailto = array($jobowner_mail, hurexMailTo);
            }


            $jobinfo = $this->getJobInfo($jobid);
            $jobowner = $this->getJobOwner($jobid);
            $processInfo = $this->getProcessInfo($resume_id, $jobinfo);
            $power = $processInfo["Process.P_Owner"];
            $process_owner_id = $power["User"]["User.P_Id"];

            $userData = $this->getUser($process_owner_id);
            //選考プロセス担当
            $userName="";
            if(!empty($userData)){
                foreach($userData as $k => $v){
                    if(!empty($v)){
                        foreach (($v) as $k2 => $v2) {
                            if(!empty($v2["User.P_Name"])){
                                if($v2["User.P_Id"] == $process_owner_id){
                                    $userName = $v2["User.P_Name"];
                                }
                            }
                        }
                    }
                }
            }

            //会社名
            $client_name = $job->c_title;

            $param_datas = array();
            $param_datas["process_owner"] = $userName;
            $param_datas["client_name"] = $client_name;
            $param_datas["job_owner"] = $jobowner;

            $this->sendMail5($agent, $jobid, $person_datas, $resume_datas, $job, $unit_url, $mailto, $param_datas);
            //$message = "案件への応募がありました。\n" . $unit_url;
            //$this->successMail($subject, $message);

            $this->session->set_flashdata(array("msg" => "応募しました。"));
            redirect($this->segment . "/home/");
        } else {
            $this->session->set_flashdata(array("msg" => $error));
            redirect($this->segment . "/home/");

        }
    }


    //////////////////////////////////////////////////////////
    //JOB打診からのキャンセル
    //////////////////////////////////////////////////////////
    function refusal()
    {
        $error="";
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $posts = $this->input->post();

        $jobid = $this->input->post("job_id");
        $clientid = $this->input->post("client_id");
        $clientname = $this->input->post("client_name");

        //job情報の取得
        $esc_jobid = $this->db->escape($jobid);
        $sql = "SELECT j.job_id, j.job_title, j.recruiter_id, j.owner_id, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = ". HrbcPhaze . " and j.job_id = " . $esc_jobid . " group by j.job_id";

        $jobs = $this->db->query($sql);
        $job = $jobs->result()[0];

        //job担当取得
        $tmpjobowner = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobowner.json', true);
        $tmpjobowner = json_decode($tmpjobowner,true);

        $jobowner_mail = "";
        $jobowner_flg = 0;
        if(!empty($tmpjobowner)){
            foreach ($tmpjobowner as $k => $v) {
                if(!empty($v)){
                    foreach($v as $k2 => $v2){
                        if(!empty($v2) && is_array($v2)){
                            if(!empty($v2["User.P_Id"]) && $v2["User.P_Id"]==$job->owner_id){
                                $jobowner_mail = $v2["User.P_Mail"];
                            }
                        }
                    }
                }
            }
        }
        if(empty($jobowner_mail)){
            $jobowner_flg = 1;
        }

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        //User情報取得　Person取得のため
        $esc_userid = $this->db->escape($login_id);
        $sql = "SELECT * From members where del = 0 and  id = " . $esc_userid;

        $users = $this->db->query($sql);
        $user = $users->result()[0];
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;


        ////////////////////////////////////////////////////////////////////////////////////////////
        //Processアップデート
        ////////////////////////////////////////////////////////////////////////////////////////////
        $result = $this->getHrbcMaster();
        $data = array();

        $url = $this->hrbcUrl . "process?partition=" . $result["partition"];
//        $now = date('Y/m/d H:i:s');
        $tmpDate = date("Y/m/d H:i:s", strtotime($posts["phasedate"] . "-9 hour"));
//        $now = date("Y/m/d H:i:s", strtotime($tmpDate . "+1 second"));
        $now = date("Y/m/d H:i:s",strtotime("-9 hour"));

        if($posts["phazeFlg"]==1){
            $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>' . $posts["processId"] . '</Process.P_Id><Process.P_Phase><Option.U_001168 /></Process.P_Phase><Process.P_PhaseDate>' . $now . '</Process.P_PhaseDate></Item></Process>';
        }else{
            $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>' . $posts["processId"] . '</Process.P_Id><Process.P_Phase><Option.U_001344 /></Process.P_Phase><Process.P_PhaseDate>' . $now . '</Process.P_PhaseDate></Item></Process>';
        }
        $tmp_xml = $xml;

        $header = array(
            "Content-Type: application/xml; charset=UTF-8",
            "X-porters-hrbc-oauth-token: " . $result['token']
        );

        $options = array('http' => array(
            'method' => 'POST',
            'content' => $xml,
            'header' => implode("\r\n", $header)
        ));

        if($xml = @file_get_contents($url, false, stream_context_create($options))){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 cancel"));
                        redirect($this->segment."/home/");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 cancel"));
                        redirect($this->segment."/home/");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other cancel"));
                        redirect($this->segment."/home/");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout cancel"));
                redirect($this->segment."/home/");
            }
        }
        $pErr = "";
        $pId = "";
        $hrbcErr = "";
        $xml = simplexml_load_string($xml);

        $this->writeRegistLog($tmp_xml, $xml, "process02");

        if ($xml->Code != 0) {
            $hrbcErr = $xml->Code;
        } else {
            $pCode = "";
            $json = json_encode($xml);
            $arr = json_decode($json, true);
            foreach ($arr['Item'] as $k => $v) {
                if ($k == "Id") {
                    $pId = $v;
                }
                if ($k == "Code") {
                    $pErr = $v;

                }
            }
        }

        if (!$pId || $pErr) {
//            $error.= "エラーが発生しました。(Process: " . $pErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
        }
        if ($hrbcErr) {
            $error .= "エラーが発生しました。(Process: " . $hrbcErr . " time:" . $now . ")<br />画面をメモして管理者へ問い合わせて下さい。";
        }

        if (empty($error)) {
            $agent = $this->getDevice();
            //Email送信
            //$resume_url = "[キャンディデイトBCリンク]" . "\n" . "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id ."?menu_id=5";
            $resume_url  ="[キャンディデイトBCリンク]" . "\n" .  "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id ."?menu_id=5#/dv/17:" . $resume_id ."/";

            $job_url = "\n【応募JobBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/job/search/id/" . $jobid . "?menu_id=3";
            $process_url = "\n【選考プロセスBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $resume_id . "#/dv/7:" . $pId . "/";
            $unit_url = $resume_url . "\n" . $job_url . "\n" . $process_url;

            $person_datas = $this->getPerson($person_id);
            $resume_datas = $this->getResume($resume_id);

            $subject = "【既登録者(" . $agent . ")：Job打診の辞退】 JOB:" . $jobid . " Client:" . $clientname . " " . $person_datas["tel1"];

            if($jobowner_flg == 1){
                //エラーの場合
                $mailto = hurexErrMailTo;
            }else{
                $mailto = array($jobowner_mail, hurexMailTo);
            }


            $jobinfo = $this->getJobInfo($jobid);

            $jobowner = $this->getJobOwner($jobid);
            $processInfo = $this->getProcessInfo($resume_id, $jobinfo);
            $power = $processInfo["Process.P_Owner"];
            $process_owner_id = $power["User"]["User.P_Id"];

            $userData = $this->getUser($process_owner_id);
            //選考プロセス担当
            $userName="";
            if(!empty($userData)){
                foreach($userData as $k => $v){
                    if(!empty($v)){
                        foreach (($v) as $k2 => $v2) {
                            if(!empty($v2["User.P_Name"])){
                                if($v2["User.P_Id"] == $process_owner_id){
                                    $userName = $v2["User.P_Name"];
                                }
                            }
                        }
                    }
                }
            }

            //会社名
            $client_name = $job->c_title;

            $param_datas = array();
            $param_datas["process_owner"] = $userName;
            $param_datas["client_name"] = $client_name;
            $param_datas["job_owner"] = $jobowner;

            $this->sendMail7($agent, $jobid, $person_datas, $resume_datas, $job, $unit_url, $mailto,$param_datas);
            //$message = "案件への応募がありました。\n" . $unit_url;
            //$this->successMail($subject, $message);

            $this->session->set_flashdata(array("msg" => "辞退しました。"));
            redirect($this->segment . "/home/");
        } else {
            $this->session->set_flashdata(array("msg" => $error));
            redirect($this->segment . "/home/");

        }
    }
}