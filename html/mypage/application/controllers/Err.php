<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Err extends MY_Controller {
 
    function __construct()
    {
        parent::__construct();
    }
 
    /**
     * 404ページ
     */
    public function error_404() {
 
        $data = array();
 
        /**
         * データベースから値を取得する等、
         * 必要な処理を記述してください。
         */
 
        // ステータスコード404を設定
        $this->output->set_status_header('404');

        $data["render"] = "error/404";
        $this->load->view("template_nologin", $data);
    }

    /**
     * HRBC Error
     */
    public function error_hrbc() {

        $data = array();

        /**
         * データベースから値を取得する等、
         * 必要な処理を記述してください。
         */

        // ステータスコード404を設定
        $this->output->set_status_header('404');

        $data["render"] = "error/error_hrbc";
        $this->load->view("template_nologin", $data);
    }
}