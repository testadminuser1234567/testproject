<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Suspension extends MY_Controller
{

    protected $page_limit = 20;

    //タイトル
    protected $sub_title = "退会フォーム";

    //segment
    protected $segment = "suspension";

    //database
    protected $database = "members";
    protected $databaseJob = "jobs";
    protected $databaseClient = "clients";

    //画像がonの時 許可ファイル
    protected $allow = 'doc|xls|ppt|pptx|pdf|docx|xlsx|html|txt|htm';

    //HRBC URL
    protected $hrbcUrl = "https://api-hrbc-jp.porterscloud.com/v1/";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('form_validation', 'encrypt'));
        $this->load->helper(array('url','cookie'));
    }

    // redirect if needed, otherwise display the user list
    function index()
    {
        $this->home();
    }

    //////////////////////////////////////////////////////////
    //退会フォーム初期処理
    //////////////////////////////////////////////////////////
    function home()
    {
        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $data[$this->segment] = $this->db->get_where($this->database, array('id' => $login_id, 'del' => 0))->row();

        //data設定
        $data["render"] = $this->segment."/home";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["segment"] = $this->segment;
        $data["body_id"] = "suspension";

        $this->load->view("template_suspension", $data);
    }

    //////////////////////////////////////////////////////////
    //退会フォーム メルマガ停止処理
    //////////////////////////////////////////////////////////
    function merumaga()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        //HRBCからキャンディデイト情報取得
        $person_datas = $this->getPerson($person_id);
        //$resume_datas = $this->getResume($resume_id);

        $person_url = "Person: " . "\n" . "https://hrbc-jp.porterscloud.com/can/search/id/" . $person_id . "#/dv/1:" . $person_id . "/";
        $resume_url  ="Resume: " . "\n" . "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id ."?menu_id=5#/dv/17:" . $resume_id ."/";

        $unit_url  = $person_url . "\n" . $resume_url . "\n";

        $subject = "【メルマガ停止】 [".$person_datas["name"]. "]";
        $this->merumagaMail($subject, $user, $person_datas, $unit_url);

        $data[$this->segment] = $this->db->get_where($this->database, array('id' => $login_id, 'del' => 0))->row();

        //data設定
        $data["render"] = $this->segment."/merumaga_finish";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["segment"] = $this->segment;
        $data["body_id"] = "suspension";

        $this->load->view("template_suspension", $data);
    }

    //////////////////////////////////////////////////////////
    //退会フォーム 活動休止
    //////////////////////////////////////////////////////////
    function activity()
    {
        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();

        $data[$this->segment] = $this->db->get_where($this->database, array('id' => $login_id, 'del' => 0))->row();

        //data設定
        $data["render"] = $this->segment."/activity";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["segment"] = $this->segment;
        $data["body_id"] = "suspension";

        $this->load->view("template_suspension", $data);
    }

    //////////////////////////////////////////////////////////
    //退会フォーム 活動中止メール送信
    //////////////////////////////////////////////////////////
    function finish()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $posts = $this->input->post();

        if(empty($posts["reason"])){
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/activity/");
        }else if($posts["reason"]=="転職活動を一時中止する"){
            if( (empty($posts["year"]) && empty($posts["month"])) && empty($posts["mitei"])){
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
                redirect($this->segment."/activity/");
            }
        }

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        //HRBCからキャンディデイト情報取得
        $person_datas = $this->getPerson($person_id);

        $person_url = "Person: " . "\n" . "https://hrbc-jp.porterscloud.com/can/search/id/" . $person_id . "#/dv/1:" . $person_id . "/";
        $resume_url  ="Resume: " . "\n" . "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id ."?menu_id=5#/dv/17:" . $resume_id ."/";

        $unit_url  = $person_url . "\n" . $resume_url . "\n";

        $subject = "【活動中止】 [".$person_datas["name"]. "]";

        $jiki="";
        if($posts["year"]|| $posts["month"]){
            $jiki = $posts["year"]."年".$posts["month"]."月";
        }else{
            $jiki = $posts["mitei"];
        }

        $reason = $posts["reason"];

        $this->suspensionMail($subject, $user, $person_datas, $unit_url, $jiki, $reason);

        $data[$this->segment] = $this->db->get_where($this->database, array('id' => $login_id, 'del' => 0))->row();

        //data設定
        $data["render"] = $this->segment."/activity_finish";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["segment"] = $this->segment;
        $data["body_id"] = "suspension";

        $this->load->view("template_suspension", $data);
    }

    //////////////////////////////////////////////////////////
    //退会フォーム 退会
    //////////////////////////////////////////////////////////
    function confirm()
    {
        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $data[$this->segment] = $this->db->get_where($this->database, array('id' => $login_id, 'del' => 0))->row();

        //data設定
        $data["render"] = $this->segment."/cancel_confirm";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["segment"] = $this->segment;
        $data["body_id"] = "suspension";

        $this->load->view("template_suspension", $data);
    }

    //////////////////////////////////////////////////////////
    //退会フォーム 退会フォーム送信
    //////////////////////////////////////////////////////////
    function close()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');
        if(empty($login_id)){
            $subject = "loginID not found suspension close";
            $message = "ログインユーザーが見つかりませんでした。（退会フォーム）";
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/home/");
        }

        $posts = $this->input->post();
        if(empty($posts["agree"])){
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/confirm/");
        }

        if(empty($posts["reason"])){
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/confirm/");
        }

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        //HRBCからキャンディデイト情報取得
        $person_datas = $this->getPerson($person_id);

        $person_url = "Person: " . "\n" . "https://hrbc-jp.porterscloud.com/can/search/id/" . $person_id . "#/dv/1:" . $person_id . "/";
        $resume_url  ="Resume: " . "\n" . "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id ."?menu_id=5#/dv/17:" . $resume_id ."/";

        $unit_url  = $person_url . "\n" . $resume_url . "\n";

        $subject = "【退会】 [".$person_datas["name"]. "]";

        $reason = $posts["reason"];
        $comment = $posts["comment"];

        $this->cancelMail($subject, $user, $person_datas, $unit_url, $reason, $comment);

        $data[$this->segment] = $this->db->get_where($this->database, array('id' => $login_id, 'del' => 0))->row();

        //CPI 削除処理
        $this->input->set_cookie("loginkey", "", "");
        $data = array(
            'loginkey' => "",
            'del' => 1
        );
        $this->db->where('id', $login_id);
        $this->db->update($this->database, $data);

        //login_status delete
        $sess = get_cookie("ci_session");
        $this->db->where('id', $sess);
        $this->db->delete('ci_sessions');

        // log the user out
        $this->session->unset_userdata('login_id');

        $this->session->sess_destroy();

        //data設定
        $data["render"] = $this->segment."/cancel_finish";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["segment"] = $this->segment;
        $data["body_id"] = "suspension";

        $this->load->view("template_suspension_close", $data);
    }

    //////////////////////////////////////////////////////////
    //バリデーション
    //////////////////////////////////////////////////////////
    private function setValidation($validation=null)
    {
        $errors ="";
//        $this->form_validation->set_rules("kiboujob[]", "希望職種","required");
//        $this->form_validation->set_rules("kibouarea[]", "希望勤務地","required");
//        $this->form_validation->set_rules("tenkin[]", "転勤","required");
        $this->form_validation->set_rules("income", "希望年収","numeric");
//        $this->form_validation->set_rules("income", "希望年収","required|numeric");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }
}
