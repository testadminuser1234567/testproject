<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Backup extends MY_Controller {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * index
     */
    public function index() {

        /**
         * データベースから値を取得する等、
         * 必要な処理を記述してください。
         */

        // DB ユーティリティクラスをロード
        $this->load->dbutil();

        //バックアップするDB
        $bkupdb = array('members', 'jobs','clients','recruiters','likes','looks');

        foreach($bkupdb as $k => $v){
            $prefs = array(
                'tables'        => array($v),   // バックアップするテーブルの配列。
                'ignore'        => array(),                     // バックアップしないテーブルのリスト。
                'add_drop'      => TRUE,                        // バックアップファイルにDROP TABLE 文を追加するかどうか
                'add_insert'    => TRUE,                        // バックアップファイルにINSERT 文を追加するかどうか
            );

            // データベース全体をバックアップしその結果を変数に代入
            $backup = $this->dbutil->backup($prefs);

            $tmp_date = date('Ymd');
            $date = date('w');
            $filename = "backup_" . $v . ".zip";
            /*
            if($date==1 || $date == 4){
                $filename = "backup_" . $tmp_date . ".zip";
            }
            */


            // ファイルヘルパーをロードし、サーバにファイルを書き出す
            $this->load->helper('file');
//        $wfile = BaseDir . '/mypage/db/backup.zip';
            $wfile = BaseDir . '/mypage/db/' . $filename;
            write_file($wfile, $backup);

            /*
                    $subject = "【backupErr】バックアップエラー";
                    $message = $pErr;
                    $this->errMail($subject, $message);
            */
            // ダウンロードヘルパーをロードし、ファイルをデスクトップに送信する
//	$this->load->helper('download');
//	force_download('backup.zip', $backup);
        }

    }

}