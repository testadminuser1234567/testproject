<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Manual extends MY_Controller {
 
    function __construct()
    {
        parent::__construct();
    }
 
    /**
     * 1ページ
     */
    public function page1() {
 
        $data = array();
 
        $data["render"] = "manual/page1";
	$data["current"] = "page1";
        $this->load->view("template_nologin", $data);
    }

    /**
     * 2ページ
     */
    public function page2() {

        $data = array();
 
        $data["render"] = "manual/page2";
	$data["current"] = "page2";
        $this->load->view("template_nologin", $data);
    }

    /**
     * 3ページ
     */
    public function page3() {

        $data = array();
 
        $data["render"] = "manual/page3";
	$data["current"] = "page3";
        $this->load->view("template_nologin", $data);
    }

    /**
     * 4ページ
     */
    public function page4() {

        $data = array();
 
        $data["render"] = "manual/page4";
	$data["current"] = "page4";
        $this->load->view("template_nologin", $data);
    }

    /**
     * 5ページ
     */
    public function page5() {

        $data = array();
 
        $data["render"] = "manual/page5";
	$data["current"] = "page5";
        $this->load->view("template_nologin", $data);
    }

    /**
     * 6ページ
     */
    public function page6() {

        $data = array();
 
        $data["render"] = "manual/page6";
	$data["current"] = "page6";
        $this->load->view("template_nologin", $data);
    }

    /**
     * 7ページ
     */
    public function page7() {

        $data = array();
 
        $data["render"] = "manual/page7";
	$data["current"] = "page7";
        $this->load->view("template_nologin", $data);
    }
}