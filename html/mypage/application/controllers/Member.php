<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MY_Controller
{

    protected $page_limit = 20;

    //タイトル
    protected $sub_title = "ユーザー";

    //segment
    protected $segment = "member";

    //database
    protected $database = "members";
    protected $databaseJob = "jobs";
    protected $databaseClient = "clients";

    //画像がonの時 許可ファイル
    protected $allow = 'doc|xls|ppt|pptx|pdf|docx|xlsx|html|txt|htm';

    //SNSのURL
    protected $fbloginUrl = "";
    protected $googleUrl = "";

    //HRBC URL
    protected $hrbcUrl = "https://api-hrbc-jp.porterscloud.com/v1/";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('form_validation', 'encrypt'));
        $this->load->helper(array('url'));
    }

    // redirect if needed, otherwise display the user list
    function index()
    {
        $this->login();
    }

    //////////////////////////////////////////////////////////
    //ログイン
    //////////////////////////////////////////////////////////
    function login()
    {
        //POSTデータの取得
        $posts = $this->input->post();

        //エラーメッセージ格納用
        $errors = "";

        //クッキーの読み出し 自動ログイン
        if (!empty($_COOKIE['loginkey'])) {
            $login_cookie = $_COOKIE['loginkey'];
        }
        /*
        //ログイン情報を記憶している場合
        */
        if (!empty($login_cookie) && empty($post)) {
            //DBにアクセス
            $return = $this->db->get_where($this->database, array('loginkey' => $login_cookie, 'del' => 0))->row();

            if (!empty($return->id) && $return->loginkey == $login_cookie) {
                //ログインOK
                $login_hash = hash_hmac("sha256", date("Ymd") . $return->email, Salt);
                $last_login = date('Y-m-d h:i:s');
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
                $ip = $_SERVER["REMOTE_ADDR"];

                $data = array(
                    'login_hash' => $login_hash,
                    'last_login' => $last_login,
                    'user_agent' => $user_agent,
                    'ip' => $ip
                );
                $this->db->where('id', $return->id);
                $this->db->update($this->database, $data);

                $sessionData = array(
                    'login_member' => hash_hmac("sha256", date("Ymd") . $return->email, Salt),
                    'login_user' => $return->email,
                    'login_id' => $return->id,
                    'login_role' => $return->role
                );
                $this->session->set_userdata($sessionData);

                redirect(Home);
            }
            //通常のログイン処理
        } else if (!empty($posts)) {
            $this->fbloginUrl = $this->facebookLoginURL();
            $this->googleUrl = $this->googleLoginURL();

            $username = $this->clean->purify($posts['email']);
            $password = $this->clean->purify($posts['password']);

            if (!strlen($username)) {
                $errors .= 'ユーザIDを入力してください<br />';
            }

            if (!strlen($password)) {
                $errors .= 'パスワードを入力してください<br />';
            }

            //エラーがなければユーザの照合
            if (empty($errors)) {
                //DBにアクセス
                $username = hash_hmac("sha256", $username, Salt);
                $pass = hash_hmac("sha256", $password, Salt);
                $salt = hash_hmac("sha256", Salt, Salt);
                $pass_unit = $salt . $username . $pass;
                $return = $this->db->get_where($this->database, array('email' => $username, 'password' => $pass_unit, 'del' => 0))->row();
                //postのユーザとDBが違う場合は失敗
                if (empty($return->email) || $return->email != $username) {
                    $errors .= 'ユーザIDかパスワードが不正です<br />';

                    //data設定
                    $data["render"] = $this->segment . "/login";
                    $data["clean"] = $this->clean;
                    $data["msg"] = $errors;
                    $data["segment"] = $this->segment;
                    $data["fbloginUrl"] = $this->fbloginUrl;
                    $data["googleUrl"] = $this->googleUrl;

                    $this->load->view("template", $data);
                    //成功
                } else {
                    //ログイン情報を記憶した場合は値保存
                    if (!empty($posts['loginkey']) && $posts['loginkey'] === 'on') {
                        $loginkey = hash_hmac("sha256", date("YmdHis"), Salt);
                        $data = array(
                            'loginkey' => $loginkey
                        );
                        $this->db->where('id', $return->id);
                        $this->db->update($this->database, $data);

                        //クッキーに書き込み
                        $expire = time() + 20 * 24 * 3600; // 20日
                        setcookie("loginkey", $loginkey, $expire, "/", $_SERVER["HTTP_HOST"], 0);
                    }
                    //ログインOK
                    $login_hash = hash_hmac("sha256", date("Ymd") . $return->email, Salt);
                    $last_login = date('Y-m-d h:i:s');
                    $user_agent = $_SERVER['HTTP_USER_AGENT'];
                    $ip = $_SERVER["REMOTE_ADDR"];

                    $data = array(
                        'login_hash' => $login_hash,
                        'last_login' => $last_login,
                        'user_agent' => $user_agent,
                        'ip' => $ip
                    );
                    $this->db->where('id', $return->id);
                    $this->db->update($this->database, $data);

                    $sessionData = array(
                        'login_member' => hash_hmac("sha256", date("Ymd") . $return->email, Salt),
                        'login_user' => $return->email,
                        'login_id' => $return->id,
                        'login_role' => $return->role
                    );
                    $this->session->set_userdata($sessionData);

                    redirect(Home);
                }
                //IDかパスワードが入力されていない場合
            } else {
                $this->fbloginUrl = $this->facebookLoginURL();
                $this->googleUrl = $this->googleLoginURL();

                //data設定
                $data["render"] = $this->segment . "/login";
                $data["clean"] = $this->clean;
                $data["msg"] = $errors;
                $data["segment"] = $this->segment;
                $data["fbloginUrl"] = $this->fbloginUrl;
                $data["googleUrl"] = $this->googleUrl;
                $this->load->view("template", $data);
            }
            //初期画面
        } else {
            $this->fbloginUrl = $this->facebookLoginURL();
            $this->googleUrl = $this->googleLoginURL();

            ///////////////////////////////////////////////////////////////////////////
            //data設定
            ///////////////////////////////////////////////////////////////////////////
            $errors .= $this->session->flashdata('msg');;
            $data["render"] = $this->segment . "/login";
            $data["clean"] = $this->clean;
            $data["msg"] = $errors;
            $data["segment"] = $this->segment;
            $data["fbloginUrl"] = $this->fbloginUrl;
            $data["googleUrl"] = $this->googleUrl;
            $this->load->view("template", $data);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    //Facebookログイン用
    ///////////////////////////////////////////////////////////////////////////
    private  function facebookLoginURL(){
        $fb = $this->fbLogin();

        $helper = $fb->getRedirectLoginHelper();
        $permissions = ['email']; // Optional permissions
        $fURL = $helper->getLoginUrl(base_url(). 'member/fboauth', $permissions);

        return $fURL;
    }

    //////////////////////////////////////////////////////////
    //facebookログイン処理
    //////////////////////////////////////////////////////////
    function fboauth()
    {
        //Facebookログイン用
        $fb = $this->fbLogin();

        $helper = $fb->getRedirectLoginHelper();

        try {
            if (isset($_SESSION['facebook_access_token'])) {
                $accessToken = $_SESSION['facebook_access_token'];
            } else {
                //アクセストークンを取得する
                $accessToken = $helper->getAccessToken();
            }
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        if (isset($accessToken)) {
            //アクセストークンをセッションに保存
            $_SESSION['facebook_access_token'] = (string) $accessToken;

            if (isset($_SESSION['facebook_access_token'])) {

                $accessToken = $_SESSION['facebook_access_token'];

                $fb->setDefaultAccessToken($accessToken);

                try {
                    //取得するユーザ情報の指定
                    $response = $fb->get('/me?fields=id,name,first_name,last_name,email,gender');
                    $profile = $response->getGraphUser();

                    //ユーザ画像取得
                    $UserPicture = $fb->get('/me/picture?redirect=false&height=200');
                    $picture = $UserPicture->getGraphUser();

                } catch(Facebook\Exceptions\FacebookResponseException $e) {
                    // When Graph returns an error
                    echo 'Graph returned an error: ' . $e->getMessage();
                    exit;
                } catch(Facebook\Exceptions\FacebookSDKException $e) {
                    // When validation fails or other local issues
                    echo 'Facebook SDK returned an error: ' . $e->getMessage();
                    exit;
                }

                //HRBCから抽出
                $email=$profile['email'];
                $result = $this->getHrbcMaster();
                //$email = hash_hmac("sha256", $email, Salt);

                $data = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Mail:full=' . $email));
                $url = "https://api-hrbc-jp.porterscloud.com/v1/candidate?" . $data;
                $opts = array(
                    'http'=>array(
                        'method' => 'GET',
                        'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                            . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                    )
                );
                $context = stream_context_create($opts);

                $xml = file_get_contents($url, false, $context);
                $xml = simplexml_load_string($xml);
                if($xml->Code!=0){
                    $pErr=$xml->Code;
                    print("エラーが発生しました。<br />コード：" . $pErr);
                    exit;
                }

                //データ設定
                $json = json_encode($xml);
                $arr = json_decode($json,true);

                //candidate ID
                $pId = $arr["Item"]["Person.P_Id"];

                //candidate Idからresume取得
                $data = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Candidate(Person.P_ID):full=' . $pId, 'field' => 'Resume.P_Candidate'));
                $url = "https://api-hrbc-jp.porterscloud.com/v1/resume?" . $data;
                $opts = array(
                    'http'=>array(
                        'method' => 'GET',
                        'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                            . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                    )
                );
                $context = stream_context_create($opts);

                $xml = file_get_contents($url, false, $context);
                $xml = simplexml_load_string($xml);
                print_r($xml);
                exit;
                if($xml->Code!=0){
                    $pErr=$xml->Code;
                    print("エラーが発生しました。<br />コード：" . $pErr);
                    exit;
                }

                //データ設定
                $json = json_encode($xml);
                $arr = json_decode($json,true);

            }else{
                // redirect them to the login page
                redirect($this->segment.'/login');
            }
        }else{
            // redirect them to the login page
            redirect($this->segment.'/login');
        }
    }


    //////////////////////////////////////////////////////////
    //facebookログイン
    //////////////////////////////////////////////////////////
    private function fbLogin(){
        require(dirname(__FILE__) . "../../libraries/src/Facebook/autoload.php");
        $fb = new Facebook\Facebook([
            'app_id' => facebookAppId,
            'app_secret' => facebookSecret,
            'default_graph_version' => 'v2.7',
        ]);
        return $fb;
    }

    ///////////////////////////////////////////////////////////////////////////
    //googleログイン用 URL
    ///////////////////////////////////////////////////////////////////////////
    private function googleLoginURL(){
        define('CLIENT_ID', googleClientId);
        define('CALLBACK', base_url() . googleCallback);

        $baseURL = 'https://accounts.google.com/o/oauth2/auth?';

        $scope = array(
            'https://www.googleapis.com/auth/userinfo.profile', // 基本情報(名前とか画像とか)
            'https://www.googleapis.com/auth/userinfo.email',   // メールアドレス
        );

        // 認証用URL生成
        $gURL = $baseURL . 'scope=' . urlencode(implode(' ', $scope)) .
            '&redirect_uri=' . urlencode(CALLBACK) .
            '&response_type=code' .
            '&client_id=' . CLIENT_ID;

        return $gURL;
    }

    //////////////////////////////////////////////////////////
    //googleログイン処理
    //////////////////////////////////////////////////////////
    function googlelogin()
    {
        define('CONSUMER_KEY', '162395260780-0fjf2rhvpulckpd8k6ljlbq4eis5so72.apps.googleusercontent.com');
        define('CONSUMER_SECRET', 'GFKbtWRoGK3S8ZSVsiT91NIF');
        define('CALLBACK_URL', base_url() . googleCallback);

        // URL
        define('TOKEN_URL', 'https://accounts.google.com/o/oauth2/token');
        define('INFO_URL', 'https://www.googleapis.com/oauth2/v1/userinfo');


        //--------------------------------------
        // アクセストークンの取得
        //--------------------------------------
        $params = array(
            'code' => $_GET['code'],
            'grant_type' => 'authorization_code',
            'redirect_uri' => CALLBACK_URL,
            'client_id' => CONSUMER_KEY,
            'client_secret' => CONSUMER_SECRET,
        );

        // POST送信
        $options = array(
            'http' => array(
                'method' => 'POST',
                'content' => http_build_query($params),
            )
        );
        $res = file_get_contents(TOKEN_URL, false, stream_context_create($options));

        // レスポンス取得
        $token = json_decode($res, true);
        if(isset($token['error'])){
            echo 'エラー発生';
            exit;
        }
        $access_token = $token['access_token'];

        //--------------------------------------
        // ユーザー情報を取得してみる
        //--------------------------------------
        $params = array('access_token' => $access_token);
        $res = file_get_contents(INFO_URL . '?' . http_build_query($params));
        $result = json_decode($res, true);
        print_r($result['email']);
    }

    //////////////////////////////////////////////////////////
    //ログアウト
    //////////////////////////////////////////////////////////
    function logout()
    {
        //自動ログイン削除
        $expire = time()-1800;
        setcookie("loginkey", "", $expire , "/", $_SERVER["HTTP_HOST"] ,0);

        $login_id = $this->session->userdata('login_id');
        $data = array(
            'loginkey' => ""
        );
        $this->db->where('id', $login_id);
        $this->db->update($this->database, $data);

        //login_status delete
        $sess = $_COOKIE['ci_session'];
        $this->db->where('id', $sess);
        $this->db->delete('ci_sessions');

        // log the user out
        $this->session->unset_userdata('login_member');
        $this->session->unset_userdata('login_user');
        $this->session->unset_userdata('login_id');
        $this->session->unset_userdata('login_role');

        $this->session->sess_destroy();

        // redirect them to the login page
        redirect($this->segment.'/login');
    }

    //////////////////////////////////////////////////////////
    //一覧出力
    //////////////////////////////////////////////////////////
    function home()
    {
        $role = $this->session->userdata('login_role');
        $login_id = $this->session->userdata('login_id');
        $login_member = $this->session->userdata('login_member');
        $login_user = $this->session->userdata('login_user');

        $posts = $this->input->post();

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $this->load->library("pagination");

        $param = array("del"=>0, "id"=>$login_id);

        ////////////////////////////////////////////////////////////////////////////////////////////
        //HRBCデータ取得
        ////////////////////////////////////////////////////////////////////////////////////////////
        $result = $this->getHrbcMaster();
        $data = array();
        $em = "hurexmypage@gmail.com";

        /*
        ////////////////////////////////////////////////////////////////////////////////////////////
        //Person
        ////////////////////////////////////////////////////////////////////////////////////////////

        $param = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_MobileMail:full='.$em, 'field'=>'Person.P_Id,Person.P_Name,Person.P_Reading,Person.P_Telephone,Person.P_Mobile,Person.P_MobileMail,Person.P_Mail', 'order'=>'Person.P_Id:desc'));
        $url =$this->hrbcUrl .  "candidate?" . $param;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        $xml = file_get_contents($url, false, $context);
        $xml = simplexml_load_string($xml);

        if($xml->Code!=0){
            $pErr=$xml->Code;
            print("抽出処理でエラーが発生しました。<br />コード：" . $pErr);
            exit;
        }

        //HRBCから帰ってきたデータ件数　1件の対応用に取得
        $cnt = $xml->attributes()->Count;

        //データ設定
        $json = json_encode($xml);
        $data["person"] = json_decode($json,true);
        */

        /*
        ////////////////////////////////////////////////////////////////////////////////////////////
        //Resume
        ////////////////////////////////////////////////////////////////////////////////////////////
        $param = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.U_774DF284856EFCC9E450FF39572729:full='.$em, 'field'=>'Resume.U_91DBD8B96BAAD6B33B538CF2494377,Resume.P_Phase,Resume.P_DateOfBirth,Resume.P_Gender,Resume.P_Owner', 'order'=>'Resume.P_Id:desc'));
        $url =$this->hrbcUrl .  "resume?" . $param;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        $xml = file_get_contents($url, false, $context);
        $xml = simplexml_load_string($xml);

        if($xml->Code!=0){
            $pErr=$xml->Code;
            print("抽出処理でエラーが発生しました。<br />コード：" . $pErr);
            exit;
        }

        //HRBCから帰ってきたデータ件数　1件の対応用に取得
        $cnt = $xml->attributes()->Count;

        //データ設定
        $json = json_encode($xml);
        $data["resume"] = json_decode($json,true);
        */

        /*
        ////////////////////////////////////////////////////////////////////////////////////////////
        //User
        ////////////////////////////////////////////////////////////////////////////////////////////
        $param = http_build_query(array('request_type'=> 1 ,'partition'=>$result["partition"], 'count'=>200 , 'start'=>0,  'field'=>'User.P_Id,User.P_Name,User.P_Mail, User.P_Type', 'order' => 'User.P_Id:asc'));
        $url =$this->hrbcUrl .  "user?" . $param;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        $xml = file_get_contents($url, false, $context);
        $xml = simplexml_load_string($xml);

        if($xml->Code!=0){
            $pErr=$xml->Code;
            print("抽出処理でエラーが発生しました。<br />コード：" . $pErr);
            exit;
        }

        //HRBCから帰ってきたデータ件数　1件の対応用に取得
        $cnt = $xml->attributes()->Count;

        //データ設定
        $json = json_encode($xml);
        $users = json_decode($json,true);
        print_r($users);
        exit;
        */

        ////////////////////////////////////////////////////////////////////////////////////////////
        //Process
        ////////////////////////////////////////////////////////////////////////////////////////////
        $param = http_build_query(array('partition'=>$result["partition"], 'count'=>5 , 'start'=>0, 'condition'=>'Process.P_Resume:eq=52321',  'field'=>'Process.P_Id,Process.P_Job,Process.P_Owner,Process.P_Recruiter,Process.P_Resume, Process.U_4E29163A766BC0D6CA7D9B1157A614,Process.P_Phase,Process.P_PhaseMemo,Process.P_Close,Process.P_PhaseDate,Process.P_Client,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.P_UpdateDate,Process.U_186596AC430DB9AF87A142A6A47DC7,Process.U_419EC7AD9BE7E1A377DF8B6D0115A1,Process.U_EFEE750C9B6F3D74F06F8E821E1C79,Process.U_1C50E7D9F0AD650BF5159E1BE0DF6D,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.U_5D69124062C230BB6DEC5F4D812C57,Process.U_3E781B18BA915F475A1FA174CAAD69,Process.P_UpdateDate', 'order' => 'Process.P_Id:desc'));
        $url =$this->hrbcUrl .  "process?" . $param;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        $xml = file_get_contents($url, false, $context);
        $xml = simplexml_load_string($xml);

        if($xml->Code!=0){
            $pErr=$xml->Code;
            print("抽出処理でエラーが発生しました。<br />コード：" . $pErr);
            exit;
        }

        //HRBCから帰ってきたデータ件数　1件の対応用に取得
        $cnt = $xml->attributes()->Count;

        //データ設定
        $json = json_encode($xml);
        $processes = json_decode($json,true);

        $i=0;
        if($cnt==0){
        }else if($cnt!=1){
            //複数データ
            foreach($processes["Item"] as $val){
                foreach($val["Process.P_Phase"] as $k => $v ){
                    $opname = $v["Option.P_Name"];
                    $opId = $v["Option.P_Id"];
                    if ($opname == "WEBエントリー") {
                        $phaze =  "お問合せ中";
                        $phazeFlg = 0;
                    }else if($opname == "JOB打診" || $opname == "JOB打診（弱）" || $opname == "JOB打診【強！】"){
                        $phaze =   "お気に入り";
                        $phazeFlg = 1;
                    }else if($opname == "本人NG"){
                        $phaze =   "辞退";
                        $phazeFlg = 0;
                    }else if($opname == "応募承諾（書類待ち）" || $opname == "応募承諾" || $opname == "社内NG予定" || $opname == "書類推薦"){
                        $phaze =   "応募中";
                        $phazeFlg = 0;
                    }else if($opname == "社内NG" || $opname == "書類NG" || $opname == "面接NG" || $opname == "面接辞退" || $opname == "内定辞退" || $opname == "期限切れ処理"){
                        $phaze =   "お見送り";
                        $phazeFlg = 0;
                    }else if($opname == "書類OK" || $opname == "面接（一次）" || $opname == "面接（一次）OK" || $opname == "面接（二次）" || $opname == "面接（二次）OK" || $opname == "面接（三次）" || $opname == "面接（三次）OK" || $opname == "面接（最終）"){
                        $phaze =   "面接";
                        $phazeFlg = 0;
                    }else if($opname == "内定"){
                        $phaze =   "内定";
                        $phazeFlg = 0;
                    }else if($opname == "入社予定" || $opname == "入社"){
                        $phaze =   "入社予定";
                        $phazeFlg = 0;
                    }else if($opname == "JOBクローズ"){
                        $phaze =   "案件終了";
                        $phazeFlg = 0;
                    }else{
                        $phaze="";
                    }
                }

                $jobId = $val["Process.P_Job"]["Job"]["Job.P_Id"];
                $clientId = $val["Process.P_Client"]["Client"]["Client.P_Id"];
                if(!empty($phaze)){
                    //ジョブと企業の設定
                    $job = $this->db->get_where($this->databaseJob, array('job_id' => $jobId))->row();
                    $client = $this->db->get_where($this->databaseClient, array('c_id' => $clientId))->row();
                    $job = (array)$job;
                    $client = (array)$client;

                    //職種の設定
                    $json2 = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
                    $tmpJobcategory = json_decode($json2,true);
                    $jobCategoryName="";
                    $jobCategoryId="";
                    foreach($tmpJobcategory['Item'] as $jck=>$jcv){
                        if(!empty($jcv["Item"])){
                            foreach($jcv['Item'] as $jck2 => $jcv2){
                                foreach($jcv2['Items'] as $jck3 => $jcv3){
                                    foreach($jcv3 as $jck4 => $jcv4){
                                        if($job["jobcategory_id"]==$jcv4['Option.P_Id']){
                                            $jobCategoryName = htmlspecialchars($jcv4['Option.P_Name'],ENT_QUOTES,'UTF-8');
                                            $jobCategoryId = htmlspecialchars($jcv4['Option.P_Id'],ENT_QUOTES,'UTF-8') . ",";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //マイページに反映するチェック
                    $mypageCheck="";
                    $mypageText="";
                    if(!empty($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])){
                        foreach($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $mk=>$mv){
                            if(!empty($mv)){
                                foreach($mv as $mk2 => $mv2){
                                    $mypageCheck = $mv2["Option.P_Id"];
                                    $mypageText = $mv2["Option.P_Name"];
                                }
                            }
                        }
                    }
                    $data["process"][$i]["processId"] = $val["Process.P_Id"];
                    $data["process"][$i]["mypageCheck"] = $mypageCheck;
                    $data["process"][$i]["mypageText"] = $mypageText;
                    $data["process"][$i]["phaze"] = $phaze;
                    $data["process"][$i]["phazeId"] = $opId;
                    $data["process"][$i]["phazeFlg"] = $phazeFlg;
                    $data["process"][$i]["job"] = $job;
                    $data["process"][$i]["client"] = $client;
                    $tmpdate="";
                    if($val["Process.P_PhaseDate"]){
                        $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_PhaseDate"])));
                    }
                    $data["process"][$i]["phasedate"] = $tmpdate;
                    $data["process"][$i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_UpdateDate"][0])));
                    $data["process"][$i]["jobcategoryName"] = $jobCategoryName;
                    $data["process"][$i]["jobcategoryId"] = $jobCategoryId;
                    $i++;
                }
            }
            //1件のデータは返却の形式が異なるので個別対応
        }else{
            foreach($processes["Item"]["Process.P_Phase"] as $k => $v ){
                $opname = $v["Option.P_Name"];
                $opId = $v["Option.P_Id"];
                if ($opname == "WEBエントリー") {
                    $phaze =  "お問合せ中";
                    $phazeFlg = 0;
                }else if($opname == "JOB打診" || $opname == "JOB打診（弱）" || $opname == "JOB打診【強！】"){
                    $phaze =   "お気に入り";
                    $phazeFlg = 1;
                }else if($opname == "本人NG"){
                    $phaze =   "辞退";
                    $phazeFlg = 0;
                }else if($opname == "応募承諾（書類待ち）" || $opname == "応募承諾" || $opname == "社内NG予定" || $opname == "書類推薦"){
                    $phaze =   "応募中";
                    $phazeFlg = 0;
                }else if($opname == "社内NG" || $opname == "書類NG" || $opname == "面接NG" || $opname == "面接辞退" || $opname == "内定辞退" || $opname == "期限切れ処理"){
                    $phaze =   "お見送り";
                    $phazeFlg = 0;
                }else if($opname == "書類OK" || $opname == "面接（一次）" || $opname == "面接（一次）OK" || $opname == "面接（二次）" || $opname == "面接（二次）OK" || $opname == "面接（三次）" || $opname == "面接（三次）OK" || $opname == "面接（最終）"){
                    $phaze =   "面接";
                    $phazeFlg = 0;
                }else if($opname == "入社予定" || $opname == "入社"){
                    $phaze =   "入社予定";
                    $phazeFlg = 0;
                }else if($opname == "JOBクローズ"){
                    $phaze =   "案件終了";
                    $phazeFlg = 0;
                }else{
                    $phaze="";
                }
            }
            $jobId = $processes["Item"]["Process.P_Job"]["Job"]["Job.P_Id"];
            $clientId = $processes["Item"]["Process.P_Client"]["Client"]["Client.P_Id"];
            if(!empty($phaze)){

                //ジョブと企業の設定
                $job = $this->db->get_where($this->databaseJob, array('job_id' => $jobId))->row();
                $client = $this->db->get_where($this->databaseClient, array('c_id' => $clientId))->row();
                $job = (array)$job;
                $client = (array)$client;

                //職種の設定
                $json2 = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
                $tmpJobcategory = json_decode($json2,true);
                $jobCategoryName="";
                $jobCategoryId="";
                if(!empty($tmpJobcategory["Item"])){
                    foreach($tmpJobcategory['Item'] as $jck=>$jcv){
                        if(!empty($jcv["Item"])){
                            foreach($jcv['Item'] as $jck2 => $jcv2){
                                foreach($jcv2['Items'] as $jck3 => $jcv3){
                                    foreach($jcv3 as $jck4 => $jcv4){
                                        if($job["jobcategory_id"]==$jcv4['Option.P_Id']){
                                            $jobCategoryName = htmlspecialchars($jcv4['Option.P_Name'],ENT_QUOTES,'UTF-8');
                                            $jobCategoryId = htmlspecialchars($jcv4['Option.P_Id'],ENT_QUOTES,'UTF-8') . ",";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //マイページに反映するチェック
                $mypageCheck="";
                $mypageText="";
                if(!empty($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])){
                    foreach($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $mk=>$mv){
                        if(!empty($mv)){
                            foreach($mv as $mk2 => $mv2){
                                $mypageCheck = $mv2["Option.P_Id"];
                                $mypageText = $mv2["Option.P_Name"];
                            }
                        }
                    }
                }

                $data["process"][$i]["processId"] = $processes["Item"]["Process.P_Id"];
                $data["process"][$i]["mypageCheck"] = $mypageCheck;
                $data["process"][$i]["mypageText"] = $mypageText;
                $data["process"][$i]["phaze"] = $phaze;
                $data["process"][$i]["phazeId"] = $opId;
                $data["process"][$i]["phazeFlg"] = $phazeFlg;
                $data["process"][$i]["job"] = $job;
                $data["process"][$i]["client"] = $client;
                $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_PhaseDate"])));
                $data["process"][$i]["phasedate"] = $tmpdate;
                $data["process"][$i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_UpdateDate"][0])));
                $data["process"][$i]["jobcategoryName"] = $jobCategoryName;
                $data["process"][$i]["jobcategoryId"] = $jobCategoryId;
                $i++;
            }
        }

        //data設定
        $data["render"] = $this->segment."/home";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["login_id"] = $login_id;
        $data["role"] = $role;
        $data["segment"] = $this->segment;

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //JOB打診からの申し込み
    //////////////////////////////////////////////////////////
    function apply()
    {
        $posts = $this->input->post();

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        ////////////////////////////////////////////////////////////////////////////////////////////
        //Processアップデート
        ////////////////////////////////////////////////////////////////////////////////////////////
        $result = $this->getHrbcMaster();
        $data = array();

        $url =$this->hrbcUrl .  "process?partition=".$result["partition"];
//        $now = date('Y/m/d H:i:s');
        $tmpDate = date("Y/m/d H:i:s",strtotime($posts["phasedate"]. "-9 hour"));
        $now = date("Y/m/d H:i:s",strtotime($tmpDate. "+1 second"));

        $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>' . $posts["processId"] . '</Process.P_Id><Process.P_Phase><Option.U_001122 /></Process.P_Phase><Process.P_PhaseDate>' . $now . '</Process.P_PhaseDate></Item></Process>';

        $header = array(
            "Content-Type: application/xml; charset=UTF-8",
            "X-porters-hrbc-oauth-token: " . $result['token']
        );

        $options = array('http' => array(
            'method' => 'POST',
            'content' => $xml,
            'header' => implode("\r\n", $header)
        ));

        $xml = file_get_contents($url, false, stream_context_create($options));
        $pErr="";
        $pId = "";
        $hrbcErr="";
        $xml = simplexml_load_string($xml);
        if($xml->Code!=0){
            $hrbcErr=$xml->Code;
        }else{
            $pCode="";
            $json = json_encode($xml);
            $arr = json_decode($json,true);
            foreach($arr['Item'] as $k => $v){
                if($k=="Id"){
                    $pId = $v;
                }
                if($k=="Code"){
                    $pErr = $v;

                }
            }
        }

        $error="";
        if(!$pId || $pErr){
//            $error.= "エラーが発生しました。(Process: " . $pErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
        }
        if($hrbcErr){
            $error.= "エラーが発生しました。(Process: " . $hrbcErr . " time:" . $now . ")<br />画面をメモして管理者へ問い合わせて下さい。";
        }

        if(empty($error)) {
            //Email送信
            $this->load->library('email');
            $this->email->from('mypage@hurex.co.jp', 'HUREX株式会社');
            $this->email->to('mypage@hurex.co.jp.test-google-a.com');
            $this->email->subject('案件への応募がありました。' . $posts["jobname"]);
            $this->email->message($posts["jobname"]);
            $this->email->send();

            $this->session->set_flashdata(array("msg"=>"応募しました。"));
            redirect($this->segment."/home");
        }else{
            $this->session->set_flashdata(array("msg"=>$error));
            redirect($this->segment."/home");

        }
    }


    //////////////////////////////////////////////////////////
    //JOB打診からのキャンセル
    //////////////////////////////////////////////////////////
    function cancel()
    {$posts = $this->input->post();

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        ////////////////////////////////////////////////////////////////////////////////////////////
        //Processアップデート
        ////////////////////////////////////////////////////////////////////////////////////////////
        $result = $this->getHrbcMaster();
        $data = array();

        $url =$this->hrbcUrl .  "process?partition=".$result["partition"];
//        $now = date('Y/m/d H:i:s');
        $tmpDate = date("Y/m/d H:i:s",strtotime($posts["phasedate"]. "-9 hour"));
        $now = date("Y/m/d H:i:s",strtotime($tmpDate. "+1 second"));

        $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>' . $posts["processId"] . '</Process.P_Id><Process.P_Phase><Option.U_001168 /></Process.P_Phase><Process.P_PhaseDate>' . $now . '</Process.P_PhaseDate></Item></Process>';

        $header = array(
            "Content-Type: application/xml; charset=UTF-8",
            "X-porters-hrbc-oauth-token: " . $result['token']
        );

        $options = array('http' => array(
            'method' => 'POST',
            'content' => $xml,
            'header' => implode("\r\n", $header)
        ));

        $xml = file_get_contents($url, false, stream_context_create($options));
        $pErr="";
        $pId = "";
        $hrbcErr="";
        $xml = simplexml_load_string($xml);
        if($xml->Code!=0){
            $hrbcErr=$xml->Code;
        }else{
            $pCode="";
            $json = json_encode($xml);
            $arr = json_decode($json,true);
            foreach($arr['Item'] as $k => $v){
                if($k=="Id"){
                    $pId = $v;
                }
                if($k=="Code"){
                    $pErr = $v;

                }
            }
        }
        $error="";
        if(!$pId || $pErr){
            $error.= "エラーが発生しました。(Process: " . $pErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
        }
        if($hrbcErr){
            $error.= "エラー2が発生しました。(Process: " . $hrbcErr . $now . ")<br />画面をメモして管理者へ問い合わせて下さい。";
        }

        if(empty($error)) {
            //Email送信
            $this->load->library('email');
            $this->email->from('mypage@hurex.co.jp', 'HUREX株式会社');
            $this->email->to('mypage@hurex.co.jp.test-google-a.com');
            $this->email->subject('案件への辞退がありました。' . $posts["jobname"]);
            $this->email->message($posts["jobname"]);
            $this->email->send();

            $this->session->set_flashdata(array("msg"=>"応募を辞退しました。"));
            redirect($this->segment."/home");
        }else{
            $this->session->set_flashdata(array("msg"=>$error));
            redirect($this->segment."/home");

        }
    }

    //////////////////////////////////////////////////////////
    //新規登録
    //////////////////////////////////////////////////////////
    function regist()
    {
        $posts = $this->input->post();

        //idが設定されていて戻るじゃない場合は編集
        if(!empty($posts['id']) && empty($posts['mode'])){
            $data[$this->segment] = $this->db->get_where($this->database, array('id' => $posts['id']))->row();
        }else{
            $data[$this->segment] = (object) $posts;
            //idがない場合は空データ設定
            if(empty($data[$this->segment]->id)){
                $data[$this->segment]->id = "";
            }
        }

        $error = "";

        //data設定
        $data["render"] = $this->segment."/regist";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["segment"] = $this->segment;

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //登録情報確認
    //////////////////////////////////////////////////////////
    function confirm()
    {
        //暗号化ライブラリ
        $this->load->library('encrypt');

        //バリデーション実行
        $error = "";
        $tmp_error = 0;
        $e_msg = "";
        $error = $this->setValidation();

        $posts = $this->input->post();
        //オブジェクトへ変換
        $data[$this->segment] = (object)$posts;

        //同一IDとメールアドレスチェック
        $email = hash_hmac("sha256", $posts["mail1"], Salt);
        $row = $this->db->get_where($this->database, array('email' => $email, 'del' => 0))->row();
        if(!empty($row)){
            $error .= "<p>登録済みのメールアドレスです。</p>";
        }

        if($error !== ""){
            //data設定
            $data["render"] = $this->segment."/regist";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["sub_title"] = $this->sub_title;
            $data["msg"] = $error;
            $data["segment"] = $this->segment;

            $this->load->view("template", $data);
        }else{
            $result=array();
            $files = $_FILES;
            $cpt = count($files['userfile']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $filename=$files['userfile']['name'][$i];
                $require=0;
                $allow =  $this->allow;
                $overwrite=true;
                //@param(post, num, files, directory, require, filename)
                $result[]= $this->fileUpload(@$posts['file'.($i+1)], $i , $files, $this->segment, $require, $filename, $overwrite,$allow);

                if(!empty($result[$i]['error'])){
                    $tmp_error = 1;
                    $tmp_cnt = $i+1;
                    $e_msg .=  $result[$i]['error'] . "ファイル". $tmp_cnt . "<br />";
                }
            }
            /////////////////////////////////////////////////////////////////////////////
            //1枚目
            /////////////////////////////////////////////////////////////////////////////
            if(!empty($result[0]['upload_data']['file_size']) || !empty($posts['file1'])){
                (!empty($result[0]['upload_data']['file_size'])) ? $data[$this->segment]->file1=basename($result[0]['upload_data']['file_name']) : $data[$this->segment]->file1=$posts['file1'];
            }else{
                $data[$this->segment]->file1="";
            }
            if(@$posts['file1_del'] == "on"){
                $data[$this->segment]->file1="";
            }

            /////////////////////////////////////////////////////////////////////////////
            //2枚目以降
            /////////////////////////////////////////////////////////////////////////////
            for ($i = 1; $i < $cpt; $i++) {
                if(!empty($result[$i]['upload_data']['file_size']) || !empty($posts['file'.($i+1)])){
                    (!empty($result[$i]['upload_data']['file_size'])) ? $data[$this->segment]->{"file".($i+1)}=basename($result[$i]['upload_data']['file_name']) : $data[$this->segment]->{"file".($i+1)}=$posts['file'.($i+1)];
                }else{
                    $data[$this->segment]->{"file".($i+1)}="";
                }
                if(@$posts['file' . ($i+1) . '_del'] == "on"){
                    $data[$this->segment]->{"file".($i+1)}="";
                }
            }

            if($tmp_error != 1){
                //data設定
                $data["render"] = $this->segment."/confirm";
                $data["clean"] = $this->clean;
                $data["csrf"] = $this->_get_csrf_nonce();
                $data["msg"] = $error;
                $data["sub_title"] = $this->sub_title;
                $data["segment"] = $this->segment;

                $this->load->view("template", $data);
            }else{
                $error="";
                $error .= "<br />" . $e_msg;

                //data設定
                $data["render"] = $this->segment."/regist";
                $data["clean"] = $this->clean;
                $data["csrf"] = $this->_get_csrf_nonce();
                $data["sub_title"] = $this->sub_title;
                $data["msg"] = $error;
                $data["segment"] = $this->segment;

                $this->load->view("template", $data);
            }
        }
    }

    //////////////////////////////////////////////////////////
    //新規登録処理
    //////////////////////////////////////////////////////////
    function add()
    {
        //CSRFチェック
        if ($this->_valid_csrf_nonce() === FALSE)
        {
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/login");
        }

        //バリデーション実行
        $error = "";
        $error = $this->setValidation();

        $posts = $this->input->post();

        //同一IDとメールアドレスチェック
        //暗号化
        $email = hash_hmac("sha256", $posts["mail1"], Salt);
        $row = $this->db->get_where($this->database, array('email' => $email, 'del' => 0))->row();
        if(!empty($row)){
            $error .= "<p>登録済みのメールアドレスです。</p>";
        }

        if($error !== ""){
            //data設定
            $data["render"] = $this->segment."/regist";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["msg"] = $error;
            $data["segment"] = $this->segment;

            $this->load->view("template", $data);
        }else{
            //HRBC登録
            //$result = $this->HrbcRegist($posts);

            //HUREXのDBに登録
            if(empty($posts['id'])){
                $data['created'] = date('Y-m-d');
            }

            $data['id'] = $posts['id'];
            $pass = hash_hmac("sha256", $posts['password'], Salt);
            $salt = hash_hmac("sha256", Salt, Salt);
            $pass_unit = $salt . $email . $pass;
            $data['password'] = $pass_unit;
            $data['email'] = $email;

            $this->load->model($this->database.'_model');
            $tmpDb=$this->database."_model";
            $ret = $this->$tmpDb->save($data['id'], $data, "id");

            if(empty($ret)){
                //エラー処理
            }else{
                //登録成功
                $this->session->set_flashdata(array("msg"=>"登録が完了しました。"));
                redirect($this->segment."/complete");
            }
        }
    }

    //////////////////////////////////////////////////////////
    //登録完了画面
    //////////////////////////////////////////////////////////
    function complete(){
        //data設定
        $data["render"] = $this->segment."/complete";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["segment"] = $this->segment;

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //バリデーション
    //////////////////////////////////////////////////////////
    private function setValidation($validation=null)
    {
        $errors ="";
        $this->form_validation->set_rules("shimei", "氏名","required");
        $this->form_validation->set_rules("kana", "ふりがな","required");
        $this->form_validation->set_rules("year", "年","required");
        $this->form_validation->set_rules("month", "月","required");
        $this->form_validation->set_rules("day", "日","required");
        $this->form_validation->set_rules("sex", "性別","required");
        $this->form_validation->set_rules("pref", "住所","required");
        $this->form_validation->set_rules("tel1", "電話番号","required");
        $this->form_validation->set_rules("school_div_id", "最終学歴","required");
        $this->form_validation->set_rules("company_number", "経験社数","required");
        $this->form_validation->set_rules("jokyo", "就業状況","required");
        $this->form_validation->set_rules("mail1", "メールアドレス","required|valid_email");
        $this->form_validation->set_rules("password", "パスワード","required|min_length[12]|max_length[20]");
        $this->form_validation->set_rules("agree", "個人情報保護方針","required");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }

    //////////////////////////////////////////////////////////
    //HRBC登録
    //////////////////////////////////////////////////////////
    private function HrbcRegist($datas=array())
    {
        $result = $this->getHrbcMaster();

        ////////////////////////////////////////////////////////////////////
        // レジュメ等設定
        ////////////////////////////////////////////////////////////////////
        $dir = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/";
        include($dir . "system_entry/mimes.php");
        //データ１
        if(!empty($datas["file1"])){
            $file1_dir = $_SERVER["DOCUMENT_ROOT"] . "/../entry/member/" . $datas["file1"];
            //mime取得
            $extension1 = strtolower(pathinfo($file1_dir, PATHINFO_EXTENSION));
            $mime_type1  = $mimes[$extension1];
            if(is_array($mime_type1)){
                $mime_type1 = $mime_type1[0];
            }
            //base64化
            $fp = fopen($file1_dir, "r");
            $file1_tmp = fread($fp, filesize($file1_dir));
            fclose($fp);
            $file1_data = base64_encode($file1_tmp);
        }

        if(!empty($datas["file2"])){
            //データ２
            $file2_dir = $_SERVER["DOCUMENT_ROOT"] . "/../entry/member/" . $datas["file2"];
            //mime取得
            $extension2 = strtolower(pathinfo($file2_dir, PATHINFO_EXTENSION));
            $mime_type2  = $mimes[$extension2];
            if(is_array($mime_type2)){
                $mime_type2 = $mime_type2[0];
            }
            //base64化
            $fp = fopen($file2_dir, "r");
            $file2_tmp = fread($fp, filesize($file2_dir));
            fclose($fp);
            $file2_data = base64_encode($file2_tmp);
        }

        //データ３
        if(!empty($datas["file3"])){
            $file3_dir = $_SERVER["DOCUMENT_ROOT"] . "/../entry/member/" . $datas["file3"];
            //mime取得
            $extension3 = strtolower(pathinfo($file3_dir, PATHINFO_EXTENSION));
            $mime_type3  = $mimes[$extension3];
            if(is_array($mime_type3)){
                $mime_type3 = $mime_type3[0];
            }
            //base64化
            $fp = fopen($file3_dir, "r");
            $file3_tmp = fread($fp, filesize($file3_dir));
            fclose($fp);
            $file3_data = base64_encode($file3_tmp);
        }

        //////////////////////////////////////////////////////////////////////////////////////////
        // その他情報設定
        //////////////////////////////////////////////////////////////////////////////////////////

        //誕生日
        $birthday = $datas["year"] . "/" . $datas["month"] . "/" . $datas["day"];

        //性別
        foreach($result["gender"]['Item'] as $k=>$v) {
            if (!empty($v["Item"])) {
                foreach ($v['Item'] as $k2 => $v2) {
                    if ($datas["sex"] == $v2['Option.P_Name']) {
                        $gender = "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                    }
                }
            }
        }

        //都道府県
        foreach($result["pref"]['Item'] as $k=>$v){
            if (!empty($v["Item"])) {
                foreach($v['Item'] as $k2=>$v2){
                    if($datas["pref"]==$v2['Option.P_Name']){
                        $prefecture = "<" . htmlspecialchars($v2['Option.P_Alias'],ENT_QUOTES,'UTF-8') . "/>";
                    }
                }
            }
        }

        //学歴
        $school_div="";
        foreach($result["background"]['Item'] as $k=>$v){
            if (!empty($v["Item"])) {
                foreach($v['Item'] as $k2=>$v2) {
                    if ($datas["school_div_id"] == $v2['Option.P_Name']) {
                        $school_div = "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                    }
                }
            }
        }

        //就業状況
        foreach($result["work"]['Item'] as $k=>$v){
            if (!empty($v["Item"])) {
                foreach($v['Item'] as $k2=>$v2) {
                    if ($datas["jokyo"] == $v2['Option.P_Name']) {
                        $present_status = "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                    }
                }
            }
        }

        $todaydate = Date('Y/m/d');

        $hrbcparam = "【エントリー日】" . $todaydate;

        /////////////////////////////////////////////////////////////////////
        //// HRBC Person
        ////////////////////////////////////////////////////////////////////////
        $url =$this->hrbcUrl .  "candidate?partition=".$result["partition"];

        $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Candidate><Item><Person.P_Id>-1</Person.P_Id><Person.P_Owner>1</Person.P_Owner><Person.P_Name>' . $datas["shimei"] . '</Person.P_Name><Person.P_Reading>' . $datas["kana"] . '</Person.P_Reading><Person.P_Mobile>' . $datas["tel1"] .'</Person.P_Mobile><Person.P_Street>' . $datas["pref"] . '</Person.P_Street><Person.P_Mail>' . $datas["mail1"] .'</Person.P_Mail></Item></Candidate>';

        $header = array(
            "Content-Type: application/xml; charset=UTF-8",
            "X-porters-hrbc-oauth-token: " . $result['token']
        );

        $options = array('http' => array(
            'method' => 'POST',
            'content' => $xml,
            'header' => implode("\r\n", $header)
        ));

        $xml = file_get_contents($url, false, stream_context_create($options));

        //xml解析
        $pErr="";
        $pId = "";
        $xml = simplexml_load_string($xml);
        if($xml->Code!=0){
            $pErr=$xml->Code;
        }else{
            $pCode="";
            $json = json_encode($xml);
            $arr = json_decode($json,true);
            foreach($arr['Item'] as $k => $v){
                if($k=="Id"){
                    $pId = $v;
                }
                if($k=="Code"){
                    $pErr = $v;

                }
            }
        }

        $error="";
        if(!$pId || $pErr){
            $error.= "エラーが発生しました。(Person: " . $pErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
            echo $error;
            exit;
        }

        if(empty($error)){
            ///////////////////////////////////////////////////////////////
            // HRBC Resume
            ///////////////////////////////////////////////////////////////
            //メールアドレス暗号化
            $email_encryption = hash_hmac("sha256", $datas["mail1"], Salt);

            $url = $this->hrbcUrl . "resume?partition=".$result["partition"];

            //※レジュメにメールとTELを入れるとエラーになる
            $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>-1</Resume.P_Id><Resume.P_Owner>1</Resume.P_Owner><Resume.P_Candidate>'. $pId .'</Resume.P_Candidate><Resume.P_Name>' . $datas["shimei"] . '</Resume.P_Name><Resume.U_91DBD8B96BAAD6B33B538CF2494377>' . $datas["kana"] . '</Resume.U_91DBD8B96BAAD6B33B538CF2494377><Resume.P_DateOfBirth>' . $birthday . '</Resume.P_DateOfBirth><Resume.P_Gender>' . $gender . '</Resume.P_Gender><Resume.P_CurrentStatus>' . $present_status . '</Resume.P_CurrentStatus><Resume.P_Memo>' . $datas["comment"] .'</Resume.P_Memo><Resume.P_ChangeJobsCount>' . $datas["company_number"] . '</Resume.P_ChangeJobsCount><Resume.U_7F3DF7F61EEF0679AA432646777832>' . $prefecture .'</Resume.U_7F3DF7F61EEF0679AA432646777832><Resume.U_673275A4DA445C9B23AC18BDD7C4DC>' . $school_div . '</Resume.U_673275A4DA445C9B23AC18BDD7C4DC><Resume.U_13C45F7FFAFC1AB430E6038C76125A>' . $hrbcparam . '</Resume.U_13C45F7FFAFC1AB430E6038C76125A><Resume.U_4BC5948ACAB7D3FF6DDD15D4EB4ADB>' . $email_encryption . '</Resume.U_4BC5948ACAB7D3FF6DDD15D4EB4ADB> </Item></Resume>';


            $header = array(
                "Content-Type: application/xml; charset=UTF-8",
                "X-porters-hrbc-oauth-token: ".$result["token"]
            );

            $options = array('http' => array(
                'method' => 'POST',
                'content' => $xml,
                'header' => implode("\r\n", $header)
            ));
            $xml = file_get_contents($url, false, stream_context_create($options));

            //xml解析
            $rErr="";
            $rId = "";
            $xml = simplexml_load_string($xml);

            if($xml->Code!=0){
                $rErr=$xml->Code;
            }else{
                $rCode="";
                $json = json_encode($xml);
                $arr = json_decode($json,true);
                foreach($arr['Item'] as $k => $v){
                    if($k=="Id"){
                        $rId = $v;
                    }
                    if($k=="Code"){
                        $rErr = $v;
                    }
                }
            }
            if(!$rId || $rErr){
                $error.= "エラーが発生しました。(Resume: ".$rErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
                echo $error;
                exit;
            }
        }

        if(empty($error)) {
            ///////////////////////////////////////////////////////////////
            // HRBC Attachment
            ///////////////////////////////////////////////////////////////
            if (!empty($datas["file1"]) || !empty($datas["file2"]) || !empty($datas["file3"])) {

                $url = "https://api-hrbc-jp.porterscloud.com/v1/attachment?partition=" . $result["partition"];

                //file1
                $param = "";
                if (!empty($datas["file1"])) {
                    $param .= '<Item><Id>-1</Id><Resource>17</Resource><ResourceId>' . $rId . '</ResourceId><FileName>' . $datas["file1"] . '</FileName><Content>' . $file1_data . '</Content><ContentType>' . $mime_type1 . '</ContentType></Item>';
                }
                if (!empty($datas["file2"])) {
                    $param .= '<Item><Id>-1</Id><Resource>17</Resource><ResourceId>' . $rId . '</ResourceId><FileName>' . $datas["file2"] . '</FileName><Content>' . $file2_data . '</Content><ContentType>' . $mime_type2 . '</ContentType></Item>';
                }
                if (!empty($datas["file3"])) {
                    $param .= '<Item><Id>-1</Id><Resource>17</Resource><ResourceId>' . $rId . '</ResourceId><FileName>' . $datas["file3"] . '</FileName><Content>' . $file3_data . '</Content><ContentType>' . $mime_type3 . '</ContentType></Item>';
                }


                $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Attachment>' . $param . '</Attachment>';

                $header = array(
                    "Content-Type: application/xml; charset=UTF-8",
                    "X-porters-hrbc-oauth-token: ".$result["token"]
                );

                $options = array('http' => array(
                    'method' => 'POST',
                    'content' => $xml,
                    'header' => implode("\r\n", $header)
                ));
                $xml = file_get_contents($url, false, stream_context_create($options));

                //xml解析
                $aErr = "";
                $aId = "";
                $xml = simplexml_load_string($xml);

                $aCode = "";
                $json = json_encode($xml);
                $arr = json_decode($json, true);
                if(!empty($arr["Item"])){
                    foreach ($arr['Item'] as $k => $v) {
                        if ($k == "Id") {
                            $aId = $v;
                        }
                        if ($k == "Code") {
                            $aErr = $v;
                        }
                    }
                }

                if (!$aId) {
                    $error .= "エラーが発生しました。(Attachment: " . $aErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
                }
            }
        }
    }

    //////////////////////////////////////////////////////////
    //更新
    //////////////////////////////////////////////////////////
    function edit()
    {
        $posts = $this->input->post();

        $login_id = $this->session->userdata('login_id');

        $role = $this->session->userdata('login_role');

        //マスターと自分以外は編集不可
        if($role != 1)
        {
            if($login_id != $posts['id']){
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
                redirect($this->segment."/home");
            }
        }

        //idが設定されていて戻るじゃない場合は編集
        if(!empty($posts['id']) && empty($posts['mode'])){
            $data[$this->segment] = $this->db->get_where($this->database, array('id' => $posts['id']))->row();
        }else{
            $data[$this->segment] = (object) $posts;
            //idがない場合は空データ設定
            if(empty($data[$this->segment]->id)){
                $data[$this->segment]->id = "";
            }
        }

        $error = "";

        //data設定
        $data["render"] = $this->segment."/edit";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["segment"] = $this->segment;

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //ユーザー情報更新
    //////////////////////////////////////////////////////////
    function update()
    {
        $posts = $this->input->post();

        $login_id = $this->session->userdata('login_id');

        $role = $this->session->userdata('login_role');

        //マスターと自分以外は編集不可
        if($role != 1)
        {
            if($login_id != $posts['id']){
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
                redirect($this->segment."/home");
            }
        }

        //CSRFチェック
        if ($this->_valid_csrf_nonce() === FALSE)
        {
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/home");
        }

        //バリデーション実行
        $error = "";
        $error = $this->setUpdateValidation();

        $posts = $this->input->post();

        //同一メールアドレスチェック
        $sameEmail = $this->db->get_where($this->database, array('id !=' => $posts['id'], 'email' => $posts['email'], 'del'=>0))->row();
        if($sameEmail) $error .= "<p>登録済みのメールアドレスです。</p>";

        if($error !== ""){
            //data設定
            $data[$this->segment] =  (object) $posts;
            $data["render"] = $this->segment."/edit";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["msg"] = $error;
            $data["segment"] = $this->segment;

            $this->load->view("template", $data);
        }else{
            $data['id'] = $posts['id'];
            $data['email'] = $posts['email'];

            $this->load->model($this->database.'_model');
            $tmpDb=$this->database."_model";
            $ret = $this->$tmpDb->save($data['id'], $data, "id");

            if(empty($ret)){
                //エラー処理
            }else{
                //登録成功
                $this->session->set_flashdata(array("msg"=>"登録が完了しました。"));
                redirect($this->segment."/home");
            }
        }

    }

    //////////////////////////////////////////////////////////
    //バリデーション
    //////////////////////////////////////////////////////////
    private function setUpdateValidation($validation=null)
    {
        $errors ="";
        $this->form_validation->set_rules("email", "メールアドレス","required|valid_email");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }

    //////////////////////////////////////////////////////////
    //パスワード変更
    //////////////////////////////////////////////////////////
    function change_password()
    {
        $posts = $this->input->post();

        $login_id = $this->session->userdata('login_id');

        $role = $this->session->userdata('login_role');

        //マスターと自分以外は編集不可
        if($role != 1)
        {
            if($login_id != $posts['id']){
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
                redirect($this->segment."/login");
            }
        }

        //idが設定されていて戻るじゃない場合は編集
        if(!empty($posts['id']) && empty($posts['mode'])){
            $data[$this->segment] = $this->db->get_where($this->database, array('id' => $posts['id']))->row();
        }else{
            $data[$this->segment] = (object) $posts;
            //idがない場合は空データ設定
            if(empty($data[$this->segment]->id)){
                $data[$this->segment]->id = "";
            }
        }

        $error = "";

        //data設定
        $data["render"] = $this->segment."/change_password";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["segment"] = $this->segment;

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //パスワード更新処理
    //////////////////////////////////////////////////////////
    public function reset_password($code = NULL)
    {
        //CSRFチェック
        if ($this->_valid_csrf_nonce() === FALSE)
        {
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/home");
        }

        //バリデーション実行
        $error = "";
        $error = $this->setPasswordValidation();

        $posts = $this->input->post();

        $login_id = $this->session->userdata('login_id');

        $role = $this->session->userdata('login_role');

        //マスターと自分以外は編集不可
        if($role != 1)
        {
            if($login_id != $posts['id']){
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
                redirect($this->segment."/home");
            }
        }

        if($posts['password'] != $posts['password_check']){
            $error.="<p>パスワードが一致しません。</p>";
        }

        if($error !== ""){
            //data設定
            $data[$this->segment] =  (object) $posts;
            $data["render"] = $this->segment."/change_password";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["msg"] = $error;
            $data["segment"] = $this->segment;

            $this->load->view("template", $data);
        }else{
            //パスワード再発行
            $pass = hash_hmac("sha256", $posts['password'], Salt);
            $salt = hash_hmac("sha256", Salt, Salt);
            $pass_unit = $salt . $posts["email"] . $pass;
            $data['password'] = $pass_unit;

            $this->load->model($this->database.'_model');
            $tmpDb=$this->database."_model";
            $ret = $this->$tmpDb->save($posts['id'], $data, "id");

            if(empty($ret)){
                //エラー処理
            }else{
                //登録成功
                $this->session->set_flashdata(array("msg"=>"パスワードの更新が完了しました。"));
                redirect($this->segment."/home");
            }
        }

    }

    //////////////////////////////////////////////////////////
    //パスワードバリデーション
    //////////////////////////////////////////////////////////
    private function setPasswordValidation($validation=null)
    {

        $errors ="";
        $this->form_validation->set_rules("password", "パスワード","required|min_length[12]|max_length[20]");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }

    //////////////////////////////////////////////////////////
    //登録情報削除
    //////////////////////////////////////////////////////////
    public function delete()
    {
        $posts = $this->input->post();

        //CSRFチェック
        if ($this->_valid_csrf_nonce() === FALSE)
        {
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/home");
        }

        $posts = $this->input->post();

        $login_id = $this->session->userdata('login_id');
        $role = $this->session->userdata('login_role');

        //マスター以外は編集不可
        if($role != 1)
        {
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/home");
        }

        //マスターIDの場合エラー
        if($posts['id']==1){
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/home");
        }

        if($posts['id'] && is_numeric($posts['id'])){
            $data = array(
                'del' => 1
            );
            $this->db->where('id', $posts['id']);
            $ret = $this->db->update($this->database, $data);

            if($ret){
                //削除成功
                $this->session->set_flashdata(array("msg"=>"削除が完了しました。"));
                redirect($this->segment."/home");
            }else{
                $this->session->set_flashdata(array("msg"=>"削除に失敗しました。"));
                redirect($this->segment."/home");
            }
        }else{
            redirect($this->segment."/login");
        }
    }

    //////////////////////////////////////////////////////////
    //パスワード忘れ
    //////////////////////////////////////////////////////////
    function forget_password()
    {
        $error="";
        $data["render"] = $this->segment."/forget_password";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $error;
        $data["segment"] = $this->segment;

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //パスワード再発行メール
    //////////////////////////////////////////////////////////
    function send_mail()
    {
        $posts = $this->input->post();

        //バリデーション実行
        $error = "";
        $error = $this->setSendmailValidation();

        $sameEmail="";
        if(empty($error)){
            $email = hash_hmac("sha256", $posts['email'], Salt);
            $sameEmail = $this->db->get_where($this->database, array('email' => $email, 'del'=>0))->row();

            if($sameEmail){
                ///////////////////////////////////////////////////////////////////////////
                //HRBCからメールアドレス抽出
                ///////////////////////////////////////////////////////////////////////////
                $result = $this->getHrbcMaster();

                $data = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.U_4BC5948ACAB7D3FF6DDD15D4EB4ADB:full=' . $email, 'field'=>'Resume.P_Candidate(Person.P_Mail)'));
                $url = "https://api-hrbc-jp.porterscloud.com/v1/resume?" . $data;
                $opts = array(
                    'http'=>array(
                        'method' => 'GET',
                        'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                            . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                    )
                );
                $context = stream_context_create($opts);

                $xml = file_get_contents($url, false, $context);
                $xml = simplexml_load_string($xml);
                if($xml->Code!=0){
                    $pErr=$xml->Code;
                    print("エラーが発生しました。<br />コード：" . $pErr);
                    exit;
                }

                //データ設定
                $json = json_encode($xml);

                $token = $this->generatePassword();
                //DBにforget_passwordトークン登録
                $data = array(
                    'forget_password' => $token,
                    'forget_password_limit' => strtotime("tomorrow")
                );

                $this->db->where('email', $email);
                $ret = $this->db->update($this->database, $data);

                $url = base_url() . $this->segment."/create_password/?re=" . $token;

                $this->load->library('email');

                $this->email->from(kanriMail, kanriMailTitle);
                $this->email->to($posts['email']);

                $this->email->subject('パスワード再発行メール');
                $this->email->message("以下のURLにアクセスしてください。\n" . $url);

                $this->email->send();

                $error.="<p>パスワード再発行メールを送信しました。</p>";
            }else{
                $error.="<p>メールアドレスが存在しません。</p>";
            }
        }

        //data設定
        $data[$this->segment] =  (object) $posts;
        $data["render"] = $this->segment."/forget_password";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $error;
        $data["segment"] = $this->segment;

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //メールアドレスチェック
    //////////////////////////////////////////////////////////
    private function setSendmailValidation($validation=null)
    {

        $errors ="";
        $this->form_validation->set_rules("email", "メールアドレス","required|valid_email");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }

    //////////////////////////////////////////////////////////
    //パスワード再発行画面
    //////////////////////////////////////////////////////////
    function create_password()
    {
        $code = $this->input->get('re', TRUE);
        $data[$this->segment] = $this->db->get_where($this->database, array('forget_password' => $code, 'del'=>0))->row();

        if(empty($data[$this->segment])){
            redirect($this->segment."/login");
        }

        $error="";
        $data["render"] = $this->segment."/create_password";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $error;
        $data["segment"] = $this->segment;

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //パスワード再発行
    //////////////////////////////////////////////////////////
    function new_password()
    {
        //CSRFチェック
        if ($this->_valid_csrf_nonce() === FALSE)
        {
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/login");
        }

        $posts = $this->input->post();

        //バリデーション実行
        $error = "";
        $error = $this->setNewPasswordValidation();

        if($posts['password'] != $posts['password_check']){
            $error.="<p>パスワードが一致しません。</p>";
        }

        $user = $this->db->get_where($this->database, array('id' => $posts['id'], 'del'=>0))->row();
        $now = strtotime("now");

        if($user->forget_password_limit < $now){
            $this->session->set_flashdata(array("msg"=>"有効期限が切れました。再度パスワードの再発行の手続きをして下さい。"));
            redirect($this->segment."/login");
        }

        if($error !== ""){
            //data設定
            $data[$this->segment] =  (object) $posts;
            $data["render"] = $this->segment."/create_password";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["msg"] = $error;
            $data["segment"] = $this->segment;

            $this->load->view("template", $data);
        }else{
            //パスワード再発行
            $pass = hash_hmac("sha256", $posts['password'], Salt);
            $user = hash_hmac("sha256", $posts['email'], Salt);
            $salt = hash_hmac("sha256", Salt, Salt);
            $pass_unit = $salt . $posts["email"] . $pass;
            $data['password'] = $pass_unit;
            $data['forget_password_limit'] = "";
            $data['forget_password'] = "";

            $this->load->model($this->database.'_model');
            $tmpDb=$this->database."_model";
            $ret = $this->$tmpDb->save($posts['id'], $data, "id");

            if(empty($ret)){
                //エラー処理
            }else{
                //登録成功
                $this->session->set_flashdata(array("msg"=>"パスワードの更新が完了しました。"));
                redirect($this->segment."/login");
            }
        }

    }

    //////////////////////////////////////////////////////////
    //バリデーション
    //////////////////////////////////////////////////////////
    private function setNewPasswordValidation($validation=null)
    {
        $errors ="";
        $this->form_validation->set_rules("password", "パスワード","required|min_length[12]|max_length[20]");
        $this->form_validation->set_rules("password_check", "パスワード(確認用）","required|min_length[12]|max_length[20]");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }
}
/*
////////////////////////////////////////////////////////////////////////////////////////////
// テストでプロセスの変更（選考プロセスを変える
////////////////////////////////////////////////////////////////////////////////////////////）
//送信パラメータ
$param = http_build_query(array('partition'=>$result["partition"], 'count'=>200 , 'alias'=>'Option.P_ProcessPhase','enabled'=>1));

$url = $this->hrbcUrl . "option?" . $param;
$opts = array(
'http'=>array(
    'method' => 'GET',
    'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
)
);
$context = stream_context_create($opts);

$xml = file_get_contents($url, false, $context);
$xml = simplexml_load_string($xml);
if($xml->Code!=0){
$pErr=$xml->Code;
print("エラーが発生しました。<br />コード：" . $pErr);
exit;
}

//データ設定
$json = json_encode($xml);
$tmp = json_decode($json,true);

foreach($tmp['Item']['Items'] as $k=>$v){
foreach($v as $k2 => $v2){
    $phaze[htmlspecialchars($v2['Option.P_Id'],ENT_QUOTES,'UTF-8')] = htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');
}
}
print_r($tmp);

/////////////////////////////////////////////////////////////////////////////////////////////////

$url =$this->hrbcUrl .  "process?partition=".$result["partition"];

$xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>97857</Process.P_Id><Process.P_Phase><Option.P_ProcessSoundOut /></Process.P_Phase></Item></Process>';

$header = array(
"Content-Type: application/xml; charset=UTF-8",
"X-porters-hrbc-oauth-token: " . $result['token']
);

$options = array('http' => array(
'method' => 'POST',
'content' => $xml,
'header' => implode("\r\n", $header)
));

$xml = file_get_contents($url, false, stream_context_create($options));
print_r($xml);
//xml解析
$pErr="";
$pId = "";
$xml = simplexml_load_string($xml);
if($xml->Code!=0){
$pErr=$xml->Code;
}else{
$pCode="";
$json = json_encode($xml);
$arr = json_decode($json,true);
foreach($arr['Item'] as $k => $v){
    if($k=="Id"){
        $pId = $v;
    }
    if($k=="Code"){
        $pErr = $v;

    }
}
}

$error="";
if(!$pId || $pErr){
$error.= "エラーが発生しました。(Person: " . $pErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
}

if(empty($error)) {
}
exit;
/////////////////////////////////////////////////////////////////////////////////////////////////
*/