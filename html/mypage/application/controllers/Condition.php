<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Condition extends MY_Controller
{

    protected $page_limit = 20;

    //タイトル
    protected $sub_title = "希望条件";

    //segment
    protected $segment = "condition";

    //database
    protected $database = "members";
    protected $databaseJob = "jobs";
    protected $databaseClient = "clients";

    //画像がonの時 許可ファイル
    protected $allow = 'doc|xls|ppt|pptx|pdf|docx|xlsx|html|txt|htm';

    //HRBC URL
    protected $hrbcUrl = "https://api-hrbc-jp.porterscloud.com/v1/";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('form_validation', 'encrypt'));
        $this->load->helper(array('url','cookie'));
    }

    // redirect if needed, otherwise display the user list
    function index()
    {
        $this->create();
    }

    //////////////////////////////////////////////////////////
    //登録情報確認
    //////////////////////////////////////////////////////////
    function home()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $tmp_data[$this->segment] = $this->db->get_where($this->database, array('id' => $login_id, 'del' => 0))->row();
        $data[$this->segment] = new stdClass;

        $data = $this->getKibouInfo($tmp_data);

        //data設定
        $data["render"] = $this->segment."/home";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["segment"] = $this->segment;
        $data["body_id"] = "condition";

        ////////////////////////////////////////////////////////////////////
        //// 閲覧履歴
        //////////////////////////////////////////////////////////////////////
        $looks = $this->getLooks($login_id);
        $data["looks"] = $looks;

        $this->load->view("template_search", $data);
    }

    //////////////////////////////////////////////////////////
    //新規登録
    //////////////////////////////////////////////////////////
    function create()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $posts = $this->input->post();
        $data= array();

        //postなしの場合はDBから取得
        if(empty($posts)){
            $tmp_data[$this->segment] = $this->db->get_where($this->database, array('id' => $login_id, 'del' => 0))->row();
            $data[$this->segment] = new stdClass;

            $data = $this->getKibouInfo($tmp_data);
        }else{
            ///////////////////////////////////////////////////////
            //postありは戻るで編集
            ///////////////////////////////////////////////////////
            $data[$this->segment] = (object) $posts;

            //idがない場合は空データ設定
            if(empty($data[$this->segment]->id)){
                $data[$this->segment]->id = "";
            }
        }

        $error = "";

        //data設定
        $data["render"] = $this->segment."/create";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["segment"] = $this->segment;
        $data["body_id"] = "registration";

        ////////////////////////////////////////////////////////////////////
        //// 閲覧履歴
        //////////////////////////////////////////////////////////////////////
        $looks = $this->getLooks($login_id);
        $data["looks"] = $looks;

        $this->load->view("template_search", $data);
    }

    //////////////////////////////////////////////////////////
    //登録情報確認
    //////////////////////////////////////////////////////////
    function confirm()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        //バリデーション実行
        $error = "";
        $tmp_error = 0;
        $e_msg = "";
        $error = $this->setValidation();

        $posts = $this->input->post();
        //オブジェクトへ変換
        $data[$this->segment] = (object)$posts;


        ////////////////////////////////////////////////////////////////////
        //// 閲覧履歴
        //////////////////////////////////////////////////////////////////////
        $looks = $this->getLooks($login_id);
        $data["looks"] = $looks;

        if($error !== ""){
            //data設定
            $data["render"] = $this->segment."/create";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["sub_title"] = $this->sub_title;
            $data["msg"] = $error;
            $data["segment"] = $this->segment;

            $this->load->view("template_search", $data);
        }else{
            //data設定
            $data["render"] = $this->segment."/confirm";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["msg"] = $error;
            $data["sub_title"] = $this->sub_title;
            $data["segment"] = $this->segment;
            $data["body_id"] = "registration";

            $this->load->view("template_search", $data);
        }
    }

    //////////////////////////////////////////////////////////
    //新規登録処理
    //////////////////////////////////////////////////////////
    function add()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        //CSRFチェック
        if ($this->_valid_csrf_nonce() === FALSE)
        {
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect("user/login/");
        }
        //バリデーション実行
        $error = "";
        $error = $this->setValidation();

        $posts = $this->input->post();


        if($error !== ""){
            //data設定
            $data["render"] = $this->segment."/regist";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["msg"] = $error;
            $data["segment"] = $this->segment;

            $this->load->view("template_search", $data);
        }else{
            //HRBCとの連動
            $result = $this->HrbcRegist($posts);

            if(!empty($result)){
                //エラー処理
                //エラーメール送信
                $subject = "【希望条件更新失敗　code:condition02】 user:" . $login_id . " error:" . $result;
                $message = "希望条件更新失敗に失敗しました。 user:" . $login_id . " error:" . $result;
                $this->errMail($subject, $message);

                //登録失敗
                $this->session->set_flashdata(array("msg"=>"登録に失敗しました。管理者へ問い合わせください。"));
                redirect($this->segment."/complete/");
            }else{
                //登録成功
                $this->session->set_flashdata(array("msg"=>"登録が完了しました。"));
                redirect($this->segment."/complete/");
            }
        }
    }

    //////////////////////////////////////////////////////////
    //登録完了画面
    //////////////////////////////////////////////////////////
    function complete(){
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        //data設定
        $data["render"] = $this->segment."/complete";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["segment"] = $this->segment;
        $data["body_id"] = "registration";

        ////////////////////////////////////////////////////////////////////
        //// 閲覧履歴
        //////////////////////////////////////////////////////////////////////
        $looks = $this->getLooks($login_id);
        $data["looks"] = $looks;

        $this->load->view("template_search", $data);
    }

    //////////////////////////////////////////////////////////
    //HRBC登録
    //////////////////////////////////////////////////////////
    private function HrbcRegist($datas=array())
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $ret = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();

        $person_id = $ret->hrbc_person_id;
        if(empty($person_id)){
            redirect($this->segment . "/login/");
        }
        $resume_id = $ret->hrbc_resume_id;
        if(empty($resume_id)){
            redirect($this->segment . "/login/");
        }

        $result = $this->getHrbcMaster();
        $error="";

        //希望勤務地1
        $expectarea1="";
        $tflg=0;
        foreach($result["area"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"]["Item"])){
                        foreach($v2['Items']['Item'] as $k3=>$v3) {
                            if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)){
                                if(!empty($datas["kibouarea1"])){
                                    if ($datas["kibouarea1"] == $v3['Option.P_Id']) {
                                        $expectarea1 = "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }else{
                                if($tflg==0){
                                    if(!empty($datas["kibouarea1"])){
                                        if ($datas["kibouarea1"] == $v2['Items']['Item']['Option.P_Id']) {
                                            $expectarea1 = "<" . htmlspecialchars($v2['Items']['Item']['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                        }
                                    }
                                    $tflg=1;
                                }
                            }
                        }
                    }
                }
            }
        }

        //希望勤務地2
        $expectarea2="";
        $tflg=0;
        foreach($result["area"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"]["Item"])){
                        foreach($v2['Items']['Item'] as $k3=>$v3) {
                            if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)){
                                if(!empty($datas["kibouarea2"])){
                                    if ($datas["kibouarea2"] == $v3['Option.P_Id']) {
                                        $expectarea2 = "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }else{
                                if($tflg==0){
                                    if(!empty($datas["kibouarea2"])){
                                        if ($datas["kibouarea2"] == $v2['Items']['Item']['Option.P_Id']) {
                                            $expectarea2 = "<" . htmlspecialchars($v2['Items']['Item']['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                        }
                                    }
                                    $tflg=1;
                                }
                            }
                        }
                    }
                }
            }
        }

        //希望勤務地3
        $expectarea3="";
        $tflg=0;
        foreach($result["area"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"]["Item"])){
                        foreach($v2['Items']['Item'] as $k3=>$v3) {
                            if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)){
                                if(!empty($datas["kibouarea3"])){
                                    if ($datas["kibouarea3"] == $v3['Option.P_Id']) {
                                        $expectarea3 = "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }else{
                                if($tflg==0){
                                    if(!empty($datas["kibouarea3"])){
                                        if ($datas["kibouarea3"] == $v2['Items']['Item']['Option.P_Id']) {
                                            $expectarea3 = "<" . htmlspecialchars($v2['Items']['Item']['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                        }
                                    }
                                    $tflg=1;
                                }
                            }
                        }
                    }
                }
            }
        }

        //転勤
        $tenkin="";
        if(!empty($datas["tenkin"])){
            foreach($result["tenkin"]['Item'] as $k=>$v){
                if (!empty($v["Item"])) {
                    foreach($v['Item'] as $k2=>$v2) {
                        foreach($datas["tenkin"] as $tk => $v){
                            if ($v == $v2['Option.P_Name']) {
                                $tenkin .= "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                            }
                        }
                    }
                }
            }
        }

        //希望職種
        $kiboujob="";
        foreach($result["jobcategory"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($datas["kiboujob"])){
                        foreach($datas["kiboujob"] as $kk => $kv){
                            if ($kv == $v2['Option.P_Id']) {
                                if(!empty($v2["Items"]["Item"])){
                                    foreach($v2['Items']['Item'] as $k3=>$v3) {
                                        $kiboujob .= "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //希望年収
        $income="";
        if(!empty($datas["income"])){
            foreach($result["income"]['Item'] as $k=>$v){
                if (!empty($v["Item"])) {
                    foreach($v['Item'] as $k2=>$v2) {
                        if(!empty($datas["income"])){
                            if ($datas["income"] == $v2['Option.P_Id']) {
                                $income .= "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                            }
                        }
                    }
                }
            }
        }

        $todaydate = Date('Y/m/d H:i:s');

        ///////////////////////////////////////////////////////////////
        // HRBC Resume
        ///////////////////////////////////////////////////////////////
        $error="";
        $url = $this->hrbcUrl . "resume?partition=".$result["partition"];

        //※レジュメにメールとTELを入れるとエラーになる
        $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>1</Resume.P_Owner><Resume.P_Candidate>'. $person_id .'</Resume.P_Candidate><Resume.U_83F3DDBA6DEC80CE66E21A04631FA7>' . $income . '</Resume.U_83F3DDBA6DEC80CE66E21A04631FA7><Resume.U_EC012BD105993E9656D208951323F4>' . $tenkin . '</Resume.U_EC012BD105993E9656D208951323F4><Resume.U_14551B3DE0AAA0F98964734D422811>'.$kiboujob.'</Resume.U_14551B3DE0AAA0F98964734D422811><Resume.U_F3312A78116CAA30201FFFFC4CBC33>' . $expectarea1 . '</Resume.U_F3312A78116CAA30201FFFFC4CBC33><Resume.U_3CA45BBA1FBA2B4D0F2E7A6D25DD7D>' . $expectarea2 . '</Resume.U_3CA45BBA1FBA2B4D0F2E7A6D25DD7D><Resume.U_EF1BA8D1B218E6A4F11BF5EA910E36>' . $expectarea3 . '</Resume.U_EF1BA8D1B218E6A4F11BF5EA910E36></Item></Resume>';
        $tmp_xml = $xml;

        /*
        $header = array(
            "Content-Type: application/xml; charset=UTF-8",
            "X-porters-hrbc-oauth-token: ".$result["token"]
        );

        $options = array('http' => array(
            'method' => 'POST',
            'content' => $xml,
            'header' => implode("\r\n", $header)
        ));

        if($xml = @file_get_contents($url, false, stream_context_create($options))){
            //ここにデータ取得が成功した時の処理
        }else{
            preg_match("/[0-9]{3}/", $http_response_header[0], $stcode);
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $c_code = explode(' ', $http_response_header[0]);  //「$c_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($c_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 signup hrbcregist2 "));
                        redirect($this->segment."/create/");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 signup hrbcregist2"));
                        redirect($this->segment."/create/");
                        break;

                    //その他のエラーの場合
                    default:
                        //$this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other signup hrbcregist2"));
                        //redirect($this->segment."/create");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout signup hrbcregist2"));
                redirect($this->segment."/create/");
            }
        }
        */
        $options = array
    (
        CURLOPT_URL            => $url,
        CURLOPT_POST           => true,
        CURLOPT_POSTFIELDS     => $xml,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => "UTF-8",
        CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result['token']),
        CURLINFO_HEADER_OUT    => true,
    );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

        //xml解析
        $rErr="";
        $rId = "";

        $this->writeRegistLog($tmp_xml, $xml, "condition01");

        if($xml->Code!=0){
            $rErr=$xml->Code;
        }else{
            $rCode="";
            $json = json_encode($xml);
            $arr = json_decode($json,true);
            foreach($arr['Item'] as $k => $v){
                if($k=="Id"){
                    $rId = $v;
                }
                if($k=="Code"){
                    $rErr = $v;
                }
            }
        }
        if(!$rId || $rErr){
            $error.= "エラーが発生しました。(Resume: ".$rErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";

            $subject = "【code:condition5】" . $error;
            $message = $rErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $error));
            redirect("err/error_404/");
            //exit;
        }

        //配列データの変換
        $karea1 = "";
        if(!empty($datas["kibouarea1"])){
            $karea1 = $datas["kibouarea1"];
        }
        $karea2 = "";
        if(!empty($datas["kibouarea2"])){
            $karea2 = $datas["kibouarea2"];
        }
        $karea3 = "";
        if(!empty($datas["kibouarea3"])){
            $karea3 = $datas["kibouarea3"];
        }

        $kjobs = "";
        if(!empty($datas["kiboujob"])){
            foreach($datas["kiboujob"] as $k => $v){
                if($k!=0) $kjobs.=",";
                $kjobs.=$v;
            }
        }

        $tenkins = "";
        if(!empty($datas["tenkin"])){
            foreach($datas["tenkin"] as $k => $v){
                if($k!=0) $tenkins.=",";
                $tenkins.=$v;
            }
        }


        $entry="";
        $job_id="";
        if(!empty($datas["param"])){
            $entry = str_replace("'","", $datas["param"]);
            $entry = explode("|",$entry);
        }
        if(!empty($entry[1]) && is_numeric($entry[1])){
            $job_id = $entry[1];
        }else{
            $job_id="";
        }

        $now = Date('Y-m-d H:i:s');
        $data = array(
            'hrbc_person_id' => $person_id,
            'hrbc_resume_id' => $resume_id,
            'kibou_area1' => $karea1,
            'kibou_area2' => $karea2,
            'kibou_area3' => $karea3,
            'kibou_job' => $kjobs,
            'tenkin' => $tenkins,
            'income' => $datas["income"]
        );
        $where = array(
            'id' => $ret->id
        );
        $ret = $this->db->update($this->database, $data, $where);
        if(empty($ret)){
            //エラーメール送信
            $subject = "【希望条件更新失敗　code:condition04】 user:" . $login_id . " error:DBの更新に失敗";
            $message = "希望条件更新失敗に失敗しました。 user:" . $login_id . " error:DBの更新に失敗";
            $this->errMail($subject, $message);

            $error = "エラーが発生しました。";
        }

        return  $error;
    }

    function getKibouInfo($tmp_data)
    {
        $data[$this->segment] = new stdClass;

        $tmpkiboujob = $tmp_data[$this->segment]->kibou_job;
        $kibouAry=array();
        if(!empty($tmpkiboujob)){
            $tmpkiboujob = explode(",",$tmpkiboujob);
            if(!empty($tmpkiboujob)){
                foreach($tmpkiboujob as $k=>$v){
                    $kibouAry[] = $v;
                }
            }
        }
        $data[$this->segment]->kiboujob = $kibouAry;

        //希望エリア1～3
        $tmpkibouarea1 = $tmp_data[$this->segment]->kibou_area1;
        $data[$this->segment]->kibouarea1= $tmpkibouarea1;

        $tmpkibouarea2 = $tmp_data[$this->segment]->kibou_area2;
        $data[$this->segment]->kibouarea2= $tmpkibouarea2;

        $tmpkibouarea3 = $tmp_data[$this->segment]->kibou_area3;
        $data[$this->segment]->kibouarea3= $tmpkibouarea3;

        $income = $tmp_data[$this->segment]->income;
        $data[$this->segment]->income = $income;

        $tmptenkin = $tmp_data[$this->segment]->tenkin;
        $tenkinAry=array();
        if(!empty($tmptenkin)){
            $tmptenkin = explode(",",$tmptenkin);
            if(!empty($tmptenkin)){
                foreach($tmptenkin as $k=>$v){
                    $tenkinAry[] = $v;
                }
            }
        }
        $data[$this->segment]->tenkin = $tenkinAry;

        return $data;
    }

    //////////////////////////////////////////////////////////
    //バリデーション
    //////////////////////////////////////////////////////////
    private function setValidation($validation=null)
    {
        $errors ="";
//        $this->form_validation->set_rules("kiboujob[]", "希望職種","required");
//        $this->form_validation->set_rules("kibouarea[]", "希望勤務地","required");
//        $this->form_validation->set_rules("tenkin[]", "転勤","required");
        $this->form_validation->set_rules("income", "希望年収","numeric");
//        $this->form_validation->set_rules("income", "希望年収","required|numeric");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }
}
