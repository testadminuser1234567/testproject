<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Job extends MY_Controller
{

    protected $page_limit = 20;

    //タイトル
    protected $sub_title = "job";

    //segment
    protected $segment = "job";

    //database
    protected $database = "jobs";
    protected $databaseClient = "clients";

    //HRBC URL
    protected $hrbcUrl = "https://api-hrbc-jp.porterscloud.com/v1/";

    //公開条件
    /*
    private $phaze = 6;
    private $publish = 11103;
    */
    private $phaze = 11203;
    private $publish = 11104;


    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('form_validation', 'encrypt','user_agent','pagination'));
        $this->load->helper(array('url'));
    }

    //////////////////////////////////////////////////////////
    //一覧
    //////////////////////////////////////////////////////////
    function home()
    {
        $job1="";
        $jobs=array();
        $pref="";
        $pref2="";
        $employees="";
        $keyword="";
        $year_income="";
        $keyword_flg="";

        $posts = $this->input->post();

//page_start_num
        $page=0;
        if(!empty($_GET["page"])) $page = $_GET['page'];
        $page = htmlspecialchars($page, ENT_QUOTES, 'UTF-8');
        if(!is_numeric($page)){
            $page = 1;
        }
        $page_start_num = ($page - 1) * $this->page_limit;

//postデータ マスターデータと名前被らないように注意！！
        if(!empty($posts)){
            $this->sessionClear();

            if(!empty($posts["job"])){
                foreach($posts["job"] as $k=>$v){
                    $job1 .= $v .",";
                    $_SESSION['jobs'][] = $v;
                    $jobs[] = $v;
                }
            }
            $_SESSION['job1'] = $job1;

            if(empty($posts["pref"]) && !empty($posts["pref2"])){
                $pref = $posts['pref2'];
                $pref2=$pref;
                $_SESSION['pref'] = $pref;
            }else if(!empty($posts["pref"]) && empty($posts["pref2"])){
                $pref = $posts['pref'];
                $_SESSION['pref'] = $pref;
            }else if(empty($posts["pref_kaso"]) && !empty($posts["pref2_kaso"])){
                $pref = $posts['pref2_kaso'];
                $pref2=$pref;
                $_SESSION['pref'] = $pref;
            }else if(!empty($posts["pref_kaso"]) && empty($posts["pref2_kaso"])){
                $pref = $posts['pref_kaso'];
                $_SESSION['pref'] = $pref;
            }

            if(!empty($posts["employee"])) $employees = $posts['employee'];
            $_SESSION['employees'] = $employees;
            if(!empty($posts["keyword"])) $keyword = $posts['keyword'];
            $_SESSION['keyword'] = $keyword;
            if(!empty($posts["keyword_flg"])) $keyword_flg = $posts['keyword_flg'];
            $_SESSION['keyword_flg'] = $keyword_flg;
            if(!empty($posts["year_income"])) $year_income = $posts['year_income'];
            $_SESSION['year_income'] = $year_income;
//ページネーションなどパラメータあり
        }else{
            $ref = $this->agent->is_referral();
            $now_url = "http://" .$_SERVER['SERVER_NAME'];

            if($ref != $now_url && empty($_GET['page'])){
                $this->sessionClear();
            }

            $jobs = array();

            $job1 = @$_SESSION['job1'];
            $jobs = @$_SESSION['jobs'];
            $pref = @$_SESSION['pref'];
            $employees = @$_SESSION['employees'];
            $keyword = @$_SESSION['keyword'];
            $keyword_flg = @$_SESSION['keyword_flg'];
            $year_income = @$_SESSION['year_income'];
        }


//雇用形態を配列に格納（パラメータ渡し用）
        $employ_ary = array();
        if(!empty($employees)){
            foreach($employees as $k => $v){
                $employ_ary[$k] = $v;
            }
        }

//キーワードフラグ or->0 and ->1
        if($keyword_flg == "or"){
            $kflg = 0;
        }else{
            $kflg = 1;
        }

////////////////////////////////////////////////////////////
//パラメータ設定
////////////////////////////////////////////////////////////

//業種

        $bizSql = " left join clients as c on j.client_id = c.c_id ";
        $bizSql2 = ", group_concat(distinct c.c_industry_name separator '|') as i_name, c.c_title, c.c_id";

//職種
        $tmpjob="";
//$jobSearch;
        $jobcnt=0;
        if(!empty($job1)){
            $ary1 = explode(",", $job1);
            foreach($ary1 as $k=>$v){
                $tmpjob .= $v .",";
            }
        }
        $tmpjob =  rtrim($tmpjob,",");
        $jobSearch="";
        $jcParam="";
        $jccnt=0;
        if(!empty($tmpjob)){
            $aryjc = explode(",", $tmpjob);
            foreach($aryjc as $k=>$v){
                if(empty($jobSearch)) {
                    $jobSearch.=" and ( ";
                }else{
                    $jobSearch.=" or ";
                }
                $val = $this->db->escape($v);
                $jobSearch .= " FIND_IN_SET(" . $val . ",j.jobcategory_id)";
            }
        }
        if($jobSearch) $jobSearch .= " ) ";

//勤務地
        $tmppref=array();
        $prefSearch="";
        $prefParam="";
        $prefcnt=0;
        if(!empty($pref)){
            $aryp = explode(",", $pref);
            foreach($aryp as $k=>$v){
                if(empty($prefSearch)) {
                    $prefSearch.=" and ( ";
                }else{
                    $prefSearch.=" or ";
                }
                $val = $this->db->escape($v);
                $prefSearch .= " FIND_IN_SET(" . $val . ",j.prefecture_id)";
            }
        }
        if($prefSearch) $prefSearch .= " ) ";

//雇用形態
        $tmpemploy=array();
        $employSearch="";
        $employcnt=0;
        $employ_id_data = "";
        if(!empty($employ_ary)){
            foreach($employ_ary as $k=>$v){
                $employ_id_data .= $v . ",";
            }
        }
        if(!empty($employ_id_data)){
            $employ_id_data = rtrim($employ_id_data,",");
            $val = $this->db->escape($employ_id_data);
            $employSearch = " and FIND_IN_SET(j.employ_id, " . $val . ") ";
        }

//給与
        $incomeSearch="";
        if(!empty($year_income)){
            $val = $this->db->escape($year_income);
            $incomeSearch = " and j.minsalary >= " . $val . " ";
        }

//キーワード
        $key="";
        $keySearch = "";
        if(!empty($keyword)){
            $key = $keyword;
            $key = mb_convert_kana($key, 's');
            $keyArr = preg_split('/[\s]+/', $key, -1, PREG_SPLIT_NO_EMPTY);
//	$keyArr = preg_split('/,/', $key, -1, PREG_SPLIT_NO_EMPTY);

            $keySearch .= "and (";
            foreach($keyArr as $k=> $v){
                $val = "%" . $v . "%";
                $val = $this->db->escape($val);
                if($k==0){
                    $keySearch .= " concat(j.job_title,' ', j.background,' ', j.summary, ' ', j.area_detail, ' ', j.experience, ' ', j.qualification, ' ', j.worktime, ' ', j.holiday, ' ', j.benefits, ' ', j.employ_name) like ".$val." ";
                }else{
                    $keySearch .= $keyword_flg . " concat(j.job_title,' ', j.background,' ', j.summary, ' ', j.area_detail, ' ', j.experience, ' ', j.qualification, ' ', j.worktime, ' ', j.holiday, ' ', j.benefits, ' ', j.employ_name) like ".$val . " ";
                }
            }

            $keySearch .= " )";
        }

////////////////////////////////////////////////////////////////////
// db抽出
////////////////////////////////////////////////////////////////////

        $sql = "SELECT count(distinct j.job_id) as cnt FROM jobs as j" . $bizSql . " where j.phaze = " . $this->phaze . " and j.publish = " .$this->publish . " " . $jobSearch . $prefSearch . $employSearch . $incomeSearch . $keySearch;
        $query = $this->db->query($sql);
        $total = $query->row();

        //pagination
        $config['base_url'] = base_url() . $this->uri->segment(1) . "/" . $this->uri->segment(2);
        $config["total_rows"] = $total->cnt;
        $config["per_page"] = $this->page_limit;
        $config["num_links"] = 2;
        $this->pagination->initialize($config);

        $offset = $this->uri->segment(3);
        if(empty($offset)){
            $offset = 0;
        }

        $sql = "SELECT j.job_id, j.job_title, j.client_id, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.j_updated, j.prefecture_name, j.jobcategory_name,j.employ_name" . $bizSql2 . " FROM jobs as j" . $bizSql . " where j.phaze = " . $this->phaze . " and j.publish =". $this->publish . " " . $jobSearch . $prefSearch . $employSearch . $incomeSearch . $keySearch . " group by j.job_id order by j.j_updated desc limit ". $offset . ", " . $this->page_limit;
        $contents = $this->db->query($sql);

        //お気に入りの一覧取得
        $memberid = $this->db->escape(1);
        $sql_like = "SELECT id, job_id from likes where member_id = " . $memberid . " order by updated desc";

        $like = $this->db->query($sql_like);
        $likes = array();
        foreach($like->result() as $val){
            $likes[$val->job_id] = $val->id;
        }

        //////////////////////////////////////////////////////////////////////
        //HRBC Process抽出
        //////////////////////////////////////////////////////////////////////
        $result = $this->getHrbcMaster();
        $param = http_build_query(array('partition'=>$result["partition"], 'count'=>200 , 'start'=>0, 'condition'=>'Process.P_Resume:eq=52321',  'field'=>'Process.P_Id,Process.P_Job,Process.P_Owner,Process.P_Recruiter,Process.P_Resume, Process.U_4E29163A766BC0D6CA7D9B1157A614,Process.P_Phase,Process.P_PhaseMemo,Process.P_Close,Process.P_PhaseDate,Process.P_Client,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.P_UpdateDate,Process.U_186596AC430DB9AF87A142A6A47DC7,Process.U_419EC7AD9BE7E1A377DF8B6D0115A1,Process.U_EFEE750C9B6F3D74F06F8E821E1C79,Process.U_1C50E7D9F0AD650BF5159E1BE0DF6D,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.U_5D69124062C230BB6DEC5F4D812C57,Process.U_3E781B18BA915F475A1FA174CAAD69,Process.P_UpdateDate', 'order' => 'Process.P_Id:desc'));
        $url =$this->hrbcUrl .  "process?" . $param;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
/*                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
*/
                    . "Content-Type:application/xml; charset=UTF-8\r\n"

            )
        );
        $context = stream_context_create($opts);

        $xml = file_get_contents($url, false, $context);
        $xml = simplexml_load_string($xml);

        if($xml->Code!=0){
            $pErr=$xml->Code;
            print("抽出処理でエラーが発生しました。<br />コード：" . $pErr);
            exit;
        }

        //HRBCから帰ってきたデータ件数　1件の対応用に取得
        $cnt = $xml->attributes()->Count;

        //データ設定
        $json = json_encode($xml);
        $processes = json_decode($json,true);

        $msg="";
        //data設定
        $data["contents"] = $contents;
        $data["render"] = "job/home";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["jobs"]  =$jobs;
        $data["pref"]  = $pref;
        $data["pref2"]  = $pref2;
        $data["year_income"]  = $year_income;
        $data["employees"]  = $employees;
        $data["keyword"]  = $keyword;
        $data["keyword_flg"]  = $keyword_flg;
        $data["likes"] = $likes;
        $data["processes"] = $processes;
        $data["total"] = $total->cnt;
        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //詳細
    //////////////////////////////////////////////////////////
    function detail()
    {
        $posts = $this->input->post();
        $userid = $posts["userId"];
        $jobid = $posts["jobId"];

        $this->db->set('member_id', $userid);
        $this->db->set('job_id', $jobid);
        $this->db->insert('looks');

        $jobid = $this->db->escape($jobid);
        $userid = $this->db->escape($userid);

        //データ取得
        $sql = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = ". $this->phaze . " and j.publish = ". $this->publish ." and j.job_id = " . $jobid . " group by j.job_id";

        $contents = $this->db->query($sql);
        $content = $contents->result()[0];

        //お気に入りの一覧取得
        $memberid = $this->db->escape(1); //HRBCのIDで設定
        $sql_like = "SELECT id, job_id from likes where member_id = " . $memberid . " order by updated desc";

        $like = $this->db->query($sql_like);
        $likes = array();
        foreach($like->result() as $val){
            $likes[$val->job_id] = $val->id;
        }

        //////////////////////////////////////////////////////////////////////
        //HRBC Process抽出
        //////////////////////////////////////////////////////////////////////
        $result = $this->getHrbcMaster();
        $param = http_build_query(array('partition'=>$result["partition"], 'count'=>200 , 'start'=>0, 'condition'=>'Process.P_Resume:eq=52321',  'field'=>'Process.P_Id,Process.P_Job,Process.P_Owner,Process.P_Recruiter,Process.P_Resume, Process.U_4E29163A766BC0D6CA7D9B1157A614,Process.P_Phase,Process.P_PhaseMemo,Process.P_Close,Process.P_PhaseDate,Process.P_Client,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.P_UpdateDate,Process.U_186596AC430DB9AF87A142A6A47DC7,Process.U_419EC7AD9BE7E1A377DF8B6D0115A1,Process.U_EFEE750C9B6F3D74F06F8E821E1C79,Process.U_1C50E7D9F0AD650BF5159E1BE0DF6D,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.U_5D69124062C230BB6DEC5F4D812C57,Process.U_3E781B18BA915F475A1FA174CAAD69,Process.P_UpdateDate', 'order' => 'Process.P_Id:desc'));
        $url =$this->hrbcUrl .  "process?" . $param;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
/*                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
*/
                    . "Content-Type:application/xml; charset=UTF-8\r\n"

            )
        );
        $context = stream_context_create($opts);

        $xml = file_get_contents($url, false, $context);
        $xml = simplexml_load_string($xml);

        if($xml->Code!=0){
            $pErr=$xml->Code;
            print("抽出処理でエラーが発生しました。<br />コード：" . $pErr);
            exit;
        }

        //HRBCから帰ってきたデータ件数　1件の対応用に取得
        $cnt = $xml->attributes()->Count;

        //データ設定
        $json = json_encode($xml);
        $processes = json_decode($json,true);

        $msg="";
        //data設定
        $data["contents"] = $content;
        $data["render"] = "job/detail";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["likes"] = $likes;
        $data["processes"] = $processes;
        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //お気に入り追加
    //////////////////////////////////////////////////////////
    function add()
    {
        $jobid = $this->input->post("job_id");
        $userid = $this->input->post("user_id");
        $likeid = $this->input->post("like_id");
        $regist = $this->input->post("regist");

        $is_ajax = $this->input->post("ajax");
        $msg = "";

        // AJAXが成功するかどうかで条件分岐
        if($is_ajax){
            if(empty($regist)){
                //お気に入り登録
                $this->db->set('member_id', $userid);
                $this->db->set('job_id', $jobid);
                $this->db->insert('likes');

                $msg = $this->db->insert_id();
                echo $msg;
            }else{
                //お気に入りから削除
                $this->db->delete('likes', array('id' => $likeid));
                $msg = $likeid;
                echo $msg;
            }
        }else{
            $msg = "error";
            echo $msg;
        }

    }

    //////////////////////////////////////////////////////////
    //お気に入り削除
    //////////////////////////////////////////////////////////
    function likedel()
    {
        $jobid = $this->input->post("job_id");
        $userid = $this->input->post("user_id");
        $likeid = $this->input->post("like_id");

        $is_ajax = $this->input->post("ajax");
        $msg = "";

        // AJAXが成功するかどうかで条件分岐
        if($is_ajax){
                //お気に入りから削除
                $this->db->delete('likes', array('id' => $likeid));
                echo $msg;
        }else{
            $msg = "error";
            echo $msg;
        }

    }

    //////////////////////////////////////////////////////////
    //お気に入り一覧
    //////////////////////////////////////////////////////////
    function like()
    {
        $posts = $this->input->post();

        //お気に入りのjobid取得
        $likes = array();
        $sql = "SELECT job_id from likes where member_id = 1";
        $likeData = $this->db->query($sql);
        if(!empty($likeData)){
            foreach($likeData->result() as $val){
                $likes[] = $this->db->escape($val->job_id);
            }
        }

        $likes = implode(",",$likes);

        $sql = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = ". $this->phaze . " and j.publish = ". $this->publish ." and j.job_id IN (" . $likes . ") group by j.job_id";

        $query = $this->db->query($sql);
        $total = $query->num_rows();

        //pagination
        $config['base_url'] = base_url() . $this->uri->segment(1) . "/" . $this->uri->segment(2);
        $config["total_rows"] = $total;
        $config["per_page"] = $this->page_limit;
        $config["num_links"] = 2;
        $this->pagination->initialize($config);

        $offset = $this->uri->segment(3);
        if(empty($offset)){
            $offset = 0;
        }

        $sql2 = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated, li.id as like_id FROM jobs as j left join clients as c on j.client_id = c.c_id left join likes as li on j.job_id = li.job_id where j.phaze = ". $this->phaze . " and j.publish = ". $this->publish ." and j.job_id IN (" . $likes . ") group by j.job_id order by j.j_updated desc limit ". $offset . ", " . $this->page_limit;
        $contents = $this->db->query($sql2);

        $msg="";
        //data設定
        $data["contents"] = $contents;
        $data["render"] = "job/like";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["total"] = $total;
        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //　
    //////////////////////////////////////////////////////////
    function apply()
    {
        $jobid = $this->input->post("job_id");
        $userid = $this->input->post("user_id");
        $clientid = $this->input->post("client_id");

        //企業担当の取得recruiterから
        $esc_clientid = $this->db->escape($clientid);
        $sql = "SELECT * from recruiters where r_client = " . $esc_clientid;
        $query = $this->db->query($sql);
        $recruiter = $query->result();
        $recruiterId = $recruiter[0]->r_id;

        //job情報の取得
        $esc_jobid = $this->db->escape($jobid);
        $sql = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = ". $this->phaze . " and j.publish = ". $this->publish ." and j.job_id = " . $esc_jobid . " group by j.job_id";

        $jobs = $this->db->query($sql);
        $job = $jobs->result()[0];

        //User情報取得　Person取得のため
        $esc_userid = $this->db->escape($userid);
        $sql = "SELECT * From members where id = " . $esc_userid;

        $users = $this->db->query($sql);
        $user = $users->result()[0];
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $is_ajax = $this->input->post("ajax");
        $error="";

        // AJAXが成功するかどうかで条件分岐
        if($is_ajax){
            ////////////////////////////////////////////////////////////////////////////////////////////
            //Processアップデート
            ////////////////////////////////////////////////////////////////////////////////////////////
            $result = $this->getHrbcMaster();

            $data = array();

            $url =$this->hrbcUrl .  "process?partition=".$result["partition"];
            $now = date('Y/m/d H:i:s');
//        $tmpDate = date("Y/m/d H:i:s",strtotime($posts["phasedate"]. "-9 hour"));
//        $now = date("Y/m/d H:i:s",strtotime($tmpDate. "+1 second"));

            $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>-1</Process.P_Id><Process.P_Owner>6</Process.P_Owner><Process.P_Client>' . $clientid . '</Process.P_Client><Process.P_Recruiter>' . $recruiterId . '</Process.P_Recruiter><Process.P_Job>' . $jobid . '</Process.P_Job><Process.P_Candidate>' . $user->hrbc_person_id . '</Process.P_Candidate><Process.P_Resume>' . $user->hrbc_resume_id . '</Process.P_Resume><Process.P_Phase><Option.U_001116 /></Process.P_Phase><Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11><Option.U_001311 /></Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11></Item></Process>';

            $header = array(
                "Content-Type: application/xml; charset=UTF-8",
                "X-porters-hrbc-oauth-token: " . $result['token']
            );

            $options = array('http' => array(
                'method' => 'POST',
                'content' => $xml,
                'header' => implode("\r\n", $header)
            ));

            $xml = file_get_contents($url, false, stream_context_create($options));
            $pErr="";
            $processId = "";
            $hrbcErr="";
            $xml = simplexml_load_string($xml);
            if($xml->Code!=0){
                $hrbcErr=$xml->Code;
            }else{
                $pCode="";
                $json = json_encode($xml);
                $arr = json_decode($json,true);
                foreach($arr['Item'] as $k => $v){
                    if($k=="Id"){
                        $processId = $v;
                    }
                    if($k=="Code"){
                        $pErr = $v;

                    }
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////
            //デバイス判定　PC SP
            ////////////////////////////////////////////////////////////////////////////////////////////
            $ua = $_SERVER['HTTP_USER_AGENT'];// ユーザエージェントを取得
            if((strpos($ua, 'iPhone') !== false) || (strpos($ua, 'iPod') !== false) ||(strpos($ua, 'Android') !== false)){
                $agent = "SP";
            }else{
                $agent = "PC";
            }

            ////////////////////////////////////////////////////////////////////////////////////////////
            //Person
            ////////////////////////////////////////////////////////////////////////////////////////////
            $param = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Id='.$user->hrbc_person_id, 'field'=>'Person.P_Id,Person.P_Name,Person.P_Reading,Person.P_Telephone,Person.P_Mobile,Person.P_MobileMail,Person.P_Mail', 'order'=>'Person.P_Id:asc'));
            $url =$this->hrbcUrl .  "candidate?" . $param;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);

            $xml = file_get_contents($url, false, $context);
            $xml = simplexml_load_string($xml);

            if($xml->Code!=0){
                $error=1;
                $subject = "【マイページエントリー(".$agent.")失敗　Person】". $job->job_title . " JOB:" . $jobid . " userid:" . $userid;
                //Email送信
                $this->load->library('email');
                $this->email->from('mypage@hurex.co.jp', 'HUREX株式会社');
                $this->email->to('mypage@hurex.co.jp.test-google-a.com');
                $this->email->subject($subject);
                $this->email->message("HRBCのPerson取得に失敗しました。useridからユーザー情報を確認してください。");
                $this->email->send();
            }

            //データ設定
            $json = json_encode($xml);
            $person = json_decode($json,true);
            $namae = $person["Item"]["Person.P_Name"];

            ////////////////////////////////////////////////////////////////////////////////////////////
            //Resume
            ////////////////////////////////////////////////////////////////////////////////////////////
            $param = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Id='.$user->hrbc_resume_id, 'field'=>'Resume.P_DateOfBirth,Resume.P_ChangeJobsCount,Resume.U_673275A4DA445C9B23AC18BDD7C4DC,Resume.P_Gender,Resume.P_Owner', 'order'=>'Resume.P_Id:asc'));
            $url =$this->hrbcUrl .  "resume?" . $param;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);

            $xml = file_get_contents($url, false, $context);
            $xml = simplexml_load_string($xml);

            if($xml->Code!=0){
                $subject = "【マイページエントリー(".$agent.")失敗　Resume】". $job->job_title . " JOB:" . $jobid . " userid:" . $userid . " 名前：" . $namae;
                //Email送信
                $this->load->library('email');
                $this->email->from('mypage@hurex.co.jp', 'HUREX株式会社');
                $this->email->to('mypage@hurex.co.jp.test-google-a.com');
                $this->email->subject($subject);
                $this->email->message("HRBCのResume取得に失敗しました。useridからユーザー情報を確認してください。");
                $this->email->send();
            }

            //データ設定
            $json = json_encode($xml);
            $resume = json_decode($json,true);
            //設定
            $gender="";
            foreach($resume["Item"]["Resume.P_Gender"] as $k=>$v){
                $gender = $v["Option.P_Name"];
            }
            $graduate = "";
            foreach($resume["Item"]["Resume.U_673275A4DA445C9B23AC18BDD7C4DC"] as $k=>$v){
                $graduate = $v["Option.P_Name"];
            }
            $birthday = str_replace("/","",$resume["Item"]["Resume.P_DateOfBirth"]);
            $birthday = (int) ((date('Ymd')-$birthday)/10000) . "歳";
            $company = $resume["Item"]["Resume.P_ChangeJobsCount"] ."社";

            if(!$processId || $pErr){
                $error=1;
                $subject = "【マイページエントリー(".$agent.")失敗　Process】". $job->job_title . " JOB:" . $jobid . " / " . $birthday . " " . $company . " " . $graduate . " " . $gender . " " . $namae;
                //Email送信
                $this->load->library('email');
                $this->email->from('mypage@hurex.co.jp', 'HUREX株式会社');
                $this->email->to('mypage@hurex.co.jp.test-google-a.com');
                $this->email->subject($subject);
                $this->email->message("HRBCのResume取得に失敗しました。useridからユーザー情報を確認してください。");
                $this->email->send();
            }
            if($hrbcErr){
                $error=1;
                $subject = "【マイページエントリー(".$agent.")失敗　Process2】". $job->job_title . " JOB:" . $jobid . " / " . $birthday . " " . $company . " " . $graduate . " " . $gender . " " . $namae;
                //Email送信
                $this->load->library('email');
                $this->email->from('mypage@hurex.co.jp', 'HUREX株式会社');
                $this->email->to('mypage@hurex.co.jp.test-google-a.com');
                $this->email->subject($subject);
                $this->email->message("HRBCのResume取得に失敗しました。useridからユーザー情報を確認してください。");
                $this->email->send();

            }

            if(empty($error)) {
                //お気に入りから削除
                $esc_jobid = $this->db->escape($jobid);
                $esc_userid = $this->db->escape($userid);
                $sql = "Delete from likes where member_id = " . $esc_userid . " and job_id = " . $esc_jobid;
                $query = $this->db->query($sql);

                $resume_url  ="Resume: https://hrbc-jp.porterscloud.com/resume/search?menu_id=5#/dv/17:" . $resume_id . "/";
                $job_url = "Job: https://hrbc-jp.porterscloud.com/job/search?menu_id=3#/dv/3:" . $jobid . "/";
                $process_url = "Process: https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $resume_id . "#/dv/7:" . $processId . "/";
                $unit_url  =$resume_url . "\n" . $job_url . "\n" . $process_url;

                $error=0;
                //Email送信 【マイページエントリー(SPorPC)】企業名 JOB:00000 / 00歳 0社 ○○卒 男性or女性 氏名
                $subject = "【マイページエントリー(".$agent.")】". $job->job_title . " JOB:" . $jobid . " / " . $birthday . " " . $company . " " . $graduate . " " . $gender . " " . $namae;
                $content="マイページよりエントリーがありました。\n" . $unit_url;
                $this->load->library('email');
                $this->email->from('mypage@hurex.co.jp', 'HUREX株式会社');
//                    $this->email->to('mypage@hurex.co.jp.test-google-a.com');
                $this->email->to('mypage@hurex.co.jp.test-google-a.com');
//                $this->email->to('tosa@smt-net.co.jp');
                $this->email->subject($subject);
                $this->email->message($content);
                $this->email->send();

            }else{
                $error=1;
                $subject = "【マイページエントリー(".$agent.")失敗　Process2】". $job->job_title . " JOB:" . $jobid . " / " . $birthday . " " . $company . " " . $graduate . " " . $gender . " " . $namae;
                //Email送信
                $this->load->library('email');
                $this->email->from('mypage@hurex.co.jp', 'HUREX株式会社');
                $this->email->to('mypage@hurex.co.jp.test-google-a.com');
                $this->email->subject($subject);
                $this->email->message("HRBCのResume取得に失敗しました。useridからユーザー情報を確認してください。");
                $this->email->send();
            }
        }else{
            //ajax失敗
            $error=1;
            $subject = "【マイページエントリー失敗　ajax】" . "user:" . $userid . " job:" . $jobid;
            //Email送信
            $this->load->library('email');
            $this->email->from('mypage@hurex.co.jp', 'HUREX株式会社');
            $this->email->to('mypage@hurex.co.jp.test-google-a.com');
            $this->email->subject($subject);
            $this->email->message("クライアントのブラウザに問題が発生しました。");
            $this->email->send();
        }
        echo $error;
    }

    //////////////////////////////////////////////////////////
    //HRBC基本情報
    //////////////////////////////////////////////////////////
    private function getHrbcMaster(){
        $dir = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/";
        include($dir . "system_entry/mimes.php");

        $error="";
        //ファイルが無ければ再作成
        if(!file_exists($dir . 'token/cache.json')) {
            require($dir . 'conf/auth.php');
        }
        //ファイル時間20分ごとのcronでミスがあったとき
        $ftime = filemtime($dir . 'token/cache.json');
        $twminbf = strtotime( "-20 min" );
        if($ftime < $twminbf){
            require($dir . 'conf/auth.php');
        }

        if(empty($token) || empty($partition)){
            require($dir . 'conf/auth.php');
        }

        $datas=array();
        //ファイル読込
        $json = file_get_contents($dir . 'token/cache.json', true);
        $arr = json_decode($json,true);
        $token = $arr['token'];
        $partition = $arr['partition'];
        $datas["token"] = $token;
        $datas["partition"] = $partition;

        //HRBCマスターと連動
        $tmppref = file_get_contents($dir. 'pref.json', true);
        $tmppref = json_decode($tmppref,true);
        $datas["pref"] = $tmppref;

        $tmpgender = file_get_contents($dir . 'gender.json', true);
        $tmpgender = json_decode($tmpgender,true);
        $datas["gender"] = $tmpgender;

        $tmpbackground = file_get_contents($dir . 'background.json', true);
        $tmpbackground = json_decode($tmpbackground,true);
        $datas["background"] = $tmpbackground;

        $tmpwork = file_get_contents($dir . 'work.json', true);
        $tmpwork = json_decode($tmpwork,true);
        $datas["work"] = $tmpwork;

        return $datas;
    }

    ////////////////////////////////////////////////////////////////////////
    // 検索用のセッション
    /////////////////////////////////////////////////////////////////////////
    function sessionClear(){
        unset($_SESSION['job1']);
        unset($_SESSION['jobs']);
        unset($_SESSION['pref']);
        unset($_SESSION['employees']);
        unset($_SESSION['keyword']);
        unset($_SESSION['keyword_flg']);
        unset($_SESSION['year_income']);
    }
}