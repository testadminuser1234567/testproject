<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MY_Controller
{

    protected $page_limit = 20;

    //タイトル
    protected $sub_title = "search";

    //segment
    protected $segment = "search";

    //database
    protected $database = "jobs";
    protected $databaseMember = "members";
    protected $databaseClient = "clients";

    //HRBC URL
    protected $hrbcUrl = "https://api-hrbc-jp.porterscloud.com/v1/";

    //転勤なしのID
    private $tenkin_flg = 11247;


    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('form_validation', 'encrypt','user_agent','pagination'));
        $this->load->helper(array('url'));
    }

    //////////////////////////////////////////////////////////
    //一覧
    //////////////////////////////////////////////////////////
    function index()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $gets = $this->input->get();

        //対象のID取得
        $ret = $this->db->get_where($this->databaseMember, array('id' => $login_id, 'del'=>0))->row();
        $person_id = $ret->hrbc_person_id;
        $resume_id = $ret->hrbc_resume_id;

        //////////////////////////////////////////////////////////////////////
        //HRBC Process抽出
        //////////////////////////////////////////////////////////////////////
        $result = $this->getHrbcMaster();
        $param = http_build_query(array('partition'=>$result["partition"], 'count'=>200 , 'start'=>0, 'condition'=>'Process.P_Resume:eq=' . $resume_id,  'field'=>'Process.P_Id,Process.P_Job,Process.P_Owner,Process.P_Recruiter,Process.P_Resume, Process.U_4E29163A766BC0D6CA7D9B1157A614,Process.P_Phase,Process.P_PhaseMemo,Process.P_Close,Process.P_PhaseDate,Process.P_Client,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.P_UpdateDate,Process.U_186596AC430DB9AF87A142A6A47DC7,Process.U_419EC7AD9BE7E1A377DF8B6D0115A1,Process.U_EFEE750C9B6F3D74F06F8E821E1C79,Process.U_1C50E7D9F0AD650BF5159E1BE0DF6D,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.U_5D69124062C230BB6DEC5F4D812C57,Process.U_3E781B18BA915F475A1FA174CAAD69,Process.P_UpdateDate', 'order' => 'Process.P_Id:desc'));
        $url =$this->hrbcUrl .  "process?" . $param;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 search index "));
                        redirect($this->segment."/index");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 search index"));
                        redirect($this->segment."/index");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other search index"));
                        redirect($this->segment."/index");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout search index"));
                redirect($this->segment."/index");
            }
        }
        $xml = simplexml_load_string($xml);

        if($xml->Code!=0){
            $pErr=$xml->Code;
            $subject = "【一覧取得に失敗しました　code:searchX1】 userid:" . $login_id;
            $message = "一覧取得に失敗しました。" . $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404");
            //exit;
        }

        //HRBCから帰ってきたデータ件数　1件の対応用に取得
        $cnt = $xml->attributes()->Count;

        //データ設定
        $json = json_encode($xml);
        if($cnt==0){
            $processes = array();
        }else if($cnt==1){
            $processes = json_decode($json,true);
            $processes["Item"][0] = $processes["Item"];
        }else{
            $processes = json_decode($json,true);
        }

        //フェーズオープンで自社サイトへの公開が非公開のデータ抽出（ＪＯＢ打診のものだけＪＯＢに表示）
        $p_job_id = "";
        if(!empty($processes["Item"])){
            foreach ($processes["Item"] as $pk => $pv){
                foreach ($pv["Process.P_Phase"] as $pk2 => $pv2) {
                    $opname = $pv2["Option.P_Name"];
                    $opId = $pv2["Option.P_Id"];
                    //if ($opname == "JOB打診" || $opname == "JOB打診（弱）" || $opname == "JOB打診【強！】" || $opname == "JOB打診（社名非公開）") {
                        $p_job_id .= $pv["Process.P_Job"]["Job"]["Job.P_Id"] . ",";
                    //}
                }
//                $p_job_id .= $pv["Process.P_Job"]["Job"]["Job.P_Id"] . ",";
            }
        }
        $p_job_id = rtrim($p_job_id,",");

        $get_param = "?get=1";

        $job="";
        $pref="";
        $tenkin="";
        $keyword="";
        $year_income="";
        $keyword_flg="";

//getデータ マスターデータと名前被らないように注意！！
        if(!empty($gets)){
            if(!empty($gets["job"])){
                $job = $gets['job'];
                foreach($job as $k => $v){
                    $get_param .= "&" . urlencode("job[]") . "=" . urlencode($v);
                }
            }

            if(!empty($gets["pref"])){
                $pref = $gets['pref'];
                foreach($pref as $k => $v){
                    $get_param .= "&" . urlencode("pref[]") . "=" . urlencode($v);
                }
            }

            if(!empty($gets["tenkin"])){
                $tenkin = $gets['tenkin'];
                $get_param .= "&" . "tenkin=" . urlencode($tenkin);
            }

            if(!empty($gets["keyword"])){
                $keyword = $gets['keyword'];
                $get_param .= "&" . "keyword=" . urlencode($keyword);
            }

            if(!empty($gets["keyword_flg"])){
                $keyword_flg = $gets['keyword_flg'];
                $get_param .= "&" . "keyword_flg=" . urlencode($keyword_flg);
            }else{
                $keyword_flg  = "and";
            }

            if(!empty($gets["year_income"])){
                $year_income = $gets['year_income'];
                $get_param .= "&" . "year_income=" . urlencode($year_income);
            }
        }

//キーワードフラグ or->0 and ->1
        if($keyword_flg == "or"){
            $kflg = 0;
        }else{
            $kflg = 1;
        }


////////////////////////////////////////////////////////////
//パラメータ設定
////////////////////////////////////////////////////////////

//業種

        $bizSql = " left join clients as c on j.client_id = c.c_id ";
        $bizSql2 = ", group_concat(distinct c.c_industry_name separator '|') as i_name, c.c_title, c.c_id";

//職種
        $tmpjob="";
//$jobSearch;
        $jobcnt=0;
        $jobSearch="";
        $jcParam="";
        $jccnt=0;
        //職種の大項目から小項目を設定
        $jobs=array();
        $tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
        $tmpjob = json_decode($tmpjob,true);
        if(!empty($job)){
            if(!empty($tmpjob['Item'])){
                foreach($tmpjob['Item'] as $k=>$v){
                    if(!empty($v['Item'])){
                        foreach($v['Item'] as $k2=>$v2){
                            foreach($job as $k3 => $v3){
                                if($v2["Option.P_Id"]==$v3){
                                    if(!empty($v2["Items"]["Item"])){
                                        foreach($v2["Items"]["Item"] as $k4 => $v4){
                                            $jobs[] = $v4["Option.P_Id"];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(!empty($jobs)){
                foreach($jobs as $k=>$v){
                    if(empty($jobSearch)) {
                        $jobSearch.=" and ( ";
                    }else{
                        $jobSearch.=" or ";
                    }
                    $val = $this->db->escape($v);
                    $jobSearch .= " FIND_IN_SET(" . $val . ",j.jobcategory_id)";
                }
            }
        }
        if($jobSearch) $jobSearch .= " ) ";

//勤務地
        $tmppref=array();
        $prefSearch="";
        $prefParam="";
        $prefcnt=0;
        if(!empty($pref)){
            foreach($pref as $k=>$v){
                if(empty($prefSearch)) {
                    $prefSearch.=" and ( ";
                }else{
                    $prefSearch.=" or ";
                }
                $val = $this->db->escape($v);
                $prefSearch .= " FIND_IN_SET(" . $val . ",j.prefecture_id)";
            }
        }
        if($prefSearch) $prefSearch .= " ) ";

//転勤
        /*
        $tmptenkin=array();
        $tenkinSearch="";
        $tenkincnt=0;
        $tenkin_id_data = "";
        if(!empty($tenkin_ary)){
            foreach($tenkin_ary as $k=>$v){
                $tenkin_id_data .= $v . ",";
            }
        }
        if(!empty($tenkin_id_data)){
            $tenkin_id_data = rtrim($tenkin_id_data,",");
            $val = $this->db->escape($tenkin_id_data);
            $tenkin = " and FIND_IN_SET(j.tenkin_id, " . $val . ") ";
        }
        */
        $tenkinSearch="";
        if(!empty($tenkin)){
            if($tenkin=="不可"){
                $kibou_tenkin = $this->tenkin_flg;
                $val = $this->db->escape($kibou_tenkin);
                $tenkinSearch = " and j.tenkin_id = " . $val . " ";
            }
        }

//給与
        $tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
        $tmpincome = json_decode($tmpincome,true);
        if(!empty($year_income)){
            if (!empty($tmpincome['Item'])) {
                foreach ($tmpincome['Item'] as $k => $v) {
                    if (!empty($v['Item'])) {
                        foreach ($v['Item'] as $k2 => $v2) {
                            if($year_income == $v2["Option.P_Id"]){
                                $income = $v2["Option.P_Name"];
                            }
                        }
                    }
                }
            }
        }

        $incomeSearch="";
        if(!empty($income)){
            $min="";
            $max="";
            $income = str_replace("万円", "", $income);
            $income = str_replace("以上", "", $income);
            $income = str_replace("以下", "", $income);
            $split_income = explode("～",$income);
            if($split_income[0]==200 && empty($split_income[1])){
                $min = 0;
                $max = $split_income[0];
            }else if($split_income[0]==1001 && empty($split_income[1])){
                $min = $split_income[0];
                $max = 99999999999999999;
            }else{
                $min = $split_income[0];
                $max = $split_income[1];
            }
            $min = $this->db->escape($min);
            $max = $this->db->escape($max);
//            $incomeSearch = " and j.minsalary <= " . $max . " ";
            $incomeSearch = " and j.maxsalary >= " . $min . " and j.minsalary <= " . $max . " ";
        }

//キーワード
        $key="";
        $keySearch = "";
        if(!empty($keyword)){
            $key = $keyword;
            $key = mb_convert_kana($key, 's');
            $keyArr = preg_split('/[\s]+/', $key, -1, PREG_SPLIT_NO_EMPTY);
//	$keyArr = preg_split('/,/', $key, -1, PREG_SPLIT_NO_EMPTY);

            $keySearch .= " and (";
            foreach($keyArr as $k=> $v){
                $val = "%" . $v . "%";
                $val = $this->db->escape($val);
                if($k==0){
                    $keySearch .= " concat(j.job_title,' ', j.background,' ', j.summary, ' ', j.area_detail, ' ', j.experience, ' ', j.qualification, ' ', j.worktime, ' ', j.holiday, ' ', j.benefits, ' ', j.tenkin_name) like ".$val." ";
                }else{
                    $keySearch .= $keyword_flg . " concat(j.job_title,' ', j.background,' ', j.summary, ' ', j.area_detail, ' ', j.experience, ' ', j.qualification, ' ', j.worktime, ' ', j.holiday, ' ', j.benefits, ' ', j.tenkin_name) like ".$val . " ";
                }
            }

            $keySearch .= " )";
        }

////////////////////////////////////////////////////////////////////
// db抽出
////////////////////////////////////////////////////////////////////

        $process_param="";
        if($p_job_id){
            $process_param = " and (j.publish = " .HrbcPublish. " or j.job_id in (" . $p_job_id . ")) ";
        }else{
            $process_param = " and j.publish = " .HrbcPublish;
        }
        $sql = "SELECT count(distinct j.job_id) as cnt FROM jobs as j" . $bizSql . " where j.phaze = " . HrbcPhaze . $process_param . $jobSearch . $prefSearch . $tenkinSearch . $incomeSearch . $keySearch;
        $query = $this->db->query($sql);
        $total = $query->row();

        //pagination
        $config['base_url'] = base_url() . $this->uri->segment(1) . "/" . $this->uri->segment(2) . $get_param;
        $config["total_rows"] = $total->cnt;
        $config["per_page"] = $this->page_limit;
        $config["num_links"] = 2;
        $config['full_tag_open'] = '<ul class="clearfix">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><span class="current">';
        $config['cur_tag_close'] = '</span></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['query_string_segment'] ="p";
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $paging = $this->pagination->create_links();

        if(!empty($_GET["p"])){
            $offset = $_GET["p"];
        }
        if(empty($offset)){
            $offset = 0;
        }else if(!is_numeric($offset)){
            $offset = 0;
        }

        $sql = "SELECT j.job_id, j.job_title, j.client_id, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.j_updated, j.prefecture_name, j.jobcategory_name, j.employ_name ,j.tenkin_name" . $bizSql2 . " FROM jobs as j" . $bizSql . " where j.phaze = " . HrbcPhaze . $process_param . $jobSearch . $prefSearch . $tenkinSearch . $incomeSearch . $keySearch . " group by j.job_id order by j.j_updated desc limit ". $offset . ", " . $this->page_limit;
        $contents = $this->db->query($sql);

        //件数範囲用
        $offset = $offset + 1;
        $hani = $offset + $this->page_limit - 1;
        if($hani >= $total->cnt){
            $hani = $total->cnt;
        }


        //お気に入りの一覧取得
        $memberid = $this->db->escape($login_id);
        $sql_like = "SELECT id, job_id from likes where member_id = " . $memberid . " order by updated desc";

        $like = $this->db->query($sql_like);
        $likes = array();
        foreach($like->result() as $val){
            $likes[$val->job_id] = $val->id;
        }

        //data設定
        $data["paging"] = $paging;
        $data["contents"] = $contents;
        $data["render"] = $this->segment . "/home";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["job"]  =$job;
        $data["pref"]  = $pref;
        $data["year_income"]  = $year_income;
        $data["tenkin"]  = $tenkin;
        $data["keyword"]  = $keyword;
        $data["keyword_flg"]  = $keyword_flg;
        $data["likes"] = $likes;
        $data["processes"] = $processes;
        $data["total"] = $total->cnt;
        $data["offset"] = $offset;
        $data["hani"] = $hani;
        $data["segment"] = $this->segment;
        $data["body_id"] = "list";

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //希望条件一覧
    //////////////////////////////////////////////////////////
    function home()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $posts = $this->input->post();

        //対象のID取得
        $ret = $this->db->get_where($this->databaseMember, array('id' => $login_id, 'del'=>0))->row();
        $person_id = $ret->hrbc_person_id;
        $resume_id = $ret->hrbc_resume_id;


        //////////////////////////////////////////////////////////////////////
        //HRBC Process抽出
        //////////////////////////////////////////////////////////////////////
        $result = $this->getHrbcMaster();
        $param = http_build_query(array('partition'=>$result["partition"], 'count'=>200 , 'start'=>0, 'condition'=>'Process.P_Resume:eq=' . $resume_id,  'field'=>'Process.P_Id,Process.P_Job,Process.P_Owner,Process.P_Recruiter,Process.P_Resume, Process.U_4E29163A766BC0D6CA7D9B1157A614,Process.P_Phase,Process.P_PhaseMemo,Process.P_Close,Process.P_PhaseDate,Process.P_Client,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.P_UpdateDate,Process.U_186596AC430DB9AF87A142A6A47DC7,Process.U_419EC7AD9BE7E1A377DF8B6D0115A1,Process.U_EFEE750C9B6F3D74F06F8E821E1C79,Process.U_1C50E7D9F0AD650BF5159E1BE0DF6D,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.U_5D69124062C230BB6DEC5F4D812C57,Process.U_3E781B18BA915F475A1FA174CAAD69,Process.P_UpdateDate', 'order' => 'Process.P_Id:desc'));
        $url =$this->hrbcUrl .  "process?" . $param;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 search process "));
                        redirect($this->segment."/index");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 search process"));
                        redirect($this->segment."/index");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other search process"));
                        redirect($this->segment."/index");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout search process"));
                redirect($this->segment."/index");
            }
        }
        $xml = simplexml_load_string($xml);

        if($xml->Code!=0){
            $pErr=$xml->Code;
            $subject = "【一覧取得に失敗しました　code:searchX2】 userid:" . $login_id;
            $message = "一覧取得に失敗しました。" . $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404");
            //exit;
        }

        //HRBCから帰ってきたデータ件数　1件の対応用に取得
        $cnt = $xml->attributes()->Count;

        //データ設定
        $json = json_encode($xml);
        if($cnt==0){
            $processes = array();
        }else if($cnt==1){
            $processes = json_decode($json,true);
            $processes["Item"][0] = $processes["Item"];
        }else{
            $processes = json_decode($json,true);
        }

        //フェーズオープンで自社サイトへの公開が非公開のデータ抽出（ＪＯＢ打診のものだけＪＯＢに表示）
        $p_job_id = "";
        if(!empty($processes["Item"])){
            foreach ($processes["Item"] as $pk => $pv){
                foreach ($pv["Process.P_Phase"] as $pk2 => $pv2) {
                    $opname = $pv2["Option.P_Name"];
                    $opId = $pv2["Option.P_Id"];
                    //if ($opname == "JOB打診" || $opname == "JOB打診（弱）" || $opname == "JOB打診【強！】" || $opname == "JOB打診（社名非公開）") {
                        $p_job_id .= $pv["Process.P_Job"]["Job"]["Job.P_Id"] . ",";
                    //}
                }
//                $p_job_id .= $pv["Process.P_Job"]["Job"]["Job.P_Id"] . ",";
            }
        }
        $p_job_id = rtrim($p_job_id,",");

        //希望条件保存の検索
        $kibou_job="";
        if(!empty($ret->kibou_job)){
            $kibou_job = explode(",",$ret->kibou_job);
        }
        $kibou_prefs=array();
        $kibou_pref1="";
        if(!empty($ret->kibou_area1)){
            $kibou_pref1 = explode(",",$ret->kibou_area1);
            $kibou_prefs[] = $ret->kibou_area1;
        }
        $kibou_pref2="";
        if(!empty($ret->kibou_area2)){
            $kibou_pref2 = explode(",",$ret->kibou_area2);
            $kibou_prefs[] = $ret->kibou_area2;
        }
        $kibou_pref3="";
        if(!empty($ret->kibou_area3)){
            $kibou_pref3 = explode(",",$ret->kibou_area3);
            $kibou_prefs[] = $ret->kibou_area3;
        }

        $kibou_year_income="";
        if(!empty($ret->income)){
            $kibou_year_income = $ret->income;
        }
        $kibou_tenkin="";
        if(!empty($ret->tenkin)) {
            $kibou_tenkin = explode(",", $ret->tenkin);
        }
        if($kibou_tenkin){
            if(in_array('不可', $kibou_tenkin)){
                $kibou_tenkin = $this->tenkin_flg;
            }else{
                $kibou_tenkin = "";
            }
        }

        $job="";
        $pref="";
        $tenkin="";
        $keyword="";
        $year_income="";
        $keyword_flg="";

        $ref = $this->agent->is_referral();
        $now_url = "http://" .$_SERVER['SERVER_NAME'];

////////////////////////////////////////////////////////////
//パラメータ設定
////////////////////////////////////////////////////////////

//業種

        $bizSql = " left join clients as c on j.client_id = c.c_id ";
        $bizSql2 = ", group_concat(distinct c.c_industry_name separator '|') as i_name, c.c_title, c.c_id";

//職種
        $tmpjob="";
//$jobSearch;
        $jobcnt=0;
        $jobSearch="";
        $jcParam="";
        $jccnt=0;
        $jobs=array();
        $tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
        $tmpjob = json_decode($tmpjob,true);
        if(!empty($kibou_job)){
            if(!empty($tmpjob['Item'])){
                foreach($tmpjob['Item'] as $k=>$v){
                    if(!empty($v['Item'])){
                        foreach($v['Item'] as $k2=>$v2){
                            foreach($kibou_job as $k3 => $v3){
                                if($v2["Option.P_Id"]==$v3){
                                    if(!empty($v2["Items"]["Item"])){
                                        foreach($v2["Items"]["Item"] as $k4 => $v4){
                                            $jobs[] = $v4["Option.P_Id"];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(!empty($jobs)){
                foreach($jobs as $k=>$v){
                    if(empty($jobSearch)) {
                        $jobSearch.=" and ( ";
                    }else{
                        $jobSearch.=" or ";
                    }
                    $val = $this->db->escape($v);
                    $jobSearch .= " FIND_IN_SET(" . $val . ",j.jobcategory_id)";
                }
            }
        }
        if($jobSearch) $jobSearch .= " ) ";


//勤務地
        $tmppref=array();
        $prefSearch="";
        $prefParam="";
        $prefcnt=0;
        if(!empty($kibou_prefs)){
            foreach($kibou_prefs as $k=>$v){
                if(empty($prefSearch)) {
                    $prefSearch.=" and ( ";
                }else{
                    $prefSearch.=" or ";
                }
                $val = $this->db->escape($v);
                $prefSearch .= " FIND_IN_SET(" . $val . ",j.prefecture_id)";
            }
        }
        if($prefSearch) $prefSearch .= " ) ";

//転勤
        /*
        $tmptenkin=array();
        $tenkinSearch="";
        $tenkincnt=0;
        $tenkin_id_data = "";
        if(!empty($tenkin_ary)){
            foreach($tenkin_ary as $k=>$v){
                $tenkin_id_data .= $v . ",";
            }
        }
        if(!empty($tenkin_id_data)){
            $tenkin_id_data = rtrim($tenkin_id_data,",");
            $val = $this->db->escape($tenkin_id_data);
            $tenkin = " and FIND_IN_SET(j.tenkin_id, " . $val . ") ";
        }
        */
        $tenkinSearch="";
        if(!empty($kibou_tenkin)){
            $val = $this->db->escape($kibou_tenkin);
            $tenkinSearch = " and j.tenkin_id = " . $val . " ";
        }

//給与
        $tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
        $tmpincome = json_decode($tmpincome,true);
        if(!empty($kibou_year_income)){
            if (!empty($tmpincome['Item'])) {
                foreach ($tmpincome['Item'] as $k => $v) {
                    if (!empty($v['Item'])) {
                        foreach ($v['Item'] as $k2 => $v2) {
                            if($kibou_year_income == $v2["Option.P_Id"]){
                                $income = $v2["Option.P_Name"];
                            }
                        }
                    }
                }
            }
        }

        $incomeSearch="";
        if(!empty($income)){
            $min="";
            $max="";
            $income = str_replace("万円", "", $income);
            $income = str_replace("以上", "", $income);
            $income = str_replace("以下", "", $income);
            $split_income = explode("～",$income);
            if($split_income[0]==200 && empty($split_income[1])){
                $min = 0;
                $max = $split_income[0];
            }else if($split_income[0]==1001 && empty($split_income[1])){
                $min = $split_income[0];
                $max = 99999999999999999;
            }else{
                $min = $split_income[0];
                $max = $split_income[1];
            }
            $min = $this->db->escape($min);
            $max = $this->db->escape($max);
            $incomeSearch = " and j.maxsalary >= " . $min . " and j.minsalary <= " . $max . " ";
//            $incomeSearch = " and j.minsalary <= " . $max . " ";
//            $incomeSearch = " and j.minsalary >= " . $min . " ";
        }

//キーワード
        $key="";
        $keySearch = "";
        if(!empty($keyword)){
            $key = $keyword;
            $key = mb_convert_kana($key, 's');
            $keyArr = preg_split('/[\s]+/', $key, -1, PREG_SPLIT_NO_EMPTY);
//	$keyArr = preg_split('/,/', $key, -1, PREG_SPLIT_NO_EMPTY);

            $keySearch .= "and (";
            foreach($keyArr as $k=> $v){
                $val = "%" . $v . "%";
                $val = $this->db->escape($val);
                if($k==0){
                    $keySearch .= " concat(j.job_title,' ', j.background,' ', j.summary, ' ', j.area_detail, ' ', j.experience, ' ', j.qualification, ' ', j.worktime, ' ', j.holiday, ' ', j.benefits, ' ', j.tenkin_name) like ".$val." ";
                }else{
                    $keySearch .= $keyword_flg . " concat(j.job_title,' ', j.background,' ', j.summary, ' ', j.area_detail, ' ', j.experience, ' ', j.qualification, ' ', j.worktime, ' ', j.holiday, ' ', j.benefits, ' ', j.tenkin_name) like ".$val . " ";
                }
            }

            $keySearch .= " )";
        }

////////////////////////////////////////////////////////////////////
// db抽出
////////////////////////////////////////////////////////////////////

        $process_param="";
        if($p_job_id){
            $process_param = " and (j.publish = " .HrbcPublish. " or j.job_id in (" . $p_job_id . ")) ";
        }else{
            $process_param = " and j.publish = " .HrbcPublish;
        }
        $sql = "SELECT count(distinct j.job_id) as cnt FROM jobs as j" . $bizSql . " where j.phaze = " . HrbcPhaze . $process_param . $jobSearch . $prefSearch . $tenkinSearch . $incomeSearch . $keySearch;
        $query = $this->db->query($sql);
        $total = $query->row();

        //pagination
        $config['base_url'] = base_url() . $this->uri->segment(1) . "/" . $this->uri->segment(2);
        $config["total_rows"] = $total->cnt;
        $config["per_page"] = $this->page_limit;
        $config["num_links"] = 2;
        $config['full_tag_open'] = '<ul class="clearfix">';
        $config['full_tag_close'] = '</ul>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><span class="current">';
        $config['cur_tag_close'] = '</span></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['query_string_segment'] ="p";
        $config['page_query_string'] = TRUE;
        $this->pagination->initialize($config);
        $paging = $this->pagination->create_links();

        if(!empty($_GET["p"])){
            $offset = $_GET["p"];
        }
        if(empty($offset)){
            $offset = 0;
        }else if(!is_numeric($offset)){
            $offset = 0;
        }

        $sql = "SELECT j.job_id, j.job_title, j.client_id, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.j_updated, j.prefecture_name, j.jobcategory_name, j.employ_name ,j.tenkin_name" . $bizSql2 . " FROM jobs as j" . $bizSql . " where j.phaze = " . HrbcPhaze . $process_param . $jobSearch . $prefSearch . $tenkinSearch . $incomeSearch . $keySearch . " group by j.job_id order by j.j_updated desc limit ". $offset . ", " . $this->page_limit;
        $contents = $this->db->query($sql);

        //件数範囲用
        $offset = $offset + 1;
        $hani = $offset + $this->page_limit - 1;
        if($hani >= $total->cnt){
            $hani = $total->cnt;
        }

        //お気に入りの一覧取得
        $memberid = $this->db->escape($login_id);
        $sql_like = "SELECT id, job_id from likes where member_id = " . $memberid . " order by updated desc";

        $like = $this->db->query($sql_like);
        $likes = array();
        foreach($like->result() as $val){
            $likes[$val->job_id] = $val->id;
        }

        //data設定
        $data["paging"] = $paging;
        $data["contents"] = $contents;
        $data["render"] = $this->segment . "/home";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["job"]  =$job;
        $data["pref"]  = $pref;
        $data["year_income"]  = $year_income;
        $data["tenkin"]  = $tenkin;
        $data["likes"] = $likes;
        $data["processes"] = $processes;
        $data["total"] = $total->cnt;
        $data["offset"] = $offset;
        $data["hani"] = $hani;
        $data["segment"] = $this->segment;
        $data["body_id"] = "list";

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //詳細
    //////////////////////////////////////////////////////////
    function detail()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $posts = $this->input->post();
        $jobid = $this->uri->segment(3);

        //対象のID取得
        $ret = $this->db->get_where($this->databaseMember, array('id' => $login_id, 'del'=>0))->row();
        $person_id = $ret->hrbc_person_id;
        $resume_id = $ret->hrbc_resume_id;

        //////////////////////////////////////////////////////////////////////
        //HRBC Process抽出
        //////////////////////////////////////////////////////////////////////
        $result = $this->getHrbcMaster();
        $param = http_build_query(array('partition'=>$result["partition"], 'count'=>200 , 'start'=>0, 'condition'=>'Process.P_Resume:eq='. $resume_id,  'field'=>'Process.P_Id,Process.P_Job,Process.P_Owner,Process.P_Recruiter,Process.P_Resume, Process.U_4E29163A766BC0D6CA7D9B1157A614,Process.P_Phase,Process.P_PhaseMemo,Process.P_Close,Process.P_PhaseDate,Process.P_Client,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.P_UpdateDate,Process.U_186596AC430DB9AF87A142A6A47DC7,Process.U_419EC7AD9BE7E1A377DF8B6D0115A1,Process.U_EFEE750C9B6F3D74F06F8E821E1C79,Process.U_1C50E7D9F0AD650BF5159E1BE0DF6D,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.U_5D69124062C230BB6DEC5F4D812C57,Process.U_3E781B18BA915F475A1FA174CAAD69,Process.P_UpdateDate', 'order' => 'Process.P_Id:desc'));
        $url =$this->hrbcUrl .  "process?" . $param;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 search detail "));
                        redirect($this->segment."/index");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 search detail"));
                        redirect($this->segment."/index");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other search detail"));
                        redirect($this->segment."/index");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout search detail"));
                redirect($this->segment."/index");
            }
        }
        $xml = simplexml_load_string($xml);

        if($xml->Code!=0){
            $pErr=$xml->Code;
            $subject = "【code:search3】" . $pErr;
            $message = $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404");
            //exit;
        }

        //HRBCから帰ってきたデータ件数　1件の対応用に取得
        $cnt = $xml->attributes()->Count;

        //データ設定
        $json = json_encode($xml);
        $tmp_processes = json_decode($json,true);

        $processes=array();
        if($cnt==0){
        }else if($cnt!=1){
            foreach($tmp_processes['Item'] as $k => $v) {
                $processes[] = $v;
            }
        }else{
            $processes[0] = $tmp_processes['Item'];
        }
        //フェーズオープンのデータ抽出
        $p_job_id = "";
        if(!empty($processes)){
            foreach ($processes as $pk => $pv){
                foreach ($pv["Process.P_Phase"] as $pk2 => $pv2) {
                    $opname = $pv2["Option.P_Name"];
                    $opId = $pv2["Option.P_Id"];
                    //if ($opname == "JOB打診" || $opname == "JOB打診（弱）" || $opname == "JOB打診【強！】" || $opname == "JOB打診（社名非公開）") {
                        if($pv["Process.P_Job"]["Job"]["Job.P_Id"] == $jobid){
                            $p_job_id .= $pv["Process.P_Job"]["Job"]["Job.P_Id"];
                        }
                    //}
                }
//                $p_job_id .= $pv["Process.P_Job"]["Job"]["Job.P_Id"] . ",";
            }
            /*
            foreach ($processes as $pk => $pv){
                if($pv["Process.P_Job"]["Job"]["Job.P_Id"] == $jobid){
                    $p_job_id .= $pv["Process.P_Job"]["Job"]["Job.P_Id"];
                }
            }
            */
        }

        $user = $this->getJobOwner($jobid);

        if(empty($jobid)){
            redirect($this->segment."/home");
        }

        $this->db->set('member_id', $login_id);
        $this->db->set('job_id', $jobid);
        $this->db->insert('looks');

        $jobid = $this->db->escape($jobid);
        $userid = $this->db->escape($login_id);

        $process_param = "";
        if($p_job_id){
            if(!empty($p_job_id)){
                //自社サイト非公開も出力
                $process_param = " and (j.job_id in (" . $p_job_id . ")) ";
            }else{
                $process_param = " and (j.publish = " .HrbcPublish. " or j.job_id in (" . $p_job_id . ")) ";
            }
        }else{
            $process_param = " and j.publish = " .HrbcPublish;
        }
        //データ取得
        $sql = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.salary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, j.publish, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, c.c_street, c.c_jigyo, c.c_capital, c.c_establish, c.c_url, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = ". HrbcPhaze . $process_param . " and j.job_id = " . $jobid . " group by j.job_id";
        $contents = $this->db->query($sql);
        if(empty($contents->result()[0])){
            $content = "";
        }else{
            $content = $contents->result()[0];
        }

        //お気に入りの一覧取得
        $memberid = $this->db->escape($login_id); //HRBCのIDで設定
        $sql_like = "SELECT id, job_id from likes where member_id = " . $memberid . " order by updated desc";

        $like = $this->db->query($sql_like);
        $likes = array();
        foreach($like->result() as $val){
            $likes[$val->job_id] = $val->id;
        }

        $msg="";
        //data設定
        $data["contents"] = $content;
        $data["render"] = "search/detail";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["likes"] = $likes;
        $data["processes"] = $processes;
        $data["body_id"] = "list";

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //お気に入り追加
    //////////////////////////////////////////////////////////
    function add()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $jobid = $this->input->post("job_id");
        $likeid = $this->input->post("like_id");
        $regist = $this->input->post("regist");

        $is_ajax = $this->input->post("ajax");
        $msg = "";

        // AJAXが成功するかどうかで条件分岐
        if($is_ajax){
            if(empty($regist)){
                //お気に入り登録
                $this->db->set('member_id', $login_id);
                $this->db->set('job_id', $jobid);
                $this->db->insert('likes');

                $msg = $this->db->insert_id();
                echo $msg;
            }else{
                //お気に入りから削除
                $this->db->delete('likes', array('id' => $likeid, 'member_id' => $login_id));
                $msg = $likeid;
                echo $msg;
            }
        }else{
            $msg = "error";
            //エラーメール送信
            $subject = "【お気に入り追加失敗　code:search01】 JOB:" . $jobid . " userid:" . $login_id . " likeid:" . $likeid;
            $message = "お気に入りの追加に失敗しました。 JOB:" . $jobid . " userid:" . $login_id . " likeid:" . $likeid;
            $this->errMail($subject, $message);
            echo $msg;
        }

    }

    //////////////////////////////////////////////////////////
    //お気に入り削除
    //////////////////////////////////////////////////////////
    function likedel()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $jobid = $this->input->post("job_id");
        $likeid = $this->input->post("like_id");

        $is_ajax = $this->input->post("ajax");
        $msg = "";

        // AJAXが成功するかどうかで条件分岐
        if($is_ajax){
            //お気に入りから削除
            $this->db->delete('likes', array('id' => $likeid, 'member_id' => $login_id));

            //likeの件数
            $member_id = $this->db->escape($login_id);
            $sql = "SELECT count(*) as cnt from likes where member_id = " . $member_id;
            $likeData = $this->db->query($sql);

            $total = $likeData->row()->cnt;
            $total = $total;
            $page = ceil($total / $this->page_limit);
            $page = ($page - 1) * $this->page_limit;
            if($page <= 0) $page = 0;
            if($page){
                $url = $this->uri->segment(1) . "/like/" . $page . "/";
            }else{
                $url = $this->uri->segment(1) . "/like/";
            }

            echo $url;
        }else{
            $msg = "error";
            //エラーメール送信
            $subject = "【お気に入り削除失敗　code:search02】 JOB:" . $jobid . " userid:" . $login_id . " likeid:" . $likeid;
            $message = "お気に入りの削除に失敗しました。 JOB:" . $jobid . " userid:" . $login_id . " likeid:" . $likeid;
            $this->errMail($subject, $message);
            echo $msg;
        }

    }

    //////////////////////////////////////////////////////////
    //お気に入り一括削除
    //////////////////////////////////////////////////////////
    function likedelall()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $posts = $this->input->post();
        $page = $posts["page"];
        $dels="";


        if(!empty($posts["likedel"])){
            $dels = $posts["likedel"];

            $cnt = count($dels);

            //likeの件数
            $member_id = $this->db->escape($login_id);
            $sql = "SELECT count(*) as cnt from likes where member_id = " . $member_id;
            $likeData = $this->db->query($sql);

            $total = $likeData->row()->cnt;
            $total = $total - $cnt;
            $page = ceil($total / $this->page_limit);
            $page = ($page - 1) * $this->page_limit;
            if($page){
                $url = $this->uri->segment(1) . "/like/" . $page . "/";
            }else{
                $url = $this->uri->segment(1) . "/like/";
            }

            $this->db->where_in('id', $dels);
            $ret = $this->db->delete('likes');

            if($ret){
                $this->session->set_flashdata(array("msg"=>"削除が完了致しました。"));
                redirect($url);
            }else{
                $msg = "error";
                //エラーメール送信
                $subject = "【お気に入り全部削除失敗　code:search03】 userid:" . $login_id;
                $message = "お気に入りの全部削除に失敗しました。 userid:" . $login_id;
                $this->errMail($subject, $message);
                $this->session->set_flashdata(array("msg"=>"削除に失敗しました。"));
                redirect($url);
            }
        }else{
            $url = $this->uri->segment(1) . "/like/" . $page . "/";
            redirect($url);
        }
    }

    //////////////////////////////////////////////////////////
    //お気に入り一覧
    //////////////////////////////////////////////////////////
    function like()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $page = $this->uri->segment(3);

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $posts = $this->input->post();

        //お気に入りのjobid取得
        $likes = array();
        $member_id = $this->db->escape($login_id);
        $sql = "SELECT job_id from likes where member_id = " . $member_id;
        $likeData = $this->db->query($sql);
        if(!empty($likeData)){
            foreach($likeData->result() as $val){
                $likes[] = $this->db->escape($val->job_id);
            }
        }
        $likes = implode(",",$likes);

        $contents=array();
        $total = 0;
        $offset=0;
        $paging="";
        if(!empty($likes)){
//            $sql = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, j.phaze, j.publish, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = ". HrbcPhaze . " and j.publish = ". HrbcPublish ." and j.job_id IN (" . $likes . ") group by j.job_id";
            $sql = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, j.phaze, j.publish, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.job_id IN (" . $likes . ") group by j.job_id";

            $query = $this->db->query($sql);
            $total = $query->num_rows();

            //pagination
            $config['base_url'] = base_url() . $this->uri->segment(1) . "/" . $this->uri->segment(2);
            $config["total_rows"] = $total;
            $config["per_page"] = $this->page_limit;
            $config["num_links"] = 2;
            $config['full_tag_open'] = '<ul class="clearfix">';
            $config['full_tag_close'] = '</ul>';
            $config['first_tag_open'] = '<li>';
            $config['first_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li><span class="current">';
            $config['cur_tag_close'] = '</span></li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['query_string_segment'] ="page";
            $config['use_page_numbers'] = TRUE;
            $config['page_query_string'] = TRUE;
            $this->pagination->initialize($config);
            $paging = $this->pagination->create_links();

            $offset = $this->uri->segment(3);
            if(empty($offset)){
                $offset = 0;
            }

            $sql2 = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, j.phaze, j.publish, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated, li.id as like_id FROM jobs as j left join clients as c on j.client_id = c.c_id left join likes as li on j.job_id = li.job_id where li.member_id = $login_id and j.job_id IN (" . $likes . ") group by j.job_id order by j.j_updated desc limit ". $offset . ", " . $this->page_limit;
            $contents = $this->db->query($sql2);
        }

        //件数範囲用
        $offset = $offset + 1;
        $hani = $offset - 1 + $this->page_limit;
        if($hani >= $total){
            $hani = $total;
        }

        //data設定
        $data["paging"] = $paging;
        $data["contents"] = $contents;
        $data["render"] = $this->segment . "/like";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["page"] = $page;
        $data["total"] = $total;
        $data["offset"] = $offset;
        $data["hani"] = $hani;
        $data["phaze"] = HrbcPhaze;
        $data["publish"] = HrbcPublish;
        $data["body_id"] = "list";

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //　応募(JOBの詳細を聞く）
    //////////////////////////////////////////////////////////
    function apply()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $jobid = $this->input->post("job_id");
        $clientid = $this->input->post("client_id");

        //企業担当の取得recruiterから
        $esc_clientid = $this->db->escape($clientid);
        $sql = "SELECT * from recruiters where r_client = " . $esc_clientid;
        $query = $this->db->query($sql);
        $recruiter = $query->result();
        $recruiterId = $recruiter[0]->r_id;

        //job情報の取得
        $esc_jobid = $this->db->escape($jobid);
        $sql = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = ". HrbcPhaze . " and j.publish = ". HrbcPublish ." and j.job_id = " . $esc_jobid . " group by j.job_id";

        $jobs = $this->db->query($sql);
        $job = $jobs->result()[0];

        //User情報取得　Person取得のため
        $esc_userid = $this->db->escape($login_id);
        $sql = "SELECT * From members where del = 0 and id = " . $esc_userid;

        $users = $this->db->query($sql);
        $user = $users->result()[0];
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $is_ajax = $this->input->post("ajax");
        $error="";

        // AJAXが成功するかどうかで条件分岐
        if($is_ajax){
            ////////////////////////////////////////////////////////////////////////////////////////////
            //Processアップデート
            ////////////////////////////////////////////////////////////////////////////////////////////
            $result = $this->getHrbcMaster();

            $data = array();

            $url =$this->hrbcUrl .  "process?partition=".$result["partition"];
            $now = date('Y/m/d H:i:s');
            $phasedate = date("Y/m/d H:i:s", strtotime($now . "-9 hour"));

            $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>-1</Process.P_Id><Process.P_Owner>6</Process.P_Owner><Process.P_Client>' . $clientid . '</Process.P_Client><Process.P_Recruiter>' . $recruiterId . '</Process.P_Recruiter><Process.P_Job>' . $jobid . '</Process.P_Job><Process.P_Candidate>' . $user->hrbc_person_id . '</Process.P_Candidate><Process.P_Resume>' . $user->hrbc_resume_id . '</Process.P_Resume><Process.P_Phase><Option.U_001342 /></Process.P_Phase><Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11><Option.U_001311 /></Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11><Process.P_PhaseDate>' . $phasedate . '</Process.P_PhaseDate></Item></Process>';

            $header = array(
                "Content-Type: application/xml; charset=UTF-8",
                "X-porters-hrbc-oauth-token: " . $result['token']
            );

            $options = array('http' => array(
                'method' => 'POST',
                'content' => $xml,
                'header' => implode("\r\n", $header)
            ));

            if($xml = @file_get_contents($url, false, stream_context_create($options))){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 search apply "));
                            redirect($this->segment."/index");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 search apply"));
                            redirect($this->segment."/index");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other search apply"));
                            redirect($this->segment."/index");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout search apply"));
                    redirect($this->segment."/index");
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////
            //デバイス判定　PC SP
            ////////////////////////////////////////////////////////////////////////////////////////////
            $agent = $this->getDevice();

            $pErr="";
            $processId = "";
            $hrbcErr="";
            $xml = simplexml_load_string($xml);
            if($xml->Code!=0){
                $hrbcErr=$xml->Code;
                //エラーメール送信
                $subject = "【Processの登録に失敗しました".$agent."　code:search_process01】". $job->job_title . " JOB:" . $jobid . " userid:" . $login_id . " Err:" . $hrbcErr;
                $message = "Processの登録に失敗しました。";
                $this->errMail($subject, $message);
            }else{
                $pCode="";
                $json = json_encode($xml);
                $arr = json_decode($json,true);
                foreach($arr['Item'] as $k => $v){
                    if($k=="Id"){
                        $processId = $v;
                    }
                    if($k=="Code"){
                        $pErr = $v;

                    }
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////
            //Person
            ////////////////////////////////////////////////////////////////////////////////////////////
            $param = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Id='.$user->hrbc_person_id, 'field'=>'Person.P_Id,Person.P_Name,Person.P_Reading,Person.P_Telephone,Person.P_Mobile,Person.P_MobileMail,Person.P_Mail', 'order'=>'Person.P_Id:asc'));
            $url =$this->hrbcUrl .  "candidate?" . $param;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);

            if($xml = @file_get_contents($url, false, $context)){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 search apply2 "));
                            redirect($this->segment."/index");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 search apply2"));
                            redirect($this->segment."/index");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other search apply2"));
                            redirect($this->segment."/index");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout search apply2"));
                    redirect($this->segment."/index");
                }
            }
            $xml = simplexml_load_string($xml);

            if($xml->Code!=0){
                $error=$xml->Code . "search03";
                //エラーメール送信
                $subject = "【応募に失敗しました".$agent."　code:search03】". $job->job_title . " JOB:" . $jobid . " userid:" . $login_id;
                $message = "Searchからの応募に失敗しました。";
                $this->errMail($subject, $message);
            }

            //データ設定
            $json = json_encode($xml);
            $person = json_decode($json,true);
            $namae = $person["Item"]["Person.P_Name"];

            ////////////////////////////////////////////////////////////////////////////////////////////
            //Resume
            ////////////////////////////////////////////////////////////////////////////////////////////
            $param = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Id='.$user->hrbc_resume_id, 'field'=>'Resume.P_DateOfBirth,Resume.P_ChangeJobsCount,Resume.U_673275A4DA445C9B23AC18BDD7C4DC,Resume.P_Gender,Resume.P_Owner', 'order'=>'Resume.P_Id:asc'));
            $url =$this->hrbcUrl .  "resume?" . $param;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);


            if($xml = @file_get_contents($url, false, $context)){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 search apply3 "));
                            redirect($this->segment."/index");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 search apply3"));
                            redirect($this->segment."/index");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other search apply3"));
                            redirect($this->segment."/index");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout search apply3"));
                    redirect($this->segment."/index");
                }
            }
            $xml = simplexml_load_string($xml);

            if($xml->Code!=0){
                //エラーメール送信
                $subject = "【Resumeの取得に失敗しました".$agent."　code:search04】". $job->job_title . " JOB:" . $jobid . " userid:" . $login_id;
                $message = "Resumeの取得に失敗しました。";
                $this->errMail($subject, $message);
            }

            //データ設定
            $json = json_encode($xml);
            $resume = json_decode($json,true);
            //設定
            $gender="";
            foreach($resume["Item"]["Resume.P_Gender"] as $k=>$v){
                $gender = $v["Option.P_Name"];
            }
            $graduate = "";
            foreach($resume["Item"]["Resume.U_673275A4DA445C9B23AC18BDD7C4DC"] as $k=>$v){
                $graduate = $v["Option.P_Name"];
            }
            if($resume["Item"]["Resume.P_DateOfBirth"]){
                $birthday = str_replace("/","",$resume["Item"]["Resume.P_DateOfBirth"]);
                $birthday = (int) ((date('Ymd')-$birthday)/10000) . "歳";
            }else{
                $birthday = "";
            }
            if(!empty($resume["Item"]["Resume.P_ChangeJobsCount"])){
                $company = $resume["Item"]["Resume.P_ChangeJobsCount"] ."社";
            }else{
                $company = "";
            }

            if(!$processId || $pErr){
                $error=$pErr . "search05";
                //エラーメール送信
                $subject = "【Resumeの取得に失敗しました".$agent."　code:search05】". $job->job_title . " JOB:" . $jobid . " userid:" . $login_id . "error : " . $pErr;
                $message = "Resumeの取得に失敗しました。";
                $this->errMail($subject, $message);
            }
            if($hrbcErr){
                $error=$hrbcErr . "search06";
                //エラーメール送信
                $subject = "【Resumeの取得に失敗しました".$agent."　code:search06】". $job->job_title . " JOB:" . $jobid . " userid:" . $login_id . "error : " . $hrbcErr;
                $message = "Resumeの取得に失敗しました。";
                $this->errMail($subject, $message);
            }

            if(empty($error)) {
                //お気に入りから削除
                $esc_jobid = $this->db->escape($jobid);
                $esc_userid = $this->db->escape($login_id);
                $sql = "Delete from likes where member_id = " . $esc_userid . " and job_id = " . $esc_jobid;
                $query = $this->db->query($sql);

                //$resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id . "?menu_id=5{/unwrap}";
                $resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id ."?menu_id=5#/dv/17:" . $resume_id ."/{/unwrap}";

                $job_url = "{unwrap}Job: https://hrbc-jp.porterscloud.com/job/search/id/".$jobid."?menu_id=3{/unwrap}";
                $process_url = "{unwrap}Process: https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $resume_id . "#/dv/7:" . $processId . "/{/unwrap}";
                $unit_url  =$resume_url . "\n" . $job_url . "\n" . $process_url;

                $error=0;
                //Email送信 【マイページエントリー(SPorPC)】企業名 JOB:00000 / 00歳 0社 ○○卒 男性or女性 氏名
                $person_datas = $this->getPerson($person_id);
                $resume_datas = $this->getResume($resume_id);

                $jobinfo = $this->getJobInfo($jobid);
                $jobowner = $this->getJobOwner($jobid);

                $processInfo = $this->getProcessInfo($resume_id, $jobinfo);
                $power = $processInfo["Process.P_Owner"];
                $process_owner_id = $power["User"]["User.P_Id"];


                $userData = $this->getUser($process_owner_id);
                //選考プロセス担当
                $userName="";
                if(!empty($userData)){
                    foreach($userData as $k => $v){
                        if(!empty($v)){
                            foreach (($v) as $k2 => $v2) {
                                if(!empty($v2["User.P_Name"])){
                                    if($v2["User.P_Id"] == $process_owner_id){
                                        $userName = $v2["User.P_Name"];
                                    }
                                }
                            }
                        }
                    }
                }

                //会社名
                $client_name = $job->c_title;

                $param_datas = array();
                $param_datas["process_owner"] = $userName;
                $param_datas["client_name"] = $client_name;
                $param_datas["job_owner"] = $jobowner;


                $this->sendMail4($agent, $jobid, $person_datas, $resume_datas, $processId, $recruiter, $job, $param_datas);

            }else{
                //エラーメール送信
                $subject = "【Resumeの取得に失敗しました".$agent."　code:search07】". $job->job_title . " JOB:" . $jobid . " userid:" . $login_id . "error : " . $error;
                $message = "Resumeの取得に失敗しました。";
                $this->errMail($subject, $message);
            }
        }else{
            //ajax失敗
            $error="ajax失敗";
            //エラーメール送信
            $subject = "【ajax通信に失敗しました。　code:search08】". $job->job_title . " JOB:" . $jobid . " userid:" . $login_id  . "error : " . $error;
            $message = "ajax通信に失敗しました。";
            $this->errMail($subject, $message);
        }
        echo $error;
    }

    //////////////////////////////////////////////////////////
    //JOB打診からの応募
    //////////////////////////////////////////////////////////
    function offer()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $posts = $this->input->post();

        $jobid = $this->input->post("job_id");
        $clientid = $this->input->post("client_id");
        $clientname = $this->input->post("client_name");

        //job情報の取得
        $esc_jobid = $this->db->escape($jobid);
//        $process_param = " and (j.publish = " .HrbcPublish. " or (j.publish = ". HrbcCloseJob . " and j.job_id in (" . $esc_jobid . "))) ";
        $process_param = " and j.job_id in (" . $esc_jobid . ") ";
        $sql = "SELECT j.job_id, j.job_title, j.recruiter_id, j.owner_id, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = ". HrbcPhaze . $process_param ." and j.job_id = " . $esc_jobid . " group by j.job_id";

        $jobs = $this->db->query($sql);
        $job = $jobs->result()[0];

        //job担当取得
        $tmpjobowner = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobowner.json', true);
        $tmpjobowner = json_decode($tmpjobowner,true);

        $jobowner_mail = "";
        $jobowner_flg = 0;
        if(!empty($tmpjobowner)){
            foreach ($tmpjobowner as $k => $v) {
                if(!empty($v)){
                    foreach($v as $k2 => $v2){
                        if(!empty($v2) && is_array($v2)){
                            if(!empty($v2["User.P_Id"]) && $v2["User.P_Id"]==$job->owner_id){
                                $jobowner_mail = $v2["User.P_Mail"];
                            }
                        }
                    }
                }
            }
        }
        if(empty($jobowner_mail)){
            $jobowner_flg = 1;
        }

        //User情報取得　Person取得のため
        $esc_userid = $this->db->escape($login_id);
        $sql = "SELECT * From members where del = 0 and  id = " . $esc_userid;

        $users = $this->db->query($sql);
        $user = $users->result()[0];
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $is_ajax = $this->input->post("ajax");
        $error=0;

        // AJAXが成功するかどうかで条件分岐
        if($is_ajax) {
            ////////////////////////////////////////////////////////////////////////////////////////////
            //デバイス判定　PC SP
            ////////////////////////////////////////////////////////////////////////////////////////////
            $agent = $this->getDevice();

            ////////////////////////////////////////////////////////////////////////////////////////////
            //Processアップデート
            ////////////////////////////////////////////////////////////////////////////////////////////
            $result = $this->getHrbcMaster();
            $data = array();

            $url = $this->hrbcUrl . "process?partition=" . $result["partition"];
//        $now = date('Y/m/d H:i:s');
            $tmpDate = date("Y/m/d H:i:s", strtotime($posts["phasedate"] . "-9 hour"));
            $now = date("Y/m/d H:i:s", strtotime($tmpDate . "+1 second"));

            $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>' . $posts["processId"] . '</Process.P_Id><Process.P_Phase><Option.U_001122 /></Process.P_Phase><Process.P_PhaseDate>' . $now . '</Process.P_PhaseDate></Item></Process>';

            $header = array(
                "Content-Type: application/xml; charset=UTF-8",
                "X-porters-hrbc-oauth-token: " . $result['token']
            );

            $options = array('http' => array(
                'method' => 'POST',
                'content' => $xml,
                'header' => implode("\r\n", $header)
            ));

            if ($xml = @file_get_contents($url, false, stream_context_create($options))) {
                //ここにデータ取得が成功した時の処理
            } else {
                //エラー処理
                if (count($http_response_header) > 0) {
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch ($status_code[1]) {
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg" => "不正な処理が行われました。Err:404 apply"));
                            redirect($this->segment . "/home");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg" => "サーバでエラーが発生しました。Err:500 apply"));
                            redirect($this->segment . "/home");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg" => "不正な処理が行われました。Err:other apply"));
                            redirect($this->segment . "/home");
                    }
                } else {
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg" => "不正な処理が行われました。Err:timeout apply"));
                    redirect($this->segment . "/home");
                }
            }
            $pErr = "";
            $pId = "";
            $hrbcErr = "";
            $xml = simplexml_load_string($xml);
            if ($xml->Code != 0) {
                $hrbcErr = $xml->Code;
            } else {
                $pCode = "";
                $json = json_encode($xml);
                $arr = json_decode($json, true);
                foreach ($arr['Item'] as $k => $v) {
                    if ($k == "Id") {
                        $pId = $v;
                    }
                    if ($k == "Code") {
                        $pErr = $v;

                    }
                }
            }

            $error = "";
            if (!$pId || $pErr) {
//            $error.= "エラーが発生しました。(Process: " . $pErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
            }
            if ($hrbcErr) {
                $error .= "エラーが発生しました。(Process: " . $hrbcErr . " time:" . $now . ")<br />画面をメモして管理者へ問い合わせて下さい。";
            }

            if (empty($error)) {
                //Email送信
                //$resume_url = "[キャンディデイトBCリンク]" . "\n" . "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id . "?menu_id=5";
                $resume_url = "[キャンディデイトBCリンク]" . "\n" . "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id ."?menu_id=5#/dv/17:" . $resume_id ."/";

                $job_url = "\n【応募JobBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/job/search/id/".$jobid."?menu_id=3";
                $process_url = "\n【選考プロセスBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $resume_id . "#/dv/7:" . $pId . "/";
                $unit_url = $resume_url . "\n" . $job_url . "\n" . $process_url;

                $person_datas = $this->getPerson($person_id);
                $resume_datas = $this->getResume($resume_id);

                //$subject = "【既登録者(" . $agent . ")：Job打診からの申込】 JOB:" . $jobid . " Client:" . $clientname . " " . $person_datas["tel1"];

                if($jobowner_flg == 1){
                    //エラーの場合
                    $mailto = hurexErrMailTo;
                }else{
                    $mailto = array($jobowner_mail, hurexMailTo);
                }

                $jobinfo = $this->getJobInfo($jobid);
                $jobowner = $this->getJobOwner($jobid);
                $processInfo = $this->getProcessInfo($resume_id, $jobinfo);
                $power = $processInfo["Process.P_Owner"];
                $process_owner_id = $power["User"]["User.P_Id"];

                $userData = $this->getUser($process_owner_id);
                //選考プロセス担当
                $userName="";
                if(!empty($userData)){
                    foreach($userData as $k => $v){
                        if(!empty($v)){
                            foreach (($v) as $k2 => $v2) {
                                if(!empty($v2["User.P_Name"])){
                                    if($v2["User.P_Id"] == $process_owner_id){
                                        $userName = $v2["User.P_Name"];
                                    }
                                }
                            }
                        }
                    }
                }

                //会社名
                $client_name = $job->c_title;

                $param_datas = array();
                $param_datas["process_owner"] = $userName;
                $param_datas["client_name"] = $client_name;
                $param_datas["job_owner"] = $jobowner;

                $this->sendMail5($agent, $jobid, $person_datas, $resume_datas, $job, $unit_url, $mailto, $param_datas);
                //$message = "案件への応募がありました。\n" . $unit_url;
                //$this->successMail($subject, $message);

            }
        }
        echo $error;
    }

    //////////////////////////////////////////////////////////
    //オファーの辞退
    //////////////////////////////////////////////////////////
    function refusal()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $posts = $this->input->post();

        $jobid = $this->input->post("job_id");
        $clientid = $this->input->post("client_id");
        $clientname = $this->input->post("client_name");

        //job情報の取得
        $esc_jobid = $this->db->escape($jobid);
        $process_param = " and j.job_id in (" . $esc_jobid . ") ";
//        $process_param = " and (j.publish = " .HrbcPublish. " or (j.publish = ". HrbcCloseJob . " and j.job_id in (" . $esc_jobid . "))) ";
        $sql = "SELECT j.job_id, j.job_title, j.recruiter_id, j.owner_id, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = ". HrbcPhaze . $process_param ." and j.job_id = " . $esc_jobid . " group by j.job_id";

        $jobs = $this->db->query($sql);
        $job = $jobs->result()[0];

        //job担当取得
        $tmpjobowner = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobowner.json', true);
        $tmpjobowner = json_decode($tmpjobowner,true);

        $jobowner_mail = "";
        $jobowner_flg = 0;
        if(!empty($tmpjobowner)){
            foreach ($tmpjobowner as $k => $v) {
                if(!empty($v)){
                    foreach($v as $k2 => $v2){
                        if(!empty($v2) && is_array($v2)){
                            if(!empty($v2["User.P_Id"]) && $v2["User.P_Id"]==$job->owner_id){
                                $jobowner_mail = $v2["User.P_Mail"];
                            }
                        }
                    }
                }
            }
        }
        if(empty($jobowner_mail)){
            $jobowner_flg = 1;
        }

        //User情報取得　Person取得のため
        $esc_userid = $this->db->escape($login_id);
        $sql = "SELECT * From members where del =0 and  id = " . $esc_userid;

        $users = $this->db->query($sql);
        $user = $users->result()[0];
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $is_ajax = $this->input->post("ajax");
        $error=0;

        // AJAXが成功するかどうかで条件分岐
        if($is_ajax) {
            ////////////////////////////////////////////////////////////////////////////////////////////
            //デバイス判定　PC SP
            ////////////////////////////////////////////////////////////////////////////////////////////
            $agent = $this->getDevice();

            ////////////////////////////////////////////////////////////////////////////////////////////
            //Processアップデート
            ////////////////////////////////////////////////////////////////////////////////////////////
            $result = $this->getHrbcMaster();
            $data = array();

            $url = $this->hrbcUrl . "process?partition=" . $result["partition"];
//        $now = date('Y/m/d H:i:s');
            $tmpDate = date("Y/m/d H:i:s", strtotime($posts["phasedate"] . "-9 hour"));
            $now = date("Y/m/d H:i:s", strtotime($tmpDate . "+1 second"));

            //社名非公開とそれ以外で判別
            if($posts["phaseStatus"]==2){
                $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>' . $posts["processId"] . '</Process.P_Id><Process.P_Phase><Option.U_001168 /></Process.P_Phase><Process.P_PhaseDate>' . $now . '</Process.P_PhaseDate></Item></Process>';
            }else{
                $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>' . $posts["processId"] . '</Process.P_Id><Process.P_Phase><Option.U_001344 /></Process.P_Phase><Process.P_PhaseDate>' . $now . '</Process.P_PhaseDate></Item></Process>';
            }

            $header = array(
                "Content-Type: application/xml; charset=UTF-8",
                "X-porters-hrbc-oauth-token: " . $result['token']
            );

            $options = array('http' => array(
                'method' => 'POST',
                'content' => $xml,
                'header' => implode("\r\n", $header)
            ));

            if ($xml = @file_get_contents($url, false, stream_context_create($options))) {
                //ここにデータ取得が成功した時の処理
            } else {
                //エラー処理
                if (count($http_response_header) > 0) {
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch ($status_code[1]) {
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg" => "不正な処理が行われました。Err:404 cancel"));
                            redirect($this->segment . "/home");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg" => "サーバでエラーが発生しました。Err:500 cancel"));
                            redirect($this->segment . "/home");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg" => "不正な処理が行われました。Err:other cancel"));
                            redirect($this->segment . "/home");
                    }
                } else {
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg" => "不正な処理が行われました。Err:timeout cancel"));
                    redirect($this->segment . "/home");
                }
            }
            $pErr = "";
            $pId = "";
            $hrbcErr = "";
            $xml = simplexml_load_string($xml);
            if ($xml->Code != 0) {
                $hrbcErr = $xml->Code;
            } else {
                $pCode = "";
                $json = json_encode($xml);
                $arr = json_decode($json, true);
                foreach ($arr['Item'] as $k => $v) {
                    if ($k == "Id") {
                        $pId = $v;
                    }
                    if ($k == "Code") {
                        $pErr = $v;

                    }
                }
            }

            if (!$pId || $pErr) {
//            $error.= "エラーが発生しました。(Process: " . $pErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
            }
            if ($hrbcErr) {
                $error .= "エラーが発生しました。(Process: " . $hrbcErr . " time:" . $now . ")<br />画面をメモして管理者へ問い合わせて下さい。";
            }

            if (empty($error)) {
                //Email送信
                //$resume_url = "[キャンディデイトBCリンク]" . "\n" . "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id ."?menu_id=5";
                $resume_url = "[キャンディデイトBCリンク]" . "\n" . "https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id ."?menu_id=5#/dv/17:" . $resume_id ."/";

                $job_url = "\n【応募JobBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/job/search/id/".$jobid."?menu_id=3";
                $process_url = "\n【選考プロセスBCリンク】" . "\n" . "https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $resume_id . "#/dv/7:" . $pId . "/";
                $unit_url = $resume_url . "\n" . $job_url . "\n" . $process_url;

                $person_datas = $this->getPerson($person_id);
                $resume_datas = $this->getResume($resume_id);

                $subject = "【既登録者(" . $agent . ")：Job打診の辞退】 JOB:" . $jobid . " Client:" . $clientname . " " . $person_datas["tel1"];

                if($jobowner_flg == 1){
                    //エラーの場合
                    $mailto = hurexErrMailTo;
                }else{
                    $mailto = array($jobowner_mail, hurexMailTo);
                }

                $jobinfo = $this->getJobInfo($jobid);
                $jobowner = $this->getJobOwner($jobid);
                $processInfo = $this->getProcessInfo($posts["processId"]);
                $power = $processInfo["Process.P_Owner"];
                $process_owner_id = $power["User"]["User.P_Id"];

                $userData = $this->getUser($process_owner_id);
                //選考プロセス担当
                $userName="";
                if(!empty($userData)){
                    foreach($userData as $k => $v){
                        if(!empty($v)){
                            foreach (($v) as $k2 => $v2) {
                                if(!empty($v2["User.P_Name"])){
                                    if($v2["User.P_Id"] == $process_owner_id){
                                        $userName = $v2["User.P_Name"];
                                    }
                                }
                            }
                        }
                    }
                }

                //会社名
                $client_name = $job->c_title;

                $param_datas = array();
                $param_datas["process_owner"] = $userName;
                $param_datas["client_name"] = $client_name;
                $param_datas["job_owner"] = $jobowner;

                $this->sendMail7($agent, $jobid, $person_datas, $resume_datas, $job, $unit_url, $mailto, $param_datas);
                //$message = "案件への応募がありました。\n" . $unit_url;
                //$this->successMail($subject, $message);

            }
        }
        echo $error;
    }

    //////////////////////////////////////////////////////////
    //　お気に入りからの応募
    //////////////////////////////////////////////////////////
    function likeapply()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $jobid = $this->input->post("job_id");
        $clientid = $this->input->post("client_id");

        //企業担当の取得recruiterから
        $esc_clientid = $this->db->escape($clientid);
        $sql = "SELECT * from recruiters where r_client = " . $esc_clientid;
        $query = $this->db->query($sql);
        $recruiter = $query->result();
        $recruiterId = $recruiter[0]->r_id;

        //job情報の取得
        $esc_jobid = $this->db->escape($jobid);
        $sql = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = ". HrbcPhaze . " and j.publish = ". HrbcPublish ." and j.job_id = " . $esc_jobid . " group by j.job_id";

        $jobs = $this->db->query($sql);
        $job = $jobs->result()[0];

        //User情報取得　Person取得のため
        $esc_userid = $this->db->escape($login_id);
        $sql = "SELECT * From members where del = 0 and  id = " . $esc_userid;

        $users = $this->db->query($sql);
        $user = $users->result()[0];
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $is_ajax = $this->input->post("ajax");
        $error="";

        // AJAXが成功するかどうかで条件分岐
        if($is_ajax){
            ////////////////////////////////////////////////////////////////////////////////////////////
            //Processアップデート
            ////////////////////////////////////////////////////////////////////////////////////////////
            $result = $this->getHrbcMaster();

            $data = array();

            $url =$this->hrbcUrl .  "process?partition=".$result["partition"];
            $now = date('Y/m/d H:i:s');
            $phasedate = date("Y/m/d H:i:s", strtotime($now . "-9 hour"));

            $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Process><Item><Process.P_Id>-1</Process.P_Id><Process.P_Owner>6</Process.P_Owner><Process.P_Client>' . $clientid . '</Process.P_Client><Process.P_Recruiter>' . $recruiterId . '</Process.P_Recruiter><Process.P_Job>' . $jobid . '</Process.P_Job><Process.P_Candidate>' . $user->hrbc_person_id . '</Process.P_Candidate><Process.P_Resume>' . $user->hrbc_resume_id . '</Process.P_Resume><Process.P_Phase><Option.U_001116 /></Process.P_Phase><Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11><Option.U_001311 /></Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11><Process.P_PhaseDate>' . $phasedate . '</Process.P_PhaseDate></Item></Process>';

            $header = array(
                "Content-Type: application/xml; charset=UTF-8",
                "X-porters-hrbc-oauth-token: " . $result['token']
            );

            $options = array('http' => array(
                'method' => 'POST',
                'content' => $xml,
                'header' => implode("\r\n", $header)
            ));

            if($xml = @file_get_contents($url, false, stream_context_create($options))){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 search apply "));
                            redirect($this->segment."/index");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 search apply"));
                            redirect($this->segment."/index");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other search apply"));
                            redirect($this->segment."/index");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout search apply"));
                    redirect($this->segment."/index");
                }
            }
            $pErr="";
            $processId = "";
            $hrbcErr="";
            $xml = simplexml_load_string($xml);
            if($xml->Code!=0){
                $hrbcErr=$xml->Code;
            }else{
                $pCode="";
                $json = json_encode($xml);
                $arr = json_decode($json,true);
                foreach($arr['Item'] as $k => $v){
                    if($k=="Id"){
                        $processId = $v;
                    }
                    if($k=="Code"){
                        $pErr = $v;

                    }
                }
            }

            ////////////////////////////////////////////////////////////////////////////////////////////
            //デバイス判定　PC SP
            ////////////////////////////////////////////////////////////////////////////////////////////
            $agent = $this->getDevice();

            ////////////////////////////////////////////////////////////////////////////////////////////
            //Person
            ////////////////////////////////////////////////////////////////////////////////////////////
            $param = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Id='.$user->hrbc_person_id, 'field'=>'Person.P_Id,Person.P_Name,Person.P_Reading,Person.P_Telephone,Person.P_Mobile,Person.P_MobileMail,Person.P_Mail', 'order'=>'Person.P_Id:asc'));
            $url =$this->hrbcUrl .  "candidate?" . $param;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);

            if($xml = @file_get_contents($url, false, $context)){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 search apply2 "));
                            redirect($this->segment."/index");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 search apply2"));
                            redirect($this->segment."/index");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other search apply2"));
                            redirect($this->segment."/index");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout search apply2"));
                    redirect($this->segment."/index");
                }
            }
            $xml = simplexml_load_string($xml);

            if($xml->Code!=0){
                $error=$xml->Code . "search03";
                //エラーメール送信
                $subject = "【応募に失敗しました".$agent."　code:search03】". $job->job_title . " JOB:" . $jobid . " userid:" . $login_id;
                $message = "Searchからの応募に失敗しました。";
                $this->errMail($subject, $message);
            }

            //データ設定
            $json = json_encode($xml);
            $person = json_decode($json,true);
            $namae = $person["Item"]["Person.P_Name"];

            ////////////////////////////////////////////////////////////////////////////////////////////
            //Resume
            ////////////////////////////////////////////////////////////////////////////////////////////
            $param = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Id='.$user->hrbc_resume_id, 'field'=>'Resume.P_DateOfBirth,Resume.P_ChangeJobsCount,Resume.U_673275A4DA445C9B23AC18BDD7C4DC,Resume.P_Gender,Resume.P_Owner', 'order'=>'Resume.P_Id:asc'));
            $url =$this->hrbcUrl .  "resume?" . $param;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);


            if($xml = @file_get_contents($url, false, $context)){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 search apply3 "));
                            redirect($this->segment."/index");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 search apply3"));
                            redirect($this->segment."/index");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other search apply3"));
                            redirect($this->segment."/index");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout search apply3"));
                    redirect($this->segment."/index");
                }
            }
            $xml = simplexml_load_string($xml);

            if($xml->Code!=0){
                //エラーメール送信
                $subject = "【Resumeの取得に失敗しました".$agent."　code:search04】". $job->job_title . " JOB:" . $jobid . " userid:" . $login_id;
                $message = "Resumeの取得に失敗しました。";
                $this->errMail($subject, $message);
            }

            //データ設定
            $json = json_encode($xml);
            $resume = json_decode($json,true);
            //設定
            $gender="";
            foreach($resume["Item"]["Resume.P_Gender"] as $k=>$v){
                $gender = $v["Option.P_Name"];
            }
            $graduate = "";
            foreach($resume["Item"]["Resume.U_673275A4DA445C9B23AC18BDD7C4DC"] as $k=>$v){
                $graduate = $v["Option.P_Name"];
            }
            if($resume["Item"]["Resume.P_DateOfBirth"]){
                $birthday = str_replace("/","",$resume["Item"]["Resume.P_DateOfBirth"]);
                $birthday = (int) ((date('Ymd')-$birthday)/10000) . "歳";
            }else{
                $birthday = "";
            }
            if(!empty($resume["Item"]["Resume.P_ChangeJobsCount"])){
                $company = $resume["Item"]["Resume.P_ChangeJobsCount"] ."社";
            }else{
                $company = "";
            }

            if(!$processId || $pErr){
                $error=$pErr . "search05";
                //エラーメール送信
                $subject = "【Resumeの取得に失敗しました".$agent."　code:search05】". $job->job_title . " JOB:" . $jobid . " userid:" . $login_id . "error : " . $pErr;
                $message = "Resumeの取得に失敗しました。";
                $this->errMail($subject, $message);
            }
            if($hrbcErr){
                $error=$hrbcErr . "search06";
                //エラーメール送信
                $subject = "【Resumeの取得に失敗しました".$agent."　code:search06】". $job->job_title . " JOB:" . $jobid . " userid:" . $login_id . "error : " . $hrbcErr;
                $message = "Resumeの取得に失敗しました。";
                $this->errMail($subject, $message);
            }

            if(empty($error)) {
                //お気に入りから削除
                $esc_jobid = $this->db->escape($jobid);
                $esc_userid = $this->db->escape($login_id);
                $sql = "Delete from likes where member_id = " . $esc_userid . " and job_id = " . $esc_jobid;
                $query = $this->db->query($sql);

                //$resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id . "?menu_id=5{/unwrap}";
                $resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $resume_id ."?menu_id=5#/dv/17:" . $resume_id ."/{/unwrap}";

                $job_url = "{unwrap}Job: https://hrbc-jp.porterscloud.com/job/search/id/".$jobid."?menu_id=3{/unwrap}";

                $process_url = "{unwrap}Process: https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $resume_id . "#/dv/7:" . $processId . "/{/unwrap}";
                $unit_url  =$resume_url . "\n" . $job_url . "\n" . $process_url;

                $error=0;
                //Email送信 【マイページエントリー(SPorPC)】企業名 JOB:00000 / 00歳 0社 ○○卒 男性or女性 氏名
                $person_datas = $this->getPerson($person_id);
                $resume_datas = $this->getResume($resume_id);

                //$subject = "【既登録者(".$agent.")：お気に入りからの申込】JOB:" . $jobid . " " . $namae . " " . $person_datas["tel1"];

                $jobinfo = $this->getJobInfo($jobid);
                $jobowner = $this->getJobOwner($jobid);
                $processInfo = $this->getProcessInfo($resume_id, $jobinfo);
                $power = $processInfo["Process.P_Owner"];
                $process_owner_id = $power["User"]["User.P_Id"];

                $userData = $this->getUser($process_owner_id);
                //選考プロセス担当
                $userName="";
                if(!empty($userData)){
                    foreach($userData as $k => $v){
                        if(!empty($v)){
                            foreach (($v) as $k2 => $v2) {
                                if(!empty($v2["User.P_Name"])){
                                    if($v2["User.P_Id"] == $process_owner_id){
                                        $userName = $v2["User.P_Name"];
                                    }
                                }
                            }
                        }
                    }
                }

                //会社名
                $client_name = $job->c_title;

                $param_datas = array();
                $param_datas["process_owner"] = $userName;
                $param_datas["client_name"] = $client_name;
                $param_datas["job_owner"] = $jobowner;

                $this->sendMail6($agent, $jobid, $person_datas, $resume_datas, $processId, $recruiter, $job, $param_datas);

            }else{
                //エラーメール送信
                $subject = "【Resumeの取得に失敗しました".$agent."　code:search07】". $job->job_title . " JOB:" . $jobid . " userid:" . $login_id . "error : " . $error;
                $message = "Resumeの取得に失敗しました。";
                $this->errMail($subject, $message);
            }
        }else{
            //ajax失敗
            $error="ajax失敗";
            //エラーメール送信
            $subject = "【ajax通信に失敗しました。　code:search08】". $job->job_title . " JOB:" . $jobid . " userid:" . $login_id  . "error : " . $error;
            $message = "ajax通信に失敗しました。";
            $this->errMail($subject, $message);
        }
        echo $error;
    }

    ////////////////////////////////////////////////////////////////////////
    // 検索用のセッション
    /////////////////////////////////////////////////////////////////////////
    function sessionClear(){
        $this->session->unset_userdata('job1');
        $this->session->unset_userdata('job');
        $this->session->unset_userdata('tenkin');
        $this->session->unset_userdata('jobs');
        $this->session->unset_userdata('pref');
        $this->session->unset_userdata('employees');
        $this->session->unset_userdata('keyword');
        $this->session->unset_userdata('keyword_flg');
        $this->session->unset_userdata('year_income');
    }
}