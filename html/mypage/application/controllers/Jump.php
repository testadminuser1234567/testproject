<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Jump extends MY_Controller {
 
    function __construct()
    {
        parent::__construct();
    }
 
    /**
     * 404ページ
     */
    public function url() {
        $data["render"] = "jump/home";
        $this->load->view("template_nologin", $data);
    }
 
}