<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller
{

    protected $page_limit = 20;

    //タイトル
    protected $sub_title = "ユーザー";

    //segment
    protected $segment = "user";

    //database
    protected $database = "members";
    protected $databaseJob = "jobs";
    protected $databaseClient = "clients";

    //画像がonの時 許可ファイル
    protected $allow = 'doc|xls|ppt|pptx|pdf|docx|xlsx|html|txt|htm';

    //HRBC URL
    protected $hrbcUrl = "https://api-hrbc-jp.porterscloud.com/v1/";

    //Pardot URL
    protected $pardotUrl = "https://info.hurex.jp/l/578091/2019-10-07/bjkl5l?";

    //転勤なしのID
    private $tenkin_flg = 11247;

    //転職相談会用
    protected $soudan = array("j","g","s");
    protected $j = "東北Ｕ・Ｉターン転職相談会 in 東京";
    //以下の文言は使わない（今後使うかも）
//    protected $g = "長期休暇相談会";
    protected $g = "年末年始Uターン&地元転職相談会";
    protected $s = "シルバーウィーク相談会";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('form_validation', 'encrypt'));
        $this->load->helper(array('url','cookie'));
    }

    // redirect if needed, otherwise display the user list
    function index()
    {
        $this->login();
    }

    //////////////////////////////////////////////////////////
    //ログイン
    //////////////////////////////////////////////////////////
    function login()
    {
        //POSTデータの取得
        $posts = $this->input->post();

        //エラーメッセージ格納用
        $errors = "";

        //クッキーの読み出し 自動ログイン
        $login_cookie = "";
        $login_cookie = get_cookie("loginkey");

        //パラメータチェック
        // p=jは参加日用
        // p=数字はジョブ登録用
        $get = $this->input->get();
        $param="";
        if(!empty($get)){
            if(!empty($get["p"])){
                $param = $get["p"];
            }
        }else if(!empty($posts["p"])){
            $param = $posts["p"];
        }

        //ヒューレックスからJOBの詳細を送ったときの対応
        $job="";
        if(!empty($get["j"])){
            $job = $get["j"];
            $job = htmlspecialchars($job, ENT_QUOTES, 'UTF-8');
        }else if(!empty($posts["j"])) {
            $job = htmlspecialchars($posts["j"], ENT_QUOTES, 'UTF-8');
        }

        /*
        //ログイン情報を記憶している場合
        */
        if (!empty($login_cookie) && empty($post)) {
            //DBにアクセス
            $return = $this->db->get_where($this->database, array('loginkey' => $login_cookie, 'del' => 0))->row();

            //メールアドレス変更チェック
            $iv = $return->iv;
            $mail = $return->email;
            $email = $this->mail_decrypt($iv, $mail);
            $email = trim($email);

            if (!empty($return->id) && $return->loginkey == $login_cookie) {
                //ログインOK
                $login_hash = hash_hmac("sha256", date("Ymd") . $return->email, Salt);
                $last_login = date('Y-m-d h:i:s');
                $user_agent = $_SERVER['HTTP_USER_AGENT'];
                $ip = $_SERVER["REMOTE_ADDR"];

                $data = array(
                    'login_hash' => $login_hash,
                    'last_login' => $last_login,
                    'user_agent' => $user_agent,
                    'ip' => $ip
                );
                $this->db->where('id', $return->id);
                $this->db->update($this->database, $data);

                //sessionIDの保持
                $this->setLoginId($return->id);

                //login情報をHRBCに登録
                $this->lastLogin();

                if(!empty($return->parameter)){
                    $entry = str_replace("'","", $return->parameter);
                    $entry = explode("|",$entry);
                    $param = $entry[1];

                    $data = array(
                        'parameter' => ""
                    );
                    $where = array(
                        'id' => $return->id
                    );
                    $ret = $this->db->update($this->database, $data, $where);
                }

                //////////////////////////////////////////////////////////////////////////////////////////////////
                // Pardotに連携
                //////////////////////////////////////////////////////////////////////////////////////////////////
                $param_pardot = "";
                $param_pardot .= "email=" . urlencode($email);

                $url = $this->pardotUrl . $param_pardot; // リクエストするURLとパラメータ

                // curlの処理を始める合図
                $curl = curl_init($url);

                // リクエストのオプションをセットしていく
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); // メソッド指定
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 証明書の検証を行わない
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // レスポンスを文字列で受け取る

                // レスポンスを変数に入れる
                $response = curl_exec($curl);
                $info    = curl_getinfo($curl);
                $errorNo = curl_errno($curl);

                // OK以外はエラーなので空白配列を返す
                if (!empty($response)) {
                    // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                    // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                    $subject = "【pardot連携13】  errorNo:" . $errorNo;
                    $errmsg = $posts["mail1"] . "\n" . "login記憶" . "\n" . $response;
                    $message = $errmsg;
                    $this->errMail($subject, $message);
                }

                // OK以外はエラーなので空白配列を返す
                if ($errorNo !== CURLE_OK) {
                    // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                    // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                    $subject = "【pardot連携14】  errorNo:" . $errorNo;
                    $errmsg = $posts["mail1"] . "\n" . "login記憶";
                    $message = $errmsg;
                    $this->errMail($subject, $message);
                }

                // 200以外のステータスコードは失敗とみなし空配列を返す
                if ($info['http_code'] !== 200) {
                    $subject = "【pardot連携15】  errorNo:" . $errorNo;
                    $errmsg = $posts["mail1"] . "\n" . "login記憶";
                    $message = $errmsg;
                    $this->errMail($subject, $message);
                }

                // curlの処理を終了
                curl_close($curl);
                //////////////////////////////////////////////////////////////////////////////////////////////////
                // Pardotに連携ここまで
                //////////////////////////////////////////////////////////////////////////////////////////////////

                //転職相談会（GW、シルバーウィーク等）
                if(in_array($param,$this->soudan)) {
                    redirect($this->segment . "/join/?p=".$param . "&action=ui_member");
                    //ジョブ詳細から
                }else if(!empty($param) && is_numeric($param)) {
                    //user agent
                    $agent = $this->getUserAgent();

                    //JOBの情報取得
                    $jobinfo = $this->getJobInfo($param);
                    //リクルート情報取得
                    $recruits = $this->getRecruitInfo($jobinfo);

                    $addurl = "";
                    $processId = $this->setProcess($param, $return->hrbc_person_id, $return->hrbc_resume_id, $jobinfo, $recruits);

                    $person = $this->getPerson($return->hrbc_person_id);
                    $resume = $this->getResume($return->hrbc_resume_id);

                    $jobowner = $this->getJobOwner($param);
                    $processInfo = $this->getProcessInfo($return->hrbc_resume_id, $jobinfo);
                    $power = $processInfo["Process.P_Owner"];
                    $process_owner_id = $power["User"]["User.P_Id"];

                    $userData = $this->getUser($process_owner_id);
                    //選考プロセス担当
                    $userName = "";
                    if (!empty($userData)) {
                        foreach ($userData as $k => $v) {
                            if (!empty($v)) {
                                foreach (($v) as $k2 => $v2) {
                                    if (!empty($v2["User.P_Name"])) {
                                        if ($v2["User.P_Id"] == $process_owner_id) {
                                            $userName = $v2["User.P_Name"];
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //会社名
                    $client_name = $jobinfo->c_title;

                    $param_datas = array();
                    $param_datas["process_owner"] = $userName;
                    $param_datas["client_name"] = $client_name;
                    $param_datas["job_owner"] = $jobowner;

                    if ($processId) {
                        if ($processId == "error301") {
                            $addurl = "&e=301";

                            $process_id = $this->getProcess($return->hrbc_resume_id, $jobinfo);

                            //既に登録済みの人の場合のメール to Hurex
                            $this->sendMail4_alreay($agent, $param, $person, $resume, $processId, $recruits, $jobinfo, $process_id);
                            $this->sendMailThanks4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo);
                        } else {
                            //メール送信
                            $this->sendMail4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo, $param_datas);
                            $this->sendMailThanks4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo);
                        }
                    } else {
                        //メール送信
                        $this->sendMail4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo, $param_datas);
                        $this->sendMailThanks4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo);
                    }

                    redirect($this->segment . "/home/" . "?action=job_member" . $addurl);
                }else if($param == "k"){
                    //希望JOBの検索
                    redirect("search/home/");
                }else if(!empty($job)){
                    redirect("search/detail/" . $job . "/");
                }else{
                    redirect($this->segment."/home/" . "?action=member");
                }
            }else{
                //自動ログイン削除
                $this->input->set_cookie("loginkey", "", "");

                //login_status delete
                $sess = get_cookie("ci_session");
                $this->db->where('id', $sess);
                $this->db->delete('ci_sessions');

                // log the user out
                $this->session->unset_userdata('login_id');

                $this->session->sess_destroy();

                // redirect them to the login page
                $add="";
                if(!empty($job)) $add = "?j=" . htmlspecialchars($job, ENT_QUOTES, 'UTF-8');
                redirect($this->segment.'/login/' . $add);
            }
            //通常のログイン処理
        } else if (!empty($posts)) {

            $username = $this->clean->purify($posts['email']);
            $password = $this->clean->purify($posts['password']);

            if (!strlen($username)) {
                $errors .= 'メールアドレスを入力してください<br />';
            }

            if (!strlen($password)) {
                $errors .= 'パスワードを入力してください<br />';
            }

            //エラーがなければユーザの照合
            if (empty($errors)) {
                //DBにアクセス
                $hash_email = hash_hmac("sha256", $posts['email'], Salt);
                $pass = hash_hmac("sha256", $password, Salt);
                $salt = hash_hmac("sha256", Salt, Salt);
                $pass_unit = $salt . $pass;
                $return = $this->db->get_where($this->database, array('hash_email' => $hash_email, 'password' => $pass_unit, 'hrbc_flg'=>1, 'activate'=>1, 'del' => 0))->row();

                //postのユーザとDBが違う場合は失敗
                if (empty($return->email) || $return->hash_email != $hash_email) {

                    //ユーザ登録が未完了（新規、既存両方）
                    $user_info = $this->db->get_where($this->database, array('hash_email' => $hash_email, 'password' => $pass_unit, 'activate'=>0, 'status'=>2, 'del'=>0))->row();
                    //$user_info = $this->db->get_where($this->database, array('hash_email' => $hash_email, 'hrbc_flg'=>0, 'activate'=>0, 'status'=>2, 'del'=>0))->row();

                    //STEP1登録済みの場合はSTEP2に飛ばす
                    if(!empty($user_info->id)) {
                        //data設定
                        $this->session->set_userdata('email', $posts["email"]);
                        $this->session->set_userdata('hrbc_flg', $user_info->hrbc_flg);
                        $this->session->set_userdata('param', $user_info->parameter);
                        redirect("signup/step2/");
                    }


                    $errors .= 'メールアドレスかパスワードが不正です<br />';

                    //data設定
                    $data["render"] = $this->segment . "/login";
                    $data["clean"] = $this->clean;
                    $data["msg"] = $errors;
                    $data["param"] = $param;
                    $data["job"] = $job;
                    $data["segment"] = $this->segment;

                    $this->load->view("template_nologin", $data);
                    //成功
                } else {
                    //ログイン情報を記憶した場合は値保存
                    if (!empty($posts['loginkey']) && $posts['loginkey'] === 'on') {
                        $loginkey = $return->id . hash_hmac("sha256", date("YmdHis"), Salt);
                        $data = array(
                            'loginkey' => $loginkey
                        );
                        $this->db->where('id', $return->id);
                        $this->db->update($this->database, $data);

                        //クッキーに書き込み
                        $expire = time() + 1 * 24 * 3600; // 1日
                        $this->input->set_cookie("loginkey", $loginkey, $expire);
                    }
                    //ログインOK
                    $login_hash = hash_hmac("sha256", date("Ymd") . $return->email, Salt);
                    $last_login = date('Y-m-d h:i:s');
                    $user_agent = $_SERVER['HTTP_USER_AGENT'];
                    $ip = $_SERVER["REMOTE_ADDR"];

                    $data = array(
                        'login_hash' => $login_hash,
                        'last_login' => $last_login,
                        'user_agent' => $user_agent,
                        'ip' => $ip
                    );
                    $this->db->where('id', $return->id);
                    $this->db->update($this->database, $data);

                    //sessionIDの保持
                    $this->setLoginId($return->id);

                    $this->lastLogin();

                    if(!empty($return->parameter)){
                        $entry = str_replace("'","", $return->parameter);
                        $entry = explode("|",$entry);
                        $param = $entry[1];

                        $data = array(
                            'parameter' => ""
                        );
                        $where = array(
                            'id' => $return->id
                        );
                        $ret = $this->db->update($this->database, $data, $where);

                    }

                    //////////////////////////////////////////////////////////////////////////////////////////////////
                    // Pardotに連携
                    //////////////////////////////////////////////////////////////////////////////////////////////////
                    $param_pardot = "";
                    $param_pardot .= "email=" . urlencode($posts["email"]);

                    $url = $this->pardotUrl . $param_pardot; // リクエストするURLとパラメータ

                    // curlの処理を始める合図
                    $curl = curl_init($url);

                    // リクエストのオプションをセットしていく
                    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); // メソッド指定
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 証明書の検証を行わない
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // レスポンスを文字列で受け取る

                    // レスポンスを変数に入れる
                    $response = curl_exec($curl);
                    $info    = curl_getinfo($curl);
                    $errorNo = curl_errno($curl);

                    // OK以外はエラーなので空白配列を返す
                    if (!empty($response)) {
                        // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                        // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                        $subject = "【pardot連携16】  errorNo:" . $errorNo;
                        $errmsg = $posts["mail1"] . "\n" . "loginノーマル" . "\n" . $response;
                        $message = $errmsg;
                        $this->errMail($subject, $message);
                    }

                    // OK以外はエラーなので空白配列を返す
                    if ($errorNo !== CURLE_OK) {
                        // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                        // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                        $subject = "【pardot連携17】  errorNo:" . $errorNo;
                        $errmsg = $posts["mail1"] . "\n" . "loginノーマル";
                        $message = $errmsg;
                        $this->errMail($subject, $message);
                    }

                    // 200以外のステータスコードは失敗とみなし空配列を返す
                    if ($info['http_code'] !== 200) {
                        $subject = "【pardot連携18】  errorNo:" . $errorNo;
                        $errmsg = $posts["mail1"] . "\n" . "loginノーマル";
                        $message = $errmsg;
                        $this->errMail($subject, $message);
                    }

                    // curlの処理を終了
                    curl_close($curl);
                    //////////////////////////////////////////////////////////////////////////////////////////////////
                    // Pardotに連携ここまで
                    //////////////////////////////////////////////////////////////////////////////////////////////////


                    if(in_array($param,$this->soudan)) {
                        redirect($this->segment . "/join/?p=".$param . "&action=ui_member");
                    }else if(!empty($param) && is_numeric($param)){

                        //user agent
                        $agent = $this->getUserAgent();

                        //JOBの情報取得
                        $jobinfo = $this->getJobInfo($param);
                        //リクルート情報取得
                        $recruits = $this->getRecruitInfo($jobinfo);

                        $addurl="";
                        $processId = $this->setProcess($param, $return->hrbc_person_id, $return->hrbc_resume_id, $jobinfo, $recruits);

                        $person = $this->getPerson($return->hrbc_person_id);
                        $resume = $this->getResume($return->hrbc_resume_id);

                        $jobowner = $this->getJobOwner($param);
                        $processInfo = $this->getProcessInfo($return->hrbc_resume_id, $jobinfo);
                        $power = $processInfo["Process.P_Owner"];
                        $process_owner_id = $power["User"]["User.P_Id"];

                        $userData = $this->getUser($process_owner_id);
                        //選考プロセス担当
                        $userName="";
                        if(!empty($userData)){
                            foreach($userData as $k => $v){
                                if(!empty($v)){
                                    foreach (($v) as $k2 => $v2) {
                                        if(!empty($v2["User.P_Name"])){
                                            if($v2["User.P_Id"] == $process_owner_id){
                                                $userName = $v2["User.P_Name"];
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //会社名
                        $client_name = $jobinfo->c_title;

                        $param_datas = array();
                        $param_datas["process_owner"] = $userName;
                        $param_datas["client_name"] = $client_name;
                        $param_datas["job_owner"] = $jobowner;

                        if($processId){
                            if($processId=="error301"){
                                $addurl = "&e=301";

                                $process_id = $this->getProcess($return->hrbc_resume_id, $jobinfo);

                                //既に登録済みの人の場合のメール to Hurex
                                $this->sendMail4_alreay($agent, $param, $person, $resume, $processId, $recruits, $jobinfo, $process_id);
                                $this->sendMailThanks4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo);
                            }else{
                                //メール送信
                                $this->sendMail4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo, $param_datas);
                                $this->sendMailThanks4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo);
                            }
                        }else{
                            //メール送信
                            $this->sendMail4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo, $param_datas);
                            $this->sendMailThanks4($agent, $param, $person, $resume, $processId, $recruits, $jobinfo);
                        }


                        redirect($this->segment."/home/" . "?action=job_member" . $addurl);
                    }else if($param == "k"){
                        //希望JOBの検索
                        redirect("search/home/");
                    }else if(!empty($job)){
                        redirect("search/detail/" . $job . "/");
                    }else{
                        redirect($this->segment."/home/" . "?action=member");
                    }
                }
                //IDかパスワードが入力されていない場合
            } else {
                //data設定
                if(in_array($param,$this->soudan)) {
                    $data["render"] = $this->segment . "/login_join";
                }else{
                    $add="";
                    if(!empty($job)) $add = "?j=" . htmlspecialchars($job, ENT_QUOTES, 'UTF-8');
                    $data["render"] = $this->segment . "/login" . $add;
                }
                $data["clean"] = $this->clean;
                $data["msg"] = $errors;
                $data["segment"] = $this->segment;
                $data["param"] = $param;
                $data["job"] = $job;
                $this->load->view("template_nologin", $data);
            }
            //初期画面
        } else {
            ///////////////////////////////////////////////////////////////////////////
            //data設定
            ///////////////////////////////////////////////////////////////////////////
            $errors .= $this->session->flashdata('msg');;
            if(in_array($param,$this->soudan)) {
                $data["render"] = $this->segment . "/login_join";
            }else{
                $add="";
                if(!empty($job)) $add = "?j=" . htmlspecialchars($job, ENT_QUOTES, 'UTF-8');
                $data["render"] = $this->segment . "/login";
            }
            $data["clean"] = $this->clean;
            $data["msg"] = $errors;
            $data["param"] = $param;
            $data["job"] = $job;
            $data["segment"] = $this->segment;
            $this->load->view("template_nologin", $data);
        }
    }

    //////////////////////////////////////////////////////////
    //UIターン参加
    //////////////////////////////////////////////////////////
    function join()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        //POSTデータの取得
        $posts = $this->input->post();

        //パラメータチェック
        // p=jは参加日用
        // p=数字はジョブ登録用
        $get = $this->input->get();
        $param="";
        if(!empty($get)){
            if(!empty($get["p"])){
                $param = $get["p"];
            }
        }else if(!empty($posts["p"])){
            $param = $posts["p"];
        }

        $text = $this->j;
        if(!empty($this->$param)){
            $text = $this->$param;
        }

        //対象のID取得
        $ret = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $person_id = $ret->hrbc_person_id;
        $resume_id = $ret->hrbc_resume_id;

        $error= "";

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $data[$this->segment] = (object) $posts;

        ////////////////////////////////////////////////////////////////////
        //// 閲覧履歴
        //////////////////////////////////////////////////////////////////////
        $looks = $this->getLooks($login_id);
        $data["looks"] = $looks;

        $data["render"] = $this->segment . "/join";
        $data["clean"] = $this->clean;
        $data["msg"] = $error;
        $data["param"] = $param;
        $data["title"] = $text;
        $data["segment"] = $this->segment;
        $data["body_id"] = "registration";

        $this->load->view("template_search", $data);
    }

    //////////////////////////////////////////////////////////
    //UIターン確認画面
    //////////////////////////////////////////////////////////
    function join_confirm()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        //POSTデータの取得
        $posts = $this->input->post();

        //パラメータチェック
        // p=jは参加日用
        // p=数字はジョブ登録用
        $get = $this->input->get();
        $param="";
        if(!empty($get)){
            if(!empty($get["p"])){
                $param = $get["p"];
            }
        }else if(!empty($posts["p"])){
            $param = $posts["p"];
        }

        $text = $this->j;
        if(!empty($this->$param)){
            $text = $this->$param;
        }


        //対象のID取得
        $ret = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $person_id = $ret->hrbc_person_id;
        $resume_id = $ret->hrbc_resume_id;

        //バリデーション実行
        $error = "";
        $tmp_error = 0;
        $e_msg = "";

        //パスワードチェック
        if(empty($posts["comment"])){
            $error.="<p>ご希望の参加日を入力してください。</p>";
        }

        //オブジェクトへ変換
        $data[$this->segment] = (object)$posts;

        ////////////////////////////////////////////////////////////////////
        //// 閲覧履歴
        //////////////////////////////////////////////////////////////////////
        $looks = $this->getLooks($login_id);
        $data["looks"] = $looks;

        if($error !== ""){
            //data設定
            $data[$this->segment] =  (object) $posts;
            $data["render"] = $this->segment."/join";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["msg"] = $error;
            $data["title"] = $text;
            $data["param"] = $param;
            $data["segment"] = $this->segment;
            $data["body_id"] = "registration";

            $this->load->view("template_search", $data);
        }else{
            //data設定
            $data["render"] = $this->segment . "/join_confirm";
            $data["clean"] = $this->clean;
            $data["msg"] = $error;
            $data["param"] = $param;
            $data["title"] = $text;
            $data["sub_title"] = $this->sub_title;
            $data["body_id"] = "registration";
            $data["segment"] = $this->segment;

            $this->load->view("template_search", $data);
        }
    }

    //////////////////////////////////////////////////////////
    //UIターン　送信
    //////////////////////////////////////////////////////////
    function join_send()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        //対象のID取得
        $ret = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $person_id = $ret->hrbc_person_id;
        $resume_id = $ret->hrbc_resume_id;

        $posts = $this->input->post();
        $param="";
        if(!empty($get)){
            if(!empty($get["p"])){
                $param = $get["p"];
            }
        }else if(!empty($posts["p"])){
            $param = $posts["p"];
        }

        $text = $this->j;
        if(!empty($this->$param)){
            $text = $this->$param;
        }

        //バリデーション実行
        $error = "";
        if(empty($posts["comment"])){
            $error.="<p>ご希望の参加日を入力してください。</p>";
        }

        if($error !== ""){
            //data設定
            $data[$this->segment] =  (object) $posts;
            $data["render"] = $this->segment."/join";
            $data["clean"] = $this->clean;
            $data["msg"] = $error;
            $data["title"] = $text;
            $data["segment"] = $this->segment;
            $data["body_id"] = "registration";

            $this->load->view("template", $data);
        }else{
            $result = $this->getHrbcMaster();

            ///////////////////////////////////////////////////
            //HRBCのresume取得（参加日用）
            ///////////////////////////////////////////////////
            /*
            $hrbc_url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Candidate:eq=' . $person_id, 'field'=>'Resume.P_Id,Resume.P_Owner,Resume.U_8E20986D8967D107BEE5D63870AD68', 'order'=>'Resume.P_Id:asc'));
            $url = "https://api-hrbc-jp.porterscloud.com/v1/resume?" . $hrbc_url;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);

            //file_get_contents関数でデータを取得
            if($xml = @file_get_contents($url, false, $context)){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404"));
                            redirect($this->segment."/home/");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                            redirect($this->segment."/home/");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                            redirect($this->segment."/home/");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout"));
                    redirect($this->segment."/home/");
                }
            }
            $xml = simplexml_load_string($xml);
            */
            $url = $this->hrbcUrl .  "resume?partition=". $result["partition"] . "&count=1&start=0&field=Resume.P_Id,Resume.P_Owner,Resume.U_8E20986D8967D107BEE5D63870AD68&condition=Resume.P_Candidate:eq=".$person_id."&order=Resume.P_Id:asc";

            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

            if($xml->Code!=0){
                $pErr=$xml->Code;

                $subject = "【code:user1】" . $pErr;
                $message = $pErr;
                $this->errMail($subject, $message);

                $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
                redirect("err/error_404/");
                //exit;
            }

            //データ設定
            $json = json_encode($xml);
            $resumeData = json_decode($json, true);
            $comment = $resumeData['Item']['Resume.U_8E20986D8967D107BEE5D63870AD68'];
            if(!empty($comment)){
                if(is_array($comment)){
                    $tmp="";
                    foreach ($comment as $k => $v){
                        $tmp .= $v . "\r\n";
                    }
                    $comment = $tmp;
                }else{
                    $comment = $comment;
                }
            }else{
                $comment="";
            }
            $owner_id = $resumeData['Item']["Resume.P_Owner"]["User"]["User.P_Id"];
            $comment = $comment . "&#x0A;" . $posts["comment"];

            ///////////////////////////////////////////////////////////
            // resume に参加日登録
            ///////////////////////////////////////////////////////////
            $url = $this->hrbcUrl . "resume?partition=". $result["partition"];

            //※レジュメにメールとTELを入れるとエラーになる
            $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>' . $owner_id . '</Resume.P_Owner><Resume.P_Candidate>'.$person_id.'</Resume.P_Candidate><Resume.U_8E20986D8967D107BEE5D63870AD68>' . $comment . '</Resume.U_8E20986D8967D107BEE5D63870AD68></Item></Resume>';
            $header = array(
                "Content-Type: application/xml; charset=UTF-8",
                "X-porters-hrbc-oauth-token: " . $result['token']
            );
            $tmp_xml = $xml;

            /*
            $options = array('http' => array(
                'method' => 'POST',
                'content' => $xml,
                'header' => implode("\r\n", $header)
            ));
            if($xml = @file_get_contents($url, false, stream_context_create($options))){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404"));
                            redirect($this->segment."/home/");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500"));
                            redirect($this->segment."/home/");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other"));
                            redirect($this->segment."/home/");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout"));
                    redirect($this->segment."/home/");
                }
            }
            */
            $options = array
            (
                CURLOPT_URL            => $url,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $xml,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result['token']),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

            //xml解析
            $rErr="";
            $rId = "";

            $this->writeRegistLog($tmp_xml, $xml, "user01");

            $error="";
            if($xml->Code!=0){
                $subject = "【resume参加日登録失敗　code:user02】 resume:" . $resume_id . " person:" . $person_id;
                $message = "resume参加日登録失敗しました。 resume:" . $resume_id . " person:" . $person_id;
                $this->errMail($subject, $message);
                $rErr=$xml->Code;
            }else{
                $rCode="";
                $json = json_encode($xml);
                $arr = json_decode($json,true);
                foreach($arr['Item'] as $k => $v){
                    if($k=="Id"){
                        $rId = $v;
                    }
                    if($k=="Code"){
                        $rErr = $v;
                    }
                }
            }
            if(!$rId || $rErr){
                $subject = "【resume参加日登録失敗　code:user03】resume:" . $resume_id . " person:" . $person_id;
                $message = "resume参加日登録失敗しました。 resume:" . $resume_id . " person:" . $person_id;
                $this->errMail($subject, $message);
                $error.= "エラーが発生しました。(Resume: ".$rErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
            }

            //PCとスマホの判定
            $agent = $this->getUserAgent();

            //person取得
            $person = $this->getPerson($ret->hrbc_person_id);
            $resume = $this->getResume($ret->hrbc_resume_id);

            //XMLの改行対策
            $comment = str_replace("&#x0A;","\n",$comment);
            $this->sendMail2($agent, $param, $person, $resume, $comment, $text);

            //登録成功
            $this->session->set_flashdata(array("msg"=>"登録が完了しました。"));
            redirect($this->segment."/join_complete?p=".$param);
        }
    }

    //////////////////////////////////////////////////////////
    //相談会完了画面
    //////////////////////////////////////////////////////////
    function join_complete(){
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $get = $this->input->get();
        $param="";
        if(!empty($get)){
            if(!empty($get["p"])){
                $param = $get["p"];
            }
        }else if(!empty($posts["p"])){
            $param = $posts["p"];
        }

        $text = $this->j;
        if(!empty($this->$param)){
            $text = $this->$param;
        }

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        ////////////////////////////////////////////////////////////////////
        //// 閲覧履歴
        //////////////////////////////////////////////////////////////////////
        $looks = $this->getLooks($login_id);
        $data["looks"] = $looks;

        //data設定
        $data["render"] = $this->segment."/join_complete";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["title"] = $text;
        $data["segment"] = $this->segment;
        $data["body_id"] = "registration";

        $this->load->view("template_search", $data);
    }

    //////////////////////////////////////////////////////////
    //ログアウト
    //////////////////////////////////////////////////////////
    function logout()
    {
        $this->_login_check();

        //自動ログイン削除
        $this->input->set_cookie("loginkey", "", "");

        $login_id = $this->session->userdata('login_id');
        $data = array(
            'loginkey' => ""
        );
        $this->db->where('id', $login_id);
        $this->db->update($this->database, $data);

        //login_status delete
        $sess = get_cookie("ci_session");
        $this->db->where('id', $sess);
        $this->db->delete('ci_sessions');

        // log the user out
        $this->session->unset_userdata('login_id');

        $this->session->sess_destroy();

        // redirect them to the login page
        redirect($this->segment.'/login');
    }

    //////////////////////////////////////////////////////////
    //ログアウト(強制終了）
    //////////////////////////////////////////////////////////
    function finishall()
    {
        $redirectUrl = "";
        $get = $this->input->get();
        if(!empty($get)){
            $redirectUrl = "?j=" . $get["j"];
        }

        //自動ログイン削除
        $this->input->set_cookie("loginkey", "", "");

        // log the user out
        $this->session->unset_userdata('login_id');

        $this->session->sess_destroy();

        // redirect them to the login page
        redirect($this->segment.'/login' . $redirectUrl);
    }

    //////////////////////////////////////////////////////////
    //一覧出力
    //////////////////////////////////////////////////////////
    function home()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        //対象のID取得
        $ret = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $person_id = $ret->hrbc_person_id;
        $resume_id = $ret->hrbc_resume_id;

        $posts = $this->input->post();

        //HRBC取得
        $result = $this->getHrbcMaster();
        $data = array();

        ////////////////////////////////////////////////////////////////////////////////////////////
        //Process
        ////////////////////////////////////////////////////////////////////////////////////////////
//        $param = http_build_query(array('partition' => $result["partition"], 'count' => 200, 'start' => 0, 'condition' => 'Process.P_Resume:eq=' . $resume_id , 'field' => 'Process.P_Id,Process.P_Job,Process.P_Owner,Process.P_Recruiter,Process.P_Resume, Process.U_4E29163A766BC0D6CA7D9B1157A614,Process.P_Phase,Process.P_PhaseMemo,Process.P_Close,Process.P_PhaseDate,Process.P_Client,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.P_UpdateDate,Process.U_186596AC430DB9AF87A142A6A47DC7,Process.U_419EC7AD9BE7E1A377DF8B6D0115A1,Process.U_EFEE750C9B6F3D74F06F8E821E1C79,Process.U_1C50E7D9F0AD650BF5159E1BE0DF6D,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.U_5D69124062C230BB6DEC5F4D812C57,Process.U_C10D749E15C59A371D293282096F5B,Process.U_3E781B18BA915F475A1FA174CAAD69,Process.P_UpdateDate', 'order' => 'Process.P_PhaseDate:desc'));
/*
        $param = http_build_query(array('partition' => $result["partition"], 'count' => 200, 'start' => 0, 'condition' => 'Process.P_Resume:eq=' . $resume_id , 'field' => 'Process.P_Id,Process.P_Job,Process.P_Owner,Process.P_Recruiter,Process.P_Resume,Process.U_4E29163A766BC0D6CA7D9B1157A614,Process.P_Phase,Process.P_PhaseMemo,Process.P_Close,Process.P_PhaseDate,Process.P_Client,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.P_UpdateDate,Process.U_186596AC430DB9AF87A142A6A47DC7,Process.U_419EC7AD9BE7E1A377DF8B6D0115A1,Process.U_EFEE750C9B6F3D74F06F8E821E1C79,Process.U_1C50E7D9F0AD650BF5159E1BE0DF6D,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.U_5D69124062C230BB6DEC5F4D812C57,Process.U_C10D749E15C59A371D293282096F5B,Process.U_3E781B18BA915F475A1FA174CAAD69,Process.P_UpdateDate', 'order' => 'Process.P_UpdateDate:desc'));

        $url = $this->hrbcUrl . "process?" . $param;
        $opts = array(
            'http' => array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        $xml="";
        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 home"));
                        $subject = "【user home プロセス失敗】 ";
                        $message = "404 error" . $xml;
                        $this->errMail($subject, $message);
                        redirect($this->segment."/home/");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 home"));
                        $subject = "【user home プロセス失敗】 ";
                        $message = "500 error" . $xml;;
                        $this->errMail($subject, $message);
                        redirect($this->segment."/home/");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other home"));
                        $subject = "【user home プロセス失敗】 ";
                        $message = $status_code[1] . " error" . $xml;;
                        $this->errMail($subject, $message);
                        redirect($this->segment."/home/");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout home"));
                $subject = "【user home プロセス失敗】 ";
                $message = "timeout error" . $xml;;
                $this->errMail($subject, $message);
                redirect($this->segment."/home/");
            }
        }
        $xml = simplexml_load_string($xml);
*/
        $url = $this->hrbcUrl .  "process?partition=". $result["partition"] . "&count=200&start=0&field=Process.P_Id,Process.P_Job,Process.P_Owner,Process.P_Recruiter,Process.P_Resume,Process.U_4E29163A766BC0D6CA7D9B1157A614,Process.P_Phase,Process.P_PhaseMemo,Process.P_Close,Process.P_PhaseDate,Process.P_Client,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.P_UpdateDate,Process.U_186596AC430DB9AF87A142A6A47DC7,Process.U_419EC7AD9BE7E1A377DF8B6D0115A1,Process.U_EFEE750C9B6F3D74F06F8E821E1C79,Process.U_1C50E7D9F0AD650BF5159E1BE0DF6D,Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11,Process.U_5D69124062C230BB6DEC5F4D812C57,Process.U_C10D749E15C59A371D293282096F5B,Process.U_3E781B18BA915F475A1FA174CAAD69,Process.P_UpdateDate&condition=Process.P_Resume:eq=".$resume_id."&order=Process.P_UpdateDate:desc";

        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

        if ($xml->Code != 0) {
            $pErr = $xml->Code;
            $subject = "【code:user2】" . $pErr;
            $message = $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));

            redirect("err/error_404/");
            //exit;

        }

        //HRBCから帰ってきたデータ件数　1件の対応用に取得
        $cnt = $xml->attributes()->Count;

        //データ設定
        $json = json_encode($xml);
        $processes = json_decode($json, true);

        $i = 0;
        $apply_i=0;
        $offer_i=0;
        //オファー件数
        $offer = array();
        //応募件数
        $apply = array();

        if ($cnt == 0) {
        } else if ($cnt != 1) {
            //複数データ
            foreach ($processes["Item"] as $val) {
                foreach ($val["Process.P_Phase"] as $k => $v) {
                    $opname = $v["Option.P_Name"];
                    $opId = $v["Option.P_Id"];
                    if ($opname == "求人詳細問合せ") {
                        $phaze = "問合せ中";
                        $phazeFlg = 0;

                        //応募件数の設定用
                        //ジョブと企業の設定
                        $apply_jobId = $val["Process.P_Job"]["Job"]["Job.P_Id"];
                        $apply_clientId = $val["Process.P_Client"]["Client"]["Client.P_Id"];
                        $apply_job = $this->db->get_where($this->databaseJob, array('job_id' => $apply_jobId))->row();
                        $apply_client = $this->db->get_where($this->databaseClient, array('c_id' => $apply_clientId))->row();
                        $apply_job = (array)$apply_job;
                        $apply_client = (array)$apply_client;

                        //職種の設定
                        if($apply_json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                            //ここにデータ取得が成功した時の処理
                        }else{
                            //エラー処理
                            if(count($http_response_header) > 0){
                                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                                //エラーの判別
                                switch($status_code[1]){
                                    //404エラーの場合
                                    case 404:
                                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 apply"));
                                        redirect($this->segment."/home/");
                                        break;

                                    //500エラーの場合
                                    case 500:
                                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 apply"));
                                        redirect($this->segment."/home/");
                                        break;

                                    //その他のエラーの場合
                                    default:
                                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other apply"));
                                        redirect($this->segment."/home/");
                                }
                            }else{
                                //タイムアウトの場合 or 存在しないドメインだった場合
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout apply"));
                                redirect($this->segment."/home/");
                            }
                        }
                        $apply_tmpJobcategory = json_decode($apply_json2, true);
                        $apply_jobCategoryName = "";
                        $apply_jobCategoryId = "";
                        foreach ($apply_tmpJobcategory['Item'] as $apply_jck => $apply_jcv) {
                            if (!empty($apply_jcv["Item"])) {
                                foreach ($apply_jcv['Item'] as $apply_jck2 => $apply_jcv2) {
                                    foreach ($apply_jcv2['Items'] as $apply_jck3 => $apply_jcv3) {
                                        foreach ($apply_jcv3 as $apply_jck4 => $apply_jcv4) {
                                            if ($apply_job["jobcategory_id"] == $apply_jcv4['Option.P_Id']) {
                                                $apply_jobCategoryName = htmlspecialchars($apply_jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                                $apply_jobCategoryId = htmlspecialchars($apply_jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //マイページに反映するチェック
                        $apply_mypageCheck = "";
                        $apply_mypageText = "";
                        if (!empty($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                            foreach ($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $apply_mk => $apply_mv) {
                                foreach($apply_mv as $apply_mk2 => $apply_mv2){
                                    if(!empty($apply_mv2)){
                                        $apply_mypageCheck = $apply_mv2["Option.P_Id"];
                                        $apply_mypageText = $apply_mv2["Option.P_Name"];
                                    }
                                }
                            }
                        }

                        //企業名表示/非表示
                        $apply_clientNameCheck = "";
                        $apply_clientNameDisplay = "";
                        if (!empty($val["Process.U_C10D749E15C59A371D293282096F5B"])) {
                            foreach ($val["Process.U_C10D749E15C59A371D293282096F5B"] as $apply_mk => $apply_mv) {
                                if (!empty($apply_mv)) {
                                    $apply_clientNameCheck = $apply_mv["Option.P_Id"];
                                    $apply_clientNameDisplay = $apply_mv["Option.P_Name"];
                                }
                            }
                        }

                        if($apply_mypageCheck != 11312) {
                            $apply[$apply_i]["processId"] = $val["Process.P_Id"];
                            $apply[$apply_i]["mypageCheck"] = $apply_mypageCheck;
                            $apply[$apply_i]["clientNameDisplay"] = $apply_clientNameDisplay;
                            $apply[$apply_i]["mypageText"] = $apply_mypageText;
                            $apply[$apply_i]["opname"] = $opname;
                            $apply[$apply_i]["phaze"] = $phaze;
                            $apply[$apply_i]["phazeId"] = $opId;
                            $apply[$apply_i]["phazeFlg"] = $phazeFlg;
                            $apply[$apply_i]["job"] = $apply_job;
                            $apply[$apply_i]["client"] = $apply_client;
                            $tmpdate = "";
                            if ($val["Process.P_PhaseDate"]) {
                                $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_PhaseDate"])));
                            }
                            $apply[$apply_i]["phasedate"] = $tmpdate;
                            $apply[$apply_i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_UpdateDate"][0])));
                            $apply[$apply_i]["jobcategoryName"] = $apply_jobCategoryName;
                            $apply[$apply_i]["jobcategoryId"] = $apply_jobCategoryId;
                            $apply_i++;
                        }

                        /* 20180702 「Job打診（社名非公開）」削除
                    } else if ($opname == "JOB打診" || $opname == "JOB打診（弱）" || $opname == "JOB打診【強！】" || $opname == "JOB打診（社名非公開）") {
                        */
                    } else if ($opname == "JOB打診" || $opname == "JOB打診（弱）" || $opname == "JOB打診【強！】") {
                        $phaze = "JOB打診";
                        $phazeFlg = 1;
                        //応募件数の設定用
                        //ジョブと企業の設定
                        $offer_jobId = $val["Process.P_Job"]["Job"]["Job.P_Id"];
                        $offer_clientId = $val["Process.P_Client"]["Client"]["Client.P_Id"];
                        $offer_job = $this->db->get_where($this->databaseJob, array('job_id' => $offer_jobId))->row();
                        $offer_client = $this->db->get_where($this->databaseClient, array('c_id' => $offer_clientId))->row();
                        $offer_job = (array)$offer_job;
                        $offer_client = (array)$offer_client;

                        //職種の設定
                        if($offer_json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                            //ここにデータ取得が成功した時の処理
                        }else{
                            //エラー処理
                            if(count($http_response_header) > 0){
                                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                                //エラーの判別
                                switch($status_code[1]){
                                    //404エラーの場合
                                    case 404:
                                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 offer"));
                                        redirect($this->segment."/home/");
                                        break;

                                    //500エラーの場合
                                    case 500:
                                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 offer"));
                                        redirect($this->segment."/home/");
                                        break;

                                    //その他のエラーの場合
                                    default:
                                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other offer"));
                                        redirect($this->segment."/home/");
                                }
                            }else{
                                //タイムアウトの場合 or 存在しないドメインだった場合
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout offer"));
                                redirect($this->segment."/home/");
                            }
                        }
                        $offer_tmpJobcategory = json_decode($offer_json2, true);
                        $offer_jobCategoryName = "";
                        $offer_jobCategoryId = "";
                        foreach ($offer_tmpJobcategory['Item'] as $offer_jck => $offer_jcv) {
                            if (!empty($offer_jcv["Item"])) {
                                foreach ($offer_jcv['Item'] as $offer_jck2 => $offer_jcv2) {
                                    foreach ($offer_jcv2['Items'] as $offer_jck3 => $offer_jcv3) {
                                        foreach ($offer_jcv3 as $offer_jck4 => $offer_jcv4) {
                                            if ($offer_job["jobcategory_id"] == $offer_jcv4['Option.P_Id']) {
                                                $offer_jobCategoryName = htmlspecialchars($offer_jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                                $offer_jobCategoryId = htmlspecialchars($offer_jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //マイページに反映するチェック
                        $offer_mypageCheck = "";
                        $offer_mypageText = "";
                        if (!empty($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                            foreach ($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $offer_mk => $offer_mv) {
                                foreach($offer_mv as $offer_mk2 => $offer_mv2){
                                    if(!empty($offer_mv2)){
                                        $offer_mypageCheck = $offer_mv2["Option.P_Id"];
                                        $offer_mypageText = $offer_mv2["Option.P_Name"];
                                    }
                                }
                            }
                        }

                        //企業名表示/非表示
                        $offer_clientNameCheck = "";
                        $offer_clientNameDisplay = "";
                        if (!empty($val["Process.U_C10D749E15C59A371D293282096F5B"])) {
                            foreach ($val["Process.U_C10D749E15C59A371D293282096F5B"] as $offer_mk => $offer_mv) {
                                if (!empty($offer_mv)) {
                                    $offer_clientNameCheck = $offer_mv["Option.P_Id"];
                                    $offer_clientNameDisplay = $offer_mv["Option.P_Name"];
                                }
                            }
                        }

                        if($offer_mypageCheck != 11312){
                            $offer[$offer_i]["processId"] = $val["Process.P_Id"];
                            $offer[$offer_i]["mypageCheck"] = $offer_mypageCheck;
                            $offer[$offer_i]["clientNameDisplay"] = $offer_clientNameDisplay;
                            $offer[$offer_i]["mypageText"] = $offer_mypageText;
                            $offer[$offer_i]["phaze"] = $phaze;
                            $offer[$offer_i]["phazeId"] = $opId;
                            $offer[$offer_i]["phazeFlg"] = $phazeFlg;
                            $offer[$offer_i]["job"] = $offer_job;
                            $offer[$offer_i]["client"] = $offer_client;
                            $tmpdate = "";
                            if ($val["Process.P_PhaseDate"]) {
                                $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_PhaseDate"])));
                            }
                            $offer[$offer_i]["phasedate"] = $tmpdate;
                            $offer[$offer_i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_UpdateDate"][0])));
                            $offer[$offer_i]["jobcategoryName"] = $offer_jobCategoryName;
                            $offer[$offer_i]["jobcategoryId"] = $offer_jobCategoryId;
                            $offer_i++;
                        }
                    } else if ($opname == "本人NG") {
                        $phaze = "辞退";
                        $phazeFlg = 0;

                        /*  20180702 「WEBエントリー」、「応募承諾（書類待ち）」、「社内NG予定」、「社内NG予定（社名非公開）」削除
                    } else if ($opname == "WEBエントリー" || $opname == "応募承諾（書類待ち）" || $opname == "応募承諾" || $opname == "社内NG予定" || $opname == "書類推薦" || $opname == "社内NG予定（社名非公開）") {
                        */
                    } else if ($opname == "応募承諾" || $opname == "応募承諾（書類待ち）" || $opname == "書類推薦") {

                        $phaze = "応募中";
                        $phazeFlg = 0;

                        //応募件数の設定用
                        //ジョブと企業の設定
                        $apply_jobId = $val["Process.P_Job"]["Job"]["Job.P_Id"];
                        $apply_clientId = $val["Process.P_Client"]["Client"]["Client.P_Id"];
                        $apply_job = $this->db->get_where($this->databaseJob, array('job_id' => $apply_jobId))->row();
                        $apply_client = $this->db->get_where($this->databaseClient, array('c_id' => $apply_clientId))->row();
                        $apply_job = (array)$apply_job;
                        $apply_client = (array)$apply_client;

                        //職種の設定
                        if($apply_json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                            //ここにデータ取得が成功した時の処理
                        }else{
                            //エラー処理
                            if(count($http_response_header) > 0){
                                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                                //エラーの判別
                                switch($status_code[1]){
                                    //404エラーの場合
                                    case 404:
                                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 apply"));
                                        redirect($this->segment."/home/");
                                        break;

                                    //500エラーの場合
                                    case 500:
                                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 apply"));
                                        redirect($this->segment."/home/");
                                        break;

                                    //その他のエラーの場合
                                    default:
                                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other apply"));
                                        redirect($this->segment."/home/");
                                }
                            }else{
                                //タイムアウトの場合 or 存在しないドメインだった場合
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout apply"));
                                redirect($this->segment."/home/");
                            }
                        }
                        $apply_tmpJobcategory = json_decode($apply_json2, true);
                        $apply_jobCategoryName = "";
                        $apply_jobCategoryId = "";
                        foreach ($apply_tmpJobcategory['Item'] as $apply_jck => $apply_jcv) {
                            if (!empty($apply_jcv["Item"])) {
                                foreach ($apply_jcv['Item'] as $apply_jck2 => $apply_jcv2) {
                                    foreach ($apply_jcv2['Items'] as $apply_jck3 => $apply_jcv3) {
                                        foreach ($apply_jcv3 as $apply_jck4 => $apply_jcv4) {
                                            if ($apply_job["jobcategory_id"] == $apply_jcv4['Option.P_Id']) {
                                                $apply_jobCategoryName = htmlspecialchars($apply_jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                                $apply_jobCategoryId = htmlspecialchars($apply_jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //マイページに反映するチェック
                        $apply_mypageCheck = "";
                        $apply_mypageText = "";
                        if (!empty($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                            foreach ($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $apply_mk => $apply_mv) {
                                foreach($apply_mv as $apply_mk2 => $apply_mv2){
                                    if(!empty($apply_mv2)){
                                        $apply_mypageCheck = $apply_mv2["Option.P_Id"];
                                        $apply_mypageText = $apply_mv2["Option.P_Name"];
                                    }
                                }
                            }
                        }

                        //企業名表示/非表示
                        $apply_clientNameCheck = "";
                        $apply_clientNameDisplay = "";
                        if (!empty($val["Process.U_C10D749E15C59A371D293282096F5B"])) {
                            foreach ($val["Process.U_C10D749E15C59A371D293282096F5B"] as $apply_mk => $apply_mv) {
                                if (!empty($apply_mv)) {
                                    $apply_clientNameCheck = $apply_mv["Option.P_Id"];
                                    $apply_clientNameDisplay = $apply_mv["Option.P_Name"];
                                }
                            }
                        }

                        if($apply_mypageCheck != 11312) {
                            $apply[$apply_i]["processId"] = $val["Process.P_Id"];
                            $apply[$apply_i]["mypageCheck"] = $apply_mypageCheck;
                            $apply[$apply_i]["clientNameDisplay"] = $apply_clientNameDisplay;
                            $apply[$apply_i]["mypageText"] = $apply_mypageText;
                            $apply[$apply_i]["opname"] = $opname;
                            $apply[$apply_i]["phaze"] = $phaze;
                            $apply[$apply_i]["phazeId"] = $opId;
                            $apply[$apply_i]["phazeFlg"] = $phazeFlg;
                            $apply[$apply_i]["job"] = $apply_job;
                            $apply[$apply_i]["client"] = $apply_client;
                            $tmpdate = "";
                            if ($val["Process.P_PhaseDate"]) {
                                $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_PhaseDate"])));
                            }
                            $apply[$apply_i]["phasedate"] = $tmpdate;
                            $apply[$apply_i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_UpdateDate"][0])));
                            $apply[$apply_i]["jobcategoryName"] = $apply_jobCategoryName;
                            $apply[$apply_i]["jobcategoryId"] = $apply_jobCategoryId;
                            $apply_i++;
                        }

                        /* 20180702 「社内NG（社名非公開）」、「応募承諾（書類待ち）」、「期限切れ処理」削除
                    } else if ($opname == "社内NG" || $opname == "社内NG（社名非公開）" || $opname == "JM対応用 社内NG（社名非公開）" || $opname == "書類NG" || $opname == "面接NG" || $opname == "面接辞退" || $opname == "内定辞退" || $opname == "期限切れ処理") {
                        */
                    } else if ($opname == "社内NG" || $opname == "JM対応用 社内NG（社名非公開）" || $opname == "書類NG" || $opname == "面接NG") {
                        $phaze = "見送り";
                        $phazeFlg = 0;
                    } else if ($opname == "面接辞退" || $opname == "内定辞退") {
                        $phaze = "辞退";
                        $phazeFlg = 0;
                        /* 20180702 削除
                    } else if ($opname == "書類OK" || $opname == "面接（一次）" || $opname == "面接（一次）OK" || $opname == "面接（二次）" || $opname == "面接（二次）OK" || $opname == "面接（三次）" || $opname == "面接（三次）OK" || $opname == "面接（最終）") {
                        */
                    } else if ($opname == "書類OK" || $opname == "面接（一次）" || $opname == "面接（二次）" || $opname == "面接（三次）" || $opname == "面接（最終）") {
                        $phaze = "面接";
                        $phazeFlg = 0;

                        //応募件数の設定用
                        //ジョブと企業の設定
                        $apply_jobId = $val["Process.P_Job"]["Job"]["Job.P_Id"];
                        $apply_clientId = $val["Process.P_Client"]["Client"]["Client.P_Id"];
                        $apply_job = $this->db->get_where($this->databaseJob, array('job_id' => $apply_jobId))->row();
                        $apply_client = $this->db->get_where($this->databaseClient, array('c_id' => $apply_clientId))->row();
                        $apply_job = (array)$apply_job;
                        $apply_client = (array)$apply_client;

                        //職種の設定
                        if($apply_json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                            //ここにデータ取得が成功した時の処理
                        }else{
                            //エラー処理
                            if(count($http_response_header) > 0){
                                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                                //エラーの判別
                                switch($status_code[1]){
                                    //404エラーの場合
                                    case 404:
                                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 apply"));
                                        redirect($this->segment."/home/");
                                        break;

                                    //500エラーの場合
                                    case 500:
                                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 apply"));
                                        redirect($this->segment."/home/");
                                        break;

                                    //その他のエラーの場合
                                    default:
                                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other apply"));
                                        redirect($this->segment."/home/");
                                }
                            }else{
                                //タイムアウトの場合 or 存在しないドメインだった場合
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout apply"));
                                redirect($this->segment."/home/");
                            }
                        }
                        $apply_tmpJobcategory = json_decode($apply_json2, true);
                        $apply_jobCategoryName = "";
                        $apply_jobCategoryId = "";
                        foreach ($apply_tmpJobcategory['Item'] as $apply_jck => $apply_jcv) {
                            if (!empty($apply_jcv["Item"])) {
                                foreach ($apply_jcv['Item'] as $apply_jck2 => $apply_jcv2) {
                                    foreach ($apply_jcv2['Items'] as $apply_jck3 => $apply_jcv3) {
                                        foreach ($apply_jcv3 as $apply_jck4 => $apply_jcv4) {
                                            if ($apply_job["jobcategory_id"] == $apply_jcv4['Option.P_Id']) {
                                                $apply_jobCategoryName = htmlspecialchars($apply_jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                                $apply_jobCategoryId = htmlspecialchars($apply_jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //マイページに反映するチェック
                        $apply_mypageCheck = "";
                        $apply_mypageText = "";
                        if (!empty($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                            foreach ($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $apply_mk => $apply_mv) {
                                foreach($apply_mv as $apply_mk2 => $apply_mv2){
                                    if(!empty($apply_mv2)){
                                        $apply_mypageCheck = $apply_mv2["Option.P_Id"];
                                        $apply_mypageText = $apply_mv2["Option.P_Name"];
                                    }
                                }
                            }
                        }

                        //企業名表示/非表示
                        $apply_clientNameCheck = "";
                        $apply_clientNameDisplay = "";
                        if (!empty($val["Process.U_C10D749E15C59A371D293282096F5B"])) {
                            foreach ($val["Process.U_C10D749E15C59A371D293282096F5B"] as $apply_mk => $apply_mv) {
                                if (!empty($apply_mv)) {
                                    $apply_clientNameCheck = $apply_mv["Option.P_Id"];
                                    $apply_clientNameDisplay = $apply_mv["Option.P_Name"];
                                }
                            }
                        }

                        if($apply_mypageCheck != 11312) {
                            $apply[$apply_i]["processId"] = $val["Process.P_Id"];
                            $apply[$apply_i]["mypageCheck"] = $apply_mypageCheck;
                            $apply[$apply_i]["clientNameDisplay"] = $apply_clientNameDisplay;
                            $apply[$apply_i]["mypageText"] = $apply_mypageText;
                            $apply[$apply_i]["opname"] = $opname;
                            $apply[$apply_i]["phaze"] = $phaze;
                            $apply[$apply_i]["phazeId"] = $opId;
                            $apply[$apply_i]["phazeFlg"] = $phazeFlg;
                            $apply[$apply_i]["job"] = $apply_job;
                            $apply[$apply_i]["client"] = $apply_client;
                            $tmpdate = "";
                            if ($val["Process.P_PhaseDate"]) {
                                $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_PhaseDate"])));
                            }
                            $apply[$apply_i]["phasedate"] = $tmpdate;
                            $apply[$apply_i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_UpdateDate"][0])));
                            $apply[$apply_i]["jobcategoryName"] = $apply_jobCategoryName;
                            $apply[$apply_i]["jobcategoryId"] = $apply_jobCategoryId;
                            $apply_i++;
                        }

                    } else if ($opname == "内定" || $opname=="入社") {
                        $phaze = "内定";
                        $phazeFlg = 0;

                        //応募件数の設定用
                        //ジョブと企業の設定
                        $apply_jobId = $val["Process.P_Job"]["Job"]["Job.P_Id"];
                        $apply_clientId = $val["Process.P_Client"]["Client"]["Client.P_Id"];
                        $apply_job = $this->db->get_where($this->databaseJob, array('job_id' => $apply_jobId))->row();
                        $apply_client = $this->db->get_where($this->databaseClient, array('c_id' => $apply_clientId))->row();
                        $apply_job = (array)$apply_job;
                        $apply_client = (array)$apply_client;

                        //職種の設定
                        if($apply_json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                            //ここにデータ取得が成功した時の処理
                        }else{
                            //エラー処理
                            if(count($http_response_header) > 0){
                                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                                //エラーの判別
                                switch($status_code[1]){
                                    //404エラーの場合
                                    case 404:
                                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 apply"));
                                        redirect($this->segment."/home/");
                                        break;

                                    //500エラーの場合
                                    case 500:
                                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 apply"));
                                        redirect($this->segment."/home/");
                                        break;

                                    //その他のエラーの場合
                                    default:
                                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other apply"));
                                        redirect($this->segment."/home/");
                                }
                            }else{
                                //タイムアウトの場合 or 存在しないドメインだった場合
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout apply"));
                                redirect($this->segment."/home/");
                            }
                        }
                        $apply_tmpJobcategory = json_decode($apply_json2, true);
                        $apply_jobCategoryName = "";
                        $apply_jobCategoryId = "";
                        foreach ($apply_tmpJobcategory['Item'] as $apply_jck => $apply_jcv) {
                            if (!empty($apply_jcv["Item"])) {
                                foreach ($apply_jcv['Item'] as $apply_jck2 => $apply_jcv2) {
                                    foreach ($apply_jcv2['Items'] as $apply_jck3 => $apply_jcv3) {
                                        foreach ($apply_jcv3 as $apply_jck4 => $apply_jcv4) {
                                            if ($apply_job["jobcategory_id"] == $apply_jcv4['Option.P_Id']) {
                                                $apply_jobCategoryName = htmlspecialchars($apply_jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                                $apply_jobCategoryId = htmlspecialchars($apply_jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //マイページに反映するチェック
                        $apply_mypageCheck = "";
                        $apply_mypageText = "";
                        if (!empty($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                            foreach ($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $apply_mk => $apply_mv) {
                                foreach($apply_mv as $apply_mk2 => $apply_mv2){
                                    if(!empty($apply_mv2)){
                                        $apply_mypageCheck = $apply_mv2["Option.P_Id"];
                                        $apply_mypageText = $apply_mv2["Option.P_Name"];
                                    }
                                }
                            }
                        }


                        //企業名表示/非表示
                        $apply_clientNameCheck = "";
                        $apply_clientNameDisplay = "";
                        if (!empty($val["Process.U_C10D749E15C59A371D293282096F5B"])) {
                            foreach ($val["Process.U_C10D749E15C59A371D293282096F5B"] as $apply_mk => $apply_mv) {
                                if (!empty($apply_mv)) {
                                    $apply_clientNameCheck = $apply_mv["Option.P_Id"];
                                    $apply_clientNameDisplay = $apply_mv["Option.P_Name"];
                                }
                            }
                        }

                        if($apply_mypageCheck != 11312) {
                            $apply[$apply_i]["processId"] = $val["Process.P_Id"];
                            $apply[$apply_i]["mypageCheck"] = $apply_mypageCheck;
                            $apply[$apply_i]["clientNameDisplay"] = $apply_clientNameDisplay;
                            $apply[$apply_i]["mypageText"] = $apply_mypageText;
                            $apply[$apply_i]["opname"] = $opname;
                            $apply[$apply_i]["phaze"] = $phaze;
                            $apply[$apply_i]["phazeId"] = $opId;
                            $apply[$apply_i]["phazeFlg"] = $phazeFlg;
                            $apply[$apply_i]["job"] = $apply_job;
                            $apply[$apply_i]["client"] = $apply_client;
                            $tmpdate = "";
                            if ($val["Process.P_PhaseDate"]) {
                                $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_PhaseDate"])));
                            }
                            $apply[$apply_i]["phasedate"] = $tmpdate;
                            $apply[$apply_i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_UpdateDate"][0])));
                            $apply[$apply_i]["jobcategoryName"] = $apply_jobCategoryName;
                            $apply[$apply_i]["jobcategoryId"] = $apply_jobCategoryId;
                            $apply_i++;
                        }

                        /* 20180702 削除
                    } else if ($opname == "入社予定" || $opname == "入社") {
                    } else if ($opname == "入社") {
                        $phaze = "ご入社決定";
                        $phazeFlg = 0;
                        */
                        /* 20180702 削除
                    } else if ($opname == "JOBクローズ") {
                        */
                    } else if ($opname == "退職") {
                        //使わないが求人検索で非表示にするため設定
                        $phaze = "退職";
                        $phazeFlg = 0;
                    } else if ($opname == "クローズ") {
                        $phaze = "募集終了";
                        $phazeFlg = 0;
                    } else {
                        $phaze = "";
                    }
                }

                $jobId = $val["Process.P_Job"]["Job"]["Job.P_Id"];
                $clientId = $val["Process.P_Client"]["Client"]["Client.P_Id"];

                if (!empty($phaze)) {
                    //ジョブと企業の設定
                    $job = $this->db->get_where($this->databaseJob, array('job_id' => $jobId))->row();
                    $client = $this->db->get_where($this->databaseClient, array('c_id' => $clientId))->row();
                    $job = (array)$job;
                    $client = (array)$client;

                    //職種の設定
                    if($json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                        //ここにデータ取得が成功した時の処理
                    }else{
                        //エラー処理
                        if(count($http_response_header) > 0){
                            //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                            $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                            //エラーの判別
                            switch($status_code[1]){
                                //404エラーの場合
                                case 404:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 offer"));
                                    redirect($this->segment."/home/");
                                    break;

                                //500エラーの場合
                                case 500:
                                    $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 offer"));
                                    redirect($this->segment."/home/");
                                    break;

                                //その他のエラーの場合
                                default:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other offer"));
                                    redirect($this->segment."/home/");
                            }
                        }else{
                            //タイムアウトの場合 or 存在しないドメインだった場合
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout offer"));
                            redirect($this->segment."/home/");
                        }
                    }
                    $tmpJobcategory = json_decode($json2, true);
                    $jobCategoryName = "";
                    $jobCategoryId = "";
                    foreach ($tmpJobcategory['Item'] as $jck => $jcv) {
                        if (!empty($jcv["Item"])) {
                            foreach ($jcv['Item'] as $jck2 => $jcv2) {
                                foreach ($jcv2['Items'] as $jck3 => $jcv3) {
                                    foreach ($jcv3 as $jck4 => $jcv4) {
                                        if ($job["jobcategory_id"] == $jcv4['Option.P_Id']) {
                                            $jobCategoryName = htmlspecialchars($jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                            $jobCategoryId = htmlspecialchars($jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //マイページに反映するチェック
                    $mypageCheck = "";
                    $mypageText = "";
                    if (!empty($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                        foreach ($val["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $mk => $mv) {
                            foreach($mv as $mk2 => $mv2){
                                if(!empty($mv2)){
                                    $mypageCheck = $mv2["Option.P_Id"];
                                    $mypageText = $mv2["Option.P_Name"];
                                }
                            }
                        }
                    }

                    //企業名表示/非表示
                    $clientNameCheck = "";
                    $clientNameDisplay = "";
                    if (!empty($val["Process.U_C10D749E15C59A371D293282096F5B"])) {
                        foreach ($val["Process.U_C10D749E15C59A371D293282096F5B"] as $mk => $mv) {
                            if (!empty($mv)) {
                                $clientNameCheck = $mv["Option.P_Id"];
                                $clientNameDisplay = $mv["Option.P_Name"];
                            }
                        }
                    }

                    $data["process"][$i]["processId"] = $val["Process.P_Id"];
                    $data["process"][$i]["mypageCheck"] = $mypageCheck;
                    $data["process"][$i]["clientNameDisplay"] = $clientNameDisplay;
                    $data["process"][$i]["mypageText"] = $mypageText;
                    $data["process"][$i]["phaze"] = $phaze;
                    $data["process"][$i]["phazeId"] = $opId;
                    $data["process"][$i]["phazeFlg"] = $phazeFlg;
                    $data["process"][$i]["job"] = $job;
                    $data["process"][$i]["client"] = $client;
                    $tmpdate = "";
                    if ($val["Process.P_PhaseDate"]) {
                        $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_PhaseDate"])));
                    }
                    $data["process"][$i]["phasedate"] = $tmpdate;
                    $data["process"][$i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($val["Process.P_UpdateDate"][0])));
                    $data["process"][$i]["jobcategoryName"] = $jobCategoryName;
                    $data["process"][$i]["jobcategoryId"] = $jobCategoryId;
                    $i++;
                }
            }
            //1件のデータは返却の形式が異なるので個別対応
        } else {
            foreach ($processes["Item"]["Process.P_Phase"] as $k => $v) {
                $opname = $v["Option.P_Name"];
                $opId = $v["Option.P_Id"];
                if ($opname == "求人詳細問合せ") {
                    $phaze = "問合せ中";
                    $phazeFlg = 0;

                    //応募件数の設定用
                    //ジョブと企業の設定
                    $apply_jobId = $processes["Item"]["Process.P_Job"]["Job"]["Job.P_Id"];
                    $apply_clientId = $processes["Item"]["Process.P_Client"]["Client"]["Client.P_Id"];
                    $apply_job = $this->db->get_where($this->databaseJob, array('job_id' => $apply_jobId))->row();
                    $apply_client = $this->db->get_where($this->databaseClient, array('c_id' => $apply_clientId))->row();
                    $apply_job = (array)$apply_job;
                    $apply_client = (array)$apply_client;

                    //職種の設定
                    if($apply_json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                        //ここにデータ取得が成功した時の処理
                    }else{
                        //エラー処理
                        if(count($http_response_header) > 0){
                            //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                            $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                            //エラーの判別
                            switch($status_code[1]){
                                //404エラーの場合
                                case 404:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 apply2"));
                                    redirect($this->segment."/home/");
                                    break;

                                //500エラーの場合
                                case 500:
                                    $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 apply2"));
                                    redirect($this->segment."/home/");
                                    break;

                                //その他のエラーの場合
                                default:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other apply2"));
                                    redirect($this->segment."/home/");
                            }
                        }else{
                            //タイムアウトの場合 or 存在しないドメインだった場合
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout apply2"));
                            redirect($this->segment."/home/");
                        }
                    }
                    $apply_tmpJobcategory = json_decode($apply_json2, true);
                    $apply_jobCategoryName = "";
                    $apply_jobCategoryId = "";
                    foreach ($apply_tmpJobcategory['Item'] as $apply_jck => $apply_jcv) {
                        if (!empty($apply_jcv["Item"])) {
                            foreach ($apply_jcv['Item'] as $apply_jck2 => $apply_jcv2) {
                                foreach ($apply_jcv2['Items'] as $apply_jck3 => $apply_jcv3) {
                                    foreach ($apply_jcv3 as $apply_jck4 => $apply_jcv4) {
                                        if ($apply_job["jobcategory_id"] == $apply_jcv4['Option.P_Id']) {
                                            $apply_jobCategoryName = htmlspecialchars($apply_jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                            $apply_jobCategoryId = htmlspecialchars($apply_jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //マイページに反映するチェック
                    $apply_mypageCheck = "";
                    $apply_mypageText = "";
                    if (!empty($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                        foreach ($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $apply_mk => $apply_mv) {
                            foreach($apply_mv as $apply_mk2 => $apply_mv2){
                                if(!empty($apply_mv2)){
                                    $apply_mypageCheck = $apply_mv2["Option.P_Id"];
                                    $apply_mypageText = $apply_mv2["Option.P_Name"];
                                }
                            }
                        }
                    }

                    //企業名表示/非表示
                    $apply_clientNameCheck = "";
                    $apply_clientNameDisplay = "";
                    if (!empty($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"])) {
                        foreach ($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"] as $apply_mk => $apply_mv) {
                            if (!empty($apply_mv)) {
                                $apply_clientNameCheck = $apply_mv["Option.P_Id"];
                                $apply_clientNameDisplay = $apply_mv["Option.P_Name"];
                            }
                        }
                    }

                    if($apply_mypageCheck != 11312) {
                        $apply[$apply_i]["processId"] = $processes["Item"]["Process.P_Id"];
                        $apply[$apply_i]["mypageCheck"] = $apply_mypageCheck;
                        $apply[$apply_i]["clientNameDisplay"] = $apply_clientNameDisplay;
                        $apply[$apply_i]["mypageText"] = $apply_mypageText;
                        $apply[$apply_i]["phaze"] = $phaze;
                        $apply[$apply_i]["phazeId"] = $opId;
                        $apply[$apply_i]["phazeFlg"] = $phazeFlg;
                        $apply[$apply_i]["job"] = $apply_job;
                        $apply[$apply_i]["client"] = $apply_client;
                        $tmpdate = "";
                        if ($processes["Item"]["Process.P_PhaseDate"]) {
                            $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_PhaseDate"])));
                        }
                        $apply[$apply_i]["phasedate"] = $tmpdate;
                        $apply[$apply_i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_UpdateDate"][0])));
                        $apply[$apply_i]["jobcategoryName"] = $apply_jobCategoryName;
                        $apply[$apply_i]["jobcategoryId"] = $apply_jobCategoryId;
                        $apply_i++;
                    }

                    /* 20180702 「Job打診（社名非公開）」削除
                } else if ($opname == "JOB打診" || $opname == "JOB打診（弱）" || $opname == "JOB打診【強！】" || $opname == "JOB打診（社名非公開）") {
                    */
                } else if ($opname == "JOB打診" || $opname == "JOB打診（弱）" || $opname == "JOB打診【強！】") {
                    $phaze = "JOB打診";
                    $phazeFlg = 1;
                    //応募件数の設定用
                    //ジョブと企業の設定
                    $offer_jobId = $processes["Item"]["Process.P_Job"]["Job"]["Job.P_Id"];
                    $offer_clientId = $processes["Item"]["Process.P_Client"]["Client"]["Client.P_Id"];
                    $offer_job = $this->db->get_where($this->databaseJob, array('job_id' => $offer_jobId))->row();
                    $offer_client = $this->db->get_where($this->databaseClient, array('c_id' => $offer_clientId))->row();
                    $offer_job = (array)$offer_job;
                    $offer_client = (array)$offer_client;

                    //職種の設定
                    if($offer_json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                        //ここにデータ取得が成功した時の処理
                    }else{
                        //エラー処理
                        if(count($http_response_header) > 0){
                            //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                            $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                            //エラーの判別
                            switch($status_code[1]){
                                //404エラーの場合
                                case 404:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 offer2"));
                                    redirect($this->segment."/home/");
                                    break;

                                //500エラーの場合
                                case 500:
                                    $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 offer2"));
                                    redirect($this->segment."/home/");
                                    break;

                                //その他のエラーの場合
                                default:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other offer2"));
                                    redirect($this->segment."/home/");
                            }
                        }else{
                            //タイムアウトの場合 or 存在しないドメインだった場合
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout offer2"));
                            redirect($this->segment."/home/");
                        }
                    }
                    $offer_tmpJobcategory = json_decode($offer_json2, true);
                    $offer_jobCategoryName = "";
                    $offer_jobCategoryId = "";
                    foreach ($offer_tmpJobcategory['Item'] as $offer_jck => $offer_jcv) {
                        if (!empty($offer_jcv["Item"])) {
                            foreach ($offer_jcv['Item'] as $offer_jck2 => $offer_jcv2) {
                                foreach ($offer_jcv2['Items'] as $offer_jck3 => $offer_jcv3) {
                                    foreach ($offer_jcv3 as $offer_jck4 => $offer_jcv4) {
                                        if ($offer_job["jobcategory_id"] == $offer_jcv4['Option.P_Id']) {
                                            $offer_jobCategoryName = htmlspecialchars($offer_jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                            $offer_jobCategoryId = htmlspecialchars($offer_jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //マイページに反映するチェック
                    $offer_mypageCheck = "";
                    $offer_mypageText = "";
                    if (!empty($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                        foreach ($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $offer_mk => $offer_mv) {
                            foreach($offer_mv as $offer_mk2 => $offer_mv2){
                                if(!empty($offer_mv2)){
                                    $offer_mypageCheck = $offer_mv2["Option.P_Id"];
                                    $offer_mypageText = $offer_mv2["Option.P_Name"];
                                }
                            }
                        }
                    }

                    //企業名表示/非表示
                    $offer_clientNameCheck = "";
                    $offer_clientNameDisplay = "";
                    if (!empty($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"])) {
                        foreach ($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"] as $offer_mk => $offer_mv) {
                            if (!empty($offer_mv)) {
                                $offer_clientNameCheck = $offer_mv["Option.P_Id"];
                                $offer_clientNameDisplay = $offer_mv["Option.P_Name"];
                            }
                        }
                    }

                    if($offer_mypageCheck != 11312) {
                        $offer[$offer_i]["processId"] = $processes["Item"]["Process.P_Id"];
                        $offer[$offer_i]["mypageCheck"] = $offer_mypageCheck;
                        $offer[$offer_i]["clientNameDisplay"] = $offer_clientNameDisplay;
                        $offer[$offer_i]["mypageText"] = $offer_mypageText;
                        $offer[$offer_i]["phaze"] = $phaze;
                        $offer[$offer_i]["phazeId"] = $opId;
                        $offer[$offer_i]["phazeFlg"] = $phazeFlg;
                        $offer[$offer_i]["job"] = $offer_job;
                        $offer[$offer_i]["client"] = $offer_client;
                        $tmpdate = "";
                        if ($processes["Item"]["Process.P_PhaseDate"]) {
                            $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_PhaseDate"])));
                        }
                        $offer[$offer_i]["phasedate"] = $tmpdate;
                        $offer[$offer_i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_UpdateDate"][0])));
                        $offer[$offer_i]["jobcategoryName"] = $offer_jobCategoryName;
                        $offer[$offer_i]["jobcategoryId"] = $offer_jobCategoryId;
                        $offer_i++;
                    }
                } else if ($opname == "本人NG") {
                    $phaze = "辞退";
                    $phazeFlg = 0;
                    /* 20180702 削除
                } else if ($opname == "WEBエントリー" || $opname == "応募承諾（書類待ち）" || $opname == "応募承諾" || $opname == "社内NG予定" || $opname == "書類推薦" || $opname == "社内NG予定（社名非公開）") {
                    */
                } else if ($opname == "応募承諾" || $opname == "応募承諾（書類待ち）" || $opname == "書類推薦") {
                    $phaze = "応募中";
                    $phazeFlg = 0;

                    //応募件数の設定用
                    //ジョブと企業の設定
                    $apply_jobId = $processes["Item"]["Process.P_Job"]["Job"]["Job.P_Id"];
                    $apply_clientId = $processes["Item"]["Process.P_Client"]["Client"]["Client.P_Id"];
                    $apply_job = $this->db->get_where($this->databaseJob, array('job_id' => $apply_jobId))->row();
                    $apply_client = $this->db->get_where($this->databaseClient, array('c_id' => $apply_clientId))->row();
                    $apply_job = (array)$apply_job;
                    $apply_client = (array)$apply_client;

                    //職種の設定
                    if($apply_json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                        //ここにデータ取得が成功した時の処理
                    }else{
                        //エラー処理
                        if(count($http_response_header) > 0){
                            //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                            $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                            //エラーの判別
                            switch($status_code[1]){
                                //404エラーの場合
                                case 404:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 apply2"));
                                    redirect($this->segment."/home/");
                                    break;

                                //500エラーの場合
                                case 500:
                                    $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 apply2"));
                                    redirect($this->segment."/home/");
                                    break;

                                //その他のエラーの場合
                                default:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other apply2"));
                                    redirect($this->segment."/home/");
                            }
                        }else{
                            //タイムアウトの場合 or 存在しないドメインだった場合
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout apply2"));
                            redirect($this->segment."/home/");
                        }
                    }
                    $apply_tmpJobcategory = json_decode($apply_json2, true);
                    $apply_jobCategoryName = "";
                    $apply_jobCategoryId = "";
                    foreach ($apply_tmpJobcategory['Item'] as $apply_jck => $apply_jcv) {
                        if (!empty($apply_jcv["Item"])) {
                            foreach ($apply_jcv['Item'] as $apply_jck2 => $apply_jcv2) {
                                foreach ($apply_jcv2['Items'] as $apply_jck3 => $apply_jcv3) {
                                    foreach ($apply_jcv3 as $apply_jck4 => $apply_jcv4) {
                                        if ($apply_job["jobcategory_id"] == $apply_jcv4['Option.P_Id']) {
                                            $apply_jobCategoryName = htmlspecialchars($apply_jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                            $apply_jobCategoryId = htmlspecialchars($apply_jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //マイページに反映するチェック
                    $apply_mypageCheck = "";
                    $apply_mypageText = "";
                    if (!empty($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                        foreach ($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $apply_mk => $apply_mv) {
                            foreach($apply_mv as $apply_mk2 => $apply_mv2){
                                if(!empty($apply_mv2)){
                                    $apply_mypageCheck = $apply_mv2["Option.P_Id"];
                                    $apply_mypageText = $apply_mv2["Option.P_Name"];
                                }
                            }
                        }
                    }

                    //企業名表示/非表示
                    $apply_clientNameCheck = "";
                    $apply_clientNameDisplay = "";
                    if (!empty($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"])) {
                        foreach ($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"] as $apply_mk => $apply_mv) {
                            if (!empty($apply_mv)) {
                                $apply_clientNameCheck = $apply_mv["Option.P_Id"];
                                $apply_clientNameDisplay = $apply_mv["Option.P_Name"];
                            }
                        }
                    }

                    if($apply_mypageCheck != 11312) {
                        $apply[$apply_i]["processId"] = $processes["Item"]["Process.P_Id"];
                        $apply[$apply_i]["mypageCheck"] = $apply_mypageCheck;
                        $apply[$apply_i]["clientNameDisplay"] = $apply_clientNameDisplay;
                        $apply[$apply_i]["mypageText"] = $apply_mypageText;
                        $apply[$apply_i]["phaze"] = $phaze;
                        $apply[$apply_i]["phazeId"] = $opId;
                        $apply[$apply_i]["phazeFlg"] = $phazeFlg;
                        $apply[$apply_i]["job"] = $apply_job;
                        $apply[$apply_i]["client"] = $apply_client;
                        $tmpdate = "";
                        if ($processes["Item"]["Process.P_PhaseDate"]) {
                            $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_PhaseDate"])));
                        }
                        $apply[$apply_i]["phasedate"] = $tmpdate;
                        $apply[$apply_i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_UpdateDate"][0])));
                        $apply[$apply_i]["jobcategoryName"] = $apply_jobCategoryName;
                        $apply[$apply_i]["jobcategoryId"] = $apply_jobCategoryId;
                        $apply_i++;
                    }
                    /* 20180702 削除
                } else if ($opname == "社内NG" || $opname == "社内NG（社名非公開）" || $opname == "JM対応用 社内NG（社名非公開）" || $opname == "書類NG" || $opname == "面接NG" || $opname == "面接辞退" || $opname == "内定辞退" || $opname == "期限切れ処理") {
                    */
                } else if ($opname == "社内NG" || $opname == "JM対応用 社内NG（社名非公開）" || $opname == "書類NG" || $opname == "面接NG") {
                    $phaze = "見送り";
                    $phazeFlg = 0;
                } else if ($opname == "面接辞退" || $opname == "内定辞退") {
                    $phaze = "辞退";
                    $phazeFlg = 0;
                    /* 20180702 削除
                } else if ($opname == "書類OK" || $opname == "面接（一次）" || $opname == "面接（一次）OK" || $opname == "面接（二次）" || $opname == "面接（二次）OK" || $opname == "面接（三次）" || $opname == "面接（三次）OK" || $opname == "面接（最終）") {
                    */
                } else if ($opname == "書類OK" || $opname == "面接（一次）" || $opname == "面接（二次）" || $opname == "面接（三次）" || $opname == "面接（最終）") {
                    $phaze = "面接";
                    $phazeFlg = 0;

                    //応募件数の設定用
                    //ジョブと企業の設定
                    $apply_jobId = $processes["Item"]["Process.P_Job"]["Job"]["Job.P_Id"];
                    $apply_clientId = $processes["Item"]["Process.P_Client"]["Client"]["Client.P_Id"];
                    $apply_job = $this->db->get_where($this->databaseJob, array('job_id' => $apply_jobId))->row();
                    $apply_client = $this->db->get_where($this->databaseClient, array('c_id' => $apply_clientId))->row();
                    $apply_job = (array)$apply_job;
                    $apply_client = (array)$apply_client;

                    //職種の設定
                    if($apply_json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                        //ここにデータ取得が成功した時の処理
                    }else{
                        //エラー処理
                        if(count($http_response_header) > 0){
                            //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                            $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                            //エラーの判別
                            switch($status_code[1]){
                                //404エラーの場合
                                case 404:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 apply2"));
                                    redirect($this->segment."/home/");
                                    break;

                                //500エラーの場合
                                case 500:
                                    $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 apply2"));
                                    redirect($this->segment."/home/");
                                    break;

                                //その他のエラーの場合
                                default:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other apply2"));
                                    redirect($this->segment."/home/");
                            }
                        }else{
                            //タイムアウトの場合 or 存在しないドメインだった場合
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout apply2"));
                            redirect($this->segment."/home/");
                        }
                    }
                    $apply_tmpJobcategory = json_decode($apply_json2, true);
                    $apply_jobCategoryName = "";
                    $apply_jobCategoryId = "";
                    foreach ($apply_tmpJobcategory['Item'] as $apply_jck => $apply_jcv) {
                        if (!empty($apply_jcv["Item"])) {
                            foreach ($apply_jcv['Item'] as $apply_jck2 => $apply_jcv2) {
                                foreach ($apply_jcv2['Items'] as $apply_jck3 => $apply_jcv3) {
                                    foreach ($apply_jcv3 as $apply_jck4 => $apply_jcv4) {
                                        if ($apply_job["jobcategory_id"] == $apply_jcv4['Option.P_Id']) {
                                            $apply_jobCategoryName = htmlspecialchars($apply_jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                            $apply_jobCategoryId = htmlspecialchars($apply_jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //マイページに反映するチェック
                    $apply_mypageCheck = "";
                    $apply_mypageText = "";
                    if (!empty($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                        foreach ($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $apply_mk => $apply_mv) {
                            foreach($apply_mv as $apply_mk2 => $apply_mv2){
                                if(!empty($apply_mv2)){
                                    $apply_mypageCheck = $apply_mv2["Option.P_Id"];
                                    $apply_mypageText = $apply_mv2["Option.P_Name"];
                                }
                            }
                        }
                    }

                    //企業名表示/非表示
                    $apply_clientNameCheck = "";
                    $apply_clientNameDisplay = "";
                    if (!empty($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"])) {
                        foreach ($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"] as $apply_mk => $apply_mv) {
                            if (!empty($apply_mv)) {
                                $apply_clientNameCheck = $apply_mv["Option.P_Id"];
                                $apply_clientNameDisplay = $apply_mv["Option.P_Name"];
                            }
                        }
                    }

                    if($apply_mypageCheck != 11312) {
                        $apply[$apply_i]["processId"] = $processes["Item"]["Process.P_Id"];
                        $apply[$apply_i]["mypageCheck"] = $apply_mypageCheck;
                        $apply[$apply_i]["clientNameDisplay"] = $apply_clientNameDisplay;
                        $apply[$apply_i]["mypageText"] = $apply_mypageText;
                        $apply[$apply_i]["phaze"] = $phaze;
                        $apply[$apply_i]["phazeId"] = $opId;
                        $apply[$apply_i]["phazeFlg"] = $phazeFlg;
                        $apply[$apply_i]["job"] = $apply_job;
                        $apply[$apply_i]["client"] = $apply_client;
                        $tmpdate = "";
                        if ($processes["Item"]["Process.P_PhaseDate"]) {
                            $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_PhaseDate"])));
                        }
                        $apply[$apply_i]["phasedate"] = $tmpdate;
                        $apply[$apply_i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_UpdateDate"][0])));
                        $apply[$apply_i]["jobcategoryName"] = $apply_jobCategoryName;
                        $apply[$apply_i]["jobcategoryId"] = $apply_jobCategoryId;
                        $apply_i++;
                    }
                } else if ($opname == "内定" || $opname=="入社") {
                    $phaze = "内定";
                    $phazeFlg = 0;

                    //応募件数の設定用
                    //ジョブと企業の設定
                    $apply_jobId = $processes["Item"]["Process.P_Job"]["Job"]["Job.P_Id"];
                    $apply_clientId = $processes["Item"]["Process.P_Client"]["Client"]["Client.P_Id"];
                    $apply_job = $this->db->get_where($this->databaseJob, array('job_id' => $apply_jobId))->row();
                    $apply_client = $this->db->get_where($this->databaseClient, array('c_id' => $apply_clientId))->row();
                    $apply_job = (array)$apply_job;
                    $apply_client = (array)$apply_client;

                    //職種の設定
                    if($apply_json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                        //ここにデータ取得が成功した時の処理
                    }else{
                        //エラー処理
                        if(count($http_response_header) > 0){
                            //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                            $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                            //エラーの判別
                            switch($status_code[1]){
                                //404エラーの場合
                                case 404:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 apply2"));
                                    redirect($this->segment."/home/");
                                    break;

                                //500エラーの場合
                                case 500:
                                    $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 apply2"));
                                    redirect($this->segment."/home/");
                                    break;

                                //その他のエラーの場合
                                default:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other apply2"));
                                    redirect($this->segment."/home/");
                            }
                        }else{
                            //タイムアウトの場合 or 存在しないドメインだった場合
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout apply2"));
                            redirect($this->segment."/home/");
                        }
                    }
                    $apply_tmpJobcategory = json_decode($apply_json2, true);
                    $apply_jobCategoryName = "";
                    $apply_jobCategoryId = "";
                    foreach ($apply_tmpJobcategory['Item'] as $apply_jck => $apply_jcv) {
                        if (!empty($apply_jcv["Item"])) {
                            foreach ($apply_jcv['Item'] as $apply_jck2 => $apply_jcv2) {
                                foreach ($apply_jcv2['Items'] as $apply_jck3 => $apply_jcv3) {
                                    foreach ($apply_jcv3 as $apply_jck4 => $apply_jcv4) {
                                        if ($apply_job["jobcategory_id"] == $apply_jcv4['Option.P_Id']) {
                                            $apply_jobCategoryName = htmlspecialchars($apply_jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                            $apply_jobCategoryId = htmlspecialchars($apply_jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //マイページに反映するチェック
                    $apply_mypageCheck = "";
                    $apply_mypageText = "";
                    if (!empty($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                        foreach ($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $apply_mk => $apply_mv) {
                            foreach($apply_mv as $apply_mk2 => $apply_mv2){
                                if(!empty($apply_mv2)){
                                    $apply_mypageCheck = $apply_mv2["Option.P_Id"];
                                    $apply_mypageText = $apply_mv2["Option.P_Name"];
                                }
                            }
                        }
                    }

                    //企業名表示/非表示
                    $apply_clientNameCheck = "";
                    $apply_clientNameDisplay = "";
                    if (!empty($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"])) {
                        foreach ($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"] as $apply_mk => $apply_mv) {
                            if (!empty($apply_mv)) {
                                $apply_clientNameCheck = $apply_mv["Option.P_Id"];
                                $apply_clientNameDisplay = $apply_mv["Option.P_Name"];
                            }
                        }
                    }

                    if($apply_mypageCheck != 11312) {
                        $apply[$apply_i]["processId"] = $processes["Item"]["Process.P_Id"];
                        $apply[$apply_i]["mypageCheck"] = $apply_mypageCheck;
                        $apply[$apply_i]["clientNameDisplay"] = $apply_clientNameDisplay;
                        $apply[$apply_i]["mypageText"] = $apply_mypageText;
                        $apply[$apply_i]["phaze"] = $phaze;
                        $apply[$apply_i]["phazeId"] = $opId;
                        $apply[$apply_i]["phazeFlg"] = $phazeFlg;
                        $apply[$apply_i]["job"] = $apply_job;
                        $apply[$apply_i]["client"] = $apply_client;
                        $tmpdate = "";
                        if ($processes["Item"]["Process.P_PhaseDate"]) {
                            $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_PhaseDate"])));
                        }
                        $apply[$apply_i]["phasedate"] = $tmpdate;
                        $apply[$apply_i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_UpdateDate"][0])));
                        $apply[$apply_i]["jobcategoryName"] = $apply_jobCategoryName;
                        $apply[$apply_i]["jobcategoryId"] = $apply_jobCategoryId;
                        $apply_i++;
                    }

                    /* 20180702 削除
                } else if ($opname == "入社予定" || $opname == "入社") {
                } else if ($opname == "入社") {
                    $phaze = "ご入社決定";
                    $phazeFlg = 0;
                    */
                    /* 20180702 削除
                } else if ($opname == "JOBクローズ") {
                    */
                } else if ($opname == "退職") {
                    //使わないが求人検索で非表示にするため設定
                    $phaze = "退職";
                    $phazeFlg = 0;
                } else if ($opname == "クローズ") {
                    $phaze = "募集終了";
                    $phazeFlg = 0;
                } else {
                    $phaze = "";
                }
            }

            $jobId = $processes["Item"]["Process.P_Job"]["Job"]["Job.P_Id"];
            $clientId = $processes["Item"]["Process.P_Client"]["Client"]["Client.P_Id"];
            if (!empty($phaze)) {

                //ジョブと企業の設定
                $job = $this->db->get_where($this->databaseJob, array('job_id' => $jobId))->row();
                $client = $this->db->get_where($this->databaseClient, array('c_id' => $clientId))->row();
                $job = (array)$job;
                $client = (array)$client;

                //職種の設定
                if($json2 = @file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true)){
                    //ここにデータ取得が成功した時の処理
                }else{
                    //エラー処理
                    if(count($http_response_header) > 0){
                        //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                        $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                        //エラーの判別
                        switch($status_code[1]){
                            //404エラーの場合
                            case 404:
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 json2"));
                                redirect($this->segment."/home/");
                                break;

                            //500エラーの場合
                            case 500:
                                $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 json2"));
                                redirect($this->segment."/home/");
                                break;

                            //その他のエラーの場合
                            default:
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other json2"));
                                redirect($this->segment."/home/");
                        }
                    }else{
                        //タイムアウトの場合 or 存在しないドメインだった場合
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout json2"));
                        redirect($this->segment."/home/");
                    }
                }
                $tmpJobcategory = json_decode($json2, true);
                $jobCategoryName = "";
                $jobCategoryId = "";
                if (!empty($tmpJobcategory["Item"])) {
                    foreach ($tmpJobcategory['Item'] as $jck => $jcv) {
                        if (!empty($jcv["Item"])) {
                            foreach ($jcv['Item'] as $jck2 => $jcv2) {
                                foreach ($jcv2['Items'] as $jck3 => $jcv3) {
                                    foreach ($jcv3 as $jck4 => $jcv4) {
                                        if ($job["jobcategory_id"] == $jcv4['Option.P_Id']) {
                                            $jobCategoryName = htmlspecialchars($jcv4['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                            $jobCategoryId = htmlspecialchars($jcv4['Option.P_Id'], ENT_QUOTES, 'UTF-8') . ",";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //マイページに反映するチェック
                $mypageCheck = "";
                $mypageText = "";
                if (!empty($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"])) {
                    foreach ($processes["Item"]["Process.U_D14FCDFBDA7DC04E89BCB5FA31CE11"] as $mk => $mv) {
                        if (!empty($mv)) {
                            foreach ($mv as $mk2 => $mv2) {
                                $mypageCheck = $mv2["Option.P_Id"];
                                $mypageText = $mv2["Option.P_Name"];
                            }
                        }
                    }
                }

                //企業名表示/非表示
                $clientNameCheck = "";
                $clientName = "";
                $clientNameDisplay="";
                if (!empty($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"])) {
                    foreach ($processes["Item"]["Process.U_C10D749E15C59A371D293282096F5B"] as $mk => $mv) {
                        if (!empty($mv)) {
                            $clientNameCheck = $mv["Option.P_Id"];
                            $clientNameDisplay = $mv["Option.P_Name"];
                        }
                    }
                }

                $data["process"][$i]["processId"] = $processes["Item"]["Process.P_Id"];
                $data["process"][$i]["mypageCheck"] = $mypageCheck;
                $data["process"][$i]["clientNameDisplay"] = $clientNameDisplay;
                $data["process"][$i]["mypageText"] = $mypageText;
                $data["process"][$i]["phaze"] = $phaze;
                $data["process"][$i]["phazeId"] = $opId;
                $data["process"][$i]["phazeFlg"] = $phazeFlg;
                $data["process"][$i]["job"] = $job;
                $data["process"][$i]["client"] = $client;
                $tmpdate="";
                if(!empty($processes["Item"]["Process.P_PhaseDate"])){
                    $tmpdate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_PhaseDate"])));
                }
                $data["process"][$i]["phasedate"] = $tmpdate;
                $data["process"][$i]["updated"] = date("Y/m/d H:i:s", strtotime("+9 hour", strtotime($processes["Item"]["Process.P_UpdateDate"][0])));
                $data["process"][$i]["jobcategoryName"] = $jobCategoryName;
                $data["process"][$i]["jobcategoryId"] = $jobCategoryId;
                $i++;
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////
        // 希望条件抽出
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //希望条件保存の検索
        $kibou_job="";
        if(!empty($ret->kibou_job)){
            $kibou_job = explode(",",$ret->kibou_job);
        }


        $kibou_pref=array();
        $kibou_pref1="";
        if(!empty($ret->kibou_area1)){
            $kibou_pref1 = explode(",",$ret->kibou_area1);
            $kibou_pref[] = $ret->kibou_area1;
        }
        $kibou_pref2="";
        if(!empty($ret->kibou_area2)){
            $kibou_pref2 = explode(",",$ret->kibou_area2);
            $kibou_pref[] = $ret->kibou_area2;
        }
        $kibou_pref3="";
        if(!empty($ret->kibou_area3)){
            $kibou_pref3 = explode(",",$ret->kibou_area3);
            $kibou_pref[] = $ret->kibou_area3;
        }

        $kibou_year_income="";
        if(!empty($ret->income)){
            $kibou_year_income = $ret->income;
        }
        $kibou_tenkin="";
        $kibou_tenkin_flg="";

        if(!empty($ret->tenkin)) {
            $kibou_tenkin = explode(",", $ret->tenkin);
        }
        if($kibou_tenkin){
            if(in_array('不可', $kibou_tenkin)){
                $kibou_tenkin_flg = $this->tenkin_flg;
            }else{
                $kibou_tenkin_flg = "";
            }
        }

        //フェーズオープンで自社サイトへの公開が非公開のデータ抽出（ＪＯＢ打診のものだけＪＯＢに表示）
        $p_job_id = array();
        $taishoku="";

        if(!empty($data["process"])){
            foreach ($data["process"] as $pk => $pv){
                if($pv["phaze"] == "JOB打診" || $pv["phaze"] == "JOB打診（弱）" || $pv["phaze"] == "JOB打診【強！】" || $pv["phaze"] == "JOB打診（社名非公開）"){
                    $p_job_id[] = $pv["job"]["job_id"];
                }

                //退職は非表示
                if ($pv["phaze"] == "退職") {
                    $taishoku .= $pv["job"]["job_id"] . ",";
                }
            }
        }
        $taishoku = rtrim($taishoku,",");

        if($kibou_job || $kibou_pref || $kibou_tenkin_flg || $kibou_year_income) {
            $kibou_jobs = $this->jobSearch($kibou_job, $kibou_pref, $kibou_tenkin_flg, $kibou_year_income, $p_job_id, $taishoku, 10);
            $data["kibou_job"] = $kibou_jobs->result();
        }else if(!empty($kibou_tenkin) && $kibou_tenkin_flg != $this->tenkin_flg){
            $kibou_jobs = $this->jobSearch($kibou_job, $kibou_pref, $kibou_tenkin_flg, $kibou_year_income, $p_job_id, $taishoku, 10);
            $data["kibou_job"] = $kibou_jobs->result();
        }else{
            $data["kibou_job"] = array();
        }

        //スマホで透過させる処理用に２つに分ける
        $data["kibou_job1"]=array();
        $data["kibou_job2"]=array();
        if(!empty($data["kibou_job"])){
            foreach($data["kibou_job"] as $k => $v){
                if($k>3) break;
                $data["kibou_job1"][] = $v;
            }
            foreach($data["kibou_job"] as $k => $v){
                $data["kibou_job2"][] = $v;
            }
        }
        $data["count"] = 0;
        $data["count"] = count($data["kibou_job2"]);

        //////////////////////////////////////////////////////////////////////////////////////////////////////
        // 希望条件抽出ここまで
        //////////////////////////////////////////////////////////////////////////////////////////////////////


        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $this->load->library("pagination");

        //PCとスマホの判定
        $this->load->library('user_agent');
        $agent="";
        if ($this->agent->is_mobile()) {
            $agent = "SP";
        }else{
            $agent = "PC";
        }

        $job_param = "";
        if(!empty($p_job_id)){
            foreach ($p_job_id as $k=>$v){
                $job_param.=$v . ",";
            }
            $job_param = rtrim($job_param,",");
        }


        ////////////////////////////////////////////////////////////////////
        //// 閲覧履歴
        //////////////////////////////////////////////////////////////////////
        $looks = $this->getLooks($login_id);
        $data["looks"] = $looks;

        //オファーのソート（募集終了を最後に持ってくる）
        $end_offer=array();
        $active_offer=array();
        $new_offer=array();
        if(!empty($offer)){
            foreach ($offer as $k => $v){
                if($v["job"]["phaze"]==HrbcPhaze){
                    $active_offer[] = $v;
                }else{
                    $end_offer[] = $v;
                }
            }
        }
        $new_offer = $active_offer;
        $new_offer = array_merge($new_offer, $end_offer);

        //応募状況のソート（募集終了を最後に持ってくる）
        $end_apply=array();
        $active_apply=array();
        $new_apply=array();
        if(!empty($apply)){
            foreach ($apply as $k => $v){
                if($v["job"]["phaze"]==HrbcPhaze){
                    $active_apply[] = $v;
                }else{
                    $end_apply[] = $v;
                }
            }
        }
        $new_apply = $active_apply;
        $new_apply = array_merge($new_apply, $end_apply);

        //data設定
        $data["phaze"] = HrbcPhaze;
        $data["render"] = $this->segment."/home";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["login_id"] = $login_id;
        $data["offer"] = $new_offer;
        $data["apply"] = $new_apply;
        $data["process_job_id"] = $job_param;
        $data["agent"] = $agent;
        $data["segment"] = $this->segment;
        $data["body_id"] = "top";

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //ログイン履歴の更新
    //////////////////////////////////////////////////////////
    function lastLogin()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        ////////////////////////////////////////////////////////////////////////////////////////////
        //HRBCデータ取得
        ////////////////////////////////////////////////////////////////////////////////////////////
        $result = $this->getHrbcMaster();

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        ////////////////////////////////////////////////////////////////////////////////////////////
        //Resume のログイン履歴登録
        ////////////////////////////////////////////////////////////////////////////////////////////
        /*
        $param = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Id:eq='.$resume_id, 'field'=>'Resume.P_Owner,Resume.U_CFD727A34F74C45C5786B706A1F90F', 'order'=>'Resume.P_Id:desc'));
        $url =$this->hrbcUrl .  "resume?" . $param;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: ". $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 lastLogin"));
                        $subject = "【lastログイン履歴登録失敗1　code:userlast】 ";
                        $message = "LastLogin失敗 404";
                        $this->errMail($subject, $message);

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 lastLogin"));
                        $subject = "【lastログイン履歴登録失敗1　code:userlast】 ";
                        $message = "LastLogin失敗 500";
                        $this->errMail($subject, $message);

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other lastLogin"));
                        $subject = "【lastログイン履歴登録失敗1　code:userlast】 ";
                        $message = "LastLogin失敗" . $status_code[1];
                        $this->errMail($subject, $message);
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout lastLogin"));
                $subject = "【lastログイン履歴登録失敗1　code:userlast】 ";
                $message = "LastLogin失敗 timeout";
                $this->errMail($subject, $message);
            }
        }
        $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "resume?partition=". $result["partition"] . "&count=1&start=0&field=Resume.P_Owner,Resume.U_CFD727A34F74C45C5786B706A1F90F&condition=Resume.P_Id:eq=".$resume_id."&order=Resume.P_Id:desc";
        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

        if($xml->Code!=0){
            $pErr=$xml->Code;
            //エラーメール送信
            $subject = "【resumeログイン履歴登録失敗　code:user01】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
            $message = "ログイン履歴登録に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404/");
            //exit;

        }else{
            //HRBCから帰ってきたデータ件数　1件の対応用に取得
            $cnt = $xml->attributes()->Count;

            //データ設定
            $json = json_encode($xml);
            $data["resume"] = json_decode($json,true);

            $now = date('Y/m/d H:i:s');
            $now_hrbc = date("Y/m/d H:i:s",strtotime($now. "-9 hour"));
            $login_log = $data["resume"]["Item"]["Resume.U_CFD727A34F74C45C5786B706A1F90F"];
            $first_login_flg = 0;
            if(empty($login_log)){
                $login_log="";
            }else{
                $first_login_flg = 1;
            }
            $login_log = $now . "&#x0A;" .  $login_log;
            $owner_id = $data["resume"]["Item"]["Resume.P_Owner"]["User"]["User.P_Id"];

            ///////////////////////////////////////////////////////////
            // resume に登録
            ///////////////////////////////////////////////////////////
            $url = $this->hrbcUrl . "resume?partition=". $result["partition"];

            //※レジュメにメールとTELを入れるとエラーになる
            //1回でもログインした場合
            if($first_login_flg==1){
                $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>' . $owner_id . '</Resume.P_Owner><Resume.P_Candidate>'.$person_id.'</Resume.P_Candidate><Resume.U_CFD727A34F74C45C5786B706A1F90F>' . $login_log . '</Resume.U_CFD727A34F74C45C5786B706A1F90F></Item></Resume>';
            }else{
                $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>' . $owner_id . '</Resume.P_Owner><Resume.P_Candidate>'.$person_id.'</Resume.P_Candidate><Resume.U_CFD727A34F74C45C5786B706A1F90F>' . $login_log . '</Resume.U_CFD727A34F74C45C5786B706A1F90F></Item></Resume>';
            }
            $tmp_xml = $xml;

/*
            $header = array(
                "Content-Type: application/xml; charset=UTF-8",
                "X-porters-hrbc-oauth-token: " . $result['token']
            );

            $options = array('http' => array(
                'method' => 'POST',
                'content' => $xml,
                'header' => implode("\r\n", $header)
            ));
            if($xml = @file_get_contents($url, false, stream_context_create($options))){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 lastLogin2"));
                            $subject = "【lastログイン履歴登録失敗2 404　code:userlast】 ";
                            $message = "LastLogin失敗" . $tmp_xml;
                            $this->errMail($subject, $message);


                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 lastLogin2"));
                            $subject = "【lastログイン履歴登録失敗2 500　code:userlast】 ";
                            $message = "LastLogin失敗" . $tmp_xml;
                            $this->errMail($subject, $message);


                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other lastLogin2"));
                            $subject = "【lastログイン履歴登録失敗2 " . $status_code[1] . "　code:userlast】 ";
                            $message = "LastLogin失敗" . $tmp_xml;
                            $this->errMail($subject, $message);

                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout lastLogin2"));
                    $subject = "【lastログイン履歴登録失敗2 timeout　code:userlast】 ";
                    $message = "LastLogin失敗" . $tmp_xml;
                    $this->errMail($subject, $message);

                }
            }
*/
            $options = array
            (
                CURLOPT_URL            => $url,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $xml,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result['token']),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

            //xml解析
            $rErr="";
            $rId = "";

            $this->writeRegistLog($tmp_xml, $xml, "user02");

            $error="";
            if($xml->Code!=0){
                $subject = "【resumeログイン履歴登録失敗　code:user02】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                $message = "ログイン履歴登録に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                $this->errMail($subject, $message);
                $rErr=$xml->Code;
            }else{
                $rCode="";
                $json = json_encode($xml);
                $arr = json_decode($json,true);
                foreach($arr['Item'] as $k => $v){
                    if($k=="Id"){
                        $rId = $v;
                    }
                    if($k=="Code"){
                        $rErr = $v;
                    }
                }
            }
            if(!$rId || $rErr){
                $subject = "【resumeログイン履歴登録失敗　code:user03】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                $message = "ログイン履歴登録に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id . " error:" . $rErr;
                $this->errMail($subject, $message);
                $error.= "エラーが発生しました。(Resume: ".$rErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
            }

        }
    }

    /////////////////////////////////////////////////////////////////////////
    // job検索 topのajax用（地域、職種、転勤のリスト）
    /////////////////////////////////////////////////////////////////////////
    function getJob()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $job=array();
        $pref=array();
        $tenkin=array();
        $income="";
        $p_job_id = "";

        if(!empty($_POST["job"])){
            $job[] = $this->input->post("job");
        }
        if(!empty($_POST["pref"])){
            $pref[] = $this->input->post("pref");
        }
        if(!empty($_POST["tenkin"])){
            $tenkin = $this->input->post("tenkin");
        }
        if(!empty($_POST["p_job_id"])){
            $p_job_id = $this->input->post("p_job_id");
        }

        ////////////////////////////////////////////////////////////
        // パラメータ設定
        //////////////////////////////////////////////////////////////
        //業種
        $bizSql = " left join clients as c on j.client_id = c.c_id ";
        $bizSql2 = ", group_concat(distinct c.c_industry_name separator '|') as i_name, c.c_title, c.c_id";

        //職種
        $jobcnt=0;
        $jobSearch="";
        $jcParam="";
        $jccnt=0;
        $jobs=array();
        $tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
        $tmpjob = json_decode($tmpjob,true);
        if(!empty($job)){
            if(!empty($tmpjob['Item'])){
                foreach($tmpjob['Item'] as $k=>$v){
                    if(!empty($v['Item'])){
                        foreach($v['Item'] as $k2=>$v2){
                            foreach($job as $k3 => $v3){
                                if($v2["Option.P_Id"]==$v3){
                                    if(!empty($v2["Items"]["Item"])){
                                        foreach($v2["Items"]["Item"] as $k4 => $v4){
                                            $jobs[] = $v4["Option.P_Id"];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if(!empty($jobs)){
                foreach($jobs as $k=>$v){
                    if(empty($jobSearch)) {
                        $jobSearch.=" and ( ";
                    }else{
                        $jobSearch.=" or ";
                    }
                    $val = $this->db->escape($v);
                    $jobSearch .= " FIND_IN_SET(" . $val . ",j.jobcategory_id)";
                }
            }
        }
        if($jobSearch) $jobSearch .= " ) ";

        //勤務地
        $prefSearch="";
        if(!empty($pref)){
            foreach($pref as $k=>$v){
                if(empty($prefSearch)) {
                    $prefSearch.=" and ( ";
                }else{
                    $prefSearch.=" or ";
                }
                $val = $this->db->escape($v);
                $prefSearch .= " FIND_IN_SET(" . $val . ",j.prefecture_id)";
            }
        }
        if($prefSearch) $prefSearch .= " ) ";

        //転勤
        if($tenkin){
            if($tenkin == '不可'){
                $tenkin = $this->tenkin_flg;
            }else{
                $tenkin = "";
            }
        }
        $tenkinSearch="";
        if(!empty($tenkin)){
            $val = $this->db->escape($tenkin);
            $tenkinSearch = " and j.tenkin_id = " . $val . " ";
        }

        //給与 使っていない
        $incomeSearch="";
        if(!empty($income)){
            $val = $this->db->escape($income);
            $incomeSearch = " and j.minsalary >= " . $val . " ";
        }

        $process_param="";
        if($p_job_id){
            $process_param = " and (j.publish = " .HrbcPublish. " or (j.publish = ". HrbcCloseJob . " and j.job_id in (" . $p_job_id . "))) ";
        }else{
            $process_param = " and j.publish = " .HrbcPublish;
        }

        ////////////////////////////////////////////////////////////////////
        //// db抽出
        //////////////////////////////////////////////////////////////////////
        $datas = array();
        $sql = "SELECT j.job_id, j.job_title, j.client_id, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.j_updated, j.prefecture_name, j.jobcategory_name, j.employ_name ,j.tenkin_name" . $bizSql2 . " FROM jobs as j" . $bizSql . " where j.phaze = " . HrbcPhaze . $process_param . $jobSearch . $prefSearch . $tenkinSearch . $incomeSearch . " group by j.job_id";
        $count = $this->db->query($sql);
        $tmp = $count->result();

        $datas = array();
        $sql = "SELECT j.job_id, j.job_title, j.client_id, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.j_updated, j.prefecture_name, j.jobcategory_name, j.employ_name ,j.tenkin_name" . $bizSql2 . " FROM jobs as j" . $bizSql . " where j.phaze = " . HrbcPhaze . $process_param . $jobSearch . $prefSearch . $tenkinSearch . $incomeSearch . " group by j.job_id order by j.j_updated desc limit 0, 8";
        $datas = $this->db->query($sql);
        $data["jobs"] = $datas->result();

        $today=date("Y/m/d");
        $contents="";
        if(!empty($data["jobs"])) {
            $contents .= "<div id=\"selectCount\">";
            $contents .= "<p><strong>" . count($tmp) . "</strong>件ヒットしました</p></div>";
            foreach ($data["jobs"] as $k => $v) {
                $date_diff = (strtotime($today) - strtotime(substr($v->j_updated, 0, 10))) / (60 * 60 * 24) + 1;
                $contents .= "<div class=\"entry jobs\">";
                $contents .= "<a href=\"" . base_url() . "search/detail/" . htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8') . "\" class=\"clearfix\">";
                $contents .= "<p class=\"date\">" . htmlspecialchars(date('Y.m.d', strtotime($v->j_updated)), ENT_QUOTES, 'UTF-8') . " 更新</p>";
                $contents .= "<p class=\"detail\">" . htmlspecialchars($v->job_title, ENT_QUOTES, 'UTF-8');
                if ($date_diff <= 8) {
                    //$contents .= "<span class=\"new\">NEW!!</span>";
                }
                $contents .= "</p>\n</a>\n</div>";
            }
        }else{
            $contents.="<div class=\"entry\"><p class=\"jobs\">現在、求人情報はございません。</p></div>";
        }
        print_r($contents);
    }

    //////////////////////////////////////////////////////////
    //登録確認
    //////////////////////////////////////////////////////////
    function profile()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $posts = $this->input->post();
        $data= array();

        $tmp_data[$this->segment] = $this->db->get_where($this->database, array('id' => $login_id, 'del' => 0))->row();

        //HRBC
        $result = $this->getHrbcMaster();

        ///////////////////////////////////////////////////
        //person取得
        ///////////////////////////////////////////////////
        /*
        $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Id:eq=' . $person_id));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/candidate?" . $url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 profile"));
                        redirect($this->segment."/profile/");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 profile"));
                        redirect($this->segment."/profile/");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other profile"));
                        redirect($this->segment."/profile/");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout profile"));
                redirect($this->segment."/profile/");
            }
        }
        $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "candidate?partition=". $result["partition"] . "&count=1&start=0&condition=Person.P_Id:eq=".$person_id;
        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

        if($xml->Code!=0){
            $pErr=$xml->Code;

            $subject = "【user基本情報編集に失敗　code:user01】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
            $message = "user基本情報編集に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id . " error:" . $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404/");
            //exit;

        }

        //データ設定
        $json = json_encode($xml);
        $person = json_decode($json,true);

        ////////////////////////////////////////////////////////////////
        //candidate Idからresume取得
        ///////////////////////////////////////////////////////////////
        /*
        $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Id:eq=' . $resume_id));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/resume?" . $url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 profile2"));
                        redirect($this->segment."/profile/");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 profile2"));
                        redirect($this->segment."/profile/");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other profile2"));
                        redirect($this->segment."/profile/");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout profile2"));
                redirect($this->segment."/profile/");
            }
        }
        $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "resume?partition=". $result["partition"] . "&count=1&start=0&condition=Resume.P_Id:eq=".$resume_id;

        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

        if($xml->Code!=0){
            $pErr=$xml->Code;

            $subject = "【user基本情報編集に失敗　code:user02】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
            $message = "user基本情報編集に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id . " error:" . $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404/");
            //exit;
        }

        //データ設定
        $json = json_encode($xml);
        $resume = json_decode($json,true);

        //データの整形
        $data[$this->segment] = new stdClass;
        $data = $this->getBasicInfo($person, $resume);

        $error = "";

        ////////////////////////////////////////////////////////////////////
        //// 閲覧履歴
        //////////////////////////////////////////////////////////////////////
        $looks = $this->getLooks($login_id);
        $data["looks"] = $looks;

        //data設定
        $data["render"] = $this->segment."/profile";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["segment"] = $this->segment;
        $data["body_id"] = "profile";

        $this->load->view("template_search", $data);
    }


    //////////////////////////////////////////////////////////
    //新規登録
    //////////////////////////////////////////////////////////
    function regist()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');
        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $posts = $this->input->post();
        $data= array();

        //postなしの場合はDBから取得
        //if(empty($posts)){
            $tmp_data[$this->segment] = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();

            //HRBC
            $result = $this->getHrbcMaster();

            ///////////////////////////////////////////////////
            //person取得
            ///////////////////////////////////////////////////
        /*
            $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Id:eq=' . $person_id));
            $url = "https://api-hrbc-jp.porterscloud.com/v1/candidate?" . $url;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);

            if($xml = @file_get_contents($url, false, $context)){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist"));
                            redirect($this->segment."/regist/");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist"));
                            redirect($this->segment."/regist/");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist"));
                            redirect($this->segment."/regist/");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist"));
                    redirect($this->segment."/regist/");
                }
            }
            $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "candidate?partition=". $result["partition"] . "&count=1&start=0&condition=Person.P_Id:eq=".$person_id;

        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

            if($xml->Code!=0){
                $pErr=$xml->Code;

                $subject = "【user基本情報編集に失敗　code:user01】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                $message = "user基本情報編集に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id . " error:" . $pErr;
                $this->errMail($subject, $message);

                $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
                redirect("err/error_404/");
                //exit;
            }

            //データ設定
            $json = json_encode($xml);
            $person = json_decode($json,true);

            ////////////////////////////////////////////////////////////////
            //candidate Idからresume取得
            ///////////////////////////////////////////////////////////////
        /*
            $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Id:eq=' . $resume_id));
            $url = "https://api-hrbc-jp.porterscloud.com/v1/resume?" . $url;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);

            if($xml = @file_get_contents($url, false, $context)){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist2"));
                            redirect($this->segment."/regist");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist2"));
                            redirect($this->segment."/regist/");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist2"));
                            redirect($this->segment."/regist/");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist2"));
                    redirect($this->segment."/regist/");
                }
            }
            $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "resume?partition=". $result["partition"] . "&count=1&start=0&condition=Resume.P_Id:eq=".$resume_id;

        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

            if($xml->Code!=0){
                $pErr=$xml->Code;

                $subject = "【user基本情報編集に失敗　code:user02】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
                $message = "user基本情報編集に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id . " error:" . $pErr;
                $this->errMail($subject, $message);

                $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
                redirect("err/error_404/");
                //exit;
            }

            //データ設定
            $json = json_encode($xml);
            $resume = json_decode($json,true);

            //データの整形
            $data[$this->segment] = new stdClass;
            $data = $this->getBasicInfo($person, $resume);
            //検索と名前がバッティングするので退避
            $data["user"]->kpref = $data["user"]->pref;

            /*
        }else{
            ///////////////////////////////////////////////////////
            //postありは戻るで編集
            ///////////////////////////////////////////////////////

            $posts["password"] = $this->session->userdata("pass");

            $data[$this->segment] = (object) $posts;

            //idがない場合は空データ設定
            if(empty($data[$this->segment]->id)){
                $data[$this->segment]->id = "";
            }
        }
            */

        $error = "";

        ////////////////////////////////////////////////////////////////////
        //// 閲覧履歴
        //////////////////////////////////////////////////////////////////////
        $looks = $this->getLooks($login_id);
        $data["looks"] = $looks;

        //data設定
        $data["render"] = $this->segment."/regist";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["segment"] = $this->segment;
        $data["body_id"] = "registration";

        $this->load->view("template_search", $data);
    }

    //////////////////////////////////////////////////////////
    //登録情報確認
    //////////////////////////////////////////////////////////
    function confirm()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $posts = $this->input->post();


        $this->session->unset_userdata('pass');
        $this->session->set_userdata("pass", $posts["password"]);

        //バリデーション実行
        $error = "";
        $tmp_error = 0;
        $e_msg = "";
        $error = $this->setValidation();

        if(!checkdate($posts["month"], $posts["day"], $posts["year"])) {
            $error.="<p>日付の形式が正しくありません。</p>";
        }

        //パスワードチェック
        if(!empty($posts["password"])){
            if($posts['password'] != $posts['password_check']){
                $error.="<p>パスワードが一致しません。</p>";
            }
            if (!empty($posts["password"])  && !preg_match('/\A(?=.*?[a-z])(?=.*?\d)[a-z\d]{8,20}+\z/i', $posts["password"]))
            {
                $error.="<p>パスワードは8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</p>";
            }
        }

        //暗号化
        //メールアドレス変更チェック
        $iv = $user->iv;
        $mail = $user->email;
        $email = $this->mail_decrypt($iv, $mail);
        $email = trim($email);

        $tmp_iv = $user->tmp_iv;
        $tmp_email = $user->tmp_email;
        $tmp_email = $this->mail_decrypt($tmp_iv, $tmp_email);
        $tmp_email = trim($tmp_email);


        //メールアドレスが変更された場合
        if($email != $posts["mail1"]){
            //HRBCに登録してあるかチェック
            $result = $this->getHrbcMaster();

            /*
            $param = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Mail:full=' . $posts['mail1'], 'field'=>'Person.P_Id', 'order'=>'Person.P_Id:asc'));
            $url = "https://api-hrbc-jp.porterscloud.com/v1/candidate?" . $param;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);

            if($xml = @file_get_contents($url, false, $context)){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 confirm"));
                            redirect($this->segment."/regist");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 confirm"));
                            redirect($this->segment."/regist/");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other confirm"));
                            redirect($this->segment."/regist/");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout confirm"));
                    redirect($this->segment."/regist/");
                }
            }
            $xml = simplexml_load_string($xml);
            */
            $url = $this->hrbcUrl .  "resume?candidate=". $result["partition"] . "&count=1&start=0&condition=Person.P_Mail:full=".$posts['mail1']."&field=Person.P_Id&order=Person.P_Id:asc";

            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

            if($xml->Code!=0){
                $pErr=$xml->Code;

                $subject = "【code:user1】" . $pErr;
                $message = $pErr;
                $this->errMail($subject, $message);

                $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
                redirect("err/error_404/");
                //exit;
            }

            //HRBCから帰ってきたデータ件数　1件の対応用に取得
            $cnt = $xml->attributes()->Count;

            if($cnt!=0) {
                $error.="<div id=\"profileMessageBox\" class=\"mailerror\"><div class=\"inner\"><p><span>登録済みのメールアドレスです。</span></p></div></div>";
            }
        }

        ////////////////////////////////////////////////////////////////////
        //// 閲覧履歴
        //////////////////////////////////////////////////////////////////////
        $looks = $this->getLooks($login_id);
        $data["looks"] = $looks;

        //オブジェクトへ変換
        $data[$this->segment] = (object)$posts;

        if($error !== ""){
            //data設定
            $data["render"] = $this->segment."/regist";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["sub_title"] = $this->sub_title;
            $data["msg"] = $error;
            $data["msgcolor"]= 1;
            $data["segment"] = $this->segment;
            $data["body_id"] = "registration";

            $this->load->view("template_search", $data);
        }else{
            //data設定
            $data["render"] = $this->segment."/confirm";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["msg"] = $error;
            $data["sub_title"] = $this->sub_title;
            $data["segment"] = $this->segment;
            $data["body_id"] = "registration";

            $this->load->view("template_search", $data);
        }
    }

    //////////////////////////////////////////////////////////
    //新規登録処理
    //////////////////////////////////////////////////////////
    function add()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        //CSRFチェック
        /*
        if ($this->_valid_csrf_nonce() === FALSE)
        {
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/logout/");
        }
        */

        //バリデーション実行
        $error = "";
        $error = $this->setValidation();

        $posts = $this->input->post();

        if(!checkdate($posts["month"], $posts["day"], $posts["year"])) {
            $error.="<p>日付の形式が正しくありません。</p>";
        }

        $this->session->unset_userdata('pass');
        $this->session->set_userdata("pass", $posts["password"]);
        $posts["password"] = $this->session->userdata("pass");

        //パスワードチェック
        if(!empty($posts["password"])){
            if($posts['password'] != $posts['password_check']){
                $error.="<p>パスワードが一致しません。</p>";
            }
            if (!empty($posts["password"])  && !preg_match('/\A(?=.*?[a-z])(?=.*?\d)[a-z\d]{8,20}+\z/i', $posts["password"]))
            {
                $error.="<p>パスワードは8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</p>";
            }
        }
        if(!empty($error)){
            $error=str_replace(array("<p>","</p>"),"",$error);
            $error="<div id=\"profileMessageBox\" class=\"mailerror\"><div class=\"inner\"><p><span>".$error."</span></p></div></div>";
        }

        //暗号化
        //メールアドレス変更チェック
        $iv = $user->iv;
        $mail = $user->email;
        $email = $this->mail_decrypt($iv, $mail);
        $email = trim($email);

        $data=array();
        //パスワード変更
        if(!empty($posts["password"])){
            $pass = hash_hmac("sha256", $posts['password'], Salt);
            $salt = hash_hmac("sha256", Salt, Salt);
            $pass_unit = $salt . $pass;
            $data['password'] = $pass_unit;
        }

        $mFlg = 0;
        if($email != $posts["mail1"]){
            $mFlg=1;
            $tmp_email = $posts["mail1"];
        }

        //メールアドレスが変更された場合
        if($mFlg==1){
            //HRBCに登録してあるかチェック
            $result = $this->getHrbcMaster();
            /*
            $param = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Mail:full=' . $posts['mail1'], 'field'=>'Person.P_Id', 'order'=>'Person.P_Id:asc'));
            $url = "https://api-hrbc-jp.porterscloud.com/v1/candidate?" . $param;
            $opts = array(
                'http'=>array(
                    'method' => 'GET',
                    'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                        . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                )
            );
            $context = stream_context_create($opts);

            if($xml = @file_get_contents($url, false, $context)){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 add"));
                            redirect($this->segment."/regist/");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 add"));
                            redirect($this->segment."/regist/");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other add"));
                            redirect($this->segment."/regist/");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout add"));
                    redirect($this->segment."/regist/");
                }
            }
            $xml = simplexml_load_string($xml);
            */
            $url = $this->hrbcUrl .  "candidate?partition=". $result["partition"] . "&count=1&start=0&condition=Person.P_Mail:full=".$posts['mail1']."&field=Person.P_Id&order=Person.P_Id:asc";

            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

            if($xml->Code!=0){
                $pErr=$xml->Code;
                $subject = "【code:user1】" . $pErr;
                $message = $pErr;
                $this->errMail($subject, $message);

                $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
                redirect("err/error_404/");
                //exit;
            }

            //HRBCから帰ってきたデータ件数　1件の対応用に取得
            $cnt = $xml->attributes()->Count;

            if($cnt!=0) {
                $error.="<div id=\"profileMessageBox\" class=\"mailerror\"><div class=\"inner\"><p><span>登録済みのメールアドレスです。</span></p></div></div>";
            }else{
                //暗号化のivを設定
                $tmp_iv = $this->mail_crypt_iv($posts["mail1"]);
                $crypt_mail = $this->mail_crypt($posts["mail1"], $tmp_iv);

                $base64_Iv = base64_encode($tmp_iv);
                $base64_CryptTarget = base64_encode($crypt_mail);

                $hash_email = hash_hmac("sha256", $posts['mail1'], Salt);

                //メールを変更したら認証処理を行うためactivateを0にする
                $token = $this->generatePassword();
                //DBにforget_passwordトークン登録
                $data["forget_password"] = $token;
                $data["forget_password_limit"]=strtotime("tomorrow");
                $data["tmp_email"] = $base64_CryptTarget;
                $data["tmp_iv"] = $base64_Iv;

                //登録のメールアドレスは旧のまま
                $posts["mail1"] = $email;
            }
        }

        if($error !== ""){
            ////////////////////////////////////////////////////////////////////
            //// 閲覧履歴
            //////////////////////////////////////////////////////////////////////
            $looks = $this->getLooks($login_id);
            $data["looks"] = $looks;

            //オブジェクトへ変換
            $data[$this->segment] = (object)$posts;

            //data設定
            $data["render"] = $this->segment."/regist";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["msg"] = $error;
            $data["msgcolor"]= 1;
            $data["segment"] = $this->segment;

            $this->load->view("template_search", $data);
        }else{
            //DB登録
            if(!empty($data)){
                $this->db->where('id', $login_id);
                $ret = $this->db->update($this->database, $data);

                if(empty($ret)){
                    //エラーメール送信
                    $subject = "【会員更新失敗　code:user01】 user:" . $login_id;
                    $message = "会員更新失敗に失敗しました。 user:" . $login_id;
                    $this->errMail($subject, $message);
                    //登録失敗
                    $this->session->set_flashdata(array("msg"=>"登録に失敗しました。管理者へ問い合わせください。"));
                    redirect($this->segment."/complete/");
                }
            }

            //登録するために退避データを正規の名前のものに入れる
            $posts["pref"] = $posts["kpref"];
            $result = $this->HrbcRegist($posts);

            if(!empty($result)){
                //エラー処理
                //エラーメール送信
                $subject = "【会員更新失敗　code:user02】 user:" . $login_id . " error:" . $result;
                $message = "会員更新失敗に失敗しました。 user:" . $login_id . " error:" . $result;
                $this->errMail($subject, $message);

                //登録失敗
                $this->session->set_flashdata(array("msg"=>"登録に失敗しました。管理者へ問い合わせください。"));
                redirect($this->segment."/complete/");
            }else{
                //メールアドレス変更の場合は認証処理
                if($mFlg==1){
                    $url = base_url() . $this->segment."/regenerate/?re=" . $token;

                    $this->load->library('email');
                    $this->email->set_wordwrap(FALSE);
                    $this->email->from(hurexMailFrom, kanriMailTitle);
                    $this->email->to($tmp_email);

                    $this->email->subject('認証メール');
                    $this->email->message("以下のURLにアクセスしてメールアドレスの変更を完了させてください。\n" . $url);

                    if ( ! $this->email->send()){
                        echo $this->email->print_debugger();
                        exit;
                        //エラーメール送信
                        $subject = "【会員更新失敗　code:userEmail】 user:" . $login_id . " error:" . $result;
                        $message = "会員更新失敗に失敗しました。 user:" . $login_id . " error:" . $result;
                        $this->errMail($subject, $message);
                    }
                }

                if($mFlg==1){
                    $msg = "<div id=\"profileMessageBox\" class=\"mailsend\"><div class=\"inner\"><div class=\"icoBox\"><p>認証メールを送信いたしました。<br>メールのURLをクリックして編集を完了させてください。</p></div></div></div>";
                }else{
                    $msg = "<div id=\"profileMessageBox\"><div class=\"inner\"><p><span>プロフィールの編集が完了しました。</span></p></div></div>";
                }
                //登録成功
                $this->session->set_flashdata(array("msg"=>$msg));
                redirect($this->segment."/complete/");
            }
        }
    }

    //////////////////////////////////////////////////////////
    //パスワード変更の認証
    //////////////////////////////////////////////////////////
    function regenerate(){
        $code = $this->input->get('re', TRUE);

        if($code){
            $ret = $this->db->get_where($this->database, array('forget_password' => $code, 'del'=>0))->row();

            if(empty($ret)){
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
                redirect($this->segment."/logout/");
            }
            $login_id = $ret->id;
            $tmp_iv = $ret->tmp_iv;
            $mail = $ret->tmp_email;
            $email = $this->mail_decrypt($tmp_iv, $mail);
            $email = trim($email);
            $hash_email = hash_hmac("sha256", $email, Salt);

            $data['id'] = $login_id;
            $data['email'] = $mail;
            $data['hash_email'] = $hash_email;
            $data["iv"] = $tmp_iv;
            $data['forget_password_limit'] = "";
            $data['forget_password'] = "";
            $data['activate'] = 1;
            $data['tmp_email'] = "";
            $data['tmp_iv'] = "";

            $this->load->model($this->database.'_model');
            $tmpDb=$this->database."_model";
            $ret = $this->$tmpDb->save($data['id'], $data, "id");

            //HRBC登録
            $data["mail1"] = $email;
            $result = $this->HrbcRegistMail($data);

            if(!empty($result)){
                //エラー処理
                //エラーメール送信
                $subject = "【会員更新失敗　code:user02】 user:" . $login_id . " error:" . $result;
                $message = "会員更新失敗に失敗しました。 user:" . $login_id . " error:" . $result;
                $this->errMail($subject, $message);

                //登録失敗
                $this->session->set_flashdata(array("msg"=>"登録に失敗しました。管理者へ問い合わせください。"));
                redirect($this->segment."/complete/");
            }
            $msg = "<div id=\"profileMessageBox\"><div class=\"inner\"><p><span>プロフィールの編集が完了しました。</span></p></div></div>";
            //登録成功
            $this->session->set_flashdata(array("msg"=>$msg));
            redirect($this->segment."/complete/");
        }else{
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/logout/");
        }
    }

    //////////////////////////////////////////////////////////
    //登録完了画面
    //////////////////////////////////////////////////////////
    function complete(){
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $user = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();
        $resume_id = $user->hrbc_resume_id;
        $person_id = $user->hrbc_person_id;

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');


        //DBから取得
        $tmp_data[$this->segment] = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();

        //HRBC
        $result = $this->getHrbcMaster();

        ///////////////////////////////////////////////////
        //person取得
        ///////////////////////////////////////////////////
        /*
        $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Id:eq=' . $person_id));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/candidate?" . $url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist"));
                        redirect($this->segment."/regist/");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist"));
                        redirect($this->segment."/regist/");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist"));
                        redirect($this->segment."/regist/");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist"));
                redirect($this->segment."/regist/");
            }
        }
        $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "candidate?partition=". $result["partition"] . "&count=1&start=0&condition=Person.P_Id:eq=".$person_id;
        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);


        if($xml->Code!=0){
            $pErr=$xml->Code;

            $subject = "【user基本情報編集に失敗　code:user01】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
            $message = "user基本情報編集に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id . " error:" . $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404/");
            //exit;
        }

        //データ設定
        $json = json_encode($xml);
        $person = json_decode($json,true);

        ////////////////////////////////////////////////////////////////
        //candidate Idからresume取得
        ///////////////////////////////////////////////////////////////
        /*
        $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Id:eq=' . $resume_id));
        $url = "https://api-hrbc-jp.porterscloud.com/v1/resume?" . $url;
        $opts = array(
            'http'=>array(
                'method' => 'GET',
                'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                    . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
            )
        );
        $context = stream_context_create($opts);

        if($xml = @file_get_contents($url, false, $context)){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist2"));
                        redirect($this->segment."/regist");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist2"));
                        redirect($this->segment."/regist/");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist2"));
                        redirect($this->segment."/regist/");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist2"));
                redirect($this->segment."/regist/");
            }
        }
        $xml = simplexml_load_string($xml);
        */
        $url = $this->hrbcUrl .  "resume?partition=". $result["partition"] . "&count=1&start=0&condition=Resume.P_Id:eq=".$resume_id;
        $options = array
        (
            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);


        if($xml->Code!=0){
            $pErr=$xml->Code;

            $subject = "【user基本情報編集に失敗　code:user02】 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id;
            $message = "user基本情報編集に失敗しました。 user:" . $login_id . " resume:" . $resume_id . " person:" . $person_id . " error:" . $pErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
            redirect("err/error_404/");
            //exit;
        }

        //データ設定
        $json = json_encode($xml);
        $resume = json_decode($json,true);

        //データの整形
        $data[$this->segment] = new stdClass;
        $data = $this->getBasicInfo($person, $resume);
        //検索と名前がバッティングするので退避
        //$data["user"]->kpref = $data["user"]->pref;


        ////////////////////////////////////////////////////////////////////
        //// 閲覧履歴
        //////////////////////////////////////////////////////////////////////
        $looks = $this->getLooks($login_id);
        //$looks="";
        $data["looks"] = $looks;

        //data設定
        $data["render"] = $this->segment."/complete";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["segment"] = $this->segment;
        $data["body_id"] = "registration";

        $this->load->view("template_search", $data);
    }

    //////////////////////////////////////////////////////////
    //HRBC登録
    //////////////////////////////////////////////////////////
    private function HrbcRegist($datas=array())
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $ret = $this->db->get_where($this->database, array('id' => $login_id, 'del'=>0))->row();

        $person_id = $ret->hrbc_person_id;
        if(empty($person_id)){
            redirect($this->segment . "/login/");
        }
        $resume_id = $ret->hrbc_resume_id;
        if(empty($resume_id)){
            redirect($this->segment . "/login/");
        }

        $result = $this->getHrbcMaster();

        //誕生日
        $birthday = $datas["year"] . "/" . $datas["month"] . "/" . $datas["day"];

        //性別
        foreach($result["gender"]['Item'] as $k=>$v) {
            if (!empty($v["Item"])) {
                foreach ($v['Item'] as $k2 => $v2) {
                    if ($datas["sex"] == $v2['Option.P_Name']) {
                        $gender = "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                    }
                }
            }
        }

        //都道府県
        foreach($result["pref"]['Item'] as $k=>$v){
            if (!empty($v["Item"])) {
                foreach($v['Item'] as $k2=>$v2){
                    if($datas["pref"]==$v2['Option.P_Name']){
                        $prefecture = "<" . htmlspecialchars($v2['Option.P_Alias'],ENT_QUOTES,'UTF-8') . "/>";
                    }
                }
            }
        }

        //学歴
        $school_div="";
        foreach($result["background"]['Item'] as $k=>$v){
            if (!empty($v["Item"])) {
                foreach($v['Item'] as $k2=>$v2) {
                    if ($datas["school_div_id"] == $v2['Option.P_Name']) {
                        $school_div = "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                    }
                }
            }
        }
        //学校名
        if(!empty($datas["school_name"])){
            $school_name = $datas["school_name"];
        }else{
            $school_name = "";
        }

        //就業状況
        foreach($result["work"]['Item'] as $k=>$v){
            if (!empty($v["Item"])) {
                foreach($v['Item'] as $k2=>$v2) {
                    if ($datas["jokyo"] == $v2['Option.P_Name']) {
                        $present_status = "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                    }
                }
            }
        }

        //現在の年収
        $earnings="";
        if(!empty($datas["earnings"])){
            foreach($result["income"]['Item'] as $k=>$v){
                if (!empty($v["Item"])) {
                    foreach($v['Item'] as $k2=>$v2) {
                        if(!empty($datas["earnings"])){
                            if ($datas["earnings"] == $v2['Option.P_Id']) {
                                $earnings .= "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                            }
                        }
                    }
                }
            }
        }

        //直近の会社名
        $company_name1 = $datas["company_name1"];
        $company_name2 = $datas["company_name2"];
        $company_name3 = $datas["company_name3"];

        //担当職種
        /*
        $tantou_job1="";
        foreach($result["jobmypagecd"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($datas["tantou_job1"])){
                        foreach($datas["tantou_job1"] as $kk => $kv){
                            if ($kv == $v2['Option.P_Id']) {
                                $tantou_job1 .= "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                            }
                        }
                    }
                }
            }
        }
        */
        $tantou_job1="";
        foreach($result["jobmypagecd"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"])){
                        foreach($v2["Items"]['Item'] as $k3=>$v3) {
                            if(!empty($datas["tantou_job1"])){
                                foreach($datas["tantou_job1"] as $kk => $kv){
                                    if ($kv == $v3['Option.P_Id']) {
                                        $tantou_job1 .= "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $tantou_job2="";
        foreach($result["jobmypagecd"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"])){
                        foreach($v2["Items"]['Item'] as $k3=>$v3) {
                            if(!empty($datas["tantou_job2"])){
                                foreach($datas["tantou_job2"] as $kk => $kv){
                                    if ($kv == $v3['Option.P_Id']) {
                                        $tantou_job2 .= "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $tantou_job3="";
        foreach($result["jobmypagecd"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"])){
                        foreach($v2["Items"]['Item'] as $k3=>$v3) {
                            if(!empty($datas["tantou_job3"])){
                                foreach($datas["tantou_job3"] as $kk => $kv){
                                    if ($kv == $v3['Option.P_Id']) {
                                        $tantou_job3 .= "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //DB保存用
        $tantou_jobs1 = "";
        if(!empty($datas["tantou_job1"])){
            foreach($datas["tantou_job1"] as $k => $v){
                if($k!=0) $tantou_jobs1.=",";
                $tantou_jobs1.=$v;
            }
        }

        $tantou_jobs2 = "";
        if(!empty($datas["tantou_job2"])){
            foreach($datas["tantou_job2"] as $k => $v){
                if($k!=0) $tantou_jobs2.=",";
                $tantou_jobs2.=$v;
            }
        }

        $tantou_jobs3 = "";
        if(!empty($datas["tantou_job3"])){
            foreach($datas["tantou_job3"] as $k => $v){
                if($k!=0) $tantou_jobs3.=",";
                $tantou_jobs3.=$v;
            }
        }

        //在籍
        $start_date1  = $datas["start_year1"] . "/" . sprintf('%02d', $datas["start_month1"]) . "/01";
        $start_date2="";
        if(!empty($datas["start_year2"]) && !empty($datas["start_month2"])){
            $start_date2  = $datas["start_year2"] . "/" . sprintf('%02d', $datas["start_month2"]) . "/01";
        }
        $start_date3="";
        if(!empty($datas["start_year3"]) && !empty($datas["start_month3"])){
            $start_date3  = $datas["start_year3"] . "/" . sprintf('%02d', $datas["start_month3"]) . "/01";
        }
        $end_date1="";
        if(!empty($datas["end_year1"]) && !empty($datas["end_month1"])){
            $end_date1  = $datas["end_year1"] . "/" . sprintf('%02d', $datas["end_month1"]) . "/01";
        }
        $end_date2="";
        if(!empty($datas["end_year2"]) && !empty($datas["end_month2"])){
            $end_date2  = $datas["end_year2"] . "/" . sprintf('%02d', $datas["end_month2"]) . "/01";
        }
        $end_date3="";
        if(!empty($datas["end_year3"]) && !empty($datas["end_month3"])){
            $end_date3  = $datas["end_year3"] . "/" . sprintf('%02d', $datas["end_month3"]) . "/01";
        }

        /////////////////////////////////////////////////////////////////////
        //// HRBC Person
        ////////////////////////////////////////////////////////////////////////
        $url =$this->hrbcUrl .  "candidate?partition=".$result["partition"];

        $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Candidate><Item><Person.P_Id>' . $person_id . '</Person.P_Id><Person.P_Owner>1</Person.P_Owner><Person.P_Name>' . $datas["shimei"] . '</Person.P_Name><Person.P_Reading>' . $datas["kana"] . '</Person.P_Reading><Person.P_Telephone>' . $datas["tel1"] .'</Person.P_Telephone><Person.P_Street>' . $datas["pref"] . '</Person.P_Street><Person.P_Mail>' . $datas["mail1"] .'</Person.P_Mail></Item></Candidate>';
        $tmp_xml=$xml;

        /*
        $header = array(
            "Content-Type: application/xml; charset=UTF-8",
            "X-porters-hrbc-oauth-token: " . $result['token']
        );

        $options = array('http' => array(
            'method' => 'POST',
            'content' => $xml,
            'header' => implode("\r\n", $header)
        ));

        if($xml = @file_get_contents($url, false, stream_context_create($options))){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 hrbcRegist"));
                        redirect($this->segment."/regist/");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 hrbcRegist"));
                        redirect($this->segment."/regist/");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other hrbcRegist"));
                        redirect($this->segment."/regist/");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout hrbcRegist"));
                redirect($this->segment."/regist/");
            }
        }
        */
        //xml解析
        $pErr="";
        $pId = "";
        $options = array
        (
            CURLOPT_URL            => $url,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $xml,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result['token']),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);
        $this->writeRegistLog($tmp_xml, $xml, "user03");

        if($xml->Code!=0){
            $pErr=$xml->Code;
        }else{
            $pCode="";
            $json = json_encode($xml);
            $arr = json_decode($json,true);
            foreach($arr['Item'] as $k => $v){
                if($k=="Id"){
                    $pId = $v;
                }
                if($k=="Code"){
                    $pErr = $v;

                }
            }
        }

        $error="";
        if(!$pId || $pErr){
            //エラーメール送信
            $subject = "【会員更新失敗　code:user02】 user:" . $login_id . " error:" . $pErr;
            $message = "会員更新失敗に失敗しました。 user:" . $login_id . " error:" . $pErr;
            $this->errMail($subject, $message);

            $error.= "エラーが発生しました。(Person: " . $pErr . ")<br />管理者へ問い合わせて下さい。";
        }

        if(empty($error)){
            ///////////////////////////////////////////////////////////////
            // HRBC Resume
            ///////////////////////////////////////////////////////////////
            $url = $this->hrbcUrl . "resume?partition=".$result["partition"];

            //※レジュメにメールとTELを入れるとエラーになる
            $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>1</Resume.P_Owner><Resume.P_Candidate>'. $pId .'</Resume.P_Candidate><Resume.P_Name>' . $datas["shimei"] . '</Resume.P_Name><Resume.U_91DBD8B96BAAD6B33B538CF2494377>' . $datas["kana"] . '</Resume.U_91DBD8B96BAAD6B33B538CF2494377><Resume.P_DateOfBirth>' . $birthday . '</Resume.P_DateOfBirth><Resume.P_Gender>' . $gender . '</Resume.P_Gender><Resume.P_CurrentStatus>' . $present_status . '</Resume.P_CurrentStatus><Resume.U_5F7FC5144BA471117A376B60BC8D90>' . $datas["comment"] .'</Resume.U_5F7FC5144BA471117A376B60BC8D90><Resume.P_ChangeJobsCount>' . $datas["company_number"] . '</Resume.P_ChangeJobsCount><Resume.U_7F3DF7F61EEF0679AA432646777832>' . $prefecture .'</Resume.U_7F3DF7F61EEF0679AA432646777832><Resume.U_85DEA8FA18EF17830FD8A036EC95A4>' . $school_name . '</Resume.U_85DEA8FA18EF17830FD8A036EC95A4><Resume.U_673275A4DA445C9B23AC18BDD7C4DC>' . $school_div . '</Resume.U_673275A4DA445C9B23AC18BDD7C4DC><Resume.U_9A9AD75E2CF88361F40E91D2B53876>' . $company_name1 . '</Resume.U_9A9AD75E2CF88361F40E91D2B53876><Resume.U_12C7C4EC761C78DB864D9AD0FA73ED>' . $start_date1 . '</Resume.U_12C7C4EC761C78DB864D9AD0FA73ED><Resume.U_4679EE290064C24E5E593F5772732D>' . $end_date1 . '</Resume.U_4679EE290064C24E5E593F5772732D><Resume.U_AE99DF32764C633CAE8A79EDC68490>' . $company_name2 . '</Resume.U_AE99DF32764C633CAE8A79EDC68490><Resume.U_7CCBD503A17CBD06078DB0907BEC9B>' . $start_date2 . '</Resume.U_7CCBD503A17CBD06078DB0907BEC9B><Resume.U_FD0444A71EB7C820DC1455C4358EE8>' . $end_date2 .'</Resume.U_FD0444A71EB7C820DC1455C4358EE8><Resume.U_C12A1CD0372B2FECA9055E0302F59D>' . $company_name3 . '</Resume.U_C12A1CD0372B2FECA9055E0302F59D><Resume.U_F65492E72A71583EE72F032C73885A>' . $start_date3 . '</Resume.U_F65492E72A71583EE72F032C73885A><Resume.U_F6662BA5A33D671A4516974181F5F0>' . $end_date3 . '</Resume.U_F6662BA5A33D671A4516974181F5F0><Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504>' . $earnings . '</Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504><Resume.U_90CE972D1600F17EDCEC4661728935>' . $tantou_job1 . '</Resume.U_90CE972D1600F17EDCEC4661728935><Resume.U_910B68CCF4A2F064E0059737A883C0>' . $tantou_job2 .'</Resume.U_910B68CCF4A2F064E0059737A883C0><Resume.U_7245CA44510F223C359FE2D7BC66DB>' . $tantou_job3 . '</Resume.U_7245CA44510F223C359FE2D7BC66DB></Item></Resume>';
            $tmp_xml = $xml;
            /* テスト用
            $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>1</Resume.P_Owner><Resume.P_Candidate>'. $pId .'</Resume.P_Candidate><Resume.P_Name>' . $datas["shimei"] . '</Resume.P_Name><Resume.U_91DBD8B96BAAD6B33B538CF2494377>' . $datas["kana"] . '</Resume.U_91DBD8B96BAAD6B33B538CF2494377><Resume.P_DateOfBirth>' . $birthday . '</Resume.P_DateOfBirth><Resume.P_Gender>' . $gender . '</Resume.P_Gender><Resume.P_CurrentStatus>' . $present_status . '</Resume.P_CurrentStatus><Resume.U_5F7FC5144BA471117A376B60BC8D90>' . $datas["comment"] .'</Resume.U_5F7FC5144BA471117A376B60BC8D90><Resume.P_ChangeJobsCount>' . $datas["company_number"] . '</Resume.P_ChangeJobsCount><Resume.U_7F3DF7F61EEF0679AA432646777832>' . $prefecture .'</Resume.U_7F3DF7F61EEF0679AA432646777832><Resume.U_85DEA8FA18EF17830FD8A036EC95A4>' . $school_name . '</Resume.U_85DEA8FA18EF17830FD8A036EC95A4><Resume.U_673275A4DA445C9B23AC18BDD7C4DC>' . $school_div . '</Resume.U_673275A4DA445C9B23AC18BDD7C4DC><Resume.U_9A9AD75E2CF88361F40E91D2B53876>' . $company_name1 . '</Resume.U_9A9AD75E2CF88361F40E91D2B53876><Resume.U_12C7C4EC761C78DB864D9AD0FA73ED>' . $start_date1 . '</Resume.U_12C7C4EC761C78DB864D9AD0FA73ED><Resume.U_4679EE290064C24E5E593F5772732D>' . $end_date1 . '</Resume.U_4679EE290064C24E5E593F5772732D><Resume.U_AE99DF32764C633CAE8A79EDC68490>' . $company_name2 . '</Resume.U_AE99DF32764C633CAE8A79EDC68490><Resume.U_7CCBD503A17CBD06078DB0907BEC9B>' . $start_date2 . '</Resume.U_7CCBD503A17CBD06078DB0907BEC9B><Resume.U_FD0444A71EB7C820DC1455C4358EE8>' . $end_date2 .'</Resume.U_FD0444A71EB7C820DC1455C4358EE8><Resume.U_C12A1CD0372B2FECA9055E0302F59D>' . $company_name3 . '</Resume.U_C12A1CD0372B2FECA9055E0302F59D><Resume.U_F65492E72A71583EE72F032C73885A>' . $start_date3 . '</Resume.U_F65492E72A71583EE72F032C73885A><Resume.U_F6662BA5A33D671A4516974181F5F0>' . $end_date3 . '</Resume.U_F6662BA5A33D671A4516974181F5F0><Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504>' . $earnings . '</Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504><Resume.U_B1451F4914E0F2E436C6CCD202528B>' . $tantou_job1 . '</Resume.U_B1451F4914E0F2E436C6CCD202528B><Resume.U_910B68CCF4A2F064E0059737A883C0>' . $tantou_job2 .'</Resume.U_910B68CCF4A2F064E0059737A883C0><Resume.U_7245CA44510F223C359FE2D7BC66DB>' . $tantou_job3 . '</Resume.U_7245CA44510F223C359FE2D7BC66DB></Item></Resume>';
            */

            /*
            $header = array(
                "Content-Type: application/xml; charset=UTF-8",
                "X-porters-hrbc-oauth-token: ".$result["token"]
            );

            $options = array('http' => array(
                'method' => 'POST',
                'content' => $xml,
                'header' => implode("\r\n", $header)
            ));
            if($xml = @file_get_contents($url, false, stream_context_create($options))){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 hrbcRegist2"));
                            redirect($this->segment."/regist/");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 hrbcRegist2"));
                            redirect($this->segment."/regist/");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other hrbcRegist2"));
                            redirect($this->segment."/regist/");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout hrbcRegist2"));
                    redirect($this->segment."/regist/");
                }
            }

            //xml解析

            $xml = simplexml_load_string($xml);
            */
            $rErr="";
            $rId = "";
            $options = array
            (
                CURLOPT_URL            => $url,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $xml,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result['token']),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

            $this->writeRegistLog($tmp_xml, $xml, "user04");

            if($xml->Code!=0){
                $rErr=$xml->Code;
            }else{
                $rCode="";
                $json = json_encode($xml);
                $arr = json_decode($json,true);
                foreach($arr['Item'] as $k => $v){
                    if($k=="Id"){
                        $rId = $v;
                    }
                    if($k=="Code"){
                        $rErr = $v;
                    }
                }
            }
            if(!$rId || $rErr){
                //エラーメール送信
                $subject = "【会員更新失敗　code:user03】 user:" . $login_id . " error:" . $pErr;
                $message = "会員更新失敗に失敗しました。 user:" . $login_id . " error:" . $pErr;
                $this->errMail($subject, $message);

                $error.= "エラーが発生しました。(Resume: ".$rErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
            }
        }

        $now = Date('Y-m-d H:i:s');
        $data = array(
            'earnings' => $datas["earnings"],
            'company_name1' => $company_name1,
            'company_name2' => $company_name2,
            'company_name3' => $company_name3,
            'tantou_job1' => $tantou_jobs1,
            'tantou_job2' => $tantou_jobs2,
            'tantou_job3' => $tantou_jobs3,
            'start_year1' => $datas["start_year1"],
            'start_month1' => $datas["start_month1"],
            'start_year2' => $datas["start_year2"],
            'start_month2' => $datas["start_month3"],
            'start_year3' => $datas["start_year2"],
            'start_month3' => $datas["start_month3"],
            'end_year1' => $datas["end_year1"],
            'end_month1' => $datas["end_month1"],
            'end_year2' => $datas["end_year2"],
            'end_month2' => $datas["end_month3"],
            'end_year3' => $datas["end_year2"],
            'end_month3' => $datas["end_month3"],
            'updated' => $now
        );
        $where = array(
            'id' => $ret->id
        );
        $ret = $this->db->update($this->database, $data, $where);
        if(empty($ret)){
            //エラーメール送信
            $subject = "【会員更新失敗　code:user04】 user:" . $login_id . " error:DBの更新に失敗";
            $message = "会員更新失敗に失敗しました。 user:" . $login_id . " error:DBの更新に失敗";
            $this->errMail($subject, $message);

            $error = "エラーが発生しました。";
        }
        $this->session->unset_userdata('login_name');
        return  $error;
    }

    //////////////////////////////////////////////////////////
    //メールアドレス登録
    //////////////////////////////////////////////////////////
    private function HrbcRegistMail($datas=array())
    {
        /*
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');
        */

        $ret = $this->db->get_where($this->database, array('id' =>$datas["id"], 'del' => 0))->row();

        $person_id = $ret->hrbc_person_id;
        if (empty($person_id)) {
            //エラーメール送信
            $subject = "【会員更新失敗　code:userP01】 user:" . $datas["id"];
            $message = "会員更新失敗に失敗しました。 user:" . $datas["id"];
            $this->errMail($subject, $message);
            redirect($this->segment . "/login/");
        }
        $resume_id = $ret->hrbc_resume_id;
        if (empty($resume_id)) {
            //エラーメール送信
            $subject = "【会員更新失敗　code:userR01】 user:" . $datas["id"];
            $message = "会員更新失敗に失敗しました。 user:" . $datas["id"];
            $this->errMail($subject, $message);
            redirect($this->segment . "/login/");
        }

        $result = $this->getHrbcMaster();

        /////////////////////////////////////////////////////////////////////
        //// HRBC Person
        ////////////////////////////////////////////////////////////////////////
        $url = $this->hrbcUrl . "candidate?partition=" . $result["partition"];

        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Candidate><Item><Person.P_Id>' . $person_id . '</Person.P_Id><Person.P_Owner>1</Person.P_Owner><Person.P_Mail>' . $datas["mail1"] . '</Person.P_Mail></Item></Candidate>';
        $tmp_xml=$xml;

        /*
        $header = array(
            "Content-Type: application/xml; charset=UTF-8",
            "X-porters-hrbc-oauth-token: " . $result['token']
        );

        $options = array('http' => array(
            'method' => 'POST',
            'content' => $xml,
            'header' => implode("\r\n", $header)
        ));

        if($xml = @file_get_contents($url, false, stream_context_create($options))){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($status_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 hrbcRegistMail"));
                        redirect($this->segment."/regist/");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 hrbcRegistMail"));
                        redirect($this->segment."/regist/");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other hrbcRegistMail"));
                        redirect($this->segment."/regist/");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout hrbcRegistMail"));
                redirect($this->segment."/regist/");
            }
        }
*/
        //xml解析
        $pErr = "";
        $pId = "";
        $options = array
    (
        CURLOPT_URL            => $url,
        CURLOPT_POST           => true,
        CURLOPT_POSTFIELDS     => $xml,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => "UTF-8",
        CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result['token']),
        CURLINFO_HEADER_OUT    => true,
    );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);
        $this->writeRegistLog($tmp_xml, $xml, "user05");

        if ($xml->Code != 0) {
            $pErr = $xml->Code;
        } else {
            $pCode = "";
            $json = json_encode($xml);
            $arr = json_decode($json, true);
            foreach ($arr['Item'] as $k => $v) {
                if ($k == "Id") {
                    $pId = $v;
                }
                if ($k == "Code") {
                    $pErr = $v;

                }
            }
        }

        $error = "";
        if (!$pId || $pErr) {
            //エラーメール送信
            $subject = "【会員更新失敗　code:user03】 user:" . $login_id . " error:" . $pErr;
            $message = "会員更新失敗に失敗しました。 user:" . $login_id . " error:" . $pErr;
            $this->errMail($subject, $message);

            $error .= "エラーが発生しました。(Person: " . $pErr . ")<br />管理者へ問い合わせて下さい。";
        }
        return  $error;
    }

    //////////////////////////////////////////////////////////
    //バリデーション
    //////////////////////////////////////////////////////////
    private function setValidation($validation=null)
    {
        $errors ="";
        $this->form_validation->set_rules("shimei", "氏名","required");
        $this->form_validation->set_rules("kana", "ふりがな","required");
        $this->form_validation->set_rules("year", "年","required");
        $this->form_validation->set_rules("month", "月","required");
        $this->form_validation->set_rules("day", "日","required");
        $this->form_validation->set_rules("sex", "性別","required");
        $this->form_validation->set_rules("kpref", "住所","required");
        $this->form_validation->set_rules("tel1", "電話番号","required");
        $this->form_validation->set_rules("school_div_id", "最終学歴","required");
        $this->form_validation->set_rules("company_number", "経験社数","required");
        $this->form_validation->set_rules("jokyo", "就業状況","required");
        $this->form_validation->set_rules("mail1", "メールアドレス","required|valid_email");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }

    //////////////////////////////////////////////////////////
    //バリデーション
    //////////////////////////////////////////////////////////
    private function setkibouValidation($validation=null)
    {
        $errors ="";
        $this->form_validation->set_rules("kiboujob[]", "希望職種","required|numeric");
        $this->form_validation->set_rules("expectarea1", "勤務地第1希望","required");
        $this->form_validation->set_rules("tenkin[]", "転勤","required");
        $this->form_validation->set_rules("income", "希望年収","required|numeric");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }


    //////////////////////////////////////////////////////////
    //パスワード変更
    //////////////////////////////////////////////////////////
    function change_password()
    {
        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        $data[$this->segment] = $this->db->get_where($this->database, array('id' => $login_id, 'del' => 0))->row();

        //data設定
        $data["render"] = $this->segment."/change_password";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["segment"] = $this->segment;

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //パスワード更新処理
    //////////////////////////////////////////////////////////
    public function reset_password($code = NULL)
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');

        //CSRFチェック
        if ($this->_valid_csrf_nonce() === FALSE)
        {
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/home/");
        }

        //バリデーション実行
        $error = "";
        $error = $this->setPasswordValidation();

        $posts = $this->input->post();

        if($posts['password'] != $posts['password_check']){
            $error.="<p>パスワードが一致しません。</p>";
        }

        if($error !== ""){
            //data設定
            $data[$this->segment] =  (object) $posts;
            $data["render"] = $this->segment."/change_password";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["msg"] = $error;
            $data["segment"] = $this->segment;

            $this->load->view("template", $data);
        }else{
            //パスワード再発行
            $pass = hash_hmac("sha256", $posts["password"], Salt);
            $salt = hash_hmac("sha256", Salt, Salt);
            $pass_unit = $salt . $pass;
            $data['password'] = $pass_unit;

            $this->load->model($this->database.'_model');
            $tmpDb=$this->database."_model";
            $ret = $this->$tmpDb->save($login_id, $data, "id");

            if(empty($ret)){
                //エラー処理
            }else{
                //登録成功
                $this->session->set_flashdata(array("msg"=>"パスワードの更新が完了しました。"));
                redirect($this->segment."/change_password/");
            }
        }

    }

    //////////////////////////////////////////////////////////
    //パスワードバリデーション
    //////////////////////////////////////////////////////////
    private function setPasswordValidation($validation=null)
    {

        $errors ="";
        $this->form_validation->set_rules("password", "パスワード","required|min_length[8]|max_length[20]");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }

    //////////////////////////////////////////////////////////
    //パスワード忘れ
    //////////////////////////////////////////////////////////
    function forget_password()
    {
        $error="";
        $data["render"] = $this->segment."/forget_password";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $error;
        $data["segment"] = $this->segment;

        $this->load->view("template_nologin", $data);
    }

    //////////////////////////////////////////////////////////
    //パスワード再発行メール
    //////////////////////////////////////////////////////////
    function send_mail()
    {
        $posts = $this->input->post();

        //バリデーション実行
        $error = "";
        $error = $this->setSendmailValidation();

        $sameEmail="";
        if(empty($error)){
            $email = hash_hmac("sha256", $posts['email'], Salt);
            $sameEmail = $this->db->get_where($this->database, array('hash_email' => $email, 'del'=>0))->row();

            $status = $sameEmail->status;

            if($sameEmail && $status == 3){
                $token = $this->generatePassword();
                //DBにforget_passwordトークン登録
                $data = array(
                    'forget_password' => $token,
                    'forget_password_limit' => strtotime("tomorrow")
                );

                $this->db->where('id', $sameEmail->id);
                $ret = $this->db->update($this->database, $data);

                $url = base_url() . $this->segment."/create_password/?re=" . $token;

                $this->load->library('email');

                $this->email->from(hurexMailFrom, hurexMailTitle);
                $this->email->to($posts['email']);

                $this->email->subject('パスワード再発行メール');
                $this->email->message("以下のURLにアクセスしてください。\n" . $url);

                $this->email->send();

                $error.="パスワード再発行メールを送信しました。";
            }else if($sameEmail && $status ==1){

                $new_flg="";
                $new_flg = $sameEmail->new_flg;
                if($new_flg==1){
                    $new_flg = "&rt=1";
                }

                $url = base_url() . "signup/step1/?re=" . $sameEmail->forget_password . $new_flg;

                $error.="<p>ご入力いただきましたメールアドレスでは、マイページへのご登録が完了しておりません。<br />恐れ入りますが、<a href=\"" . $url . "\">基本情報入力画面</a>より、登録をお願い致します。</p>";
            }else if($sameEmail && $status ==2){

                //STEP1の登録済みチェック
                $hash_email = hash_hmac("sha256", $email, Salt);
                $user_info = $this->db->get_where($this->database, array('hash_email' => $hash_email, 'activate'=>0, 'status'=>2, 'del'=>0))->row();

                //data設定
                $this->session->set_userdata('email', $posts["email"]);
                $this->session->set_userdata('param', $user_info->parameter);


                $new_flg="";
                $new_flg = $sameEmail->new_flg;
                if($new_flg==1){
                    $new_flg = "&rt=1";
                }

                $url = base_url() . "signup/step2";

                $error.="<p>ご入力いただきましたメールアドレスでは、マイページへのご登録が完了しておりません。<br />恐れ入りますが、<a href=\"" . $url . "\" style=\"color:#0000ff;text-decoration:underline;\">希望情報入力画面</a>より、登録をお願い致します。</p>";
            }else{
                $error.="<p>ご入力いただきましたメールアドレスではマイページにご登録がございません。<br />恐れ入りますが、<a href=\"https://www.hurex.jp/mypage/signup/regist\" style=\"color:#0000ff;text-decoration:underline;\">新規登録画面</a>より、新規登録をお願い致します。</p>";
            }
        }

        //data設定
        $data[$this->segment] =  (object) $posts;
        $data["render"] = $this->segment."/forget_password";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $error;
        $data["segment"] = $this->segment;

        $this->load->view("template_nologin", $data);
    }

    //////////////////////////////////////////////////////////
    //メールアドレスチェック
    //////////////////////////////////////////////////////////
    private function setSendmailValidation($validation=null)
    {
        $errors ="";
        $this->form_validation->set_rules("email", "メールアドレス","required|valid_email");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }

    //////////////////////////////////////////////////////////
    //パスワード再発行画面
    //////////////////////////////////////////////////////////
    function create_password()
    {
        $code = $this->input->get('re', TRUE);
        $data[$this->segment] = $this->db->get_where($this->database, array('forget_password' => $code, 'del'=>0))->row();

        if(empty($data[$this->segment])){
            redirect($this->segment."/login/");
        }

        $error="";
        $data["render"] = $this->segment."/create_password";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $error;
        $data["code"] = $code;
        $data["segment"] = $this->segment;

        $this->load->view("template_nologin", $data);
    }

    //////////////////////////////////////////////////////////
    //パスワード再発行
    //////////////////////////////////////////////////////////
    function new_password()
    {
        $posts = $this->input->post();

        //codeチェック
        $code = $posts["code"];
        $result = $this->db->get_where($this->database, array('forget_password' => $code, 'del'=>0))->row();
        if(empty($result)){
            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/logout/");
        }

        //バリデーション実行
        $error = "";
        $error = $this->setNewPasswordValidation();

        if($posts['password'] != $posts['password_check']){
            $error.="<p>パスワードが一致しません。</p>";
        }

        $now = strtotime("now");

        if($result->forget_password_limit < $now){
            $this->session->set_flashdata(array("msg"=>"有効期限が切れました。再度パスワードの再発行の手続きをして下さい。"));
            redirect($this->segment."/logout/");
        }

        if($error !== ""){
            //data設定
            $data[$this->segment] =  (object) $posts;
            $data["render"] = $this->segment."/create_password";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["msg"] = $error;
            $data["segment"] = $this->segment;

            $this->load->view("template_nologin", $data);
        }else{
            //パスワード再発行
            $pass = hash_hmac("sha256", $posts['password'], Salt);
            $salt = hash_hmac("sha256", Salt, Salt);
            $pass_unit = $salt . $pass;
            $data['password'] = $pass_unit;
            $data['forget_password_limit'] = "";
            $data['forget_password'] = "";

            $this->load->model($this->database.'_model');
            $tmpDb=$this->database."_model";
            $ret = $this->$tmpDb->save($result->id, $data, "id");

            if(empty($ret)){
                //エラー処理
            }else{
                //登録成功
                $this->session->set_flashdata(array("msg"=>"パスワードの更新が完了しました。"));
                redirect($this->segment."/login/");
            }
        }
    }

    function getBasicInfo($person, $resume)
    {
        $data[$this->segment] = new stdClass;

        $data[$this->segment]->shimei = $person["Item"]["Person.P_Name"];
        if(!empty($person["Item"]["Person.P_Reading"])){
            $data[$this->segment]->kana = $person["Item"]["Person.P_Reading"];
        }else{
            $data[$this->segment]->kana = "";
        }

        if(!empty($resume["Item"]["Resume.P_DateOfBirth"])){
            $birthday = $resume["Item"]["Resume.P_DateOfBirth"];
            list($data[$this->segment]->year, $data[$this->segment]->month, $data[$this->segment]->day) = explode('/', $birthday);
        }else{
            $data[$this->segment]->year =  "";
            $data[$this->segment]->month =  "";
            $data[$this->segment]->day =  "";
        }

        $tmpgender = $resume["Item"]["Resume.P_Gender"];
        if(!empty($tmpgender)){
            foreach($tmpgender as $k=>$v){
                $data[$this->segment]->sex = $v["Option.P_Name"];
            }
        }else{
            $data[$this->segment]->sex = "";
        }
        $tmppref = $resume["Item"]["Resume.U_7F3DF7F61EEF0679AA432646777832"];
        if(!empty($tmppref)){
            foreach($tmppref as $k=>$v){
                $data[$this->segment]->pref = $v["Option.P_Name"];
            }
        }else{
            $data[$this->segment]->pref = "";
        }
        if(empty($person["Item"]["Person.P_Telephone"])){
            if(!empty($person["Item"]["Person.P_Mobile"])){
                $data[$this->segment]->tel1 = $person["Item"]["Person.P_Mobile"];
            }else{
                $data[$this->segment]->tel1 = "";
            }
        }else{
            $data[$this->segment]->tel1 = $person["Item"]["Person.P_Telephone"];
        }
        $data[$this->segment]->mail1 = $person["Item"]["Person.P_Mail"];
        $tmpschool = $resume["Item"]["Resume.U_673275A4DA445C9B23AC18BDD7C4DC"];
        if(!empty($tmpschool)){
            foreach($tmpschool as $k=>$v){
                $data[$this->segment]->school_div_id = $v["Option.P_Name"];
            }
        }else{
            $data[$this->segment]->school_div_id = "";
        }

        if(!empty($resume["Item"]["Resume.U_85DEA8FA18EF17830FD8A036EC95A4"])){
            $tmpschool_name = $resume["Item"]["Resume.U_85DEA8FA18EF17830FD8A036EC95A4"];
            $data[$this->segment]->school_name = $tmpschool_name;
        }else{
            $data[$this->segment]->school_name = "";
        }

        $tmpjokyo = $resume["Item"]["Resume.P_CurrentStatus"];
        if(!empty($tmpjokyo)){
            foreach($tmpjokyo as $k=>$v){
                $data[$this->segment]->jokyo = $v["Option.P_Name"];
            }
        }else{
            $data[$this->segment]->jokyo = "";
        }
        if(!empty($resume["Item"]["Resume.P_ChangeJobsCount"])){
            $data[$this->segment]->company_number = $resume["Item"]["Resume.P_ChangeJobsCount"];
        }else{
            $data[$this->segment]->company_number = "";
        }

        if(!empty($resume["Item"]["Resume.U_5F7FC5144BA471117A376B60BC8D90"])){
            $data[$this->segment]->comment = $resume["Item"]["Resume.U_5F7FC5144BA471117A376B60BC8D90"];
        }else{
            $data[$this->segment]->comment =  "";
        }

        //直近の会社
        if(!empty($resume["Item"]["Resume.U_9A9AD75E2CF88361F40E91D2B53876"])){
            $data[$this->segment]->company_name1 = $resume["Item"]["Resume.U_9A9AD75E2CF88361F40E91D2B53876"];
        }else{
            $data[$this->segment]->company_name1 =  "";
        }

        $tmp_tantou_job1="";
        $tantou_jobs1 = array();
        if(!empty($resume["Item"]["Resume.U_90CE972D1600F17EDCEC4661728935"])){
            $tmp_tantou_job1 = $resume["Item"]["Resume.U_90CE972D1600F17EDCEC4661728935"];
        }
        if(!empty($tmp_tantou_job1)){
            foreach($tmp_tantou_job1 as $k=>$v){
                $tantou_jobs1[] = $v["Option.P_Id"];
            }
        }
        $data[$this->segment]->tantou_job1= $tantou_jobs1;

        //テスト用
        /*
        $tmp_tantou_job1="";
        $tantou_jobs1 = array();
        if(!empty($resume["Item"]["Resume.U_B1451F4914E0F2E436C6CCD202528B"])){
            $tmp_tantou_job1 = $resume["Item"]["Resume.U_B1451F4914E0F2E436C6CCD202528B"];
        }
        if(!empty($tmp_tantou_job1)){
            foreach($tmp_tantou_job1 as $k=>$v){
                $tantou_jobs1[] = $v["Option.P_Id"];
            }
        }
        $data[$this->segment]->tantou_job1= $tantou_jobs1;
        */
        //テスト用ここまで

        if(!empty($resume["Item"]["Resume.U_12C7C4EC761C78DB864D9AD0FA73ED"])){
            $start_date1 = $resume["Item"]["Resume.U_12C7C4EC761C78DB864D9AD0FA73ED"];
            $data[$this->segment]->start_year1 =  substr($start_date1,0,4);
            $data[$this->segment]->start_month1 =  substr($start_date1,5,2);
        }else{
            $data[$this->segment]->start_year1 =  "";
            $data[$this->segment]->start_month1 =  "";
        }

        if(!empty($resume["Item"]["Resume.U_4679EE290064C24E5E593F5772732D"])){
            $end_date1 = $resume["Item"]["Resume.U_4679EE290064C24E5E593F5772732D"];
            $data[$this->segment]->end_year1 =  substr($end_date1,0,4);
            $data[$this->segment]->end_month1 =  substr($end_date1,5,2);
        }else{
            $data[$this->segment]->end_year1 =  "";
            $data[$this->segment]->end_month1 =  "";
        }

        //直近の会社２
        if(!empty($resume["Item"]["Resume.U_AE99DF32764C633CAE8A79EDC68490"])){
            $data[$this->segment]->company_name2 = $resume["Item"]["Resume.U_AE99DF32764C633CAE8A79EDC68490"];
        }else{
            $data[$this->segment]->company_name2 =  "";
        }

        $tmp_tantou_job2="";
        $tantou_jobs2 = array();
        if(!empty($resume["Item"]["Resume.U_910B68CCF4A2F064E0059737A883C0"])){
            $tmp_tantou_job2 = $resume["Item"]["Resume.U_910B68CCF4A2F064E0059737A883C0"];
        }
        if(!empty($tmp_tantou_job2)){
            foreach($tmp_tantou_job2 as $k=>$v){
                $tantou_jobs2[] = $v["Option.P_Id"];
            }
        }
        $data[$this->segment]->tantou_job2= $tantou_jobs2;

        if(!empty($resume["Item"]["Resume.U_7CCBD503A17CBD06078DB0907BEC9B"])){
            $start_date2 = $resume["Item"]["Resume.U_7CCBD503A17CBD06078DB0907BEC9B"];
            $data[$this->segment]->start_year2 =  substr($start_date2,0,4);
            $data[$this->segment]->start_month2 =  substr($start_date2,5,2);
        }else{
            $data[$this->segment]->start_year2 =  "";
            $data[$this->segment]->start_month2 =  "";
        }

        if(!empty($resume["Item"]["Resume.U_FD0444A71EB7C820DC1455C4358EE8"])){
            $end_date2 = $resume["Item"]["Resume.U_FD0444A71EB7C820DC1455C4358EE8"];
            $data[$this->segment]->end_year2 =  substr($end_date2,0,4);
            $data[$this->segment]->end_month2 =  substr($end_date2,5,2);
        }else{
            $data[$this->segment]->end_year2 =  "";
            $data[$this->segment]->end_month2 =  "";
        }

        //直近の会社３
        if(!empty($resume["Item"]["Resume.U_C12A1CD0372B2FECA9055E0302F59D"])){
            $data[$this->segment]->company_name3 = $resume["Item"]["Resume.U_C12A1CD0372B2FECA9055E0302F59D"];
        }else{
            $data[$this->segment]->company_name3 =  "";
        }

        $tmp_tantou_job3="";
        $tantou_jobs3 = array();
        if(!empty($resume["Item"]["Resume.U_7245CA44510F223C359FE2D7BC66DB"])){
            $tmp_tantou_job3 = $resume["Item"]["Resume.U_7245CA44510F223C359FE2D7BC66DB"];
        }
        if(!empty($tmp_tantou_job3)){
            foreach($tmp_tantou_job3 as $k=>$v){
                $tantou_jobs3[] = $v["Option.P_Id"];
            }
        }
        $data[$this->segment]->tantou_job3= $tantou_jobs3;

        if(!empty($resume["Item"]["Resume.U_F65492E72A71583EE72F032C73885A"])){
            $start_date3 = $resume["Item"]["Resume.U_F65492E72A71583EE72F032C73885A"];
            $data[$this->segment]->start_year3 =  substr($start_date3,0,4);
            $data[$this->segment]->start_month3 =  substr($start_date3,5,2);
        }else{
            $data[$this->segment]->start_year3 =  "";
            $data[$this->segment]->start_month3 =  "";
        }

        if(!empty($resume["Item"]["Resume.U_F6662BA5A33D671A4516974181F5F0"])){
            $end_date3 = $resume["Item"]["Resume.U_F6662BA5A33D671A4516974181F5F0"];
            $data[$this->segment]->end_year3 =  substr($end_date3,0,4);
            $data[$this->segment]->end_month3 =  substr($end_date3,5,2);
        }else{
            $data[$this->segment]->end_year3 =  "";
            $data[$this->segment]->end_month3 =  "";
        }

        //現在の年収
        $tmpearnings = $resume["Item"]["Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504"];
        if(!empty($tmpearnings)){
            foreach($tmpearnings as $k=>$v){
                $data[$this->segment]->earnings = $v["Option.P_Id"];
            }
        }else{
            $data[$this->segment]->earnings = "";
        }

        return $data;
    }

    //////////////////////////////////////////////////////////
    //バリデーション
    //////////////////////////////////////////////////////////
    private function setNewPasswordValidation($validation=null)
    {
        $errors ="";
        $this->form_validation->set_rules("password", "パスワード","required|min_length[8]|max_length[20]");
        $this->form_validation->set_rules("password_check", "パスワード(確認用）","required|min_length[8]|max_length[20]");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }
}
