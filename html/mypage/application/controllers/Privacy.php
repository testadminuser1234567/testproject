<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Privacy extends MY_Controller {
 
    function __construct()
    {
        parent::__construct();
    }
 
    /**
     * home
     */
    public function home() {
 
        $data = array();
 
        $data["render"] = "privacy/home";
	$data["current"] = "privacy";
        $this->load->view("template_nologin", $data);
    }
}