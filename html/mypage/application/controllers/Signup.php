<?php defined('BASEPATH') OR exit('No direct script access allowed');

//////////////////////////////////////////////////////////////////////////////
// HRBC既存登録者確認
//////////////////////////////////////////////////////////////////////////////
class Signup extends MY_Controller
{

    protected $page_limit = 20;

    //タイトル
    protected $sub_title = "ユーザー新規登録";

    //segment
    protected $segment = "signup";

    //database
    protected $database = "members";

    //画像がonの時 許可ファイル
    protected $allow = 'doc|xls|ppt|pptx|pdf|docx|xlsx|html|txt|htm|jpg|jpeg';

    //SNSのURL
    protected $fbloginUrl = "";
    protected $googleUrl = "";

    //HRBC URL
    protected $hrbcUrl = "https://api-hrbc-jp.porterscloud.com/v1/";

    //Pardot URL
    protected $pardotUrl = "https://info.hurex.jp/l/578091/2019-10-07/bjkl5l?";

    //転職相談会用
    protected $soudan = array("j","g","s");
    protected $join = "j";
    protected $gw = "g";
    protected $silver = "s";
    protected $join_text = "Ｕ・Ｉターン登録通知";
//    protected $gw_text = "長期休暇相談会登録通知";
    protected $gw_text = "年末年始Uターン&地元転職相談会登録通知";
    //以下の文言は使わない（今後使うかも）
    //protected $silver_text = "【シルバーウィーク相談会 登録通知】";
    protected $job_text = "求人問合";

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('form_validation', 'encrypt'));
        $this->load->helper(array('url'));
    }

    // redirect if needed, otherwise display the user list
    function regist()
    {
        $this->index();
    }

    //////////////////////////////////////////////////////////
    //新規登録
    //////////////////////////////////////////////////////////
    function index()
    {
        $error = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        //パラメータチェック
        // p=jは参加日用
        // p=gはGW
        // p=sはシルバー
        // p=数字はジョブ登録用
        $get = $this->input->get();
        $param="";
        if(!empty($get)){
            if(!empty($get["p"])){
                $param = "p|" . $get["p"];
            }
        }

        $data["render"] = $this->segment."/regist";
        $data["clean"] = $this->clean;
        $data["msg"] = $error;
        $data["param"] = $param;
        $data["segment"] = $this->segment;

        $this->load->view("template_nologin", $data);
    }

    //////////////////////////////////////////////////////////
    //メール認証
    //////////////////////////////////////////////////////////
    function send_mail()
    {
        $posts = $this->input->post();

        if(empty($posts)){
            redirect($this->segment . "/regist/");
        }

        $email = $posts["email"];
        $email = str_replace(array(" ", "　"), "", $email);
        $param = $posts["parameter"];

        //バリデーション実行
        $error = "";
        $error = $this->setSendmailValidation();

        //STEP1の登録済みチェック
        $email = trim($email);
        $hash_email = hash_hmac("sha256", $email, Salt);
        $user_info = $this->db->get_where($this->database, array('hash_email' => $hash_email, 'activate'=>0, 'status'=>2, 'del'=>0))->row();
        //$user_info = $this->db->get_where($this->database, array('hash_email' => $hash_email, 'hrbc_flg'=>0, 'activate'=>0, 'status'=>2, 'del'=>0))->row();

        //STEP1登録済みの場合はSTEP2に飛ばす
        if(!empty($user_info->id)){
            //data設定
            $this->session->set_userdata('email', $posts["email"]);
            $this->session->set_userdata('param', $user_info->parameter);

            //newフラグ取得
            $addurl="";
            $new_flg=0;
            if(!empty($posts["email"])){
                $tmp_mail = trim($posts["email"]);
                $hash_email = hash_hmac("sha256", $tmp_mail, Salt);
                $ret = $this->db->get_where($this->database, array('hash_email' => $hash_email, 'del'=>0))->row();
                $new_flg = $ret->new_flg;
            }
            if($new_flg==1){
                $addurl = "?rt=1";
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////
            // Pardotに連携
            //////////////////////////////////////////////////////////////////////////////////////////////////
            $param_pardot = "";
            $param_pardot .= "email=" . urlencode($email);

            $url = $this->pardotUrl . $param_pardot; // リクエストするURLとパラメータ

            // curlの処理を始める合図
            $curl = curl_init($url);

            // リクエストのオプションをセットしていく
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); // メソッド指定
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 証明書の検証を行わない
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // レスポンスを文字列で受け取る

            // レスポンスを変数に入れる
            $response = curl_exec($curl);
            $info    = curl_getinfo($curl);
            $errorNo = curl_errno($curl);

            //暗号化のivを設定
            $iv = $this->mail_crypt_iv($email);
            $crypt_mail = $this->mail_crypt($email, $iv);

            $base64_Iv = base64_encode($iv);
            $base64_CryptTarget = base64_encode($crypt_mail);

            // OK以外はエラーなので空白配列を返す
            if (!empty($response)) {
                // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                $subject = "【pardot連携1】  errorNo:" . $errorNo;
                $errmsg = $email . " : " . $base64_CryptTarget . "\n" . "マイページメールアドレス登録" . "\n" . $response;
                $message = $errmsg;
                $this->errMail($subject, $message);
            }

            // OK以外はエラーなので空白配列を返す
            if ($errorNo !== CURLE_OK) {
                // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                $subject = "【pardot連携2】  errorNo:" . $errorNo;
                $errmsg = $email . " : " . $base64_CryptTarget . "\n" . "マイページメールアドレス登録";
                $message = $errmsg;
                $this->errMail($subject, $message);
            }

            // 200以外のステータスコードは失敗とみなし空配列を返す
            if ($info['http_code'] !== 200) {
                $subject = "【pardot連携3】  errorNo:" . $errorNo;
                $errmsg = $email . " : " . $base64_CryptTarget . "\n" . "マイページメールアドレス登録";
                $message = $errmsg;
                $this->errMail($subject, $message);
            }

            // curlの処理を終了
            curl_close($curl);
            //////////////////////////////////////////////////////////////////////////////////////////////////
            // Pardotに連携ここまで
            //////////////////////////////////////////////////////////////////////////////////////////////////

            redirect($this->segment . "/step2/".$addurl);

        }else{
            $sameEmail="";
            $tmp_mail = trim($posts["email"]);
            $hash_email = hash_hmac("sha256", $tmp_mail, Salt);
            $sameEmail = $this->db->get_where($this->database, array('hash_email' => $hash_email, 'del'=>0))->row();

            if(empty($error)){

                //////////////////////////////////////////////////////////////////////////
                // HUREXDBに登録済 CPIのDBで確認
                //////////////////////////////////////////////////////////////////////////
                if(!empty($sameEmail->hrbc_person_id) && !empty($sameEmail->hrbc_resume_id) && !empty($sameEmail->email) && !empty($sameEmail->activate) && !empty($sameEmail->hrbc_flg) && $sameEmail->status == 3) {
                    $error .= "<p>会員情報は登録済みです。</p>";

                    //////////////////////////////////////////////////////////////////////////////////////////////////
                    // Pardotに連携
                    //////////////////////////////////////////////////////////////////////////////////////////////////
                    $email = $posts["email"];

                    $param_pardot = "";
                    $param_pardot .= "email=" . urlencode($email);

                    $url = $this->pardotUrl . $param_pardot; // リクエストするURLとパラメータ

                    // curlの処理を始める合図
                    $curl = curl_init($url);

                    // リクエストのオプションをセットしていく
                    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); // メソッド指定
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 証明書の検証を行わない
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // レスポンスを文字列で受け取る

                    // レスポンスを変数に入れる
                    $response = curl_exec($curl);
                    $info    = curl_getinfo($curl);
                    $errorNo = curl_errno($curl);

                    //暗号化のivを設定
                    $iv = $this->mail_crypt_iv($email);
                    $crypt_mail = $this->mail_crypt($email, $iv);

                    $base64_Iv = base64_encode($iv);
                    $base64_CryptTarget = base64_encode($crypt_mail);

                    // OK以外はエラーなので空白配列を返す
                    if (!empty($response)) {
                        // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                        // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                        $subject = "【pardot連携1】  errorNo:" . $errorNo;
                        $errmsg = $email . " : " . $base64_CryptTarget . "\n" . "マイページメールアドレス登録" . "\n" . $response;
                        $message = $errmsg;
                        $this->errMail($subject, $message);
                    }

                    // OK以外はエラーなので空白配列を返す
                    if ($errorNo !== CURLE_OK) {
                        // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                        // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                        $subject = "【pardot連携2】  errorNo:" . $errorNo;
                        $errmsg = $email . " : " . $base64_CryptTarget . "\n" . "マイページメールアドレス登録";
                        $message = $errmsg;
                        $this->errMail($subject, $message);
                    }

                    // 200以外のステータスコードは失敗とみなし空配列を返す
                    if ($info['http_code'] !== 200) {
                        $subject = "【pardot連携3】  errorNo:" . $errorNo;
                        $errmsg = $email . " : " . $base64_CryptTarget . "\n" . "マイページメールアドレス登録";
                        $message = $errmsg;
                        $this->errMail($subject, $message);
                    }

                    // curlの処理を終了
                    curl_close($curl);
                    //////////////////////////////////////////////////////////////////////////////////////////////////
                    // Pardotに連携ここまで
                    //////////////////////////////////////////////////////////////////////////////////////////////////

                    //data設定
                    $data[$this->segment] =  (object) $posts;
                    $data["render"] = $this->segment."/already";
                    $data["clean"] = $this->clean;
                    $data["msg"] = $error;
                    $data["param"] = $param;
                    $data["segment"] = $this->segment;

                    $this->load->view("template_nologin", $data);

                    //////////////////////////////////////////////////////////////////////////
                    // 未登録
                    //////////////////////////////////////////////////////////////////////////
                }else {
                    ///////////////////////////////////////////////////////////////////////////
                    //Person ID を取得　HRBCからメールを取得して登録済みか確認
                    ///////////////////////////////////////////////////////////////////////////
                    $result = $this->getHrbcMaster();

                    /*
                    $data = http_build_query(array('partition' => $result["partition"], 'count' => 1, 'start' => 0, 'condition' => 'Person.P_Mail:full=' . $email, 'field' => 'Person.P_Id', 'order' => 'Person.P_Id:asc'));
                    $url = "https://api-hrbc-jp.porterscloud.com/v1/candidate?" . $data;
                    $opts = array(
                        'http' => array(
                            'method' => 'GET',
                            'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                                . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                        )
                    );
                    $context = stream_context_create($opts);

                    if ($xml = @file_get_contents($url, false, $context)) {
                        //ここにデータ取得が成功した時の処理
                    } else {
                        //エラー処理
                        if (count($http_response_header) > 0) {
                            //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                            $c_code = explode(' ', $http_response_header[0]);  //「$c_code[1]」にステータスコードの数字のみが入る

                            //エラーの判別
                            switch ($c_code[1]) {
                                //404エラーの場合
                                case 404:
                                    $this->session->set_flashdata(array("msg" => "不正な処理が行われました。Err:404 signup regist"));
                                    redirect($this->segment . "/regist/");
                                    break;

                                //500エラーの場合
                                case 500:
                                    $this->session->set_flashdata(array("msg" => "サーバでエラーが発生しました。Err:500  signup regist"));
                                    redirect($this->segment . "/regist/");
                                    break;

                                //その他のエラーの場合
                                default:
                                    $this->session->set_flashdata(array("msg" => "不正な処理が行われました。Err:other  signup regist"));
                                    redirect($this->segment . "/regist/");
                            }
                        } else {
                            //タイムアウトの場合 or 存在しないドメインだった場合
                            $this->session->set_flashdata(array("msg" => "不正な処理が行われました。Err:timeout  signup regist"));
                            redirect($this->segment . "/regist/");
                        }
                    }
                    $xml = simplexml_load_string($xml);
                    */
                    $url = $this->hrbcUrl .  "candidate?partition=". $result["partition"] . "&count=1&start=0&condition=Person.P_Mail:full=".$email."&field=Person.P_Id&order=Person.P_Id:asc";
                    $options = array
                    (
                        CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING       => "UTF-8",
                        CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
                        CURLINFO_HEADER_OUT    => true,
                    );

                    $ch = curl_init();
                    curl_setopt_array($ch, $options);
                    $res = curl_exec($ch);
                    $info = curl_getinfo($ch);
                    $xml = simplexml_load_string($res);


                    if ($xml->Code != 0) {
                        $pErr = $xml->Code;

                        $subject = "【code:signup1】" . $pErr;
                        $message = $pErr;
                        $this->errMail($subject, $message);

                        $this->session->set_flashdata(array("msg" => "エラーが発生しました。 <br />コード：" . $pErr));
                        redirect("err/error_404/");
                        //print("エラーが発生しました。<br />コード：" . $pErr);
                        //exit;
                    }

                    //HRBCから帰ってきたデータ件数　1件の対応用に取得
                    $cnt = $xml->attributes()->Count;

                    //データ設定
                    $json = json_encode($xml);
                    $tmp = json_decode($json, true);
                    $person_id = "";
                    $counter = 0;
                    if ($cnt == 0) {
                    } else if ($cnt == 1) {
                        $person_id = $tmp['Item']['Person.P_Id'];
                    } else {
                        foreach ($tmp['Item'] as $k => $v) {
                            $person_id = $v['Person.P_Id'];
                            $counter++;
                            if ($cnt > 0) break;
                        }
                    }
                    $resume_id = "";
                    if ($person_id) {
                        ///////////////////////////////////////////////////////////////////////////
                        //Resume ID を取得
                        ///////////////////////////////////////////////////////////////////////////
                        $result = $this->getHrbcMaster();
                        /*
                        //                $data = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.U_774DF284856EFCC9E450FF39572729:full=' . $email, 'field'=>'Resume.P_Id', 'order'=>'Resume.P_Id:asc'));
                                                $data = http_build_query(array('partition' => $result["partition"], 'count' => 1, 'start' => 0, 'condition' => 'Resume.P_Candidate:eq=' . $person_id, 'field' => 'Resume.P_Id,Resume.P_ExpectJobCategory,Resume.P_ExpectArea,Resume.U_D16C6D4F1B6C32B4AC53819D88043F,Resume.U_CE0C8801E69FA2E26E349B09856C0C', 'order' => 'Resume.P_Id:asc'));
                                                $url = "https://api-hrbc-jp.porterscloud.com/v1/resume?" . $data;
                                                $opts = array(
                                                    'http' => array(
                                                        'method' => 'GET',
                                                        'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                                                            . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                                                    )
                                                );
                                                $context = stream_context_create($opts);

                                                if ($xml = @file_get_contents($url, false, $context)) {
                                                    //ここにデータ取得が成功した時の処理
                                                } else {
                                                    //エラー処理
                                                    if (count($http_response_header) > 0) {
                                                        //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                                                        $c_code = explode(' ', $http_response_header[0]);  //「$c_code[1]」にステータスコードの数字のみが入る

                                                        //エラーの判別
                                                        switch ($c_code[1]) {
                                                            //404エラーの場合
                                                            case 404:
                                                                $this->session->set_flashdata(array("msg" => "不正な処理が行われました。Err:404 signup regist2"));
                                                                redirect($this->segment . "/regist/");
                                                                break;

                                                            //500エラーの場合
                                                            case 500:
                                                                $this->session->set_flashdata(array("msg" => "サーバでエラーが発生しました。Err:500  signup regist2"));
                                                                redirect($this->segment . "/regist/");
                                                                break;

                                                            //その他のエラーの場合
                                                            default:
                                                                $this->session->set_flashdata(array("msg" => "不正な処理が行われました。Err:other  signup regist2"));
                                                                redirect($this->segment . "/regist/");
                                                        }
                                                    } else {
                                                        //タイムアウトの場合 or 存在しないドメインだった場合
                                                        $this->session->set_flashdata(array("msg" => "不正な処理が行われました。Err:timeout  signup regist2"));
                                                        redirect($this->segment . "/regist/");
                                                    }
                                                }
                                                $xml = simplexml_load_string($xml);
                        */
                        $url = $this->hrbcUrl .  "resume?partition=". $result["partition"] . "&count=1&start=0&condition=Resume.P_Candidate:eq=".$person_id."&field=Resume.P_Id,Resume.P_ExpectJobCategory,Resume.P_ExpectArea,Resume.U_D16C6D4F1B6C32B4AC53819D88043F,Resume.U_CE0C8801E69FA2E26E349B09856C0C&order=Resume.P_Id:asc";
                        $options = array
                        (
                            CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING       => "UTF-8",
                            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
                            CURLINFO_HEADER_OUT    => true,
                        );

                        $ch = curl_init();
                        curl_setopt_array($ch, $options);
                        $res = curl_exec($ch);
                        $info = curl_getinfo($ch);
                        $xml = simplexml_load_string($res);

                        if ($xml->Code != 0) {
                            $pErr = $xml->Code;
                            $subject = "【code:signup2】" . $pErr;
                            $message = $pErr;
                            $this->errMail($subject, $message);

                            $this->session->set_flashdata(array("msg" => "エラーが発生しました。 <br />コード：" . $pErr));
                            redirect("err/error_404/");

                            //exit;
                        }

                        //HRBCから帰ってきたデータ件数　1件の対応用に取得
                        $cnt = $xml->attributes()->Count;

                        //データ設定
                        $json = json_encode($xml);
                        $tmp = json_decode($json, true);
                        $resume_data = $tmp;
                        $resume_id = "";
                        $counter = 0;
                        if ($cnt == 0) {
                        } else if ($cnt == 1) {
                            $resume_id = $tmp['Item']['Resume.P_Id'];
                        } else {
                            foreach ($tmp['Item'] as $k => $v) {
                                $resume_id = $v['Resume.P_Id'];
                                $counter++;
                                if ($cnt > 0) break;
                            }
                        }
                    }

                    //一旦forget_passwordを設定
                    $forget_pass="";
                    if(!empty($sameEmail->forget_password)){
                        $forget_pass = $sameEmail->forget_password;
                    }

                    //hurexDBだけに登録しているメアドを削除 二重登録の防止
                    if (!empty($sameEmail->email) && !empty($sameEmail->iv)) {
                        $this->db->delete($this->database, array("hash_email" => $sameEmail->hash_email, 'del' => 0));
                    }

                    //暗号化のivを設定
                    $iv = $this->mail_crypt_iv($email);
                    $crypt_mail = $this->mail_crypt($email, $iv);

                    $base64_Iv = base64_encode($iv);
                    $base64_CryptTarget = base64_encode($crypt_mail);

                    $esc_param = "";
                    if ($param) {
                        $esc_param = $this->db->escape($param);
                    }

                    //////////////////////////////////////////////////////////////////////////
                    // HRBC登録済みならHUREXのDBに登録
                    //////////////////////////////////////////////////////////////////////////
                    $ip = $_SERVER["REMOTE_ADDR"];
                    if ($person_id && $resume_id) {
                        //////////////////////////////////////////////////////////////////////////////////////////////////
                        // Pardotに連携
                        //////////////////////////////////////////////////////////////////////////////////////////////////
                        $email = $posts["email"];

                        $param_pardot = "";
                        $param_pardot .= "email=" . urlencode($email);

                        $url = $this->pardotUrl . $param_pardot; // リクエストするURLとパラメータ

                        // curlの処理を始める合図
                        $curl = curl_init($url);

                        // リクエストのオプションをセットしていく
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); // メソッド指定
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 証明書の検証を行わない
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // レスポンスを文字列で受け取る

                        // レスポンスを変数に入れる
                        $response = curl_exec($curl);
                        $info    = curl_getinfo($curl);
                        $errorNo = curl_errno($curl);

                        //暗号化のivを設定
                        $iv = $this->mail_crypt_iv($email);
                        $crypt_mail = $this->mail_crypt($email, $iv);

                        $base64_Iv = base64_encode($iv);
                        $base64_CryptTarget = base64_encode($crypt_mail);

                        // OK以外はエラーなので空白配列を返す
                        if (!empty($response)) {
                            // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                            // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                            $subject = "【pardot連携1】  errorNo:" . $errorNo;
                            $errmsg = $email . " : " . $base64_CryptTarget . "\n" . "マイページメールアドレス登録" . "\n" . $response;
                            $message = $errmsg;
                            $this->errMail($subject, $message);
                        }

                        // OK以外はエラーなので空白配列を返す
                        if ($errorNo !== CURLE_OK) {
                            // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                            // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                            $subject = "【pardot連携2】  errorNo:" . $errorNo;
                            $errmsg = $email . " : " . $base64_CryptTarget . "\n" . "マイページメールアドレス登録";
                            $message = $errmsg;
                            $this->errMail($subject, $message);
                        }

                        // 200以外のステータスコードは失敗とみなし空配列を返す
                        if ($info['http_code'] !== 200) {
                            $subject = "【pardot連携3】  errorNo:" . $errorNo;
                            $errmsg = $email . " : " . $base64_CryptTarget . "\n" . "マイページメールアドレス登録";
                            $message = $errmsg;
                            $this->errMail($subject, $message);
                        }

                        // curlの処理を終了
                        curl_close($curl);
                        //////////////////////////////////////////////////////////////////////////////////////////////////
                        // Pardotに連携ここまで
                        //////////////////////////////////////////////////////////////////////////////////////////////////

                        $data = array(
                            'hrbc_person_id' => $person_id,
                            'hrbc_resume_id' => $resume_id,
                            'email' => $base64_CryptTarget,
                            'iv' => $base64_Iv,
                            'hash_email' => $hash_email,
                            'hrbc_flg' => 1,
                            'status' => 1,
                            'parameter' => $esc_param,
                            'ip' => $ip
                        );

                        $ret = $this->db->insert($this->database, $data);
                        if (empty($ret)) {
                            $subject = "【code:signup DB error】";
                            $message = $data;
                            $this->errMail($subject, $message);

                            $this->session->set_flashdata(array("msg" => "エラーが発生しました。 <br />コード：" . $pErr));
                            redirect("err/error_404/");
                        }

                        if(empty($forget_pass)){
                            $token = $this->generatePassword();
                        }else{
                            $token = $forget_pass;
                        }
                        //DBにforget_passwordトークン登録
                        $data = array(
                            'forget_password' => $token,
                            'forget_password_limit' => strtotime("tomorrow")
                        );

                        $this->db->where('email', $base64_CryptTarget);
                        $ret = $this->db->update($this->database, $data);

                        $url = base_url() . $this->segment . "/step1/?re=" . $token . "&utm_source=myPageAuth&utm_medium=email&argument=vgmWMMby&dmai=mypageauth";

                        $to = $posts["email"];
                        $this->signupMail($to, $url);

                        $addurl = "?hrbc=1&email=".$email;
                        if($param){
                            $addurl .= "&p=".$param;
                        }
                        redirect($this->segment . "/signup_complete/".$addurl);
                    } else {
                        //////////////////////////////////////////////////////////////////////////
                        // HRBC登録していないかレジュメが不足している場合（新規登録扱い）
                        //////////////////////////////////////////////////////////////////////////
                        $ip = $_SERVER["REMOTE_ADDR"];
                        $data = array(
                            'email' => $base64_CryptTarget,
                            'iv' => $base64_Iv,
                            'parameter' => $param,
                            'status' => 1,
                            'new_flg' => 1,
                            'hash_email' => $hash_email,
                            'ip' => $ip
                        );

                        $ret = $this->db->insert($this->database, $data);
                        if (empty($ret)) {
                            $subject = "【code:signup DB error】";
                            $message = $data;
                            $this->errMail($subject, $message);

                            $this->session->set_flashdata(array("msg" => "エラーが発生しました。 <br />コード：" . $pErr));
                            redirect("err/error_404/");
                        }

                        if(empty($forget_pass)){
                            $token = $this->generatePassword();
                        }else{
                            $token = $forget_pass;
                        }
                        //DBにforget_passwordトークン登録
                        $data = array(
                            'forget_password' => $token,
                            'forget_password_limit' => strtotime("tomorrow")
                        );

                        $this->db->where('email', $base64_CryptTarget);
                        $ret = $this->db->update($this->database, $data);

                        //////////////////////////////////////////////////////////////////////////////////////////////////
                        // Pardotに連携
                        //////////////////////////////////////////////////////////////////////////////////////////////////
                        $param_pardot = "";
                        $param_pardot .= "email=" . urlencode($email);

                        $url = $this->pardotUrl . $param_pardot; // リクエストするURLとパラメータ

                        // curlの処理を始める合図
                        $curl = curl_init($url);

                        // リクエストのオプションをセットしていく
                        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); // メソッド指定
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 証明書の検証を行わない
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // レスポンスを文字列で受け取る

                        // レスポンスを変数に入れる
                        $response = curl_exec($curl);
                        $info    = curl_getinfo($curl);
                        $errorNo = curl_errno($curl);

                        // OK以外はエラーなので空白配列を返す
                        if (!empty($response)) {
                            // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                            // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                            $subject = "【pardot連携1】  errorNo:" . $errorNo;
                            $errmsg = $email . " : " . $base64_CryptTarget . "\n" . "マイページメールアドレス登録" . "\n" . $response;
                            $message = $errmsg;
                            $this->errMail($subject, $message);
                        }

                        // OK以外はエラーなので空白配列を返す
                        if ($errorNo !== CURLE_OK) {
                            // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                            // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                            $subject = "【pardot連携2】  errorNo:" . $errorNo;
                            $errmsg = $email . " : " . $base64_CryptTarget . "\n" . "マイページメールアドレス登録";
                            $message = $errmsg;
                            $this->errMail($subject, $message);
                        }

                        // 200以外のステータスコードは失敗とみなし空配列を返す
                        if ($info['http_code'] !== 200) {
                            $subject = "【pardot連携3】  errorNo:" . $errorNo;
                            $errmsg = $email . " : " . $base64_CryptTarget . "\n" . "マイページメールアドレス登録";
                            $message = $errmsg;
                            $this->errMail($subject, $message);
                        }

                        // curlの処理を終了
                        curl_close($curl);
                        //////////////////////////////////////////////////////////////////////////////////////////////////
                        // Pardotに連携ここまで
                        //////////////////////////////////////////////////////////////////////////////////////////////////


                        $getEmail = urlencode($email);
                        $url = base_url() . $this->segment . "/step1/?re=" . $token . "&rt=1&email=" . $getEmail;

                        //20191024認証処理追加
                        $this->signupMail($email, $url);

                        $addurl = "?email=".$email;
                        if($param){
                            $addurl .= "&p=".$param;
                        }
                        redirect($this->segment . "/signup_complete/".$addurl);
                    }
                }
            }else{
                //data設定
                $data[$this->segment] =  (object) $posts;
                $data["render"] = $this->segment."/regist";
                $data["clean"] = $this->clean;
                $data["msg"] = $error;
                $data["param"] = $param;
                $data["segment"] = $this->segment;

                $this->load->view("template_nologin", $data);
            }
        }
    }

    //////////////////////////////////////////////////////////
    //登録完了画面（リダイレクト）
    //////////////////////////////////////////////////////////
    function signup_complete()
    {
        $hrbc = $this->input->get('hrbc', TRUE);
        $param = $this->input->get('p', TRUE);

        if($hrbc==1){
            //data設定
            $data["render"] = $this->segment."/signup_complete";
            $data["clean"] = $this->clean;
            $data["param"] = $param;
            $data["segment"] = $this->segment;

            $this->load->view("template_nologin", $data);
        }else{
            //data設定
            $data["render"] = $this->segment."/signup_new_complete";
            $data["clean"] = $this->clean;
            $data["param"] = $param;
            $data["segment"] = $this->segment;

            $this->load->view("template_nologin", $data);
        }
    }

    //////////////////////////////////////////////////////////
    //基本情報登録 STEP1
    //////////////////////////////////////////////////////////
    function step1()
    {
        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $code = $this->input->get('re', TRUE);
        $data["code"] = $code;
        $param="";
        if($code){
            $session_data[$this->segment] = $this->db->get_where($this->database, array('forget_password' => $code, 'del'=>0))->row();
            if(empty($session_data[$this->segment])){
                redirect($this->segment . "/regist/");
            }
            $data[$this->segment] = $session_data[$this->segment];
            $param = $data[$this->segment]->parameter;
        }else{
            $posts = $this->input->post();
            if(!empty($posts["param"])){
                $param = $posts["param"];
            }
            $posts["password"] = $this->session->userdata("pass");
            $data[$this->segment] = (object) $posts;
        }

        if(empty($data[$this->segment])){
            redirect($this->segment."/regist/");
        }

        if(empty($session_data)){
            $session_data[$this->segment] = $this->session->userdata("signup");
        }
        $this->session->set_userdata($session_data);

        //対象データがなければエラー
        if(empty($session_data[$this->segment])){
            redirect($this->segment . "/regist/");
        }else{
            //hrbc登録済みか確認
            $hrbc_flg = $session_data["signup"]->hrbc_flg;

            //戻るボタンの確認
            $modoru="";
            if(!empty($posts["mode"])){
                $modoru = $posts["mode"];
            }

            ///////////////////////////////////////////////////////////
            // 戻るボタンの処理
            ///////////////////////////////////////////////////////////
            if($modoru == "return"){
                $split_param=array("","");
                if(!empty($posts["param"])){
                    $split_param = str_replace("'","", $posts["param"]);
                    $split_param = explode("|",$split_param);
                }

                //hrbcフラグ取得
                $ret = $this->db->get_where($this->database, array('email' => $posts["hash"], 'del'=>0))->row();
                $hrbc_flg = $ret->hrbc_flg;

                //登録済みでパラメータ（UIターンやGW転職相談会など）なし
                if($hrbc_flg==1 && empty($posts["param"])) {
                    $data["render"] = $this->segment . "/step1_password";
                    //UIターン
                }else if($hrbc_flg==1  && in_array($split_param[1],$this->soudan)){
                    $data["render"] = $this->segment."/step1_password_ui";
                    //jobの応募
                }else if($hrbc_flg==1 && $split_param[0]=="p" && is_numeric($split_param[1])){
                    $data["render"] = $this->segment."/step1_password";
                    //UIターンの新規登録
                }else if(empty($hrbc_flg)  && in_array($split_param[1],$this->soudan)){
                    $data["render"] = $this->segment."/step1_ui";
                    //上記以外の場合(新規登録、JOB応募など）
                }else{
                    $data["render"] = $this->segment."/step1";
                }

                $data[$this->segment] =  (object) $posts;

                $data["clean"] = $this->clean;
                $data["msg"] = "";
                $data["param"] = $posts["param"];
                $data["hrbc_flg"] = $hrbc_flg;
                $data["segment"] = $this->segment;

                $this->load->view("template_nologin", $data);
            }else{
                ////////////////////////////////////////////////////////////
                // メール認証
                ////////////////////////////////////////////////////////////

                //HRBC
                $result = $this->getHrbcMaster();

                if($session_data["signup"]->hrbc_person_id){
                    ///////////////////////////////////////////////////
                    //person取得
                    ///////////////////////////////////////////////////
                    /*
                    $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Person.P_Id:eq=' . $data["signup"]->hrbc_person_id));
                    $url = "https://api-hrbc-jp.porterscloud.com/v1/candidate?" . $url;
                    $opts = array(
                        'http'=>array(
                            'method' => 'GET',
                            'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                                . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                        )
                    );
                    $context = stream_context_create($opts);

                    if($xml = @file_get_contents($url, false, $context)){
                        //ここにデータ取得が成功した時の処理
                    }else{
                        //エラー処理
                        if(count($http_response_header) > 0){
                            //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                            $c_code = explode(' ', $http_response_header[0]);  //「$c_code[1]」にステータスコードの数字のみが入る

                            //エラーの判別
                            switch($c_code[1]){
                                //404エラーの場合
                                case 404:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist"));
                                    redirect($this->segment."/regist/");
                                    break;

                                //500エラーの場合
                                case 500:
                                    $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist"));
                                    redirect($this->segment."/regist/");
                                    break;

                                //その他のエラーの場合
                                default:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist"));
                                    redirect($this->segment."/regist/");
                            }
                        }else{
                            //タイムアウトの場合 or 存在しないドメインだった場合
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist"));
                            redirect($this->segment."/regist/");
                        }
                    }
                    $xml = simplexml_load_string($xml);
                    */
                    $url = $this->hrbcUrl .  "candidate?partition=". $result["partition"] . "&count=1&start=0&condition=Person.P_Id:eq=".$data["signup"]->hrbc_person_id;
                    $options = array
                    (
                        CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING       => "UTF-8",
                        CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
                        CURLINFO_HEADER_OUT    => true,
                    );

                    $ch = curl_init();
                    curl_setopt_array($ch, $options);
                    $res = curl_exec($ch);
                    $info = curl_getinfo($ch);
                    $xml = simplexml_load_string($res);

                    if($xml->Code!=0){
                        $pErr=$xml->Code;

                        $subject = "【user基本情報編集に失敗　code:user01】 person:" . $data["signup"]->hrbc_person_id . " resume:" . $data["signup"]->hrbc_resume_id;
                        $message = "user基本情報編集に失敗しました。 person:" . $data["signup"]->hrbc_person_id . " resume:" . $data["signup"]->hrbc_resume_id . " error:" . $pErr;
                        $this->errMail($subject, $message);

                        $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
                        redirect("err/error_404/");
                        //exit;
                    }

                    //データ設定
                    $json = json_encode($xml);
                    $person = json_decode($json,true);
                }

                if($session_data["signup"]->hrbc_resume_id){
                    ////////////////////////////////////////////////////////////////
                    //candidate Idからresume取得
                    ///////////////////////////////////////////////////////////////
                    /*
                    $url = http_build_query(array('partition'=>$result["partition"], 'count'=>1 , 'start'=>0, 'condition'=>'Resume.P_Id:eq=' . $data["signup"]->hrbc_resume_id));
                    $url = "https://api-hrbc-jp.porterscloud.com/v1/resume?" . $url;
                    $opts = array(
                        'http'=>array(
                            'method' => 'GET',
                            'header' => "X-porters-hrbc-oauth-token: " . $result["token"] . "\r\n"
                                . "Content-Type'=>'application/xml; charset=UTF-8\r\n"
                        )
                    );
                    $context = stream_context_create($opts);

                    if($xml = @file_get_contents($url, false, $context)){
                        //ここにデータ取得が成功した時の処理
                    }else{
                        //エラー処理
                        if(count($http_response_header) > 0){
                            //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                            $c_code = explode(' ', $http_response_header[0]);  //「$c_code[1]」にステータスコードの数字のみが入る

                            //エラーの判別
                            switch($c_code[1]){
                                //404エラーの場合
                                case 404:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 regist2"));
                                    redirect($this->segment."/regist/");
                                    break;

                                //500エラーの場合
                                case 500:
                                    $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 regist2"));
                                    redirect($this->segment."/regist/");
                                    break;

                                //その他のエラーの場合
                                default:
                                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other regist2"));
                                    redirect($this->segment."/regist/");
                            }
                        }else{
                            //タイムアウトの場合 or 存在しないドメインだった場合
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout regist2"));
                            redirect($this->segment."/regist/");
                        }
                    }
                    $xml = simplexml_load_string($xml);
                    */
                    $url = $this->hrbcUrl .  "resume?partition=". $result["partition"] . "&count=1&start=0&condition=Resume.P_Id:eq=".$data["signup"]->hrbc_resume_id;
                    $options = array
                    (
                        CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING       => "UTF-8",
                        CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result["token"]),
                        CURLINFO_HEADER_OUT    => true,
                    );

                    $ch = curl_init();
                    curl_setopt_array($ch, $options);
                    $res = curl_exec($ch);
                    $info = curl_getinfo($ch);
                    $xml = simplexml_load_string($res);

                    if($xml->Code!=0){
                        $pErr=$xml->Code;

                        $subject = "【user基本情報編集に失敗　code:user02】 person:" . $data["signup"]->hrbc_person_id . " resume:" . $data["signup"]->hrbc_resume_id;
                        $message = "user基本情報編集に失敗しました。 person:" . $data["signup"]->hrbc_person_id . " resume:" . $data["signup"]->hrbc_resume_id . " error:" . $pErr;
                        $this->errMail($subject, $message);

                        $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $pErr));
                        redirect("err/error_404/");
                        //exit;
                    }

                    //データ設定
                    $json = json_encode($xml);
                    $resume = json_decode($json,true);
                }

                //データの整形
                $data[$this->segment] = new stdClass;

                //hurexDBのID
                $data[$this->segment]->hurexid = $session_data["signup"]->id;
                $data[$this->segment]->hash = $session_data["signup"]->hash_email;
                $data[$this->segment]->new_flg = $session_data["signup"]->new_flg;

                //氏名
                if(!empty($person["Item"]["Person.P_Name"])){
                    $data[$this->segment]->shimei = $person["Item"]["Person.P_Name"];
                }else{
                    $data[$this->segment]->shimei="";
                }

                //ふりがな
                if(!empty($resume["Item"]["Resume.U_91DBD8B96BAAD6B33B538CF2494377"])){
                    $data[$this->segment]->kana = $resume["Item"]["Resume.U_91DBD8B96BAAD6B33B538CF2494377"];
                }else{
                    $data[$this->segment]->kana = "";
                }

                //誕生日
                if(!empty($resume["Item"]["Resume.P_DateOfBirth"])){
                    $birthday = $resume["Item"]["Resume.P_DateOfBirth"];
                    list($data[$this->segment]->year, $data[$this->segment]->month, $data[$this->segment]->day) = explode('/', $birthday);
                }else{
                    $data[$this->segment]->year="";
                    $data[$this->segment]->month="";
                    $data[$this->segment]->day="";
                }

                //性別
                if(!empty($resume["Item"]["Resume.P_Gender"])){
                    $tmpgender = $resume["Item"]["Resume.P_Gender"];
                    if(!empty($tmpgender)){
                        foreach($tmpgender as $k=>$v){
                            $data[$this->segment]->sex = $v["Option.P_Name"];
                        }
                    }else{
                        $data[$this->segment]->sex = "";
                    }
                }

                //都道府県
                if(!empty($resume["Item"]["Resume.U_7F3DF7F61EEF0679AA432646777832"])){
                    $tmppref = $resume["Item"]["Resume.U_7F3DF7F61EEF0679AA432646777832"];
                    if(!empty($tmppref)){
                        foreach($tmppref as $k=>$v){
                            $data[$this->segment]->pref = $v["Option.P_Name"];
                        }
                    }else{
                        $data[$this->segment]->sex = "";
                    }
                }

                //TEL
                if(empty($person["Item"]["Person.P_Telephone"])){
                    if(!empty($person["Item"]["Person.P_Mobile"])){
                        $data[$this->segment]->tel1 = $person["Item"]["Person.P_Mobile"];
                    }else{
                        $data[$this->segment]->tel1 = "";
                    }
                }else{
                    $data[$this->segment]->tel1 = $person["Item"]["Person.P_Telephone"];
                }

                //Mail
                if(!empty($person["Item"]["Person.P_Mail"])){
                    $data[$this->segment]->mail1 = $person["Item"]["Person.P_Mail"];
                }else{
                    $iv = $session_data["signup"]->iv;
                    $mail = $session_data["signup"]->email;
                    $email = $this->mail_decrypt($iv, $mail);
                    $mailTo = trim($email);
                    $data[$this->segment]->mail1 = $mailTo;
                }

                //学歴
                if(!empty($resume["Item"]["Resume.U_673275A4DA445C9B23AC18BDD7C4DC"])){
                    $tmpschool = $resume["Item"]["Resume.U_673275A4DA445C9B23AC18BDD7C4DC"];
                    if(!empty($tmpschool)){
                        foreach($tmpschool as $k=>$v){
                            $data[$this->segment]->school_div_id = $v["Option.P_Name"];
                        }
                    }else{
                        $data[$this->segment]->school_div_id = "";
                    }
                }

                //学校名
                if(!empty($resume["Item"]["Resume.U_85DEA8FA18EF17830FD8A036EC95A4"])){
                    $tmpschool_name = $resume["Item"]["Resume.U_85DEA8FA18EF17830FD8A036EC95A4"];
                    $data[$this->segment]->school_name = $tmpschool_name;
                }else{
                    $data[$this->segment]->school_name = "";
                }

                //就業状況
                if(!empty($resume["Item"]["Resume.P_CurrentStatus"])){
                    $tmpjokyo = $resume["Item"]["Resume.P_CurrentStatus"];
                    if(!empty($tmpjokyo)){
                        foreach($tmpjokyo as $k=>$v){
                            $data[$this->segment]->jokyo = $v["Option.P_Name"];
                        }
                    }else{
                        $data[$this->segment]->school_div_id = "";
                    }
                }

                //経験社数
                if(!empty($resume["Item"]["Resume.P_ChangeJobsCount"])){
                    $data[$this->segment]->company_number = $resume["Item"]["Resume.P_ChangeJobsCount"];
                }else{
                    $data[$this->segment]->company_number = "";
                }

                //コメント なし？
                /*
                if(!empty($resume["Item"]["Resume.P_Memo"])){
                    $data[$this->segment]->comment = $resume["Item"]["Resume.P_Memo"];
                }else{
                    $data[$this->segment]->comment = "";
                }
                */

                $tmp = (array) $session_data;
                if(empty($tmp)){
                    redirect($this->segment . "/regist/");
                }

                $split_param=array("","");
                if(!empty($param)){
                    $split_param = str_replace("'","", $param);
                    $split_param = explode("|",$split_param);
                }

                //登録済みでパラメータ（UIターンやGW転職相談会など）なし
                if(!empty($hrbc_flg) && empty($param)) {
                    $data["render"] = $this->segment . "/step1_password";
                    //UIターン
                }else if(!empty($hrbc_flg) && in_array($split_param[1],$this->soudan)){
                    $data["render"] = $this->segment."/step1_password_ui";
                    //jobの応募
                }else if(!empty($hrbc_flg) && $split_param[0]=="p" && is_numeric($split_param[1])){
                    $data["render"] = $this->segment."/step1_password";
                    //UIターンの新規登録
                }else if(empty($hrbc_flg)  && in_array($split_param[1],$this->soudan)){
                    $data["render"] = $this->segment."/step1_ui";
                    //上記以外の場合(新規登録、JOB応募など）
                }else{
                    $data["render"] = $this->segment."/step1";
                }

                $error="";
                $data["clean"] = $this->clean;
                $data["msg"] = $error;
                $data["hrbc_flg"] = $hrbc_flg;
                $data["param"] = $param;
                $data["segment"] = $this->segment;

                $this->load->view("template_nologin", $data);
            }
        }
    }

    //////////////////////////////////////////////////////////
    //登録情報確認 STEP2
    //////////////////////////////////////////////////////////
    function step2()
    {
        $posts = $this->input->post();

        $session_data = $this->session->userdata('email');
        $hrbc_flg = $this->session->userdata('hrbc_flg');

        //newフラグ取得
        $new_flg=0;
        if(!empty($posts["hurexid"])){
            $ret = $this->db->get_where($this->database, array('id' => $posts["hurexid"], 'del'=>0))->row();
        }else if(!empty($session_data)){
            $tmp_mail = trim($session_data);
            $hash_email = hash_hmac("sha256", $tmp_mail, Salt);
            $ret = $this->db->get_where($this->database, array('hash_email' => $hash_email, 'del'=>0))->row();
            $hrbc_flg = $ret->hrbc_flg;
        }
        $new_flg = $ret->new_flg;

        //STEP1→STEP3のルート
        if(empty($posts) && !empty($session_data)) {

            $data[$this->segment] = new stdClass;
            $data[$this->segment]->mail1 = $session_data;
            $this->session->unset_userdata('email');

            //ログイン画面から来た場合 pwdchkが1の場合はテンプレート変更
            if(empty($posts["pwdchk"])){
                if($hrbc_flg==1){
                    $posts["pwdchk"] = 1;
                }
            }

            //data設定
            $error = "";
            if(!empty($posts["pwdchk"])){
                //hrbc登録済み
                $data["render"] = $this->segment . "/step2_hrbc_complete";
            }else{
                $data["render"] = $this->segment . "/step2";
            }
            $data["new_flg"] = $new_flg;
            $data["hurexid"] = $posts["hurexid"];
            $data["clean"] = $this->clean;
            $data["sub_title"] = $this->sub_title;
            $data["msg"] = $error;
            $data["segment"] = $this->segment;

            $this->load->view("template_nologin", $data);
        }else if(!empty($posts["mode"]) && $posts["mode"]=="return"){
            //戻るの処理

            //オブジェクトへ変換
            $data[$this->segment] = (object)$posts;

            $error = "";
            if(!empty($posts["pwdchk"])){
                //hrbc登録済み
                $data["render"] = $this->segment . "/step2_hrbc_complete";
            }else{
                $data["render"] = $this->segment . "/step2";
            }
            $data["clean"] = $this->clean;
            $data["sub_title"] = $this->sub_title;
            $data["msg"] = $error;
            $data["segment"] = $this->segment;
            $data["new_flg"] = $new_flg;
            $data["hurexid"] = $posts["hurexid"];

            $this->load->view("template_nologin", $data);
        }else if(!empty($posts)){
            //バリデーション実行
            $error = "";
            $tmp_error = 0;
            $e_msg = "";

            //HRBC既存登録者は基本情報のチェック不要
            if(!empty($posts["pwdchk"])){
                $error = $this->setValidationPwdStep2();
            }else{
                $error = $this->setValidationStep2();
                if(!checkdate($posts["month"], $posts["day"], $posts["year"])) {
                    $error.="<p>日付の形式が正しくありません。</p>";
                }
            }

            //パスワードチェック
            if($posts['password'] != $posts['password_check']){
                $error.="<p>パスワードが一致しません。</p>";
            }
            if(empty($posts["password"])){
                $error.="<p>パスワードは必須フィールドです。</p>";
            }
            if (!empty($posts["password"])  && !preg_match('/\A(?=.*?[a-z])(?=.*?\d)[a-z\d]{8,20}+\z/i', $posts["password"]))
            {
                $error.="<p>パスワードは8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</p>";
            }

            //パラメーター（UIターンとかJOB)
            $param=array("","");
            if(!empty($posts["param"])){
                $param = str_replace("'","", $posts["param"]);
                $param = explode("|",$param);
            }

            $naiyo = "";
            $add="";
            if(!empty($param[1])){
                if(in_array($param[1],$this->soudan)){
                    //メール送信
                    if($param[1]=="j") {
                        $naiyo = $this->join_text;
                    }else if($param[1]=="g"){
                        $naiyo = $this->gw_text;
                    }else if($param[1]=="s"){
                        $naiyo = $this->silver_text;
                    }

                    if(empty($posts["kiboubi"])){
                        $error.="<p>ご希望の参加日を入力してください。</p>";
                    }
                }else if(is_numeric($param[1])){
                    $jobinfo = $this->getJobInfo($param[1]);
                    if(!empty($jobinfo)){
                        $naiyo = $this->job_text;
                        $add = "JOB:" . $param[1] . " / ";
                    }
                }
            }

            //オブジェクトへ変換
            $data[$this->segment] = (object)$posts;

            if($error !== ""){
                //data設定
                if(!empty($param[1]) && in_array($param[1],$this->soudan)) {
                    $data["render"] = $this->segment . "/step1_ui";
                }else if(!empty($posts["pwdchk"])){
                    $data["render"] = $this->segment . "/step1_password";
                }else{
                    $data["render"] = $this->segment . "/step1";
                }
                $data["clean"] = $this->clean;
                $data["sub_title"] = $this->sub_title;
                $data["msg"] = $error;
                $data["segment"] = $this->segment;
                $data["param"] = @$posts["param"];
                $data["new_flg"] = $new_flg;
                $data["hurexid"] = $posts["hurexid"];

                $this->load->view("template_nologin", $data);

            }else{
                //////////////////////////////////////////////////////////////////////////////////////////////////
                // Pardotに連携
                //////////////////////////////////////////////////////////////////////////////////////////////////
                $param_pardot = "";
                $param_pardot .= "email=" . urlencode($posts["mail1"]);

                $url = $this->pardotUrl . $param_pardot; // リクエストするURLとパラメータ

                // curlの処理を始める合図
                $curl = curl_init($url);

                // リクエストのオプションをセットしていく
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); // メソッド指定
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 証明書の検証を行わない
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // レスポンスを文字列で受け取る

                // レスポンスを変数に入れる
                $response = curl_exec($curl);
                $info    = curl_getinfo($curl);
                $errorNo = curl_errno($curl);

                // OK以外はエラーなので空白配列を返す
                if (!empty($response)) {
                    // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                    // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                    $subject = "【pardot連携4】  errorNo:" . $errorNo;
                    $errmsg = $posts["mail1"] . "\n" . "Step1 → Step2" . "\n" . $response;
                    $message = $errmsg;
                    $this->errMail($subject, $message);
                }

                // OK以外はエラーなので空白配列を返す
                if ($errorNo !== CURLE_OK) {
                    // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                    // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                    $subject = "【pardot連携5】  errorNo:" . $errorNo;
                    $errmsg = $posts["mail1"] . "\n" . "Step1 → Step2";
                    $message = $errmsg;
                    $this->errMail($subject, $message);
                }

                // 200以外のステータスコードは失敗とみなし空配列を返す
                if ($info['http_code'] !== 200) {
                    $subject = "【pardot連携6】  errorNo:" . $errorNo;
                    $errmsg = $posts["mail1"] . "\n" . "Step1 → Step2";
                    $message = $errmsg;
                    $this->errMail($subject, $message);
                }

                // curlの処理を終了
                curl_close($curl);
                //////////////////////////////////////////////////////////////////////////////////////////////////
                // Pardotに連携ここまで
                //////////////////////////////////////////////////////////////////////////////////////////////////

                //HRBC登録
                $result = $this->HrbcRegistStep2($posts);

                if(!empty($posts["pwdchk"])){
                    //Person と Resumeを取得
                    $person_datas = $this->getPerson($result["person"]);
                    $resume_datas = $this->getResume($result["resume"]);

                    $shimei = $person_datas["name"];
                    $kana = $person_datas["kana"];
                    $tel1=$person_datas["tel1"];
                    $mailTo = $person_datas["mail1"];
                    $birthday = $resume_datas["birthday"];
                    $now = date("Ymd");
                    $tmp_birthday = explode("/",$birthday);
                    $tmp_birthday = $tmp_birthday[0] . sprintf('%02d', $tmp_birthday[1]) . sprintf('%02d', $tmp_birthday[2]);
                    $age =  floor(($now-$tmp_birthday)/10000) . "歳";
                    $sex = $resume_datas["sex"];
                    $pref=$resume_datas["pref"];
                    $school_div_id = $resume_datas["school_div_id"];
                    $school_name = $resume_datas["school_name"];
                    $tmp_company_number = $resume_datas["company_number"]. "社";
                    $jokyo = $resume_datas["jokyo"];
                    $comment = $resume_datas["comment"];
                    $tenshoku_kiboubi = $resume_datas["tenshoku_kiboubi"];
                    $kibouarea1 = $resume_datas["kibouarea1"];
                    $kibouarea2 = $resume_datas["kibouarea2"];
                    $kibouarea3 = $resume_datas["kibouarea3"];
                    $kibouarea3 = $resume_datas["kibouarea3"];

                    $agent = $this->getUserAgent();

                    //URLの設定
                    $person_url="";
                    $resume_url="";

                    if(!empty($result["person"])){
                        $person_url = "{unwrap}Person: https://hrbc-jp.porterscloud.com/can/search/id/" . $result["person"] . "#/dv/1:" . $result["person"] . "/{/unwrap}";
                    }
                    if(!empty($result["resume"])){
                        //$resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $result["resume"] . "?menu_id=5{/unwrap}";
                        $resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $result["resume"] ."?menu_id=5#/dv/17:" . $result["resume"] ."/{/unwrap}";
                    }

                    $userData = $this->db->get_where($this->database, array('hrbc_person_id' => $result["person"], 'hrbc_resume_id' => $result["resume"], 'del'=>0))->row();

                    //既存ユーザーでUIターンはメール送信
                    if($userData->parameter=="'p|j'"){
                        //メール送信
                        $unit_url = $resume_url . "{unwrap}\n{/unwrap}";

                        $this->sendMailStep1($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $kibouarea1, $kibouarea2, $kibouarea3, $tenshoku_kiboubi, $age , $agent, $mailTo, $unit_url, $naiyo, $add);
                    }
                }else{
                    $shimei = $posts["shimei"];
                    $kana = $posts["kana"];
                    $birthday = $posts["year"] . "/" . $posts["month"] . "/" . $posts["day"];
                    $now = date("Ymd");
                    $tmp_birthday = explode("/",$birthday);
                    $tmp_birthday = $tmp_birthday[0] . sprintf('%02d', $tmp_birthday[1]) . sprintf('%02d', $tmp_birthday[2]);
                    $age =  floor(($now-$tmp_birthday)/10000). "歳";
                    $sex = $posts["sex"];
                    $pref= $posts["pref"];
                    $tel1 = $posts["tel1"];
                    $school_div_id = $posts["school_div_id"];
                    $school_name = $posts["school_name"];
                    $tmp_company_number = $posts["company_number"]. "社";
                    $jokyo = $posts["jokyo"];
                    $comment = $posts["comment"];
                    $tmp_kibouarea1 = $posts["kibouarea1"];
                    $tmp_kibouarea2 = $posts["kibouarea2"];
                    $tmp_kibouarea3 = $posts["kibouarea3"];
                    $tenshoku_kiboubi = $posts["tenshoku_kiboubi"];
                    $mailTo = $posts["mail1"];

                    $master = $this->getHrbcMaster();
                    $kibouarea1="";
                    foreach($master["area"]['Item'] as $k=>$v){
                        if(!empty($v["Item"])){
                            foreach($v['Item'] as $k2=>$v2){
                                if(!empty($v2["Items"]["Item"])){
                                    foreach($v2['Items']['Item'] as $k3=>$v3) {
                                        if(!empty($tmp_kibouarea1)){
                                            if ($tmp_kibouarea1 == $v3['Option.P_Id']) {
                                                $kibouarea1 =  htmlspecialchars($v3['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $kibouarea2="";
                    foreach($master["area"]['Item'] as $k=>$v){
                        if(!empty($v["Item"])){
                            foreach($v['Item'] as $k2=>$v2){
                                if(!empty($v2["Items"]["Item"])){
                                    foreach($v2['Items']['Item'] as $k3=>$v3) {
                                        if(!empty($tmp_kibouarea2)){
                                            if ($tmp_kibouarea2 == $v3['Option.P_Id']) {
                                                $kibouarea2 =  htmlspecialchars($v3['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $kibouarea3="";
                    foreach($master["area"]['Item'] as $k=>$v){
                        if(!empty($v["Item"])){
                            foreach($v['Item'] as $k2=>$v2){
                                if(!empty($v2["Items"]["Item"])){
                                    foreach($v2['Items']['Item'] as $k3=>$v3) {
                                        if(!empty($tmp_kibouarea3)){
                                            if ($tmp_kibouarea3 == $v3['Option.P_Id']) {
                                                $kibouarea3 =  htmlspecialchars($v3['Option.P_Name'], ENT_QUOTES, 'UTF-8');
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $agent = $this->getUserAgent();

                //URLの設定
                $person_url="";
                $resume_url="";

                if(!empty($result["person"])){
                    $person_url = "{unwrap}Person: https://hrbc-jp.porterscloud.com/can/search/id/" . $result["person"] . "#/dv/1:" . $result["person"] . "/{/unwrap}";
                }
                if(!empty($result["resume"])){
                    //$resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $result["resume"] . "?menu_id=5{/unwrap}";
                    $resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $result["resume"] ."?menu_id=5#/dv/17:" . $result["resume"] ."/{/unwrap}";
                }

                $userData = $this->db->get_where($this->database, array('hrbc_person_id' => $result["person"], 'hrbc_resume_id' => $result["resume"], 'del'=>0))->row();

                //新規登録はメール送信
                if($userData->hrbc_flg!=1){
                    //メール送信
                    $unit_url = $resume_url . "{unwrap}\n{/unwrap}";

                    $tmp_tenshokuKibou = $this->getTenshokuKiboubi($tenshoku_kiboubi);
                    //$tenshoku_kiboubi = $tenshoku_kiboubi . "(" . $tmp_tenshokuKibou . ")";

                    $this->sendMailStep1($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $kibouarea1, $kibouarea2, $kibouarea3, $tmp_tenshokuKibou, $age , $agent, $mailTo, $unit_url, $naiyo, $add);
                }

                //data設定
                if(!empty($posts["pwdchk"])){
                    //メールアドレス設定
                    //IDを取得
                    $ret = $this->db->get_where($this->database, array('id' => $posts["hurexid"], 'hash_email'=>$posts["hash"], 'del'=>0))->row();

                    $iv = $ret->iv;
                    $mail = $ret->email;
                    $email = $this->mail_decrypt($iv, $mail);
                    $email = trim($email);
                    $data[$this->segment]->mail1 = $email;

                    $data["render"] = $this->segment . "/step2_hrbc_complete";
                }else{
                    $data["render"] = $this->segment . "/step2";
                }
                $data["clean"] = $this->clean;
                $data["sub_title"] = $this->sub_title;
                $data["msg"] = $error;
                $data["segment"] = $this->segment;
                $data["param"] = @$posts["param"];
                $data["new_flg"] = $new_flg;
                $data["hurexid"] = $posts["hurexid"];

                $this->load->view("template_nologin", $data);
            }

        }else{
            redirect($this->segment."/regist/");
        }
    }


    //////////////////////////////////////////////////////////
    //登録情報確認
    //////////////////////////////////////////////////////////
    function confirm()
    {
        $posts = $this->input->post();

        if(empty($posts)){
            redirect($this->segment."/regist/");
        }

        //newフラグ取得
        $new_flg="";
        $ret = $this->db->get_where($this->database, array('id' => $posts["hurexid"], 'del'=>0))->row();
        $new_flg = $ret->new_flg;

        //バリデーション実行
        $error = "";
        $tmp_error = 0;
        $e_msg = "";
        if($posts["pwdchk"]==1){
            $error = $this->setValidationStepPwd3();
        }else{
            $error = $this->setValidationStep3();
        }

        //オブジェクトへ変換
        $data[$this->segment] = (object)$posts;


        if($error !== ""){
            //data設定
            if($posts["pwdchk"]==1){
                $data["render"] = $this->segment . "/step2_hrbc_complete";
            }else{
                $data["render"] = $this->segment . "/step2";
            }
            $data["clean"] = $this->clean;
            $data["sub_title"] = $this->sub_title;
            $data["msg"] = $error;
            $data["new_flg"] = $new_flg;
            $data["hurexid"] = $posts["hurexid"];
            $data["segment"] = $this->segment;

            $this->load->view("template_nologin", $data);

        }else{
            $result=array();
            $files = $_FILES;
            $cpt = count($files['userfile']['name']);
            for ($i = 0; $i < $cpt; $i++) {
                $filename=$files['userfile']['name'][$i];
                $require=0;
                $allow =  $this->allow;
                $overwrite=true;
                //@param(post, num, files, directory, require, filename)
                $result[]= $this->fileUpload(@$posts['file'.($i+1)], $i , $files, $this->segment, $require, $filename, $overwrite,$allow);
                if(!empty($result[$i]['error'])){
                    $tmp_error = 1;
                    $tmp_cnt = $i+1;
                    $e_msg .=  $result[$i]['error'] . "ファイル". $tmp_cnt . "<br />";
                }
            }
            /////////////////////////////////////////////////////////////////////////////
            //1枚目
            /////////////////////////////////////////////////////////////////////////////
            if(!empty($result[0]['upload_data']['file_size']) || !empty($posts['file1'])){
                (!empty($result[0]['upload_data']['file_size'])) ? $data[$this->segment]->file1=basename($result[0]['upload_data']['file_name']) : $data[$this->segment]->file1=$posts['file1'];
            }else{
                $data[$this->segment]->file1="";
            }
            if(@$posts['file1_del'] == "on"){
                $data[$this->segment]->file1="";
            }

            /////////////////////////////////////////////////////////////////////////////
            //2枚目以降
            /////////////////////////////////////////////////////////////////////////////
            for ($i = 1; $i < $cpt; $i++) {
                if(!empty($result[$i]['upload_data']['file_size']) || !empty($posts['file'.($i+1)])){
                    (!empty($result[$i]['upload_data']['file_size'])) ? $data[$this->segment]->{"file".($i+1)}=basename($result[$i]['upload_data']['file_name']) : $data[$this->segment]->{"file".($i+1)}=$posts['file'.($i+1)];
                }else{
                    $data[$this->segment]->{"file".($i+1)}="";
                }
                if(@$posts['file' . ($i+1) . '_del'] == "on"){
                    $data[$this->segment]->{"file".($i+1)}="";
                }
            }

            if($tmp_error != 1){
                //////////////////////////////////////////////////////////////////////////////////////////////////
                // Pardotに連携
                //////////////////////////////////////////////////////////////////////////////////////////////////
                $param_pardot = "";
                $param_pardot .= "email=" . urlencode($posts["mail1"]);

                $url = $this->pardotUrl . $param_pardot; // リクエストするURLとパラメータ

                // curlの処理を始める合図
                $curl = curl_init($url);

                // リクエストのオプションをセットしていく
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); // メソッド指定
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 証明書の検証を行わない
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // レスポンスを文字列で受け取る

                // レスポンスを変数に入れる
                $response = curl_exec($curl);
                $info    = curl_getinfo($curl);
                $errorNo = curl_errno($curl);

                // OK以外はエラーなので空白配列を返す
                if (!empty($response)) {
                    // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                    // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                    $subject = "【pardot連携7】  errorNo:" . $errorNo;
                    $errmsg = $posts["mail1"] . "\n" . "Step2 → confirm" . "\n" . $response;
                    $message = $errmsg;
                    $this->errMail($subject, $message);
                }

                // OK以外はエラーなので空白配列を返す
                if ($errorNo !== CURLE_OK) {
                    // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                    // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                    $subject = "【pardot連携8】  errorNo:" . $errorNo;
                    $errmsg = $posts["mail1"] . "\n" . "Step2 → confirm";
                    $message = $errmsg;
                    $this->errMail($subject, $message);
                }

                // 200以外のステータスコードは失敗とみなし空配列を返す
                if ($info['http_code'] !== 200) {
                    $subject = "【pardot連携9】  errorNo:" . $errorNo;
                    $errmsg = $posts["mail1"] . "\n" . "Step2 → confirm";
                    $message = $errmsg;
                    $this->errMail($subject, $message);
                }

                // curlの処理を終了
                curl_close($curl);
                //////////////////////////////////////////////////////////////////////////////////////////////////
                // Pardotに連携ここまで
                //////////////////////////////////////////////////////////////////////////////////////////////////

                //data設定
                if($posts["pwdchk"]==1){
                    $data["render"] = $this->segment . "/confirm_hrbc_complete";
                }else{
                    $data["render"] = $this->segment . "/confirm";
                }
                $data["clean"] = $this->clean;
                $data["msg"] = $error;
                $data["sub_title"] = $this->sub_title;
                $data["segment"] = $this->segment;
                $data["new_flg"] = $new_flg;
                $data["hurexid"] = $posts["hurexid"];

                $this->load->view("template_nologin", $data);
            }else {
                $error = "";
                $error .= "<br />" . $e_msg;

                //data設定
                if($posts["pwdchk"]==1){
                    $data["render"] = $this->segment . "/step2_hrbc_complete";
                }else{
                    $data["render"] = $this->segment . "/step2";
                }
                $data["clean"] = $this->clean;
                $data["sub_title"] = $this->sub_title;
                $data["msg"] = $error;
                $data["segment"] = $this->segment;
                $data["new_flg"] = $new_flg;
                $data["hurexid"] = $posts["hurexid"];

                $this->load->view("template_nologin", $data);
            }
        }
    }

    //////////////////////////////////////////////////////////
    //新規登録処理
    //////////////////////////////////////////////////////////
    function add()
    {
        $posts = $this->input->post();

        //newフラグ取得
        $new_flg=0;
        $ret = $this->db->get_where($this->database, array('id' => $posts["hurexid"], 'del'=>0))->row();
        $new_flg = $ret->new_flg;

        $db_param="";

        //バリデーション実行
        $error = "";
        if($posts["pwdchk"]==1){
            $error = $this->setValidationStepPwd3();
        }else{
            $error = $this->setValidationStep3();
        }

        //ID取得用
        $tmp_mail = trim($posts['mail1']);
        $hash_mail = hash_hmac("sha256", $tmp_mail, Salt);
        $db_info = $this->db->get_where($this->database, array('hash_email' => $hash_mail, 'del'=>0))->row();
        $tmp_hrbc_flg = $db_info->hrbc_flg;

        if($error !== ""){
            //data設定
            $data["render"] = $this->segment."/step2";
            $data["clean"] = $this->clean;
            $data["msg"] = $error;
            $data["segment"] = $this->segment;
            $data["hurexid"] = $posts["hurexid"];

            $this->load->view("template_nologin", $data);
        }else{
            //////////////////////////////////////////////////////////////////////////////////////////////////
            // Pardotに連携
            //////////////////////////////////////////////////////////////////////////////////////////////////
            $param_pardot = "";
            $param_pardot .= "email=" . urlencode($posts["mail1"]);

            $url = $this->pardotUrl . $param_pardot; // リクエストするURLとパラメータ

            // curlの処理を始める合図
            $curl = curl_init($url);

            // リクエストのオプションをセットしていく
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); // メソッド指定
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 証明書の検証を行わない
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // レスポンスを文字列で受け取る

            // レスポンスを変数に入れる
            $response = curl_exec($curl);
            $info    = curl_getinfo($curl);
            $errorNo = curl_errno($curl);

            // OK以外はエラーなので空白配列を返す
            if (!empty($response)) {
                // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                $subject = "【pardot連携10】  errorNo:" . $errorNo;
                $errmsg = $posts["mail1"] . "\n" . "Step2 → confirm" . "\n" . $response;
                $message = $errmsg;
                $this->errMail($subject, $message);
            }

            // OK以外はエラーなので空白配列を返す
            if ($errorNo !== CURLE_OK) {
                // 詳しくエラーハンドリングしたい場合はerrorNoで確認
                // タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
                $subject = "【pardot連携11】  errorNo:" . $errorNo;
                $errmsg = $posts["mail1"] . "\n" . "Step2 → confirm";
                $message = $errmsg;
                $this->errMail($subject, $message);
            }

            // 200以外のステータスコードは失敗とみなし空配列を返す
            if ($info['http_code'] !== 200) {
                $subject = "【pardot連携12】  errorNo:" . $errorNo;
                $errmsg = $posts["mail1"] . "\n" . "Step2 → confirm";
                $message = $errmsg;
                $this->errMail($subject, $message);
            }

            // curlの処理を終了
            curl_close($curl);
            //////////////////////////////////////////////////////////////////////////////////////////////////
            // Pardotに連携ここまで
            //////////////////////////////////////////////////////////////////////////////////////////////////

            //HRBC登録
            $result = $this->HrbcRegistStep3($posts);

            //職種の設定
            $tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
            $tmpjob = json_decode($tmpjob,true);

            $tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
            $tmppref = json_decode($tmppref,true);

            $tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
            $tmpgender = json_decode($tmpgender,true);

            $tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
            $tmpbackground = json_decode($tmpbackground,true);

            $tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
            $tmpwork = json_decode($tmpwork,true);

            $tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
            $tmparea = json_decode($tmparea,true);

            $tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
            $tmptenkin = json_decode($tmptenkin,true);

            $tmp_kibou_job="";
            if(!empty($tmpjob['Item'])){
                foreach($tmpjob['Item'] as $k=>$v){
                    if(!empty($v['Item'])){
                        foreach($v['Item'] as $k2=>$v2){
                            if(!empty($_POST["kiboujob"])){
                                foreach($_POST["kiboujob"] as $pk => $pv){
                                    if($pv==$v2['Option.P_Id']){
                                        $tmp_kibou_job .= $v2['Option.P_Name'] . "\n";
                                    }
                                }
                            }
                            if(!empty($v2["Items"]['Item'])){
                                foreach($v2['Items']['Item'] as $k3=>$v3){
                                    if(!empty($_POST["kiboujob"])){
                                        foreach($_POST["kiboujob"] as $pk2 => $pv2){
                                            if($pv2==$v3['Option.P_Id']){
                                                $tmp_kibou_job .= $v3['Option.P_Name'] . "\n";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $kibou_income = $posts["income"];
            $earnings = $posts["earnings"];

            $start1 = $posts["start_year1"] . "年" . $posts["start_month1"] . "月";
            $start2 = $posts["start_year2"] . "年" . $posts["start_month2"] . "月";
            $start3 = $posts["start_year3"] . "年" . $posts["start_month3"] . "月";
            $end1 = $posts["end_year1"] . "年" . $posts["end_month1"] . "月";
            $end2 = $posts["end_year2"] . "年" . $posts["end_month2"] . "月";
            $end3 = $posts["end_year3"] . "年" . $posts["end_month3"] . "月";

            $company_name1 = $posts["company_name1"];
            $company_name2 = $posts["company_name2"];
            $company_name3 = $posts["company_name3"];

            $tantou_job1 = $posts["tantou_job1"];
            $tantou_job2 = $posts["tantou_job2"];
            $tantou_job3 = $posts["tantou_job3"];

            $tmp_tenkin = "";
            if(!empty($_POST["tenkin"])){
                foreach($_POST["tenkin"] as $k => $v){
                    $tmp_tenkin .= htmlspecialchars($v, ENT_QUOTES, 'UTF-8') . "\n";
                }
            }

            $comment = $posts["comment"];
            $how = $posts["how"];

            //PCとスマホの判定
            $agent = $this->getUserAgent();

            //personと取得
            $person = $this->getPerson($result["person"]);
            $resume = $this->getResume($result["resume"]);

            if(empty($result)){
                $subject = "【ユーザー登録に失敗しました　code:signup01】";
                $this->entryErrMail($subject, $person["name"], $person["mail1"], $tmp_kibou_job, $kibou_income, $tmp_tenkin, $company_name1, $company_name2, $company_name3, $tantou_job1, $tantou_job2, $tantou_job3, $start1, $start2, $start3, $end1, $end2, $end3, $earnings );
            }else{

                //パラメータ取得（UI,JOB)
                $split_param = str_replace("'","", $db_info->parameter);
                $split_param = explode("|",$split_param);

                $jobinfo="";
                $ui_flg=0;
                if(!empty($split_param[1])){
                    if(is_numeric($split_param[1])){
                        $jobinfo = $this->getJobInfo($split_param[1]);
                    }else if(in_array($split_param[1],$this->soudan)){
                        $ui_flg = 1;
                    }
                }

                $iv = $db_info->iv;
                $mail = $db_info->email;
                $email = $this->mail_decrypt($iv, $mail);
                $mailTo = trim($email);

                /////////////////////////////////////////////////////////////////////////////////////////////////////////
                //メール送信 転職相談会以外は使いまわしていたらしい
                /////////////////////////////////////////////////////////////////////////////////////////////////////////

                //URLの設定
                $person_url="";
                $resume_url="";
                $job_url="";
                $process_url="";
                $rid= "";

                if(!empty($result["person"])){
                    $person_url = "{unwrap}Person: https://hrbc-jp.porterscloud.com/can/search/id/" . $result["person"] . "#/dv/1:" . $result["person"] . "/{/unwrap}";
                }
                if(!empty($result["resume"])){
                    //$resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $result["resume"] . "?menu_id=5{/unwrap}";
                    $resume_url  ="{unwrap}Resume: https://hrbc-jp.porterscloud.com/resume/search/id/" . $result["resume"] ."?menu_id=5#/dv/17:" . $result["resume"] ."/{/unwrap}";
                    $rid = $result["resume"];
                }
                if(!empty($jobinfo->job_id)){
                    $job_url ="{unwrap}Job: https://hrbc-jp.porterscloud.com/job/search/id/" . $jobinfo->job_id . "?menu_id=3{/unwrap}";
                }

                //UIターン
                $add="";

                $shimei = $person["name"];
                $kana = $person["kana"];
                $tel1 = $person["tel1"];
                $mail1 = $person["mail1"];
                $birthday = $resume["birthday"];
                $age = $resume["age"];
                $tmp_company_number = $resume["company_number"] . "社";
                $sex = $resume["sex"];
                $pref = $resume["pref"];
                $school_div_id = $resume["school_div_id"];
                $jokyo = $resume["jokyo"];
                $comment = $resume["comment"];
                $tmp_kibou_job = $resume["kiboujob"];
                $kibouarea1  = $resume["kibouarea1"];
                $kibouarea2  = $resume["kibouarea2"];
                $kibouarea3  = $resume["kibouarea3"];
                $school_name = $resume["school_name"];

                $kibouincome = $resume["kibouincome"];
                $current_earnings = $resume["earnings"];
                $start_date1 = $resume["start_date1"];
                $end_date1 = $resume["end_date1"];
                $start_date2 = $resume["start_date2"];
                $end_date2 = $resume["end_date2"];
                $start_date3 = $resume["start_date3"];
                $end_date3 = $resume["end_date3"];

                $tantou_jobs1 = $resume["tantou_jobs1"];
                $tantou_jobs2 = $resume["tantou_jobs2"];
                $tantou_jobs3 = $resume["tantou_jobs3"];

                $tenshoku_kiboubi = $resume["tenshoku_kiboubi"];

                $job_id="";

                $add_url="";
                if(!empty($new_flg) && $new_flg==1){
                    $add_url = "&rt=1";
                }

                if(!empty($jobinfo->job_id)) {
                    $job_id = $jobinfo->job_id;
                    //リクルート情報取得
                    $recruits = $this->getRecruitInfo($jobinfo);

                    $processId = $this->setProcess($split_param[1], $result["person"], $result["resume"], $jobinfo, $recruits);
                    if (!empty($processId)) {
                        if($processId != "error301"){
                            $process_url = "{unwrap}Process: https://hrbc-jp.porterscloud.com/process/search?resume_id=" . $result["resume"] . "#/dv/7:" . $processId . "/{/unwrap}";
                        }else if($processId == "error301"){
                            //201801ここいらないかもしれない
                            $db_param = "p|" . $split_param[1];
                        }
                    }

                    if ($processId && $processId!="error301") {

                        $text = $this->job_text;
                        $add = "JOB:" . $jobinfo->job_id . " / ";

//                        if($tmp_hrbc_flg!=1){
                        //メール送信
                        $unit_url = $resume_url . "{unwrap}\n{/unwrap}" . $job_url . "{unwrap}\n{/unwrap}" . $process_url;

                        $this->sendMailStep2($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibouincome, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $tenshoku_kiboubi, $job_id, $unit_url, $age , $agent, $mailTo, $text, $add);

                        //                       }
                    }

                    $add = "?action=job_new&rid=" . $rid . $add_url;
                }else if($ui_flg==1){

                    $param="";
                    if($split_param[1]==$this->join) {
                        //UIターン
                        $text = $this->join_text;
                        $param = $this->join;
                    }else{
                        //長期休暇
                        $text = $this->gw_text;
                        $param = $this->gw;
                    }

                    $unit_url = $person_url . "{unwrap}\n{/unwrap}" . $resume_url;

                    $this->sendMailStep2_ui($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibouincome, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $tenshoku_kiboubi, $job_id, $unit_url, $age , $agent, $mailTo, $text, $add,$param);


                    /*
                    $unit_url = $person_url . "{unwrap}\n{/unwrap}" . $resume_url;
                    if($tmp_hrbc_flg!=1){
                    $this->sendMailStep2_ui($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibouincome, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $job_id, $unit_url, $age , $agent, $mailTo, $text, $add);
                    }
                    */

                    $add = "?action=ui_new&rid=" . $rid . $add_url;

                }else{
                    $unit_url = $person_url . "{unwrap}\n{/unwrap}" . $resume_url;

                    $text="";
//                    if($tmp_hrbc_flg!=1){
                    $this->sendMailStep2($shimei, $kana, $birthday, $sex, $pref, $tel1, $school_div_id, $school_name, $tmp_company_number ,$jokyo , $comment, $tmp_kibou_job, $kibouincome, $tmp_tenkin, $kibouarea1, $kibouarea2, $kibouarea3, $current_earnings , $company_name1, $company_name2, $company_name3, $start_date1, $end_date1, $start_date2, $end_date2, $start_date3, $end_date3, $tantou_jobs1, $tantou_jobs2, $tantou_jobs3, $tenshoku_kiboubi, $job_id, $unit_url, $age , $agent, $mailTo, $text, $add);
//                    }

                    $add = "?action=new&rid=" . $rid . $add_url;
                }

                //DBからパラメータ消去
                $data = array(
                    'parameter' => $db_param
                );
                $where = array(
                    'id' => $db_info->id
                );
                $ret = $this->db->update($this->database, $data, $where);

                if(!empty($jobinfo->job_id)){
                    $add .= "&j=" . $jobinfo->job_id;
                }
                //登録成功
                $this->session->set_flashdata(array("msg"=>"登録が完了しました。"));
                redirect($this->segment."/complete" . $add);
            }
        }
    }

    //////////////////////////////////////////////////////////
    //HRBC登録 Step2
    //////////////////////////////////////////////////////////
    private function HrbcRegistStep2($datas=array())
    {
        //IDを取得
        $ret = $this->db->get_where($this->database, array('id' => $datas["hurexid"], 'hash_email'=>$datas["hash"], 'del'=>0))->row();

        if(empty($ret)){
            redirect($this->segment . "/regist/");
        }

        $parameter = $ret->parameter;

        $iv = $ret->iv;
        $mail = $ret->email;
        $email = $this->mail_decrypt($iv, $mail);
        $email = trim($email);

        $person_id = $ret->hrbc_person_id;
        if(empty($person_id)){
            $person_id = -1;
        }
        $resume_id = $ret->hrbc_resume_id;
        if(empty($resume_id)){
            $resume_id = -1;
        }

        $result = $this->getHrbcMaster();

        //誕生日
        if(!empty($datas["pwdchk"])){
            $birthday="";
            $gender="";
            $prefecture="";
            $school_div="";
            $school_name="";
            $current_info="";
        }else{
            $birthday = $datas["year"] . "/" . $datas["month"] . "/" . $datas["day"];

            //性別
            foreach($result["gender"]['Item'] as $k=>$v) {
                if (!empty($v["Item"])) {
                    foreach ($v['Item'] as $k2 => $v2) {
                        if ($datas["sex"] == $v2['Option.P_Name']) {
                            $gender = "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                        }
                    }
                }
            }

            //都道府県
            foreach($result["pref"]['Item'] as $k=>$v){
                if (!empty($v["Item"])) {
                    foreach($v['Item'] as $k2=>$v2){
                        if($datas["pref"]==$v2['Option.P_Name']){
                            $prefecture = "<" . htmlspecialchars($v2['Option.P_Alias'],ENT_QUOTES,'UTF-8') . "/>";
                        }
                    }
                }
            }

            //学歴
            $school_div="";
            foreach($result["background"]['Item'] as $k=>$v){
                if (!empty($v["Item"])) {
                    foreach($v['Item'] as $k2=>$v2) {
                        if ($datas["school_div_id"] == $v2['Option.P_Name']) {
                            $school_div = "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                        }
                    }
                }
            }

            //学校名
            $school_name = $datas["school_name"];

            //就業状況
            foreach($result["work"]['Item'] as $k=>$v){
                if (!empty($v["Item"])) {
                    foreach($v['Item'] as $k2=>$v2) {
                        if ($datas["jokyo"] == $v2['Option.P_Name']) {
                            $current_info = "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                        }
                    }
                }
            }
        }

        //希望勤務地1
        $expectarea1="";
        $tflg=0;
        foreach($result["area"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"]["Item"])){
                        foreach($v2['Items']['Item'] as $k3=>$v3) {
                            if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)){
                                if(!empty($datas["kibouarea1"])){
                                    if ($datas["kibouarea1"] == $v3['Option.P_Id']) {
                                        $expectarea1 = "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }else{
                                if($tflg==0){
                                    if(!empty($datas["kibouarea1"])){
                                        if ($datas["kibouarea1"] == $v2['Items']['Item']['Option.P_Id']) {
                                            $expectarea1 = "<" . htmlspecialchars($v2['Items']['Item']['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                        }
                                    }
                                    $tflg=1;
                                }
                            }
                        }
                    }
                }
            }
        }

        //希望勤務地2
        $expectarea2="";
        $tflg=0;
        foreach($result["area"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"]["Item"])){
                        foreach($v2['Items']['Item'] as $k3=>$v3) {
                            if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)){
                                if(!empty($datas["kibouarea2"])){
                                    if ($datas["kibouarea2"] == $v3['Option.P_Id']) {
                                        $expectarea2 = "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }else{
                                if($tflg==0){
                                    if(!empty($datas["kibouarea2"])){
                                        if ($datas["kibouarea2"] == $v2['Items']['Item']['Option.P_Id']) {
                                            $expectarea2 = "<" . htmlspecialchars($v2['Items']['Item']['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                        }
                                    }
                                    $tflg=1;
                                }
                            }
                        }
                    }
                }
            }
        }

        //希望勤務地3
        $expectarea3="";
        $tflg=0;
        foreach($result["area"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"]["Item"])){
                        foreach($v2['Items']['Item'] as $k3=>$v3) {
                            if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)){
                                if(!empty($datas["kibouarea3"])){
                                    if ($datas["kibouarea3"] == $v3['Option.P_Id']) {
                                        $expectarea3 = "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }else{
                                if($tflg==0){
                                    if(!empty($datas["kibouarea3"])){
                                        if ($datas["kibouarea3"] == $v2['Items']['Item']['Option.P_Id']) {
                                            $expectarea3 = "<" . htmlspecialchars($v2['Items']['Item']['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                        }
                                    }
                                    $tflg=1;
                                }
                            }
                        }
                    }
                }
            }
        }

        $todaydate = Date('Y/m/d H:i:s');

        $resume_addinfo="";
        if(!empty($resume_id)){
            $tmp_resumt = $this->getResume($resume_id);
            if(!empty($tmp_resumt["addinfo"])){
                $resume_addinfo = $tmp_resumt["addinfo"];
            }
        }

        //パラメータ付与
        $tmpentry="";
        $tmp_job_id="";
        if(!empty($datas["param"])){
            $tmpentry = str_replace("'","", $datas["param"]);
            $tmpentry = explode("|",$tmpentry);
        }
        $sub="";
        if(!empty($tmpentry[1]) && is_numeric($tmpentry[1])) {
            $tmp_job_id = $tmpentry[1];
            $sub = "求人詳細問い合わせからのエントリー";
        }else if(!empty($tmpentry[1])  && in_array($tmpentry[1],$this->soudan)){
            if($tmpentry[1]=="j") {
                $sub = "東北Ｕ・Ｉターン転職相談会in東京からのエントリー";
            }else if($tmpentry[1]=="g"){
                $sub = "長期休暇相談会からのエントリー";
            }
            $tmp_job_id="";
        }else{
            $sub = "転職支援申し込み（無料）からのエントリー";
            $tmp_job_id="";
        }

        $tmp_job_title="";
        $tmp_client_title="";
        if(!empty($tmp_job_id)){
            $esc_clientid = $this->db->escape($tmp_job_id);
            $sql = "SELECT * FROM clients as c left join jobs as j on c.c_id = j.client_id where j.job_id = " . $esc_clientid;
            $clients_info = $this->db->query($sql);
            if(!empty($clients_info->result()[0])){
                $tmp_client_title = $clients_info->result()[0]->c_title;
            }

            $sql = "SELECT * FROM jobs where job_id = " . $esc_clientid;
            $job_info = $this->db->query($sql);
            if(!empty($job_info->result()[0])){
                $tmp_job_title = $job_info->result()[0]->job_title;
            }
        }


        $todaydate = Date('Y/m/d');

        $agent = $this->getUserAgent();

        $hrbcparam = "【エントリー日】" . $todaydate . "\n【求人企業】" . $tmp_client_title .  "\n【希望JOB】 " . $tmp_job_title . " JOB" . $tmp_job_id . "\n【" . $agent . "】" . $sub;

        $hrbcparam = $resume_addinfo . "\n" . $hrbcparam;

        //パスワードだけの場合はResumeの希望勤務地を設定
        if(!empty($datas["pwdchk"])){
            $error="";
            ///////////////////////////////////////////////////////////////
            // HRBC Resume
            ///////////////////////////////////////////////////////////////
            $url = $this->hrbcUrl . "resume?partition=".$result["partition"];

            $sankabi="";
            if(!empty($datas["sankabi"])){
                $sankabi = $datas["sankabi"];
            }
            $kiboubi = $datas["kiboubi"];

            //※レジュメにメールとTELを入れるとエラーになる
            $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>1</Resume.P_Owner><Resume.P_Candidate>'. $person_id .'</Resume.P_Candidate><Resume.U_F3312A78116CAA30201FFFFC4CBC33>' . $expectarea1 . '</Resume.U_F3312A78116CAA30201FFFFC4CBC33><Resume.U_3CA45BBA1FBA2B4D0F2E7A6D25DD7D>' . $expectarea2 . '</Resume.U_3CA45BBA1FBA2B4D0F2E7A6D25DD7D><Resume.U_EF1BA8D1B218E6A4F11BF5EA910E36>' . $expectarea3 . '</Resume.U_EF1BA8D1B218E6A4F11BF5EA910E36><Resume.U_13C45F7FFAFC1AB430E6038C76125A>' . $hrbcparam . '</Resume.U_13C45F7FFAFC1AB430E6038C76125A><Resume.U_8E20986D8967D107BEE5D63870AD68>' . $kiboubi .'</Resume.U_8E20986D8967D107BEE5D63870AD68></Item></Resume>';
            $tmp_xml = $xml;

            $options = array
            (
                CURLOPT_URL            => $url,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $xml,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result['token']),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

            /*
            $header = array(
                "Content-Type: application/xml; charset=UTF-8",
                "X-porters-hrbc-oauth-token: ".$result["token"]
            );

            $options = array('http' => array(
                'method' => 'POST',
                'content' => $xml,
                'header' => implode("\r\n", $header)
            ));
            if($xml = @file_get_contents($url, false, stream_context_create($options))){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $c_code = explode(' ', $http_response_header[0]);  //「$c_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($c_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 signup hrbcregist2 "));
                            redirect("err/error_404/");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 signup hrbcregist2"));
                            redirect("err/error_404/");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other signup hrbcregist2"));
                            redirect("err/error_404/");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout signup hrbcregist2"));
                    redirect("err/error_404/");
                }

                //xml解析
                $rErr="";
                $rId = "";
                $xml = simplexml_load_string($xml);

                $this->writeRegistLog($tmp_xml, $xml, "signup01");

                if($xml->Code!=0){
                    $rErr=$xml->Code;
                }else{
                    $rCode="";
                    $json = json_encode($xml);
                    $arr = json_decode($json,true);
                    foreach($arr['Item'] as $k => $v){
                        if($k=="Id"){
                            $rId = $v;
                        }
                        if($k=="Code"){
                            $rErr = $v;
                        }
                    }
                }
                if(!$rId || $rErr){
                    $error.= "エラーが発生しました。(Resume: ".$rErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";

                    $subject = "【code:signup5】" . $error;
                    $message = $rErr;
                    $this->errMail($subject, $message);

                    $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $error));
                    redirect("err/error_404/");
                    //exit;
                }
            }
            */

            //配列データの変換
            $kjobs = "";
            if(!empty($datas["kiboujob"])){
                foreach($datas["kiboujob"] as $k => $v){
                    if($k!=0) $kjobs.=",";
                    $kjobs.=$v;
                }
            }
            $tenkins = "";
            if(!empty($datas["tenkin"])){
                foreach($datas["tenkin"] as $k => $v){
                    if($k!=0) $tenkins.=",";
                    $tenkins.=$v;
                }
            }
            $karea1 = "";
            if(!empty($datas["kibouarea1"])){
                $karea1 = $datas["kibouarea1"];
            }
            $karea2 = "";
            if(!empty($datas["kibouarea2"])){
                $karea2 = $datas["kibouarea2"];
            }
            $karea3 = "";
            if(!empty($datas["kibouarea3"])){
                $karea3 = $datas["kibouarea3"];
            }

            $entry="";
            $job_id="";
            if(!empty($datas["param"])){
                $entry = str_replace("'","", $datas["param"]);
                $entry = explode("|",$entry);
            }
            if(!empty($entry[1]) && is_numeric($entry[1])){
                $job_id = $entry[1];
            }else{
                $job_id="";
            }

            //HUREXのDBに登録
            if(empty($datas['id'])){
                $data['created'] = date('Y-m-d');
            }

            $pass = hash_hmac("sha256", $datas['password'], Salt);
            $salt = hash_hmac("sha256", Salt, Salt);
            $pass_unit = $salt . $pass;

            $ip = $_SERVER["REMOTE_ADDR"];
            $data = array(
                'password'=>$pass_unit,
                'kibou_area1' => $karea1,
                'kibou_area2' => $karea2,
                'kibou_area3' => $karea3,
                'parameter' => $parameter,
                'forget_password'=>'',
                'forget_password_limit'=>'',
                'status' => 2,
                'ip' => $ip
            );
            $where = array(
                'id' => $ret->id
            );
            $ret = $this->db->update($this->database, $data, $where);
            if(empty($ret)){
                $subject = "【Step2登録失敗】id: " . $datas["hurexid"] . " resume_id: " . $rId;
                $message = "id: " . $datas["hurexid"] . " " . " resume_id: " . $rId;
                $this->errHurexMail($subject, $message);

                $this->errMail($subject, $message);
            }

            $params=array();
            $params["person"] = $person_id;
            $params["resume"] = $resume_id;

            return $params;

        }else{
            /////////////////////////////////////////////////////////////////////
            //// HRBC Person
            ////////////////////////////////////////////////////////////////////////
            $url =$this->hrbcUrl .  "candidate?partition=".$result["partition"];

            $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Candidate><Item><Person.P_Id>' . $person_id . '</Person.P_Id><Person.P_Owner>1</Person.P_Owner><Person.P_Name>' . $datas["shimei"] . '</Person.P_Name><Person.P_Reading>' . $datas["kana"] . '</Person.P_Reading><Person.P_Telephone>' . $datas["tel1"] .'</Person.P_Telephone><Person.P_Street>' . $datas["pref"] . '</Person.P_Street><Person.P_Mail>' . $email .'</Person.P_Mail></Item></Candidate>';
            $tmp_xml = $xml;

            $header = array(
                "Content-Type: application/xml; charset=UTF-8",
                "X-porters-hrbc-oauth-token: " . $result['token']
            );

            $options = array('http' => array(
                'method' => 'POST',
                'content' => $xml,
                'header' => implode("\r\n", $header)
            ));

            if($xml = @file_get_contents($url, false, stream_context_create($options))){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $c_code = explode(' ', $http_response_header[0]);  //「$c_code[1]」にステータスコードの数字のみが入る

                    //エラーの判別
                    switch($c_code[1]){
                        //404エラーの場合
                        case 404:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 signup hrbcregist "));
                            redirect("err/error_404/");
                            break;

                        //500エラーの場合
                        case 500:
                            $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 signup hrbcregist"));
                            redirect("err/error_404/");
                            break;

                        //その他のエラーの場合
                        default:
                            $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other signup hrbcregist"));
                            redirect("err/error_404/");
                    }
                }else{
                    //タイムアウトの場合 or 存在しないドメインだった場合
                    $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout signup hrbcregist"));
                    redirect("err/error_404/");
                }
            }

            //xml解析
            $pErr="";
            $pId = "";
            $xml = simplexml_load_string($xml);

            $this->writeRegistLog($tmp_xml, $xml, "signup02");

            if($xml->Code!=0){
                $pErr=$xml->Code;
            }else{
                $pCode="";
                $json = json_encode($xml);
                $arr = json_decode($json,true);
                foreach($arr['Item'] as $k => $v){
                    if($k=="Id"){
                        $pId = $v;
                    }
                    if($k=="Code"){
                        $pErr = $v;

                    }
                }
            }

            $error="";
            if(!$pId || $pErr){
                $error.= "エラーが発生しました。(Person: " . $pErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";

                $subject = "【code:signup4】" . $error;
                $message = $pErr;
                $this->errMail($subject, $message);

                $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $error));
                redirect("err/error_404/");
                //exit;
            }

            if(empty($error)){
                ///////////////////////////////////////////////////////////////
                // HRBC Resume
                ///////////////////////////////////////////////////////////////
                $url = $this->hrbcUrl . "resume?partition=".$result["partition"];

                //転職希望日の変換
                $tnsk=$this->getTenshokuKiboubi($datas["tenshoku_kiboubi"]);

                //※レジュメにメールとTELを入れるとエラーになる
                $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>1</Resume.P_Owner><Resume.P_Candidate>'. $pId .'</Resume.P_Candidate><Resume.P_Name>' . $datas["shimei"] . '</Resume.P_Name><Resume.U_91DBD8B96BAAD6B33B538CF2494377>' . $datas["kana"] . '</Resume.U_91DBD8B96BAAD6B33B538CF2494377><Resume.P_DateOfBirth>' . $birthday . '</Resume.P_DateOfBirth><Resume.P_Gender>' . $gender . '</Resume.P_Gender><Resume.P_CurrentStatus>' . $current_info . '</Resume.P_CurrentStatus><Resume.U_8E20986D8967D107BEE5D63870AD68>' . $datas["kiboubi"] .'</Resume.U_8E20986D8967D107BEE5D63870AD68><Resume.P_ChangeJobsCount>' . $datas["company_number"] . '</Resume.P_ChangeJobsCount><Resume.U_7F3DF7F61EEF0679AA432646777832>' . $prefecture .'</Resume.U_7F3DF7F61EEF0679AA432646777832><Resume.U_F3312A78116CAA30201FFFFC4CBC33>' . $expectarea1 . '</Resume.U_F3312A78116CAA30201FFFFC4CBC33><Resume.U_3CA45BBA1FBA2B4D0F2E7A6D25DD7D>' . $expectarea2 . '</Resume.U_3CA45BBA1FBA2B4D0F2E7A6D25DD7D><Resume.U_EF1BA8D1B218E6A4F11BF5EA910E36>' . $expectarea3 . '</Resume.U_EF1BA8D1B218E6A4F11BF5EA910E36><Resume.U_85DEA8FA18EF17830FD8A036EC95A4>' . $school_name . '</Resume.U_85DEA8FA18EF17830FD8A036EC95A4><Resume.U_673275A4DA445C9B23AC18BDD7C4DC>' . $school_div . '</Resume.U_673275A4DA445C9B23AC18BDD7C4DC><Resume.U_13C45F7FFAFC1AB430E6038C76125A>' . $hrbcparam . '</Resume.U_13C45F7FFAFC1AB430E6038C76125A><Resume.U_E7A1F795DE4A2C4F94EB1847006899>' . $tnsk . '</Resume.U_E7A1F795DE4A2C4F94EB1847006899></Item></Resume>';
                $tmp_xml = $xml;

                /*
                $header = array(
                    "Content-Type: application/xml; charset=UTF-8",
                    "X-porters-hrbc-oauth-token: ".$result["token"]
                );

                $options = array('http' => array(
                    'method' => 'POST',
                    'content' => $xml,
                    'header' => implode("\r\n", $header)
                ));
                if($xml = @file_get_contents($url, false, stream_context_create($options))){
                    //ここにデータ取得が成功した時の処理
                }else{
                    //エラー処理
                    if(count($http_response_header) > 0){
                        //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                        $c_code = explode(' ', $http_response_header[0]);  //「$c_code[1]」にステータスコードの数字のみが入る

                        //エラーの判別
                        switch($c_code[1]){
                            //404エラーの場合
                            case 404:
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 signup hrbcregist2 "));
                                redirect("err/error_404/");
                                break;

                            //500エラーの場合
                            case 500:
                                $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 signup hrbcregist2"));
                                redirect("err/error_404/");
                                break;

                            //その他のエラーの場合
                            default:
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other signup hrbcregist2"));
                                redirect("err/error_404/");
                        }
                    }else{
                        //タイムアウトの場合 or 存在しないドメインだった場合
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout signup hrbcregist2"));
                        redirect("err/error_404/");
                    }
                }
                */

                //xml解析
                $rErr="";
                $rId = "";
                $options = array
                (
                    CURLOPT_URL            => $url,
                    CURLOPT_POST           => true,
                    CURLOPT_POSTFIELDS     => $xml,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING       => "UTF-8",
                    CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result['token']),
                    CURLINFO_HEADER_OUT    => true,
                );

                $ch = curl_init();
                curl_setopt_array($ch, $options);
                $res = curl_exec($ch);
                $info = curl_getinfo($ch);
                $xml = simplexml_load_string($res);
                $this->writeRegistLog($tmp_xml, $xml, "signup03");

                if($xml->Code!=0){
                    $rErr=$xml->Code;
                }else{
                    $rCode="";
                    $json = json_encode($xml);
                    $arr = json_decode($json,true);
                    foreach($arr['Item'] as $k => $v){
                        if($k=="Id"){
                            $rId = $v;
                        }
                        if($k=="Code"){
                            $rErr = $v;
                        }
                    }
                }
                if(!$rId || $rErr){
                    $error.= "エラーが発生しました。(Resume: ".$rErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";

                    $subject = "【code:signup5】" . $error;
                    $message = $pErr;
                    $this->errMail($subject, $message);

                    $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $error));
                    redirect("err/error_404/");
                    //exit;
                }
            }


            //配列データの変換
            $kjobs = "";
            if(!empty($datas["kiboujob"])){
                foreach($datas["kiboujob"] as $k => $v){
                    if($k!=0) $kjobs.=",";
                    $kjobs.=$v;
                }
            }
            $tenkins = "";
            if(!empty($datas["tenkin"])){
                foreach($datas["tenkin"] as $k => $v){
                    if($k!=0) $tenkins.=",";
                    $tenkins.=$v;
                }
            }
            $karea1 = "";
            if(!empty($datas["kibouarea1"])){
                $karea1 = $datas["kibouarea1"];
            }
            $karea2 = "";
            if(!empty($datas["kibouarea2"])){
                $karea2 = $datas["kibouarea2"];
            }
            $karea3 = "";
            if(!empty($datas["kibouarea3"])){
                $karea3 = $datas["kibouarea3"];
            }

            $entry="";
            $job_id="";
            if(!empty($datas["param"])){
                $entry = str_replace("'","", $datas["param"]);
                $entry = explode("|",$entry);
            }
            if(!empty($entry[1]) && is_numeric($entry[1])){
                $job_id = $entry[1];
            }else{
                $job_id="";
            }

            //HUREXのDBに登録
            if(empty($datas['id'])){
                $data['created'] = date('Y-m-d');
            }

            $pass = hash_hmac("sha256", $datas['password'], Salt);
            $salt = hash_hmac("sha256", Salt, Salt);
            $pass_unit = $salt . $pass;
            $ip = $_SERVER["REMOTE_ADDR"];

            $data = array(
                'hrbc_person_id' => $pId,
                'hrbc_resume_id' => $rId,
                'password'=>$pass_unit,
                'kibou_area1' => $karea1,
                'kibou_area2' => $karea2,
                'kibou_area3' => $karea3,
                'parameter' => $parameter,
                'forget_password'=>'',
                'forget_password_limit'=>'',
                'status' => 2,
                'ip' => $ip
            );
            $where = array(
                'id' => $ret->id
            );
            $ret = $this->db->update($this->database, $data, $where);
            if(empty($ret)){
                $subject = "【Step2登録失敗】id: " . $datas["hurexid"] . "person_id: " . $pId . " resume_id: " . $rId;
                $message = "id: " . $datas["hurexid"] . "person_id: " . $pId . " resume_id: " . $rId;
                $this->errHurexMail($subject, $message);

                $this->errMail($subject, $message);
            }

            $params=array();
            $params["person"] = $pId;
            $params["resume"] = $rId;

            return $params;
        }
    }

    //////////////////////////////////////////////////////////
    //HRBC登録
    //////////////////////////////////////////////////////////
    private function HrbcRegistStep3($datas=array())
    {
        ////////////////////////////////////////////////////////////////////
        // レジュメ等設定
        ////////////////////////////////////////////////////////////////////
        $dir = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/";
        include($dir . "system_entry/mimes.php");
        //データ１
        if(!empty($datas["file1"])){
            $file1_dir = $_SERVER["DOCUMENT_ROOT"] . "/../entry/" . $this->segment . "/" . $datas["file1"];
            //mime取得
            $extension1 = strtolower(pathinfo($file1_dir, PATHINFO_EXTENSION));
            $mime_type1  = $mimes[$extension1];
            if(is_array($mime_type1)){
                $mime_type1 = $mime_type1[0];
            }
            //base64化
            $fp = fopen($file1_dir, "r");
            $file1_tmp = fread($fp, filesize($file1_dir));
            fclose($fp);
            $file1_data = base64_encode($file1_tmp);
        }

        if(!empty($datas["file2"])){
            //データ２
            $file2_dir = $_SERVER["DOCUMENT_ROOT"] . "/../entry/" . $this->segment . "/" . $datas["file2"];
            //mime取得
            $extension2 = strtolower(pathinfo($file2_dir, PATHINFO_EXTENSION));
            $mime_type2  = $mimes[$extension2];
            if(is_array($mime_type2)){
                $mime_type2 = $mime_type2[0];
            }
            //base64化
            $fp = fopen($file2_dir, "r");
            $file2_tmp = fread($fp, filesize($file2_dir));
            fclose($fp);
            $file2_data = base64_encode($file2_tmp);
        }

        //データ３
        if(!empty($datas["file3"])){
            $file3_dir = $_SERVER["DOCUMENT_ROOT"] . "/../entry/" . $this->segment . "/" . $datas["file3"];
            //mime取得
            $extension3 = strtolower(pathinfo($file3_dir, PATHINFO_EXTENSION));
            $mime_type3  = $mimes[$extension3];
            if(is_array($mime_type3)){
                $mime_type3 = $mime_type3[0];
            }
            //base64化
            $fp = fopen($file3_dir, "r");
            $file3_tmp = fread($fp, filesize($file3_dir));
            fclose($fp);
            $file3_data = base64_encode($file3_tmp);
        }

        //IDを取得
        $tmp_mail = trim($datas["mail1"]);
        $hash_email = hash_hmac("sha256", $tmp_mail, Salt);
        $ret = $this->db->get_where($this->database, array('hash_email' => $hash_email, 'del'=>0))->row();

        $parameter = $ret->parameter;

        $iv = $ret->iv;
        $mail = $ret->email;
        $email = $this->mail_decrypt($iv, $mail);
        $email = trim($email);

        $person_id = $ret->hrbc_person_id;
        if(empty($person_id)){
            redirect($this->segment . "/regist/");
        }
        $resume_id = $ret->hrbc_resume_id;
        if(empty($resume_id)){
            redirect($this->segment . "/regist/");
        }

        $result = $this->getHrbcMaster();

        //希望勤務地1
        $expectarea1="";
        foreach($result["area"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"]["Item"])){
                        foreach($v2['Items']['Item'] as $k3=>$v3) {
                            if(!empty($datas["kibouarea1"])){
                                if ($datas["kibouarea1"] == $v3['Option.P_Id']) {
                                    $expectarea1 = "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                }
                            }
                        }
                    }
                }
            }
        }

        //希望勤務地2
        $expectarea2="";
        foreach($result["area"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"]["Item"])){
                        foreach($v2['Items']['Item'] as $k3=>$v3) {
                            if(!empty($datas["kibouarea2"])){
                                if ($datas["kibouarea2"] == $v3['Option.P_Id']) {
                                    $expectarea2 = "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                }
                            }
                        }
                    }
                }
            }
        }

        //希望勤務地3
        $expectarea3="";
        foreach($result["area"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"]["Item"])){
                        foreach($v2['Items']['Item'] as $k3=>$v3) {
                            if(!empty($datas["kibouarea3"])){
                                if ($datas["kibouarea3"] == $v3['Option.P_Id']) {
                                    $expectarea3 = "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                }
                            }
                        }
                    }
                }
            }
        }

        //転勤
        $tenkin="";
        if(!empty($datas["tenkin"])){
            foreach($result["tenkin"]['Item'] as $k=>$v){
                if (!empty($v["Item"])) {
                    foreach($v['Item'] as $k2=>$v2) {
                        foreach($datas["tenkin"] as $tk => $v){
                            if ($v == $v2['Option.P_Name']) {
                                $tenkin .= "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                            }
                        }
                    }
                }
            }
        }

        //希望職種
        $kiboujob="";
        foreach($result["jobcategory"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($datas["kiboujob"])){
                        foreach($datas["kiboujob"] as $kk => $kv){
                            if ($kv == $v2['Option.P_Id']) {
                                if(!empty($v2["Items"]["Item"])){
                                    foreach($v2['Items']['Item'] as $k3=>$v3) {
                                        $kiboujob .= "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //希望年収
        $income="";
        if(!empty($datas["income"])){
            foreach($result["income"]['Item'] as $k=>$v){
                if (!empty($v["Item"])) {
                    foreach($v['Item'] as $k2=>$v2) {
                        if(!empty($datas["income"])){
                            if ($datas["income"] == $v2['Option.P_Id']) {
                                $income .= "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                            }
                        }
                    }
                }
            }
        }

        //現在の年収
        $earnings="";
        if(!empty($datas["earnings"])){
            foreach($result["income"]['Item'] as $k=>$v){
                if (!empty($v["Item"])) {
                    foreach($v['Item'] as $k2=>$v2) {
                        if(!empty($datas["earnings"])){
                            if ($datas["earnings"] == $v2['Option.P_Id']) {
                                $earnings .= "<" . htmlspecialchars($v2['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                            }
                        }
                    }
                }
            }
        }

        //直近の会社名
        $company_name1 = $datas["company_name1"];
        $company_name2 = $datas["company_name2"];
        $company_name3 = $datas["company_name3"];

        //担当職種
        $tantou_job1="";
        foreach($result["jobmypagecd"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"])){
                        foreach($v2["Items"]['Item'] as $k3=>$v3) {
                            if(!empty($datas["tantou_job1"])){
                                foreach($datas["tantou_job1"] as $kk => $kv){
                                    if ($kv == $v3['Option.P_Id']) {
                                        $tantou_job1 .= "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $tantou_job2="";
        foreach($result["jobmypagecd"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"])){
                        foreach($v2["Items"]['Item'] as $k3=>$v3) {
                            if(!empty($datas["tantou_job2"])){
                                foreach($datas["tantou_job2"] as $kk => $kv){
                                    if ($kv == $v3['Option.P_Id']) {
                                        $tantou_job2 .= "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $tantou_job3="";
        foreach($result["jobmypagecd"]['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"])){
                        foreach($v2["Items"]['Item'] as $k3=>$v3) {
                            if(!empty($datas["tantou_job3"])){
                                foreach($datas["tantou_job3"] as $kk => $kv){
                                    if ($kv == $v3['Option.P_Id']) {
                                        $tantou_job3 .= "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //在籍
        $start_date1  = $datas["start_year1"] . "/" . sprintf('%02d', $datas["start_month1"]) . "/01";
        $start_date2="";
        if(!empty($datas["start_year2"]) && !empty($datas["start_month2"])){
            $start_date2  = $datas["start_year2"] . "/" . sprintf('%02d', $datas["start_month2"]) . "/01";
        }
        $start_date3="";
        if(!empty($datas["start_year3"]) && !empty($datas["start_month3"])){
            $start_date3  = $datas["start_year3"] . "/" . sprintf('%02d', $datas["start_month3"]) . "/01";
        }
        $end_date1="";
        if(!empty($datas["end_year1"]) && !empty($datas["end_month1"])){
            $end_date1  = $datas["end_year1"] . "/" . sprintf('%02d', $datas["end_month1"]) . "/01";
        }
        $end_date2="";
        if(!empty($datas["end_year2"]) && !empty($datas["end_month2"])){
            $end_date2  = $datas["end_year2"] . "/" . sprintf('%02d', $datas["end_month2"]) . "/01";
        }
        $end_date3="";
        if(!empty($datas["end_year3"]) && !empty($datas["end_month3"])){
            $end_date3  = $datas["end_year3"] . "/" . sprintf('%02d', $datas["end_month3"]) . "/01";
        }

        //コメント
        $comment = $datas["comment"];

        //hurexをどうやってしったか
        $how = $datas["how"];

        $todaydate = Date('Y/m/d H:i:s');

        ///////////////////////////////////////////////////////////////
        // HRBC Resume
        ///////////////////////////////////////////////////////////////
        $error="";
        $url = $this->hrbcUrl . "resume?partition=".$result["partition"];

        $now = date('Y/m/d H:i:s');
        $now_hrbc = date("Y/m/d H:i:s",strtotime($now. "-9 hour"));

        //転職希望日の変換
        if(!empty($datas["tenshoku_kiboubi"])){
            $tnsk=$this->getTenshokuKiboubi($datas["tenshoku_kiboubi"]);
        }

        //※レジュメにメールとTELを入れるとエラーになる
        if($datas["pwdchk"]==1){
            $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>1</Resume.P_Owner><Resume.P_Candidate>'. $person_id .'</Resume.P_Candidate><Resume.U_83F3DDBA6DEC80CE66E21A04631FA7>' . $income . '</Resume.U_83F3DDBA6DEC80CE66E21A04631FA7><Resume.U_EC012BD105993E9656D208951323F4>' . $tenkin . '</Resume.U_EC012BD105993E9656D208951323F4><Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504>' . $earnings . '</Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504><Resume.U_14551B3DE0AAA0F98964734D422811>'.$kiboujob.'</Resume.U_14551B3DE0AAA0F98964734D422811><Resume.U_F3312A78116CAA30201FFFFC4CBC33>' . $expectarea1 . '</Resume.U_F3312A78116CAA30201FFFFC4CBC33><Resume.U_3CA45BBA1FBA2B4D0F2E7A6D25DD7D>' . $expectarea2 . '</Resume.U_3CA45BBA1FBA2B4D0F2E7A6D25DD7D><Resume.U_EF1BA8D1B218E6A4F11BF5EA910E36>' . $expectarea3 . '</Resume.U_EF1BA8D1B218E6A4F11BF5EA910E36><Resume.U_E7DFF5F197913246BD6A40336802D4>' . $now_hrbc . '</Resume.U_E7DFF5F197913246BD6A40336802D4><Resume.U_E7A1F795DE4A2C4F94EB1847006899>' . $tnsk .'</Resume.U_E7A1F795DE4A2C4F94EB1847006899></Item></Resume>';

        }else{
            $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>1</Resume.P_Owner><Resume.P_Candidate>'. $person_id .'</Resume.P_Candidate><Resume.U_9A9AD75E2CF88361F40E91D2B53876>' . $company_name1 . '</Resume.U_9A9AD75E2CF88361F40E91D2B53876><Resume.U_12C7C4EC761C78DB864D9AD0FA73ED>' . $start_date1 . '</Resume.U_12C7C4EC761C78DB864D9AD0FA73ED><Resume.U_4679EE290064C24E5E593F5772732D>' . $end_date1 . '</Resume.U_4679EE290064C24E5E593F5772732D><Resume.U_AE99DF32764C633CAE8A79EDC68490>' . $company_name2 . '</Resume.U_AE99DF32764C633CAE8A79EDC68490><Resume.U_7CCBD503A17CBD06078DB0907BEC9B>' . $start_date2 . '</Resume.U_7CCBD503A17CBD06078DB0907BEC9B><Resume.U_FD0444A71EB7C820DC1455C4358EE8>' . $end_date2 .'</Resume.U_FD0444A71EB7C820DC1455C4358EE8><Resume.U_C12A1CD0372B2FECA9055E0302F59D>' . $company_name3 . '</Resume.U_C12A1CD0372B2FECA9055E0302F59D><Resume.U_F65492E72A71583EE72F032C73885A>' . $start_date3 . '</Resume.U_F65492E72A71583EE72F032C73885A><Resume.U_F6662BA5A33D671A4516974181F5F0>' . $end_date3 . '</Resume.U_F6662BA5A33D671A4516974181F5F0><Resume.U_83F3DDBA6DEC80CE66E21A04631FA7>' . $income . '</Resume.U_83F3DDBA6DEC80CE66E21A04631FA7><Resume.U_EC012BD105993E9656D208951323F4>' . $tenkin . '</Resume.U_EC012BD105993E9656D208951323F4><Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504>' . $earnings . '</Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504><Resume.U_14551B3DE0AAA0F98964734D422811>'.$kiboujob.'</Resume.U_14551B3DE0AAA0F98964734D422811><Resume.U_90CE972D1600F17EDCEC4661728935>' . $tantou_job1 . '</Resume.U_90CE972D1600F17EDCEC4661728935><Resume.U_910B68CCF4A2F064E0059737A883C0>' . $tantou_job2 .'</Resume.U_910B68CCF4A2F064E0059737A883C0><Resume.U_7245CA44510F223C359FE2D7BC66DB>' . $tantou_job3 . '</Resume.U_7245CA44510F223C359FE2D7BC66DB><Resume.U_E7DFF5F197913246BD6A40336802D4>' . $now_hrbc . '</Resume.U_E7DFF5F197913246BD6A40336802D4><Resume.U_5F7FC5144BA471117A376B60BC8D90>' . $comment .'</Resume.U_5F7FC5144BA471117A376B60BC8D90><Resume.U_DDF5A119155AD1F559BAA37C031C4E>' . $how .'</Resume.U_DDF5A119155AD1F559BAA37C031C4E></Item></Resume>';
            $tmp_xml  =$xml;
            /* テスト用
            $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>' . $resume_id . '</Resume.P_Id><Resume.P_Owner>1</Resume.P_Owner><Resume.P_Candidate>'. $person_id .'</Resume.P_Candidate><Resume.U_9A9AD75E2CF88361F40E91D2B53876>' . $company_name1 . '</Resume.U_9A9AD75E2CF88361F40E91D2B53876><Resume.U_12C7C4EC761C78DB864D9AD0FA73ED>' . $start_date1 . '</Resume.U_12C7C4EC761C78DB864D9AD0FA73ED><Resume.U_4679EE290064C24E5E593F5772732D>' . $end_date1 . '</Resume.U_4679EE290064C24E5E593F5772732D><Resume.U_AE99DF32764C633CAE8A79EDC68490>' . $company_name2 . '</Resume.U_AE99DF32764C633CAE8A79EDC68490><Resume.U_7CCBD503A17CBD06078DB0907BEC9B>' . $start_date2 . '</Resume.U_7CCBD503A17CBD06078DB0907BEC9B><Resume.U_FD0444A71EB7C820DC1455C4358EE8>' . $end_date2 .'</Resume.U_FD0444A71EB7C820DC1455C4358EE8><Resume.U_C12A1CD0372B2FECA9055E0302F59D>' . $company_name3 . '</Resume.U_C12A1CD0372B2FECA9055E0302F59D><Resume.U_F65492E72A71583EE72F032C73885A>' . $start_date3 . '</Resume.U_F65492E72A71583EE72F032C73885A><Resume.U_F6662BA5A33D671A4516974181F5F0>' . $end_date3 . '</Resume.U_F6662BA5A33D671A4516974181F5F0><Resume.U_83F3DDBA6DEC80CE66E21A04631FA7>' . $income . '</Resume.U_83F3DDBA6DEC80CE66E21A04631FA7><Resume.U_EC012BD105993E9656D208951323F4>' . $tenkin . '</Resume.U_EC012BD105993E9656D208951323F4><Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504>' . $earnings . '</Resume.U_CCABD82E1EA0F53F27AC1C1C1E4504><Resume.U_14551B3DE0AAA0F98964734D422811>'.$kiboujob.'</Resume.U_14551B3DE0AAA0F98964734D422811><Resume.U_B1451F4914E0F2E436C6CCD202528B>' . $tantou_job1 . '</Resume.U_B1451F4914E0F2E436C6CCD202528B><Resume.U_910B68CCF4A2F064E0059737A883C0>' . $tantou_job2 .'</Resume.U_910B68CCF4A2F064E0059737A883C0><Resume.U_7245CA44510F223C359FE2D7BC66DB>' . $tantou_job3 . '</Resume.U_7245CA44510F223C359FE2D7BC66DB><Resume.U_E7DFF5F197913246BD6A40336802D4>' . $now_hrbc . '</Resume.U_E7DFF5F197913246BD6A40336802D4></Item></Resume>';
            */
        }

        /*
        $header = array(
            "Content-Type: application/xml; charset=UTF-8",
            "X-porters-hrbc-oauth-token: ".$result["token"]
        );

        $options = array('http' => array(
            'method' => 'POST',
            'content' => $xml,
            'header' => implode("\r\n", $header)
        ));
        if($xml = @file_get_contents($url, false, stream_context_create($options))){
            //ここにデータ取得が成功した時の処理
        }else{
            //エラー処理
            if(count($http_response_header) > 0){
                //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                $c_code = explode(' ', $http_response_header[0]);  //「$c_code[1]」にステータスコードの数字のみが入る

                //エラーの判別
                switch($c_code[1]){
                    //404エラーの場合
                    case 404:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 signup hrbcregist2 "));
                        redirect($this->segment."/regist/");
                        break;

                    //500エラーの場合
                    case 500:
                        $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 signup hrbcregist2"));
                        redirect($this->segment."/regist/");
                        break;

                    //その他のエラーの場合
                    default:
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other signup hrbcregist2"));
                        redirect($this->segment."/regist/");
                }
            }else{
                //タイムアウトの場合 or 存在しないドメインだった場合
                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout signup hrbcregist2"));
                redirect($this->segment."/regist/");
            }
        }
        */
        $options = array
        (
            CURLOPT_URL            => $url,
            CURLOPT_POST           => true,
            CURLOPT_POSTFIELDS     => $xml,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "UTF-8",
            CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result['token']),
            CURLINFO_HEADER_OUT    => true,
        );

        $ch = curl_init();
        curl_setopt_array($ch, $options);
        $res = curl_exec($ch);
        $info = curl_getinfo($ch);
        $xml = simplexml_load_string($res);

        //xml解析
        $rErr="";
        $rId = "";

        $this->writeRegistLog($tmp_xml, $xml, "signup04");

        if($xml->Code!=0){
            $rErr=$xml->Code;
        }else{
            $rCode="";
            $json = json_encode($xml);
            $arr = json_decode($json,true);
            foreach($arr['Item'] as $k => $v){
                if($k=="Id"){
                    $rId = $v;
                }
                if($k=="Code"){
                    $rErr = $v;
                }
            }
        }
        if(!$rId || $rErr){
            $error.= "エラーが発生しました。(Resume: ".$rErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";

            $subject = "【code:signup5】" . $error;
            $message = $rErr;
            $this->errMail($subject, $message);

            $this->session->set_flashdata(array("msg"=>"エラーが発生しました。 <br />コード：" . $error));
            redirect("err/error_404/");
            //exit;
        }

        if(empty($error)) {
            ///////////////////////////////////////////////////////////////
            // HRBC Attachment
            ///////////////////////////////////////////////////////////////
            if (!empty($datas["file1"]) || !empty($datas["file2"]) || !empty($datas["file3"])) {

                $url = "https://api-hrbc-jp.porterscloud.com/v1/attachment?partition=" . $result["partition"];

                //file1
                $param = "";
                if (!empty($datas["file1"])) {
                    $param .= '<Item><Id>-1</Id><Resource>17</Resource><ResourceId>' . $rId . '</ResourceId><FileName>' . $datas["file1"] . '</FileName><Content>' . $file1_data . '</Content><ContentType>' . $mime_type1 . '</ContentType></Item>';
                }
                if (!empty($datas["file2"])) {
                    $param .= '<Item><Id>-1</Id><Resource>17</Resource><ResourceId>' . $rId . '</ResourceId><FileName>' . $datas["file2"] . '</FileName><Content>' . $file2_data . '</Content><ContentType>' . $mime_type2 . '</ContentType></Item>';
                }
                if (!empty($datas["file3"])) {
                    $param .= '<Item><Id>-1</Id><Resource>17</Resource><ResourceId>' . $rId . '</ResourceId><FileName>' . $datas["file3"] . '</FileName><Content>' . $file3_data . '</Content><ContentType>' . $mime_type3 . '</ContentType></Item>';
                }


                $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Attachment>' . $param . '</Attachment>';
                $tmp_xml = $xml;

                /*
                $header = array(
                    "Content-Type: application/xml; charset=UTF-8",
                    "X-porters-hrbc-oauth-token: ".$result["token"]
                );

                $options = array('http' => array(
                    'method' => 'POST',
                    'content' => $xml,
                    'header' => implode("\r\n", $header)
                ));
                if($xml = @file_get_contents($url, false, stream_context_create($options))){
                    //ここにデータ取得が成功した時の処理
                }else{
                    //エラー処理
                    if(count($http_response_header) > 0){
                        //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                        $c_code = explode(' ', $http_response_header[0]);  //「$c_code[1]」にステータスコードの数字のみが入る

                        //エラーの判別
                        switch($c_code[1]){
                            //404エラーの場合
                            case 404:
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:404 signup hrbcregist3 "));
                                redirect($this->segment."/regist/");
                                break;

                            //500エラーの場合
                            case 500:
                                $this->session->set_flashdata(array("msg"=>"サーバでエラーが発生しました。Err:500 signup hrbcregist3"));
                                redirect($this->segment."/regist/");
                                break;

                            //その他のエラーの場合
                            default:
                                $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:other signup hrbcregist3"));
                                redirect($this->segment."/regist/");
                        }
                    }else{
                        //タイムアウトの場合 or 存在しないドメインだった場合
                        $this->session->set_flashdata(array("msg"=>"不正な処理が行われました。Err:timeout signup hrbcregist3"));
                        redirect($this->segment."/regist/");
                    }
                }
                */
                $options = array
                (
                    CURLOPT_URL            => $url,
                    CURLOPT_POST           => true,
                    CURLOPT_POSTFIELDS     => $xml,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING       => "UTF-8",
                    CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $result['token']),
                    CURLINFO_HEADER_OUT    => true,
                );

                $ch = curl_init();
                curl_setopt_array($ch, $options);
                $res = curl_exec($ch);
                $info = curl_getinfo($ch);
                $xml = simplexml_load_string($res);

                //xml解析
                $aErr = "";
                $aId = "";

                $this->writeRegistLog($tmp_xml, $xml, "signup05");

                $aCode = "";
                $json = json_encode($xml);
                $arr = json_decode($json, true);
                if(!empty($arr["Item"])){
                    foreach ($arr['Item'] as $k => $v) {
                        if ($k == "Id") {
                            $aId = $v;
                        }
                        if ($k == "Code") {
                            $aErr = $v;
                        }
                    }
                }

                if (!$aId) {
                    $error .= "エラーが発生しました。(Attachment: " . $aErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
                }
            }
        }

        //配列データの変換
        $kjobs = "";
        if(!empty($datas["kiboujob"])){
            foreach($datas["kiboujob"] as $k => $v){
                if($k!=0) $kjobs.=",";
                $kjobs.=$v;
            }
        }

        $tantou_jobs1 = "";
        if(!empty($datas["tantou_job1"])){
            foreach($datas["tantou_job1"] as $k => $v){
                if($k!=0) $tantou_jobs1.=",";
                $tantou_jobs1.=$v;
            }
        }

        $tantou_jobs2 = "";
        if(!empty($datas["tantou_job2"])){
            foreach($datas["tantou_job2"] as $k => $v){
                if($k!=0) $tantou_jobs2.=",";
                $tantou_jobs2.=$v;
            }
        }

        $tantou_jobs3 = "";
        if(!empty($datas["tantou_job3"])){
            foreach($datas["tantou_job3"] as $k => $v){
                if($k!=0) $tantou_jobs3.=",";
                $tantou_jobs3.=$v;
            }
        }

        $tenkins = "";
        if(!empty($datas["tenkin"])){
            foreach($datas["tenkin"] as $k => $v){
                if($k!=0) $tenkins.=",";
                $tenkins.=$v;
            }
            //$tenkins = $datas["tenkin"];
        }


        $entry="";
        $job_id="";
        if(!empty($datas["param"])){
            $entry = str_replace("'","", $datas["param"]);
            $entry = explode("|",$entry);
        }
        if(!empty($entry[1]) && is_numeric($entry[1])){
            $job_id = $entry[1];
        }else{
            $job_id="";
        }

        $karea1 = "";
        if(!empty($datas["kibouarea1"])){
            $karea1 = $datas["kibouarea1"];
        }
        $karea2 = "";
        if(!empty($datas["kibouarea2"])){
            $karea2 = $datas["kibouarea2"];
        }
        $karea3 = "";
        if(!empty($datas["kibouarea3"])){
            $karea3 = $datas["kibouarea3"];
        }

        $ip = $_SERVER["REMOTE_ADDR"];
        if($datas["pwdchk"]==1){
            $data = array(
                'hrbc_flg' => 1,
                'kibou_job' => $kjobs,
                'tenkin' => $tenkins,
                'income' => $datas["income"],
                'earnings' => $datas["earnings"],
                'kibou_area1' => $karea1,
                'kibou_area2' => $karea2,
                'kibou_area3' => $karea3,
                'company_name1' => $company_name1,
                'company_name2' => $company_name2,
                'company_name3' => $company_name3,
                'tantou_job1' => $tantou_jobs1,
                'tantou_job2' => $tantou_jobs2,
                'tantou_job3' => $tantou_jobs3,
                'start_year1' => $datas["start_year1"],
                'start_month1' => $datas["start_month1"],
                'start_year2' => $datas["start_year2"],
                'start_month2' => $datas["start_month3"],
                'start_year3' => $datas["start_year2"],
                'start_month3' => $datas["start_month3"],
                'end_year1' => $datas["end_year1"],
                'end_month1' => $datas["end_month1"],
                'end_year2' => $datas["end_year2"],
                'end_month2' => $datas["end_month3"],
                'end_year3' => $datas["end_year2"],
                'end_month3' => $datas["end_month3"],
                'status' => 3,
                'activate' => 1,
                'ip' => $ip
            );
        }else{
            $data = array(
                'hrbc_flg' => 1,
                'kibou_job' => $kjobs,
                'tenkin' => $tenkins,
                'income' => $datas["income"],
                'earnings' => $datas["earnings"],
                'company_name1' => $company_name1,
                'company_name2' => $company_name2,
                'company_name3' => $company_name3,
                'tantou_job1' => $tantou_jobs1,
                'tantou_job2' => $tantou_jobs2,
                'tantou_job3' => $tantou_jobs3,
                'start_year1' => $datas["start_year1"],
                'start_month1' => $datas["start_month1"],
                'start_year2' => $datas["start_year2"],
                'start_month2' => $datas["start_month3"],
                'start_year3' => $datas["start_year2"],
                'start_month3' => $datas["start_month3"],
                'end_year1' => $datas["end_year1"],
                'end_month1' => $datas["end_month1"],
                'end_year2' => $datas["end_year2"],
                'end_month2' => $datas["end_month3"],
                'end_year3' => $datas["end_year2"],
                'end_month3' => $datas["end_month3"],
                'status' => 3,
                'activate' => 1,
                'ip' => $ip
            );
        }
        $where = array(
            'id' => $ret->id
        );
        $ret = $this->db->update($this->database, $data, $where);
        if(empty($ret)){
            $msg = "エラーが発生しました。code:101";
        }

        $params=array();
        $params["person"] = $person_id;
        $params["resume"] = $resume_id;

        return $params;
    }

    //////////////////////////////////////////////////////////
    //登録完了画面
    //////////////////////////////////////////////////////////
    function complete(){
        $this->session->sess_destroy();

        //data設定
        $data["render"] = $this->segment."/complete";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["segment"] = $this->segment;

        $this->load->view("template_nologin", $data);
    }

    //////////////////////////////////////////////////////////
    //バリデーション
    //////////////////////////////////////////////////////////
    private function setValidationStep2($validation=null)
    {
        $errors ="";
        $this->form_validation->set_rules("shimei", "氏名","required");
        $this->form_validation->set_rules("kana", "ふりがな","required");
        $this->form_validation->set_rules("year", "年","required");
        $this->form_validation->set_rules("month", "月","required");
        $this->form_validation->set_rules("day", "日","required");
        $this->form_validation->set_rules("sex", "性別","required");
        $this->form_validation->set_rules("pref", "住所","required");
        $this->form_validation->set_rules("tel1", "電話番号","required");
        $this->form_validation->set_rules("school_div_id", "最終学歴","required");
        $this->form_validation->set_rules("school_name", "学校名","required");
        $this->form_validation->set_rules("company_number", "経験社数","required");
        $this->form_validation->set_rules("jokyo", "就業状況","required");
        $this->form_validation->set_rules("tenshoku_kiboubi", "転職希望日","required");
        $this->form_validation->set_rules("kibouarea1", "希望勤務地：第１希望","required");
        $this->form_validation->set_rules("agree", "個人情報保護方針","required");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }

    private function setValidationPwdStep2($validation=null)
    {
        $errors ="";
//        $this->form_validation->set_rules("kibouarea1", "希望勤務地：第１希望","required");
        $this->form_validation->set_rules("agree", "個人情報保護方針","required");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }

    private function setValidationStep3($validation=null)
    {
        $errors ="";
        $this->form_validation->set_rules("company_name1", "会社名１","required");
        $this->form_validation->set_rules("tantou_job1[]", "経験職種１","required");
        $this->form_validation->set_rules("start_year1", "勤務開始年１","required");
        $this->form_validation->set_rules("start_month1", "勤務開始月１","required");
        $this->form_validation->set_rules("kiboujob[]", "希望職種","required");
        $this->form_validation->set_rules("income", "希望年収","required");
        $this->form_validation->set_rules("earnings", "現在の年収","required");
        $this->form_validation->set_rules("tenkin[]", "転勤の可否","required");
        $this->form_validation->set_rules("earnings", "利用規約","required");


        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }

    private function setValidationStepPwd3($validation=null)
    {
        $errors ="";
        $this->form_validation->set_rules("kibouarea1", "希望勤務地：第１希望","required");
        $this->form_validation->set_rules("kiboujob[]", "希望職種","required");
        $this->form_validation->set_rules("income", "希望年収","required");
        $this->form_validation->set_rules("tenshoku_kiboubi", "転職希望日","required");
        $this->form_validation->set_rules("tenkin[]", "転勤の可否","required");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }


    //////////////////////////////////////////////////////////
    //メールアドレスチェック
    //////////////////////////////////////////////////////////
    private function setSendmailValidation($validation=null)
    {

        $errors ="";
        $this->form_validation->set_rules("email", "メールアドレス","required|valid_email");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }
}
