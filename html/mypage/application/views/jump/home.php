<h1>HUREXオフィシャルサイトへ移動します。</h1>
<div id="jumpPage">
        <div class="inner">
            <p class="aCenter pc">これよりアクセスしようとしているリンク先は<br>
            [HUREX マイページ]ではなく、[HUREXのオフィシャルサイト]になります。<br>
            求人詳細の問合せや、進捗管理の画面は[HUREXマイページ]のみの機能になります。<br>
            マイページの機能をお使いいただく場合は、マイページへ戻る必要がございますのでご了承ください。 </p>
            <p class="sp">これよりアクセスしようとしているリンク先は[HUREX マイページ]ではなく、[HUREXのオフィシャルサイト]になります。<br>
            求人詳細の問合せや、進捗管理の画面は[HUREXマイページ]のみの機能になります。<br>
            マイページの機能をお使いいただく場合は、マイページへ戻る必要がございますのでご了承ください。 </p>

<?php if($_GET["p"]==1):?>
            <p class="pageTitle aCenter"><a href="https://www.hurex.jp/">HUREXホームページ<br>
            （https://www.hurex.jp/）</a></p>
<?php elseif($_GET["p"]==2):?>            
            <p class="pageTitle aCenter"><a href="https://www.hurex.jp/campaign/">HUREX お知り合い紹介キャンペーン<br>
            （https://www.hurex.jp/campaign/）</a></p>
<?php elseif($_GET["p"]==3):?>            
            <p class="pageTitle aCenter"><a href="https://www.hurex.jp/guide/">HUREX 転職成功ガイド<br>
            （https://www.hurex.jp/guide/）</a></p>
<?php elseif($_GET["p"]==4):?>                        
            <p class="pageTitle aCenter"><a href="https://www.hurex.jp/column/resume-adviser/writing.html">HUREX 応募で差がつく職務経歴書<br>
            （https://www.hurex.jp/column/resume-adviser/writing.html）</a></p>
<?php elseif($_GET["p"]==5):?>            
            <p class="pageTitle aCenter"><a href="https://www.hurex.jp/success/">HUREX 成功者インタビュー<br>
            （https://www.hurex.jp/success/）</a></p>            
<?php elseif($_GET["p"]==6):?>            
            <p class="pageTitle aCenter"><a href="https://www.hurex.jp/interview/">HUREX [企業対談]地方創生<br>
            （https://www.hurex.jp/interview/）</a></p>
<?php elseif($_GET["p"]==7):?>            
            <p class="pageTitle aCenter"><a href="https://www.hurex.jp/company/privacy-policy/">HUREX 個人情報の取扱い<br>
            （https://www.hurex.jp/company/privacy-policy/）</a></p>
<?php elseif($_GET["p"]==8):?>            
            <p class="pageTitle aCenter"><a href="https://www.hurex.jp/company/responsibility/">HUREX 利用規約/免責事項<br>
            （https://www.hurex.jp/company/responsibility/）</a></p>
<?php elseif($_GET["p"]==9):?>            
            <p class="pageTitle aCenter"><a href="https://www.hurex.jp/inquiry/">HUREX お問い合わせ<br>
            （https://www.hurex.jp/inquiry/）</a></p>
<?php endif;?>
            <p class="close"><a href="javascript:;" onClick="window.close(); return false;">閉じる</a></p>
        </div>
    </div>