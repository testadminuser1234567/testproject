<h1>ご利用マニュアル</h1>
<!-- manualPage start -->
<div id="manualPage">
    <div class="inner">
	    <?php include(BaseDir.kanriDirectory.'/include/manual_navi.php'); ?>
            <h2>TOPページ</h2>
            <ul class="list">
                <li class="n01"><strong>オファー：ページ上部表示エリア</strong></li>
            </ul>
            <p class="pc aCenter"><img src="<?php echo base_url();?>images/manual/p02_img_01.jpg" alt=""/></p>
        <p class="sp">
            <img src="<?php echo base_url();?>images/manual/p02_img_01_sp.jpg" width="100%" alt=""/>
            </p>
            <ul class="list">
                <li class="na">現在のオファー件数が表示されます。</li>
                <li class="nb">オファーされた企業の求人情報を掲載しています。</li>
                <li class="nc">企業名が公開されている場合、企業名を掲載しています。</li>
            </ul>
            <ul class="list line">
                <li class="n02"><strong>応募状況</strong><br>
問合せ、応募などの応募状況を掲載しています。</li>
            </ul>
            <p class="pc aCenter"><img src="<?php echo base_url();?>images/manual/p02_img_02.jpg" alt=""/></p>
        <p class="sp">
            <img src="<?php echo base_url();?>images/manual/p02_img_02_sp.jpg" width="100%" alt=""/>
            </p>
            <ul class="list">
                <li class="nd">現在の応募状況（フェーズ）が表示されます。</li>
                <li class="ne">進行中の企業の求人情報を掲載しています。</li>
            </ul>
            <ul class="list line">
                <li class="n03"><strong>希望条件に合う求人</strong><br>
保存済の「希望条件」を満たす求人情報を新着順に掲載しています。</li>
            </ul>
            <p class="pc aCenter"><img src="<?php echo base_url();?>images/manual/p02_img_03.jpg" alt=""/></p>
        <p class="sp">
            <img src="<?php echo base_url();?>images/manual/p02_img_03_sp.jpg" width="100%" alt=""/>
        </p>
        <div class="clearfix">
            <p class="pctR mt25"><img src="<?php echo base_url();?>images/manual/p02_img_04.jpg" class="pc" alt=""/></p>
            <ul class="list line">
            <li class="n04"><strong>求人検索</strong><br>
    「地域」「職種」「転勤可否」を指定して検索することができます。<br>
    指定しない場合は新着順に求人情報が表示されます。</li>
            </ul>
            <p><img src="<?php echo base_url();?>images/manual/p02_img_04_sp.jpg" width="100%" class="sp" alt=""/></p>
        <p>※パソコンで閲覧する場合はページ右側に表示されます。<br>
※スマートフォンで閲覧する場合はページ下部に表示されます。</p>
        </div>
            <!-- btnBox start -->
            <ul class="btnBox clearfix">
                <li class="prev"><a href="<?php echo base_url();?>manual/page1">前へ</a></li>
            <li class="close"><a href="javascript:;" onClick="window.close(); return false;">閉じる</a></li>
                <li class="next"><a href="<?php echo base_url();?>manual/page3">次へ</a></li>
            </ul>
            <!-- btnBox start -->
        </div>
    </div>