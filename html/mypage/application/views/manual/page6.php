<h1>ご利用マニュアル</h1>
<!-- manualPage start -->
<div id="manualPage">
    <div class="inner">
	    <?php include(BaseDir.kanriDirectory.'/include/manual_navi.php'); ?>
            <h2>応募状況ページ</h2>
            <p>現状（フェーズ）ごとに、ご自身の応募された求人情報の一覧を表示しています。</p>
            <p class="pc aCenter"><img src="<?php echo base_url();?>images/manual/p06_img_01.jpg" alt=""/></p>
        <p class="sp">
            <img src="<?php echo base_url();?>images/manual/p06_img_01_sp.jpg" width="100%" alt=""/>
            </p>
            <ul class="list">
                <li>フェーズを選択すると、そのフェーズに対応した求人情報が表示されます。<br>
                    <strong>【フェーズ一覧】</strong><br>
                
                ・オファー：オファーを受けた求人情報を掲載します。<br>
                    ・問い合わせ中：現在、問い合わせ中の求人情報を掲載します。<br>
                ・応募中：現在、応募中の求人情報を掲載します。<br>
                
                ・面接：面接を予定している求人情報を掲載します。<br>
                ・内定：内定を受けた求人情報を掲載します。<br>
                ・辞退済み：過去に辞退した求人情報を掲載します。<br>
               ・お見送り：過去にお見送りした求人情報を掲載します。<br>
                ・募集終了：募集が終了した求人情報を掲載します。</li>
            </ul>
        <p class="pc aCenter"><img src="<?php echo base_url();?>images/manual/p06_img_02.jpg" alt=""/></p>
        <p class="sp">
            <img src="<?php echo base_url();?>images/manual/p06_img_02_sp.jpg" width="100%" alt=""/>
            </p>
        <ul class="list">
        <li class="na">求人情報名、[この求人の詳細をみる]をクリックすると詳細ページを閲覧することができます。</li>
        </ul>
            <h3>TOPページ応募状況との関連性</h3>
            <ol><li>TOPページで表示されるオファー（件数）は、フェーズ「オファー」を選択すると閲覧できます。</li>
                <li>TOPページで表示される応募中・問合せ中（件数）は、フェーズ「応募中」「問い合わせ中」を選択すると閲覧できます。</li>
            </ol>
            <h3>オファーを受けた求人について</h3>
            <p>オファーを受けた求人情報は、詳細ページに下記ボタンが表示されます。</p>
            <p class="pc aCenter"><img src="<?php echo base_url();?>images/manual/p06_img_03.jpg" alt=""/></p>
        <p class="sp">
            <img src="<?php echo base_url();?>images/manual/p06_img_03_sp.jpg" width="100%" alt=""/>
            </p>
            <ul class="point01">
                <li>[応募する] </li>
                <li>[辞退する]</li>
            </ul>
            <p>応募される場合は[応募する]をクリック、辞退される場合は[辞退する]をクリックしてください。</p>
        <h3>辞退した求人について</h3>
        <p>過去に辞退した求人情報は、一覧ページ・詳細ページ共に下記ボタン[やっぱり応募する]が表示されています。</p>
        <p class="pc aCenter"><img src="<?php echo base_url();?>images/manual/p06_img_04.jpg" alt=""/></p>
        <p class="sp">
            <img src="<?php echo base_url();?>images/manual/p06_img_04_sp.jpg" width="100%" alt=""/>
            </p>
            <p>再度、応募する場合は[やっぱり応募する]をクリックしてください。</p>
            <!-- btnBox start -->
            <ul class="btnBox clearfix">
                <li class="prev"><a href="<?php echo base_url();?>manual/page5">前へ</a></li>
            <li class="close"><a href="javascript:;" onClick="window.close(); return false;">閉じる</a></li>
                <li class="next"><a href="<?php echo base_url();?>manual/page7">次へ</a></li>
            </ul>
            <!-- btnBox start -->
        </div>
    </div>