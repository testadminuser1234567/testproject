<h1>ご利用マニュアル</h1>
<!-- manualPage start -->
<div id="manualPage">
    <div class="inner">
	    <?php include(BaseDir.kanriDirectory.'/include/manual_navi.php'); ?>
            <h2>求人検索ページ</h2>
            <p>任意の条件を指定して求人検索を行います。</p>
            <p><img src="<?php echo base_url();?>images/manual/p03_img_01.jpg" width="100%" class="pc" alt=""/>
            <img src="<?php echo base_url();?>images/manual/p03_img_01_sp.jpg" width="100%" class="sp" alt=""/>
            </p>
            <h3>検索条件</h3>
            <ul class="point01">
                <li>職種・勤務地・年収・転勤・フリーワード</li>
            </ul>
            <h3>検索方法</h3>
            <ol>
                <li>MENUから[求人検索]をクリック</li><li>別画面に遷移しますので条件を入力の上、[この条件で検索する]をクリックしてください。</li>
                <li>選択された条件を満たす求人情報一覧が表示されます。</li>
                <li>詳細ページを閲覧する場合は[この求人の詳細をみる]をクリックしてください。</li>
            </ol>
            <!-- btnBox start -->
            <ul class="btnBox clearfix">
                <li class="prev"><a href="<?php echo base_url();?>manual/page2">前へ</a></li>
            <li class="close"><a href="javascript:;" onClick="window.close(); return false;">閉じる</a></li>
                <li class="next"><a href="<?php echo base_url();?>manual/page4">次へ</a></li>
            </ul>
            <!-- btnBox start -->
        </div>
    </div>