<h1>ご利用マニュアル</h1>
<!-- manualPage start -->
<div id="manualPage">
    <div class="inner">
	    <?php include(BaseDir.kanriDirectory.'/include/manual_navi.php'); ?>
            <h2>気になる求人ページ</h2>
            <p>追加された気になる求人リストを確認することができます。</p>
            <h3>一覧ページ</h3>
            <p class="pc aCenter"><img src="<?php echo base_url();?>images/manual/p04_img_01.jpg" alt=""/></p>
        <p class="sp">
            <img src="<?php echo base_url();?>images/manual/p04_img_01_sp.jpg" width="100%" alt=""/>
            </p>
            <ul class="list">
                <li class="na">クリックすると詳細ページを閲覧することができます。</li>
                <li class="nb">クリックすると問合せができます。</li>
                <li class="nc">気になる求人リストから削除することができます。</li>
                <li class="nd">削除したい情報をチェックの上、まとめて削除することができます。</li>
            </ul>
            <h3>求人詳細ページ</h3>
            <p class="pc aCenter"><img src="<?php echo base_url();?>images/manual/p04_img_02.jpg" alt=""/></p>
        <p class="sp">
            <img src="<?php echo base_url();?>images/manual/p04_img_02_sp.jpg" width="100%" alt=""/>
            </p>
            <ul class="list">
                <li class="ne">クリックすると問合せができます。</li>
            </ul>
            <h3>問合せ後について</h3>
            <p>一覧ページ[問合せ]、もしくは求人詳細ページ[求人の詳細をきく]をクリックすると、問合せされます。<br>
            該当の求人情報は応募状況ページ（フェーズ：問い合わせ中）に格納されます。</p>
                <h3>気になる求人への追加方法</h3>
            <ol>
                    <li>求人検索を行ってください。 </li>
                    <li>[この求人の詳細をみる]をクリックし、求人詳細ページを閲覧します。</li>
                    <li>求人詳細ぺージ下部にある[気になる求人リストに保存]をクリックすると追加されます。</li>
            </ol>
            <!-- btnBox start -->
            <ul class="btnBox clearfix">
                <li class="prev"><a href="<?php echo base_url();?>manual/page3">前へ</a></li>
            <li class="close"><a href="javascript:;" onClick="window.close(); return false;">閉じる</a></li>
                <li class="next"><a href="<?php echo base_url();?>manual/page5">次へ</a></li>
            </ul>
            <!-- btnBox start -->
        </div>
    </div>