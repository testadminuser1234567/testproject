<h1>ご利用マニュアル</h1>
<!-- manualPage start -->
<div id="manualPage">
    <div class="inner">
	    <?php include(BaseDir.kanriDirectory.'/include/manual_navi.php'); ?>
            <h2>希望条件ページ</h2>
            <p>登録された希望条件を確認することができます。</p>
            <p class="pc aCenter"><img src="<?php echo base_url();?>images/manual/p05_img_01.jpg" alt=""/></p>
        <p class="sp">
            <img src="<?php echo base_url();?>images/manual/p05_img_01_sp.jpg" width="100%" alt=""/>
            </p>
            <h3>希望条件変更の流れ</h3>
            <ol>
                <li>[希望条件を変更]をクリックしてください。</li>
                <li>変更箇所をご入力の上、[入力した内容に変更する]をクリックしてください。</li>
            </ol>
            <!-- btnBox start -->
            <ul class="btnBox clearfix">
                <li class="prev"><a href="<?php echo base_url();?>manual/page4">前へ</a></li>
            <li class="close"><a href="javascript:;" onClick="window.close(); return false;">閉じる</a></li>
                <li class="next"><a href="<?php echo base_url();?>manual/page6">次へ</a></li>
            </ul>
            <!-- btnBox start -->
        </div>
    </div>