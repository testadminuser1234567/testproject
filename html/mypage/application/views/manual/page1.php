<h1>ご利用マニュアル</h1>
<!-- manualPage start -->
<div id="manualPage">
    <div class="inner">
	    <?php include(BaseDir.kanriDirectory.'/include/manual_navi.php'); ?>
            <h2>メニューについて</h2>
            <p><img src="<?php echo base_url();?>images/manual/p01_img_01.jpg" width="100%" class="pc" alt=""/>
            <img src="<?php echo base_url();?>images/manual/p01_img_01_sp.jpg" width="100%" class="sp" alt=""/>
            </p>
            <ul class="list">
                <li class="n01"><strong>[求人検索]</strong><br> 任意の条件を指定して求人検索が可能です。
                </li>
                <li class="n02"><strong>[気になる求人]</strong><br>
                     追加された気になる求人リストを表示します。
                   <br> 
                   ※気になるリストから応募、問合せを行うと応募状況ページに格納されますのでご注意ください。
                </li>
                <li class="n03"><strong>[希望条件</strong><strong>]</strong><br>
                     マイページ登録時、もしくは登録後保存された希望条件を確認・変更するページです。
               </li>
                <li class="n04"><strong>[応募状況]</strong><br>
                     <span class="n02">応募、問合せ</span>された求人情報の現状を確認することが可能です。 <br>
                弊社よりオファーを受けた求人情報が掲載されます。                </li>
                <li class="n05"><strong>[プロフィール]</strong><br>
                     登録されたプロフィールを確認・変更することが可能です。
               </li>
            </ul>
            <!-- btnBox start -->
            <ul class="btnBox clearfix">
                <li class="close"><a href="javascript:;" onClick="window.close(); return false;">閉じる</a>
                </li>
                <li class="next"><a href="<?php echo base_url();?>manual/page2">次へ</a>
                </li>
            </ul>
            <!-- btnBox start -->
        </div>
    </div>
<!-- manualPage end -->