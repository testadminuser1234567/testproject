    <section class="co">
        <div class="inner">
            <h1><span class="line">ページが見つかりません</span></h1>
            <p class="aCenter">お探しのページは削除されたか、リンクが間違っている可能性があります。<br>
            <a href="<?php echo base_url();?>user/home/" class="color">こちら</a>からトップページへお戻りください。</p>
        </div>
    </section>