<?php
//HRBCマスターと連動
$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);
?>
<div class="title clearfix">
<p class="tit2">会員情報を更新する</p>
<div class="naiyoToko">
<br/>
<p class="txt">▼下記の項目をご記入下さい。</p>
<div class="error"><?php echo $msg ?></div>
    <?php echo form_open_multipart('user/confirm');?>
<table border="0" cellspacing="0" cellpadding="0">

    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th class="<?php if(@${$segment}->shimei):?>ok<?php else:?>hissu<?php endif;?> shimei">氏名</th>
            <td>
                <?php echo form_input('shimei', @${$segment}->shimei);?>
            </td>
        </tr>
        <tr>
            <th class="<?php if(@${$segment}->kana):?>ok<?php else:?>hissu<?php endif;?> kana">ふりがな</th>
            <td>
                <?php echo form_input('kana', @${$segment}->kana);?>
            </td>
        </tr>
        <tr>
            <th class="<?php if(@${$segment}->year && @${$segment}->month && @${$segment}->day):?>ok<?php else:?>hissu<?php endif;?> birth">生年月日</th>
            <td>
                <?php
                $y= date('Y');
                $y_from = $y-18;
                $y_to = $y-63;
                $options_y = array(""=>"年");
                for($y=$y_from;$y>=$y_to;$y--){
                    $options_y[$y] = $y . "年";
                }
                $options_m=array(""=>"月");
                for($m=1;$m<=12;$m++){
                    $options_m[$m] = $m . "月";
                }
                $options_d=array(""=>"日");
                for($d=1;$d<=31;$d++){
                    $options_d[$d] = $d . "日";
                }
                ?>
                <?php echo form_dropdown('year', $options_y, @${$segment}->year);?>
                <?php echo form_dropdown('month', $options_m, @${$segment}->month);?>
                <?php echo form_dropdown('day', $options_d, @${$segment}->day);?>
            </td>
        </tr>
        <tr>
            <th class="<?php if(@${$segment}->sex):?>ok<?php else:?>hissu<?php endif;?> sex">性別</th>
            <td>
                <?php foreach($tmpgender['Item'] as $k=>$v):?>
                    <?php if(!empty($v['Item'])):?>
                        <?php foreach($v['Item'] as $k2=>$v2):?>
                            <input type="radio" name="sex" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(count($v['Item'])-1 == $k2):?>class="required"<?php endif;?>  <?php if(@${$segment}->sex==$v2['Option.P_Name']):?>checked<?php endif;?>  />

                            <?php echo htmlspecialchars($v2['Option.P_Name'], ENT_QUOTES, 'UTF-8');?>
                        <?php endforeach;?>
                    <?php endif;?>
                <?php endforeach;?>
            </td>
        </tr>
        <tr>
            <th class="<?php if(@${$segment}->pref):?>ok<?php else:?>hissu<?php endif;?> pref">住所</th>
            <td>

                <select id="pref" name="pref" class="hissu required">
                    <option value="" selected="selected" label="">▼お選びください</option>
                    <?php foreach($tmppref['Item'] as $k=>$v):?>
                        <?php if(!empty($v['Item'])):?>
                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                <?php if(!empty($v2['Items']['Item'])):?>
                                    <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                        <option value="<?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->pref==$v3['Option.P_Name']):?>selected<?php endif;?> ><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                        <?php set_value($v3['Option.P_Name']);?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>
                </select></td>
        </tr>
        <tr>
            <th class="<?php if(@${$segment}->tel1):?>ok<?php else:?>hissu<?php endif;?> tel1">電話</th>
            <td>
		<?php echo form_input(array('name'=>'tel1','type'=>'number', 'id'=>'tel1', 'value'=> @${$segment}->tel1));?>
            </td>
        </tr>

<tr>
<th class="<?php if(@${$segment}->mail1):?>ok<?php else:?>hissu<?php endif;?> mail1">メール</th>
<td><?php echo form_input('mail1', @${$segment}->mail1);?></td>
</tr>


        <tr>
            <th class="<?php if(@${$segment}->school_div_id):?>ok<?php else:?>hissu<?php endif;?> school_div_id">最終学歴</th>
            <td>
<?php if(!empty($tmpbackground['Item'])):?>
<?php foreach($tmpbackground['Item'] as $k=>$v):?>
<?php if(!empty($v['Item'])):?>
<?php foreach($v['Item'] as $k2=>$v2):?>
<label for="gakureki0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>" class="radiolabel gakurekilabel">
<input type="radio" class="radio gakurekiradio<?php if($k2-1==count($tmpbackground['Item'])):?> required<?php endif;?>" name="school_div_id" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->school_div_id==$v2['Option.P_Name']):?>checked<?php endif;?> id="gakureki0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>">
<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
</label>
<?php endforeach;?>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>
</td>
        </tr>
        <tr>
            <th class="<?php if(@${$segment}->company_number):?>ok<?php else:?>hissu<?php endif;?> company_number">経験社数</th>
            <td>
	<span class="hissu">
<label for="keiken01" class="radiolabel companylabel">
<input type="radio" class="radio compradio" name="company_number" value="1" <?php echo @${$segment}->company_number=="1" ? "checked" : "";?> id="keiken01" />
1社</label>
<label for="keiken02" class="radiolabel companylabel">
<input type="radio" class="radio compradio" name="company_number" value="2" <?php echo @${$segment}->company_number=="2" ? "checked" : "";?> id="keiken02" />
2社</label>
<label for="keiken03" class="radiolabel companylabel">
<input type="radio" class="radio compradio" name="company_number" value="3" <?php echo @${$segment}->company_number=="3" ? "checked" : "";?> id="keiken03" />
3社</label>
<label for="keiken04" class="radiolabel companylabel">
<input type="radio" class="radio compradio" name="company_number" value="4" <?php echo @${$segment}->company_number=="4" ? "checked" : "";?> id="keiken04" />
4社</label>
<label for="keiken05" class="radiolabel companylabel">
<input type="radio" class="radio compradio required" name="company_number" value="5" <?php echo @${$segment}->company_number=="5" ? "checked" : "";?> id="keiken05" />
5社以上</label>
</span>

<div id="company_number_err"></div>
            </td>
        </tr>
        <tr>
            <th class="<?php if(@${$segment}->jokyo):?>ok<?php else:?>hissu<?php endif;?> jokyo">就業状況</th>
            <td>
<?php if(!empty($tmpwork['Item'])):?>
<?php foreach($tmpwork['Item'] as $k=>$v):?>
<?php if(!empty($v['Item'])):?>
<?php foreach($v['Item'] as $k2=>$v2):?>
<label for="syugyou0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>" class="radiolabel jokyolabel">
<input type="radio" name="jokyo" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" class="radio jokyoradio<?php if(count($v['Item'])-1 == $k2):?> required<?php endif;?>" <?php if(@${$segment}->jokyo==$v2['Option.P_Name']):?>checked<?php endif;?> id="syugyou0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>"  />

<?php print_r($v2['Option.P_Name']);?>
</label>
<?php endforeach;?>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>


            </td>
        </tr>
        <tr>
            <th class="<?php if(@${$segment}->expectarea1):?>ok<?php else:?>hissu<?php endif;?> expectarea1">勤務地<br />
                （第1希望）</th>
            <td>

                <select id="expectarea1" name="expectarea1" class="hissu required">
                    <option value="" selected="selected" label="">お選びください</option>
                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                        <?php foreach($v['Item'] as $k2=>$v2):?>
                            <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                <option value="<?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->expectarea1==$v3['Option.P_Name']):?>selected<?php endif;?> ><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                <?php print_r($v3['Option.P_Name']);?>
                            <?php endforeach;?>
                        <?php endforeach;?>
                    <?php endforeach;?>
                </select></td>
        </tr>

        <tr>
            <th class="<?php if(@${$segment}->expectarea2):?>ok<?php else:?><?php endif;?> expectarea2">勤務地<br />
                （第2希望）</th>
            <td>

                <select id="expectarea2" name="expectarea2" class="" style="">
                    <option value="" selected="selected" label="">お選びください</option>
                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                        <?php foreach($v['Item'] as $k2=>$v2):?>
                            <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                <option value="<?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->expectarea2==$v3['Option.P_Name']):?>selected<?php endif;?> ><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                <?php print_r($v3['Option.P_Name']);?>
                            <?php endforeach;?>
                        <?php endforeach;?>
                    <?php endforeach;?>
                </select></td>
        </tr>

        <tr>
            <th class="<?php if(@${$segment}->expectarea3):?>ok<?php else:?><?php endif;?> expectarea3">勤務地<br />
                （第3希望）</th>
            <td>

                <select id="expectarea3" name="expectarea3" class="" style="">
                    <option value="" selected="selected" label="">お選びください</option>
                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                        <?php foreach($v['Item'] as $k2=>$v2):?>
                            <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                <option value="<?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->expectarea3==$v3['Option.P_Name']):?>selected<?php endif;?> ><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                <?php print_r($v3['Option.P_Name']);?>
                            <?php endforeach;?>
                        <?php endforeach;?>
                    <?php endforeach;?>
                </select></td>
        </tr>

        <tr class="<?php if(@${$segment}->comment):?>ok<?php else:?><?php endif;?> comment">
            <th>その他、<br />ご希望などございましたら<br />記入をお願いいたします</th>
            <td><?php echo form_textarea('comment', @${$segment}->comment);?></td>
        </tr>
        </table>
    </div>
</div>
<div class="toukou noLine clearfix">
<p class="b" style="text-align:center">
<?php echo form_hidden($csrf); ?>
<input type="submit" value="確認する" style="width: 150px; height: 30px;font-weight:bold;" onclick="return check(1);"  />
</div>
</div>
<?php echo form_close();?>
<script>
    /*
    function check(param){
        e1 = $("#expectarea1").val();
        e2 = $("#expectarea2").val();
        e3 = $("#expectarea3").val();

        if(e1 && e2){
            if(e1 == e2){
                $("#expectarea1").parent().append("<div class='error'>勤務地（第1希望）と勤務地（第2希望）は別の都道府県を選択してください。。</div>");
            }
        }
        if(e1 && e3){
            if(e1 == e3){
                $("#expectarea1").parent().append("<div class='error'>勤務地（第1希望）と勤務地（第3希望）は別の都道府県を選択してください。。</div>");
            }
        }
        if(e2 && e3){
            if(e2 == e3){
                $("#expectarea2").parent().append("<div class='error'>勤務地（第2希望）と勤務地（第3希望）は別の都道府県を選択してください。。</div>");
            }
        }
        //エラー処理
        if($("div.error").size() > 0){
            $('html,body').animate({ scrollTop: $("div.error:first").offset().top-150}, 'slow');
            return false;
        }else{
//            return true;
            return false;
        }
    }
    */
</script>