<script src="<?php echo base_url();?>js/jquery.tile.js"></script>
<script src="<?php echo base_url();?>js/js.cookie.js"></script>

<script>
    if ( window.matchMedia( 'screen and (min-width:769px)' ).matches ) {
        $(window).on('load resize',function() {
            $('#kibouList .entry').tile(2);
            $('#kibouList .entry p.date').tile(2);
            $('#kibouList .entry p.detail').tile(2);
        } );
    }


    $( window ).load( function () {
        $( '.list01 .entry:odd' ).addClass( 'odd' );
        $( '.list02 .entry:odd' ).addClass( 'odd' );
        $( '.list03 .entry:odd' ).addClass( 'odd' );
        $( '.list04 .entry:odd' ).addClass( 'odd' );
    } );

    $(function(){
        //manual
        manCheck = Cookies.get('hurexManual');

        if(manCheck!=1){
            $("#manualBox").show();
        }

        $(".close").click(function(){
            $("#manualBox").hide();
        });
        $("#manualBox").click(function(e){
            cls = e.target.className;

            if(!cls.match(/man/)){
                $("#manualBox").hide();
            }
        });

        $("#manualBtn").click(function(){
            val = $("[name=manualBtn]").prop("checked");
            if(val){
                Cookies.set('hurexManual', '1', { expires: 365 });
            }else{
                Cookies.remove('hurexManual');
            }
        });
    });



</script>
<!-- co start -->
<div id="co">
    <div id="coSub" class="clearfix">
        <main id="mainCo">
				<!--<div style="
    width: 100%;
    background: #fff;
    padding: 16px;
    box-sizing: border-box;
    border: solid 2px #ccc;
    border-radius: 9px;
    margin: 0 0 15px;
    text-align: center;
">
					<a href="https://www.hurex.jp/news/detail.html?id=126" target="_blank" style="
    color: #5784df;font-size: 14px;
">4月5日、6日の営業時間変更のお知らせ</a>
				</div>-->

            <?php if(!empty($_GET["e"]) && $_GET["e"]==301):?>
                <!-- cautionBox start -->
                <div id="cautionBox">
                    <div class="inner">
                        <h2 class="pc">既にお問い合わせ済みの求人です</h2>
                        <h2 class="sp">既にお問い合わせ<br>
                            済みの求人です</h2>
                        <p>お問い合わせ中の求人は「応募状況」から確認できます。ただし、過去にメールもしくは、お電話で回答させていただいた求人に関しては「応募状況」には表示されませんのでご注意ください。</p>
                    </div>
                </div>
                <!-- cautionBox end -->
            <?php endif;?>
            <!-- co end -->
            <?php
            $days=7; //NEWをつける日数
            $today=date("Y/m/d");
            ?>
            <!-- ouboList start -->
            <?php if(!empty($offer)):?>
                <div class="ouboList unread">
                    <div class="inner">
                        <?php $offer_i=0;?>
                        <h2><span>現在オファーが <strong><span class="number"><?php echo count($offer);?></span>件</strong> あります</span></h2>
                        <?php foreach($offer as $k=>$v):?>
                            <?php $date_diff=(strtotime($today)-strtotime(substr($v["updated"],0,10)))/(60*60*24)+1;?>
                            <?php if($offer_i < 3):?>
                                <div class="entry<?php if($date_diff<=8):?> new<?php endif;?>">
                                    <?php if($v["job"]["phaze"]==6):?>
                                        <a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v["job"]["job_id"], ENT_QUOTES, 'UTF-8');?>" class="clearfix">
                                            <p class="category"><span class="ico">オファー</span><?php if($date_diff<=8):?><span class="new pc">NEW!!</span><?php endif;?>更新日:<strong><?php echo htmlspecialchars(date('Y/m/d',strtotime($v["updated"])), ENT_QUOTES, 'UTF-8');?></strong></p>
                                            <p class="name">
                                                <?php //if($v["clientNameDisplay"]!="非公開"):?>
                                                <?php if ($v["phaze"] == "興味あり" || $v["phaze"] == "問合せ中" || $v["phaze"] == "WEBエントリー" || $v["phaze"] == "JOB打診依頼" || $v["phaze"] == "JOB打診（社名非公開）" || $v["phaze"] == "本人NG（社名非公開）" || $v["phaze"] == "社内NG予定（社名非公開）" || $v["phaze"] == "社内NG（社名非公開）" || $v["phaze"] == "JOBクローズ（社名非公開）" || $v["phaze"] == "期限切れ処理（社名非公開）" || $v["phaze"] == "JOB打診予定" || $v["phaze"] == "JOB打診NG" || $v["phaze"] == "JM対応用 社内NG（社名非公開）"):?>
                                                <?php else:?>

                                                    <?php echo htmlspecialchars($v["client"]["c_title"], ENT_QUOTES, 'UTF-8');?>
                                                <?php endif;?>
                                            </p>
                                            <?php $title = $v["job"]["job_title"];?>
                                            <p class="detail"><?php echo htmlspecialchars($title, ENT_QUOTES, 'UTF-8');?></p>
                                        </a>
                                    <?php else:?>
                                        <div class="phazeEnd clearfix" style="padding:13px 20px 13px 15px;">
                                            <p class="category"><span class="ico">募集終了</span><?php if($date_diff<=8):?><span class="new pc">NEW!!</span><?php endif;?>更新日:<strong><?php echo htmlspecialchars(date('Y/m/d',strtotime($v["updated"])), ENT_QUOTES, 'UTF-8');?></strong></p>
                                            <p class="name">
                                                <?php //if($v["clientNameDisplay"]!="非公開"):?>
                                                <?php if ($v["phaze"] == "興味あり" || $v["phaze"] == "問合せ中" || $v["phaze"] == "WEBエントリー" || $v["phaze"] == "JOB打診依頼" || $v["phaze"] == "JOB打診（社名非公開）" || $v["phaze"] == "本人NG（社名非公開）" || $v["phaze"] == "社内NG予定（社名非公開）" || $v["phaze"] == "社内NG（社名非公開）" || $v["phaze"] == "JOBクローズ（社名非公開）" || $v["phaze"] == "期限切れ処理（社名非公開）" || $v["phaze"] == "JOB打診予定" || $v["phaze"] == "JOB打診NG" || $v["phaze"] == "JM対応用 社内NG（社名非公開）"):?>
                                                <?php else:?>

                                                    <?php echo htmlspecialchars($v["client"]["c_title"], ENT_QUOTES, 'UTF-8');?>
                                                <?php endif;?>
                                            </p>
                                            <?php $title = $v["job"]["job_title"];?>
                                            <p class="detail" style="text-decoration:none;color:#545454;"><?php echo htmlspecialchars($title, ENT_QUOTES, 'UTF-8');?></p>
                                        </div>
                                    <?php endif;?>
                                </div>
                                <?php $offer_i++;?>
                            <?php endif;?>
                        <?php endforeach;?>
                        <!-- entry end -->
                        <?php if(count($offer)>3):?>
                            <p class="btn"><a href="<?php echo base_url();?>process/home/offer"><span>オファーを見る</span></a></p>
                        <?php endif;?>
                    </div>
                </div>
            <?php else:?>
                <!--<p>現在オファーは届いておりません。</p>-->
            <?php endif;?>
            <!-- ouboList end -->
            <!-- ouboList start -->
            <div class="ouboList">
                <div class="inner">
                    <h2><span>応募状況</span></h2>
                    <!-- entry start -->
                    <?php $apply_i =0;?>
                    <?php if(!empty($apply)):?>
                        <?php foreach($apply as $k=>$v):?>
                            <?php if($apply_i < 3):?>
                                <div class="entry">
                                    <?php if($v["job"]["phaze"]==$phaze):?>
                                        <a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v["job"]["job_id"], ENT_QUOTES, 'UTF-8');?>" class="clearfix">
                                            <p class="category"><span class="ico"><?php echo htmlspecialchars($v["phaze"], ENT_QUOTES, 'UTF-8');?></span>
                                                <?php if($v["phaze"]=="面接"):?>
                                                    面接日:<strong><?php if($v["opname"]=="書類OK"):?>調整中<?php else:?><?php echo htmlspecialchars(date('Y/m/d', strtotime($v["phasedate"])));?></strong><?php endif;?>
                                                <?php endif;?>
                                                <!-- 面接日:<strong>2018/02/15</strong>--></p>
                                            <?php $title = $v["job"]["job_title"];?>
                                            <p class="name">
                                                <?php //if($v["clientNameDisplay"]!="非公開"):?>
                                                <?php if ($v["phaze"] == "興味あり" || $v["phaze"] == "問合せ中" || $v["phaze"] == "WEBエントリー" || $v["phaze"] == "JOB打診依頼" || $v["phaze"] == "JOB打診（社名非公開）" || $v["phaze"] == "本人NG（社名非公開）" || $v["phaze"] == "社内NG予定（社名非公開）" || $v["phaze"] == "社内NG（社名非公開）" || $v["phaze"] == "JOBクローズ（社名非公開）" || $v["phaze"] == "期限切れ処理（社名非公開）" || $v["phaze"] == "JOB打診予定" || $v["phaze"] == "JOB打診NG" || $v["phaze"] == "JM対応用 社内NG（社名非公開）"):?>
                                                <?php else:?>
                                                    <?php echo htmlspecialchars($v["client"]["c_title"], ENT_QUOTES, 'UTF-8');?><?php endif;?></p>

                                            <p class="detail"><?php echo htmlspecialchars($title, ENT_QUOTES, 'UTF-8');?></p>
                                        </a>
                                    <?php else:?>
                                        <div class="phazeEnd clearfix" style="padding:13px 20px 13px 15px;">
                                            <p class="category"><span class="ico">募集終了</span></p>
                                            <?php $title = $v["job"]["job_title"];?><p class="name">
                                                <?php //if($v["clientNameDisplay"]!="非公開"):?>
                                                <?php if ($v["phaze"] == "興味あり" || $v["phaze"] == "問合せ中" || $v["phaze"] == "WEBエントリー" || $v["phaze"] == "JOB打診依頼" || $v["phaze"] == "JOB打診（社名非公開）" || $v["phaze"] == "本人NG（社名非公開）" || $v["phaze"] == "社内NG予定（社名非公開）" || $v["phaze"] == "社内NG（社名非公開）" || $v["phaze"] == "JOBクローズ（社名非公開）" || $v["phaze"] == "期限切れ処理（社名非公開）" || $v["phaze"] == "JOB打診予定" || $v["phaze"] == "JOB打診NG" || $v["phaze"] == "JM対応用 社内NG（社名非公開）"):?>
                                                    <?php echo htmlspecialchars($v["client"]["c_title"], ENT_QUOTES, 'UTF-8');?>
                                                <?php else:?>
                                                <?php endif;?></p>

                                            <p class="detail" style="text-decoration:none;color:#545454;"><?php echo htmlspecialchars($title, ENT_QUOTES, 'UTF-8');?></p>
                                        </div>
                                    <?php endif;?>
                                </div>
                                <?php $apply_i++;?>
                            <?php endif;?>
                        <?php endforeach;?>
                        <!-- entry end -->
                    <?php else:?>
                        <div class="txt">
                            <p>現在応募中の求人はございません<br><span class="ss">─応募した求人の進捗がここに表示されます─</span></p>
                        </div>
                    <?php endif;?>
                    <?php if(count($apply)>3):?>
                        <p class="btn"><a href="<?php echo base_url();?>process/home/"><span>応募状況をもっと見る</span></a></p>
                    <?php endif;?>
                    </p>
                </div>
            </div>
            <!-- ouboList end -->
            <!-- kibouList start -->
            <div id="kibouList">
                <div class="inner">
                    <p class="btn ss"><a href="<?php echo base_url();?>condition/home/"><span>希望条件を変更</span></a></p>
                    <h2><span>希望条件に合う求人</span></h2>
                    <?php $kibou_cnt=0;?>
                    <?php if(!empty($kibou_job2)):?>
                        <div id="entryList">
                            <!-- entry start -->
                            <?php foreach($kibou_job2 as $k => $v):?>
                                <?php if($kibou_cnt>=8) break;?>
                                <?php $date_diff=(strtotime($today)-strtotime(substr($v->j_updated,0,10)))/(60*60*24)+1;?>
                                <div class="entry">
                                    <a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id,ENT_QUOTES,'UTF-8');?>" class="clearfix">
                                        <p class="date"><?php echo htmlspecialchars(date('Y.m.d', strtotime($v->j_updated)),ENT_QUOTES,'UTF-8');?> 更新</p>
                                        <p class="detail"><?php echo htmlspecialchars($v->job_title,ENT_QUOTES,'UTF-8');?></p>
                                    </a>
                                </div>
                                <?php $kibou_cnt++;?>
                            <?php endforeach;?>
                            <!-- entry end -->
                        </div>
                    <?else:?>
                        <!-- txtBox start -->
                        <div class="txtBox">
                            <p>ご希望に合う求人はございませんでした。<br>
                                <a href="<?php echo base_url();?>search/home/">こちら</a>よりご希望条件を変更してください。</p>
                        </div>
                        <!-- txtBox end -->
                    <?php endif;?>
                    <?php if(!empty($kibou_job2) && count($kibou_job2) > 8):?>
                        <p class="btn"><a href="<?php echo base_url();?>search/home/"><span>もっと見る</span></a></p>
                    <?php endif;?>
                    <p class="txt"><a href="<?php echo base_url();?>condition/home/">希望条件を変更する→</a></p>
                </div>
            </div>
            <!-- kibouList end -->
        </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($looks)):?>
                <div id="historyBox">
                    <h2><span>最近閲覧した求人</span></h2>
                    <div class="inner">
                        <?php foreach($looks as $k=>$v):?>
                            <!-- entry start -->
                            <div class="entry">
                                <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                            </div>
                            <!-- entry end -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
            <!-- historyBox end -->
