<!-- co start -->
<script src="<?php echo base_url();?>js/jquery-1.6.4.min.js" charset="utf-8"></script>
<script src="<?php echo base_url();?>js/form_validation_ui.js?j=<?php echo date('Ymdhis');?>"></script>

<h1><?php echo $clean->purify($title); ?></h1>
<!-- co start -->
<div id="co">
    <div id="coSub" class="clearfix">
        <main id="mainCo">

            <div class="error"><?php echo $msg ?></div>
            <?php echo form_open_multipart('user/join_confirm');?>


            <div class="registInputPage form">

                <table>
                    <tr>
                        <th class="<?php if(@${$segment}->comment):?>ok<?php else:?>hissu<?php endif;?> comment">ご希望の参加日を<br />ご記入ください</th>
                        <td><?php echo form_textarea(array('id'=>'comment', 'name'=>'comment', 'value'=>@${$segment}->comment, 'class'=>"required", 'placeholder'=>'入力例：4月1日 16:00から'));?>
                            <div class="error"><?php echo $msg ?></div>

                        </td>
                    </tr>

                </table>
                <input type="hidden" name="p" value="<?php echo $clean->purify($param); ?>" />
                <div class="btnBox">
                    <button class="btn b02" onclick="return check(1);"><span>入力内容を確認する</span></button>
                </div>
                <?php echo form_close();?>
                <!-- conditionListt end -->
</div>
        </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($looks)):?>
                <div id="historyBox">
                    <h2><span>最近閲覧した求人</span></h2>
                    <div class="inner">
                        <?php foreach($looks as $k=>$v):?>
                            <!-- entry start -->
                            <div class="entry">
                                <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                            </div>
                            <!-- entry end -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
            <!-- historyBox end -->