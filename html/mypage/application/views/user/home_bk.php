    <script src="<?php echo base_url();?>js/js.cookie.js"></script>
    <script>
        $( window ).load( function () {
            $( '.list01 .entry:odd' ).addClass( 'odd' );
            $( '.list02 .entry:odd' ).addClass( 'odd' );
            $( '.list03 .entry:odd' ).addClass( 'odd' );
            $( '.list04 .entry:odd' ).addClass( 'odd' );
        } );

        $(function(){
	    //manual
            manCheck = Cookies.get('hurexManual');
            
            if(manCheck!=1){
                $("#manualBox").show();
            }
    	  
            $(".close").click(function(){
                $("#manualBox").hide();
            });
            $("#manualBox").click(function(e){
                cls = e.target.className;

                if(!cls.match(/man/)){
                     $("#manualBox").hide();
                }
            });

            $("#manualBtn").click(function(){
                val = $("[name=manualBtn]").prop("checked");
                if(val){
		    Cookies.set('hurexManual', '1', { expires: 365 });
                }else{
                    Cookies.remove('hurexManual');
                }
            });
        });
    </script>
    <!-- offerBox start -->
    <div id="offerBox">
	<?php if(!empty($offer)):?>
        <a href="<?php echo base_url();?>process/home/オファー">
        <h2><?php echo date('Y/m/d', strtotime($offer[0]["updated"]));?><span>最新のオファーが届いています。</span></h2>
        <p>あなたに合計<span class="point"><?php echo count($offer);?></span>件のオファーが届いています。</p>
        </a>
	<?php else:?>
	<p>現在オファーは届いておりません。</p>
	<?php endif;?>
    </div>
    <!-- offerBox end -->
<?php
$days=7; //NEWをつける日数
$today=date("Y/m/d");
?>
    <!-- co start -->
    <section class="co searchList list01">
        <h1>希望条件に合致する求人情報</h1>
        <div class="inner">
	<?php if(!empty($kibou_job2)):?>
	    <?php if($agent=="SP"):?>
                <div class="entryList"> 
                <!-- entry start -->
                <?php foreach($kibou_job1 as $k => $v):?>
                <?php $date_diff=(strtotime($today)-strtotime(substr($v->j_updated,0,10)))/(60*60*24)+1;?>
                <div class="entry">
                    <a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id,ENT_QUOTES,'UTF-8');?>" class="clearfix">
                        <p class="date"><?php echo htmlspecialchars(date('Y.m.d', strtotime($v->j_updated)),ENT_QUOTES,'UTF-8');?><?php if($date_diff<=8):?><span class="new sp">NEW!!</span><?php endif;?></p>
                        <p class="title"><?php echo htmlspecialchars($v->job_title,ENT_QUOTES,'UTF-8');?><?php if($date_diff<=8):?><span class="new pc">NEW!!</span><?php endif;?></p>
                    </a>
                </div>
                <?php endforeach;?>
                <!-- entry end -->
                <p class="more sp"><a href="javascript:;"><span>もっと見る</span></a></p>
                </div>
		<script>
			$(function(){
				$(".entryList").eq(1).hide();

				$(".more").click(function(){
					$(".entryList").eq(0).hide();
					$(".entryList").eq(1).show();
				});
				$(".close").click(function(){
					$(".entryList").eq(1).hide();
					$(".entryList").eq(0).show();
				});
			});
		</script>
	    <?php endif;?>
                <div class="entryList"> 
                <!-- entry start -->
                <?php foreach($kibou_job2 as $k => $v):?>
                <?php $date_diff=(strtotime($today)-strtotime(substr($v->j_updated,0,10)))/(60*60*24)+1;?>
                <div class="entry">
                    <a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id,ENT_QUOTES,'UTF-8');?>" class="clearfix">
                        <p class="date"><?php echo htmlspecialchars(date('Y.m.d', strtotime($v->j_updated)),ENT_QUOTES,'UTF-8');?><?php if($date_diff<=8):?><span class="new sp">NEW!!</span><?php endif;?></p>
                        <p class="title"><?php echo htmlspecialchars($v->job_title,ENT_QUOTES,'UTF-8');?><?php if($date_diff<=8):?><span class="new pc">NEW!!</span><?php endif;?></p>
                    </a>
                </div>
                <?php endforeach;?>
                <!-- entry end -->
                <p class="close sp"><a href="javascript:;"><span>閉じる</span></a></p>
                </div>
                <p class="btn aCenter"><a href="<?php echo base_url();?>search/home/">希望条件に合致する求人情報一覧を見る</a></p>
	<?else:?>
            <div class="entry">
	    <p>現在、登録している希望条件はございません。</p>
	    </div>
	<?php endif;?>
        </div>
    </section>
    <!-- co end -->
    <!-- co start -->
    <section class="co searchList">
        <h1>応募状況</h1>
        <div id="tab">
            <ul id="tabNavi" class="clearfix">
                <li><a href="#tabBox01" class="nopscr" id="tab01"><span>オファー件数<strong><?php echo htmlspecialchars(count($offer),ENT_QUOTES,'UTF-8');?></strong>件</span></a>
                </li>
                <li><a href="#tabBox02" class="nopscr" id="tab02"><span>応募件数<strong><?php echo htmlspecialchars(count($apply),ENT_QUOTES,'UTF-8');?></strong>件</span></a>
                </li>
            </ul>
	    <?php $offer_i=0;?>
            <div id="tabBox01" class="tabSub list02">
                <div class="inner">
                    <h2>オファー求人リスト<span>（更新順）</span></h2>
                    <!-- entry start -->
		    <?php if(!empty($offer)):?>
                    <?php foreach($offer as $k=>$v):?>
		    <?php if($offer_i < 6):?>
                    <div class="entry">
                        <a href="<?php echo base_url();?>process/home/オファー#<?php echo htmlspecialchars($v["job"]["job_id"], ENT_QUOTES, 'UTF-8');?>" class="clearfix">
                            <p class="title"><?php echo htmlspecialchars($v["job"]["job_title"], ENT_QUOTES, 'UTF-8');?></p>
                        </a>
                    </div>
		    <?php $offer_i++;?>
	  	    <?php endif;?>
                    <?php endforeach;?>
                    <!-- entry end -->
                    <p class="btn aCenter"><a href="<?php echo base_url();?>process/home/オファー">オファー求人一覧を見る</a></p>
		    <?php else:?>
		    <p>現在オファーは届いておりません。</p>
		    <?php endif;?>
                </div>
            </div>
	    <?php $apply_i =0;?>
            <div id="tabBox02" class="tabSub list03">
                <div class="inner">
                    <h2>応募求人リスト<span>（更新順）</span></h2>
                    <!-- entry start -->
		    <?php if(!empty($apply)):?>
                    <?php foreach($apply as $k=>$v):?>
		    <?php if($apply_i < 6):?>
                    <div class="entry">
                        <a href="<?php echo base_url();?>process/home/応募中#<?php echo htmlspecialchars($v["job"]["job_id"], ENT_QUOTES, 'UTF-8');?>" class="clearfix">
                            <p class="title"><?php echo htmlspecialchars($v["job"]["job_title"], ENT_QUOTES, 'UTF-8');?></p>
                        </a>
                    </div>
		    <?php $apply_i++;?>
	  	    <?php endif;?>
                    <?php endforeach;?>
                    <!-- entry end -->
                    <p class="btn aCenter"><a href="<?php echo base_url();?>process/home/応募中">応募求人一覧を見る</a></p>
		    <?php else:?>
		    <p>現在、応募している求人はございません。</p>
		    <?php endif;?>
                </div>
            </div>
        </div>
    </section>
<?php if(count($offer)==0 && count($apply) >0 ):?>
<script>
$(window).load(function(){
	$("#tab02").click();
})
</script>
<?php endif;?>
    <!-- co end -->
    <!-- co start -->
<?php
//HRBCマスターと連動

//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);
$job_ary=array();
if(!empty($tmpjob['Item'])){
	foreach($tmpjob['Item'] as $k=>$v){
		if(!empty($v['Item'])){
			foreach($v['Item'] as $k2=>$v2){
				$job_ary[] = $v2;
			}
		}
	}
	foreach ((array) $job_ary as $key => $value) {
		$sort[$key] = $value["Option.P_Order"];
	}
}
array_multisort($sort, SORT_ASC, $job_ary);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

?>
    <section class="co searchList list04">
        <h1>地域と職種で検索
            <div id="selectList">
                <select name="pref" id="prefS" class="changeSel">
<option value="">地域選択</option>
<?php if(!empty($tmparea['Item'])):?>
<?php foreach($tmparea['Item'] as $k=>$v):?>
<?php if(!empty($v['Item'])):?>
<?php foreach($v['Item'] as $k2=>$v2):?>
<?php if($v2['Option.P_Name']!="勤務地不問"):?>
<?php if($v2['Option.P_Name']=="海外"):?>
<option value="<?php echo htmlspecialchars($v2['Option.P_Id'],ENT_QUOTES,'UTF-8');?>"><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
<?php endif;?>
<?php if(!empty($v2["Items"]['Item'])):?>
<?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
<option value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>"><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
<?php endforeach;?>
<?php endif;?>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>        
                </select>
                <select name="job" id="jobS" class="changeSel">
<option value="">職種選択</option>
                    <?php if(!empty($job_ary)):?>
                        <?php foreach($job_ary as $k=>$v):?>
                                                <option name="job[]" value="<?php echo htmlspecialchars($v['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty($job)):?><?php foreach($job as $jk=>$jv):?><?php if($jv==$v["Option.P_Id"]):?>selectd<?php endif;?><?php endforeach;?><?php endif;?>><?php echo htmlspecialchars($v['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                        <?php endforeach;?>
                    <?php endif;?>
	        </select>
                <select name="tenkin" id="tenkinS" class="changeSel">
<option value="">転勤可否</option>
<option value="可">可</option>
<option value="不可">不可</option>
                </select>
            </div>
        </h1>
	<?php if(!empty($jobs)):?>
        <div class="inner" id="searchCon">
	    <div id="entryArea">
            <!-- entry start -->
	    <?php foreach($jobs as $k=> $v):?>
	    <?php $date_diff=(strtotime($today)-strtotime(substr($v->j_updated,0,10)))/(60*60*24)+1;?>
            <div class="entry jobs">
                <a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id,ENT_QUOTES,'UTF-8');?>" class="clearfix">
                    <p class="date"><?php echo htmlspecialchars(date('Y.m.d', strtotime($v->j_updated)), ENT_QUOTES, 'UTF-8');?><?php if($date_diff<=8):?><span class="new sp">NEW!!</span><?php endif;?></p>
                    <p class="title"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, 'UTF-8');?><?php if($date_diff<=8):?><span class="new pc">NEW!!</span><?php endif;?>
                    </p>
                </a>
            </div>
	    <?php endforeach;?>
	    </div>
            <!-- entry end -->
            <p class="btn aCenter searchBtn"><a href="javascript:void(0);" class="search">この条件の求人一覧を見る</a>
            </p>
        </div>
	<?php else:?>
	<p class="jobs">現在、求人情報はございません。</p>
	<?php endif;?>
	<form name="searchF"method="get" action="<?php echo base_url();?>search/index/">
        <input type="hidden" name="job[]" id="job" />
        <input type="hidden" name="pref[]" id="pref" />
        <input type="hidden" name="tenkin" id="tenkin" />
	<input type="hidden" name="csrf_test_name" value="<?php echo $this->security->get_csrf_hash();?>" />
	</form>
    </section>
    <!-- co end -->
    <!-- co start -->
<section class="co link">
        <h1>関連リンク</h1>
        <div class="inner">
            <ul class="clearfix">
                <li><a href="<?php echo BaseUrl;?>jump/url?p=2" target="_blank"><img src="<?php echo base_url();?>images/bnr_campaign.jpg" alt="お知り合い紹介キャンペーン"/></a></li>
                <li><a href="<?php echo BaseUrl;?>jump/url?p=3" target="_blank"><img src="<?php echo base_url();?>images/bnr_guide.jpg" alt="転職成功ガイド"/></a></li>
                <li><a href="<?php echo BaseUrl;?>jump/url?p=4" target="_blank"><img src="<?php echo base_url();?>images/bnr_keireki.jpg" alt="応募で差が付く職務経歴書"/></a></li>
                <li><a href="<?php echo BaseUrl;?>jump/url?p=5" target="_blank"><img src="<?php echo base_url();?>images/bnr_interview.jpg" alt="成功者インタビュー"/></a></li>
                <li><a href="<?php echo BaseUrl;?>jump/url?p=6" target="_blank"><img src="<?php echo base_url();?>images/bnr_chihousousei.jpg" alt="企業対談 地方創生"/></a></li>
            </ul>
    </div>
    </section>
    <!-- co end -->
        <script>
            $(function(){
		//ジョブ検索へ
		$(".searchBtn").click(function(){
		    pref = $("#pref").val();
		    job = $("#job").val();
		    tenkin = $("#tenkin").val();

		    if(pref){
			    document.searchF.pref.value=pref;
		    }else{
			    $("#pref").remove();
		    }
		    if(job){
			    document.searchF.job.value=job;
		    }else{
			    $("#job").remove();
		    }
		    if(tenkin){
			    document.searchF.tenkin.value=tenkin;
		    }else{
			    $("#tenkin").remove();
		    }
		    document.searchF.submit();
		});

                //検索
                $(".changeSel").change(function() {
		    pref = $("#prefS").val();
		    job = $("#jobS").val();
		    tenkin = $("#tenkinS").val();
		    $("#pref").val(pref);
		    $("#job").val(job);
		    $("#tenkin").val(tenkin);

                    // フォームの送信データをAJAXで取得する
                    var form_data = {
                        pref: pref,
                        job: job,
                        tenkin: tenkin,
		        income: '',
                        ajax: "1",
			csrf_test_name: "<?php echo $this->security->get_csrf_hash();?>"
                    };
                    // jQueryのAJAXファンクションを利用
                    $.ajax({
                        url: "<?php echo base_url();?>user/getJob/",
                        type: "POST",
                        data: form_data,

                        // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                        success: function(data){
				$(".jobs").remove();
				$("#entryArea").append(data);
		                $( '.list01 .entry:odd' ).addClass( 'odd' );
		                $( '.list02 .entry:odd' ).addClass( 'odd' );
		                $( '.list03 .entry:odd' ).addClass( 'odd' );
            			$( '.list04 .entry:odd' ).addClass( 'odd' );
                        },
                        error:function(XMLHttpRequest, textStatus, errorThrown) {
                            alert("求人の取得に失敗しました");
                        }
                    });

                    return false;
                })

            })
        </script>