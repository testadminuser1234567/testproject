<script src="<?php echo base_url();?>js/jquery-1.6.4.min.js" charset="utf-8"></script>
<script src="<?php echo base_url();?>js/form_validation.js?j=<?php echo date('Ymdhis');?>"></script>
<h1><span class="line">パスワード再発行</span></h1>
<div id="registPage">
    <?php echo form_open_multipart('user/new_password');?>
    <div id="mailBox">
        <p class=""><?php echo form_password(array('name'=>"password", 'placeholder'=>"パスワード（※半角英数8～20文字）", 'value'=>"", 'class'=>"required txt", 'id'=>'password'));?>
        </p>
        <p><?php echo form_password(array('name'=>"password_check", 'placeholder'=>"パスワード（再入力）", 'value'=>"", 'class'=>"required txt", 'id'=>'password_check'));?>
        </p>
        <div class="btn aCenter">
            <input type="hidden" name="code" value="<?php echo @$code;?>" />
            <button class="btnLogin" onclick="return check(1);"><span>パスワードを再発行する</span></button>
        </div>
        <?php echo form_close();?>

    </div>
</div>