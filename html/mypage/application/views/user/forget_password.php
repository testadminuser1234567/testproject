<h1><span class="line">パスワード再発行</span></h1>
<div id="registPage">
        <?php echo form_open_multipart('user/send_mail');?>
        <div id="mailBox">
                <p><input type="text" class="txt required mail" name="email" placeholder="メールアドレス">
                </p>
                <div class="error"><?php echo $clean->purify($msg); ?></div>
<div class="aCenter">
                <button class="btnLogin" onclick="return check(1);"><span>送信する</span></button>
            </div>
        </div>
        <?php echo form_close();?>
</div>
<script>
        var data = {};
        data.email="メールアドレス";
        function check(param){
                $("div.error").remove();
                $("div.mailerr").remove();
                window.onbeforeunload=null;
                //テキスト、テキストエリアのチェック
                $(":text, textarea").filter(".required").each(function(){
                        if($(this).val()==""){
                                tmp = $(this).attr("id");
                                $(this).parent().append("<div class='error'>メールアドレスを入力してください。</div>");
                        }

                        //メールアドレスチェック
                        $(this).filter(".mail").each(function(){
                                if($(this).val() && !$(this).val().match(/.+@.+\..+/g)){
                                        $(this).parent().append("<div class='error'>メールアドレスの形式が異なります。</div>");
                                }
                        });
                });

                //エラー処理
                if($("div.error").size() > 0){
                        $('html,body').animate({ scrollTop: $("div.error:first").offset().top-150}, 'slow');
                        return false;
                }else{
                        return true;
                }
        }

</script>