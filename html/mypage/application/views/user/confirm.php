<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);
$job_ary=array();
if(!empty($tmpjob['Item'])){
    foreach($tmpjob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $job_ary as $key => $value) {
        $sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($sort, SORT_ASC, $job_ary);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

/*
$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);
$tantou_job_ary=array();
if(!empty($tmptantoujob['Item'])){
	foreach($tmptantoujob['Item'] as $k=>$v){
		if(!empty($v['Item'])){
			foreach($v['Item'] as $k2=>$v2){
				$tantou_job_ary[] = $v2;
			}
		}
	}
	foreach ((array) $tantou_job_ary as $key => $value) {
		$tantou_sort[$key] = $value["Option.P_Order"];
	}
}
array_multisort($tantou_sort, SORT_ASC, $tantou_job_ary);
*/
//担当職種（新）
$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);

$start=date('Y');
$end=$start-65;
?>
<h1>プロフィール</h1>
<div id="co">
    <div id="coSub" class="clearfix">
        <main id="mainCo">
            <div class="registInputPage form">
                <p>以下の内容で登録します。</p>
                <table class="confirm">
                    <tr>
                        <th class="<?php if(@${$segment}->shimei):?>ok<?php else:?>hissu<?php endif;?> shimei">氏名</th>
                        <td>
                            <?php echo set_value('shimei'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="<?php if(@${$segment}->kana):?>ok<?php else:?>hissu<?php endif;?> kana">ふりがな</th>
                        <td>
                            <?php echo set_value('kana'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="<?php if(@${$segment}->year && @${$segment}->month && @${$segment}->day):?>ok<?php else:?>hissu<?php endif;?> birth">生年月日</th>
                        <td>
                            <?php echo set_value('year'); ?>年<?php echo set_value('month'); ?>月<?php echo set_value('day'); ?>日
                        </td>
                    </tr>
                    <tr>
                        <th class="<?php if(@${$segment}->sex):?>ok<?php else:?>hissu<?php endif;?> sex">性別</th>
                        <td>
                            <?php echo set_value('sex'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="<?php if(@${$segment}->kpref):?>ok<?php else:?>hissu<?php endif;?> pref">住所</th>
                        <td><?php echo set_value('kpref'); ?></td>
                    </tr>
                    <tr>
                        <th class="<?php if(@${$segment}->tel1):?>ok<?php else:?>hissu<?php endif;?> tel1">電話</th>
                        <td>
                            <?php echo set_value('tel1'); ?>
                        </td>
                    </tr>
                    <tr>
                        <th class="<?php if(@${$segment}->school_div_id):?>ok<?php else:?>hissu<?php endif;?> school_div_id">最終学歴</th>
                        <td>
                            <?php echo set_value('school_div_id'); ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->school_name):?>ok<?php else:?>hissu<?php endif;?> school_name">学校名/学部/学科</th>
                        <td>
                            <?php echo set_value('school_name'); ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->company_number):?>ok<?php else:?>hissu<?php endif;?> company_number">経験社数</th>
                        <td>
                            <?php echo set_value('company_number'); ?>社
                        </td>
                    </tr>
                    <tr>
                        <th class="<?php if(@${$segment}->jokyo):?>ok<?php else:?>hissu<?php endif;?> jokyo">就業状況</th>
                        <td>
                            <?php echo set_value('jokyo'); ?>
                        </td>
                    </tr>






                    <tr>
                        <th class="<?php if(@${$segment}->comment):?>ok<?php else:?><?php endif;?> comment">その他、<br />ご希望などございましたら<br />記入をお願いいたします</th>
                        <td><?php echo set_value('comment'); ?></td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->mail1):?>ok<?php else:?>hissu<?php endif;?> mail1">メール</th>
                        <td>
                            <?php echo set_value('mail1'); ?>
                        </td>
                    </tr>
                    <?php if(!empty(${$segment}->password)):?>
                        <tr>
                            <th  class="<?php if(@${$segment}->password):?>ok<?php else:?>nini<?php endif;?> password">パスワード</th>
                            <td><?php echo str_repeat('*', strlen($this->session->userdata("pass")));?></td>
                        </tr>
                    <?php endif;?>


                    <!-- 会社1 -->
                    <tr>
                        <th class="<?php if(@${$segment}->company_name1):?>ok<?php else:?>hissu<?php endif;?> company_name1">会社名１</th>
                        <td>
                            <?php echo set_value('company_name1'); ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->tantou_job1):?>ok<?php else:?>hissu<?php endif;?> tantou_job1">経験職種１</th>
                        <td class="">
                            <?php
                            $flg=array();
                            $datas=array();
                            if(!empty($tmptantoujob)){
                                foreach($tmptantoujob as $k=>$v){
                                    if(!empty($v["Items"])){
                                        foreach($v["Items"] as $k2=>$v2){
                                            if(!empty($v2)){
                                                foreach($v2 as $k3=>$v3){

                                                    $datas[$k3]["title"] = $v3["Option.P_Name"];
                                                    $datas[$k3]["id"] = $v3["Option.P_Id"];
                                                    if(!empty($v3["Items"])){
                                                        foreach($v3["Items"] as $k4=>$v4){
                                                            if(!empty($v4)){
                                                                foreach($v4 as $k5=>$v5){
                                                                    foreach(${$segment}->tantou_job1 as $jk=>$jv){
                                                                        if($jv==$v5["Option.P_Id"]){
                                                                            $flg[$k3] = 1;
                                                                        }
                                                                    }
                                                                    $datas[$k3][$k5]["title"] = $v5["Option.P_Name"];
                                                                    $datas[$k3][$k5]["id"] = $v5["Option.P_Id"];
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            ?>
                            <?php if(!empty($datas)):?>
                                <?php foreach($datas as $k=>$v):?>
                                    <?php if($flg[$k]==1):?><?php if($k!=0):?><br /><?php endif;?>
                                        【<?php echo htmlspecialchars($v["title"],ENT_QUOTES,'UTF-8');?>】<br />
                                        <?php foreach($v as $k2 => $v2):?>
                                            <?php if(is_array($v2)):?>
                                                <?php foreach($_POST["tantou_job1"] as $jk=>$jv):?><?php if($jv==$v2["id"]):?><?php echo htmlspecialchars($v2['title'],ENT_QUOTES,'UTF-8');?><br /><?php endif;?><?php endforeach;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->start_year1 && @${$segment}->start_month1):?>ok<?php else:?>hissu<?php endif;?> start_year1">勤務開始１</th>
                        <td>
                            <?php echo set_value('start_year1'); ?><?php if(!empty($_POST["start_year1"])):?>年<?php endif;?><?php echo set_value('start_month1'); ?><?php if(!empty($_POST["start_month1"])):?>月<?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->end_year1 && @${$segment}->end_month1):?>ok<?php else:?><?php endif;?> end_year1">勤務終了１</th>
                        <td>
                            <?php echo set_value('end_year1'); ?><?php if(!empty($_POST["end_year1"])):?>年<?php endif;?><?php echo set_value('end_month1'); ?><?php if(!empty($_POST["end_month1"])):?>月<?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->earnings):?>ok<?php else:?><?php endif;?> earnings">現在の年収</th>
                        <td>
                            <?php if(empty(${$segment}->earnings)):?>指定なし
                            <?php else:?>
                                <?php if(!empty($tmpincome['Item'])):?>
                                    <?php foreach($tmpincome['Item'] as $k=>$v):?>
                                        <?php if(!empty($v['Item'])):?>
                                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                                <?php if($_POST["earnings"]==$v2['Option.P_Id']):?><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br /><?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            <?php endif;?>
                        </td>
                    </tr>

                    　　　　　　<!-- 会社２ -->
                    <tr>
                        <th class="<?php if(@${$segment}->company_name2):?>ok<?php else:?><?php endif;?> company_name1">会社名２</th>
                        <td>
                            <?php echo set_value('company_name2'); ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->tantou_job2):?>ok<?php else:?><?php endif;?> tantou_job1">経験職種２</th>
                        <td class="">
                            <?php
                            $flg=array();
                            $datas=array();
                            if(!empty($tmptantoujob)){
                                foreach($tmptantoujob as $k=>$v){
                                    if(!empty($v["Items"])){
                                        foreach($v["Items"] as $k2=>$v2){
                                            if(!empty($v2)){
                                                foreach($v2 as $k3=>$v3){

                                                    $datas[$k3]["title"] = $v3["Option.P_Name"];
                                                    $datas[$k3]["id"] = $v3["Option.P_Id"];
                                                    if(!empty($v3["Items"])){
                                                        foreach($v3["Items"] as $k4=>$v4){
                                                            if(!empty($v4)){
                                                                foreach($v4 as $k5=>$v5){
                                                                    foreach(${$segment}->tantou_job2 as $jk=>$jv){
                                                                        if($jv==$v5["Option.P_Id"]){
                                                                            $flg[$k3] = 1;
                                                                        }
                                                                    }
                                                                    $datas[$k3][$k5]["title"] = $v5["Option.P_Name"];
                                                                    $datas[$k3][$k5]["id"] = $v5["Option.P_Id"];
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            ?>
                            <?php if(!empty($datas)):?>
                                <?php foreach($datas as $k=>$v):?>
                                    <?php if($flg[$k]==1):?><?php if($k!=0):?><br /><?php endif;?>
                                        【<?php echo htmlspecialchars($v["title"],ENT_QUOTES,'UTF-8');?>】<br />
                                        <?php foreach($v as $k2 => $v2):?>
                                            <?php if(is_array($v2)):?>
                                                <?php foreach($_POST["tantou_job2"] as $jk=>$jv):?><?php if($jv==$v2["id"]):?><?php echo htmlspecialchars($v2['title'],ENT_QUOTES,'UTF-8');?><br /><?php endif;?><?php endforeach;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->start_year2 && @${$segment}->start_month2):?>ok<?php else:?><?php endif;?> start_year2">勤務開始２</th>
                        <td>
                            <?php echo set_value('start_year2'); ?><?php if(!empty($_POST["start_year2"])):?>年<?php endif;?><?php echo set_value('start_month2'); ?><?php if(!empty($_POST["start_month2"])):?>月<?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->end_year2 && @${$segment}->end_month2):?>ok<?php else:?><?php endif;?> end_year2">勤務終了２</th>
                        <td>
                            <?php echo set_value('end_year2'); ?><?php if(!empty($_POST["end_year2"])):?>年<?php endif;?><?php echo set_value('end_month2'); ?><?php if(!empty($_POST["end_month2"])):?>月<?php endif;?>
                        </td>
                    </tr>

                    　　　　　　<!-- 会社３ -->
                    <tr>
                        <th class="<?php if(@${$segment}->company_name3):?>ok<?php else:?><?php endif;?> company_name1">会社名３</th>
                        <td>
                            <?php echo set_value('company_name3'); ?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->tantou_job3):?>ok<?php else:?><?php endif;?> tantou_job1">経験職種３</th>
                        <td class="">
                            <?php
                            $flg=array();
                            $datas=array();
                            if(!empty($tmptantoujob)){
                                foreach($tmptantoujob as $k=>$v){
                                    if(!empty($v["Items"])){
                                        foreach($v["Items"] as $k2=>$v2){
                                            if(!empty($v2)){
                                                foreach($v2 as $k3=>$v3){

                                                    $datas[$k3]["title"] = $v3["Option.P_Name"];
                                                    $datas[$k3]["id"] = $v3["Option.P_Id"];
                                                    if(!empty($v3["Items"])){
                                                        foreach($v3["Items"] as $k4=>$v4){
                                                            if(!empty($v4)){
                                                                foreach($v4 as $k5=>$v5){
                                                                    foreach(${$segment}->tantou_job3 as $jk=>$jv){
                                                                        if($jv==$v5["Option.P_Id"]){
                                                                            $flg[$k3] = 1;
                                                                        }
                                                                    }
                                                                    $datas[$k3][$k5]["title"] = $v5["Option.P_Name"];
                                                                    $datas[$k3][$k5]["id"] = $v5["Option.P_Id"];
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            ?>
                            <?php if(!empty($datas)):?>
                                <?php foreach($datas as $k=>$v):?>
                                    <?php if($flg[$k]==1):?><?php if($k!=0):?><br /><?php endif;?>
                                        【<?php echo htmlspecialchars($v["title"],ENT_QUOTES,'UTF-8');?>】<br />
                                        <?php foreach($v as $k2 => $v2):?>
                                            <?php if(is_array($v2)):?>
                                                <?php foreach($_POST["tantou_job3"] as $jk=>$jv):?><?php if($jv==$v2["id"]):?><?php echo htmlspecialchars($v2['title'],ENT_QUOTES,'UTF-8');?><br /><?php endif;?><?php endforeach;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->start_year3 && @${$segment}->start_month3):?>ok<?php else:?><?php endif;?> start_year3">勤務開始３</th>
                        <td>
                            <?php echo set_value('start_year3'); ?><?php if(!empty($_POST["start_year3"])):?>年<?php endif;?><?php echo set_value('start_month3'); ?><?php if(!empty($_POST["start_month3"])):?>月<?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->end_year3 && @${$segment}->end_month3):?>ok<?php else:?><?php endif;?> end_year3">勤務終了３</th>
                        <td>
                            <?php echo set_value('end_year3'); ?><?php if(!empty($_POST["end_year3"])):?>年<?php endif;?><?php echo set_value('end_month3'); ?><?php if(!empty($_POST["end_month3"])):?>月<?php endif;?>
                        </td>
                    </tr>




                </table>

                <!-- start -->
                <div class="confirmBtnBox clearfix">
                    <div class="send">
                        <?php echo form_open('user/add/');?>
                        <p class="btnSend">
                            <input type="submit" value="プロフィールを確定する" onclick="window.onbeforeunload=null;">
                        </p>
                        <?php echo form_hidden('mode', 'send'); ?>
                        <?php echo form_hidden('shimei', ${$segment}->shimei); ?>
                        <?php echo form_hidden('kana', ${$segment}->kana); ?>
                        <?php echo form_hidden('year', ${$segment}->year); ?>
                        <?php echo form_hidden('month', ${$segment}->month); ?>
                        <?php echo form_hidden('day', ${$segment}->day); ?>
                        <?php echo form_hidden('sex', ${$segment}->sex); ?>
                        <?php echo form_hidden('kpref', ${$segment}->kpref); ?>
                        <?php echo form_hidden('tel1', ${$segment}->tel1); ?>
                        <?php echo form_hidden('mail1', ${$segment}->mail1); ?>
                        <?php echo form_hidden('school_div_id', ${$segment}->school_div_id); ?>
                        <?php echo form_hidden('school_name', ${$segment}->school_name); ?>
                        <?php echo form_hidden('jokyo', ${$segment}->jokyo); ?>
                        <?php echo form_hidden('comment', ${$segment}->comment); ?>
                        <?php echo form_hidden('company_number', ${$segment}->company_number); ?>
                        <?php echo form_hidden('password', ${$segment}->password); ?>
                        <?php echo form_hidden('password_check', ${$segment}->password_check); ?>
                        <?php echo form_hidden('company_name1', @${$segment}->company_name1); ?>
                        <?php echo form_hidden('company_name2', @${$segment}->company_name2); ?>
                        <?php echo form_hidden('company_name3', @${$segment}->company_name3); ?>
                        <?php echo form_hidden('start_year1', @${$segment}->start_year1); ?>
                        <?php echo form_hidden('start_year2', @${$segment}->start_year2); ?>
                        <?php echo form_hidden('start_year3', @${$segment}->start_year3); ?>
                        <?php echo form_hidden('start_month1', @${$segment}->start_month1); ?>
                        <?php echo form_hidden('start_month2', @${$segment}->start_month2); ?>
                        <?php echo form_hidden('start_month3', @${$segment}->start_month3); ?>
                        <?php echo form_hidden('end_year1', @${$segment}->end_year1); ?>
                        <?php echo form_hidden('end_year2', @${$segment}->end_year2); ?>
                        <?php echo form_hidden('end_year3', @${$segment}->end_year3); ?>
                        <?php echo form_hidden('end_month1', @${$segment}->end_month1); ?>
                        <?php echo form_hidden('end_month2', @${$segment}->end_month2); ?>
                        <?php echo form_hidden('end_month3', @${$segment}->end_month3); ?>
                        <?php echo form_hidden('earnings', ${$segment}->earnings); ?>
                        <?php echo form_hidden('tantou_job1', @${$segment}->tantou_job1); ?>
                        <?php echo form_hidden('tantou_job2', @${$segment}->tantou_job2); ?>
                        <?php echo form_hidden('tantou_job3', @${$segment}->tantou_job3); ?>
                        <?php echo form_hidden($csrf); ?>
                        <?php echo form_close();?>
                    </div>
                    <div class="back">
                        <?php echo form_open('user/regist/');?>
                        <p class="btnBack">
                            <input type="submit" value="入力画面に戻って修正する" onclick="window.onbeforeunload=null;">
                        </p>
                        <?php echo form_hidden('mode', 'return'); ?>
                        <?php echo form_hidden('shimei', ${$segment}->shimei); ?>
                        <?php echo form_hidden('kana', ${$segment}->kana); ?>
                        <?php echo form_hidden('year', ${$segment}->year); ?>
                        <?php echo form_hidden('month', ${$segment}->month); ?>
                        <?php echo form_hidden('day', ${$segment}->day); ?>
                        <?php echo form_hidden('sex', ${$segment}->sex); ?>
                        <?php echo form_hidden('kpref', ${$segment}->kpref); ?>
                        <?php echo form_hidden('tel1', ${$segment}->tel1); ?>
                        <?php echo form_hidden('mail1', ${$segment}->mail1); ?>
                        <?php echo form_hidden('school_div_id', ${$segment}->school_div_id); ?>
                        <?php echo form_hidden('school_name', ${$segment}->school_name); ?>
                        <?php echo form_hidden('jokyo', ${$segment}->jokyo); ?>
                        <?php echo form_hidden('company_name1', @${$segment}->company_name1); ?>
                        <?php echo form_hidden('company_name2', @${$segment}->company_name2); ?>
                        <?php echo form_hidden('company_name3', @${$segment}->company_name3); ?>
                        <?php echo form_hidden('start_year1', @${$segment}->start_year1); ?>
                        <?php echo form_hidden('start_year2', @${$segment}->start_year2); ?>
                        <?php echo form_hidden('start_year3', @${$segment}->start_year3); ?>
                        <?php echo form_hidden('start_month1', @${$segment}->start_month1); ?>
                        <?php echo form_hidden('start_month2', @${$segment}->start_month2); ?>
                        <?php echo form_hidden('start_month3', @${$segment}->start_month3); ?>
                        <?php echo form_hidden('end_year1', @${$segment}->end_year1); ?>
                        <?php echo form_hidden('end_year2', @${$segment}->end_year2); ?>
                        <?php echo form_hidden('end_year3', @${$segment}->end_year3); ?>
                        <?php echo form_hidden('end_month1', @${$segment}->end_month1); ?>
                        <?php echo form_hidden('end_month2', @${$segment}->end_month2); ?>
                        <?php echo form_hidden('end_month3', @${$segment}->end_month3); ?>
                        <?php echo form_hidden('earnings', ${$segment}->earnings); ?>
                        <?php echo form_hidden('tantou_job1', @${$segment}->tantou_job1); ?>
                        <?php echo form_hidden('tantou_job2', @${$segment}->tantou_job2); ?>
                        <?php echo form_hidden('tantou_job3', @${$segment}->tantou_job3); ?>
                        <?php echo form_hidden('comment', ${$segment}->comment); ?>
                        <?php echo form_hidden('company_number', ${$segment}->company_number); ?>
                        <?php echo form_hidden('password', ${$segment}->password); ?>
                        <?php echo form_hidden('password_check', ${$segment}->password_check); ?>
                        <?php echo form_hidden($csrf); ?>
                        <?php echo form_close();?>
                    </div>
                </div>
                <!-- end -->


                <!-- conditionListt end -->
        </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($looks)):?>
                <div id="historyBox">
                    <h2><span>最近閲覧した求人</span></h2>
                    <div class="inner">
                        <?php foreach($looks as $k=>$v):?>
                            <!-- entry start -->
                            <div class="entry">
                                <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                            </div>
                            <!-- entry end -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
            <!-- historyBox end -->