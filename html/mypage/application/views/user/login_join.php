<h1>ログイン</h1>
<!-- loginPage start -->
<div id="loginPage">
    <div class="error"><?php echo $clean->purify($msg); ?></div>
    <?php echo form_open("user/login");?>
    <ul class="inputBox">
        <li><input type="text" name="email"  class="txt" placeholder="メールアドレス"></li>
        <li><input type="password" name="password" class="txt" autocomplete="off" placeholder="パスワード"></li>
        <li><label for="loginkey"><input type="checkbox" id="loginkey" name="loginkey">次回から自動ログイン</label></li>
    </ul>
    <p class="forget"><a href="<?php echo base_url();?>user/forget_password">ログインができない方はこちら→</a></p>
    <button class="btnLogin"><span>ログインする</span></button>
    <input type="hidden" name="p" value="<?php echo $clean->purify($param); ?>" />
    <input type="hidden" name="j" value="<?php echo $clean->purify($job); ?>" />
    <?php echo form_close();?>
</div>
<!-- loginPage end -->
<!-- registBox start -->
<div id="registBox">
    <div class="inner">
        <h2>一度もログインしたことがない方</h2>
        <?php echo form_open('signup/send_mail', array('id'=>'newregist')); ?>
        <ul class="inputBox">
            <li><input type="text" name="email" class="txt" placeholder="メールアドレス"></li>
        </ul>
        <button class="btnRegist"><span>無料会員登録する</span></button>
        <p class="merit"><a href="<?php echo base_url();?>signup/regist<?php if(!empty($param)):?>?p=<?php echo $clean->purify($param); ?><?php endif;?>">会員登録するメリットを詳しくみる→</a></p>
        <?php
        $tmp_param="";
        if(!empty($param)){
            $tmp_param= "p|" . $param;
        }else if(!empty($job)){
            $tmp_param = "p|" . $job;
        }
        ?>
        <input type="hidden" name="parameter" value="<?php echo $clean->purify($tmp_param);?>" />
        <?php echo form_close();?>
    </div>
</div>
<script>
    $(function(){
        var tmp = "";
        $("form#newregist").submit(function(){
            $("div.error").remove();
            //メールアドレス確認用
            if($("#email").val()==""){
                $("#email").parent().append("<div class='error'>メールアドレスを入力してください。</div>");
            }
            if($("#email").val() && !$("#email").val().match(/.+@.+\..+/g)){
                $("#email").parent().append("<div class='error'>メールアドレスの形式が異なります。</div>");
            }
        });
    });
</script>
<!-- registBox end -->
<!-- securityBox start -->
<div id="securityBox">
    <p><strong>HUREXは万全のセキュリティ体制</strong><br> 現在の職場やご友人など、第三者に知らせることは決してございませんので安心してご登録下さい。
        <a href="https://www.hurex.jp/company/privacy-policy/" target="_blank" class="link">プライバシーポリシー</a>
    </p>
</div>
<!-- securityBox end -->

