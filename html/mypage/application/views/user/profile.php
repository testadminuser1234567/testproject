<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);
$job_ary=array();
if(!empty($tmpjob['Item'])){
    foreach($tmpjob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $job_ary as $key => $value) {
        $sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($sort, SORT_ASC, $job_ary);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

/*
$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);
$tantou_job_ary=array();
if(!empty($tmptantoujob['Item'])){
foreach($tmptantoujob['Item'] as $k=>$v){
if(!empty($v['Item'])){
foreach($v['Item'] as $k2=>$v2){
    $tantou_job_ary[] = $v2;
}
}
}
foreach ((array) $tantou_job_ary as $key => $value) {
$tantou_sort[$key] = $value["Option.P_Order"];
}
}
array_multisort($tantou_sort, SORT_ASC, $tantou_job_ary);
*/
//担当職種（新）
$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);

$start=date('Y');
$end=$start-65;
?>
<script src="<?php echo base_url();?>js/form_validation_regist.js"></script>
<h1 class="no">プロフィール</h1>
<!--
<p style="background: #BC3509; color: #fff;font-size: 13px;padding: 10px 0;">現在、メンテンナンス中のためメールアドレスのみ変更ができません。ご了承ください。&lt;メンテナンスは8月19日(月)10:00頃終了予定&gt;</p>
-->
<!-- co start -->
<div id="co">
    <div id="coSub" class="clearfix">
        <main id="mainCo">
            <!-- conditionList start -->
            <div id="profileBox" class="profileIndex">
                <div class="inner">
                    <h2>基本情報</h2>
                    <table class="profile">
                        <tr>
                            <th>メール</th>
                            <td><?php echo htmlspecialchars(@${$segment}->mail1, ENT_QUOTES, 'UTF-8');?>
                            </td>
                        </tr>
                        <tr>
                            <th>氏名</th>
                            <td><?php echo htmlspecialchars(@${$segment}->shimei, ENT_QUOTES, 'UTF-8');?>
                            </td>
                        </tr>
                        <tr>
                            <th>ふりがな</th>
                            <td><?php echo htmlspecialchars(@${$segment}->kana, ENT_QUOTES, 'UTF-8');?>
                            </td>
                        </tr>
                        <tr>
                            <th>生年月日</th>
                            <td><?php echo htmlspecialchars(@${$segment}->year, ENT_QUOTES, 'UTF-8');?>年<?php echo htmlspecialchars(@${$segment}->month, ENT_QUOTES, 'UTF-8');?>月<?php echo htmlspecialchars(@${$segment}->day, ENT_QUOTES, 'UTF-8');?>日
                            </td>
                        </tr>
                        <tr>
                            <th>性別</th>
                            <td>
                                <?php foreach($tmpgender['Item'] as $k=>$v):?>
                                    <?php if(!empty($v['Item'])):?>
                                        <?php foreach($v['Item'] as $k2=>$v2):?>
                                            <?php if(@${$segment}->sex==$v2['Option.P_Name']):?>
                                                <?php echo htmlspecialchars($v2['Option.P_Name'], ENT_QUOTES, 'UTF-8');?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            </td>
                        </tr>
                        <tr>
                            <th>住所</th>
                            <td>
                                <?php foreach($tmppref['Item'] as $k=>$v):?>
                                    <?php if(!empty($v['Item'])):?>
                                        <?php foreach($v['Item'] as $k2=>$v2):?>
                                            <?php if(!empty($v2['Items']['Item'])):?>
                                                <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                                    <?php if(@${$segment}->pref==$v3['Option.P_Name']):?>
                                                        <?php echo htmlspecialchars($v3['Option.P_Name'], ENT_QUOTES, 'UTF-8');?>
                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            </td>
                        </tr>
                        <tr>
                            <th>電話番号</th>
                            <td><?php echo htmlspecialchars(@${$segment}->tel1, ENT_QUOTES, 'UTF-8');?>
                            </td>
                        </tr>

                        <tr>
                            <th>最終学歴</th>
                            <td>
                                <?php if(!empty($tmpbackground['Item'])):?>
                                    <?php foreach($tmpbackground['Item'] as $k=>$v):?>
                                        <?php if(!empty($v['Item'])):?>
                                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                                <?php if(@${$segment}->school_div_id==$v2['Option.P_Name']):?>
                                                    <?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </td>
                        </tr>

                        <tr>
                            <th>学校名/学部/学科</th>
                            <td><?php echo htmlspecialchars(@${$segment}->school_name, ENT_QUOTES, 'UTF-8');?>
                            </td>
                        </tr>

                        <tr>
                            <th>経験社数</th>
                            <td><?php echo htmlspecialchars(@${$segment}->company_number, ENT_QUOTES, 'UTF-8');?>社<?php if(@${$segment}->company_number == 5):?>以上<?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <th>就業状況</th>
                            <td>
                                <?php if(!empty($tmpwork['Item'])):?>
                                    <?php foreach($tmpwork['Item'] as $k=>$v):?>
                                        <?php if(!empty($v['Item'])):?>
                                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                                <?php if(@${$segment}->jokyo==$v2['Option.P_Name']):?>
                                                    <?php print_r($v2['Option.P_Name']);?>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <th>その他</th>
                            <td class="other"><?php echo htmlspecialchars(@${$segment}->comment, ENT_QUOTES, 'UTF-8');?></td>
                        </tr>
                    </table>
                    <h2>直近3社</h2>
                    <table class="company">
                        <thead>
                        <tr>
                            <th colspan="2" class="title">現在または直前の勤務先</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>社 名</th>
                            <td>
                                <?php echo htmlspecialchars(@${$segment}->company_name1,ENT_QUOTES,'UTF-8');?>
                            </td>
                        </tr>
                        <tr>
                            <th>勤務開始</th>
                            <td>
                                <?php if(!empty(${$segment}->start_year1)):?>
                                    <?php echo htmlspecialchars(@${$segment}->start_year1,ENT_QUOTES, 'UTF-8');?>年
                                <?php endif;?>
                                <?php if(!empty(${$segment}->start_month1)):?>
                                    <?php echo htmlspecialchars(@${$segment}->start_month1,ENT_QUOTES, 'UTF-8');?>月
                                <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <th>勤務終了</th>
                            <td>
                                <?php if(!empty(${$segment}->end_year1)):?>
                                    <?php echo htmlspecialchars(@${$segment}->end_year1,ENT_QUOTES, 'UTF-8');?>年
                                <?php endif;?>
                                <?php if(!empty(${$segment}->end_month1)):?>
                                    <?php echo htmlspecialchars(@${$segment}->end_month1,ENT_QUOTES, 'UTF-8');?>月
                                <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <th>現在の年収</th>
                            <td>
                                <?php if(!empty($tmpincome['Item'])):?>
                                    <?php foreach($tmpincome['Item'] as $k=>$v):?>
                                        <?php if(!empty($v['Item'])):?>
                                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                                <?php if($v2["Option.P_Id"]==@${$segment}->earnings):?><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <th>経験職種</th>
                            <td class="checkboxrequired">
                                <?php
                                $flg=array();
                                $datas=array();
                                if(!empty($tmptantoujob)){
                                    foreach($tmptantoujob as $k=>$v){
                                        if(!empty($v["Items"])){
                                            foreach($v["Items"] as $k2=>$v2){
                                                if(!empty($v2)){
                                                    foreach($v2 as $k3=>$v3){
                                                        //既存ユーザーの大カテゴリーを小カテゴリーに変換する
                                                        foreach(${$segment}->tantou_job1 as $tk=>$tv){
                                                            if($v3["Option.P_Id"] == $tv){
                                                                if(!empty($v3["Items"])){
                                                                    foreach($v3["Items"] as $k4=>$v4){
                                                                        if(!empty($v4)){
                                                                            foreach($v4 as $k5=>$v5){
                                                                                ${$segment}->tantou_job1[] = $v5["Option.P_Id"];
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        //既存ユーザー対応ここまで

                                                        $datas[$k3]["title"] = $v3["Option.P_Name"];
                                                        $datas[$k3]["id"] = $v3["Option.P_Id"];
                                                        if(!empty($v3["Items"])){
                                                            foreach($v3["Items"] as $k4=>$v4){
                                                                if(!empty($v4)){
                                                                    foreach($v4 as $k5=>$v5){
                                                                        foreach(${$segment}->tantou_job1 as $jk=>$jv){
                                                                            if($jv==$v5["Option.P_Id"]){
                                                                                $flg[$k3] = 1;
                                                                            }
                                                                        }
                                                                        $datas[$k3][$k5]["title"] = $v5["Option.P_Name"];
                                                                        $datas[$k3][$k5]["id"] = $v5["Option.P_Id"];
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                ?>
                                <?php if(!empty($datas)):?>
                                    <?php foreach($datas as $k=>$v):?>
                                        <?php if($flg[$k]==1):?><?php if($k!=0):?><?php endif;?>
                                            【<?php echo htmlspecialchars($v["title"],ENT_QUOTES,'UTF-8');?>】<br />
                                            <?php $unit="<p>";?>
                                            <?php foreach($v as $k2 => $v2):?>
                                                <?php if(is_array($v2)):?>
                                                    <?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==$v2["id"]):?><?php $unit .= str_replace(array(" ","　"),"",htmlspecialchars($v2['title'],ENT_QUOTES,'UTF-8')) . "|";?><?php endif;?><?php endforeach;?>
                                                <?php endif;?>
                                            <?php endforeach;?><?php $unit = rtrim($unit,"|");?><?php $unit.="</p>";?><?php $unit = str_replace("|","、",$unit);?>
                                            <?php echo $unit;?><br />
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <!-- 会社２ -->
                    <table class="company">
                        <thead>
                        <tr>
                            <th colspan="2" class="title">以前の勤務先1</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="line">
                            <th>社 名</th>
                            <td><?php if(empty(@${$segment}->company_name2)):?>未入力<?php endif;?>
                                <?php echo htmlspecialchars(@${$segment}->company_name2,ENT_QUOTES,'UTF-8');?>
                            </td>
                        </tr>
                        <tr>
                            <th>勤務開始</th>
                            <td><?php if(empty(@${$segment}->start_year2) && empty(@${segment}->start_month2)):?>未入力<?php endif;?>
                                <?php if(!empty(${$segment}->start_year2)):?>
                                    <?php echo htmlspecialchars(@${$segment}->start_year2,ENT_QUOTES, 'UTF-8');?>年
                                <?php endif;?>
                                <?php if(!empty(${$segment}->start_month2)):?>
                                    <?php echo htmlspecialchars(@${$segment}->start_month2,ENT_QUOTES, 'UTF-8');?>月
                                <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <th>勤務終了</th>
                            <td><?php if(empty(@${$segment}->end_year2) && empty(@${segment}->end_month2)):?>未入力<?php endif;?>
                                <?php if(!empty(${$segment}->end_year2)):?>
                                    <?php echo htmlspecialchars(@${$segment}->end_year2,ENT_QUOTES, 'UTF-8');?>年
                                <?php endif;?>
                                <?php if(!empty(${$segment}->end_month2)):?>
                                    <?php echo htmlspecialchars(@${$segment}->end_month2,ENT_QUOTES, 'UTF-8');?>月
                                <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <th>経験職種</th>
                            <td class="checkboxrequired"><?php if(empty(@${$segment}->tantou_job2)):?>未入力<?php endif;?>
                                <?php
                                $flg=array();
                                $datas=array();
                                if(!empty($tmptantoujob)){
                                    foreach($tmptantoujob as $k=>$v){
                                        if(!empty($v["Items"])){
                                            foreach($v["Items"] as $k2=>$v2){
                                                if(!empty($v2)){
                                                    foreach($v2 as $k3=>$v3){
                                                        //既存ユーザーの大カテゴリーを小カテゴリーに変換する
                                                        foreach(${$segment}->tantou_job2 as $tk=>$tv){
                                                            if($v3["Option.P_Id"] == $tv){
                                                                if(!empty($v3["Items"])){
                                                                    foreach($v3["Items"] as $k4=>$v4){
                                                                        if(!empty($v4)){
                                                                            foreach($v4 as $k5=>$v5){
                                                                                ${$segment}->tantou_job2[] = $v5["Option.P_Id"];
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }//既存ユーザー対応ここまで
                                                        $datas[$k3]["title"] = $v3["Option.P_Name"];
                                                        $datas[$k3]["id"] = $v3["Option.P_Id"];
                                                        if(!empty($v3["Items"])){
                                                            foreach($v3["Items"] as $k4=>$v4){
                                                                if(!empty($v4)){
                                                                    foreach($v4 as $k5=>$v5){
                                                                        foreach(${$segment}->tantou_job2 as $jk=>$jv){
                                                                            if($jv==$v5["Option.P_Id"]){
                                                                                $flg[$k3] = 1;
                                                                            }
                                                                        }
                                                                        $datas[$k3][$k5]["title"] = $v5["Option.P_Name"];
                                                                        $datas[$k3][$k5]["id"] = $v5["Option.P_Id"];
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                ?>
                                <?php if(!empty($datas)):?>
                                    <?php foreach($datas as $k=>$v):?>
                                        <?php if($flg[$k]==1):?><?php if($k!=0):?><?php endif;?>
                                            【<?php echo htmlspecialchars($v["title"],ENT_QUOTES,'UTF-8');?>】<br />
                                            <?php $unit="<p>";?>
                                            <?php foreach($v as $k2 => $v2):?>
                                                <?php if(is_array($v2)):?>
                                                    <?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==$v2["id"]):?><?php $unit .= str_replace(array(" ","　"),"",htmlspecialchars($v2['title'],ENT_QUOTES,'UTF-8')) . "|";?><?php endif;?><?php endforeach;?>
                                                <?php endif;?>
                                            <?php endforeach;?><?php $unit = rtrim($unit,"|");?><?php $unit.="</p>";?><?php $unit = str_replace("|","、",$unit);?>
                                            <?php echo $unit;?><br />
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </td>
                        </tr>
                        <!-- 会社３ -->
                        <table class="company">
                            <thead>
                            <tr>
                                <th colspan="2" class="title">以前の勤務先2</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th>社 名</th>
                                <td><?php if(empty(@${$segment}->company_name3)):?>未入力<?php endif;?>
                                    <?php echo htmlspecialchars(@${$segment}->company_name3,ENT_QUOTES,'UTF-8');?>
                                </td>
                            </tr>
                            <tr>
                                <th>勤務開始</th>
                                <td><?php if(empty(@${$segment}->start_year3) && empty(@${segment}->start_month3)):?>未入力<?php endif;?>
                                    <?php if(!empty(${$segment}->start_year3)):?>
                                        <?php echo htmlspecialchars(@${$segment}->start_year3,ENT_QUOTES, 'UTF-8');?>年
                                    <?php endif;?>
                                    <?php if(!empty(${$segment}->start_month3)):?>
                                        <?php echo htmlspecialchars(@${$segment}->start_month3,ENT_QUOTES, 'UTF-8');?>月
                                    <?php endif;?>
                                </td>
                            </tr>
                            <tr>
                                <th>勤務終了</th>
                                <td><?php if(empty(@${$segment}->end_year3) && empty(@${segment}->end_month3)):?>未入力<?php endif;?>
                                    <?php if(!empty(${$segment}->end_year3)):?>
                                        <?php echo htmlspecialchars(@${$segment}->end_year3,ENT_QUOTES, 'UTF-8');?>年
                                    <?php endif;?>
                                    <?php if(!empty(${$segment}->end_month3)):?>
                                        <?php echo htmlspecialchars(@${$segment}->end_month3,ENT_QUOTES, 'UTF-8');?>月
                                    <?php endif;?>
                                </td>
                            </tr>
                            <tr>
                                <th>経験職種</th>
                                <td class="checkboxrequired"><?php if(empty(@${$segment}->tantou_job3)):?>未入力<?php endif;?>
                                    <?php
                                    $flg=array();
                                    $datas=array();
                                    if(!empty($tmptantoujob)){
                                        foreach($tmptantoujob as $k=>$v){
                                            if(!empty($v["Items"])){
                                                foreach($v["Items"] as $k2=>$v2){
                                                    if(!empty($v2)){
                                                        foreach($v2 as $k3=>$v3){
                                                            //既存ユーザーの大カテゴリーを小カテゴリーに変換する
                                                            foreach(${$segment}->tantou_job3 as $tk=>$tv){
                                                                if($v3["Option.P_Id"] == $tv){
                                                                    if(!empty($v3["Items"])){
                                                                        foreach($v3["Items"] as $k4=>$v4){
                                                                            if(!empty($v4)){
                                                                                foreach($v4 as $k5=>$v5){
                                                                                    ${$segment}->tantou_job3[] = $v5["Option.P_Id"];
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            //既存ユーザー対応ここまで

                                                            $datas[$k3]["title"] = $v3["Option.P_Name"];
                                                            $datas[$k3]["id"] = $v3["Option.P_Id"];
                                                            if(!empty($v3["Items"])){
                                                                foreach($v3["Items"] as $k4=>$v4){
                                                                    if(!empty($v4)){
                                                                        foreach($v4 as $k5=>$v5){
                                                                            foreach(${$segment}->tantou_job3 as $jk=>$jv){
                                                                                if($jv==$v5["Option.P_Id"]){
                                                                                    $flg[$k3] = 1;
                                                                                }
                                                                            }
                                                                            $datas[$k3][$k5]["title"] = $v5["Option.P_Name"];
                                                                            $datas[$k3][$k5]["id"] = $v5["Option.P_Id"];
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <?php if(!empty($datas)):?>
                                        <?php foreach($datas as $k=>$v):?>
                                            <?php if($flg[$k]==1):?><?php if($k!=0):?><?php endif;?>
                                                【<?php echo htmlspecialchars($v["title"],ENT_QUOTES,'UTF-8');?>】<br />
                                                <?php $unit="<p>";?>
                                                <?php foreach($v as $k2 => $v2):?>
                                                    <?php if(is_array($v2)):?>
                                                        <?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==$v2["id"]):?><?php $unit .= str_replace(array(" ","　"),"",htmlspecialchars($v2['title'],ENT_QUOTES,'UTF-8')) . "|";?><?php endif;?><?php endforeach;?>
                                                    <?php endif;?>
                                                <?php endforeach;?><?php $unit = rtrim($unit,"|");?><?php $unit.="</p>";?><?php $unit = str_replace("|","、",$unit);?>
                                                <?php echo $unit;?><br />
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </td>
                            </tr>
                        </table>
                        <p class="btn"><a href="javascript:void(0);" onclick="document.frm.submit();return false;"><span>プロフィールを変更する</span></a></p>
                        <?php $attributes = array('name' => 'frm', 'id' => 'frm');?>
                        <?php echo form_open('user/regist/',$attributes);?>
                        <p class="btnSend aCenter">
                            <input type="hidden" value="プロフィールを編集する" onclick="return check(1);">
                        </p>
                </div>
            </div>
            <!-- profileBoxt end -->
        </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($looks)):?>
                <div id="historyBox">
                    <h2><span>最近閲覧した求人</span></h2>
                    <div class="inner">
                        <?php foreach($looks as $k=>$v):?>
                            <!-- entry start -->
                            <div class="entry">
                                <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                            </div>
                            <!-- entry end -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
            <!-- historyBox end -->


