<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

?>
    <h1><?php echo $clean->purify($title); ?></h1>
    <!-- co start -->
    <div id="co">
        <div id="coSub" class="clearfix">
        <main id="mainCo">
        <p>以下の内容で送信致します。
        </p>

	<div class="registInputPage form">

<table>
        <table width="100%" class="confirm">
             <tr>
                <th class="<?php if(@${$segment}->comment):?>ok<?php else:?>hissu<?php endif;?> comment">ご希望の参加日を<br />ご記入ください</th>
                <td><?php echo set_value('comment'); ?></td>
            </tr>

        </table>
 

                
        <!-- start -->
        <div class="confirmBtnBox clearfix">
            <div class="send">
            <?php echo form_open('user/join_send');?>
            <div class="btnBox">
<button class="btn b02" onclick="window.onbeforeunload=null;"><span>送信する</span></button>
            </div>
            <?php echo form_hidden('mode', 'send'); ?>
<input type="hidden" name="p" value="<?php echo $clean->purify($param); ?>" />
            <?php echo form_hidden('comment', ${$segment}->comment); ?>
		
            <?php echo form_close();?>
            </div>
            <div class="back">
            <?php echo form_open('user/join');?>
                <p class="btnBack">
                    <input type="submit" value="入力画面に戻って修正する" onclick="window.onbeforeunload=null;">
                </p>

            <?php echo form_hidden('mode', 'return'); ?>
<input type="hidden" name="p" value="<?php echo $clean->purify($param); ?>" />
            <?php echo form_hidden('comment', ${$segment}->comment); ?>
            <?php echo form_close();?>
            </div>
</div>
</div>
        </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($looks)):?>
            <div id="historyBox">
                <h2><span>最近閲覧した求人</span></h2>
                <div class="inner">
                    <?php foreach($looks as $k=>$v):?>
                    <!-- entry start -->
                    <div class="entry">
                        <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                    </div>
                    <!-- entry end -->
                    <?php endforeach;?>
                </div>
            </div>
            <?php endif;?>
            <!-- historyBox end -->