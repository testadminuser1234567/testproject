<h1>退会のお申し込み</h1>
<!-- suspensionPage start -->
<div id="suspensionPage">
    <div class="inner">
        <p>平素よりヒューレックスの転職支援サービスをご利用いただき、ありがとうございます。<br>
退会をご希望の方は注意事項をご確認のうえ、「退会」ボタンをクリックしてください。 </p>
            <h2>注意事項</h2>
        <ol>
        <li>退会をすると、ヒューレックスへのご登録情報は全て削除されます</li>
            <li>求人への応募は全てキャンセルされます</li>
        </ol>
            <p>登録情報を残したまま、各サービスを停止することもできます。<br>
            メールマガジンの配信停止を希望される方は「メルマガ停止」ボタンをクリックしてください。<br>
            転職活動を中止され、求人のご案内がご不要になった方は「活動中止」ボタンをクリックしてください。</p>
        <!-- btnBox start -->
        <div class="btnBox">
            <ul>
                <li><a href="<?php echo base_url();?>suspension/merumaga/"  onclick="return confirm('メールマガジンを停止します。\n※配信停止のご希望をいただいてから、反映まで2～3日かかる場合がございます。ご了承ください。')"><span>メルマガ停止</span></a></li>
                <li><a href="<?php echo base_url();?>suspension/activity/"><span>活動中止</span></a></li>
                <li><a href="<?php echo base_url();?>suspension/confirm/"><span>退会</span></a></li>
            </ul>
        </div>
        <!-- btnBox start -->
        <!-- btnClose start -->
        <ul class="btnClose clearfix">
            <li class="close"><a href="javascript:;" onClick="window.close(); return false;">閉じる</a>
            </li>
        </ul>
        <!-- btnClose start -->
    </div>
</div>
<!-- suspensionPage end -->



