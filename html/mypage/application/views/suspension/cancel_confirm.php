<script src="<?php echo base_url();?>js/jquery-1.6.4.min.js" charset="utf-8"></script>
<h1>退会</h1>
<!-- suspensionPage start -->
        <div id="suspensionPage" class="form">
    <div class="inner">
            <p>退会手続きはまだ完了しておりません。<br>
            注意事項をご確認のうえ、当てはまるものを選択し「退会」ボタンをクリックしてください。 </p>
            <h2>退会に係る注意事項</h2>
            <ol>
                <li>退会すると、下記情報が削除・無効となります。<br>
                <span class="ind">・ご登録情報(プロフィール、提出済の履歴書や職務経歴書など)</span><br>
                <span class="ind">・現在届いているオファー</span><br>
                <span class="ind">・応募状況</span><br>
                    <span class="ind">・気になる求人リスト</span></li>
                <li>退会手続きは取り消すことができませんので、ご注意ください。</li>
                <li>システムの関係上、退会の手続きには2～3日かかることがございます。ご了承ください。</li>
            </ol>
            <div class="error"><?php echo $msg ?></div>
            <?php echo form_open('suspension/close');?>
        <div id="douiBox">
        <p class="checkboxrequired">
                <label for="agree" class="CheckBoxLabelClass checkboxlabel" id="agr"><input type="checkbox" class="rc checkbox agree" name="agree" value="1" id="agree">上記内容に同意する</label>
        </p>
        </div>
            
            
        <div class="registInputPage form">
            <table>
                <tr>
                    <th class="hissu">退会理由<br>
                        <img src="<?php echo base_url();?>images/ico_hissu.png" class="taikai" width="49" height="14" alt=""/></th>
                    <td>
<label for="reason1" class="radiolabel reasonradio">
			<input type="radio" name="reason" id="reason1" class="radio reasonradio reason"  value="転職先が決まったため活動を終了する（自己応募）" />転職先が決まったため活動を終了する（自己応募）
</label>
<label for="reason2" class="radiolabel reasonradio">
			<input type="radio" name="reason" id="reason2" class="radio reasonradio reason" value="転職先が決まったため活動を終了する（ハローワーク、転職エージェント など）" />転職先が決まったため活動を終了する（ハローワーク、転職エージェント など）
</label><div class="clearBox"></div>
<label for="reason3" class="radiolabel reasonradio">
			<input type="radio" name="reason" id="reason3" class="radio reasonradio reason" value="希望する求人がない" />希望する求人がない
</label><div class="clearBox"></div>
<label for="reason4" class="radiolabel reasonradio" id="reason">
			<input type="radio" name="reason" id="reason4" class="radio reasonradio reason" value="その他" />その他
</label><div class="clearBox"></div>
<textarea name="comment" id="comment" cols="70" rows="3" style="width: 100%;" placeholder="その他をご選択の方は退会理由をご記入ください。"></textarea>
                    </td>
                </tr>
	</table>
	</div>
        
        
        <!-- btnBox start -->
        <div class="btnBox">
            <ul>
<li class="back"><a href="<?php echo base_url();?>suspension/home"><span>戻る</span></a></li>
<li><button class="btn" onclick="return susCheck(1);"><span>退会</span></button></li>
            </ul>
            </div>
        <!-- btnBox start --></div>
    </div>
<?php echo form_hidden($csrf); ?>
</form>
<!-- suspensionPage end -->
<script>
$(function(){
	if($("#agree").prop('checked')==true){
		$("#agr").addClass('checked');
	}


	$('#agree').click(function() {
		if($(this).prop('checked')==false){
			$("#agr").removeClass('checked');
		}else{
			$("#agr").addClass('checked');
		}
	});

	//理由
	$('.reasonradio').click(function(){
	    var idx = $(".reasonradio").index(this);
	    var cnt = $('.reasonradio').length;
	    var checked = 0;
	    for(i=0;i<cnt;i++){
		if($(".reasonradio").eq(i).prop("checked")){
			 checked++;
		}
	    }
	    if (checked > 0) {
			$(".taikai").removeClass("hissu");
			$(".taikai").addClass("ok");
			$(".taikai").attr("src", "/mypage/images/ico_ok.png");
	    }else{
			$(".taikai").removeClass("ok");
			$(".taikai").addClass("hissu");
			$(".taikai").attr("src", "/mypage/images/ico_hissu.png");
	    }
	})
});
	function susCheck(param){
		$("div.error").remove();

		if(!$("#agree").prop('checked')){
			$("#agr").parent().append("<div class='error'>退会の同意にチェックしてください</div>");
		}

		var radio_tmp = document.getElementsByName("reason");
		var cnt = 0;
		for(var i=0;i<radio_tmp.length;i++) {
			if (radio_tmp[i].checked) {
				cnt = 1;
			}
		}
		if(cnt == 0){
			$("#reason").parent().append("<div class='error'>退会理由を選択してください。</div>");
		}

		if($("div.error").size() > 0){
			$('html,body').animate({ scrollTop: $("div.error:first").offset().top-150}, 'slow');
			return false;
		}else{
			return true;
			//document.myform.action = "<?php echo base_url();?>suspension/test/";
		}
	}
</script>

