<script src="<?php echo base_url();?>js/jquery-1.6.4.min.js" charset="utf-8"></script>
<h1>活動中止</h1>
<!-- suspensionPage start -->
        <div id="suspensionPage">
    <div class="inner">
            <p>求人のご案内やメールマガジンなどのサービスを全て停止いたします。<br>
            注意事項をご確認のうえ、当てはまるものを選択し「活動中止」ボタンをクリックしてください。</p>
<h2>注意事項</h2>
        <ol>
<li>ご登録情報は削除されません</li>
<li>マイページは引き続きご利用いただけます</li>
<li>求人への応募は全てキャンセルされます</li>
        </ol>
        <!-- form start -->
            <div class="error"><?php echo $msg ?></div>
            <?php echo form_open('suspension/finish/');?>
        <div class="registInputPage form">
            <table>
                <tr>
                    <th class="hissu">活動中止理由<br>
                        <img src="<?php echo base_url();?>images/ico_hissu.png" class="stop" width="49" height="14" alt=""/></th>
                    <td>
<label for="reason1" class="radiolabel reasonradio">
			<input type="radio" name="reason" id="reason1" class="radio reasonradio reason"  value="転職先が決まったため活動を終了する（自己応募）" />転職先が決まったため活動を終了する（自己応募）
</label>
<label for="reason2" class="radiolabel reasonradio">
			<input type="radio" name="reason" id="reason2" class="radio reasonradio reason" value="転職先が決まったため活動を終了する（ハローワーク、転職エージェント など）" />転職先が決まったため活動を終了する（ハローワーク、転職エージェント など）
</label><div class="clearBoth"></div>
<label for="reason3" class="radiolabel reasonradio" id="reason">
			<input type="radio" name="reason" id="reason3" class="radio reasonradio reason" value="転職活動を一時中止する " />
			転職活動を一時中止する 
</label>
                    </td>
                </tr>
                <tr>
                    <th class="nini">活動再開時期<br>
                        <img src="<?php echo base_url();?>images/ico_nini.png" class="katudo" width="49" height="14" alt=""/></th>
                    <td>
                        <select name="year" id="year" class="required">
			<option value="">年</option>
			<?php $y=date('Y');?>
			<?php $e=date("Y",strtotime("+2 year"));?>
			<?php for($i=$y;$i<=$e;$i++):?>
                            <option value="<?php echo htmlspecialchars($i, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($i, ENT_QUOTES, 'UTF-8');?>年</option>                            
			<?php endfor;?>
                        </select>
                        <select name="month" id="month" class="required">
                            <option value="" selected="selected">月</option>
                            <option value="1">1月</option>
                            <option value="2">2月</option>
                            <option value="3">3月</option>
                            <option value="4">4月</option>
                            <option value="5">5月</option>
                            <option value="6">6月</option>
                            <option value="7">7月</option>
                            <option value="8">8月</option>
                            <option value="9">9月</option>
                            <option value="10">10月</option>
                            <option value="11">11月</option>
                            <option value="12">12月</option>
                            </select>
                        <input type="checkbox" class="checkbox chk" name="mitei" value="未定" id="mitei">
                    <label for="mitei" class="checkboxlabel  CheckBoxLabelClass">未定</label>
                    </td>
                </tr>
            </table></div>
        <!-- form end -->
        <!-- btnBox start -->
        <div class="btnBox">
            <ul>
<li class="back"><a href="<?php echo base_url();?>suspension/home"><span>戻る</span></a></li>
<li><button class="btn" onclick="return susCheck(1);"><span>活動中止</span></button></li>
</ul>
<!--
        <ul>
            <li class="send" id="suspensionBtn"><a href="javascript:void(0);"><span>入力した内容を送信する</span></a></li>
        </ul>
-->
            </div>
                <?php echo form_hidden($csrf); ?>

            </form>
        <!-- btnBox start --></div>
    </div>
<!-- suspensionPage end -->
<script>
$(function(){
	//$("#year").css("background-color","#fffafa");
	//$("#month").css("background-color","#fffafa");
	if($("#mitei").prop('checked')==true){
		$("#mitei").next(".checkboxlabel").addClass('checked');
	}

	$("select#year").change(function() {
		if(($("#year").val() && $("#month").val()) && !$("#mitei").prop('checked')){
			$("div.error").remove();
			$(".katudo").removeClass("hissu");
			$(".katudo").addClass("ok");
			$(".katudo").attr("src", "/mypage/images/ico_ok.png");
			//$("#year").css("background-color","#FFF");
			//$("#month").css("background-color","#FFF");
		}else if((!$("#year").val() && !$("#month").val()) && $("#mitei").prop('checked')){
			$("div.error").remove();
			$(".katudo").removeClass("hissu");
			$(".katudo").addClass("ok");
			$(".katudo").attr("src", "/mypage/images/ico_ok.png");
			//$("#month").css("background-color","#fffafa");
		}else{
			$(".katudo").removeClass("ok");
			$(".katudo").addClass("hissu");
			$(".katudo").attr("src", "/mypage/images/ico_nini.png");
			//$("#month").css("background-color","#fffafa");
		}
	});
	$("select#month").change(function() {
		if(($("#year").val() && $("#month").val()) && !$("#mitei").prop('checked')){
			$("div.error").remove();
			$(".katudo").removeClass("hissu");
			$(".katudo").addClass("ok");
			$(".katudo").attr("src", "/mypage/images/ico_ok.png");
			//$("#year").css("background-color","#FFF");
			//$("#month").css("background-color","#FFF");
		}else if((!$("#year").val() && !$("#month").val()) && $("#mitei").prop('checked')){
			$("div.error").remove();
			$(".katudo").removeClass("hissu");
			$(".katudo").addClass("ok");
			$(".katudo").attr("src", "/mypage/images/ico_ok.png");
			//$("#month").css("background-color","#fffafa");
		}else{
			$(".katudo").removeClass("ok");
			$(".katudo").addClass("hissu");
			$(".katudo").attr("src", "/mypage/images/ico_nini.png");
			//$("#month").css("background-color","#fffafa");
		}
	});
	//未定
	$('#mitei').click(function() {
		if($(this).prop('checked')==false){
			$(this).next(".checkboxlabel").removeClass('checked');
			if($("#year").val() && $("#month").val()){
				$("div.error").remove();
				$(".katudo").removeClass("hissu");
				$(".katudo").addClass("ok");
				$(".katudo").attr("src", "/mypage/images/ico_ok.png");
			}else{
				$(".katudo").removeClass("ok");
				$(".katudo").addClass("hissu");
				$(".katudo").attr("src", "/mypage/images/ico_nini.png");
			}
		}else{
			$(this).next(".checkboxlabel").addClass('checked');
			if(!$("#year").val() && !$("#month").val()){
				$("div.error").remove();
				$(".katudo").removeClass("hissu");
				$(".katudo").addClass("ok");
				$(".katudo").attr("src", "/mypage/images/ico_ok.png");
			}else{
				$(".katudo").removeClass("ok");
				$(".katudo").addClass("hissu");
				$(".katudo").attr("src", "/mypage/images/ico_nini.png");
			}
		}
	});

	//理由
	$('.reasonradio').click(function(){
	    var idx = $(".reasonradio").index(this);
	    var cnt = $('.reasonradio').length;
	    var checked = 0;
	    for(i=0;i<cnt;i++){
		if($(".reasonradio").eq(i).prop("checked")){
			 checked++;
		}
	    }
	    if (checked > 0) {
			$(".stop").removeClass("hissu");
			$(".stop").addClass("ok");
			$(".stop").attr("src", "/mypage/images/ico_ok.png");
	    }else{
			$(".stop").removeClass("ok");
			$(".stop").addClass("hissu");
			$(".stop").attr("src", "/mypage/images/ico_hissu.png");
	    }
	})

});
	function susCheck(param){
		$("div.error").remove();

		var reason = $("input[name='reason']:checked").val();
		if(reason=="転職活動を一時中止する "){
			if((!$("#year").val() && !$("#month").val()) && !$("#mitei").prop('checked')){
				$("#mitei").parent().append("<div class='error'>活動再開時期(年月)か未定のどちらかを設定してください</div>");
			}else if(($("#year").val() && $("#month").val()) && !$("#mitei").prop('checked')){
			}else if((!$("#year").val() && !$("#month").val()) && $("#mitei").prop('checked')){
			}else{
				$("#mitei").parent().append("<div class='error'>活動再開時期(年月)か未定のどちらかを設定してください</div>");
			}
		}else{
			if(($("#year").val() && $("#month").val()) && $("#mitei").prop('checked')){
				$("#mitei").parent().append("<div class='error'>活動再開時期(年月)か未定のどちらかを設定してください</div>");
			}else if(($("#year").val() && !$("#month").val()) && !$("#mitei").prop('checked')){
				$("#mitei").parent().append("<div class='error'>活動再開時期(年月)か未定のどちらかを設定してください</div>");
			}else if((!$("#year").val() && $("#month").val()) && !$("#mitei").prop('checked')){
				$("#mitei").parent().append("<div class='error'>活動再開時期(年月)か未定のどちらかを設定してください</div>");
			}else if(($("#year").val() && !$("#month").val()) && $("#mitei").prop('checked')){
				$("#mitei").parent().append("<div class='error'>活動再開時期(年月)か未定のどちらかを設定してください</div>");
			}else if((!$("#year").val() && $("#month").val()) && $("#mitei").prop('checked')){
				$("#mitei").parent().append("<div class='error'>活動再開時期(年月)か未定のどちらかを設定してください</div>");
			}
		}

		var radio_tmp = document.getElementsByName("reason");
		var cnt = 0;
		for(var i=0;i<radio_tmp.length;i++) {
			if (radio_tmp[i].checked) {
				cnt = 1;
			}
		}
		if(cnt == 0){
			$("#reason").parent().append("<div class='error'>活動停止理由を選択してください。</div>");
		}

		if($("div.error").size() > 0){
			$('html,body').animate({ scrollTop: $("div.error:first").offset().top-150}, 'slow');
			return false;
		}else{
			return true;
			//document.myform.action = "<?php echo base_url();?>suspension/test/";
		}
	}
</script>

