<h1>メールマガジン停止</h1>
<!-- suspensionPage start -->
        <div id="suspensionPage">
    <div class="inner">
            <h2 class="finish">配信停止を受け付けました</h2>
            <p class="finish">メールマガジンを停止しました。<br>
            配信再開をご希望の場合、<a href="mailto:&#104;&#114;&#120;&#64;&#104;&#117;&#114;&#101;&#120;&#46;&#99;&#111;&#46;&#106;&#112;"><img src="<?php echo base_url();?>images/mail.gif" width="107" height="15" alt=""/></a>へ「配信再開」と記載し、送信ください。</p>
                <p class="finish">※配信停止のご希望をいただいてから、反映まで2～3日かかる場合がございます。ご了承ください。 </p>
        <!-- btnBox start -->
        <div class="btnBox">
        <ul>
            <li><a href="<?php echo base_url();?>user/home/" target="_blank"><span>マイページへもどる</span></a></li>
            <li class="home"><a href="https://www.hurex.jp/" target="_blank"><span>HUREXホームページ</span></a></li>
        </ul>
            </div>
        <!-- btnBox start --></div>
    </div>
<!-- suspensionPage end -->

