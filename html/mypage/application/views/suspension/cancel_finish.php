<h1>退会</h1>
<!-- suspensionPage start -->
        <div id="suspensionPage">
    <div class="inner">
            <h2 class="finish">Thank you for using.</h2>
            <p class="finish">退会手続きのご希望を受け付けました。<br>
                ヒューレックスをご利用いただきありがとうございました。</p>
            <p class="finish">※手続き完了まで2～3日かかる場合がございます。ご了承ください。</p>
        <!-- btnBox start -->
        <div class="btnBox">
        <ul>
            <li class="home"><a href="https://www.hurex.jp/" target="_blank"><span>HUREXホームページ</span></a></li>
        </ul>
            </div>
        <!-- btnBox start --></div>
    </div>
<!-- suspensionPage end -->