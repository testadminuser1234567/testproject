<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);
$job_ary=array();
if(!empty($tmpjob['Item'])){
    foreach($tmpjob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $job_ary as $key => $value) {
        $sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($sort, SORT_ASC, $job_ary);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

/*
$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);
$tantou_job_ary=array();
if(!empty($tmptantoujob['Item'])){
	foreach($tmptantoujob['Item'] as $k=>$v){
		if(!empty($v['Item'])){
			foreach($v['Item'] as $k2=>$v2){
				$tantou_job_ary[] = $v2;
			}
		}
	}
	foreach ((array) $tantou_job_ary as $key => $value) {
		$tantou_sort[$key] = $value["Option.P_Order"];
	}
}
array_multisort($tantou_sort, SORT_ASC, $tantou_job_ary);
*/
//担当職種（新）
$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);


$start=date('Y');
$end=$start-65;
?>
<script src="<?php echo base_url();?>/js/accordion_ex.js"></script>
<script src="<?php echo base_url();?>js/jquery-1.6.4.min.js" charset="utf-8"></script>
<script src="<?php echo base_url();?>js/form_validation_kiboujouken_step2.js?j=<?php echo date('Ymdhis');?>"></script>

<h1>新規会員登録</h1>
<!-- registInputPage start -->
<div class="registInputPage">
    <p id="step"><img src="<?php echo base_url();?>images/step02.png" class="pc" alt=""/><img src="<?php echo base_url();?>images/step02_sp.png" class="sp" width="100%" alt=""/>
    </p>
    <p>基本情報のご登録が完了しました。次に、希望条件のご入力をお願い致します。</p>
</div>
<!-- registInputPage end -->

<div class="error"><?php echo $msg ?></div>
<?php echo form_open_multipart('signup/confirm');?>
<div class="registInputPage form">

    <table width="100%">

        <tr>
            <th rowspan="3"  class="<?php if(@${$segment}->kibouarea1):?>ok<?php else:?>hissu<?php endif;?> kibouarea1">希望勤務地</th>
            <td>第1希望<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->kibouarea1):?>ok<?php else:?>hissu<?php endif;?> kibouarea1"/>
            </td>
            <td>
                <?php $tflg=0;?>
                <?php if(!empty($tmparea['Item'])):?>
                    <select name="kibouarea1" id="kibouarea1" class="hissu required">
                        <option value="" selected="selected" label="">▼お選びください</option>
                        <?php foreach($tmparea['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <?php if(!empty($v2["Items"]['Item'])):?>
                                        <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                            <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                <option value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea1==$v3['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>

                                            <?php else:?>
                                                <?php if($tflg==0):?>
                                                    <option value="<?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea1==$v2['Items']['Item']['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                                    <?php $tflg=1;?>
                                                <?php endif;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    </select>
                <?php endif;?>
            </td>
        </tr>


        <tr>
            <td>第2希望
            </td>
            <td>

                <?php $tflg=0;?>
                <?php if(!empty($tmparea['Item'])):?>
                    <select name="kibouarea2" id="kibouarea2" class="">
                        <option value="" selected="selected" label="">▼お選びください</option>
                        <?php foreach($tmparea['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <?php if(!empty($v2["Items"]['Item'])):?>
                                        <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                            <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                <option value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea2==$v3['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>

                                            <?php else:?>
                                                <?php if($tflg==0):?>
                                                    <option value="<?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea2==$v2['Items']['Item']['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                                    <?php $tflg=1;?>
                                                <?php endif;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    </select>
                <?php endif;?>
            </td>
        </tr>

        <tr>
            <td>第3希望
            </td>
            <td>

                <?php $tflg=0;?>
                <?php if(!empty($tmparea['Item'])):?>
                    <select name="kibouarea3" id="kibouarea3" class="">
                        <option value="" selected="selected" label="">▼お選びください</option>
                        <?php foreach($tmparea['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <?php if(!empty($v2["Items"]['Item'])):?>
                                        <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                            <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                <option value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea3==$v3['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>

                                            <?php else:?>
                                                <?php if($tflg==0):?>
                                                    <option value="<?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea3==$v2['Items']['Item']['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                                    <?php $tflg=1;?>
                                                <?php endif;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    </select>
                <?php endif;?>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <th>希望職種<br /><img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->kiboujob):?>ok<?php else:?>hissu<?php endif;?>.png" width="49" height="14" alt="" class="<?php if(@${$segment}->kiboujob):?>ok<?php else:?>hissu<?php endif;?> kiboujob"/></th>
            <td class="checkboxrequired">
                <?php if(!empty($job_ary)):?>
                    <ul class="job clearfix">
                        <?php foreach($job_ary as $k=>$v):?>
                            <li class="<?php if($v['Option.P_Name']=="営業系"):?>ss<?php endif;?><?php if($v['Option.P_Name']=="管理部門/事務系"):?>ss no<?php endif;?>"><input type="checkbox" class="checkbox joballchk jobcheckbox<?php if($k2-1==count($tmpjob['Item'])):?> required<?php endif;?>" name="kiboujob[]" value="<?php echo htmlspecialchars($v['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty(${$segment}->kiboujob)):?><?php foreach(${$segment}->kiboujob as $jk=>$jv):?><?php if($jv==$v["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?> id="cat_job0<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>"><label id="joblabel<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" for="cat_job0<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" class="checkboxlabel joblabel CheckBoxLabelClass joballnavichecklabel <?php if(!empty(${$segment}->kiboujob)):?><?php foreach(${$segment}->kiboujob as $jk=>$jv):?><?php if($jv==$v["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?>"><?php echo htmlspecialchars($v['Option.P_Name'],ENT_QUOTES,'UTF-8');?></label></li>
                        <?php endforeach;?>
                    </ul>
                <?php endif;?>
            </td>
        </tr>


        <tr>
            <th>希望年収<br>
                <img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->income):?>ok<?php else:?>hissu<?php endif;?>.png" width="49" height="14" alt="" class="<?php if(@${$segment}->income):?>ok<?php else:?>hissu<?php endif;?> income"/></th>
            <td>
                <?php if(!empty($tmpincome['Item'])):?>
                    <select name="income" id="income" class="required">
                        <option value="">指定なし</option>
                        <?php foreach($tmpincome['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <option value="<?php echo htmlspecialchars($v2["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" <?php if($v2["Option.P_Id"]==@${$segment}->income):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    </select>
                <?php endif;?>
            </td>
        </tr>

        <tr>
            <th>転勤<br>
                <img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->tenkin):?>ok<?php else:?>hissu<?php endif;?>.png" width="49" height="14" alt="" class="<?php if(@${$segment}->tenkin):?>ok<?php else:?>hissu<?php endif;?> tenkin"/></th>
            <td class="checkboxrequired">
                <ul class="two">
                    <?php if(!empty($tmptenkin['Item'])):?>
                        <?php foreach($tmptenkin['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?><li>
                                    <input type="checkbox" class="checkbox chk tenkincheckbox<?php if($k2-1==count($tmptenkin['Item'])):?> required<?php endif;?>" name="tenkin[]" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty(${$segment}->tenkin)):?><?php if(in_array($v2['Option.P_Name'], @${$segment}->tenkin, true)):?>checked<?php endif;?><?php endif;?> id="tenkin0<?php echo htmlspecialchars($v2["Option.P_Id"],ENT_QUOTES,'UTF-8');?>">
                                    <label for="tenkin0<?php echo htmlspecialchars($v2["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" class="checkboxlabel tenkinlabel  CheckBoxLabelClass navichecklabel">
                                        <?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
                                    </label>
                                    </li>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?></ul>
            </td>
        </tr>
        <tr>
            <th>転職希望日<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->tenshoku_kiboubi):?>ok<?php else:?>hissu<?php endif;?> tenshoku_kiboubi"/></th>
            <td>
	<span class="hissu">
<label for="tnk01" class="radiolabel tnsklabel">
<input type="radio" class="radio tnshkkibouradio" name="tenshoku_kiboubi" value="すぐに" <?php echo @${$segment}->tenshoku_kiboubi=="すぐに" ? "checked" : "";?> id="tnk01" />
すぐに</label>
<label for="tnk02" class="radiolabel tnsklabel">
<input type="radio" class="radio tnshkkibouradio" name="tenshoku_kiboubi" value="3ヶ月後" <?php echo @${$segment}->tenshoku_kiboubi=="3ヶ月後" ? "checked" : "";?> id="tnk02" />
3ヶ月後</label>
<label for="tnk03" class="radiolabel tnsklabel">
<input type="radio" class="radio tnshkkibouradio" name="tenshoku_kiboubi" value="半年後" <?php echo @${$segment}->tenshoku_kiboubi=="半年後" ? "checked" : "";?> id="tnk03" />
半年後</label>
<label for="tnk04" class="radiolabel tnsklabel">
<input type="radio" class="radio tnshkkibouradio" name="tenshoku_kiboubi" value="1年後" <?php echo @${$segment}->tenshoku_kiboubi=="1年後" ? "checked" : "";?> id="tnk04" />
1年後</label>
<label for="tnk05" class="radiolabel tnsklabel">
<input type="radio" class="radio tnshkkibouradio" name="tenshoku_kiboubi" value="未定（情報収集段階）" <?php echo @${$segment}->tenshoku_kiboubi=="未定（情報収集段階）" ? "checked" : "";?> id="tnk05" />
未定（情報収集段階）</label>
<label for="tnk06" class="radiolabel tnsklabel">
<input type="radio" class="radio tnshkkibouradio required" name="tenshoku_kiboubi" value="未定（良いところがあれば転職したい）" <?php echo @${$segment}->tenshoku_kiboubi=="未定（良いところがあれば転職したい）" ? "checked" : "";?> id="tnk06" />
未定（良いところがあれば転職したい）</label>

    </span>

                <div id="tenshoku_kiboubi_err"></div>
            </td>
        </tr>

    </table>




    <input type="hidden" name="pwdchk" value="1" />
    <input type="hidden" name="param" value="<?php echo $clean->purify(@$param);?>" />
    <input type="hidden" name="mail1" value="<?php echo $clean->purify(@${$segment}->mail1);?>" />
    <div class="btnBox">
    <button class="btn" onclick="return check(1);"><span>入力内容を確認する</span></button>
    </div>

    <?php echo form_close();?>


</div>

