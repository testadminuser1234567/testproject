<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);
$job_ary=array();
if(!empty($tmpjob['Item'])){
    foreach($tmpjob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $job_ary as $key => $value) {
        $sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($sort, SORT_ASC, $job_ary);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);
$tantou_job_ary=array();
if(!empty($tmptantoujob['Item'])){
    foreach($tmptantoujob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $tantou_job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $tantou_job_ary as $key => $value) {
        $tantou_sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($tantou_sort, SORT_ASC, $tantou_job_ary);

?>
<h1>新規会員登録</h1>
<!-- registInputPage start -->
<div class="registInputPage">
    <p id="step"><img src="<?php echo base_url();?>images/step02.png" class="pc" alt=""/><img src="<?php echo base_url();?>images/step02_sp.png" class="sp" width="100%" alt=""/>
    </p>
    <p>以下の内容で登録します。問題なければ「登録する」ボタンを押してください。</p>
</div>
<!-- registInputPage end -->

<div class="registInputPage form">
    <div id="experienceBox">
        <h2>希望条件</h2>
        <h3>希望勤務地</h3>
           <div class="inner">
            <p>第一希望：
                <?php if(!empty($tmparea['Item'])):?>
                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                        <?php if(!empty($v['Item'])):?>
                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                <?php if(!empty(${$segment}->kibouarea1)):?>
                                    <?php if(${$segment}->kibouarea1==$v2["Items"]['Item']['Option.P_Id']):?>
                                        <?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br />
                                    <?php endif;?>
                                <?php endif;?>
                                <?php if(!empty($v2["Items"]['Item'])):?>
                                    <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                        <?php if(!empty(${$segment}->kibouarea1)):?>
                                            <?php if(${$segment}->kibouarea1==$v3['Option.P_Id']):?>
                                                <?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br />
                                            <?php endif;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
            </p>
        </div>

        <div class="inner">
            <p>第二希望：
                <?php if(!empty($tmparea['Item'])):?>
                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                        <?php if(!empty($v['Item'])):?>
                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                <?php if(!empty(${$segment}->kibouarea2)):?>
                                    <?php if(${$segment}->kibouarea2==$v2["Items"]['Item']['Option.P_Id']):?>
                                        <?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br />
                                    <?php endif;?>
                                <?php endif;?>
                                <?php if(!empty($v2["Items"]['Item'])):?>
                                    <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                        <?php if(!empty(${$segment}->kibouarea2)):?>
                                            <?php if(${$segment}->kibouarea2==$v3['Option.P_Id']):?>
                                                <?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br />
                                            <?php endif;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
            </p>
        </div>

<div class="inner">
    <p>第三希望：
                <?php if(!empty($tmparea['Item'])):?>
                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                        <?php if(!empty($v['Item'])):?>
                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                <?php if(!empty(${$segment}->kibouarea3)):?>
                                    <?php if(${$segment}->kibouarea3==$v2["Items"]['Item']['Option.P_Id']):?>
                                        <?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br />
                                    <?php endif;?>
                                <?php endif;?>
                                <?php if(!empty($v2["Items"]['Item'])):?>
                                    <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                        <?php if(!empty(${$segment}->kibouarea3)):?>
                                            <?php if(${$segment}->kibouarea3==$v3['Option.P_Id']):?>
                                                <?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br />
                                            <?php endif;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
        </p>
</div>

        <h3>希望職種</h3>
        <div class="inner">
            <p>
                <?php if(!empty($job_ary)):?>
                    <?php foreach($job_ary as $k=>$v):?>
                        <?php if(!empty($_POST["kiboujob"])):?>
                            <?php foreach($_POST["kiboujob"] as $pk => $pv):?>
                                <?php if($pv==$v['Option.P_Id']):?><?php echo htmlspecialchars($v['Option.P_Name'],ENT_QUOTES,'UTF-8');?> / <?php endif;?>
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
            </p>
        </div>


        <h3>希望年収</h3>
        <div class="inner">
            <p>
                <?php if(empty(${$segment}->income)):?>指定なし
                <?php else:?>
                    <?php if(!empty($tmpincome['Item'])):?>
                        <?php foreach($tmpincome['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <?php if($_POST["income"]==$v2['Option.P_Id']):?><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br /><?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
                <?php endif;?>
            </p>
        </div>

        <h3>転勤</h3>
        <div class="inner">
            <p>
                <?php if(!empty($_POST["tenkin"])):?>
                    <?php foreach($_POST["tenkin"] as $k => $v):?>
                        <?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?><br />
                    <?php endforeach;?>
                <?php endif;?>
            </p>
        </div>

        <h3>転職希望日</h3>
        <div class="inner">
            <p>
                <?php if(!empty($_POST["tenshoku_kiboubi"])):?>
                        <?php echo htmlspecialchars($_POST["tenshoku_kiboubi"], ENT_QUOTES, 'UTF-8');?><br />
                <?php endif;?>
            </p>
        </div>


        <?php if(${$segment}->file1 || ${$segment}->file2 || ${$segment}->file3):?>
        <h2>履歴書・職務経歴書</h2>
        <table width="100%">
            <?php if(!empty(${$segment}->file1)):?>
                <tr>
                    <th class="<?php if(@${$segment}->file1):?>ok<?php else:?><?php endif;?>">履歴書</th>
                    <td><?php echo htmlspecialchars(${$segment}->file1, ENT_QUOTES, 'UTF-8'); ?></td>
                </tr>
            <?php endif;?>
            <?php if(!empty(${$segment}->file2)):?>
                <tr>
                    <th class="<?php if(@${$segment}->file2):?>ok<?php else:?><?php endif;?>">職務経歴書</th>
                    <td><?php echo htmlspecialchars(${$segment}->file2, ENT_QUOTES, 'UTF-8'); ?></td>
                </tr>
            <?php endif;?>
            <?php if(!empty(${$segment}->file3)):?>
                <tr>
                    <th class="<?php if(@${$segment}->file3):?>ok<?php else:?><?php endif;?>">その他</th>
                    <td><?php echo htmlspecialchars(${$segment}->file3, ENT_QUOTES, 'UTF-8'); ?></td>
                </tr>
            <?php endif;?>
        </table>
    <?php endif;?>

    <!-- start -->
    <div class="confirmBtnBox clearfix">
        <div class="send">
            <?php echo form_open('signup/add/');?>
            <div class="btnBox">
                <button class="btn" onclick="window.onbeforeunload=null;"><span>送信する</span></button>
            </div>
            <?php echo form_hidden('mode', 'send'); ?>

            <?php echo form_hidden('company_name1', @${$segment}->company_name1); ?>
            <?php echo form_hidden('company_name2', @${$segment}->company_name2); ?>
            <?php echo form_hidden('company_name3', @${$segment}->company_name3); ?>
            <?php echo form_hidden('kibouarea1', @${$segment}->kibouarea1); ?>
            <?php echo form_hidden('kibouarea2', @${$segment}->kibouarea2); ?>
            <?php echo form_hidden('kibouarea3', @${$segment}->kibouarea3); ?>
            <?php echo form_hidden('start_year1', @${$segment}->start_year1); ?>
            <?php echo form_hidden('start_year2', @${$segment}->start_year2); ?>
            <?php echo form_hidden('start_year3', @${$segment}->start_year3); ?>
            <?php echo form_hidden('start_month1', @${$segment}->start_month1); ?>
            <?php echo form_hidden('start_month2', @${$segment}->start_month2); ?>
            <?php echo form_hidden('start_month3', @${$segment}->start_month3); ?>
            <?php echo form_hidden('end_year1', @${$segment}->end_year1); ?>
            <?php echo form_hidden('end_year2', @${$segment}->end_year2); ?>
            <?php echo form_hidden('end_year3', @${$segment}->end_year3); ?>
            <?php echo form_hidden('end_month1', @${$segment}->end_month1); ?>
            <?php echo form_hidden('end_month2', @${$segment}->end_month2); ?>
            <?php echo form_hidden('end_month3', @${$segment}->end_month3); ?>

            <?php echo form_hidden('kiboujob', @${$segment}->kiboujob); ?>
            <?php echo form_hidden('tantou_job1', @${$segment}->tantou_job1); ?>
            <?php echo form_hidden('tantou_job2', @${$segment}->tantou_job2); ?>
            <?php echo form_hidden('tantou_job3', @${$segment}->tantou_job3); ?>
            <?php echo form_hidden('income', @${$segment}->income); ?>
            <?php echo form_hidden('earnings', @${$segment}->earnings); ?>
            <?php echo form_hidden('tenkin', @${$segment}->tenkin); ?>
            <?php echo form_hidden('file1', ${$segment}->file1); ?>
            <?php echo form_hidden('file2', ${$segment}->file2); ?>
            <?php echo form_hidden('file3', ${$segment}->file3); ?>
            <?php echo form_hidden('mail1', ${$segment}->mail1); ?>
            <?php echo form_hidden('comment', ${$segment}->comment); ?>
            <?php echo form_hidden('tenshoku_kiboubi', ${$segment}->tenshoku_kiboubi); ?>
            <?php echo form_hidden('how', ${$segment}->how); ?>
            <input type="hidden" name="pwdchk" value="1" />
            <?php echo form_close();?>
        </div>
        <div class="back">
            <?php echo form_open('signup/step2/');?>
            <p class="btnBack">
                <input type="submit" value="入力画面に戻って修正する" onclick="window.onbeforeunload=null;">
            </p>
            <?php echo form_hidden('mode', 'return'); ?>
            <?php echo form_hidden('company_name1', @${$segment}->company_name1); ?>
            <?php echo form_hidden('company_name2', @${$segment}->company_name2); ?>
            <?php echo form_hidden('company_name3', @${$segment}->company_name3); ?>
            <?php echo form_hidden('kibouarea1', @${$segment}->kibouarea1); ?>
            <?php echo form_hidden('kibouarea2', @${$segment}->kibouarea2); ?>
            <?php echo form_hidden('kibouarea3', @${$segment}->kibouarea3); ?>
            <?php echo form_hidden('start_year1', @${$segment}->start_year1); ?>
            <?php echo form_hidden('start_year2', @${$segment}->start_year2); ?>
            <?php echo form_hidden('start_year3', @${$segment}->start_year3); ?>
            <?php echo form_hidden('start_month1', @${$segment}->start_month1); ?>
            <?php echo form_hidden('start_month2', @${$segment}->start_month2); ?>
            <?php echo form_hidden('start_month3', @${$segment}->start_month3); ?>
            <?php echo form_hidden('end_year1', @${$segment}->end_year1); ?>
            <?php echo form_hidden('end_year2', @${$segment}->end_year2); ?>
            <?php echo form_hidden('end_year3', @${$segment}->end_year3); ?>
            <?php echo form_hidden('end_month1', @${$segment}->end_month1); ?>
            <?php echo form_hidden('end_month2', @${$segment}->end_month2); ?>
            <?php echo form_hidden('end_month3', @${$segment}->end_month3); ?>

            <?php echo form_hidden('kiboujob', @${$segment}->kiboujob); ?>
            <?php echo form_hidden('tantou_job1', @${$segment}->tantou_job1); ?>
            <?php echo form_hidden('tantou_job2', @${$segment}->tantou_job2); ?>
            <?php echo form_hidden('tantou_job3', @${$segment}->tantou_job3); ?>
            <?php echo form_hidden('income', @${$segment}->income); ?>
            <?php echo form_hidden('earnings', @${$segment}->earnings); ?>
            <?php echo form_hidden('tenkin', @${$segment}->tenkin); ?>
            <?php echo form_hidden('file1', ${$segment}->file1); ?>
            <?php echo form_hidden('file2', ${$segment}->file2); ?>
            <?php echo form_hidden('file3', ${$segment}->file3); ?>
            <?php echo form_hidden('mail1', ${$segment}->mail1); ?>
            <?php echo form_hidden('comment', ${$segment}->comment); ?>
            <?php echo form_hidden('tenshoku_kiboubi', ${$segment}->tenshoku_kiboubi); ?>

            <?php echo form_hidden('how', ${$segment}->how); ?>
            <input type="hidden" name="pwdchk" value="1" />
            <?php echo form_close();?>
        </div>
