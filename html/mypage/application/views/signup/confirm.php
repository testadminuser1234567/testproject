<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);
$job_ary=array();
if(!empty($tmpjob['Item'])){
    foreach($tmpjob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $job_ary as $key => $value) {
        $sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($sort, SORT_ASC, $job_ary);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

/*
$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);
$tantou_job_ary=array();
if(!empty($tmptantoujob['Item'])){
	foreach($tmptantoujob['Item'] as $k=>$v){
		if(!empty($v['Item'])){
			foreach($v['Item'] as $k2=>$v2){
				$tantou_job_ary[] = $v2;
			}
		}
	}
	foreach ((array) $tantou_job_ary as $key => $value) {
		$tantou_sort[$key] = $value["Option.P_Order"];
	}
}
array_multisort($tantou_sort, SORT_ASC, $tantou_job_ary);
*/
//担当職種（新）
$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);
?>
<script src="<?php echo base_url();?>/js/accordion_ex.js"></script>
<script src="<?php echo base_url();?>js/jquery-1.6.4.min.js" charset="utf-8"></script>
<script src="<?php echo base_url();?>js/form_validation_kiboujouken_step2.js?j=<?php echo date('Ymdhis');?>"></script>

<h1>新規会員登録</h1>
<!-- registInputPage start -->
<div class="registInputPage">
    <p id="step"><img src="<?php echo base_url();?>images/step02.png" class="pc" alt=""/><img src="<?php echo base_url();?>images/step02_sp.png" class="sp" width="100%" alt=""/>
    </p>
    <p>以下の内容で登録します。問題なければ「登録する」ボタンを押してください。</p>
</div>
<!-- registInputPage end -->

<div class="registInputPage form">
    <div id="experienceBox">
        <h2>希望条件</h2>
        <h3>希望職種</h3>
        <div class="inner">
            <p>
                <?php if(!empty($job_ary)):?>
                    <?php foreach($job_ary as $k=>$v):?>
                        <?php if(!empty($_POST["kiboujob"])):?>
                            <?php foreach($_POST["kiboujob"] as $pk => $pv):?>
                                <?php if($pv==$v['Option.P_Id']):?><?php echo str_replace(array(" ","　"),"",htmlspecialchars($v['Option.P_Name'],ENT_QUOTES,'UTF-8'));?><?php if($pk!=count($_POST["kiboujob"])-1):?>、<?php endif;?><?php endif;?>
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
            </p>
        </div>


        <h3>希望年収</h3>
        <div class="inner">
            <p>
                <?php if(empty(${$segment}->income)):?>指定なし
                <?php else:?>
                    <?php if(!empty($tmpincome['Item'])):?>
                        <?php foreach($tmpincome['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <?php if($_POST["income"]==$v2['Option.P_Id']):?><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br /><?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
                <?php endif;?>
            </p>
        </div>

        <h3>転勤</h3>
        <div class="inner">
            <p>
                <?php if(!empty($_POST["tenkin"])):?>
                    <?php foreach($_POST["tenkin"] as $k => $v):?>
                        <?php echo str_replace(array(" ","　"),"",htmlspecialchars($v, ENT_QUOTES, 'UTF-8'));?><?php if($k!=count($_POST["tenkin"])-1):?>、<?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
                </p>
            </div>

        <h2>直近3社のご経験</h2>
        <h3>現在または直前の勤務先</h3>
        <div class="experienceList">
        <div class="inner">
            <h4>会社名</h4>
            <p>
                <?php echo set_value('company_name1'); ?>
            </p>
            </div>
            <div class="inner">
            <table>
                <tbody>
                <tr>
                    <th>勤務開始日</th>
                    <th>&nbsp;</th>
                    <th>勤務終了日</th>
                </tr>
                <tr>
                    <td>
                        <?php echo set_value('start_year1'); ?><?php if(!empty($_POST["start_year1"])):?>年<?php endif;?><?php echo set_value('start_month1'); ?><?php if(!empty($_POST["start_month1"])):?>月<?php endif;?>
                    </td>
                    <td class="point">～</td>
                    <td>
                        <?php echo set_value('end_year1'); ?><?php if(!empty($_POST["end_year1"])):?>年<?php endif;?><?php echo set_value('end_month1'); ?><?php if(!empty($_POST["end_month1"])):?>月<?php endif;?>
                    </td>
                </tr>
                </tbody>
            </table>
                </div>

            <div class="inner">
            <h4>現在の年収</h4>
            <p>
                <?php if(empty(${$segment}->income)):?>指定なし
                <?php else:?>
                    <?php if(!empty($tmpincome['Item'])):?>
                        <?php foreach($tmpincome['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <?php if($_POST["earnings"]==$v2['Option.P_Id']):?><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br /><?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
                <?php endif;?>
            </p>
            </div>
            <div class="inner">
            <h4>経験職種</h4>
            <?php
            $flg=array();
            $datas=array();
            if(!empty($tmptantoujob)){
                foreach($tmptantoujob as $k=>$v){
                    if(!empty($v["Items"])){
                        foreach($v["Items"] as $k2=>$v2){
                            if(!empty($v2)){
                                foreach($v2 as $k3=>$v3){

                                    $datas[$k3]["title"] = $v3["Option.P_Name"];
                                    $datas[$k3]["id"] = $v3["Option.P_Id"];
                                    if(!empty($v3["Items"])){
                                        foreach($v3["Items"] as $k4=>$v4){
                                            if(!empty($v4)){
                                                foreach($v4 as $k5=>$v5){
                                                    foreach(${$segment}->tantou_job1 as $jk=>$jv){
                                                        if($jv==$v5["Option.P_Id"]){
                                                            $flg[$k3] = 1;
                                                        }
                                                    }
                                                    $datas[$k3][$k5]["title"] = $v5["Option.P_Name"];
                                                    $datas[$k3][$k5]["id"] = $v5["Option.P_Id"];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ?>
            <?php if(!empty($datas)):?>
                <?php foreach($datas as $k=>$v):?>
                    <?php if($flg[$k]==1):?><?php if($k!=0):?><?php endif;?>
                <h5><?php echo htmlspecialchars($v["title"],ENT_QUOTES,'UTF-8');?></h5>
                        <?php $unit="<p>";?>
                        <?php foreach($v as $k2 => $v2):?>
                            <?php if(is_array($v2)):?>
                <?php foreach($_POST["tantou_job1"] as $jk=>$jv):?><?php if($jv==$v2["id"]):?><?php $unit .= str_replace(array(" ","　"),"",htmlspecialchars($v2['title'],ENT_QUOTES,'UTF-8')) . "|";?><?php endif;?><?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?><?php $unit = rtrim($unit,"|");?><?php $unit.="</p>";?><?php $unit = str_replace("|","、",$unit);?>
                        <?php echo $unit;?>
                    <?php endif;?>
                <?php endforeach;?>
            <?php endif;?>
            </div>
        </div>
        <?php if(@$_POST["company_name2"] || @$_POST["start_year2"] || @$_POST["start_month2"] || @$_POST["end_year2"] || @$_POST["end_month2"] || @$_POST["tantou_job2"]):?>
        <div class="experienceList">
        <h3>さらに前の勤務先1</h3>
        <div class="inner">
            <h4>会社名</h4>
            <p>
                <?php echo set_value('company_name2'); ?>
            </p>
            </div>
            <div class="inner">
            <table>
                <tbody>
                <tr>
                    <th class="<?php if(@${$segment}->start_year2 && @${$segment}->start_month2):?>ok<?php else:?>hissu<?php endif;?> start_year2">勤務開始日</th>
                    <th>&nbsp;</th>
                    <th>勤務終了日</th>
                </tr>
                <tr>
                    <td>
                        <?php echo set_value('start_year2'); ?><?php if(!empty($_POST["start_year2"])):?>年<?php endif;?><?php echo set_value('start_month2'); ?><?php if(!empty($_POST["start_month2"])):?>月<?php endif;?>
                    </td>
                    <td class="point">～</td>
                    <td>
                        <?php echo set_value('end_year2'); ?><?php if(!empty($_POST["end_year2"])):?>年<?php endif;?><?php echo set_value('end_month2'); ?><?php if(!empty($_POST["end_month2"])):?>月<?php endif;?>
                    </td>
                </tr>
            </table>
</div>
            <div class="inner">

            <h4 class="<?php if(@${$segment}->tantou_job2):?>ok<?php else:?><?php endif;?> tantou_job2">経験職種</h4>
            <?php
            $flg=array();
            $datas=array();
            if(!empty($tmptantoujob)){
                foreach($tmptantoujob as $k=>$v){
                    if(!empty($v["Items"])){
                        foreach($v["Items"] as $k2=>$v2){
                            if(!empty($v2)){
                                foreach($v2 as $k3=>$v3){

                                    $datas[$k3]["title"] = $v3["Option.P_Name"];
                                    $datas[$k3]["id"] = $v3["Option.P_Id"];
                                    if(!empty($v3["Items"])){
                                        foreach($v3["Items"] as $k4=>$v4){
                                            if(!empty($v4)){
                                                foreach($v4 as $k5=>$v5){
                                                    foreach(${$segment}->tantou_job2 as $jk=>$jv){
                                                        if($jv==$v5["Option.P_Id"]){
                                                            $flg[$k3] = 1;
                                                        }
                                                    }
                                                    $datas[$k3][$k5]["title"] = $v5["Option.P_Name"];
                                                    $datas[$k3][$k5]["id"] = $v5["Option.P_Id"];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ?>
            <?php if(!empty($datas)):?>
                <?php foreach($datas as $k=>$v):?>
                    <?php if($flg[$k]==1):?><?php if($k!=0):?><?php endif;?>
                        <h5><?php echo htmlspecialchars($v["title"],ENT_QUOTES,'UTF-8');?></h5>
                        <?php $unit="<p>";?>
                        <?php foreach($v as $k2 => $v2):?>
                            <?php if(is_array($v2)):?>
                                <?php foreach($_POST["tantou_job2"] as $jk=>$jv):?><?php if($jv==$v2["id"]):?><?php $unit .= str_replace(array(" ","　"),"",htmlspecialchars($v2['title'],ENT_QUOTES,'UTF-8')) . "|";?><?php endif;?><?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?><?php $unit = rtrim($unit,"|");?><?php $unit.="</p>";?><?php $unit = str_replace("|","、",$unit);?>
                        <?php echo $unit;?>
                    <?php endif;?>
                <?php endforeach;?>
            <?php endif;?>
        </div>
            </div>
<?php endif;?>

        <?php if(@$_POST["company_name3"] || @$_POST["start_year3"] || @$_POST["start_month3"] || @$_POST["end_year3"] || @$_POST["end_month3"] || @$_POST["tantou_job3"]):?>
        <div class="experienceList">
            <h3>さらに前の勤務先2</h3>
        <div class="inner">
            <h4 class="<?php if(@${$segment}->company_name3):?>ok<?php else:?><?php endif;?> company_name3">会社名</h4>
            <p>
                <?php echo set_value('company_name3'); ?>
            </p>
            </div>
            <div class="inner">
            <table>
                <tbody>
                <tr>
                    <th class="<?php if(@${$segment}->start_year3 && @${$segment}->start_month3):?>ok<?php else:?>hissu<?php endif;?> start_year3">勤務開始日</th>
                    <th>&nbsp;</th>
                    <th>勤務終了日</th>
                </tr>
                <tr>
                    <td>
                        <?php echo set_value('start_year3'); ?><?php if(!empty($_POST["start_year3"])):?>年<?php endif;?><?php echo set_value('start_month3'); ?><?php if(!empty($_POST["start_month3"])):?>月<?php endif;?>
                    </td>
                    <td class="point">～</td>
                    <td>
                        <?php echo set_value('end_year3'); ?><?php if(!empty($_POST["end_year3"])):?>年<?php endif;?><?php echo set_value('end_month3'); ?><?php if(!empty($_POST["end_month3"])):?>月<?php endif;?>
                    </td>
                </tr>
            </table>
                </div>
            <div class="inner">



            <h4 class="<?php if(@${$segment}->tantou_job3):?>ok<?php else:?><?php endif;?> tantou_job3">経験職種</h4>
            <?php
            $flg=array();
            $datas=array();
            if(!empty($tmptantoujob)){
                foreach($tmptantoujob as $k=>$v){
                    if(!empty($v["Items"])){
                        foreach($v["Items"] as $k2=>$v2){
                            if(!empty($v2)){
                                foreach($v2 as $k3=>$v3){

                                    $datas[$k3]["title"] = $v3["Option.P_Name"];
                                    $datas[$k3]["id"] = $v3["Option.P_Id"];
                                    if(!empty($v3["Items"])){
                                        foreach($v3["Items"] as $k4=>$v4){
                                            if(!empty($v4)){
                                                foreach($v4 as $k5=>$v5){
                                                    foreach(${$segment}->tantou_job3 as $jk=>$jv){
                                                        if($jv==$v5["Option.P_Id"]){
                                                            $flg[$k3] = 1;
                                                        }
                                                    }
                                                    $datas[$k3][$k5]["title"] = $v5["Option.P_Name"];
                                                    $datas[$k3][$k5]["id"] = $v5["Option.P_Id"];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ?>
            <?php if(!empty($datas)):?>
                <?php foreach($datas as $k=>$v):?>
                    <?php if($flg[$k]==1):?><?php if($k!=0):?><?php endif;?>
                        <h5><?php echo htmlspecialchars($v["title"],ENT_QUOTES,'UTF-8');?></h5>
                        <?php $unit="<p>";?>
                        <?php foreach($v as $k2 => $v2):?>
                            <?php if(is_array($v2)):?>
                                <?php foreach($_POST["tantou_job3"] as $jk=>$jv):?><?php if($jv==$v2["id"]):?><?php $unit .= str_replace(array(" ","　"),"",htmlspecialchars($v2['title'],ENT_QUOTES,'UTF-8')) . "|";?><?php endif;?><?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?><?php $unit = rtrim($unit,"|");?><?php $unit.="</p>";?><?php $unit = str_replace("|","、",$unit);?>
                        <?php echo $unit;?>
                    <?php endif;?>
                <?php endforeach;?>
            <?php endif;?>
        </div>
            </div>
<?php endif;?>
        <h2>その他：ご希望等記入欄</h2>
        <div class="inner">
            <p><?php echo nl2br(set_value('comment')); ?></p>
        </div>


        <h2>ヒューレックスをどちらで<br>お知りになりましたか？</h2>
        <div class="inner">
            <p>
                <?php echo set_value('how'); ?>
            </p>
            </div>


    </div>



    <?php echo form_open('signup/add/');?>
    <div class="btnBox">
        <button class="btn" onclick="window.onbeforeunload=null;"><span>送信する</span></button>
    </div>
    <?php echo form_hidden('mode', 'send'); ?>

    <?php echo form_hidden('company_name1', @${$segment}->company_name1); ?>
    <?php echo form_hidden('company_name2', @${$segment}->company_name2); ?>
    <?php echo form_hidden('company_name3', @${$segment}->company_name3); ?>
    <?php echo form_hidden('start_year1', @${$segment}->start_year1); ?>
    <?php echo form_hidden('start_year2', @${$segment}->start_year2); ?>
    <?php echo form_hidden('start_year3', @${$segment}->start_year3); ?>
    <?php echo form_hidden('start_month1', @${$segment}->start_month1); ?>
    <?php echo form_hidden('start_month2', @${$segment}->start_month2); ?>
    <?php echo form_hidden('start_month3', @${$segment}->start_month3); ?>
    <?php echo form_hidden('end_year1', @${$segment}->end_year1); ?>
    <?php echo form_hidden('end_year2', @${$segment}->end_year2); ?>
    <?php echo form_hidden('end_year3', @${$segment}->end_year3); ?>
    <?php echo form_hidden('end_month1', @${$segment}->end_month1); ?>
    <?php echo form_hidden('end_month2', @${$segment}->end_month2); ?>
    <?php echo form_hidden('end_month3', @${$segment}->end_month3); ?>

    <?php echo form_hidden('kiboujob', @${$segment}->kiboujob); ?>
    <?php echo form_hidden('tantou_job1', @${$segment}->tantou_job1); ?>
    <?php echo form_hidden('tantou_job2', @${$segment}->tantou_job2); ?>
    <?php echo form_hidden('tantou_job3', @${$segment}->tantou_job3); ?>
    <?php echo form_hidden('income', ${$segment}->income); ?>
    <?php echo form_hidden('earnings', ${$segment}->earnings); ?>
    <?php echo form_hidden('tenkin', @${$segment}->tenkin); ?>
    <?php echo form_hidden('file1', ${$segment}->file1); ?>
    <?php echo form_hidden('file2', ${$segment}->file2); ?>
    <?php echo form_hidden('file3', ${$segment}->file3); ?>
    <?php echo form_hidden('mail1', ${$segment}->mail1); ?>
    <?php echo form_hidden('comment', ${$segment}->comment); ?>
    <?php echo form_hidden('how', ${$segment}->how); ?>
    <input type="hidden" name="pwdchk" value="0" />
    <input type="hidden" name="hurexid" value="<?php echo htmlspecialchars(${$segment}->hurexid, ENT_QUOTES, 'UTF-8'); ?>" />
    <input type="hidden" name="new_flg" value="<?php echo htmlspecialchars(${$segment}->new_flg, ENT_QUOTES, 'UTF-8'); ?>" />
    <?php echo form_close();?>

    <?php
    $url = "signup/step2/";
    if(!empty(${$segment}->new_flg) && ${$segment}->new_flg == 1){
        $url .= "?rt=1";
    }
    ?>
    <?php echo form_open("$url");?>
    <p class="btnBack">
        <input type="submit" value="入力画面に戻って修正する" onclick="window.onbeforeunload=null;">
    </p>
    <?php echo form_hidden('mode', 'return'); ?>
    <?php echo form_hidden('company_name1', @${$segment}->company_name1); ?>
    <?php echo form_hidden('company_name2', @${$segment}->company_name2); ?>
    <?php echo form_hidden('company_name3', @${$segment}->company_name3); ?>
    <?php echo form_hidden('start_year1', @${$segment}->start_year1); ?>
    <?php echo form_hidden('start_year2', @${$segment}->start_year2); ?>
    <?php echo form_hidden('start_year3', @${$segment}->start_year3); ?>
    <?php echo form_hidden('start_month1', @${$segment}->start_month1); ?>
    <?php echo form_hidden('start_month2', @${$segment}->start_month2); ?>
    <?php echo form_hidden('start_month3', @${$segment}->start_month3); ?>
    <?php echo form_hidden('end_year1', @${$segment}->end_year1); ?>
    <?php echo form_hidden('end_year2', @${$segment}->end_year2); ?>
    <?php echo form_hidden('end_year3', @${$segment}->end_year3); ?>
    <?php echo form_hidden('end_month1', @${$segment}->end_month1); ?>
    <?php echo form_hidden('end_month2', @${$segment}->end_month2); ?>
    <?php echo form_hidden('end_month3', @${$segment}->end_month3); ?>

    <?php echo form_hidden('kiboujob', @${$segment}->kiboujob); ?>
    <?php echo form_hidden('tantou_job1', @${$segment}->tantou_job1); ?>
    <?php echo form_hidden('tantou_job2', @${$segment}->tantou_job2); ?>
    <?php echo form_hidden('tantou_job3', @${$segment}->tantou_job3); ?>
    <?php echo form_hidden('income', ${$segment}->income); ?>
    <?php echo form_hidden('earnings', ${$segment}->earnings); ?>
    <?php echo form_hidden('tenkin', @${$segment}->tenkin); ?>
    <?php echo form_hidden('file1', ${$segment}->file1); ?>
    <?php echo form_hidden('file2', ${$segment}->file2); ?>
    <?php echo form_hidden('file3', ${$segment}->file3); ?>
    <?php echo form_hidden('mail1', ${$segment}->mail1); ?>
    <?php echo form_hidden('comment', ${$segment}->comment); ?>
    <?php echo form_hidden('how', ${$segment}->how); ?>
    <input type="hidden" name="pwdchk" value="0" />
    <input type="hidden" name="hurexid" value="<?php echo htmlspecialchars(${$segment}->hurexid, ENT_QUOTES, 'UTF-8'); ?>" />
    <input type="hidden" name="new_flg" value="<?php echo htmlspecialchars(${$segment}->new_flg, ENT_QUOTES, 'UTF-8'); ?>" />

    <?php echo form_close();?>


</div>

