<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

?>
<section class="co form">
    <div class="inner">
        <h1><span class="line">新規会員登録</span></h1>
        <p><img src="<?php echo base_url();?>images/step03.png" class="pc" alt=""/><img src="<?php echo base_url();?>images/step03_sp.png" class="sp" width="100%" alt=""/>
        </p>
        <p>以下の内容で送信致します。
        </p>


        <table width="100%" class="confirm">
            <tr>
                <th class="<?php if(@${$segment}->comment):?>ok<?php else:?>hissu<?php endif;?> comment">ご希望の参加日を<br />ご記入ください</th>
                <td><?php echo set_value('comment'); ?></td>
            </tr>
            <tr>
                <th  class="<?php if(@${$segment}->password):?>ok<?php else:?>hissu<?php endif;?> password">パスワード</th>
                <td><?php echo str_repeat('*', strlen(@${$segment}->password));?></td>
            </tr>

        </table>



        <!-- start -->
        <div class="confirmBtnBox clearfix">
            <div class="send">
                <?php echo form_open('signup/new_password_ui_send/');?>
                <p class="btnSend">
                    <input type="submit" value="送信する" onclick="window.onbeforeunload=null;">
                </p>
                <?php echo form_hidden('mode', 'send'); ?>
                <?php echo form_hidden('comment', ${$segment}->comment); ?>
                <?php echo form_hidden('password', ${$segment}->password); ?>
                <?php echo form_hidden('password_check', ${$segment}->password_check); ?>
                <?php echo form_hidden('param', ${$segment}->param); ?>
                <?php echo form_hidden('hash', ${$segment}->hash); ?>

                <?php echo form_close();?>
            </div>
            <div class="back">
                <?php echo form_open('signup/create/');?>
                <p class="btnBack">
                    <input type="submit" value="入力画面に戻って修正する" onclick="window.onbeforeunload=null;">
                </p>
                <?php echo form_hidden('mode', 'return'); ?>
                <?php echo form_hidden('comment', ${$segment}->comment); ?>
                <?php echo form_hidden('password', ${$segment}->password); ?>
                <?php echo form_hidden('password_check', ${$segment}->password_check); ?>
                <?php echo form_hidden('param', ${$segment}->param); ?>
                <?php echo form_hidden('hash', ${$segment}->hash); ?>
                <?php echo form_close();?>
            </div>
        </div>
        <!-- end -->

    </div>
</section>
<!-- co end -->
<!-- co start -->
<section class="co">
    <div class="inner">
        <h1 class="ss"><span class="line">ご登録･ご利用に際してのご確認事項</span></h1>
        <p>マイページにご登録、ご利用の際には「個人情報の取扱い」「利用規約/免責事項」を必ずご確認の上ご利用下さい。<br>
            尚、ご登録時に本情報についての確認が発生いたしますので必ずご確認下さいませ。</p>
        <div class="centered">
            <ul class="btn clearfix">
                <li><a href="https://www.hurex.jp/company/privacy-policy/" target="_blank"><span>個人情報の取扱い</span></a></li>
                <li><a href="https://www.hurex.jp/company/responsibility/" target="_blank"><span>利用規約/免責事項</span></a></li>
            </ul>
        </div>
    </div>
</section>
