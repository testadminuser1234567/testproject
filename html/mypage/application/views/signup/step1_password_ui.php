<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

?>
<!-- co start -->
<script src="<?php echo base_url();?>js/jquery-1.6.4.min.js" charset="utf-8"></script>
<script src="<?php echo base_url();?>js/form_validation_ui.js"></script>
<h1>新規会員登録</h1>
<!-- registInputPage start -->
<div class="registInputPage">
    <p><img src="<?php echo base_url();?>images/step01.png" class="pc" alt=""/><img src="<?php echo base_url();?>images/step01_sp.png" class="sp" width="100%" alt=""/>
    </p>
</div>
<!-- registInputPage end -->
<?php echo form_open_multipart('signup/step2/');?>
<div id="passwordBox" class="form">
    <div class="inner">
        <table>
            <tbody>
            <tr>
                <th>パスワード<br>
                    <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->password):?>ok<?php else:?>hissu<?php endif;?> password"/></th>
                <td>
                    <?php echo form_password(array('name'=>"password", 'value'=>@${$segment}->password, 'class'=>"required txt", 'id'=>'password'));?>
                    <span class="txtCaution">8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</span><div id="password_err"></div>
                </td>
            </tr>
            <tr>
                <th>パスワード(確認用）<br>
                    <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->password):?>ok<?php else:?>hissu<?php endif;?> password_check"/></th>
                <td>
                    <?php echo form_password(array('name'=>"password_check", 'value'=>@${$segment}->password, 'class'=>"required txt", 'id'=>'password_check'));?>
                    <span class="txtCaution">8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</span><div id="password_check_err"></div>
                </td>
            </tr>
            <tr>
                <th class="<?php if(@${$segment}->kiboubi):?>ok<?php else:?>hissu<?php endif;?> kiboubi">ご希望の参加日を<br />ご記入ください</th>
                <td><?php echo form_textarea(array('id'=>'kiboubi', 'name'=>'kiboubi', 'value'=>@${$segment}->kiboubi, 'class'=>"required", 'placeholder'=>'入力例：4月1日'));?></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="registInputPage form">
    <!-- privacyBox start -->
    <div id="privacyBox">
        <div id="privacyBoxSub" class="inner">
            <p><a href="https://www.hurex.jp/company/privacy-policy/index.html" target="_blank">個人情報保護への取り組み</a>をご覧ください</p>
            <p class="checkboxrequired">
                <label for="agreeChk" class="CheckBoxLabelClass privacy<?php if(@${$segment}->agree==1):?> checked<?php endif;?>" id="agr"><input type="hidden" name="agree" value="" /><input type="checkbox" class="rc checkbox agree" name="agree" value="1" id="agreeChk" <?php if(@${$segment}->agree=="1"):?>checked<?php endif;?>>個人情報保護方針に同意する</label>
            </p>
        </div>
    </div>
    <!-- privacyBox end -->

    <div class="error"><?php echo $msg ?></div>
    <div class="btnBox">
    <button class="btn" onclick="return check(1);"><span>基本情報を登録する</span></button>
    </div>
</div>
<input type="hidden" name="hurexid" value="<?php echo $clean->purify(${$segment}->hurexid);?>" />
<input type="hidden" name="hash" value="<?php echo $clean->purify(@${$segment}->hash);?>" />
<input type="hidden" name="param" value="<?php echo $clean->purify($param);?>" />
<input type="hidden" name="pwdchk" value="1" />
<?php echo form_close();?>
