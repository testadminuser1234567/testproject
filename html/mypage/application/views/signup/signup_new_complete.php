<!-- co start -->
<h1>新規会員登録</h1>
<!-- registInputPage start -->
<div class="registInputPage">
<h2 class="title aCenter">会員登録を受け付けました</h2>
<p class="aCenter">ご入力いただいたメールアドレス宛に、確認のメールを送信致しました。<br>
    メールに記載のリンクをクリックし、詳細情報登録へお進みください。</p>
    <!-- btnBox start -->
        <div class="btnBox type02">
        <ul>
            <li><a href="javascript:;" onClick="window.close(); return false;"><span>この画面を閉じる</span></a></li>
            <li class="home"><a href="https://www.hurex.jp/"><span>HUREXホームページ</span></a></li>
        </ul>
            </div>
        <!-- btnBox start -->
</div>
<!-- registInputPage end -->
<!-- securityBox start -->
<div id="securityBox">
    <p><strong>HUREXは万全のセキュリティ体制</strong><br> 現在の職場やご友人など、第三者に知らせることは決してございませんので安心してご登録下さい。
        <a href="https://www.hurex.jp/company/privacy-policy/" target="_blank" class="link">プライバシーポリシー</a>
    </p>
</div>
<!-- securityBox end -->