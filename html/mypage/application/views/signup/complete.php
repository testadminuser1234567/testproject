<h1>新規会員登録</h1>
<!-- registInputPage start -->
<div class="registInputPage">
    <p id="step"><img src="<?php echo base_url();?>images/step03.png" class="pc" alt=""/><img src="<?php echo base_url();?>images/step03_sp.png" class="sp" width="100%" alt=""/></p>
    <?php if($_GET["j"]):?>
    <p class="finish pc">ご登録ありがとうございました。<br>
        今回お問い合わせいただいた求人の他にご希望条件に合う求人を探してみましょう。</p>
    <p class="finish sp">ご登録ありがとうございました。<br>
        今回お問い合わせいただいた求人の他に<br>
        ご希望条件に合う求人を探してみましょう。</p>
    <?php else:?>
        <p class="finish">ご登録ありがとうございました。<br>
            登録した希望条件に合致する求人はコチラ▼</p>
    <?php endif;?>
    <div class="aCenter">
    <p class="btnFinish">
        <a href="<?php echo base_url();?>user/login/?p=k"><span class="ico">希望条件に合致する求人をみる</span><br>
            <span class="ss">マイページにログインする</span></a>
    </p>
    </div>
    <p class="txtLink"><a href="<?php echo base_url();?>user/login/">マイページTOPへ→</a></p>
</div>
<!-- registInputPage end -->
<!-- Begin INDEED conversion code -->
<script type="text/javascript">
    /* <![CDATA[ */
    var indeed_conversion_id = '5775465888875426';
    var indeed_conversion_label = '';
    /* ]]> */
</script>
<script type="text/javascript" src="//conv.indeed.com/applyconversion.js">
</script>
<noscript>
    <img height=1 width=1 border=0 src="//conv.indeed.com/pagead/conv/5775465888875426/?script=0">
</noscript>
<!-- End INDEED conversion code -->
<!-- Facebook Conversion Code for ヒューレックス　登録 -->
<script>(function() {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded) {
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
        }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6022076393030', {'value':'0.00','currency':'JPY'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none"
               src="https://www.facebook.com/tr?ev=6022076393030&amp;cd[value]=0.00&amp;cd[
currency]=JPY&amp;noscript=1"
    /></noscript>
<!-- Facebook Conversion Code for ヒューレックス　リマーケ -->
<script>(function() {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded) {
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
        }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6022076371230', {'value':'0.00','currency':'JPY'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none"
               src="https://www.facebook.com/tr?ev=6022076371230&amp;cd[value]=0.00&amp;cd[
currency]=JPY&amp;noscript=1"
    /></noscript>
<!-- Facebook Conversion Code for 転職支援申込み -->
<script>(function() {
        var _fbq = window._fbq || (window._fbq = []); if (!_fbq.loaded) { var fbds = document.createElement('script'); fbds.async = true; fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
        }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6035195483782', {'value':'0.01','currency':'USD'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none"
               src="https://www.facebook.com/tr?ev=6035195483782&amp;cd[value]=0.01&amp;cd[
currency]=USD&amp;noscript=1"
    /></noscript>
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
    fbq('init', '924312387607433');
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
    /></noscript>
<!-- End Facebook Pixel Code -->
<!-- Facebook Conversion Code for 登録完了 -->
<script>(function() {
        var _fbq = window._fbq || (window._fbq = []);
        if (!_fbq.loaded) {
            var fbds = document.createElement('script');
            fbds.async = true;
            fbds.src = '//connect.facebook.net/en_US/fbds.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(fbds, s);
            _fbq.loaded = true;
        }
    })();
    window._fbq = window._fbq || [];
    window._fbq.push(['track', '6027957872489', {'value':'0.00','currency':'JPY'}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6027957872489&amp;cd[value]=0.00&amp;cd[currency]=JPY&amp;noscript=1" /></noscript>
<?php
$r = htmlspecialchars($_GET["rid"], ENT_QUOTES, 'UTF-8');
?>
<IMG SRC="https://www.rentracks.jp/secure/e.gifx?sid=2057&pid=3142&price=1&quantity=1&reward=-1&cinfo=<?php echo htmlspecialchars($r, ENT_QUOTES, 'UTF-8');?>" width="1" height="1">
<?php if($_GET["action"] == "new" || $_GET["action"] == "job_new" || $_GET["action"] == "ui_new"):?>
    <script type="text/javascript" src="https://js.felmat.net/fmcv.js?adid=L21505&uqid=<?php echo htmlspecialchars($r, ENT_QUOTES, 'UTF-8');?>"></script>
<script type="text/javascript">
(function(){
function loadScriptRTCV(callback){
var script = document.createElement('script');
script.type = 'text/javascript';
script.src = 'https://www.rentracks.jp/js/itp/rt.track.js?t=' + (new Date()).getTime();
if ( script.readyState ) {
script.onreadystatechange = function() {
if ( script.readyState === 'loaded' || script.readyState === 'complete' ) {
script.onreadystatechange = null;
callback();
};
};
} else {
script.onload = function() {
callback();
};
};
document.getElementsByTagName('head')[0].appendChild(script);
}

loadScriptRTCV(function(){
_rt.sid = 2057;
_rt.pid = 3142;
_rt.price = 0;
_rt.reward = -1;
_rt.cname = '';
_rt.ctel = '';
_rt.cemail = '';
_rt.cinfo = "<?php echo htmlspecialchars($r, ENT_QUOTES, 'UTF-8');?>";
rt_tracktag();
});
}(function(){}));
</script>
<?php endif;?>