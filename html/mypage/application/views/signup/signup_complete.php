<!-- co start -->
<h1><img src="<?php echo base_url();?>images/ico_free.png" width="40" height="21" alt="無料"/>新規会員登録</h1>
<!-- registPage start -->
<div id="registPage">
    <div id="mailBox">
        <p class="txt">ご入力頂いたメールアドレスに仮登録完了のメールを送信しました。<br />
            メールの内容を確認後、本登録をしてください。</p>
        <div class="btn">
            <?php
            $add_param="";
            $split_param=array("","");
            if(!empty($param)){
                $split_param = str_replace("'","", $param);
                $split_param = explode("|",$split_param);
                $add_param = "?" . htmlspecialchars($split_param[0],ENT_QUOTES, 'UTF-8') . "=" .  htmlspecialchars($split_param[1],ENT_QUOTES, 'UTF-8');
            }
            ?>
            <div class="aCenter">
            <p class="btnLogin"><a href="<?php echo base_url();?>user/login/<?php echo $add_param;?>"><span>マイページにログインする</span></a>
            </p>
            </div>
        </div>
    </div>
</div>
<!-- Begin INDEED conversion code -->
<script type="text/javascript">
    /* <![CDATA[ */
    var indeed_conversion_id = '5775465888875426';
    var indeed_conversion_label = '';
    /* ]]> */
</script>
<script type="text/javascript" src="//conv.indeed.com/applyconversion.js">
</script>
<noscript>
    <img height=1 width=1 border=0 src="//conv.indeed.com/pagead/conv/5775465888875426/?script=0">
</noscript>
<!-- End INDEED conversion code -->