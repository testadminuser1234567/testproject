<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);
$job_ary=array();
if(!empty($tmpjob['Item'])){
    foreach($tmpjob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $job_ary as $key => $value) {
        $sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($sort, SORT_ASC, $job_ary);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

/*
$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);
$tantou_job_ary=array();
if(!empty($tmptantoujob['Item'])){
	foreach($tmptantoujob['Item'] as $k=>$v){
		if(!empty($v['Item'])){
			foreach($v['Item'] as $k2=>$v2){
				$tantou_job_ary[] = $v2;
			}
		}
	}
	foreach ((array) $tantou_job_ary as $key => $value) {
		$tantou_sort[$key] = $value["Option.P_Order"];
	}
}
array_multisort($tantou_sort, SORT_ASC, $tantou_job_ary);
*/
//担当職種（新）
$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);


$start=date('Y');
$end=$start-65;
?>
<script src="<?php echo base_url();?>/js/accordion_ex.js"></script>
<script src="<?php echo base_url();?>js/jquery-1.6.4.min.js" charset="utf-8"></script>
<script src="<?php echo base_url();?>js/form_validation_kiboujouken_step2.js?j=<?php echo date('Ymdhis');?>"></script>

<h1>新規会員登録</h1>
<!-- registInputPage start -->
<div class="registInputPage">
    <p id="step"><img src="<?php echo base_url();?>images/step02.png" class="pc" alt=""/><img src="<?php echo base_url();?>images/step02_sp.png" class="sp" width="100%" alt=""/>
    </p>
    <p>基本情報のご登録が完了しました。次に、希望条件と職歴のご入力をお願い致します。※３つ以上職歴のある方は直近の３つの職歴をご入力ください。</p>
</div>
<!-- registInputPage end -->

<div class="error"><?php echo $msg ?></div>
<?php
$url = "signup/confirm/";
if(!empty($new_flg) && $new_flg == 1){
    $url .= "?rt=1";
}
?>
<?php echo form_open_multipart("$url");?>
<div class="registInputPage form">
    <table width="100%">


        <tr>
            <th>希望職種<br /><img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->kiboujob):?>ok<?php else:?>hissu<?php endif;?>.png" width="49" height="14" alt="" class="<?php if(@${$segment}->kiboujob):?>ok<?php else:?>hissu<?php endif;?> kiboujob"/></th>
            <td class="checkboxrequired">
                <?php if(!empty($job_ary)):?>
                    <ul class="job clearfix">
                        <?php foreach($job_ary as $k=>$v):?>
                            <li class="<?php if($v['Option.P_Name']=="営業系"):?>ss<?php endif;?><?php if($v['Option.P_Name']=="管理部門/事務系"):?>ss no<?php endif;?>"><input type="checkbox" class="checkbox joballchk jobcheckbox<?php if($k2-1==count($tmpjob['Item'])):?> required<?php endif;?>" name="kiboujob[]" value="<?php echo htmlspecialchars($v['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty(${$segment}->kiboujob)):?><?php foreach(${$segment}->kiboujob as $jk=>$jv):?><?php if($jv==$v["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?> id="cat_job0<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>"><label id="joblabel<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" for="cat_job0<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" class="checkboxlabel joblabel CheckBoxLabelClass joballnavichecklabel <?php if(!empty(${$segment}->kiboujob)):?><?php foreach(${$segment}->kiboujob as $jk=>$jv):?><?php if($jv==$v["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?>"><?php echo htmlspecialchars($v['Option.P_Name'],ENT_QUOTES,'UTF-8');?></label></li>
                        <?php endforeach;?>
                    </ul>
                <?php endif;?>
            </td>
        </tr>


        <tr>
            <th>希望年収<br>
                <img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->income):?>ok<?php else:?>hissu<?php endif;?>.png" width="49" height="14" alt="" class="<?php if(@${$segment}->income):?>ok<?php else:?>hissu<?php endif;?> income"/></th>
            <td>
                <?php if(!empty($tmpincome['Item'])):?>
                    <select name="income" id="income" class="required">
                        <option value="">指定なし</option>
                        <?php foreach($tmpincome['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <option value="<?php echo htmlspecialchars($v2["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" <?php if($v2["Option.P_Id"]==@${$segment}->income):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    </select>
                <?php endif;?>
            </td>
        </tr>

        <tr>
            <th>転勤可否<br>
                <img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->tenkin):?>ok<?php else:?>hissu<?php endif;?>.png" width="49" height="14" alt="" class="<?php if(@${$segment}->tenkin):?>ok<?php else:?>hissu<?php endif;?> tenkin"/></th>
            <td class="checkboxrequired">
                <ul class="two">
                    <?php if(!empty($tmptenkin['Item'])):?>
                        <?php foreach($tmptenkin['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?><li>
                                    <input type="checkbox" class="checkbox chk tenkincheckbox<?php if($k2-1==count($tmptenkin['Item'])):?> required<?php endif;?>" name="tenkin[]" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty(${$segment}->tenkin)):?><?php if(in_array($v2['Option.P_Name'], @${$segment}->tenkin, true)):?>checked<?php endif;?><?php endif;?> id="tenkin0<?php echo htmlspecialchars($v2["Option.P_Id"],ENT_QUOTES,'UTF-8');?>">
                                    <label for="tenkin0<?php echo htmlspecialchars($v2["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" class="checkboxlabel tenkinlabel  CheckBoxLabelClass navichecklabel">
                                        <?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
                                    </label>
                                    </li>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?></ul>
            </td>
        </tr>


    </table>
    <!-- experienceBox start -->
    <div id="experienceBox">
        <h2>直近3社のご経験</h2>
        <h3>現在または直前の勤務先</h3>
        <div class="inner">
<!-- 会社1 -->
            <h4>会社名<img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->company_name1):?>ok<?php else:?>hissu<?php endif;?>.png" width="49" height="14" alt="" class="<?php if(@${$segment}->company_name1):?>ok<?php else:?>hissu<?php endif;?> company_name1"/></h4>
            <p>
                <?php echo form_input(array('name'=>'company_name1', 'id'=>'company_name1', 'value'=> @${$segment}->company_name1, 'class'=>"txt required", 'placeholder'=>"", 'maxlength'=>25));?>
            </p>
            <table>
                <tbody>
                <tr>
                    <th>勤務開始日<img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->start_year1 && @${$segment}->start_month1):?>ok<?php else:?>hissu<?php endif;?>.png" width="49" height="14" alt="" class="<?php if(@${$segment}->start_year1 && @${$segment}->start_month1):?>ok<?php else:?>hissu<?php endif;?> start_year1"/></th>
                    <th>&nbsp;</th>
                    <th>勤務終了日</th>
                </tr>
                <tr>
                    <td>
                        <select name="start_year1" id="start_year1" class="required">
                            <option value="">年</option>
                            <?php for($i=$start; $i>=$end; $i--):?>
                            <option value="<?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>" <?php if(@${$segment}->start_year1==$i):?>selected<?php endif;?>><?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>年
                                <?php endfor;?>
                        </select>		<select name="start_month1" id="start_month1" class="required">
                            <option value="">月</option>
                            <?php for($i=1; $i<=12; $i++):?>
                            <option value="<?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>" <?php if(@${$segment}->start_month1==$i):?>selected<?php endif;?>><?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>月
                                <?php endfor;?>
                        </select>
                    </td>
                    <td class="point">～</td>
                    <td>
                        <select name="end_year1" id="end_year1" class="">
                            <option value="">年</option>
                            <?php for($i=$start; $i>=$end; $i--):?>
                            <option value="<?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>" <?php if(@${$segment}->end_year1==$i):?>selected<?php endif;?>><?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>年
                                <?php endfor;?>
                        </select>
                        <select name="end_month1" id="end_month1" class="">
                            <option value="">月</option>
                            <?php for($i=1; $i<=12; $i++):?>
                            <option value="<?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>" <?php if(@${$segment}->end_month1==$i):?>selected<?php endif;?>><?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>月
                                <?php endfor;?>
                        </select>
                    </td>
                </tr>
            </table>
            <p id="kinmu_err1"></p>

            <h4>現在の年収<img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->earnings):?>ok<?php else:?>hissu<?php endif;?>.png" width="49" height="14" alt="" class="<?php if(@${$segment}->earnings):?>ok<?php else:?>hissu<?php endif;?> earnings"/></h4>
            <p>
                <?php if(!empty($tmpincome['Item'])):?>
                    <select name="earnings" id="earnings" class="required">
                        <option value="">指定なし</option>
                        <?php foreach($tmpincome['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <option value="<?php echo htmlspecialchars($v2["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" <?php if($v2["Option.P_Id"]==@${$segment}->earnings):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    </select>
                <?php endif;?>
            </p>

            <h4 class="">経験職種<img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->tantou_job1):?>ok<?php else:?>hissu<?php endif;?>.png" width="49" height="14" alt="" class="<?php if(@${$segment}->tantou_job1):?>ok<?php else:?><?php endif;?> tantou_job1 checkboxrequire"/></h4>
            <div class="checkboxrequired">
                <?php
                $flg=array();
                $datas=array();
                if(!empty($tmptantoujob)){
                    foreach($tmptantoujob as $k=>$v){
                        if(!empty($v["Items"])){
                            foreach($v["Items"] as $k2=>$v2){
                                if(!empty($v2)){
                                    foreach($v2 as $k3=>$v3){

                                        $datas[$k3]["title"] = $v3["Option.P_Name"];
                                        $datas[$k3]["id"] = $v3["Option.P_Id"];
                                        if(!empty($v3["Items"])){
                                            foreach($v3["Items"] as $k4=>$v4){
                                                if(!empty($v4)){
                                                    foreach($v4 as $k5=>$v5){
                                                        foreach(${$segment}->tantou_job1 as $jk=>$jv){
                                                            if($jv==$v5["Option.P_Id"]){
                                                                $flg[$k3] = 1;
                                                            }
                                                        }
                                                        $datas[$k3][$k5]["title"] = $v5["Option.P_Name"];
                                                        $datas[$k3][$k5]["id"] = $v5["Option.P_Id"];
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                ?>
                <ul class="experience clearfix">
                    <li class="title <?php if($flg[0]==1):?>checked<?php endif;?>" id="job1_parent_11357"><a href="javascript:;">営業<span>法人営業/個人営業/営業管理職</span></a></li>
                    <div class="sub">
                        <ul class="clearfix">
                            <li><input type="checkbox" class="job1_parent_11357 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11537" id="sub_job1_11537" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11537):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11537" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11537):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">営業(法人)</label></li>
                            <li><input type="checkbox" class="job1_parent_11357 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11538" id="sub_job1_11538"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11538):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11538" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11538):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">営業(個人)</label></li>
                            <li><input type="checkbox" class="job1_parent_11357 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11539" id="sub_job1_11539"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11539):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11539" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11539):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">代理店営業</label></li>
                            <li><input type="checkbox" class="job1_parent_11357 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11540" id="sub_job1_11540"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11540):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11540" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11540):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">海外営業</label></li>
                            <li><input type="checkbox" class="job1_parent_11357 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11541" id="sub_job1_11541"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11541):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11541" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11541):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">プリセールス・営業支援</label></li>
                            <li><input type="checkbox" class="job1_parent_11357 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11542" id="sub_job1_11542"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11542):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11542" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11542):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">その他営業</label></li>
                        </ul>
                    </div>
                    <li class="title <?php if($flg[1]==1):?>checked<?php endif;?>" id="job1_parent_11358"><a href="javascript:;">管理部門/事務<span>経営企画/マーケティング/広報/IR</span></a></li>
                    <div class="sub">
                        <ul class="clearfix">
                            <li><input type="checkbox" class="job1_parent_11358 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11543" id="sub_job1_11543" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11543):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11543" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11543):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">人事/総務</label></li>
                            <li><input type="checkbox" class="job1_parent_11358 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11544" id="sub_job1_11544" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11544):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11544" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11544):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">法務/特許/知財</label></li>
                            <li><input type="checkbox" class="job1_parent_11358 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11545" id="sub_job1_11545" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11545):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11545" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11545):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">経理/財務/株式公開</label></li>
                            <li><input type="checkbox" class="job1_parent_11358 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11546" id="sub_job1_11546" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11546):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11546" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11546):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">広報/IR</label></li>
                            <li><input type="checkbox" class="job1_parent_11358 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11547" id="sub_job1_11547" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11547):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11547" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11547):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">秘書/事務アシスタント/その他</label></li>
                        </ul>
                    </div>
                    <li class="title <?php if($flg[2]==1):?>checked<?php endif;?>" id="job1_parent_11359"><a href="javascript:;">経営幹部/企画/マーケティング<span>エグゼクティブ/営業企画など</span></a></li>
                    <div class="sub">
                        <ul class="clearfix">
                            <li><input type="checkbox" class="job1_parent_11359 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11548" id="sub_job1_11548" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11548):?>checked<?php endif;?><?php endforeach;?><?php endif;?> ><label for="sub_job1_11548" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11548):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">経営管理/エグゼクティブ/事業開発</label></li>
                            <li><input type="checkbox" class="job1_parent_11359 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11549" id="sub_job1_11549"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11549):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11549" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11549):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">マーケティング/広告宣伝/営業企画</label></li>
                            <li><input type="checkbox" class="job1_parent_11359 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11550" id="sub_job1_11550"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11550):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11550" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11550):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">その他(専門コンサルタント)</label></li>
                            <li><input type="checkbox" class="job1_parent_11359 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11551" id="sub_job1_11551"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11551):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11551" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11551):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">購買/物流</label></li>
                        </ul>
                    </div>
                    <li class="title  <?php if($flg[3]==1):?>checked<?php endif;?>" id="job1_parent_11360"><a href="javascript:;">技術職（電気/電子/機械）<span>回路/システム設計/製品企画など</span></a></li>
                    <div class="sub">
                        <ul class="clearfix">
                            <li><input type="checkbox" class="job1_parent_11360 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11552" id="sub_job1_11552"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11552):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11552" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11552):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">基礎研究/製品企画/その他</label></li>
                            <li><input type="checkbox" class="job1_parent_11360 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11553" id="sub_job1_11553"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11553):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11553" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11553):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">光学設計他</label></li>
                            <li><input type="checkbox" class="job1_parent_11360 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11554" id="sub_job1_11554"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11554):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11554" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11554):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">回路/システム設計</label></li>
                            <li><input type="checkbox" class="job1_parent_11360 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11555" id="sub_job1_11555"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11555):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11555" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11555):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">機械/機構/金型設計</label></li>
                            <li><input type="checkbox" class="job1_parent_11360 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11556" id="sub_job1_11556"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11556):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11556" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11556):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">組み込み/制御設計</label></li>
                            <li><input type="checkbox" class="job1_parent_11360 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11557" id="sub_job1_11557"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11557):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11557" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11557):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">生産管理/品質管理/品質保証</label></li>
                            <li><input type="checkbox" class="job1_parent_11360 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11558" id="sub_job1_11558"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11568):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11558" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11558):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">生産技術</label></li>
                            <li><input type="checkbox" class="job1_parent_11360 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11559" id="sub_job1_11559"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11559):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11559" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11559):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">サービスエンジニア</label></li>
                            <li><input type="checkbox" class="job1_parent_11360 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11560" id="sub_job1_11560"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11560):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11560" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11560):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">セールスエンジニア/FAE</label></li>
                            <li><input type="checkbox" class="job1_parent_11360 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11561" id="sub_job1_11561"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11561):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11561" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11561):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">プロセスエンジニア</label></li>
                            <li><input type="checkbox" class="job1_parent_11360 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11562" id="sub_job1_11562"  <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11562):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11562" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11562):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">その他技術職</label></li>
                        </ul>
                    </div>
                    <li class="title  <?php if($flg[4]==1):?>checked<?php endif;?>" id="job1_parent_11361"><a href="javascript:;">技術職（建築/土木）<span>土木設計/施工管理など</span></a></li>
                    <div class="sub">
                        <ul class="clearfix">
                            <li><input type="checkbox" class="job1_parent_11361 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11563" id="sub_job1_11563" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11563):?>checked<?php endif;?><?php endforeach;?><?php endif;?> ><label for="sub_job1_11563" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11563):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">建築/土木設計</label></li>
                            <li><input type="checkbox" class="job1_parent_11361 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11564" id="sub_job1_11564" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11564):?>checked<?php endif;?><?php endforeach;?><?php endif;?> ><label for="sub_job1_11564" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11564):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">建築/土木施工管理</label></li>
                            <li><input type="checkbox" class="job1_parent_11361 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11565" id="sub_job1_11565" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11565):?>checked<?php endif;?><?php endforeach;?><?php endif;?> ><label for="sub_job1_11565" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11565):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">その他(建築・土木)</label></li>
                        </ul>
                    </div>
                    <li class="title  <?php if($flg[5]==1):?>checked<?php endif;?>" id="job1_parent_11362"><a href="javascript:;">技術職（メディカル/化学/食品）<span>医療/福祉/化成/化粧品など</span></a></li>
                    <div class="sub">
                        <ul class="clearfix">
                            <li><input type="checkbox" class="job1_parent_11362 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11566" id="sub_job1_11566" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11566):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11566" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11566):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">医療/福祉サービス</label></li>
                            <li><input type="checkbox" class="job1_parent_11362 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11567" id="sub_job1_11567" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11567):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11567" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11567):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">素材/化成品</label></li>
                            <li><input type="checkbox" class="job1_parent_11362 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11568" id="sub_job1_11568" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11568):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11568" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11568):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">医薬品/医療機器</label></li>
                            <li><input type="checkbox" class="job1_parent_11362 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11569" id="sub_job1_11569" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11569):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11569" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11569):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">化粧品</label></li>
                            <li><input type="checkbox" class="job1_parent_11362 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11570" id="sub_job1_11570" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11570):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11570" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11570):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">食品</label></li>
                        </ul>
                    </div>
                    <li class="title  <?php if($flg[7]==1):?>checked<?php endif;?>" id="job1_parent_11364"><a href="javascript:;">技術職（IT/ソフトウェア/ネットワーク）<span>SE/システム開発/インフラなど</span></a></li>
                    <div class="sub">
                        <ul class="clearfix">
                            <li><input type="checkbox" class="job1_parent_11364 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11571" id="sub_job1_11571" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11571):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11571" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11571):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">システム開発(Web/オープン系)</label></li>
                            <li><input type="checkbox" class="job1_parent_11364 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11572" id="sub_job1_11572" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11572):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11572" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11572):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">システム開発(汎用機系)</label></li>
                            <li><input type="checkbox" class="job1_parent_11364 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11573" id="sub_job1_11573" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11573):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11573" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11573):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">システム開発(組み込み/制御系)</label></li>
                            <li><input type="checkbox" class="job1_parent_11364 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11574" id="sub_job1_11574" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11574):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11574" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11574):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">プロジェクトマネージャー</label></li>
                            <li><input type="checkbox" class="job1_parent_11364 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11575" id="sub_job1_11575" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11575):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11575" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11575):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">研究開発/その他</label></li>
                            <li><input type="checkbox" class="job1_parent_11364 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11576" id="sub_job1_11576" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11576):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11576" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11576):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">データベースエンジニア</label></li>
                            <li><input type="checkbox" class="job1_parent_11364 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11577" id="sub_job1_11577" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11577):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11577" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11577):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">通信インフラ/ネットワーク</label></li>
                            <li><input type="checkbox" class="job1_parent_11364 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11578" id="sub_job1_11578" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11578):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11578" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11578):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">社内情報システム</label></li>
                            <li><input type="checkbox" class="job1_parent_11364 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11579" id="sub_job1_11579" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11579):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11579" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11579):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">テクニカルサポート</label></li>
                            <li><input type="checkbox" class="job1_parent_11364 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11580" id="sub_job1_11580" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11580):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11580" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11580):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">コンサルティング・プリセールス(IT/ネットワーク)</label></li>
                        </ul>
                    </div>
                    <li class="title  <?php if($flg[6]==1):?>checked<?php endif;?>" id="job1_parent_11363"><a href="javascript:;">専門職（コンサルタント/金融/不動産）<span>シンクタンクなど</span></a></li>
                    <div class="sub">
                        <ul class="clearfix">
                            <li><input type="checkbox" class="job1_parent_11363 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11581" id="sub_job1_11581" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11581):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11581" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11581):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">金融</label></li>
                            <li><input type="checkbox" class="job1_parent_11363 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11582" id="sub_job1_11582" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11582):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11582" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11582):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">コンサルタント/シンクタンク研究員</label></li>
                            <li><input type="checkbox" class="job1_parent_11363 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11583" id="sub_job1_11583" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11583):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11583" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11583):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">不動産/プロパティマネジメント</label></li>
                        </ul>
                    </div>
                    <li class="title  <?php if($flg[8]==1):?>checked<?php endif;?>" id="job1_parent_11365"><a href="javascript:;">クリエイティブ職<span>デザイナー/ディレクターなど</span></a></li>
                    <div class="sub">
                        <ul class="clearfix">
                            <li><input type="checkbox" class="job1_parent_11365 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11584" id="sub_job1_11584" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11584):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11584" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11584):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">Webプロデューサー/ディレクター</label></li>
                            <li><input type="checkbox" class="job1_parent_11365 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11585" id="sub_job1_11585" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11585):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11585" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11585):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">Webデザイナー/Webマスター</label></li>
                            <li><input type="checkbox" class="job1_parent_11365 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11586" id="sub_job1_11586" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11586):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11586" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11586):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">Web編集/コンテンツ企画</label></li>
                            <li><input type="checkbox" class="job1_parent_11365 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11587" id="sub_job1_11587" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11587):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11587" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11587):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">広告/メディア/ゲーム/その他</label></li>
                            <li><input type="checkbox" class="job1_parent_11365 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11588" id="sub_job1_11588" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11588):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11588" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11588):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">工業デザイン</label></li>
                            <li><input type="checkbox" class="job1_parent_11365 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11589" id="sub_job1_11589" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11589):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11589" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11589):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">店舗設計/内装</label></li>
                        </ul>
                    </div>
                    <li class="title  <?php if($flg[9]==1):?>checked<?php endif;?>" id="job1_parent_11366"><a href="javascript:;">サービス<span>小売/フード/人材など</span></a></li>
                    <div class="sub">
                        <ul class="clearfix">
                            <li><input type="checkbox" class="job1_parent_11366 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11590" id="sub_job1_11590" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11590):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11590" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11590):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">MD/バイヤー</label></li>
                            <li><input type="checkbox" class="job1_parent_11366 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11591" id="sub_job1_11591" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11591):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11591" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11591):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">店舗開発/FC開発</label></li>
                            <li><input type="checkbox" class="job1_parent_11366 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11592" id="sub_job1_11592" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11592):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11592" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11592):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">スーパーバイザ/その他サービス</label></li>
                            <li><input type="checkbox" class="job1_parent_11366 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11593" id="sub_job1_11593" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11593):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11593" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11593):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">コールセンタースーパーバイザー/カスタマーサポート</label></li>
                            <li><input type="checkbox" class="job1_parent_11366 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11594" id="sub_job1_11594" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11594):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11594" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11594):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">人材サービス/キャリアコンサルタント</label></li>
                            <li><input type="checkbox" class="job1_parent_11366 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11595" id="sub_job1_11595" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11595):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11595" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11595):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">教育/講師/インストラクター</label></li>
                            <li><input type="checkbox" class="job1_parent_11366 checkbox tantoujob1allchk tantoujob1checkbox" name="tantou_job1[]"  value="11596" id="sub_job1_11596" <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11596):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job1_11596" class="checkboxlabel tantoujob1label CheckBoxLabelClass tantoujob1allnavichecklabel <?php if(!empty(${$segment}->tantou_job1)):?><?php foreach(${$segment}->tantou_job1 as $jk=>$jv):?><?php if($jv==11596):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">店長/販売/店舗管理</label></li>
                        </ul>
            </div>
        </div>
</div>
<!-- 会社２ -->
        <h3 class="more"><a href="javascript:;">さらに前の勤務先1</a></h3>
        <div class="inner">
            <h4>会社名</h4>
            <p>
                <?php echo form_input(array('name'=>'company_name2', 'id'=>'company_name2', 'value'=> @${$segment}->company_name2, 'class'=>"txt ", 'placeholder'=>"", 'maxlength'=>25));?>
            </p>
            <table>
                <tbody>
                <tr>
                    <th class="<?php if(@${$segment}->start_year2 && @${$segment}->start_month2):?>ok<?php else:?>hissu<?php endif;?> start_year2">勤務開始日</th>
                    <th>&nbsp;</th>
                    <th>勤務終了日</th>
                </tr>
                <tr>
                    <td>
                        <select name="start_year2" id="start_year2" class="">
                            <option value="">年</option>
                            <?php for($i=$start; $i>=$end; $i--):?>
                            <option value="<?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>" <?php if(@${$segment}->start_year2==$i):?>selected<?php endif;?>><?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>年
                                <?php endfor;?>
                        </select>		<select name="start_month2" id="start_month2" class="">
                            <option value="">月</option>
                            <?php for($i=1; $i<=12; $i++):?>
                            <option value="<?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>" <?php if(@${$segment}->start_month2==$i):?>selected<?php endif;?>><?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>月
                                <?php endfor;?>
                        </select>
                    </td>
                    <td class="point">～</td>
                    <td>
                        <select name="end_year2" id="end_year2" class="">
                            <option value="">年</option>
                            <?php for($i=$start; $i>=$end; $i--):?>
                            <option value="<?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>" <?php if(@${$segment}->end_year2==$i):?>selected<?php endif;?>><?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>年
                                <?php endfor;?>
                        </select>
                        <select name="end_month2" id="end_month2" class="">
                            <option value="">月</option>
                            <?php for($i=1; $i<=12; $i++):?>
                            <option value="<?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>" <?php if(@${$segment}->end_month2==$i):?>selected<?php endif;?>><?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>月
                                <?php endfor;?>
                        </select>
                    </td>
                </tr>
            </table>
            <p id="kinmu_err2"></p>


            <h4 class="<?php if(@${$segment}->tantou_job2):?>ok<?php else:?><?php endif;?> tantou_job2">経験職種</h4>
            <?php
            $flg=array();
            $datas=array();
            if(!empty($tmptantoujob)){
                foreach($tmptantoujob as $k=>$v){
                    if(!empty($v["Items"])){
                        foreach($v["Items"] as $k2=>$v2){
                            if(!empty($v2)){
                                foreach($v2 as $k3=>$v3){

                                    $datas[$k3]["title"] = $v3["Option.P_Name"];
                                    $datas[$k3]["id"] = $v3["Option.P_Id"];
                                    if(!empty($v3["Items"])){
                                        foreach($v3["Items"] as $k4=>$v4){
                                            if(!empty($v4)){
                                                foreach($v4 as $k5=>$v5){
                                                    foreach(${$segment}->tantou_job2 as $jk=>$jv){
                                                        if($jv==$v5["Option.P_Id"]){
                                                            $flg[$k3] = 1;
                                                        }
                                                    }
                                                    $datas[$k3][$k5]["title"] = $v5["Option.P_Name"];
                                                    $datas[$k3][$k5]["id"] = $v5["Option.P_Id"];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ?>
            <ul class="experience clearfix">
                <li class="title <?php if($flg[0]==1):?>checked<?php endif;?>" id="job2_parent_11357"><a href="javascript:;">営業<span>法人営業/個人営業/営業管理職</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job2_parent_11357 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11537" id="sub_job2_11537" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11537):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11537" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11537):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">営業(法人)</label></li>
                        <li><input type="checkbox" class="job2_parent_11357 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11538" id="sub_job2_11538"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11538):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11538" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11538):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">営業(個人)</label></li>
                        <li><input type="checkbox" class="job2_parent_11357 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11539" id="sub_job2_11539"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11539):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11539" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11539):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">代理店営業</label></li>
                        <li><input type="checkbox" class="job2_parent_11357 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11540" id="sub_job2_11540"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11540):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11540" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11540):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">海外営業</label></li>
                        <li><input type="checkbox" class="job2_parent_11357 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11541" id="sub_job2_11541"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11541):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11541" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11541):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">プリセールス・営業支援</label></li>
                        <li><input type="checkbox" class="job2_parent_11357 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11542" id="sub_job2_11542"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11542):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11542" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11542):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">その他営業</label></li>
                    </ul>
                </div>
                <li class="title <?php if($flg[1]==1):?>checked<?php endif;?>" id="job2_parent_11358"><a href="javascript:;">管理部門/事務<span>経営企画/マーケティング/広報/IR</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job2_parent_11358 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11543" id="sub_job2_11543" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11543):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11543" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11543):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">人事/総務</label></li>
                        <li><input type="checkbox" class="job2_parent_11358 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11544" id="sub_job2_11544" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11544):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11544" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11544):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">法務/特許/知財</label></li>
                        <li><input type="checkbox" class="job2_parent_11358 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11545" id="sub_job2_11545" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11545):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11545" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11545):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">経理/財務/株式公開</label></li>
                        <li><input type="checkbox" class="job2_parent_11358 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11546" id="sub_job2_11546" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11546):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11546" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11546):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">広報/IR</label></li>
                        <li><input type="checkbox" class="job2_parent_11358 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11547" id="sub_job2_11547" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11547):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11547" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11547):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">秘書/事務アシスタント/その他</label></li>
                    </ul>
                </div>
                <li class="title <?php if($flg[2]==1):?>checked<?php endif;?>" id="job2_parent_11359"><a href="javascript:;">経営幹部/企画/マーケティング<span>エグゼクティブ/営業企画など</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job2_parent_11359 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11548" id="sub_job2_11548" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11548):?>checked<?php endif;?><?php endforeach;?><?php endif;?> ><label for="sub_job2_11548" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11548):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">経営管理/エグゼクティブ/事業開発</label></li>
                        <li><input type="checkbox" class="job2_parent_11359 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11549" id="sub_job2_11549"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11549):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11549" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11549):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">マーケティング/広告宣伝/営業企画</label></li>
                        <li><input type="checkbox" class="job2_parent_11359 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11550" id="sub_job2_11550"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11550):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11550" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11550):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">その他(専門コンサルタント)</label></li>
                        <li><input type="checkbox" class="job2_parent_11359 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11551" id="sub_job2_11551"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11551):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11551" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11551):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">購買/物流</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[3]==1):?>checked<?php endif;?>" id="job2_parent_11360"><a href="javascript:;">技術職（電気/電子/機械）<span>回路/システム設計/製品企画など</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job2_parent_11360 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11552" id="sub_job2_11552"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11552):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11552" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11552):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">基礎研究/製品企画/その他</label></li>
                        <li><input type="checkbox" class="job2_parent_11360 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11553" id="sub_job2_11553"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11553):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11553" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11553):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">光学設計他</label></li>
                        <li><input type="checkbox" class="job2_parent_11360 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11554" id="sub_job2_11554"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11554):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11554" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11554):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">回路/システム設計</label></li>
                        <li><input type="checkbox" class="job2_parent_11360 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11555" id="sub_job2_11555"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11555):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11555" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11555):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">機械/機構/金型設計</label></li>
                        <li><input type="checkbox" class="job2_parent_11360 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11556" id="sub_job2_11556"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11556):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11556" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11556):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">組み込み/制御設計</label></li>
                        <li><input type="checkbox" class="job2_parent_11360 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11557" id="sub_job2_11557"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11557):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11557" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11557):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">生産管理/品質管理/品質保証</label></li>
                        <li><input type="checkbox" class="job2_parent_11360 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11558" id="sub_job2_11558"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11568):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11558" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11558):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">生産技術</label></li>
                        <li><input type="checkbox" class="job2_parent_11360 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11559" id="sub_job2_11559"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11559):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11559" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11559):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">サービスエンジニア</label></li>
                        <li><input type="checkbox" class="job2_parent_11360 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11560" id="sub_job2_11560"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11560):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11560" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11560):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">セールスエンジニア/FAE</label></li>
                        <li><input type="checkbox" class="job2_parent_11360 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11561" id="sub_job2_11561"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11561):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11561" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11561):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">プロセスエンジニア</label></li>
                        <li><input type="checkbox" class="job2_parent_11360 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11562" id="sub_job2_11562"  <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11562):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11562" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11562):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">その他技術職</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[4]==1):?>checked<?php endif;?>" id="job2_parent_11361"><a href="javascript:;">技術職（建築/土木）<span>土木設計/施工管理など</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job2_parent_11361 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11563" id="sub_job2_11563" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11563):?>checked<?php endif;?><?php endforeach;?><?php endif;?> ><label for="sub_job2_11563" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11563):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">建築/土木設計</label></li>
                        <li><input type="checkbox" class="job2_parent_11361 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11564" id="sub_job2_11564" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11564):?>checked<?php endif;?><?php endforeach;?><?php endif;?> ><label for="sub_job2_11564" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11564):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">建築/土木施工管理</label></li>
                        <li><input type="checkbox" class="job2_parent_11361 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11565" id="sub_job2_11565" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11565):?>checked<?php endif;?><?php endforeach;?><?php endif;?> ><label for="sub_job2_11565" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11565):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">その他(建築・土木)</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[5]==1):?>checked<?php endif;?>" id="job2_parent_11362"><a href="javascript:;">技術職（メディカル/化学/食品）<span>医療/福祉/化成/化粧品など</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job2_parent_11362 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11566" id="sub_job2_11566" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11566):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11566" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11566):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">医療/福祉サービス</label></li>
                        <li><input type="checkbox" class="job2_parent_11362 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11567" id="sub_job2_11567" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11567):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11567" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11567):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">素材/化成品</label></li>
                        <li><input type="checkbox" class="job2_parent_11362 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11568" id="sub_job2_11568" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11568):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11568" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11568):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">医薬品/医療機器</label></li>
                        <li><input type="checkbox" class="job2_parent_11362 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11569" id="sub_job2_11569" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11569):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11569" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11569):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">化粧品</label></li>
                        <li><input type="checkbox" class="job2_parent_11362 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11570" id="sub_job2_11570" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11570):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11570" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11570):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">食品</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[7]==1):?>checked<?php endif;?>" id="job2_parent_11364"><a href="javascript:;">技術職（IT/ソフトウェア/ネットワーク）<span>SE/システム開発/インフラなど</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job2_parent_11364 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11571" id="sub_job2_11571" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11571):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11571" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11571):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">システム開発(Web/オープン系)</label></li>
                        <li><input type="checkbox" class="job2_parent_11364 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11572" id="sub_job2_11572" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11572):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11572" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11572):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">システム開発(汎用機系)</label></li>
                        <li><input type="checkbox" class="job2_parent_11364 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11573" id="sub_job2_11573" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11573):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11573" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11573):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">システム開発(組み込み/制御系)</label></li>
                        <li><input type="checkbox" class="job2_parent_11364 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11574" id="sub_job2_11574" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11574):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11574" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11574):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">プロジェクトマネージャー</label></li>
                        <li><input type="checkbox" class="job2_parent_11364 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11575" id="sub_job2_11575" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11575):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11575" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11575):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">研究開発/その他</label></li>
                        <li><input type="checkbox" class="job2_parent_11364 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11576" id="sub_job2_11576" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11576):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11576" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11576):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">データベースエンジニア</label></li>
                        <li><input type="checkbox" class="job2_parent_11364 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11577" id="sub_job2_11577" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11577):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11577" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11577):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">通信インフラ/ネットワーク</label></li>
                        <li><input type="checkbox" class="job2_parent_11364 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11578" id="sub_job2_11578" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11578):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11578" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11578):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">社内情報システム</label></li>
                        <li><input type="checkbox" class="job2_parent_11364 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11579" id="sub_job2_11579" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11579):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11579" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11579):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">テクニカルサポート</label></li>
                        <li><input type="checkbox" class="job2_parent_11364 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11580" id="sub_job2_11580" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11580):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11580" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11580):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">コンサルティング・プリセールス(IT/ネットワーク)</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[6]==1):?>checked<?php endif;?>" id="job2_parent_11363"><a href="javascript:;">専門職（コンサルタント/金融/不動産）<span>シンクタンクなど</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job2_parent_11363 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11581" id="sub_job2_11581" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11581):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11581" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11581):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">金融</label></li>
                        <li><input type="checkbox" class="job2_parent_11363 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11582" id="sub_job2_11582" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11582):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11582" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11582):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">コンサルタント/シンクタンク研究員</label></li>
                        <li><input type="checkbox" class="job2_parent_11363 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11583" id="sub_job2_11583" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11583):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11583" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11583):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">不動産/プロパティマネジメント</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[8]==1):?>checked<?php endif;?>" id="job2_parent_11365"><a href="javascript:;">クリエイティブ職<span>デザイナー/ディレクターなど</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job2_parent_11365 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11584" id="sub_job2_11584" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11584):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11584" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11584):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">Webプロデューサー/ディレクター</label></li>
                        <li><input type="checkbox" class="job2_parent_11365 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11585" id="sub_job2_11585" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11585):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11585" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11585):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">Webデザイナー/Webマスター</label></li>
                        <li><input type="checkbox" class="job2_parent_11365 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11586" id="sub_job2_11586" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11586):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11586" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11586):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">Web編集/コンテンツ企画</label></li>
                        <li><input type="checkbox" class="job2_parent_11365 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11587" id="sub_job2_11587" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11587):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11587" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11587):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">広告/メディア/ゲーム/その他</label></li>
                        <li><input type="checkbox" class="job2_parent_11365 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11588" id="sub_job2_11588" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11588):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11588" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11588):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">工業デザイン</label></li>
                        <li><input type="checkbox" class="job2_parent_11365 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11589" id="sub_job2_11589" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11589):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11589" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11589):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">店舗設計/内装</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[9]==1):?>checked<?php endif;?>" id="job2_parent_11366"><a href="javascript:;">サービス<span>小売/フード/人材など</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job2_parent_11366 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11590" id="sub_job2_11590" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11590):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11590" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11590):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">MD/バイヤー</label></li>
                        <li><input type="checkbox" class="job2_parent_11366 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11591" id="sub_job2_11591" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11591):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11591" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11591):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">店舗開発/FC開発</label></li>
                        <li><input type="checkbox" class="job2_parent_11366 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11592" id="sub_job2_11592" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11592):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11592" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11592):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">スーパーバイザ/その他サービス</label></li>
                        <li><input type="checkbox" class="job2_parent_11366 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11593" id="sub_job2_11593" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11593):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11593" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11593):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">コールセンタースーパーバイザー/カスタマーサポート</label></li>
                        <li><input type="checkbox" class="job2_parent_11366 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11594" id="sub_job2_11594" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11594):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11594" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11594):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">人材サービス/キャリアコンサルタント</label></li>
                        <li><input type="checkbox" class="job2_parent_11366 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11595" id="sub_job2_11595" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11595):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11595" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11595):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">教育/講師/インストラクター</label></li>
                        <li><input type="checkbox" class="job2_parent_11366 checkbox tantoujob2allchk tantoujob2checkbox" name="tantou_job2[]"  value="11596" id="sub_job2_11596" <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11596):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job2_11596" class="checkboxlabel tantoujob2label CheckBoxLabelClass tantoujob2allnavichecklabel <?php if(!empty(${$segment}->tantou_job2)):?><?php foreach(${$segment}->tantou_job2 as $jk=>$jv):?><?php if($jv==11596):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">店長/販売/店舗管理</label></li>
                    </ul>
</div>
        </div><!-- 会社３ --><h3 class="more"><a href="javascript:;">さらに前の勤務先2</a></h3>
        <div class="inner">
            <h4 class="<?php if(@${$segment}->company_name3):?>ok<?php else:?><?php endif;?> company_name3">会社名</h4>
            <p>
                <?php echo form_input(array('name'=>'company_name3', 'id'=>'company_name3', 'value'=> @${$segment}->company_name3, 'class'=>"txt ", 'placeholder'=>"", 'maxlength'=>25));?>
            </p>
            <table>
                <tbody>
                <tr>
                    <th class="<?php if(@${$segment}->start_year3 && @${$segment}->start_month3):?>ok<?php else:?>hissu<?php endif;?> start_year3">勤務開始日</th>
                    <th>&nbsp;</th>
                    <th>勤務終了日</th>
                </tr>
                <tr>
                    <td>
                        <select name="start_year3" id="start_year3" class="">
                            <option value="">年</option>
                            <?php for($i=$start; $i>=$end; $i--):?>
                            <option value="<?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>" <?php if(@${$segment}->start_year3==$i):?>selected<?php endif;?>><?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>年
                                <?php endfor;?>
                        </select>		<select name="start_month3" id="start_month3" class="">
                            <option value="">月</option>
                            <?php for($i=1; $i<=12; $i++):?>
                            <option value="<?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>" <?php if(@${$segment}->start_month3==$i):?>selected<?php endif;?>><?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>月
                                <?php endfor;?>
                        </select>
                    </td>
                    <td class="point">～</td>
                    <td>
                        <select name="end_year3" id="end_year3" class="">
                            <option value="">年</option>
                            <?php for($i=$start; $i>=$end; $i--):?>
                            <option value="<?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>" <?php if(@${$segment}->end_year3==$i):?>selected<?php endif;?>><?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>年
                                <?php endfor;?>
                        </select>
                        <select name="end_month3" id="end_month3" class="">
                            <option value="">月</option>
                            <?php for($i=1; $i<=12; $i++):?>
                            <option value="<?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>" <?php if(@${$segment}->end_month3==$i):?>selected<?php endif;?>><?php echo htmlspecialchars($i,ENT_QUOTES, 'UTF-8');?>月
                                <?php endfor;?>
                        </select>
                    </td>
                </tr>
            </table>
            <p id="kinmu_err3"></p>


            <h4 class="<?php if(@${$segment}->tantou_job3):?>ok<?php else:?><?php endif;?> tantou_job3">経験職種</h4>
            <?php
            $flg=array();
            $datas=array();
            if(!empty($tmptantoujob)){
                foreach($tmptantoujob as $k=>$v){
                    if(!empty($v["Items"])){
                        foreach($v["Items"] as $k2=>$v2){
                            if(!empty($v2)){
                                foreach($v2 as $k3=>$v3){

                                    $datas[$k3]["title"] = $v3["Option.P_Name"];
                                    $datas[$k3]["id"] = $v3["Option.P_Id"];
                                    if(!empty($v3["Items"])){
                                        foreach($v3["Items"] as $k4=>$v4){
                                            if(!empty($v4)){
                                                foreach($v4 as $k5=>$v5){
                                                    foreach(${$segment}->tantou_job3 as $jk=>$jv){
                                                        if($jv==$v5["Option.P_Id"]){
                                                            $flg[$k3] = 1;
                                                        }
                                                    }
                                                    $datas[$k3][$k5]["title"] = $v5["Option.P_Name"];
                                                    $datas[$k3][$k5]["id"] = $v5["Option.P_Id"];
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ?>
            <ul class="experience clearfix">
                <li class="title <?php if($flg[0]==1):?>checked<?php endif;?>" id="job3_parent_11357"><a href="javascript:;">営業<span>法人営業/個人営業/営業管理職</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job3_parent_11357 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11537" id="sub_job3_11537" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11537):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11537" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11537):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">営業(法人)</label></li>
                        <li><input type="checkbox" class="job3_parent_11357 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11538" id="sub_job3_11538"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11538):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11538" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11538):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">営業(個人)</label></li>
                        <li><input type="checkbox" class="job3_parent_11357 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11539" id="sub_job3_11539"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11539):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11539" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11539):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">代理店営業</label></li>
                        <li><input type="checkbox" class="job3_parent_11357 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11540" id="sub_job3_11540"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11540):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11540" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11540):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">海外営業</label></li>
                        <li><input type="checkbox" class="job3_parent_11357 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11541" id="sub_job3_11541"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11541):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11541" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11541):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">プリセールス・営業支援</label></li>
                        <li><input type="checkbox" class="job3_parent_11357 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11542" id="sub_job3_11542"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11542):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11542" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11542):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">その他営業</label></li>
                    </ul>
                </div>
                <li class="title <?php if($flg[1]==1):?>checked<?php endif;?>" id="job3_parent_11358"><a href="javascript:;">管理部門/事務<span>経営企画/マーケティング/広報/IR</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job3_parent_11358 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11543" id="sub_job3_11543" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11543):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11543" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11543):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">人事/総務</label></li>
                        <li><input type="checkbox" class="job3_parent_11358 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11544" id="sub_job3_11544" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11544):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11544" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11544):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">法務/特許/知財</label></li>
                        <li><input type="checkbox" class="job3_parent_11358 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11545" id="sub_job3_11545" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11545):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11545" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11545):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">経理/財務/株式公開</label></li>
                        <li><input type="checkbox" class="job3_parent_11358 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11546" id="sub_job3_11546" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11546):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11546" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11546):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">広報/IR</label></li>
                        <li><input type="checkbox" class="job3_parent_11358 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11547" id="sub_job3_11547" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11547):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11547" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11547):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">秘書/事務アシスタント/その他</label></li>
                    </ul>
                </div>
                <li class="title <?php if($flg[2]==1):?>checked<?php endif;?>" id="job3_parent_11359"><a href="javascript:;">経営幹部/企画/マーケティング<span>エグゼクティブ/営業企画など</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job3_parent_11359 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11548" id="sub_job3_11548" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11548):?>checked<?php endif;?><?php endforeach;?><?php endif;?> ><label for="sub_job3_11548" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11548):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">経営管理/エグゼクティブ/事業開発</label></li>
                        <li><input type="checkbox" class="job3_parent_11359 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11549" id="sub_job3_11549"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11549):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11549" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11549):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">マーケティング/広告宣伝/営業企画</label></li>
                        <li><input type="checkbox" class="job3_parent_11359 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11550" id="sub_job3_11550"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11550):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11550" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11550):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">その他(専門コンサルタント)</label></li>
                        <li><input type="checkbox" class="job3_parent_11359 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11551" id="sub_job3_11551"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11551):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11551" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11551):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">購買/物流</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[3]==1):?>checked<?php endif;?>" id="job3_parent_11360"><a href="javascript:;">技術職（電気/電子/機械）<span>回路/システム設計/製品企画など</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job3_parent_11360 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11552" id="sub_job3_11552"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11552):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11552" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11552):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">基礎研究/製品企画/その他</label></li>
                        <li><input type="checkbox" class="job3_parent_11360 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11553" id="sub_job3_11553"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11553):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11553" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11553):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">光学設計他</label></li>
                        <li><input type="checkbox" class="job3_parent_11360 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11554" id="sub_job3_11554"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11554):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11554" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11554):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">回路/システム設計</label></li>
                        <li><input type="checkbox" class="job3_parent_11360 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11555" id="sub_job3_11555"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11555):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11555" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11555):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">機械/機構/金型設計</label></li>
                        <li><input type="checkbox" class="job3_parent_11360 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11556" id="sub_job3_11556"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11556):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11556" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11556):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">組み込み/制御設計</label></li>
                        <li><input type="checkbox" class="job3_parent_11360 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11557" id="sub_job3_11557"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11557):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11557" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11557):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">生産管理/品質管理/品質保証</label></li>
                        <li><input type="checkbox" class="job3_parent_11360 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11558" id="sub_job3_11558"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11568):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11558" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11558):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">生産技術</label></li>
                        <li><input type="checkbox" class="job3_parent_11360 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11559" id="sub_job3_11559"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11559):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11559" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11559):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">サービスエンジニア</label></li>
                        <li><input type="checkbox" class="job3_parent_11360 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11560" id="sub_job3_11560"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11560):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11560" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11560):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">セールスエンジニア/FAE</label></li>
                        <li><input type="checkbox" class="job3_parent_11360 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11561" id="sub_job3_11561"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11561):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11561" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11561):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">プロセスエンジニア</label></li>
                        <li><input type="checkbox" class="job3_parent_11360 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11562" id="sub_job3_11562"  <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11562):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11562" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11562):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">その他技術職</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[4]==1):?>checked<?php endif;?>" id="job3_parent_11361"><a href="javascript:;">技術職（建築/土木）<span>土木設計/施工管理など</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job3_parent_11361 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11563" id="sub_job3_11563" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11563):?>checked<?php endif;?><?php endforeach;?><?php endif;?> ><label for="sub_job3_11563" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11563):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">建築/土木設計</label></li>
                        <li><input type="checkbox" class="job3_parent_11361 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11564" id="sub_job3_11564" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11564):?>checked<?php endif;?><?php endforeach;?><?php endif;?> ><label for="sub_job3_11564" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11564):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">建築/土木施工管理</label></li>
                        <li><input type="checkbox" class="job3_parent_11361 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11565" id="sub_job3_11565" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11565):?>checked<?php endif;?><?php endforeach;?><?php endif;?> ><label for="sub_job3_11565" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11565):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">その他(建築・土木)</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[5]==1):?>checked<?php endif;?>" id="job3_parent_11362"><a href="javascript:;">技術職（メディカル/化学/食品）<span>医療/福祉/化成/化粧品など</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job3_parent_11362 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11566" id="sub_job3_11566" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11566):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11566" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11566):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">医療/福祉サービス</label></li>
                        <li><input type="checkbox" class="job3_parent_11362 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11567" id="sub_job3_11567" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11567):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11567" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11567):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">素材/化成品</label></li>
                        <li><input type="checkbox" class="job3_parent_11362 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11568" id="sub_job3_11568" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11568):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11568" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11568):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">医薬品/医療機器</label></li>
                        <li><input type="checkbox" class="job3_parent_11362 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11569" id="sub_job3_11569" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11569):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11569" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11569):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">化粧品</label></li>
                        <li><input type="checkbox" class="job3_parent_11362 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11570" id="sub_job3_11570" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11570):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11570" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11570):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">食品</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[7]==1):?>checked<?php endif;?>" id="job3_parent_11364"><a href="javascript:;">技術職（IT/ソフトウェア/ネットワーク）<span>SE/システム開発/インフラなど</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job3_parent_11364 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11571" id="sub_job3_11571" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11571):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11571" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11571):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">システム開発(Web/オープン系)</label></li>
                        <li><input type="checkbox" class="job3_parent_11364 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11572" id="sub_job3_11572" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11572):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11572" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11572):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">システム開発(汎用機系)</label></li>
                        <li><input type="checkbox" class="job3_parent_11364 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11573" id="sub_job3_11573" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11573):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11573" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11573):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">システム開発(組み込み/制御系)</label></li>
                        <li><input type="checkbox" class="job3_parent_11364 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11574" id="sub_job3_11574" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11574):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11574" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11574):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">プロジェクトマネージャー</label></li>
                        <li><input type="checkbox" class="job3_parent_11364 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11575" id="sub_job3_11575" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11575):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11575" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11575):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">研究開発/その他</label></li>
                        <li><input type="checkbox" class="job3_parent_11364 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11576" id="sub_job3_11576" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11576):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11576" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11576):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">データベースエンジニア</label></li>
                        <li><input type="checkbox" class="job3_parent_11364 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11577" id="sub_job3_11577" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11577):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11577" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11577):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">通信インフラ/ネットワーク</label></li>
                        <li><input type="checkbox" class="job3_parent_11364 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11578" id="sub_job3_11578" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11578):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11578" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11578):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">社内情報システム</label></li>
                        <li><input type="checkbox" class="job3_parent_11364 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11579" id="sub_job3_11579" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11579):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11579" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11579):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">テクニカルサポート</label></li>
                        <li><input type="checkbox" class="job3_parent_11364 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11580" id="sub_job3_11580" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11580):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11580" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11580):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">コンサルティング・プリセールス(IT/ネットワーク)</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[6]==1):?>checked<?php endif;?>" id="job3_parent_11363"><a href="javascript:;">専門職（コンサルタント/金融/不動産）<span>シンクタンクなど</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job3_parent_11363 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11581" id="sub_job3_11581" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11581):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11581" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11581):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">金融</label></li>
                        <li><input type="checkbox" class="job3_parent_11363 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11582" id="sub_job3_11582" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11582):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11582" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11582):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">コンサルタント/シンクタンク研究員</label></li>
                        <li><input type="checkbox" class="job3_parent_11363 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11583" id="sub_job3_11583" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11583):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11583" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11583):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">不動産/プロパティマネジメント</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[8]==1):?>checked<?php endif;?>" id="job3_parent_11365"><a href="javascript:;">クリエイティブ職<span>デザイナー/ディレクターなど</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job3_parent_11365 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11584" id="sub_job3_11584" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11584):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11584" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11584):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">Webプロデューサー/ディレクター</label></li>
                        <li><input type="checkbox" class="job3_parent_11365 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11585" id="sub_job3_11585" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11585):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11585" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11585):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">Webデザイナー/Webマスター</label></li>
                        <li><input type="checkbox" class="job3_parent_11365 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11586" id="sub_job3_11586" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11586):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11586" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11586):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">Web編集/コンテンツ企画</label></li>
                        <li><input type="checkbox" class="job3_parent_11365 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11587" id="sub_job3_11587" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11587):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11587" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11587):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">広告/メディア/ゲーム/その他</label></li>
                        <li><input type="checkbox" class="job3_parent_11365 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11588" id="sub_job3_11588" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11588):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11588" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11588):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">工業デザイン</label></li>
                        <li><input type="checkbox" class="job3_parent_11365 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11589" id="sub_job3_11589" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11589):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11589" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11589):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">店舗設計/内装</label></li>
                    </ul>
                </div>
                <li class="title  <?php if($flg[9]==1):?>checked<?php endif;?>" id="job3_parent_11366"><a href="javascript:;">サービス<span>小売/フード/人材など</span></a></li>
                <div class="sub">
                    <ul class="clearfix">
                        <li><input type="checkbox" class="job3_parent_11366 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11590" id="sub_job3_11590" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11590):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11590" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11590):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">MD/バイヤー</label></li>
                        <li><input type="checkbox" class="job3_parent_11366 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11591" id="sub_job3_11591" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11591):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11591" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11591):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">店舗開発/FC開発</label></li>
                        <li><input type="checkbox" class="job3_parent_11366 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11592" id="sub_job3_11592" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11592):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11592" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11592):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">スーパーバイザ/その他サービス</label></li>
                        <li><input type="checkbox" class="job3_parent_11366 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11593" id="sub_job3_11593" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11593):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11593" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11593):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">コールセンタースーパーバイザー/カスタマーサポート</label></li>
                        <li><input type="checkbox" class="job3_parent_11366 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11594" id="sub_job3_11594" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11594):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11594" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11594):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">人材サービス/キャリアコンサルタント</label></li>
                        <li><input type="checkbox" class="job3_parent_11366 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11595" id="sub_job3_11595" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11595):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11595" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11595):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">教育/講師/インストラクター</label></li>
                        <li><input type="checkbox" class="job3_parent_11366 checkbox tantoujob3allchk tantoujob3checkbox" name="tantou_job3[]"  value="11596" id="sub_job3_11596" <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11596):?>checked<?php endif;?><?php endforeach;?><?php endif;?>><label for="sub_job3_11596" class="checkboxlabel tantoujob3label CheckBoxLabelClass tantoujob3allnavichecklabel <?php if(!empty(${$segment}->tantou_job3)):?><?php foreach(${$segment}->tantou_job3 as $jk=>$jv):?><?php if($jv==11596):?>checked style="display:block"<?php endif;?><?php endforeach;?><?php endif;?>">店長/販売/店舗管理</label></li>
                    </ul>
                    </div>
        </div>

        <!-- ここまで -->


        <h3 class="other">その他：ご希望等記入欄<img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->comment):?>ok<?php else:?>nini<?php endif;?>.png" width="49" height="14" alt="" class="<?php if(@${$segment}->comment):?>ok<?php else:?><?php endif;?> comment"/></h3>
        <p>里帰り支援サービスでお申し込みの方は所属会員生協名と組合員番号をご記入下さい。</p>
        <div class="inner other">
            <p><?php echo form_textarea(array('id'=>'comment', 'name'=>'comment', 'value'=>@${$segment}->comment, 'placeholder'=>'記入例：電話での面談希望など'));?></p>
        </div>


        <h3 class="other">ヒューレックスをどちらでお知りになりましたか？<img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->how):?>ok<?php else:?>nini<?php endif;?>.png" width="49" height="14" alt="" class="<?php if(@${$segment}->how):?>ok<?php else:?><?php endif;?> how"/></h3>
            <div class="inner other">
            <p>
                <label for="how01" class="radiolabel companylabel">
                    <input type="radio" class="radio compradio" name="how" value="知人の紹介" <?php echo @${$segment}->how=="知人の紹介" ? "checked" : "";?> id="how01" />
                    知人の紹介</label>
                <label for="how02" class="radiolabel companylabel">
                    <input type="radio" class="radio compradio" name="how" value="ネット検索" <?php echo @${$segment}->how=="ネット検索" ? "checked" : "";?> id="how02" />
                    ネット検索</label>
                <label for="how03" class="radiolabel companylabel">
                    <input type="radio" class="radio compradio" name="how" value="ネット広告" <?php echo @${$segment}->how=="ネット広告" ? "checked" : "";?> id="how03" />
                    ネット広告</label>
                <label for="how04" class="radiolabel companylabel">
                    <input type="radio" class="radio compradio" name="how" value="スカウトメール" <?php echo @${$segment}->how=="スカウトメール" ? "checked" : "";?> id="how04" />
                    スカウトメール</label>
                <label for="how05" class="radiolabel companylabel">
                    <input type="radio" class="radio compradio" name="how" value="仙台の転職 .com" <?php echo @${$segment}->how=="仙台の転職 .com" ? "checked" : "";?> id="how05" />
                    仙台の転職 .com</label>
                <label for="how06" class="radiolabel companylabel">
                    <input type="radio" class="radio compradio" name="how" value="SNS" <?php echo @${$segment}->how=="SNS" ? "checked" : "";?> id="how06" />
                    SNS</label>
                <label for="how07" class="radiolabel companylabel">
                    <input type="radio" class="radio compradio" name="how" value="新聞・テレビ・雑誌" <?php echo @${$segment}->how=="新聞・テレビ・雑誌" ? "checked" : "";?> id="how07" />
                    新聞・テレビ・雑誌</label>
                <label for="how08" class="radiolabel companylabel">
                    <input type="radio" class="radio compradio" name="how" value="アエルのポスター・チラシ" <?php echo @${$segment}->how=="アエルのポスター・チラシ" ? "checked" : "";?> id="how08" />
                    アエルのポスター・チラシ</label>
                <label for="how09" class="radiolabel companylabel">
                    <input type="radio" class="radio compradio" name="how" value="その他" <?php echo @${$segment}->how=="その他" ? "checked" : "";?> id="how09" />
                    その他</label>
            </p>
                </div>
    </div>
    <!-- experienceBox end -->
<!-- privacyBox start -->
    <div id="privacyBox">
        <div id="privacyBoxSub" class="inner">
            <p><a href="<?php echo base_url();?>privacy/home" target="_blank">「転職支援サービス利用規約」</a>をご覧ください</p>
            <p class="checkboxrequired">
                <label for="privacyChk" class="CheckBoxLabelClass privacy<?php if(@${$segment}->privacy==1):?> checked<?php endif;?>" id="prv"><input type="hidden" name="privacy" value="" /><input type="checkbox" class="rc checkbox privacy" name="privacy" value="1" id="privacyChk" <?php if(@${$segment}->privacy=="1"):?>checked<?php endif;?>>
                利用規約に同意する</label>
            </p>
        </div>
    </div>
    <!-- privacyBox end -->




    <input type="hidden" name="pwdchk" value="0" />
    <input type="hidden" name="hurexid" value="<?php echo $clean->purify(@$hurexid);?>" />
    <input type="hidden" name="new_flg" value="<?php echo $clean->purify(@$new_flg);?>" />
    <input type="hidden" name="param" value="<?php echo $clean->purify(@$param);?>" />
    <input type="hidden" name="mail1" value="<?php echo $clean->purify(@${$segment}->mail1);?>" />
            <div class="btnBox">
    <button class="btn" onclick="return check(1);"><span>入力内容を確認する</span></button>
            </div>
    <?php echo form_close();?>


</div>

