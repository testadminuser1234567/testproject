<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

?>
<script src="<?php echo base_url();?>js/jquery-1.6.4.min.js" charset="utf-8"></script>
<script src="<?php echo base_url();?>js/form_validation.js?j=<?php echo date('Ymdhis');?>"></script>
<h1>新規会員登録</h1>
<!-- registInputPage start -->
<div class="registInputPage">
    <p id="step"><img src="<?php echo base_url();?>images/step01.png" class="pc" alt=""/><img src="<?php echo base_url();?>images/step01_sp.png" class="sp" width="100%" alt=""/>
    </p>
    <p>ご登録からご紹介まで一切費用はかかりません。履歴書・職務経歴書を作成されていない方も、お申込み可能ですので、下記フォームにご入力の上、[入力内容を確認する]ボタンを押してください。</p>
</div>
<!-- registInputPage end -->
<?php
//パラメータ指定CV用
$split_param = str_replace("'","", $param);
$split_param = explode("|",$split_param);
?>
<div class="error"><?php echo $msg ?></div>
<?php echo form_open_multipart('signup/step2?rt=1&p='.$clean->purify($split_param[1]));?>
<input type="text" name="mail1" value="<?php echo $clean->purify(${$segment}->mail1);?>" style="display:none;" />
<!-- passwordBox start -->
<div id="passwordBox" class="form">
    <div class="inner">
        <table>
            <tbody>
            <tr>
                <th>パスワード<br>
                    <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->password):?>ok<?php else:?>hissu<?php endif;?> password"/></th>
                <td>
                    <?php echo form_password(array('name'=>"password", 'value'=>@${$segment}->password, 'class'=>"required txt", 'id'=>'password'));?>
                    <span class="txtCaution">8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</span><div id="password_err"></div>
                </td>
            </tr>
            <tr>
                <th>パスワード(確認用）<br>
                    <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->password):?>ok<?php else:?>hissu<?php endif;?> password_check"/></th>
                <td>
                    <?php echo form_password(array('name'=>"password_check", 'value'=>@${$segment}->password, 'class'=>"required txt", 'id'=>'password_check'));?>
                    <span class="txtCaution">8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</span><div id="password_check_err"></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<!-- passwordBox end -->
<!-- registInputPage start -->
<div class="registInputPage form">
    <table width="100%">
        <tr>
            <th>氏名<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->shimei):?>ok<?php else:?>hissu<?php endif;?> shimei"/></th>
            <td>
                <?php echo form_input(array('name'=>'shimei', 'id'=>'shimei', 'value'=> @${$segment}->shimei, 'class'=>"required txt", 'placeholder'=>'入力例:鈴木 太郎', 'maxlength'=>25));?>
            </td>
        </tr>
        <tr>
            <th>ふりがな<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->kana):?>ok<?php else:?>hissu<?php endif;?> kana"/></th>
            <td>
                <?php echo form_input(array('name'=>'kana', 'id'=>'kana', 'value'=> @${$segment}->kana, 'class'=>"required txt", 'placeholder'=>'入力例:すずき たろう', 'maxlength'=>25));?>
            </td>
        </tr>
        <tr>
            <th>生年月日<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->year && @${$segment}->month && @${$segment}->day):?>ok<?php else:?>hissu<?php endif;?> birth"/></th>
            <td>
                <?php
                $y= date('Y');
                $y_from = $y-18;
                //                    $y_to = $y-63;
                $y_to = 1940;
                $options_y = array(""=>"年");
                for($y=$y_from;$y>=$y_to;$y--){
                    $options_y[$y] = $y . "年";
                }
                $options_m=array(""=>"月");
                for($m=1;$m<=12;$m++){
                    $options_m[$m] = $m . "月";
                }
                $options_d=array(""=>"日");
                for($d=1;$d<=31;$d++){
                    $options_d[$d] = $d . "日";
                }
                ?>
                <?php echo form_dropdown(array('name'=>'year', 'options'=>$options_y, 'selected'=>@${$segment}->year, 'id'=>'year', 'class'=>'required'));?>
                <?php echo form_dropdown(array('name'=>'month', 'options'=>$options_m, 'selected'=>@${$segment}->month, 'id'=>'month', 'class'=>'required'));?>
                <?php echo form_dropdown(array('name'=>'day', 'options'=>$options_d, 'selected'=>@${$segment}->day, 'id'=>'day', 'class'=>'required'));?>
            </td>
        </tr>
        <tr>
            <th>性別<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->sex):?>ok<?php else:?>hissu<?php endif;?> sex"/></th>
            <td>
                <?php foreach($tmpgender['Item'] as $k=>$v):?>
                    <?php if(!empty($v['Item'])):?>
                        <?php foreach($v['Item'] as $k2=>$v2):?>
                            <label for="sex<?php echo htmlspecialchars($v2['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" class="radiolabel sexlabel">
                                <input type="radio" name="sex" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" class="radio sexradio<?php if(count($v['Item'])-1 == $k2):?> required<?php endif;?>"  <?php if(@${$segment}->sex==$v2['Option.P_Name']):?>checked<?php endif;?> id="sex<?php echo htmlspecialchars($v2['Option.P_Id'],ENT_QUOTES,'UTF-8');?>"  />
                                <?php echo htmlspecialchars($v2['Option.P_Name'], ENT_QUOTES, 'UTF-8');?>
                            </label>
                        <?php endforeach;?>
                    <?php endif;?>
                <?php endforeach;?>
                <div id="sex_err"></div>
            </td>
        </tr>
        <tr>
            <th>住所<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->pref):?>ok<?php else:?>hissu<?php endif;?> pref"/></th>
            <td>
                <?php $tflg=0;?>
                <?php if(!empty($tmppref['Item'])):?>
                    <select name="pref" id="pref" class="hissu required">
                        <option value="" selected="selected" label="">▼お選びください</option>
                        <?php foreach($tmppref['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <?php if(!empty($v2["Items"]['Item'])):?>
                                        <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                            <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                <option value="<?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->pref==$v3['Option.P_Name']):?>selected<?php endif;?>><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>

                                            <?php else:?>
                                                <?php if($tflg==0):?>
                                                    <?php $tflg=1;?>
                                                <?php endif;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    </select>
                <?php endif;?>
            </td>
        </tr>
        <tr>
            <th>電話番号<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->tel1):?>ok<?php else:?>hissu<?php endif;?> tel1"/></th>
            <td>
                <?php echo form_input(array('name'=>'tel1','type'=>'number', 'id'=>'tel1', 'value'=> @${$segment}->tel1, 'class'=>"required txt", 'placeholder'=>'入力例:08098765432', 'maxlength'=>15));?>
            </td>
        </tr>

        <tr>
            <th>最終学歴<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->school_div_id):?>ok<?php else:?>hissu<?php endif;?> school_div_id"/></th>
            <td>
                <?php if(!empty($tmpbackground['Item'])):?>
                    <?php foreach($tmpbackground['Item'] as $k=>$v):?>
                        <?php if(!empty($v['Item'])):?>
                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                <?php if($v2['Option.P_Name']!="不問"):?>
                                    <label for="gakureki0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>" class="radiolabel gakurekilabel">
                                        <input type="radio" class="radio gakurekiradio<?php if($k2==count($tmpbackground['Item'])):?> required<?php endif;?>" name="school_div_id" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->school_div_id==$v2['Option.P_Name']):?>checked<?php endif;?> id="gakureki0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>">
                                        <?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
                                    </label>
                                <?php endif;?>
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
                <div id="school_div_id_err"></div>
            </td>
        </tr>

        <tr>
            <th>学校名<br> 学部/学科<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->school_name):?>ok<?php else:?>hissu<?php endif;?> school_name"/>
            </th>
            <td>
                <?php echo form_input(array('name'=>'school_name', 'id'=>'school_name', 'value'=> @${$segment}->school_name, 'class'=>"required txt"));?>
            </td>
        </tr>

        <tr>
            <th>経験社数<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->company_number):?>ok<?php else:?>hissu<?php endif;?> company_number"/></th>
            <td>
	<span class="hissu">
<label for="keiken01" class="radiolabel companylabel">
<input type="radio" class="radio compradio" name="company_number" value="1" <?php echo @${$segment}->company_number=="1" ? "checked" : "";?> id="keiken01" />
1社</label>
<label for="keiken02" class="radiolabel companylabel">
<input type="radio" class="radio compradio" name="company_number" value="2" <?php echo @${$segment}->company_number=="2" ? "checked" : "";?> id="keiken02" />
2社</label>
<label for="keiken03" class="radiolabel companylabel">
<input type="radio" class="radio compradio" name="company_number" value="3" <?php echo @${$segment}->company_number=="3" ? "checked" : "";?> id="keiken03" />
3社</label>
<label for="keiken04" class="radiolabel companylabel">
<input type="radio" class="radio compradio" name="company_number" value="4" <?php echo @${$segment}->company_number=="4" ? "checked" : "";?> id="keiken04" />
4社</label>
<label for="keiken05" class="radiolabel companylabel">
<input type="radio" class="radio compradio required" name="company_number" value="5" <?php echo @${$segment}->company_number=="5" ? "checked" : "";?> id="keiken05" />
5社以上</label>
</span>

                <div id="company_number_err"></div>
            </td>
        </tr>
        <tr>
            <th>就業状況<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->jokyo):?>ok<?php else:?>hissu<?php endif;?> jokyo"/></th>
            <td>
                <?php if(!empty($tmpwork['Item'])):?>
                    <?php foreach($tmpwork['Item'] as $k=>$v):?>
                        <?php if(!empty($v['Item'])):?>
                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                <label for="syugyou0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>" class="radiolabel jokyolabel">
                                    <input type="radio" name="jokyo" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" class="radio jokyoradio<?php if(count($v['Item'])-1 == $k2):?> required<?php endif;?>" <?php if(@${$segment}->jokyo==$v2['Option.P_Name']):?>checked<?php endif;?> id="syugyou0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>"  />

                                    <?php print_r($v2['Option.P_Name']);?>
                                </label>
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
                <div id="jokyo_err"></div>
            </td>
        </tr>


        <tr>
            <th>転職希望日<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->tenshoku_kiboubi):?>ok<?php else:?>hissu<?php endif;?> tenshoku_kiboubi"/></th>
            <td>
	<span class="hissu">
<label for="tnk01" class="radiolabel tnsklabel">
<input type="radio" class="radio tnshkkibouradio" name="tenshoku_kiboubi" value="すぐに" <?php echo @${$segment}->tenshoku_kiboubi=="すぐに" ? "checked" : "";?> id="tnk01" />
すぐに</label>
<label for="tnk02" class="radiolabel tnsklabel">
<input type="radio" class="radio tnshkkibouradio" name="tenshoku_kiboubi" value="3ヶ月後" <?php echo @${$segment}->tenshoku_kiboubi=="3ヶ月後" ? "checked" : "";?> id="tnk02" />
3ヶ月後</label>
<label for="tnk03" class="radiolabel tnsklabel">
<input type="radio" class="radio tnshkkibouradio" name="tenshoku_kiboubi" value="半年後" <?php echo @${$segment}->tenshoku_kiboubi=="半年後" ? "checked" : "";?> id="tnk03" />
半年後</label>
<label for="tnk04" class="radiolabel tnsklabel">
<input type="radio" class="radio tnshkkibouradio" name="tenshoku_kiboubi" value="1年後" <?php echo @${$segment}->tenshoku_kiboubi=="1年後" ? "checked" : "";?> id="tnk04" />
1年後</label>
<label for="tnk05" class="radiolabel tnsklabel">
<input type="radio" class="radio tnshkkibouradio" name="tenshoku_kiboubi" value="未定（情報収集段階）" <?php echo @${$segment}->tenshoku_kiboubi=="未定（情報収集段階）" ? "checked" : "";?> id="tnk05" />
未定（情報収集段階）</label>
<label for="tnk06" class="radiolabel tnsklabel">
<input type="radio" class="radio tnshkkibouradio required" name="tenshoku_kiboubi" value="未定（良いところがあれば転職したい）" <?php echo @${$segment}->tenshoku_kiboubi=="未定（良いところがあれば転職したい）" ? "checked" : "";?> id="tnk06" />
未定（良いところがあれば転職したい）</label>

    </span>

                <div id="tenshoku_kiboubi_err"></div>
            </td>
        </tr>


        <input type="hidden" name="dummy" value=""/>


        <tr>
            <th>ご希望の参加日を<br />ご記入ください<br /><img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->kiboubi):?>ok<?php else:?>hissu<?php endif;?> kiboubi"/></th>
            <td><?php echo form_textarea(array('id'=>'kiboubi', 'name'=>'kiboubi', 'value'=>@${$segment}->kiboubi, 'class'=>"required", 'placeholder'=>'入力例：4月1日 16:00から'));?></td>
        </tr>

    </table>
    <table width="100%">

        <tr>
            <th rowspan="3"  class="<?php if(@${$segment}->kibouarea1):?>ok<?php else:?>hissu<?php endif;?> kibouarea1">希望勤務地</th>
            <td>第1希望<br>
                <img src="<?php echo base_url();?>images/ico_hissu.png" width="49" height="14" alt="" class="<?php if(@${$segment}->kibouarea1):?>ok<?php else:?>hissu<?php endif;?> kibouarea1"/>
            </td>
            <td>
                <?php $tflg=0;?>
                <?php if(!empty($tmparea['Item'])):?>
                    <select name="kibouarea1" id="kibouarea1" class="hissu required">
                        <option value="" selected="selected" label="">▼お選びください</option>
                        <?php foreach($tmparea['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <?php if(!empty($v2["Items"]['Item'])):?>
                                        <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                            <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                <option value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea1==$v3['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>

                                            <?php else:?>
                                                <?php if($tflg==0):?>
                                                    <option value="<?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea1==$v2['Items']['Item']['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                                    <?php $tflg=1;?>
                                                <?php endif;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    </select>
                <?php endif;?>
            </td>
        </tr>

        <tr>
            <td>第2希望<br>
                <img src="<?php echo base_url();?>images/ico_nini.png" width="49" height="14" alt="" class="<?php if(@${$segment}->kibouarea2):?>ok<?php else:?>nini<?php endif;?> kibouarea2"/>
            </td>
            <td>

                <?php $tflg=0;?>
                <?php if(!empty($tmparea['Item'])):?>
                    <select name="kibouarea2" id="kibouarea2" class="">
                        <option value="" selected="selected" label="">▼お選びください</option>
                        <?php foreach($tmparea['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <?php if(!empty($v2["Items"]['Item'])):?>
                                        <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                            <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                <option value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea2==$v3['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>

                                            <?php else:?>
                                                <?php if($tflg==0):?>
                                                    <option value="<?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea2==$v2['Items']['Item']['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                                    <?php $tflg=1;?>
                                                <?php endif;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    </select>
                <?php endif;?>
            </td>
        </tr>

        <tr>
            <td>第3希望<br>
                <img src="<?php echo base_url();?>images/ico_nini.png" width="49" height="14" alt="" class="<?php if(@${$segment}->kibouarea3):?>ok<?php else:?>nini<?php endif;?> kibouarea3"/>
            </td>
            <td>

                <?php $tflg=0;?>
                <?php if(!empty($tmparea['Item'])):?>
                    <select name="kibouarea3" id="kibouarea3" class="">
                        <option value="" selected="selected" label="">▼お選びください</option>
                        <?php foreach($tmparea['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <?php if(!empty($v2["Items"]['Item'])):?>
                                        <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                            <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                <option value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea3==$v3['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>

                                            <?php else:?>
                                                <?php if($tflg==0):?>
                                                    <option value="<?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea3==$v2['Items']['Item']['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                                    <?php $tflg=1;?>
                                                <?php endif;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    </select>
                <?php endif;?>
            </td>
        </tr>

    </table>
    <!-- privacyBox start -->
    <div id="privacyBox">
        <div id="privacyBoxSub" class="inner">
            <p><a href="https://www.hurex.jp/company/privacy-policy/index.html" target="_blank">個人情報保護への取り組み</a>をご覧ください</p>
            <p class="checkboxrequired">
                <label for="agreeChk" class="CheckBoxLabelClass privacy<?php if(@${$segment}->agree==1):?> checked<?php endif;?>" id="agr"><input type="hidden" name="agree" value="" /><input type="checkbox" class="rc checkbox agree" name="agree" value="1" id="agreeChk" <?php if(@${$segment}->agree=="1"):?>checked<?php endif;?>>個人情報保護方針に同意する</label>
            </p>
        </div>
    </div>
    <!-- privacyBox end -->
    <div class="btnBox">
    <button class="btn" onclick="return check(1);"><span>基本情報を登録する</span></button>
    </div>
</div>
<!-- registInputPage end -->
<input type="hidden" name="hurexid" value="<?php echo $clean->purify(${$segment}->hurexid);?>" />
<input type="hidden" name="new_flg" value="<?php echo $clean->purify(${$segment}->new_flg);?>" />
<input type="hidden" name="param" value="<?php echo $clean->purify($param);?>" />
<input type="hidden" name="hash" value="<?php echo $clean->purify(@${$segment}->hash);?>" />
<input type="hidden" name="pwdchk" value="0" />
<?php echo form_close();?>
<!-- kakuninBox start -->
<div id="kakuninBox">
    <div class="inner">
        <h2>ご登録･ご利用に際してのご確認事項</h2>
        <p>マイページにご登録、ご利用の際には「個人情報の取扱い」「利用規約/免責事項」を必ずご確認の上ご利用下さい。<br>
            尚、ご登録時に本情報についての確認が発生いたしますので必ずご確認下さいませ。</p>
        <ul class="clearfix">
            <li><a href="https://www.hurex.jp/company/privacy-policy/" target="_blank"><span>個人情報の取扱い</span></a></li>
            <li><a href="https://www.hurex.jp/company/responsibility/" target="_blank"><span>利用規約/免責事項</span></a></li>
        </ul>
    </div>
</div>
<!-- kakuninBox end -->











