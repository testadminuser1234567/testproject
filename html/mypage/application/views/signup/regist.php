<?php
$tmp_param="";
if(!empty($param)){
    $split_param = explode("|",$param);
    $tmp_param = "?p=" . $clean->purify($split_param[1]);
}
?>
<h1><img src="<?php echo base_url();?>images/ico_free.png" width="40" height="21" alt="無料"/>新規会員登録</h1>
<!-- registPage start -->
<!--<p style="margin: 20px 0;padding: 0 20px;"><a href="https://www.hurex.jp/news/detail.html?id=140" target="_blank" style="color: red">マイページ復旧のお知らせ</a></p>-->
<div id="registPage">
    <?php echo form_open('signup/send_mail/'); ?>

    <div id="mailBox">
        <p class="limited"><img src="<?php echo base_url();?>images/txt_limited.png" alt="今、会員登録すると転職コンサルタントが監修したすぐ使える職種別･職務経歴書テンプレートをプレゼント！" class="sp"/><img src="<?php echo base_url();?>images/txt_limited_pc.png" alt="今、会員登録すると転職コンサルタントが監修したすぐ使える職種別･職務経歴書テンプレートをプレゼント！" width="615" height="51" class="pc"/>
        </p>
        <p id="txtComplete"><img src="<?php echo base_url();?>images/regist_mailtitle.png" alt="1分でカンタンに登録完了!!" width="205" height="29" class="sp"/><img src="<?php echo base_url();?>images/regist_mailtitle_pc.png" alt="1分でカンタンに登録完了!!" width="267" height="43" class="pc"/>
        </p>
        <p>
            <input name="email" type="text" class="txt" id="email" placeholder="ここにメールアドレスを入力" value=""/>
        </p>        <?php if(!empty($msg)):?><div class="error"><?php echo str_replace(array("<p>","</p>"),"",$msg); ?></div><?php endif;?>
        <p class="ex">入力例：example@docomo.ne.jp</p>
    </div>
    <input type="hidden" name="parameter" value="<?php echo $clean->purify($param);?>" />
    <div class="aCenter">
    <button class="btnLogin yahooClick"><span>無料会員登録する</span></button>
    </div>
    <?php echo form_close();?>
    <script>
        $(function(){
            var tmp = "";
            $("form").submit(function(){
                $("div.error").remove();
                //メールアドレス確認用
                if($("#email").val()==""){
                    $("#email").parent().append("<div class='error'>メールアドレスを入力してください。</div>");
                    return false;
                }
                if($("#email").val() && !$("#email").val().match(/.+@.+\..+/g)){
                    $("#email").parent().append("<div class='error'>メールアドレスの形式が異なります。</div>");
                    return false;
                }
            });
        });
    </script>
    <p class="txtLogin"><a href="<?php echo base_url();?>user/login/<?php echo $tmp_param;?>" class="link">すでに会員の方はこちら→</a>
    </p>
    <p id="summary"><img src="<?php echo base_url();?>images/regist_summary.png" alt="" class="sp" width="100%"/><img src="<?php echo base_url();?>images/regist_summary_pc.png" alt="" class="pc"/>
    </p>
    <h2>リクナビNEXT、DODAなどの転職情報サービス※をご利用のお客様へ。</strong></h2>
    <p>弊社がお送りしたスカウトメールにご回答いただいたお客様は、各転職情報サービスの利用規約の下、個人情報をお預かりしております。また、お客様の手間を省くために、個人情報の一部をマイページに反映させていただいております。</p>
    <p>リクナビNEXT、DODA、エン転職、ミドルの転職、イーキャリアFA、BIZREACH、マイナビ転職エージェントサーチ、careertrek、日経キャリアNET、A-STAR、Switch</p>
</div>
<!-- registPage end -->
<!-- securityBox start -->
<div id="securityBox">
    <p><strong>HUREXは万全のセキュリティ体制</strong><br> 現在の職場やご友人など、第三者に知らせることは決してございませんので安心してご登録下さい。
        <a href="https://www.hurex.jp/company/privacy-policy/" target="_blank" class="link">プライバシーポリシー</a>
    </p>
</div>
<!-- securityBox end -->