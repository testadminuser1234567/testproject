<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

?>
<section class="co form">
    <div class="inner">
        <h1><span class="line">新規会員登録</span></h1>
        <p><img src="<?php echo base_url();?>images/step03.png" class="pc" alt=""/><img src="<?php echo base_url();?>images/step03_sp.png" class="sp" width="100%" alt=""/>
        </p>
        <p>以下の内容で送信致します。
        </p>


        <table width="100%" class="confirm">
            <tr>
                <th class="<?php if(@${$segment}->shimei):?>ok<?php else:?>hissu<?php endif;?> shimei">氏名</th>
                <td>
                    <?php echo set_value('shimei'); ?>
                </td>
            </tr>
            <tr>
                <th class="<?php if(@${$segment}->kana):?>ok<?php else:?>hissu<?php endif;?> kana">ふりがな</th>
                <td>
                    <?php echo set_value('kana'); ?>
                </td>
            </tr>
            <tr>
                <th class="<?php if(@${$segment}->year && @${$segment}->month && @${$segment}->day):?>ok<?php else:?>hissu<?php endif;?> birth">生年月日</th>
                <td>
                    <?php echo set_value('year'); ?>年<?php echo set_value('month'); ?>月<?php echo set_value('day'); ?>日
                </td>
            </tr>
            <tr>
                <th class="<?php if(@${$segment}->sex):?>ok<?php else:?>hissu<?php endif;?> sex">性別</th>
                <td>
                    <?php echo set_value('sex'); ?>
                </td>
            </tr>
            <tr>
                <th class="<?php if(@${$segment}->pref):?>ok<?php else:?>hissu<?php endif;?> pref">住所</th>
                <td><?php echo set_value('pref'); ?></td>
            </tr>
            <tr>
                <th class="<?php if(@${$segment}->tel1):?>ok<?php else:?>hissu<?php endif;?> tel1">電話</th>
                <td>
                    <?php echo set_value('tel1'); ?>
                </td>
            </tr>

            <tr>
                <th class="<?php if(@${$segment}->school_div_id):?>ok<?php else:?>hissu<?php endif;?> school_div_id">最終学歴</th>
                <td>
                    <?php echo set_value('school_div_id'); ?>
                </td>
            </tr>

            <tr>
                <th class="<?php if(@${$segment}->school_name):?>ok<?php else:?>hissu<?php endif;?> school_name">学校名/学部/学科</th>
                <td>
                    <?php echo set_value('school_name'); ?>
                </td>
            </tr>

            <tr>
                <th class="<?php if(@${$segment}->company_number):?>ok<?php else:?>hissu<?php endif;?> company_number">経験社数</th>
                <td>
                    <?php echo set_value('company_number'); ?>社
                </td>
            </tr>
            <tr>
                <th class="<?php if(@${$segment}->jokyo):?>ok<?php else:?>hissu<?php endif;?> jokyo">就業状況</th>
                <td>
                    <?php echo set_value('jokyo'); ?>
                </td>
            </tr>






            <tr>
                <th class="<?php if(@${$segment}->comment):?>ok<?php else:?>hissu<?php endif;?> comment">ご希望の参加日を<br />ご記入ください</th>
                <td><?php echo set_value('comment'); ?></td>
            </tr>
            <tr>
                <th  class="<?php if(@${$segment}->password):?>ok<?php else:?>hissu<?php endif;?> password">パスワード</th>
                <td><?php echo str_repeat('*', strlen($this->session->userdata("pass")));?></td>
            </tr>

        </table>
        <h2>希望条件</h2>
        <table width="100%" class="confirm">


            <tr>
                <th class="<?php if(@${$segment}->kiboujob):?>ok<?php else:?><?php endif;?>">職種</th>
                <td>
                    <?php if(!empty($tmpjob['Item'])):?>
                        <?php foreach($tmpjob['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <?php if(!empty($_POST["kiboujob"])):?>
                                        <?php foreach($_POST["kiboujob"] as $pk => $pv):?>
                                            <?php if($pv==$v2['Option.P_Id']):?><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>全て<br /><?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>

                                    <?php if(!empty($v2["Items"]['Item'])):?>
                                        <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                            <?php if(!empty($_POST["kiboujob"])):?>
                                                <?php foreach($_POST["kiboujob"] as $pk2 => $pv2):?>
                                                    <?php if($pv2==$v3['Option.P_Id']):?><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br /><?php endif;?>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>

                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
                </td>


            <tr>
                <th class="<?php if(@${$segment}->income):?>ok<?php else:?><?php endif;?> income">希望年収</th>
                <td><?php if(empty(${$segment}->income)):?>指定なし
                    <?php else:?>
                        <?php echo set_value('income'); ?>万円以上
                    <?php endif;?>
                </td>
            </tr>

            <tr>
                <th class="<?php if(@${$segment}->tenkin):?>ok<?php else:?><?php endif;?> tenkin">転勤</th>
                <td>
                    <?php if(!empty($_POST["tenkin"])):?>
                        <?php foreach($_POST["tenkin"] as $k => $v):?>
                            <?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?><br />
                        <?php endforeach;?>
                    <?php endif;?>
                </td>
            </tr>



            <tr>
                <th class="<?php if(@${$segment}->kibouarea):?>ok<?php else:?><?php endif;?> kibouarea">勤務地<br />
                </th>
                <td>
                    <?php if(!empty($tmparea['Item'])):?>
                        <?php foreach($tmparea['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <?php if(!empty($_POST["kibouarea"])):?>
                                        <?php foreach($_POST["kibouarea"] as $pk => $pv):?>
                                            <?php if($pv==$v2['Option.P_Id']):?><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>全て<br /><?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>

                                    <?php if(!empty($v2["Items"]['Item'])):?>
                                        <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                            <?php if(!empty($_POST["kibouarea"])):?>
                                                <?php foreach($_POST["kibouarea"] as $pk2 => $pv2):?>
                                                    <?php if($pv2==$v3['Option.P_Id']):?><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br /><?php endif;?>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>

                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endif;?>
                </td>
            </tr>
        </table>

        <?php if(${$segment}->file1 || ${$segment}->file2 || ${$segment}->file3):?>
            <h2>履歴書・職務経歴書</h2>
            <table width="100%" class="confirm">
                <?php if(!empty(${$segment}->file1)):?>
                    <tr>
                        <th class="<?php if(@${$segment}->file1):?>ok<?php else:?><?php endif;?>">履歴書</th>
                        <td><?php echo htmlspecialchars(${$segment}->file1, ENT_QUOTES, 'UTF-8'); ?></td>
                    </tr>
                <?php endif;?>
                <?php if(!empty(${$segment}->file2)):?>
                    <tr>
                        <th class="<?php if(@${$segment}->file2):?>ok<?php else:?><?php endif;?>">職務経歴書</th>
                        <td><?php echo htmlspecialchars(${$segment}->file2, ENT_QUOTES, 'UTF-8'); ?></td>
                    </tr>
                <?php endif;?>
                <?php if(!empty(${$segment}->file3)):?>
                    <tr>
                        <th class="<?php if(@${$segment}->file3):?>ok<?php else:?><?php endif;?>">その他</th>
                        <td><?php echo htmlspecialchars(${$segment}->file3, ENT_QUOTES, 'UTF-8'); ?></td>
                    </tr>
                <?php endif;?>
            </table>
        <?php endif;?>


        <!-- start -->
        <div class="confirmBtnBox clearfix">
            <div class="send">
                <?php echo form_open('signup/add/');?>
                <p class="btnSend">
                    <input type="submit" value="送信する" onclick="window.onbeforeunload=null;">
                </p>
                <?php echo form_hidden('mode', 'send'); ?>
                <?php echo form_hidden('shimei', ${$segment}->shimei); ?>
                <?php echo form_hidden('kana', ${$segment}->kana); ?>
                <?php echo form_hidden('year', ${$segment}->year); ?>
                <?php echo form_hidden('month', ${$segment}->month); ?>
                <?php echo form_hidden('day', ${$segment}->day); ?>
                <?php echo form_hidden('sex', ${$segment}->sex); ?>
                <?php echo form_hidden('pref', ${$segment}->pref); ?>
                <?php echo form_hidden('tel1', ${$segment}->tel1); ?>
                <?php echo form_hidden('agree', ${$segment}->agree); ?>
                <?php echo form_hidden('school_div_id', ${$segment}->school_div_id); ?>
                <?php echo form_hidden('school_name', ${$segment}->school_name); ?>
                <?php echo form_hidden('jokyo', ${$segment}->jokyo); ?>
                <?php echo form_hidden('kiboujob', @${$segment}->kiboujob); ?>
                <?php echo form_hidden('income', ${$segment}->income); ?>
                <?php echo form_hidden('tenkin', @${$segment}->tenkin); ?>
                <?php echo form_hidden('comment', ${$segment}->comment); ?>
                <?php echo form_hidden('company_number', ${$segment}->company_number); ?>
                <?php echo form_hidden('kibouarea', @${$segment}->kibouarea); ?>
                <?php echo form_hidden('file1', ${$segment}->file1); ?>
                <?php echo form_hidden('file2', ${$segment}->file2); ?>
                <?php echo form_hidden('file3', ${$segment}->file3); ?>
                <?php echo form_hidden('param', ${$segment}->param); ?>
                <?php echo form_hidden('hash', ${$segment}->hash); ?>
                <?php echo form_hidden('password', ${$segment}->password); ?>
                <?php echo form_hidden('password_check', ${$segment}->password_check); ?>
                <?php echo form_close();?>
            </div>
            <div class="back">
                <?php echo form_open('signup/create/');?>
                <p class="btnBack">
                    <input type="submit" value="入力画面に戻って修正する" onclick="window.onbeforeunload=null;">
                </p>
                <?php echo form_hidden('mode', 'return'); ?>
                <?php echo form_hidden('shimei', ${$segment}->shimei); ?>
                <?php echo form_hidden('kana', ${$segment}->kana); ?>
                <?php echo form_hidden('year', ${$segment}->year); ?>
                <?php echo form_hidden('month', ${$segment}->month); ?>
                <?php echo form_hidden('day', ${$segment}->day); ?>
                <?php echo form_hidden('sex', ${$segment}->sex); ?>
                <?php echo form_hidden('pref', ${$segment}->pref); ?>
                <?php echo form_hidden('tel1', ${$segment}->tel1); ?>
                <?php echo form_hidden('agree', ${$segment}->agree); ?>
                <?php echo form_hidden('school_div_id', ${$segment}->school_div_id); ?>
                <?php echo form_hidden('school_name', ${$segment}->school_name); ?>
                <?php echo form_hidden('jokyo', ${$segment}->jokyo); ?>
                <?php echo form_hidden('tenkin', @${$segment}->tenkin); ?>
                <?php echo form_hidden('income', ${$segment}->income); ?>
                <?php echo form_hidden('kiboujob', @${$segment}->kiboujob); ?>
                <?php echo form_hidden('comment', ${$segment}->comment); ?>
                <?php echo form_hidden('company_number', ${$segment}->company_number); ?>
                <?php echo form_hidden('kibouarea', @${$segment}->kibouarea); ?>
                <?php echo form_hidden('file1', ${$segment}->file1); ?>
                <?php echo form_hidden('file2', ${$segment}->file2); ?>
                <?php echo form_hidden('file3', ${$segment}->file3); ?>
                <?php echo form_hidden('param', ${$segment}->param); ?>
                <?php echo form_hidden('hash', ${$segment}->hash); ?>
                <?php echo form_hidden('password', ${$segment}->password); ?>
                <?php echo form_hidden('password_check', ${$segment}->password_check); ?>
                <?php echo form_close();?>
            </div>
        </div>
        <!-- end -->

    </div>
</section>
<!-- co end -->
<!-- co start -->
<section class="co">
    <div class="inner">
        <h1 class="ss"><span class="line">ご登録･ご利用に際してのご確認事項</span></h1>
        <p>マイページにご登録、ご利用の際には「個人情報の取扱い」「利用規約/免責事項」を必ずご確認の上ご利用下さい。<br>
            尚、ご登録時に本情報についての確認が発生いたしますので必ずご確認下さいませ。</p>
        <div class="centered">
            <ul class="btn clearfix">
                <li><a href="https://www.hurex.jp/company/privacy-policy/" target="_blank"><span>個人情報の取扱い</span></a></li>
                <li><a href="https://www.hurex.jp/company/responsibility/" target="_blank"><span>利用規約/免責事項</span></a></li>
            </ul>
        </div>
    </div>
</section>
