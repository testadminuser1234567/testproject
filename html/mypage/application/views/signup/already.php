<h1><img src="<?php echo base_url();?>images/ico_free.png" width="40" height="21" alt="無料"/>新規会員登録</h1>
<!-- registPage start -->
<div id="registPage">
    <div id="mailBox">
        <p class="txt">ご入力のメールアドレスは登録済です。<br>
            下記ボタンよりマイページにリンクします。</p>
        <div class="btn">
            <?php
            $add_param="";
            $split_param=array("","");
            if(!empty($param)){
                $split_param = str_replace("'","", $param);
                $split_param = explode("|",$split_param);
                $add_param = "?" . htmlspecialchars($split_param[0],ENT_QUOTES, 'UTF-8') . "=" .  htmlspecialchars($split_param[1],ENT_QUOTES, 'UTF-8');
            }
            ?>
            <div class="aCenter">
            <p class="btnLogin"><a href="<?php echo base_url();?>user/login/<?php echo $add_param;?>"><span>マイページにログインする</span></a>
            </p>
            </div>
        </div>
    </div>
</div>
