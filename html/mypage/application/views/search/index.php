<?php
//HRBCマスターと連動

//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);
$job_ary=array();
$sort=array();
if(!empty($tmpjob['Item'])){
    foreach($tmpjob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $job_ary as $key => $value) {
        $sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($sort, SORT_ASC, $job_ary);
?>
<script src="<?php echo base_url();?>js/jquery.tile.js"></script>
<script>
    if ( window.matchMedia( 'screen and (min-width:769px)' ).matches ) {
        $(window).on('load resize',function() {
            $('#jobList .jobBox h2').tile(2);
            $('#jobList .jobBox .tableWrap').tile(2);
        } );
    }
</script>
<h1>求人検索</h1>
<!-- co start -->
<div id="co">
    <div id="coSub" class="clearfix">
        <main id="mainCo">
            <!-- conditionsList start -->
            <div id="conditionsList" class="clearfix">
                <p class="btn"><a data-remodal-target="modal"><span>検索条件を変更</span></a></p>
                <h2>現在の検索条件</h2>
                <table>
                    <tbody>
                    <tr>
                        <th>希望勤務地：</th>
                        <td>
                            <?php $output_pref="";?>
                            <?php if(!empty($tmparea['Item'])):?>
                                <?php foreach($tmparea['Item'] as $k=>$v):?>
                                    <?php if(!empty($v['Item'])):?>
                                        <?php foreach($v['Item'] as $k2=>$v2):?>
                                            <?php if($v2['Option.P_Name']!="勤務地不問"):?>
                                                <?php if(!empty($v2["Items"]['Item'])):?>
                                                    <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                                        <?php if(!empty($v3["Option.P_Id"] )):?>
                                                            <?php if(!empty($pref)):?>
                                                                <?php foreach($pref as $jk2=>$jv2):?>
                                                                    <?php if($jv2==$v3["Option.P_Id"]):?>
                                                                        <?php $output_pref .= str_replace(array("　"," "),"",htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8')) . "、";?>
                                                                    <?php endif;?>
                                                                <?php endforeach;?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                            <?php foreach($pref as $jk2=>$jv2):?>
                                <?php if($jv2==74):?><?php $output_pref .= "海外";?><?php endif;?>
                            <?php endforeach;?>
                            <?php echo rtrim($output_pref,"、");?>
                        </td>
                    </tr>
                    <tr>
                        <th>職種：</th>
                        <td>
                            <?php $output_job="";?>
                            <?php if(!empty($job_ary)):?>
                                <?php foreach($job_ary as $k=>$v):?>
                                    <?php if(!empty($job)):?>
                                        <?php foreach($job as $jk=>$jv):?>
                                            <?php if($jv==$v["Option.P_Id"]):?>
                                                <?php $output_job .= str_replace(array("　"," "),"",htmlspecialchars($v['Option.P_Name'],ENT_QUOTES,'UTF-8')) . "、" ;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                            <?php echo rtrim($output_job,"、");?>
                        </td>
                    </tr>
                    <tr>
                        <th>希望年収：</th>
                        <td>
                            <?php if(!empty($tmpincome['Item'])):?>
                                <?php foreach($tmpincome['Item'] as $k=>$v):?>
                                    <?php if(!empty($v['Item'])):?>
                                        <?php foreach($v['Item'] as $k2=>$v2):?>
                                            <?php if($v2["Option.P_Id"]==@$year_income):?>
                                                <?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        </td>
                    </tr>
                    <tr>
                        <th>転勤：</th>
                        <td><?php echo htmlspecialchars(@$tenkin, ENT_QUOTES, 'UFT-8');?></td>
                    </tr>
                    <tr>
                        <th>フリーワード：</th>
                        <td><?php echo htmlspecialchars(@$keyword,ENT_QUOTES,'UTF-8');?><br>
                            <?php if(@$keyword_flg=="or"):?>※いずれかの文字を含む<?php endif;?><?php if(@$keyword_flg=="and"):?>すべての文字を含む<?php endif;?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- conditionsList end -->
            <!-- jobList strat -->
            <div id="jobList">
                <?php if(!empty($contents->result())):?>
                <p class="resultTxt"><span class="point"><?php echo $total;?></span>件中 <?php echo $offset;?>〜<?php echo $hani;?>件の検索結果を表示しています。</p>
                <?php if(!empty($paging)):?>
                    <div class="pagenation">
                        <?php echo $paging ?>
                    </div>
                <?php endif;?>
                <div id="jobWrapper">
                    <?php foreach ($contents->result()  as $val): ?>
                        <!-- jobBox start -->
                        <div class="jobBox">
                            <div class="inner">
                                <?php $phaze_name="";?>
                                <?php $process_id = "";?>
                                <?php $flg=0;?>
                                <?php if(!empty($processes)):?>
                                    <?php foreach($processes["Item"] as $k3 => $v3):?>
                                        <?php if($v3["Process.P_Job"]["Job"]["Job.P_Id"]==$val->job_id):?>
                                            <?php $process_id = $v3["Process.P_Id"];?>
                                            <?php $opDate = $v3["Process.P_PhaseDate"];?>
                                            <?php if(!empty($v3["Process.P_Phase"])):?>
                                                <?php foreach($v3["Process.P_Phase"] as $k4=>$v4):?>
                                                    <?php
                                                    $op = $v4["Option.P_Name"];
                                                    $opId = $v4["Option.P_Id"];
                                                    ?>

                                                    <?php if ($op == "JOB打診" || $op == "JOB打診（弱）" || $op == "JOB打診【強！】"):?>
                                                        <?php $phaze_name="オファー";?>
                                                        <?php $flg=2; //応募可?>
                                                    <?php elseif ($op == "JOB打診（社名非公開）"):?>
                                                        <?php $flg=3; //応募可 flg 3は削除されている第２ローンチ ?>
                                                    <?php elseif ($op == "JOB打診依頼" || $op == "JOB打診NG" || $op == "JOB打診予定"):?>
                                                        <?php $flg=4; //この求人の詳細を聞く?>
                                                    <?php elseif ($op == "本人NG"):?>
                                                        <?php $phaze_name="辞退済み";?>
                                                        <?php $flg=5; //やっぱり応募する?>
                                                    <?php elseif ($op == "書類NG" || $op == "面接NG"):?>
                                                        <?php $phaze_name="お見送り";?>
                                                        <?php $flg=6; ?>
                                                    <?php elseif ($op == "面接辞退" || $op == "内定辞退"):?>
                                                        <?php $phaze_name="辞退済み";?>
                                                        <?php $flg=6; ?>
                                                    <?php elseif ($op == "クローズ" || $op == "JOBクローズ"):?>
                                                        <?php $phaze_name="募集終了";?>
                                                        <?php $flg=6; ?>
                                                    <?php elseif ($op =="求人詳細問合せ" || $op=="応募承諾" || $op == "応募承諾（書類待ち）"  || $op=="書類推薦"  || $op=="書類OK"  || $op=="面接（一次）"  || $op=="面接（二次）"  || $op=="面接（三次）"  || $op=="面接（最終）" || $op=="内定" || $op=="社内NG" || $op == "JM対応用 社内NG（社名非公開）" || $op == "JM対応用 社内NG（社名非公開）" || $op == "社内NG（社名非公開）" || $op=="入社"):?>
                                                        <?php $flg=1; //問い合わせ中　このパターンのみ赤ボタンの文言変更?>
                                                        <?php if ($op == "求人詳細問合せ"):?>
                                                            <?php $phaze_name="問い合わせ中";?>
                                                        <?php elseif ($op == "応募承諾" || $op == "応募承諾（書類待ち）"):?>
                                                            <?php $phaze_name="応募中";?>
                                                        <?php elseif ($op == "書類推薦"):?>
                                                            <?php $phaze_name="応募中";?>
                                                        <?php elseif ($op == "書類OK"):?>
                                                            <?php $phaze_name="面接";?>
                                                        <?php elseif ($op == "面接（一次）"):?>
                                                            <?php $phaze_name="面接";?>
                                                        <?php elseif ($op == "面接（二次）"):?>
                                                            <?php $phaze_name="面接";?>
                                                        <?php elseif ($op == "面接（三次）"):?>
                                                            <?php $phaze_name="面接";?>
                                                        <?php elseif ($op == "面接（最終）"):?>
                                                            <?php $phaze_name="面接";?>
                                                        <?php elseif ($op == "内定"):?>
                                                            <?php $phaze_name="内定";?>
                                                        <?php elseif ($op == "社内NG" || $op == "JM対応用 社内NG（社名非公開）" || $op == "社内NG（社名非公開）"):?>
                                                            <?php $phaze_name="お見送り";?>
                                                            <?php $flg=6; ?>
                                                        <?php elseif ($op == "入社"):?>
                                                            <?php $phaze_name="内定";?>
                                                        <?php endif;?>

                                                    <?php endif;?>
                                                <?php endforeach;?>
                                            <?php endif;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>

                                <h2><?php if(!empty($phaze_name) && $phaze_name!="面接"):?>
                                        <span id="status" class="process type0<?php if($flg==2):?>1<?php elseif($flg==1):?>2<?php elseif($flg==5 || $flg==6):?>3<?php endif;?>"><?php echo $clean->purify($phaze_name);?></span>
                                    <?php elseif(!empty($phaze_name) && $phaze_name=="面接"):?>
                                        <span id="status" class="process type0<?php if($flg==2):?>1<?php elseif($flg==1):?>2<?php elseif($flg==5 || $flg==6):?>3<?php endif;?>">面接:<strong><?php if($op=="書類OK"):?>調整中<?php else:?><?php echo htmlspecialchars(date('Y/m/d', strtotime($opDate)));?><?php endif;?></span>
                                    <?php endif;?><a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($val->job_id);?>/"><?php echo str_replace('-','/',$clean->purify($val->job_title));?></a></h2>
                                <div class="tableWrap">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <th>仕事内容</th>
                                            <?php
                                            $naiyo = $val->summary;
                                            if(mb_strlen($naiyo) > 80){
                                                $naiyo = mb_substr($naiyo, 0, 80) . "...";
                                                $naiyo = $clean->purify($naiyo);
                                            }else{
                                                $naiyo = $clean->purify($naiyo);
                                            }
                                            ?>
                                            <td><?php echo $clean->purify($naiyo);?></td>
                                        </tr>
                                        <tr>
                                            <th>想定年収</th>
                                            <td><?php if(!empty($val->minsalary)):?><?php echo htmlspecialchars($val->minsalary, ENT_QUOTES, 'UTF-8');?>万円<?php if(empty($val->maxsalary)):?><?php else:?>～<?php endif;?><?php endif;?><?php if(!empty($val->maxsalary)):?><?php echo htmlspecialchars($val->maxsalary, ENT_QUOTES, 'UTF-8');?>万円&nbsp;<?php endif;?></td>
                                        </tr>
                                        <tr>
                                            <th>勤務地</th>
                                            <td>                        <?php if(!empty($val->prefecture_name)):?>
                                                    <?php $tmpp = explode(",",$val->prefecture_name);?>
                                                    <?php foreach($tmpp as $pk => $pv):?>
                                                        <?php echo htmlspecialchars($pv, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpp) > 1):?> /<?php endif;?>
                                                    <?php endforeach;?>
                                                <?php endif;?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <p class="btn"><a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($val->job_id);?>/"><span>この求人の詳細をみる</span></a></p>

                                <?php if($flg==5):?>
                                    <!--
                        <p id="offer_<?php echo $clean->purify($val->job_id);?>_<?php echo $clean->purify($val->client_id);?>"  class="more offer detail">
                        <a href="javascript:void(0);" class="offer_<?php echo $clean->purify($val->job_id);?>_<?php echo $clean->purify($val->client_id);?>">やっぱり応募する</a>
                        <input type="hidden" id="job_name" name="job_name" value="<?php echo $clean->purify($val->job_title);?>" />
                        <input type="hidden" id="job_id" name="job_id" value="<?php echo $clean->purify($val->job_id);?>" />
                        <input type="hidden" id="client_id" name="client_id" value="<?php echo $clean->purify($val->client_id);?>" />
                        <input type="hidden" id="client_name" name="client_name" value="<?php echo $clean->purify($val->c_title);?>" />
                        <input type="hidden" id="processId" name="processId" value="<?php echo $clean->purify($process_id);?>" />
                        <input type="hidden" id="phaseId" name="phaseId" value="<?php echo $clean->purify($phazeId);?>" />
                        <input type="hidden" id="phasedate" name="phasedate" value="<?php echo $clean->purify($phasedate);?>" />
                        <input type="hidden" id="phaseStatus" name="phaseStatus" value="<?php echo $clean->purify($flg);?>" />
                        </p>
-->
                                <?php endif;?>
                            </div>
                        </div>
                        <!-- jobBox end -->
                    <?php endforeach;?>
                </div>
                <p class="resultTxt"><span class="point"><?php echo $total;?></span>件中 <?php echo $offset;?>〜<?php echo $hani;?>件の検索結果を表示しています。</p>
                <?php if(!empty($paging)):?>
                    <div class="pagenation">
                        <?php echo $paging ?>
                    </div>
                <?php endif;?>
            </div>
            <?php else:?>
                <p>お探しの求人はございません</p>
            <?php endif;?>
            <!-- jobList end -->
        </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($looks)):?>
                <div id="historyBox">
                    <h2><span>最近閲覧した求人</span></h2>
                    <div class="inner">
                        <?php foreach($looks as $k=>$v):?>
                            <!-- entry start -->
                            <div class="entry">
                                <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                            </div>
                            <!-- entry end -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
            <!-- historyBox end -->
            <script>
                $(function(){
                    //お気に入り
                    $(".like").click(function() {
                        like_tmp_id = $(this).attr("id");

                        $("#" + like_tmp_id).before("<li id='loading' style='text-align:center;margin-right:0px;'><img src='<?php echo base_url();?>images/loading.gif' /></li>");
                        $(".like").hide();

                        var like_info = like_tmp_id.split( "_" );

                        var job_id = like_info[1];
                        var like_id = like_info[2];

                        // フォームの送信データをAJAXで取得する
                        var form_data = {
                            job_id: job_id,
                            like_id: like_id,
                            regist: like_id,
                            ajax: "1",
                            csrf_test_name: "<?php echo $this->security->get_csrf_hash();?>"
                        };
                        // jQueryのAJAXファンクションを利用
                        $.ajax({
                            url: "<?php echo base_url();?>search/add/",
                            type: "POST",
                            data: form_data,

                            // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                            success: function(msg){
                                if(like_id){
                                    unit = "like_" + job_id + "_";
                                    $("#" + like_tmp_id).removeClass('done');
                                    $("#" + like_tmp_id).attr('id', unit);
                                    $(".j_" + job_id + "_apply").text("この求人の詳細をきく")
                                    $(".j_" + job_id + " span").text("気になる求人リストに保存")
                                    $(".like").show();
                                    $("#loading").remove();
                                }else{
                                    ga('send', 'event', 'myPageReentry', 'CTA', 'entryButton_interest');
                                    unit = "like_" + job_id + "_" + msg;
                                    $("#"+like_tmp_id).addClass('done');
                                    $("#"+like_tmp_id).attr('id', unit);
                                    $(".j_" + job_id + "_apply").text("求人の詳細をきく")
                                    $(".j_" + job_id + " span").text("気になる求人リストに保存済")
                                    $(".like").show();
                                    $("#loading").remove();
                                }
                            },
                            error:function(XMLHttpRequest, textStatus, errorThrown) {
                                alert("お気に入り追加に失敗しました");
                            }
                        });

                        return false;
                    })

                    //JOBの詳細を聞く
                    $(".apply").click(function() {
                        if(confirm("お問い合わせいただき、ありがとうございます。求人の詳細については、確認次第、順次ご回答いたします。")) {
                            apply_tmp_id = $(this).attr("id");

                            $("#" + apply_tmp_id).before("<li id='loading' style='text-align:center;margin-right:0px;'><img src='<?php echo base_url();?>images/loading.gif' /></li>");
                            $(".apply").hide();
                            $(".like").hide();

                            var apply_info = apply_tmp_id.split( "_" );

                            job_id = apply_info[1];
                            client_id = apply_info[2];

                            // フォームの送信データをAJAXで取得する
                            var form_data = {
                                job_id: job_id,
                                client_id: client_id,
                                ajax: "1",
                                csrf_test_name: "<?php echo $this->security->get_csrf_hash();?>"
                            };
                            // jQueryのAJAXファンクションを利用
                            $.ajax({
                                url: "<?php echo base_url();?>search/apply/",
                                type: "POST",
                                data: form_data,

                                // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                                success: function(msg){
                                    if(msg!=0){
                                        alert("エントリーに失敗しました(apply)"+msg);
                                    }else{
                                        ga('send', 'event', 'myPageReentry', 'CTA', 'entryButton_ok');
                                        $("a." + apply_tmp_id).hide();
                                        $("#" + apply_tmp_id).before('<li class="detail applyDone"><span class="apply_' + job_id + '_' + client_id + '">応募中</span></li>');
                                        $("#" + apply_tmp_id).addClass("applyDone");
                                        $("#" + apply_tmp_id).remove();
                                        $(".j_" + job_id).hide();
                                        $("#loading").remove();
                                    }
                                },
                                error:function(XMLHttpRequest, textStatus, errorThrown) {
                                    alert("エントリーに失敗しました code:101");
                                }
                            });
                        }
                        return false;
                    })


                    //オファーの応募
                    $(".offer").click(function() {
                        if(confirm("ご応募いただき、ありがとうございます。求人のご応募について、確認次第、順次ご回答いたします。")) {
                            offer_tmp_id = $(this).attr("id");

                            $("#" + offer_tmp_id).before("<li id='loading' style='text-align:center;margin-right:0px;'><img src='<?php echo base_url();?>images/loading.gif' /></li>");
                            $(".offer").hide();
                            $(".refusal").hide();

                            job_id = $("#job_id").val();
                            client_id = $("#client").val();
                            job_name = $("#job_name").val();
                            client_name = $("#client_name").val();
                            processId = $("#processId").val();
                            phaseId = $("#phaseId").val();
                            phasedate = $("#phasedate").val();

                            // フォームの送信データをAJAXで取得する
                            var form_data = {
                                job_id: job_id,
                                client_id: client_id,
                                job_name: job_name,
                                client_name: client_name,
                                processId: processId,
                                phaseId: phaseId,
                                phasedate: phasedate,
                                ajax: "1",
                                csrf_test_name: "<?php echo $this->security->get_csrf_hash();?>"
                            };
                            // jQueryのAJAXファンクションを利用
                            $.ajax({
                                url: "<?php echo base_url();?>search/offer/",
                                type: "POST",
                                data: form_data,

                                // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                                success: function(msg){
                                    if(msg!=0){
                                        alert("エントリーに失敗しました(offer)"+msg);
                                    }else{
                                        ga('send', 'event', 'myPageApply', 'CTA', 'entryButton_Apply');
                                        $("a." + offer_tmp_id).hide();
                                        $(".refusal").hide();
                                        $("#" + offer_tmp_id).addClass("offerDone");
                                        $("#" + offer_tmp_id).remove();
                                        $("#status").removeClass("type03");
                                        $("#status").text("応募中");
                                        $("#status").addClass("type02");
                                        $(".j_" + job_id).hide();
                                        $("#loading").remove();
                                    }
                                },
                                error:function(XMLHttpRequest, textStatus, errorThrown) {
                                    alert("エントリーに失敗しました code:101");
                                }
                            });
                        }
                        return false;
                    })

                    //オファーの辞退
                    $(".refusal").click(function() {
                        if(confirm("応募を辞退しますか？")) {
                            refusal_tmp_id = $(this).attr("id");

                            $("#" + refusal_tmp_id).before("<li id='loading' style='text-align:center;margin-right:0px;'><img src='<?php echo base_url();?>images/loading.gif' /></li>");
                            $(".offer").hide();
                            $(".refusal").hide();

                            job_id = $("#job_id").val();
                            client_id = $("#client").val();
                            job_name = $("#job_name").val();
                            client_name = $("#client_name").val();
                            processId = $("#processId").val();
                            phaseId = $("#phaseId").val();
                            phasedate = $("#phasedate").val();
                            phaseStatus = $("#phaseStatus").val();

                            // フォームの送信データをAJAXで取得する
                            var form_data = {
                                job_id: job_id,
                                client_id: client_id,
                                job_name: job_name,
                                client_name: client_name,
                                processId: processId,
                                phaseId: phaseId,
                                phasedate: phasedate,
                                phaseStatus: phaseStatus,
                                ajax: "1",
                                csrf_test_name: "<?php echo $this->security->get_csrf_hash();?>"
                            };
                            // jQueryのAJAXファンクションを利用
                            $.ajax({
                                url: "<?php echo base_url();?>search/refusal/",
                                type: "POST",
                                data: form_data,

                                // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                                success: function(msg){
                                    if(msg!=0){
                                        alert("エントリーに失敗しました"+msg);
                                    }else{
                                        ga('send', 'event', 'myPageApply', 'CTA', 'entryButton_notApply');
                                        $("a." + refusal_tmp_id).hide();
                                        $(".offer").hide();
                                        $("#" + refusal_tmp_id).before('<li class="detail applyDone"><span class="apply_' + job_id + '_' + client_id + '">辞退済み</span></li>');
                                        $("#" + refusal_tmp_id).addClass("refusalDone");
                                        $("#" + refusal_tmp_id).remove();
                                        $(".j_" + job_id).hide();
                                        $("#loading").remove();
                                    }
                                },
                                error:function(XMLHttpRequest, textStatus, errorThrown) {
                                    alert("エントリーに失敗しました code:101");
                                }
                            });
                        }
                        return false;
                    })

                    //この求人の詳細をきく
                    $(".contact").click(function() {
                        if(confirm("お問い合わせいただき、ありがとうございます。求人の詳細については、確認次第、順次ご回答いたします。")) {
                            contact_tmp_id = $(this).attr("id");

                            $("#" + contact_tmp_id).before("<li id='loading' style='text-align:center;margin-right:0px;'><img src='<?php echo base_url();?>images/loading.gif' /></li>");
                            $(".contact").hide();
                            $(".like").hide();

                            job_id = $("#job_id").val();
                            client_id = $("#client").val();
                            job_name = $("#job_name").val();
                            client_name = $("#client_name").val();
                            processId = $("#processId").val();
                            phaseId = $("#phaseId").val();
                            phasedate = $("#phasedate").val();

                            // フォームの送信データをAJAXで取得する
                            var form_data = {
                                job_id: job_id,
                                client_id: client_id,
                                job_name: job_name,
                                client_name: client_name,
                                processId: processId,
                                phaseId: phaseId,
                                phasedate: phasedate,
                                ajax: "1",
                                csrf_test_name: "<?php echo $this->security->get_csrf_hash();?>"
                            };
                            // jQueryのAJAXファンクションを利用
                            $.ajax({
                                url: "<?php echo base_url();?>search/contact/",
                                type: "POST",
                                data: form_data,

                                // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                                success: function(msg){
                                    if(msg!=0){
                                        alert("処理に失敗しました(contact)"+msg);
                                    }else{
                                        ga('send', 'event', 'myPageContact', 'CTA', 'entryButton_Contact');
                                        $("a." + contact_tmp_id).hide();
                                        $(".like").show();
                                        $("#" + contact_tmp_id).before('<li class="detail applyDone"><span class="apply_' + job_id + '_' + client_id + '">問い合わせ中</span></li>');
                                        $("#" + contact_tmp_id).addClass("contactDone");
                                        $("#" + contact_tmp_id).remove();
                                        $(".j_" + job_id).hide();
                                        $("#loading").remove();
                                    }
                                },
                                error:function(XMLHttpRequest, textStatus, errorThrown) {
                                    alert("エントリーに失敗しました code:101");
                                }
                            });
                        }
                        return false;
                    })
                })
            </script>