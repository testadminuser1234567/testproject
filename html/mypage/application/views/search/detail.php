<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-6405327-12"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-6405327-12');


</script>
<!-- box start -->
<?php $flg=0;?>
<?php $phaze_name="";?>
<?php $company_name_flg = 0;?>
<?php $add_flg = 0;?>
<?php if(!empty($processes)):?>
    <?php foreach ($processes as $v): ?>
        <?php if($v["Process.P_Job"]["Job"]["Job.P_Id"]==@$contents->job_id):?>
            <?php $processId = $v["Process.P_Id"]?>
            <?php $phasedate = date("Y-m-d H:i:s", strtotime("+9 hour", strtotime($v["Process.P_PhaseDate"])));;?>
            <?php $op="";?>
            <?php if(!empty($v["Process.P_Phase"])):?>
                <?php foreach($v["Process.P_Phase"] as $k2 => $v2):?>
                    <?php $phazeId = $v2["Option.P_Id"];?>
                    <?php $op = $v2["Option.P_Name"];?>
                <?php endforeach;?>
            <?php endif;?>
            <?php if ($op == "JOB打診" || $op == "JOB打診（弱）" || $op == "JOB打診【強！】"):?>
                <?php $phaze_name="オファー";?>
                <?php $flg=2; //応募可?>
            <?php elseif ($op == "JOB打診（社名非公開）"):?>
                <?php $flg=3; //応募可 flg 3は削除されている第２ローンチ ?>
            <?php elseif ($op == "JOB打診依頼" || $op == "JOB打診NG" || $op == "JOB打診予定"):?>
                <?php $flg=4; //この求人の詳細を聞く?>
            <?php elseif ($op == "本人NG"):?>
                <?php $phaze_name="辞退済み";?>
                <?php $flg=5; //やっぱり応募する?>
            <?php elseif ($op == "書類NG" || $op == "面接NG"):?>
                <?php $phaze_name="お見送り";?>
                <?php $flg=6; ?>
            <?php elseif ($op == "面接辞退" || $op == "内定辞退"):?>
                <?php $phaze_name="辞退済み";?>
                <?php $flg=6; ?>
            <?php elseif ($op == "クローズ" || $op == "JOBクローズ" || $op == "期限切れ処理"):?>
                <?php $phaze_name="募集終了";?>
                <?php $flg=6; ?>
            <?php elseif ($op =="求人詳細問合せ" || $op=="応募承諾" || $op == "応募承諾（書類待ち）"  || $op=="書類推薦"  || $op=="書類OK"  || $op=="面接（一次）"  || $op=="面接（二次）"  || $op=="面接（三次）"  || $op=="面接（最終）" || $op=="内定" || $op=="社内NG" || $op == "社内NG（社名非公開）" || $op == "JM対応用 社内NG（社名非公開）" || $op=="入社"):?>
                <?php $flg=1; //問い合わせ中　このパターンのみ赤ボタンの文言変更?>
                <?php if ($op == "求人詳細問合せ"):?>
                    <?php $phaze_name="問い合わせ中";?>
                <?php elseif ($op == "応募承諾" || $op == "応募承諾（書類待ち）"):?>
                    <?php $phaze_name="応募中";?>
                <?php elseif ($op == "書類推薦"):?>
                    <?php $phaze_name="応募中";?>
                <?php elseif ($op == "書類OK"):?>
                    <?php $phaze_name="面接";?>
                <?php elseif ($op == "面接（一次）"):?>
                    <?php $phaze_name="面接";?>
                <?php elseif ($op == "面接（二次）"):?>
                    <?php $phaze_name="面接";?>
                <?php elseif ($op == "面接（三次）"):?>
                    <?php $phaze_name="面接";?>
                <?php elseif ($op == "面接（最終）"):?>
                    <?php $phaze_name="面接";?>
                <?php elseif ($op == "内定"):?>
                    <?php $phaze_name="内定";?>
                <?php elseif ($op == "社内NG" || $op == "JM対応用 社内NG（社名非公開）" || $op == "社内NG（社名非公開）"):?>
                    <?php $phaze_name="お見送り";?>
                    <?php $flg=6; ?>
                <?php elseif ($op == "入社"):?>
                    <?php $phaze_name="内定";?>
                <?php endif;?>

            <?php endif;?>

            <?php if ($op == "興味あり" || $op == "求人詳細問合せ" || $op == "WEBエントリー" || $op == "JOB打診依頼" || $op == "JOB打診（社名非公開）" || $op == "本人NG（社名非公開）" || $op == "社内NG予定（社名非公開）" || $op == "社内NG（社名非公開）" || $op == "JOBクローズ（社名非公開）" || $op == "期限切れ処理（社名非公開）" || $op == "JOB打診予定" || $op == "JOB打診NG" || $op == "JM対応用 社内NG（社名非公開）"):?>
            <?php else:?>
                <?php $company_name_flg=1; //会社名公開?>
            <?php endif;?>

            <?php if ($op == "興味あり" || $op == "求人詳細問合せ" || $op == "WEBエントリー" || $op == "JOB打診依頼" || $op == "JOB打診（社名非公開）" || $op == "本人NG（社名非公開）" || $op == "社内NG予定（社名非公開）" || $op == "社内NG（社名非公開）" || $op == "JM対応用 社内NG（社名非公開）"):?>
            <?php else:?>
                <?php $add_flg=1; //会社概要公開?>
            <?php endif;?>

        <?php endif;?>
    <?php endforeach;?>
<?php endif;?>

<!-- co start -->
<?php if(!empty($flg) && !empty($phaze_name)):?><p id="status" class="entryTxt type0<?php if($flg==2):?>1<?php elseif($flg==1):?>2<?php elseif($flg==5 || $flg==6):?>3<?php endif;?>">この求人は現在【<?php echo htmlspecialchars($phaze_name, ENT_QUOTES, 'UTF-8');?>】です</p><?php endif;?>
<div id="co" <?php if(!empty($flg)):?>class="info"<?php endif;?>>
    <div id="coSub" class="clearfix">
        <main id="mainCo">
            <!-- jibDetail start -->
            <div id="jobDetail">
                <div class="inner">
                    <?php if(empty($contents)):?>
                        <p>お探しの求人情報はございません。</p>
                    <?php else:?>
                    <!-- 求人詳細 -->
                    <h1><span class="jobID">求人詳細情報 [JOB<?php echo htmlspecialchars($contents->job_id, ENT_QUOTES, 'UTF-8');?>]</span><br>
                        <?php echo htmlspecialchars($contents->job_title, ENT_QUOTES, 'UTF-8');?></h1>
<div class="error"><?php echo $msg ?></div>
                    <table class="detail">
                        <?php if($company_name_flg == 1):?>
                            <tr>
                                <th>企業名</th>
                                <td><?php echo htmlspecialchars($contents->c_title, ENT_QUOTES, 'UTF-8');?>
                                </td>
                            </tr>
                        <?php else:?>
                            <tr>
                                <th>企業名</th>
                                <td>******
                                </td>
                            </tr>
                        <?php endif;?>

                        <tr>
                            <th>募集背景</th>
                            <td><?php echo nl2br(htmlspecialchars($contents->background, ENT_QUOTES, 'UTF-8'));?></td>
                        </tr>
                        <tr>
                            <th>仕事内容</th>
                            <td><?php echo nl2br(htmlspecialchars($contents->summary, ENT_QUOTES, 'UTF-8'));?></td>
                        </tr>
                        <tr>
                            <th>応募資格</th>
                            <td><?php echo nl2br(htmlspecialchars($contents->qualification, ENT_QUOTES, 'UTF-8'));?>
                            </td>
                        </tr>

                        <tr>
                            <th>雇用形態</th>
                            <td>
                                <?php if(!empty($contents->employ_name)):?>
                                    <?php $tmpe = explode(",",$contents->employ_name);?>
                                    <?php foreach($tmpe as $ek => $ev):?>
                                        <?php echo htmlspecialchars($ev, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpe) > 1):?><br /><?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <th>想定年収</th>
                            <td><?php if(!empty($contents->minsalary)):?><?php echo htmlspecialchars($contents->minsalary, ENT_QUOTES, 'UTF-8');?>万円<?php if(empty($contents->maxsalary)):?><?php else:?>～<?php endif;?><?php endif;?><?php if(!empty($contents->maxsalary)):?><?php echo htmlspecialchars($contents->maxsalary, ENT_QUOTES, 'UTF-8');?>万円&nbsp;<?php endif;?></td>
                        </tr>


                        <?php if($add_flg==1):?>
                            <tr>
                                <th>給与詳細</th>
                                <td><?php echo nl2br(htmlspecialchars($contents->salary, ENT_QUOTES, 'UTF-8'));?></td>
                            </tr>
                        <?php endif;?>

                        <tr>
                            <th>勤務地</th>
                            <td>
                                <?php if(!empty($contents->prefecture_name)):?>
                                    <?php $tmpp = explode(",",$contents->prefecture_name);?>
                                    <?php foreach($tmpp as $pk => $pv):?>
                                        <?php echo htmlspecialchars($pv, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpp) > 1):?> / <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <th>勤務地(詳細)</th>
                            <td>
                                <?php echo nl2br(htmlspecialchars($contents->area_detail, ENT_QUOTES, 'UTF-8'));?>
                            </td>
                        </tr>

                        <tr>
                            <th>諸手当</th>
                            <td><?php echo nl2br(htmlspecialchars($contents->benefits, ENT_QUOTES, 'UTF-8'));?></td>
                        </tr>
                        <tr>
                            <th>休日休暇</th>
                            <td><?php echo nl2br(htmlspecialchars($contents->holiday, ENT_QUOTES, 'UTF-8'));?></td>
                        </tr>
                        <tr>
                            <th>勤務時間</th>
                            <td><?php echo nl2br(htmlspecialchars($contents->worktime, ENT_QUOTES, 'UTF-8'));?></td>
                        </tr>

                    </table>
                    <table class="summary">
                        <tbody>
                        <tr>
                            <th colspan="2" class="title">会社概要</th>
                        </tr>
                        <tr>

                            <?php if($add_flg == 1):?>
                            <?php if($company_name_flg == 1):?>
                        <tr>
                            <th>企業名</th>
                            <td><?php echo htmlspecialchars($contents->c_title, ENT_QUOTES, 'UTF-8');?>
                            </td>
                        </tr>
                        <?php if(!empty($contents->c_url)):?>
                            <tr>
                                <th>URL</th>
                                <td>
                                    <a href="<?php echo nl2br(htmlspecialchars($contents->c_url, ENT_QUOTES, 'UTF-8'));?>" target="_blank" style="color:#0000ff;text-decoration:underline;"><?php echo nl2br(htmlspecialchars($contents->c_url, ENT_QUOTES, 'UTF-8'));?></a>
                                </td>
                            </tr>
                        <?php endif;?>
                        <?php endif;?>
                        <tr>
                            <th>設立日</th>
                            <td><?php echo nl2br(htmlspecialchars($contents->c_establish, ENT_QUOTES, 'UTF-8'));?></td>
                        </tr>
                        <tr>
                            <th>資本金</th>
                            <td><?php echo nl2br(htmlspecialchars($contents->c_capital, ENT_QUOTES, 'UTF-8'));?></td>
                        </tr>
                        <tr>
                            <th>従業員数</th>
                            <td><?php echo nl2br(htmlspecialchars($contents->c_employment, ENT_QUOTES, 'UTF-8'));?></td>
                        </tr>
                        <tr>
                            <th>本社住所</th>
                            <td><?php echo nl2br(htmlspecialchars($contents->c_street, ENT_QUOTES, 'UTF-8'));?></td>
                        </tr>
                        <tr>
                            <th>会社の特徴</th>
                            <td><?php echo nl2br(htmlspecialchars($contents->c_jigyo, ENT_QUOTES, 'UTF-8'));?></td>
                        </tr>
                        <?php else:?>
                            <tr>
                                <th>業種</th>
                                <td>
                                    <?php if(!empty($contents->i_name)):?>
                                        <?php $tmpp = explode(",",$contents->i_name);?>
                                        <?php foreach($tmpp as $pk => $pv):?>
                                            <?php echo htmlspecialchars($pv, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpp) > 1):?><br /><?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                </td>
                            </tr>
                            <tr>
                                <th>事業内容</th>
                                <td><?php echo nl2br(htmlspecialchars($contents->c_naiyo, ENT_QUOTES, 'UTF-8'));?></td>
                            </tr>

                            <tr>
                                <th>従業員数</th>
                                <td><?php echo nl2br(htmlspecialchars($contents->c_employment, ENT_QUOTES, 'UTF-8'));?></td>
                            </tr>
                        <?php endif;?>

                    </table>

                    <!-- btnBox start -->
                    <div class="jobBtnBox" id="<?php if($flg!=1 && $flg!=6):?>jobApplyBtn<?php endif;?>">
                        <ul class="clearfix">
                            <?php if($flg==0):?>
                                <li id="apply_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($contents->client_id);?>"  class="more apply detail">
                                    <a href="javascript:void(0);" class="j_<?php echo $clean->purify($contents->job_id);?>_apply apply_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($contents->client_id);?>"><span><?php if(isset($likes[$contents->job_id])):?>求人の詳細をきく<?php else:?>求人の詳細をきく<?php endif;?></span></a>
                                </li>
                            <?php endif;?>
                            <?php if($flg==1):?>
                                <?php if(!empty($flg)):?><p class="entryTxt type0<?php if($flg==2):?>1<?php elseif($flg==1):?>2<?php elseif($flg==5 || $flg==6):?>3<?php endif;?>">この求人は現在【<?php echo htmlspecialchars($phaze_name, ENT_QUOTES, 'UTF-8');?>】です</p><?php endif;?>
                                <!--
                    <li class="detail applyDone">
                        <span class="apply_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($contents->client_id);?>"><?php echo $clean->purify($phaze_name);?></span>
                    </li>
-->
                            <?php endif;?>

                            <?php if($flg==2 || $flg==3):?>
                                <li id="offer_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($contents->client_id);?>"  class="more offer detail">
                                    <a href="javascript:void(0);" class="offer_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($contents->client_id);?>"><span>応募する</span></a>
                                    <input type="hidden" id="job_name" name="job_name" value="<?php echo $clean->purify($contents->job_title);?>" />
                                    <input type="hidden" id="job_id" name="job_id" value="<?php echo $clean->purify($contents->job_id);?>" />
                                    <input type="hidden" id="client_id" name="client_id" value="<?php echo $clean->purify($contents->c_industry_id);?>" />
                                    <input type="hidden" id="client_name" name="client_name" value="<?php echo $clean->purify($contents->c_title);?>" />
                                    <input type="hidden" id="processId" name="processId" value="<?php echo $clean->purify($processId);?>" />
                                    <input type="hidden" id="phaseId" name="phaseId" value="<?php echo $clean->purify($phazeId);?>" />
                                    <input type="hidden" id="phasedate" name="phasedate" value="<?php echo $clean->purify($phasedate);?>" />
                                    <input type="hidden" id="phaseStatus" name="phaseStatus" value="<?php echo $clean->purify($flg);?>" />
                                </li>
                                <li id="refusal_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($contents->client_id);?>"  class="jitai refusal detail">
                                    <a href="javascript:void(0);" class="refusal_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($contents->client_id);?>"><span>辞退する</span></a>
                                    <?php echo form_open("search/refusal", array('name' => 'refuseform', 'id' => 'refuseform'));?>
                                    <input type="hidden" id="job_name" name="job_name" value="<?php echo $clean->purify($contents->job_title);?>" />
                                    <input type="hidden" id="job_id" name="job_id" value="<?php echo $clean->purify($contents->job_id);?>" />
                                    <input type="hidden" id="client_id" name="client_id" value="<?php echo $clean->purify($contents->c_industry_id);?>" />
                                    <input type="hidden" id="client_name" name="client_name" value="<?php echo $clean->purify($contents->c_title);?>" />
                                    <input type="hidden" id="processId" name="processId" value="<?php echo $clean->purify($processId);?>" />
                                    <input type="hidden" id="phaseId" name="phaseId" value="<?php echo $clean->purify($phazeId);?>" />
                                    <input type="hidden" id="phasedate" name="phasedate" value="<?php echo $clean->purify($phasedate);?>" />
                                    <input type="hidden" id="phaseStatus" name="phaseStatus" value="<?php echo $clean->purify($flg);?>" />
				<?php echo form_hidden($csrf); ?>
                                    <?php echo form_close();?>
                                </li>
                            <?php endif;?>

                            <?php if($flg==4):?>
                                <li id="contact_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($contents->client_id);?>"  class="more contact detail">
                                    <a href="javascript:void(0);" class="contact_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($contents->client_id);?>"><span>求人の詳細をきく</span></a>
                                    <input type="hidden" id="job_name" name="job_name" value="<?php echo $clean->purify($contents->job_title);?>" />
                                    <input type="hidden" id="job_id" name="job_id" value="<?php echo $clean->purify($contents->job_id);?>" />
                                    <input type="hidden" id="client_id" name="client_id" value="<?php echo $clean->purify($contents->c_industry_id);?>" />
                                    <input type="hidden" id="client_name" name="client_name" value="<?php echo $clean->purify($contents->c_title);?>" />
                                    <input type="hidden" id="processId" name="processId" value="<?php echo $clean->purify($processId);?>" />
                                    <input type="hidden" id="phaseId" name="phaseId" value="<?php echo $clean->purify($phazeId);?>" />
                                    <input type="hidden" id="phasedate" name="phasedate" value="<?php echo $clean->purify($phasedate);?>" />
                                    <input type="hidden" id="phaseStatus" name="phaseStatus" value="<?php echo $clean->purify($flg);?>" />
                                </li>
                            <?php endif;?>

                            <?php if($flg==5):?>
                                <li id="offer_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($contents->client_id);?>"  class="more offer detail">
                                    <a href="javascript:void(0);" class="offer_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($contents->client_id);?>"><span>やっぱり応募する</span></a>
                                    <input type="hidden" id="job_name" name="job_name" value="<?php echo $clean->purify($contents->job_title);?>" />
                                    <input type="hidden" id="job_id" name="job_id" value="<?php echo $clean->purify($contents->job_id);?>" />
                                    <input type="hidden" id="client_id" name="client_id" value="<?php echo $clean->purify($contents->c_industry_id);?>" />
                                    <input type="hidden" id="client_name" name="client_name" value="<?php echo $clean->purify($contents->c_title);?>" />
                                    <input type="hidden" id="processId" name="processId" value="<?php echo $clean->purify($processId);?>" />
                                    <input type="hidden" id="phaseId" name="phaseId" value="<?php echo $clean->purify($phazeId);?>" />
                                    <input type="hidden" id="phasedate" name="phasedate" value="<?php echo $clean->purify($phasedate);?>" />
                                    <input type="hidden" id="phaseStatus" name="phaseStatus" value="<?php echo $clean->purify($flg);?>" />
                                </li>
                            <?php endif;?>

                            <?php if($flg==6):?>
                                <?php if(!empty($flg)):?><p class="entryTxt type0<?php if($flg==2):?>1<?php elseif($flg==1):?>2<?php elseif($flg==5 || $flg==6):?>3<?php endif;?>">この求人は現在【<?php echo htmlspecialchars($phaze_name, ENT_QUOTES, 'UTF-8');?>】です</p><?php endif;?>
                                <!--
                    <li class="detail applyDone">
                        <span class="apply_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($contents->client_id);?>"><?php echo $clean->purify($phaze_name);?></span>
                    </li>
-->
                            <?php endif;?>

                            <?php if(isset($likes[$contents->job_id])){
                                $likeid = $likes[$contents->job_id];
                            }else{
                                $likeid = "";
                            }
                            ?>
                            <li id="like_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($likeid);?>" class="<?php if(!isset($likes[$contents->job_id])):?>list<?php endif;?> j_<?php echo $clean->purify($contents->job_id);?> preserve like<?php if($likeid):?> done add<?php endif;?>" <?php if($flg==1 || $flg==2 || $flg==3 || $flg==6):?>style="display:none;"<?php endif;?>>
                                <a href="javascript:void(0);"><span class="like_<?php echo $clean->purify($contents->job_id);?>_<?php echo $clean->purify($likeid);?>">気になる求人<?php if(isset($likes[$contents->job_id])):?>リストに<br /><strong>追加済み</strong><?php else:?><br />リストへ追加<?php endif;?></span></a>
                            </li>
                        </ul>
                    </div>
                    <script>
                        var setBoxId = '#jobApplyBtn';      // スクロールさせる要素
                        var initOffsetTop = null;   // 要素の初期位置

                        $(window).load(function() {


                            var applyBtn = $("#jobApplyBtn").height();
                            $("#jobApplyBtn").clone(true).attr('id', 'temp1').insertAfter('#jobApplyBtn');
                            $(".apply").eq(1).attr('id', 'temp2');
                            $(".like").eq(1).attr('id','temp3');

                            thisOffset = $('#jobApplyBtn').offset().top + $('#jobApplyBtn').outerHeight();
                            $(setBoxId).addClass('fixed');

                            $(window).scroll(function(){
                                if( $(window).scrollTop() + $(window).height() + 110 > thisOffset){
                                    // ボタンの位置を超えた場合
                                    $("#temp1").hide();
                                    $(setBoxId).removeClass("fixed");
                                } else {
                                    $("#temp1").show();
                                    // ボタンの位置を超えていない
                                    $(setBoxId).addClass('fixed');
                                }
                            });

                            /*
                             var applyBtn = $("#jobApplyBtn").height();
                             $("#jobApplyBtn").clone(true).attr('id', 'temp1').insertAfter('#jobApplyBtn');
                             $(".apply").eq(1).attr('id', 'temp2');
                             $(".like").eq(1).attr('id','temp3');

                             // 初期位置取得
                             initOffsetTop = $(setBoxId).offset().top;
                             $(setBoxId).css('position', 'fixed');
                             $(setBoxId).css('bottom', '0');
                             //        $(setBoxId).css('width', '35%');

                             //スクロールしたらこの処理が走る
                             $(window).scroll(function() {
                             scrollbox();
                             });

                             // スクロール処理
                             var now_top = 0;
                             function scrollbox(){
                             // 初期位置が取れていなければ処理を抜ける
                             if(initOffsetTop == null) return;

                             // スクロールさせる要素の初期位置と現在のスクロールの位置を比較
                             //初期位置より下にスクロールした時

                             scrl = $(window).scrollTop();

                             // 現在のスクロール位置を取得
                             now_top = $(setBoxId).offset().top;

                             if(initOffsetTop - 20 > now_top) {
                             // positionを設定
                             $("#temp1").show();
                             $(setBoxId).css('position', 'fixed');
                             //        $(setBoxId).css('width', '35%');
                             // topの位置を設定
                             $(setBoxId).animate({bottom: '0'}, {duration: 0});
                             } else {
                             // 設定したスタイルを持とに戻す
                             $("#temp1").hide();
                             //$(setBoxId).css('position', 'fixed');
                             //$(setBoxId).css('bottom', 1536);
                             }
                             }
                             */

                        });

                    </script>

                    <!-- btnBox end -->
                </div>
                <?php endif;?>

                <?php if(empty($data)):?>
            </div>
            <?php endif;?>
            <?php
            //リダイレクト用に現在のURL設定
            $nowUrl = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
            ?>
            <script>
                $(function(){
                    //お気に入り
                    $(".like").click(function() {
                        like_tmp_id = $(this).attr("id");

                        $("#" + like_tmp_id).before("<span id='loading'><img src='<?php echo base_url();?>images/loading.gif' /></span>");
                        $(".like").hide();

                        var like_info = like_tmp_id.split( "_" );

                        var job_id = like_info[1];
                        var like_id = like_info[2];

                        // フォームの送信データをAJAXで取得する
                        var form_data = {
                            job_id: job_id,
                            like_id: like_id,
                            regist: like_id,
                            ajax: "1",
                            csrf_test_name: "<?php echo $this->security->get_csrf_hash();?>"
                        };
                        // jQueryのAJAXファンクションを利用
                        $.ajax({
                            url: "<?php echo base_url();?>search/add/",
                            type: "POST",
                            data: form_data,

                            // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                            success: function(msg){
                                if(like_id){
                                    unit = "like_" + job_id + "_";
                                    $("#" + like_tmp_id).removeClass('done');
                                    $("#" + like_tmp_id).removeClass('add');
                                    $("#"+like_tmp_id).addClass('list');
                                    $("#" + like_tmp_id).attr('id', unit);
                                    $(".j_" + job_id + "_apply").html("<span>求人の詳細をきく</span>")
                                    $(".j_" + job_id + " span").html("気になる求人<br />リストへ追加")
                                    $(".like").show();
                                    $("#loading").remove();
                                }else{
                                    gtag('event', 'CTA', {'event_category': 'myPageReentry','event_label': 'entryButton_interest'});
                                    unit = "like_" + job_id + "_" + msg;
                                    $("#" + like_tmp_id).removeClass('list');
                                    $("#"+like_tmp_id).addClass('done');
                                    $("#"+like_tmp_id).addClass('add');
                                    $("#"+like_tmp_id).attr('id', unit);
                                    $(".j_" + job_id + "_apply").html("<span>求人の詳細をきく</span>")
                                    $(".j_" + job_id + " span").html("気になる求人リストに<br /><strong>追加済み</strong>")
                                    $(".like").show();
                                    $("#loading").remove();
                                }
                            },
                            error:function(XMLHttpRequest, textStatus, errorThrown) {
                                alert("お気に入り追加に失敗しました");
                            }
                        });

                        return false;
                    })

                    //JOBの詳細を聞く
                    $(".apply").click(function() {
                        if(confirm("お問い合わせいただき、ありがとうございます。求人の詳細については、確認次第、順次ご回答いたします。")) {
                            apply_tmp_id = $(this).attr("id");

                            $("#" + apply_tmp_id).before("<span id='loading'><img src='<?php echo base_url();?>images/loading.gif' /></span>");
                            $(".apply").hide();
                            $(".like").hide();

                            var apply_info = apply_tmp_id.split( "_" );

                            job_id = apply_info[1];
                            client_id = apply_info[2];

                            // フォームの送信データをAJAXで取得する
                            var form_data = {
                                job_id: job_id,
                                client_id: client_id,
                                ajax: "1",
                                csrf_test_name: "<?php echo $this->security->get_csrf_hash();?>"
                            };
                            // jQueryのAJAXファンクションを利用
                            $.ajax({
                                url: "<?php echo base_url();?>search/apply/",
                                type: "POST",
                                data: form_data,

                                // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                                success: function(msg){
                                    if(msg!=0){
                                        alert("エントリーに失敗しました(apply)"+msg);
                                    }else{
                                        gtag('event', 'CTA', {'event_category': 'myPageReentry','event_label': 'entryButton_ok'});
                                        location.href="<?php echo $nowUrl;?>";
                                        /*
                                         $("a." + apply_tmp_id).hide();
                                         $("#" + apply_tmp_id).before('<li class="detail applyDone"><span class="apply_' + job_id + '_' + client_id + '">応募中</span></li>');
                                         $("#" + apply_tmp_id).addClass("applyDone");
                                         $("#" + apply_tmp_id).remove();
                                         $(".j_" + job_id).hide();
                                         $("#loading").remove();
                                         */
                                    }
                                },
                                error:function(XMLHttpRequest, textStatus, errorThrown) {
//                                    alert("エントリーに失敗しました ".textStatus);
                                }
                            });
                        }
                        return false;
                    })


                    //オファーの応募
                    $(".offer").click(function() {
                        if(confirm("ご応募いただき、ありがとうございます。求人のご応募について、確認次第、順次ご回答いたします。")) {
                            offer_tmp_id = $(this).attr("id");

                            $("#" + offer_tmp_id).before("<span id='loading'><img src='<?php echo base_url();?>images/loading.gif' /></span>");
                            $(".offer").hide();
                            $(".refusal").hide();

                            job_id = $("#job_id").val();
                            client_id = $("#client").val();
                            job_name = $("#job_name").val();
                            client_name = $("#client_name").val();
                            processId = $("#processId").val();
                            phaseId = $("#phaseId").val();
                            phasedate = $("#phasedate").val();

                            // フォームの送信データをAJAXで取得する
                            var form_data = {
                                job_id: job_id,
                                client_id: client_id,
                                job_name: job_name,
                                client_name: client_name,
                                processId: processId,
                                phaseId: phaseId,
                                phasedate: phasedate,
                                ajax: "1",
                                csrf_test_name: "<?php echo $this->security->get_csrf_hash();?>"
                            };
                            // jQueryのAJAXファンクションを利用
                            $.ajax({
                                url: "<?php echo base_url();?>search/offer/",
                                type: "POST",
                                data: form_data,

                                // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                                success: function(msg){
                                    if(msg!=0){
                                        alert("エントリーに失敗しました(offer)"+msg);
                                    }else{
                                        gtag('event', 'CTA', {'event_category': 'myPageApply','event_label': 'entryButton_Apply'});
                                        location.href="<?php echo $nowUrl;?>";
                                        /*
                                         $("a." + offer_tmp_id).hide();
                                         $(".refusal").hide();
                                         //$("#" + offer_tmp_id).before('<li class="detail applyDone"><span class="apply_' + job_id + '_' + client_id + '">応募中</span></li>');
                                         $("#" + offer_tmp_id).before('<p class="entryTxt type02">この求人は現在【応募中】です</p>');
                                         $(".jobBtnBox").removeAttr("style");
                                         $("#" + offer_tmp_id).addClass("offerDone");
                                         $("#status").removeClass("type03");
                                         $("#status").text("この求人は現在【応募中】です");
                                         $("#status").addClass("type02");
                                         $("#" + offer_tmp_id).remove();
                                         $(".j_" + job_id).hide();
                                         $("#loading").remove();

                                         $(".jobBtnBox").removeAttr("id");
                                         */
                                    }
                                },
                                error:function(XMLHttpRequest, textStatus, errorThrown) {
                                    //alert("エントリーに失敗しました code:101");
                                }
                            });
                        }
                        return false;
                    })

                    //オファーの辞退
                    $(".refusal").click(function() {
                        if(confirm("応募を辞退しますか？")) {
                            refuse_tmp_id = $(this).attr("id");

                            $("#" + refuse_tmp_id).before("<span id='loading'><img src='<?php echo base_url();?>images/loading.gif' /></span>");
                            $(".offer").hide();
                            $(".refusal").hide();

                            var fm = document.getElementById("refuseform");
                            fm.submit();
                        }
                    })

                    //求人の詳細をきく
                    $(".contact").click(function() {
                        if(confirm("お問い合わせいただき、ありがとうございます。求人の詳細については、確認次第、順次ご回答いたします。")) {
                            contact_tmp_id = $(this).attr("id");

                            $("#" + contact_tmp_id).before("<span id='loading'><img src='<?php echo base_url();?>images/loading.gif' /></span>");
                            $(".contact").hide();
                            $(".like").hide();

                            job_id = $("#job_id").val();
                            client_id = $("#client").val();
                            job_name = $("#job_name").val();
                            client_name = $("#client_name").val();
                            processId = $("#processId").val();
                            phaseId = $("#phaseId").val();
                            phasedate = $("#phasedate").val();

                            // フォームの送信データをAJAXで取得する
                            var form_data = {
                                job_id: job_id,
                                client_id: client_id,
                                job_name: job_name,
                                client_name: client_name,
                                processId: processId,
                                phaseId: phaseId,
                                phasedate: phasedate,
                                ajax: "1",
                                csrf_test_name: "<?php echo $this->security->get_csrf_hash();?>"
                            };
                            // jQueryのAJAXファンクションを利用
                            $.ajax({
                                url: "<?php echo base_url();?>search/contact/",
                                type: "POST",
                                data: form_data,

                                // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                                success: function(msg){
                                    if(msg!=0){
                                        alert("処理に失敗しました(contact)"+msg);
                                    }else{
                                        gtag('event', 'CTA', {'event_category': 'myPageContact','event_label': 'entryButton_Contact'});
                                        location.href="<?php echo $nowUrl;?>";
                                        /*
                                         $("a." + contact_tmp_id).hide();
                                         $(".like").show();
                                         $("#" + contact_tmp_id).before('<li class="detail applyDone"><span class="apply_' + job_id + '_' + client_id + '">問い合わせ中</span></li>');
                                         $("#" + contact_tmp_id).addClass("contactDone");
                                         $("#" + contact_tmp_id).remove();
                                         $(".j_" + job_id).hide();
                                         $("#loading").remove();
                                         */
                                    }
                                },
                                error:function(XMLHttpRequest, textStatus, errorThrown) {
                                    //alert("エントリーに失敗しました code:101");
                                }
                            });
                        }
                        return false;
                    })
                })
            </script>
        </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($related)):?>
                <div id="jobList">
                    <div id="jobWrapper">
                        <h2 class="title"><span>類似する求人情報</span></h2>
                        <?php foreach($related as $k=>$v):?>

                            <!-- jobBox start -->
                            <div class="jobBox">
                                <div class="inner">
                                    <h2><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></h2>
                                    <div class="tableWrap">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <th>仕事内容</th>
                                                <td><?php echo mb_substr(htmlspecialchars($v->summary, ENT_QUOTES, 'UTF-8'),0,50);?>…</td>
                                            </tr>
                                            <tr>
                                                <th>想定年収</th>
                                                <td><?php if(!empty($v->minsalary)):?><?php echo htmlspecialchars($v->minsalary, ENT_QUOTES, 'UTF-8');?>万円<?php if(empty($v->maxsalary)):?><?php else:?>～<?php endif;?><?php endif;?><?php if(!empty($v->maxsalary)):?><?php echo htmlspecialchars($v->maxsalary, ENT_QUOTES, 'UTF-8');?>万円&nbsp;<?php endif;?></td>
                                            </tr>
                                            <tr>
                                                <th>勤務地</th>
                                                <td>                    <?php if(!empty($v->prefecture_name)):?>
                                                        <?php $tmpp = explode(",",$v->prefecture_name);?>
                                                        <?php foreach($tmpp as $pk => $pv):?>
                                                            <?php echo htmlspecialchars($pv, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpp) > 1):?>/<?php endif;?>
                                                        <?php endforeach;?>
                                                    <?php endif;?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <p class="btn"><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><span>この求人の詳細をみる</span></a></p>
                                </div>
                            </div>
                            <!-- jobBox end -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
            <!-- historyBox end -->