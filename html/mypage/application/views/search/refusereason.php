<?php
//HRBCマスターと連動

//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);
$job_ary=array();
$sort=array();
if(!empty($tmpjob['Item'])){
    foreach($tmpjob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $job_ary as $key => $value) {
        $sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($sort, SORT_ASC, $job_ary);
?>
<h1>辞退理由</h1>
<!-- co start -->
<div id="co">
    <div id="coSub" class="clearfix">
        <main id="mainCo">
                <div id="editTxt">
                    <div class="inner">
                        <p>辞退を受け付けました。差し支えなければ理由を教えて下さい。<br>
                            （今後のご案内の参考にさせていただきます）</p>
                    </div>
                </div>
                <div class="error"></div>
            <div class="error"><?php echo $msg ?></div>
            <?php echo form_open('search/refusefinish/');?>
                    <div class="registInputPage form">
                        <table class="no">
                            <tr>
                                <th>辞退理由<br>
                                    <img src="<?php echo base_url();?>images/ico_hissu.png" class="jitaireason" width="49" height="14" alt=""/></th>
                                <td><label for="jitai00" class="radiolabel reasonradio">
                                        <input type="radio" name="reason" value="仕事内容" class="radio reasonradio"  id="jitai00"  />
                                        仕事内容</label>
                                    <label for="jitai01" class="radiolabel reasonradio">
                                        <input type="radio" name="reason" value="業界内容" class="radio reasonradio"  id="jitai01"  />
                                        業界内容</label>
                                    <label for="jitai02" class="radiolabel reasonradio">
                                        <input type="radio" name="reason" value="年収" class="radio reasonradio" id="jitai02"  />
                                        年収</label>
                                    <label for="jitai03" class="radiolabel reasonradio">
                                        <input type="radio" name="reason" value="休日" class="radio reasonradio" id="jitai03" />
                                        休日 </label>
                                    <label for="jitai04" class="radiolabel reasonradio">
                                        <input type="radio" name="reason" value="勤務地・転勤の有無" class="radio reasonradio" id="jitai04" />
                                        勤務地・転勤の有無</label>
                                <label for="jitai05" class="radiolabel reasonradio">
                                        <input type="radio" name="reason" value="すでに別ルートで応募済み" class="radio reasonradio" id="jitai05" />すでに別ルートで応募済み</label>
                                <label for="jitai06" class="radiolabel reasonradio">
                                        <input type="radio" name="reason" value="その他" class="radio reasonradio" id="jitai06" />その他</label>
                                <textarea name="comment" id="comment" cols="70" rows="3" style="width: 100%;" placeholder="その他をお選びの方は辞退理由をご記入ください。"></textarea>
<div id="reason"></div>
                                </td>
                            </tr>
                        </table>
                        <div class="btnBox inline" id="btnBox">
                            <button class="btn" id="sbt" onclick="return reasonCheck(1);"><span>この内容で送信する</span></button>
                            <p class="btnBack"><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($job->job_id, ENT_QUOTES, 'UTF-8');?>/" id="modoru"><span>求人詳細に戻る</span></a></p>
                        </div>
                    </div>
		<input type="hidden" name="process_id" value="<?php echo htmlspecialchars($process_id, ENT_QUOTES, 'UTF-8');?>" />
		<input type="hidden" name="job_id" value="<?php echo htmlspecialchars($job->job_id, ENT_QUOTES, 'UTF-8');?>" />
                <?php echo form_hidden($csrf); ?>
                </form>
                <!-- conditionListt end --> 
            </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($looks)):?>
                <div id="historyBox">
                    <h2><span>最近閲覧した求人</span></h2>
                    <div class="inner">
                        <?php foreach($looks as $k=>$v):?>
                            <!-- entry start -->
                            <div class="entry">
                                <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                            </div>
                            <!-- entry end -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
            <!-- historyBox end -->

<script>
$(function(){

	var beforecode;

	$(document).on('keydown',function(e){
		var keycode = (e.keyCode ? e.keyCode : e.which);
		var tagname = e.target.tagName;
		var altkey = event.altKey;
		if (keycode == 116
			|| keycode == 17 && beforecode == 82 
			|| keycode == 82 && beforecode == 17
			|| keycode == 8 && (tagname != 'INPUT' && tagname != 'TEXTAREA')
			|| altkey && (keycode == 37 || keycode == 39)
			|| keycode == 18 && beforecode == 37
			|| keycode == 37 && beforecode == 18) {
			// 中断
			return false;
		}
		beforecode = keycode;
	});

	history.pushState(null, null, null);
	$(window).on('popstate', function(){
		history.pushState(null, null, null);
	});

	//理由
	$('.reasonradio').click(function(){
	    var idx = $(".reasonradio").index(this);
	    var cnt = $('.reasonradio').length;
	    var checked = 0;
	    for(i=0;i<cnt;i++){
		if($(".reasonradio").eq(i).prop("checked")){
			 checked++;
		}
	    }
	    if (checked > 0) {
			if($("input[name='reason']:checked").val()!="その他"){
				$(".jitaireason").removeClass("hissu");
				$(".jitaireason").addClass("ok");
				$(".jitaireason").attr("src", "/mypage/images/ico_ok.png");
			}else if($("input[name='reason']:checked").val()=="その他" && $("#comment").val()){
				$(".jitaireason").removeClass("hissu");
				$(".jitaireason").addClass("ok");
				$(".jitaireason").attr("src", "/mypage/images/ico_ok.png");
			}else if($("input[name='reason']:checked").val()=="その他" && !$("#comment").val()){
				$(".jitaireason").removeClass("ok");
				$(".jitaireason").addClass("hissu");
				$(".jitaireason").attr("src", "/mypage/images/ico_hissu.png");
			}
	    }else{
			$(".jitaireason").removeClass("ok");
			$(".jitaireason").addClass("hissu");
			$(".jitaireason").attr("src", "/mypage/images/ico_hissu.png");
	    }
	})

	$("#comment").blur(function(){
		if($("input[name='reason']:checked").val()=="その他" && $("#comment").val()){
				$(".jitaireason").removeClass("hissu");
				$(".jitaireason").addClass("ok");
				$(".jitaireason").attr("src", "/mypage/images/ico_ok.png");
		}else{
			if($("input[name='reason']:checked").val()=="その他"){
				$(".jitaireason").removeClass("ok");
				$(".jitaireason").addClass("hissu");
				$(".jitaireason").attr("src", "/mypage/images/ico_hissu.png");
			}
		}
	});
});
	function reasonCheck(param){
		$("div.error").remove();

		var radio_tmp = document.getElementsByName("reason");
		var cnt = 0;
		for(var i=0;i<radio_tmp.length;i++) {
			if (radio_tmp[i].checked) {
				cnt = 1;
			}
		}
		if(cnt == 0){
			$("#reason").append("<div class='error'>辞退理由を選択してください。</div>");
		}else{
			var reason = $("input[name='reason']:checked").val();
			if(reason=="その他" && !$("#comment").val()){
				$("#reason").append("<div class='error'>その他を選択した場合は辞退理由を記入してください。</div>");
			}
		}

		if($("div.error").size() > 0){
			$('html,body').animate({ scrollTop: $("div.error:first").offset().top-150}, 'slow');
			return false;
		}else{
	                $("#btnBox ").before("<div id='loading' style='text-align:center;'><img src='<?php echo base_url();?>images/loading.gif' /></div>");
	                $("#sbt").hide();
	                $("#modoru").hide();
			return true;
			//document.myform.action = "<?php echo base_url();?>suspension/test/";
		}
	}
</script>

