<?php
//HRBCマスターと連動

//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);
$job_ary=array();
$sort=array();
if(!empty($tmpjob['Item'])){
    foreach($tmpjob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $job_ary as $key => $value) {
        $sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($sort, SORT_ASC, $job_ary);
?>
    <h1>辞退理由</h1>
    <!-- co start -->
    <div id="co">
        <div id="coSub" class="clearfix">
            <main id="mainCo">
                <div id="jitaiTxt">
                        <p>ご回答ありがとうございました</p>
                </div>
                    <div class="registInputPage form">
                        <div class="btnBox inline ss">
                            <p class="btnBack"><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($job->job_id, ENT_QUOTES, 'UTF-8');?>/"><span>求人詳細に戻る</span></a></p>
                        </div>
                    </div>
                <!-- conditionListt end --> 
            </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($looks)):?>
                <div id="historyBox">
                    <h2><span>最近閲覧した求人</span></h2>
                    <div class="inner">
                        <?php foreach($looks as $k=>$v):?>
                            <!-- entry start -->
                            <div class="entry">
                                <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                            </div>
                            <!-- entry end -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
            <!-- historyBox end -->

<script>
$(function(){

	var beforecode;

	$(document).on('keydown',function(e){
		var keycode = (e.keyCode ? e.keyCode : e.which);
		var tagname = e.target.tagName;
		var altkey = event.altKey;
		if (keycode == 116
			|| keycode == 17 && beforecode == 82 
			|| keycode == 82 && beforecode == 17
			|| keycode == 8 && (tagname != 'INPUT' && tagname != 'TEXTAREA')
			|| altkey && (keycode == 37 || keycode == 39)
			|| keycode == 18 && beforecode == 37
			|| keycode == 37 && beforecode == 18) {
			// 中断
			return false;
		}
		beforecode = keycode;
	});

	history.pushState(null, null, null);
	$(window).on('popstate', function(){
		history.pushState(null, null, null);
	});
});
</script>