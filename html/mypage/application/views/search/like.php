<?php
//HRBCマスターと連動

//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

?>
<script src="<?php echo base_url();?>js/jquery.tile.js"></script>
<script>
    if ( window.matchMedia( 'screen and (min-width:769px)' ).matches ) {
        $(window).on('load resize',function() {
            $('#jobList .jobBox h2').tile(2);
            $('#jobList .jobBox .tableWrap').tile(2);
        } );
    }
</script>
<h1>気になる求人リスト</h1>
<!-- co start -->
<div id="co">
    <div id="coSub" class="clearfix">
        <main id="mainCo"><!-- jobList strat -->
            <div id="jobList">

                <?php if(!empty($contents)):?>
                <p class="resultTxt"><span class="point"><?php echo $total;?></span>件中 <?php echo $offset;?>〜<?php echo $hani;?>件の検索結果を表示しています。</p>
                <!-- pagenation start -->
                <?php if(!empty($paging)):?>
                    <div class="pagenation">
                        <?php echo $paging ?>
                    </div>
                <?php endif;?>

                <!-- pagenation end -->

                <!-- like del form -->
                <div class="btnBox">
                    <?php echo form_open('search/likedelall/');?>
                    <input type="hidden" name="page" value="<?php echo $clean->purify($page);?>" />
                    <button class="allclear" onclick='return confirm("削除してよろしいですか？");'><span>チェックした求人をまとめて削除する</span></button>
                </div>
                <div id="jobWrapper">
                    <div id="jobInner">

                        <?php foreach ($contents->result()  as $val): ?>
                            <!-- jobBox start -->
                            <div class="jobBox checklist">
                                <div class="inner">
                                    <label class="CheckBoxLabelClass" for="ch<?php echo $clean->purify($val->like_id);?>">
                                        <input type="checkbox" id="ch<?php echo $clean->purify($val->like_id);?>"  class="checkbox" name="likedel[]" value="<?php echo $clean->purify($val->like_id);?>">
                                    </label>
                                    <p id="like_<?php echo $clean->purify($val->job_id);?>_<?php echo $clean->purify($val->like_id);?>" class="j_<?php echo $clean->purify($val->job_id);?> preserve like btnClose"><a href="javascript:void(0);"><img src="<?php echo base_url();?>images/ico_close_02.png" width="14" height="14" alt=""/></a></p>
                                    <h2><a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($val->job_id);?>/"><?php echo str_replace('-','/',$clean->purify($val->job_title));?></a></h2>
                                    <?php
                                    $naiyo = $val->summary;
                                    if(mb_strlen($naiyo) > 80){
                                        $naiyo = mb_substr($naiyo, 0, 80) . "...";
                                        $naiyo = $clean->purify($naiyo);
                                    }else{
                                        $naiyo = $clean->purify($naiyo);
                                    }
                                    ?>

                                    <div class="tableWrap">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <th>仕事内容</th>
                                                <td><?php echo $clean->purify($naiyo);?></td>
                                            </tr>
                                            <tr>
                                                <th>想定年収</th>
                                                <td><?php if(!empty($val->minsalary)):?><?php echo htmlspecialchars($val->minsalary, ENT_QUOTES, 'UTF-8');?>万円<?php if(empty($val->maxsalary)):?><?php else:?>～<?php endif;?><?php endif;?><?php if(!empty($val->maxsalary)):?><?php echo htmlspecialchars($val->maxsalary, ENT_QUOTES, 'UTF-8');?>万円&nbsp;<?php endif;?></td>
                                            </tr>
                                            <tr>
                                                <th>勤務地</th>
                                                <td><?php echo $clean->purify($val->prefecture_name);?></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <ul class="btn clearfix">
                                        <?php if($val->phaze == $phaze && $val->publish == $publish):?>
                                            <li class="application more"> <a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($val->job_id);?>/"><span>もっとみる</span></a></li>

                                            <li id="apply_<?php echo $clean->purify($val->job_id);?>_<?php echo $clean->purify($val->client_id);?>"  class="apply application contact">
                                                <a href="javascript:void(0);" class="apply_<?php echo $clean->purify($val->job_id);?>_<?php echo $clean->purify($val->client_id);?>"><span>問合せ</span></a>
                                            </li>
                                        <?php else:?>
                                            <li class="applyDone">
                                                <span class="apply_<?php echo $clean->purify($val->job_id);?>_<?php echo $clean->purify($val->client_id);?>">募集終了</span>
                                            </li>
                                        <?php endif;?>
                                    </ul>
                                </div>
                            </div>
                            <!-- jobBox end -->
                        <?php endforeach;?>
                    </div>
                </div>
                <div class="btnBox">
                    <button class="allclear" onclick='return confirm("削除してよろしいですか？");'><span>チェックした求人をまとめて削除する</span></button>
                </div>
                <p class="resultTxt"><span class="point"><?php echo $total;?></span>件中 <?php echo $offset;?>〜<?php echo $hani;?>件の検索結果を表示しています。</p>
                <!-- pagenation start -->
                <?php if(!empty($paging)):?>
                    <div class="pagenation">
                        <?php echo $paging ?>
                    </div>
                <?php endif;?>
            </div>
            <!-- jobList end -->
        </main>
        <script>
            $(".like").click(function() {
                if(confirm("削除してよろしいですか？")) {
                    like_tmp_id = $(this).attr("id");
                    var like_info = like_tmp_id.split("_");

                    var job_id = like_info[1];
                    var like_id = like_info[2];

                    // フォームの送信データをAJAXで取得する
                    var form_data = {
                        job_id: job_id,
                        like_id: like_id,
                        ajax: "1",
                        csrf_test_name: "<?php echo $this->security->get_csrf_hash();?>"
                    };
                    // jQueryのAJAXファンクションを利用
                    $.ajax({
                        url: "<?php echo base_url();?>search/likedel/",
                        type: "POST",
                        data: form_data,

                        // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                        success: function (msg) {
                            if (msg == "error") {
                                alert("削除に失敗しました" + msg);
                            } else {
                                location.href = "<?php echo base_url();?>" + msg;
                            }
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert("削除に失敗しました");
                        }
                    });
                }
                return false;
            })

            //申込
            $(".apply").click(function() {
                if(confirm("ご応募いただき、ありがとうございます。求人のご応募について、確認次第、順次ご回答いたします。")) {
                    apply_tmp_id = $(this).attr("id");

                    $("#" + apply_tmp_id).before("<div id='loading' style='text-align:center;margin-right:0px;' class=''><img src='<?php echo base_url();?>images/loading.gif' /></div>");
                    $(".application").hide();
                    $(".like").hide();

                    var apply_info = apply_tmp_id.split( "_" );

                    job_id = apply_info[1];
                    client_id = apply_info[2];

                    // フォームの送信データをAJAXで取得する
                    var form_data = {
                        job_id: job_id,
                        client_id: client_id,
                        ajax: "1",
                        csrf_test_name: "<?php echo $this->security->get_csrf_hash();?>"
                    };
                    // jQueryのAJAXファンクションを利用
                    $.ajax({
                        url: "<?php echo base_url();?>search/likeapply/",
                        type: "POST",
                        data: form_data,

                        // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                        success: function(msg){
                            if(msg!=0){
                                alert("エントリーに失敗しました"+msg);
                            }else{
                                ga('send', 'event', 'myPageReentry', 'CTA', 'entryButton_ok');
                                location.href = "<?php echo base_url();?>search/like/";
                                /*
                                 $("a." + apply_tmp_id).hide();
                                 $("#" + apply_tmp_id).before('<li class="detail applyDone"><span class="apply_' + job_id + '_' + client_id + '">応募済み</span></li>');
                                 $("#" + apply_tmp_id).addClass("applyDone");
                                 $("#" + apply_tmp_id).remove();
                                 $(".j_" + job_id).hide();
                                 */
                            }
                        },
                        error:function(XMLHttpRequest, textStatus, errorThrown) {
                            alert("エントリーに失敗しました code:101");
                        }
                    });
                }
                return false;
            })
        </script>
        <script>
            //チェンジイベント
            $(function(){
                $('ul input[type="checkbox"]').change(function(){
                    if ($(this).is(':checked')) {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', true);
                    }
                    else {
                        $(this).parent().find('input[type="checkbox"]').prop('checked', false);
                        $(this).parents('li').each(function(){
                            $(this).children('input[type="checkbox"]').prop('checked', false);
                        });
                    }
                });

                $('.checkbox').click(function () {
                    var idx = $(".checkbox").index(this);
                    var cnt = $('.checkbox').length;
                    for (i = 0; i < cnt; i++) {
                        if ($(".checkbox").eq(i).prop("checked")) {
                            $(".CheckBoxLabelClass").eq(i).addClass('checked');
                        } else {
                            $(".CheckBoxLabelClass").eq(i).removeClass('checked');
                        }
                    }
                });
            })
        </script>
        <style>
            ul li{
                list-style-type:none;
            }
        </style>


        <?php echo form_close();?>

        <?php else:?>
        <p>気になる求人リストには登録されていません。</p>
    </div>
    <!-- jobList end -->
    </main>
    <?php endif;?>
    <aside id="subCo">
        <!-- historyBox start -->
        <?php if(!empty($looks)):?>
            <div id="historyBox">
                <h2><span>最近閲覧した求人</span></h2>
                <div class="inner">
                    <?php foreach($looks as $k=>$v):?>
                        <!-- entry start -->
                        <div class="entry">
                            <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                        </div>
                        <!-- entry end -->
                    <?php endforeach;?>
                </div>
            </div>
        <?php endif;?>
        <!-- historyBox end -->
