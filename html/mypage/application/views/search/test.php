<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Recommend extends MY_Controller
{

    //////////////////////////////////////////////////////////
    //conf
    //////////////////////////////////////////////////////////

    //カテゴリー、画像、ソート、日付指定がありの場合はon ※ソートと日付はどっちか一つ
    protected $use = array("category" => "on", "file" => "on", "sort" => "off", "date" => "on", "display" => "on");

    //カテゴリーのtable名
    protected $category_table = "recommends_categories";

    //タイトル
    protected $sub_title = "オススメ";

    //segment
    protected $segment = "recommend";

    //database
    protected $database = "recommends";

    //画像がonの時 許可ファイル
    protected $allow = 'gif|jpg|png|jpeg|doc|xls|ppt|pdf';

    //サムネイル有り無し（PDFなどの時はなし)
    protected $thumb = "off";
    protected $width = 100;
    protected $height = 100;

    //ページネーションの数
    protected $page_limit = 20;

    //////////////////////////////////////////////////////////
    // construct
    //////////////////////////////////////////////////////////
    function __construct()
    {
        parent::__construct();
    }

    //////////////////////////////////////////////////////////
    //一覧出力
    //////////////////////////////////////////////////////////
    function home()
    {
        $posts = $this->input->post();

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $params = array();
        $params["del"] = 0;

        $cat = "";
        $disp = "";
        $url = "/";
        $order = "";
        //order sort or create_date
        if ($this->use["sort"] == "on") {
            $order = "sort asc";
        } else if ($this->use["date"] == "on") {
            $order = "create_date desc, updated desc";
        } else {
            echo "ソートか日付の設定をして下さい。";
            exit;
        }

        //カテゴリーありの場合
        /*
                if($this->use["category"] == "on"){

                    //カテゴリー取得
                    $data["categories"] = $this->db->order_by('category_sort asc')->get_where($this->category_table, array("category_del"=>0, "category_display"=>1));

                    $cat = $this->uri->segment(3);
                    $chk=0;
                    foreach ($data["categories"]->result() as $row){
                        if($row->category_id == $cat){
                            $chk=1;
                            break;
                        }
                    }

                    if($chk==1){
                        $cat = $cat;
                    }else if(!empty($data['categories']->result_object[0]->category_id)){
                        $cat = $data['categories']->result_object[0]->category_id;
                    }else{
                        //カテゴリーが登録してない
                    }

                    //カテゴリー　パラメータ
                    $params["category_id"] = $cat;

                    //urlパラメータ
                    //$url = $cat . "/";
                }else{
                    $cat = 1;
                }

                //表示/非表示有の場合
                if($this->use["display"]=="on"){
                    //カテゴリーありの場合はセグメント４　なしは３
                    if($this->use["category"] == "on"){
                        $disp = $this->uri->segment(4);
                    }else{
                        $disp = $this->uri->segment(3);
                    }
                    if(empty($disp)) $disp=1;

                    $params["display"] = $disp;

                    //urlパラメータ
                    //$url = $disp . "/";
                }
        */

        //Pagination
        $this->load->library("pagination");
        $config['base_url'] = base_url() . $this->uri->segment(1) . "/" . $this->uri->segment(2) . "/" . $url;
        $config["total_rows"] = $this->db->get_where($this->database, $params)->num_rows();
        $config["per_page"] = $this->page_limit;
        $config["num_links"] = 2;
        $config['page_query_string'] = TRUE;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><span class="current">';
        $config['cur_tag_close'] = '</span></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        //data設定
        if (!empty($this->input->get('per_page'))) {
            $offset = $this->input->get('per_page');
        } else {
            $offset = 0;
        }
        $data["contents"] = $this->db->order_by($order)->get_where($this->database, $params, $config["per_page"], $offset);;
        $data["render"] = $this->segment . "/home";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["cat"] = $cat;
        $data["disp"] = $disp;
        $data["use"] = $this->use;
        $data["sub_title"] = $this->sub_title;
        $data["segment"] = $this->segment;

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //登録情報入力
    //////////////////////////////////////////////////////////
    function regist()
    {
        //カテゴリー取得
        $data["categories"] = $this->db->order_by('category_sort asc')->get_where($this->category_table, array("category_del" => 0, "category_display" => 1));

        $posts = $this->input->post();

        //idが設定されていて戻るじゃない場合は編集
        if (!empty($posts['id']) && empty($posts['mode'])) {
            $data[$this->segment] = $this->db->get_where($this->database, array('id' => $posts['id']))->row();
        } else {
            $data[$this->segment] = (object)$posts;
            //idがない場合は空データ設定
            if (empty($data[$this->segment]->id)) {
                $data[$this->segment]->id = "";
            }
        }

        $error = "";

        //data設定
        $data["render"] = $this->segment . "/regist";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["use"] = $this->use;
        $data["sub_title"] = $this->sub_title;
        $data["segment"] = $this->segment;

        $this->load->view("template", $data);

    }

    //////////////////////////////////////////////////////////
    //登録情報確認
    //////////////////////////////////////////////////////////
    function confirm()
    {
        //カテゴリー取得
        $data["categories"] = $this->db->order_by('category_sort asc')->get_where($this->category_table, array("category_del" => 0, "category_display" => 1));

        //バリデーション実行
        $error = "";
        $tmp_error = 0;
        $e_msg = "";
        $error = $this->setValidation();

        $posts = $this->input->post();

        //オブジェクトへ変換
        $data[$this->segment] = (object)$posts;


        if ($error !== "") {
            //data設定
            $data["render"] = $this->segment . "/regist";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["use"] = $this->use;
            $data["sub_title"] = $this->sub_title;
            $data["msg"] = $error;
            $data["segment"] = $this->segment;

            $this->load->view("template", $data);
        } else {
            if ($this->use["file"] == "on") {
                //fileアップロード
                $result = array();
                $files = $_FILES;
                $cpt = count($files['userfile']['name']);
                for ($i = 0; $i < $cpt; $i++) {
                    $filename = "";
                    $require = 1;
                    $allow = $this->allow;
                    $overwrite = true;
                    //@param(post, num, files, directory, require, filename)
                    $result[] = $this->fileUpload(@$posts['image' . ($i + 1)], $i, $files, $this->segment, $require, $filename, $overwrite, $allow);

                    if (!empty($result[$i]['error'])) {
                        $tmp_error = 1;
                        $tmp_cnt = $i + 1;
                        $e_msg .= $result[$i]['error'] . "ファイル" . $tmp_cnt . "<br />";
                    }
                }

                if ($this->thumb == "on") {
                    //サムネイル
                    if (!empty($result[0]['upload_data']['full_path'])) $this->resizeImage($result[0]['upload_data']['full_path'], $this->width, $this->height, "_thumb", "auto");
                }

                /*
                /   画像とアップロードファイルの設定
                /	１．アップロードされている場合はそのファイルを使用
                /	２．アップロードがない場合はpostデータ（元々のファイルか未設定）を使用
                /	３．削除チェックしてある場合は設定しない
                */

                /////////////////////////////////////////////////////////////////////////////
                //1枚目
                /////////////////////////////////////////////////////////////////////////////
                if (!empty($result[0]['upload_data']['file_size']) || !empty($posts['image1'])) {
                    (!empty($result[0]['upload_data']['file_size'])) ? $data[$this->segment]->image1 = basename($result[0]['upload_data']['file_name']) : $data[$this->segment]->image1 = $posts['image1'];
                } else {
                    $data[$this->segment]->image1 = "";
                }
                //if(@$posts['image1_del'] == "on"){
                //	$data[$this->segment]->image1="";
                //}

                /////////////////////////////////////////////////////////////////////////////
                //2枚目以降
                /////////////////////////////////////////////////////////////////////////////
                for ($i = 1; $i < $cpt; $i++) {
                    if (!empty($result[$i]['upload_data']['file_size']) || !empty($posts['image' . ($i + 1)])) {
                        (!empty($result[$i]['upload_data']['file_size'])) ? $data[$this->segment]->{"image" . ($i + 1)} = basename($result[$i]['upload_data']['file_name']) : $data[$this->segment]->{"image" . ($i + 1)} = $posts['image' . ($i + 1)];
                    } else {
                        $data[$this->segment]->{"image" . ($i + 1)} = "";
                    }
                    if (@$posts['image' . ($i + 1) . '_del'] == "on") {
                        $data[$this->segment]->{"image" . ($i + 1)} = "";
                    }
                }
            }

            if ($tmp_error != 1) {
                //data設定
                $data["render"] = $this->segment . "/confirm";
                $data["clean"] = $this->clean;
                $data["csrf"] = $this->_get_csrf_nonce();
                $data["msg"] = $error;
                $data["use"] = $this->use;
                $data["sub_title"] = $this->sub_title;
                $data["segment"] = $this->segment;

                $this->load->view("template", $data);
            } else {
                $error = "";
                $error .= "<br />" . $e_msg;

                //data設定
                $data["render"] = $this->segment . "/regist";
                $data["clean"] = $this->clean;
                $data["csrf"] = $this->_get_csrf_nonce();
                $data["msg"] = $error;
                $data["use"] = $this->use;
                $data["sub_title"] = $this->sub_title;
                $data["segment"] = $this->segment;

                $this->load->view("template", $data);
            }
        }
    }

    //////////////////////////////////////////////////////////
    //登録処理
    //////////////////////////////////////////////////////////
    function add()
    {
        //カテゴリー取得
        $data["categories"] = $this->db->order_by('category_sort asc')->get_where($this->category_table, array("category_del"=>0, "category_display"=>1));

        //バリデーション実行
        $tmp_error = 0;
        $e_msg = "";
        $error = "";
        $error = $this->setValidation();

        $posts = $this->input->post();


        //オブジェクトへ変換
        $data[$this->segment] = (object)$posts;

        //プレビュー
        if($posts["action"]=="preview"){
            //data設定
            $data["render"] = $this->segment."/preview";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["msg"] = $error;
            $data["use"] = $this->use;
            $data["sub_title"] = $this->sub_title;
            $data["segment"] = $this->segment;

            $this->load->view("template_preview", $data);
        }else {

            if (empty($posts["content"]) && empty($posts["url"])) {
                $error .= "<p>URLか本文のどちらか一つを入力してください。</p>";
            }
            if (!empty($posts["content"]) && !empty($posts["url"])) {
                $error .= "<p>URLか本文のどちらか一つを入力してください。</p>";
            }

            if($error !== ""){
                //data設定
                $data["render"] = $this->segment."/regist";
                $data["clean"] = $this->clean;
                $data["csrf"] = $this->_get_csrf_nonce();
                $data["msg"] = $error;
                $data["cat"] = $posts["category_id"];
                $data["use"] = $this->use;
                $data["sub_title"] = $this->sub_title;
                $data["segment"] = $this->segment;

                $this->load->view("template", $data);
            }else{
                if($this->use["file"] == "on"){
                    //fileアップロード
                    $result=array();
                    $files = $_FILES;
                    $cpt = count($files['userfile']['name']);
                    for ($i = 0; $i < $cpt; $i++) {
                        $filename="";
                        $require=1;
                        $allow =  $this->allow;
                        $overwrite=true;
                        //@param(post, num, files, directory, require, filename)
                        $result[]= $this->fileUpload(@$posts['image'.($i+1)], $i , $files, $this->segment, $require, $filename, $overwrite,$allow);

                        if(!empty($result[$i]['error'])){
                            $tmp_error = 1;
                            $tmp_cnt = $i+1;
                            $e_msg .=  $result[$i]['error'] . "ファイル". $tmp_cnt . "<br />";
                        }
                    }

                    if($this->thumb=="on"){
                        //サムネイル
                        if(!empty($result[0]['upload_data']['full_path'])) $this->resizeImage($result[0]['upload_data']['full_path'], $this->width, $this->height, "_thumb", "auto");
                    }

                    /*
                    /   画像とアップロードファイルの設定
                    /	１．アップロードされている場合はそのファイルを使用
                    /	２．アップロードがない場合はpostデータ（元々のファイルか未設定）を使用
                    /	３．削除チェックしてある場合は設定しない
                    */

                    /////////////////////////////////////////////////////////////////////////////
                    //1枚目
                    /////////////////////////////////////////////////////////////////////////////
                    if(!empty($result[0]['upload_data']['file_size']) || !empty($posts['image1'])){
                        (!empty($result[0]['upload_data']['file_size'])) ? $data[$this->segment]->image1=basename($result[0]['upload_data']['file_name']) : $data[$this->segment]->image1=$posts['image1'];
                    }else{
                        $data[$this->segment]->image1="";
                    }
                    //if(@$posts['image1_del'] == "on"){
                    //	$data[$this->segment]->image1="";
                    //}

                    /////////////////////////////////////////////////////////////////////////////
                    //2枚目以降
                    /////////////////////////////////////////////////////////////////////////////
                    for ($i = 1; $i < $cpt; $i++) {
                        if(!empty($result[$i]['upload_data']['file_size']) || !empty($posts['image'.($i+1)])){
                            (!empty($result[$i]['upload_data']['file_size'])) ? $data[$this->segment]->{"image".($i+1)}=basename($result[$i]['upload_data']['file_name']) : $data[$this->segment]->{"image".($i+1)}=$posts['image'.($i+1)];
                        }else{
                            $data[$this->segment]->{"image".($i+1)}="";
                        }
                        if(@$posts['image' . ($i+1) . '_del'] == "on"){
                            $data[$this->segment]->{"image".($i+1)}="";
                        }
                    }
                }

                if($tmp_error == 1){
                    $error="";
                    $error .= "<br />" . $e_msg;

                    //data設定
                    $data["render"] = $this->segment."/regist";
                    $data["clean"] = $this->clean;
                    $data["csrf"] = $this->_get_csrf_nonce();
                    $data["msg"] = $error;
                    $data["use"] = $this->use;
                    $data["cat"] = $posts["category_id"];
                    $data["sub_title"] = $this->sub_title;
                    $data["segment"] = $this->segment;

                    $this->load->view("template", $data);
                }else{

                    //idありの場合、現在のカテゴリー、ソート、表示/非表示
                    $tmpCat="";
                    $tmpSort="";
                    $tmpDisp="";
                    $tmpDatas = $this->db->get_where($this->database, array('id' => $posts['id']))->row();
                    if(!empty($tmpDatas)){
                        $tmpCat = $tmpDatas->category_id;
                        $tmpSort = $tmpDatas->sort;
                        $tmpDisp = $tmpDatas->display;
                    }

                    if(empty($posts['id'])){
                        $insert['created'] = date('Y-m-d');
                    }
                    $insert['id'] = $posts['id'];
                    $insert['title'] = $posts['title'];
                    $insert['content'] = $posts['content'];
                    //$insert['image1'] = $data[$this->segment]->image1;

                    //日付ありの場合
                    if($this->use["date"] == "on"){
                        $insert['create_date'] = $posts['create_date'];
                    }

                    //カテゴリーありの場合
                    if($this->use["category"] == "on"){
                        $insert['category_id'] = $posts['category_id'];
                    }

                    //画像ありの場合
                    /*
                                    if($this->use["file"] == "on"){
                                        $insert['image1'] = @$posts['image1'];
                                    }
                    */

                    //表示/非表示ありの場合
                    if($this->use["display"] == "on"){
                        $insert['display'] = $posts['display'];
                        //非表示の場合は99999999
                        if($posts["display"]==2){
                            $posts["sort"] = 99999999;
                        }
                    }

                    $this->load->model($this->database . '_model');
                    $tmpDb=$this->database."_model";
                    $ret = $this->$tmpDb->save($insert['id'], $insert, "id");

                    if($this->use["sort"] == "on"){
                        //カテゴリー変更をしたとき
                        if($posts['id'] && $tmpCat != $posts['category_id']){
                            //表示/非表示ありの時で表示->非表示に変える場合
                            if($this->use["display"]=="on" && ($posts["display"] == 2 && $tmpDisp==1)){
                                $data = array('sort' => 99999999);
                                $this->db->where('id', $posts['id']);
                                $this->db->update($this->database, $data);
                                //表示/非表示なしか　表示の場合は新しいカテゴリーの最後にする
                            }else{
                                //非表示のままの場合は何もしない
                                if($this->use["display"]=="on" && ($posts["display"] == 2 && $tmpDisp==2)){
                                }else{
                                    $param=array();
                                    $param["del"] = 0;
                                    $param["category_id"] = $posts["category_id"];
                                    $param["display"] = 1;

                                    $sort_last = $this->db->get_where($this->database, $param)->num_rows();
                                    $data = array('sort' => $sort_last);
                                    $this->db->where('id', $posts['id']);
                                    $this->db->update($this->database, $data);
                                }
                            }

                            //元のカテゴリーのソート順を変更（変更前のソートより大きいデータを１ずつマイナス）
                            $data2 = $this->db->order_by('sort asc')->get_where($this->database, array("del"=>0, "category_id"=>$tmpCat, "sort >" =>  $tmpSort, "display"=>1));
                            //データがあった場合だけソート処理
                            if(!empty($data2->num_rows())){
                                foreach ($data2->result() as $row){
                                    $sort_num=$row->sort - 1;
                                    $data = array('sort' => $sort_num);
                                    $this->db->where('id', $row->id);
                                    $this->db->update($this->database, $data);
                                }
                            }
                            //カテゴリーが同じとき
                        }else if($posts['id'] && $tmpCat == $posts['category_id']){
                            //表示/非表示ありの時で表示->非表示に変える場合
                            if($this->use["display"]=="on" && ($posts["display"] == 2 && $tmpDisp==1)){
                                $data = array('sort' => 99999999);
                                $this->db->where('id', $posts['id']);
                                $this->db->update($this->database, $data);

                                //元のカテゴリーのソート順を変更（変更前のソートより大きいデータを１ずつマイナス）
                                $data2 = $this->db->order_by('sort asc')->get_where($this->database, array("del"=>0, "category_id"=>$tmpCat, "sort >" =>  $tmpSort, "display"=>1));
                                //データがあった場合だけソート処理
                                if(!empty($data2->num_rows())){
                                    foreach ($data2->result() as $row){
                                        $sort_num=$row->sort - 1;
                                        $data = array('sort' => $sort_num);
                                        $this->db->where('id', $row->id);
                                        $this->db->update($this->database, $data);
                                    }
                                }
                                //非表示->表示
                            }else if($this->use["display"]=="on" && ($posts["display"] == 1 && $tmpDisp==2)){
                                $param=array();
                                $param["del"] = 0;
                                $param["category_id"] = $posts["category_id"];
                                $param["display"] = 1;

                                $sort_last = $this->db->get_where($this->database, $param)->num_rows();
                                $data = array('sort' => $sort_last);
                                $this->db->where('id', $posts['id']);
                                $this->db->update($this->database, $data);
                            }

                            //ソート番号の並び替え（新規）
                        }else if(empty($posts['id'])){
                            $param=array();
                            $param["del"] = 0;
                            $param["category_id"] = $posts["category_id"];
                            $param["id !="] = $this->db->insert_id();

                            //表示/非表示ありの場合で非表示の場合はソート番号の変更
                            if($this->use["display"] == "on" && $posts["display"]==2){
                                $id = $this->db->insert_id();
                                $data = array('sort' => 99999999);
                                $this->db->where('id', $id);
                                $this->db->update($this->database, $data);
                            }else{
                                //表示/非表示の設定なしか　表示の場合はカウントアップ
                                $param["display"] = 1;

                                $query = $this->db->order_by('sort asc')->get_where($this->database, $param); ;
                                if(!empty($query)){
                                    foreach ($query->result() as $row){
                                        $sort_num=$row->sort + 1;
                                        $data = array('sort' => $sort_num);
                                        $this->db->where('id', $row->id);
                                        $this->db->update($this->database, $data);
                                    }
                                }
                            }
                        }
                    }

                    $url="";

                    //カテゴリーありの場合
                    if($this->use["category"] == "on"){
                        $url .= $posts["category_id"] . "/";
                    }

                    //表示/非表示ありの場合
                    if($this->use["display"] == "on"){
                        $url .= $posts["display"] . "/";
                    }

                    if(empty($ret)){
                        //data設定
                        $data["render"] = $this->segment."/regist";
                        $data["clean"] = $this->clean;
                        $data["csrf"] = $this->_get_csrf_nonce();
                        $data["msg"] = $error;
                        $data["use"] = $this->use;
                        $data["cat"] = $posts["category_id"];
                        $data["sub_title"] = $this->sub_title;
                        $data["segment"] = $this->segment;

                        $this->load->view("template", $data);
                    }else{
                        //登録成功
                        $this->session->set_userdata(array("msg"=>"登録が完了しました。"));
                        redirect($this->segment."/home/".$url);
                    }
                }
            }
        }
    }

    //////////////////////////////////////////////////////////
    //登録情報削除
    //////////////////////////////////////////////////////////
    function delete()
    {
        $posts = $this->input->post();

        //CSRFチェック
        if ($this->_valid_csrf_nonce() === FALSE) {
            $this->session->set_userdata(array("msg" => "不正な処理が行われました。"));
            redirect($this->segment . "/home");
        }

        $url = "";

        if ($posts['id'] && is_numeric($posts['id'])) {
            $data = array(
                'del' => 1,
                'sort' => 99999999
            );
            $this->db->where('id', $posts['id']);
            $ret = $this->db->update($this->database, $data);

            if ($ret) {
                if ($this->use["sort"] == "on") {

                    //表示/非表示の情報を取得
                    $tmpDisp = "";
                    $tmpDatas = $this->db->get_where($this->database, array('id' => $posts['id']))->row();
                    if (!empty($tmpDatas)) {
                        $tmpDisp = $tmpDatas->display;
                    }

                    //表示/非表示ありの場合で非表示の場合は何もしない
                    if ($this->use["display"] == "on" && $tmpDisp == 2) {
                    } else {
                        //ソート番号の並び替え（カウントダウン）
                        $query = $this->db->order_by('sort asc')->get_where($this->database, array("del" => 0, 'category_id' => $posts['category_id'], 'sort >' => $posts["sort"], "display" => 1));;
                        foreach ($query->result() as $row) {
                            $sort_num = $row->sort - 1;
                            $data = array('sort' => $sort_num);
                            $this->db->where('id', $row->id);
                            $this->db->update($this->database, $data);
                        }
                    }
                }

                //カテゴリーありの場合
                if ($this->use["category"] == "on") {
                    $url .= $posts["category_id"] . "/";
                }

                //表示/非表示ありの場合
                if ($this->use["display"] == "on") {
                    $url .= $posts["display"] . "/";
                }

                //削除成功
                $this->session->set_userdata(array("msg" => "削除が完了しました。"));
                redirect($this->segment . "/home/" . $url);
            } else {
                $this->session->set_userdata(array("msg" => "削除に失敗しました。"));
                redirect($this->segment . "/home/" . $url);
            }
        } else {
            redirect("auth/login");
        }
    }

    //////////////////////////////////////////////////////////
    //公開/非公開
    //////////////////////////////////////////////////////////
    function display()
    {
        $id = $this->uri->segment(3);
        //idチェック
        if (empty($id) || !is_numeric($id)) {
            $this->session->set_userdata(array("msg" => "不正な処理が行われました。"));
            redirect($this->segment . "/home");
        }
        $data = $this->db->get_where($this->database, array('id' => $id))->row();
        $display = $data->display;
        if ($display == 1) {
            $new_display = 2;
        } else {
            $new_display = 1;
        }

        $param = array('display' => $new_display);
        $this->db->where('id', $id);
        $this->db->update($this->database, $param);

        $url="";
        $this->session->set_userdata(array("msg" => "処理が完了しました。"));
        redirect($this->segment . "/home/" . $url);
    }


    //////////////////////////////////////////////////////////
    //プレビュー
    //////////////////////////////////////////////////////////
    function preview()
    {
        $posts = $this->input->post();

        $error = "";
        //オブジェクトへ変換
        $data[$this->segment] = $this->db->get_where($this->database, array('id' => $posts['id']))->row();

        //プレビュー
        //data設定
        $data["render"] = $this->segment . "/preview";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $error;
        $data["use"] = $this->use;
        $data["sub_title"] = $this->sub_title;
        $data["segment"] = $this->segment;

        $this->load->view("template_preview", $data);
    }


    //////////////////////////////////////////////////////////
    //ソート
    //////////////////////////////////////////////////////////
    function sort()
    {
        $posts = $this->input->post();

        //@param($id, $category_id, $sort, $segment, $database, $this->use)
        $result[] = $this->sortChange($posts["id"], $posts["category_id"], $posts["sort"], $this->segment, $this->database);
    }

    //////////////////////////////////////////////////////////
    //バリデーション
    //////////////////////////////////////////////////////////
    private
    function setValidation($validation = null)
    {

        $errors = "";
        $this->form_validation->set_rules("title", "タイトル", "required");
        //$this->form_validation->set_rules("content", "本文","required");

        if ($this->form_validation->run() == FALSE) {
            $errors = validation_errors();
        }
        return $errors;
    }

}