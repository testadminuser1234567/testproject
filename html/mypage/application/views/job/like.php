<div class="title clearfix">
    <div class="midasi">
        <p class="tit">お気に入り </p>
    </div>
    <div class="sibori">

    </div>
</div>
<div class="toukou">
    <a href="<?php echo base_url();?>job/home/">JOB</a>

    <div class="sibori">
        <?php echo $total;?>件
        <p class="page">
            <?php echo $this->pagination->create_links(); ?>
        </p>
    </div>
    <div class="naiyo">
        <p style="color:#FF0000;margin-top:10px;margin-bottom:10px"><?php echo $clean->purify($msg);?></p>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <th style="width:35%">ポジション名</th>
                <th style="width:30%">概要</th>
                <th style="width:30%">年収</th>
                <th style="width:8%">詳細</th>
                <th style="width:8%">応募</th>
                <th style="width:8%">削除</th>
            </tr>
            <?php foreach ($contents->result()  as $val): ?>

                <tr>
                    <td><?php echo str_replace('-','/',$clean->purify($val->job_title));?></td>
                    <td><?php echo $clean->purify($val->background);?></td>
                    <td>
                        <?php //echo $clean->purify($val->area_detail);?>
                        <?php //echo $clean->purify($val->experience);?>
                        <?php //echo $clean->purify($val->qualification);?>
                        <?php //echo $clean->purify($val->worktime);?>
                        <?php //echo $clean->purify($val->holiday);?>
                        <?php //echo $clean->purify($val->benefits);?>
                        <?php echo $clean->purify($val->minsalary);?>～
                        <?php echo $clean->purify($val->maxsalary);?>
                        <?php //echo $clean->purify($val->jobcategory_name);?>
                        <?php //echo $clean->purify($val->prefecture_name);?>
                        <?php //echo $clean->purify($val->j_updated);?>
                        <?php //echo $clean->purify($val->employ_name);?>
                        <?php //echo $clean->purify($val->c_title);?>
                        <?php //echo $clean->purify($val->i_name);?>
                    </td>
                    <td>
                        <form action="<?php echo base_url();?>job/detail/" method="post">
                            <input type="submit" name="submit" value="詳細" />
                            <?php echo form_hidden($csrf); ?>
                            <input type="hidden" name="jobId" class="jobId" value="<?php echo $clean->purify($val->job_id);?>" />
                            <input type="hidden" name="userId" class="userId" value="1" />
                        </form>
                    </td>
                    <td>
                        <div id="applyMsg_<?php echo $clean->purify($val->job_id);?>">
                                <form action="<?php echo base_url();?>job/apply/" method="post" id="applyBtn_<?php echo $clean->purify($val->job_id);?>">
                                    <input type="submit" name="applySubmit" class="apply" value="応募" id="apply_<?php echo $clean->purify($val->job_id);?>" />
                                    <?php echo form_hidden($csrf); ?>
                                    <input type="hidden" name="applyJobId" id="applyJobId_<?php echo $clean->purify($val->job_id);?>" value="<?php echo $clean->purify($val->job_id);?>" />
                                    <input type="hidden" name="applyClientId" id="applyClientId_<?php echo $clean->purify($val->job_id);?>" value="<?php echo $clean->purify($val->client_id);?>" />
                                    <input type="hidden" name="applyUserId" id="applyUserId_<?php echo $clean->purify($val->job_id);?>" value="1" />
                                </form>
                        </div>
                    </td>
                    <td>
                        <form action="<?php echo base_url();?>job/likedel/" method="post">
                            <input type="submit" name="submit" class="like" id="like_<?php echo $clean->purify($val->job_id);?>" value="削除" />
                            <?php echo form_hidden($csrf); ?>
                            <input type="hidden" name="likeId" class="likeId" id="likeId_<?php echo $clean->purify($val->job_id);?>" value="<?php echo $clean->purify($val->like_id);?>" />
                            <input type="hidden" name="jobId" class="jobId" id="likeJobId_<?php echo $clean->purify($val->job_id);?>" value="<?php echo $clean->purify($val->job_id);?>" />
                            <input type="hidden" name="userId" class="userId" id="likeUserId_<?php echo $clean->purify($val->job_id);?>" value="1" />
                        </form>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <script>
        $(".like").click(function() {
            if(confirm("削除をします")) {
                line = $(this).attr("id");
                var like_job_id = line.split("_");

                job_id = "#likeJobId_" + like_job_id[1];
                user_id = "#likeUserId_" + like_job_id[1];
                like_id = "#likeId_" + like_job_id[1];

                // フォームの送信データをAJAXで取得する
                var form_data = {
                    job_id: $(job_id).val(),
                    user_id: $(user_id).val(),
                    like_id: $(like_id).val(),
                    ajax: "1"
                };
                // jQueryのAJAXファンクションを利用
                $.ajax({
                    url: "<?php echo base_url();?>job/likedel/",
                    type: "POST",
                    data: form_data,

                    // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                    success: function (msg) {
                        if(msg!=0){
                            alert("エントリーに失敗しました"+msg);
                        }else{
                            location.href="<?php echo base_url();?>job/like/";
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("お気に入り追加に失敗しました");
                    }
                });
            }
            return false;
        })
    </script>
    <style>

        .registed {
            background: #ebebeb;
            color:#b3b3b3;
        }
        #search{
            display:none;
        }
    </style>

    <script>
        $(".apply").click(function() {
            if(confirm("応募をします")) {
                line = $(this).attr("id");
                var apply_id = line.split( "_" );

                job_id = "#applyJobId_" + apply_id[1];
                client_id = "#applyClientId_" + apply_id[1];
                user_id = "#applyUserId_" + apply_id[1];

                // フォームの送信データをAJAXで取得する
                var form_data = {
                    job_id: $(job_id).val(),
                    user_id: $(user_id).val(),
                    client_id: $(client_id).val(),
                    ajax: "1"
                };
                // jQueryのAJAXファンクションを利用
                $.ajax({
                    url: "<?php echo base_url();?>job/apply/",
                    type: "POST",
                    data: form_data,

                    // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                    success: function(msg){
                        if(msg!=0){
                            alert("エントリーに失敗しました"+msg);
                        }
                        $("#applyBtn_" + apply_id[1]).hide();
                        $("#applyMsg_" + apply_id[1]).text("応募済");
                        $("#likeBtn_" + apply_id[1]).hide();
                        $("#likeMsg_" + apply_id[1]).text("応募済");
                    },
                    error:function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("エントリーに失敗しました code:101");
                    }
                });
            }
            return false;
        })
    </script>