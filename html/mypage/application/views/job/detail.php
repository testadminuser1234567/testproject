<!--suppress ALL -->
<div class="boxwrap">
    <?php if(empty($contents)):?>
        <p>お探しの求人情報はございません。</p>
        <p style = "text-align : center; padding : 20px !important;">
            <a href="./index.html"><input type="image" src="../images/btn_back.png" alt="一覧に戻る"></a>
        </p>
    <?php else:?>
    <!-- 求人詳細 -->
    <p class="headline">求人詳細情報 [JOB<?php echo htmlspecialchars($contents->job_id, ENT_QUOTES, 'UTF-8');?>]</p>
    <table width="650"cellspacing="1" class="normal" border="1">
        <col width=120>
        <col width=610>

        <tr>
            <th colspan="2">
                <?php echo htmlspecialchars($contents->job_title, ENT_QUOTES, 'UTF-8');?>				</th>
        </tr>
        <tr>
            <th>募集背景</th>
            <td><?php echo nl2br(htmlspecialchars($contents->background, ENT_QUOTES, 'UTF-8'));?></td>
        </tr>
        <tr>
            <th>仕事内容</th>
            <td><?php echo nl2br(htmlspecialchars($contents->summary, ENT_QUOTES, 'UTF-8'));?></td>
        </tr>
        <tr>
            <th>応募資格</th>
            <td><?php echo nl2br(htmlspecialchars($contents->qualification, ENT_QUOTES, 'UTF-8'));?>
            </td>
        </tr>
        <tr>
            <th>雇用形態</th>
            <td>
                <?php if(!empty($contents->employ_name)):?>
                    <?php $tmpe = explode(",",$contents->employ_name);?>
                    <?php foreach($tmpe as $ek => $ev):?>
                        <?php echo htmlspecialchars($ev, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpe) > 1):?><br /><?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
            </td>
        </tr>
        <tr>
            <th>想定年収</th>
            <td><?php if(!empty($contents->minsalary)):?><?php echo htmlspecialchars($contents->minsalary, ENT_QUOTES, 'UTF-8');?><?php if(empty($contents->maxsalary)):?>万円<?php else:?>～<?php endif;?><?php endif;?><?php if(!empty($contents->maxsalary)):?><?php echo htmlspecialchars($contents->maxsalary, ENT_QUOTES, 'UTF-8');?>万円&nbsp;<?php endif;?></td>
        </tr>
        <!--
				<tr>
					<th>年収について補足</th>
					<td>
						<?php //if(!empty($contents->free_items[28]['memo)):?>【給与形態メモ】<?php //endif;?><?php //echo nl2br(htmlspecialchars($contents->free_items[28]['memo, ENT_QUOTES, 'UTF-8'));?><?php //if(!empty($contents->free_items[28]['memo)):?><br /><br /><?php //endif;?>
						<?php //if(!empty($contents->income_month_from)):?>【月額:下限】<?php //echo htmlspecialchars($contents->income_month_from, ENT_QUOTES, 'UTF-8');?><?php //endif;?><?php //if(!empty($contents->income_month_to)):?><br>【月額:上限】<?php //echo htmlspecialchars($contents->income_month_to, ENT_QUOTES, 'UTF-8');?><?php //endif;?>
<?php //if(!empty($contents->income_month_from)):?><br><br><?php //endif;?>
<?php //if(!empty($contents->bonus_num)):?>【賞与】<?php //echo htmlspecialchars($contents->bonus_num, ENT_QUOTES, 'UTF-8');?>回、<?php //endif;?><br><?php //if(!empty($contents->bonus_num)):?>【賞与昨年実績】<?php //echo htmlspecialchars($contents->bonus, ENT_QUOTES, 'UTF-8');?><?php //endif;?><?php //if(!empty($contents->bonus_memo)):?><br /><?php //echo nl2br(htmlspecialchars($contents->bonus_memo, ENT_QUOTES, 'UTF-8'));?><?php //endif;?><br></td>
				</tr>
-->
        <tr>
            <th>勤務地</th>
            <td>
                <?php if(!empty($contents->prefecture_name)):?>
                    <?php $tmpp = explode(",",$contents->prefecture_name);?>
                    <?php foreach($tmpp as $pk => $pv):?>
                        <?php echo htmlspecialchars($pv, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpp) > 1):?><br /><?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
            </td>
        </tr>
        <tr>
            <th>勤務地(詳細)</th>
            <td>
                <?php echo nl2br(htmlspecialchars($contents->area_detail, ENT_QUOTES, 'UTF-8'));?>
            </td>
        </tr>
        <!--
				<tr>
					<th>転勤の有無</th>
					<td>
						<?php //if(!empty($contents->free_items[23]['items)):?>
						<?php //foreach($contents->free_items[23]['items as $ek => $ev):?>
						<?php //echo htmlspecialchars($tenkin[$ev], ENT_QUOTES, 'UTF-8');?>
						<?php //endforeach;?>
						<?php //endif;?>
					</td>
				</tr>
				<tr>
					<th>募集年齢</th>
					<td>
						<?php //if($contents->age_limit_type == 0):?>
						<?php //elseif($contents->age_limit_type ==1):?>
							<?php //if(!empty($contents->age_from) || !empty($contents->age_to)):?>
							<?php //echo htmlspecialchars($contents->age_from, ENT_QUOTES, 'UTF-8');?>歳～<?php //echo htmlspecialchars($contents->age_to, ENT_QUOTES, 'UTF-8');?>歳
							<?php //endif;?>
						<?php //else:?>
							<?php //if($contents->age_limit_type == 9):?>年齢不問
							<?php //endif;?>
						<?php //endif;?>
					</td>
				</tr>
				<tr>
					<th>年齢制限の理由</th>
					<td>
						<?php //if(!empty($contents->age_limit_reason)):?>
							<?php //if($contents->age_limit_reason == 1):?>長期勤続によるキャリア形成を図るため新規学卒者等を対象とする
							<?php //elseif($contents->age_limit_reason == 2):?>技能・ノウハウ等の継承の観点から、年齢構成を維持・回復させるため特定年齢層を対象とする
							<?php //elseif($contents->age_limit_reason == 6):?>芸術・芸能の分野における表現の真実性等の要請がある
							<?php //elseif($contents->age_limit_reason == 9):?>行政の施策を踏まえて中高年齢者に限定した募集・採用する
							<?php //elseif($contents->age_limit_reason == 10):?>労働基準法等の法令により、特定の年齢層の労働者の就業等が禁止・制限されている
							<?php //elseif($contents->age_limit_reason == 11):?>定年年齢を上限として当該上限年齢未満の労働者を対象とする
							<?php //endif;?>
						<?php //endif;?>
					</td>
				</tr>
				<tr>
					<th>最終学歴</th>
					<td>
						<?php //if(!empty($contents->free_items[120]['items)):?>
						<?php //foreach($contents->free_items[120]['items as $ek => $ev):?>
						<?php //echo htmlspecialchars($gakureki[$ev], ENT_QUOTES, 'UTF-8');?>
						<?php //endforeach;?>
						<?php //endif;?><br />
						<?php //echo nl2br(htmlspecialchars($contents->free_items[120]['memo, ENT_QUOTES, 'UTF-8'));?>
					</td>
				</tr>
				<tr>
					<th>各種保険</th>
					<td>
						<?php //if(!empty($contents->free_items[33]['items)):?>【保険】<br />
						<?php //foreach($contents->free_items[33]['items as $ek => $ev):?>
						<?php //echo htmlspecialchars($insurence[$ev], ENT_QUOTES, 'UTF-8');?><br />
						<?php //endforeach;?>
						<?php //endif;?><?php //echo htmlspecialchars($contents->free_items[33]['memo, ENT_QUOTES, 'UTF-8');?>
					</td>
				</tr>
-->
        <tr>
            <th>諸手当</th>
            <td><?php echo nl2br(htmlspecialchars($contents->benefits, ENT_QUOTES, 'UTF-8'));?></td>
        </tr>
        <tr>
            <th>休日休暇</th>
            <td><?php echo nl2br(htmlspecialchars($contents->holiday, ENT_QUOTES, 'UTF-8'));?></td>
        </tr>
        <tr>
            <th>勤務時間</th>
            <td><?php echo nl2br(htmlspecialchars($contents->worktime, ENT_QUOTES, 'UTF-8'));?></td>
        </tr>
        <!--
				<tr>
					<th>就業日</th>
					<td>
 						<?php //if(!empty($contents->free_items[134]['items)):?>
						<?php //foreach($contents->free_items[134]['items as $ek => $ev):?>
						<?php //echo htmlspecialchars($working_days[$ev], ENT_QUOTES, 'UTF-8');?>&nbsp;&nbsp;
						<?php //endforeach;?>
						<?php //endif;?><br /><?php e//cho nl2br(htmlspecialchars($contents->free_items[134]['memo, ENT_QUOTES, 'UTF-8'));?>
					</td>
				</tr>
-->
    </table>

    <p style="margin-bottom:0px;font-size:0.85em"><strong>【会社概要】</strong></p>
    <table width="650"cellspacing="1" class="normal" border="1">
        <col width=120>
        <col width=610>
        <!--
				<tr>
					<th>会社名</th>
					<td><?php //echo htmlspecialchars($contents->recruit_company['name, ENT_QUOTES, 'UTF-8');?></td>
				</tr>
-->
        <tr>
            <th>業種</th>
            <td>
                <?php if(!empty($contents->i_name)):?>
                    <?php $tmpp = explode(",",$contents->i_name);?>
                    <?php foreach($tmpp as $pk => $pv):?>
                        <?php echo htmlspecialchars($pv, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpp) > 1):?><br /><?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
            </td>
        </tr>
        <tr>
            <th>事業内容</th>
            <td><?php echo nl2br(htmlspecialchars($contents->c_naiyo, ENT_QUOTES, 'UTF-8'));?></td>
        </tr>
        <!--
				<tr>
					<th>資本金</th>
					<td><?php //echo htmlspecialchars($contents->recruit_company['capital, ENT_QUOTES, 'UTF-8');?>円</td>
				</tr>
-->

        <tr>
            <th>従業員数</th>
            <td><?php echo nl2br(htmlspecialchars($contents->c_employment, ENT_QUOTES, 'UTF-8'));?></td>
        </tr>
        <!--
				<tr>
					<th>事業内容</th>
					<td><?php //echo nl2br(htmlspecialchars($contents->recruit_company['business_summary, ENT_QUOTES, 'UTF-8'));?></td>
				</tr>
				<tr>
					<th>会社の特徴</th>
					<td><?php //echo nl2br(htmlspecialchars($contents->recruit_company['characteristic, ENT_QUOTES, 'UTF-8'));?></td>
				</tr>
-->
    </table>

    <!--
			<table width="650"cellspacing="1" class="normal">
				<col width=120>
				<col width=610>
				<tr>
					<th>コンサルタントから</th>
					<td><?php //echo nl2br(htmlspecialchars($contents->free_items[35]['memo, ENT_QUOTES, 'UTF-8'));?></td>
				</tr>
			</table>
-->
    <?php $flg=0;?>
    <?php if(!empty($processes)):?>
        <?php foreach ($processes["Item"] as $v): ?>
            <?php if($v["Process.P_Job"]["Job"]["Job.P_Id"]==$contents->job_id):?>
                <?php $flg=1;?>
            <?php endif;?>
        <?php endforeach;?>
    <?php endif;?>
    <div style="width:650px;text-align: center;margin-top:30px">
    <table style="margin-left: 250px;">
        <tr>
            <td>
                <div id="applyMsg_<?php echo $clean->purify($contents->job_id);?>">
                    <?php if($flg==1):?>応募済み<?php endif;?>
                <?php if($flg==0):?>
                <form action="<?php echo base_url();?>job/apply/" method="post">
                    <input type="submit" name="applySubmit" class="apply" value="応募" id="apply_<?php echo $clean->purify($contents->job_id);?>" />
                    <?php echo form_hidden($csrf); ?>
                    <input type="hidden" name="applyJobId" id="applyJobId_<?php echo $clean->purify($contents->job_id);?>" value="<?php echo $clean->purify($contents->job_id);?>" />
                    <input type="hidden" name="applyClientId" id="applyClientId_<?php echo $clean->purify($contents->job_id);?>" value="<?php echo $clean->purify($contents->client_id);?>" />
                    <input type="hidden" name="applyUserId" id="applyUserId_<?php echo $clean->purify($contents->job_id);?>" value="1" />
                </form>
            <?php endif;?>
            </div>
            <td>
                <div id="likeMsg_<?php echo $clean->purify($contents->job_id);?>">
                    <?php if($flg==1):?>応募済み<?php endif;?>
                    <?php if($flg==0):?>
                        <form action="<?php echo base_url();?>job/add/" method="post" id="likeBtn_<?php echo $clean->purify($contents->job_id);?>">
                            <input type="submit" name="jobSubmit" class="like<?php if(isset($likes[$contents->job_id])):?> registed<?php endif;?>" value="お気に入り" />
                            <?php echo form_hidden($csrf); ?>
                            <input type="hidden" name="likeJobId" class="likeJobId" value="<?php echo $clean->purify($contents->job_id);?>" />
                            <input type="hidden" name="likeUserId" class="likeUserId" value="1" />
                            <input type="hidden" name="likeRegisted" class="likeResigsted" value="<?php if(isset($likes[$contents->job_id])):?>1<?php endif;?>" />
                            <input type="hidden" name="likeId" class="likeId" value="<?php if(isset($likes[$contents->job_id])):?><?php echo $clean->purify($likes[$contents->job_id]);?><?php endif;?>" />
                        </form>
                    <?php endif;?>
                </div>

            </td>

            </td>
        </tr>
    </table>
            </div>
</div>
<?php endif;?>

<?php if(empty($data)):?>
    </div>
<?php endif;?>
<style>
    #contents{
        padding:30px !important;
    }
</style>
<script>
    $(".like").click(function() {
        idx = $(".like").index(this);

        regist = $(".likeResigsted").eq(idx).val();

        // フォームの送信データをAJAXで取得する
        var form_data = {
            job_id: $(".likeJobId").eq(idx).val(),
            user_id: $(".likeUserId").eq(idx).val(),
            like_id: $(".likeId").eq(idx).val(),
            regist: regist,
            ajax: "1"
        };
        // jQueryのAJAXファンクションを利用
        $.ajax({
            url: "<?php echo base_url();?>job/add/",
            type: "POST",
            data: form_data,

            // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
            success: function(msg){
                if(regist){
                    $(".like").eq(idx).removeClass('registed');
                    $(".likeResigsted").eq(idx).val("");
                    $(".likeId").eq(idx).val("");
                }else{
                    $(".like").eq(idx).addClass('registed');
                    $(".likeResigsted").eq(idx).val(1);
                    $(".likeId").eq(idx).val(msg);
                }
            },
            error:function(XMLHttpRequest, textStatus, errorThrown) {

            }
        });

        return false;
    })
</script>
<style>
    .like {
        background:#FFF;
        color:#555;
    }
    .registed {
        background: #ebebeb;
        color:#b3b3b3;
    }
</style>
<script>
    $(".apply").click(function() {
        if(confirm("応募をします")) {
            line = $(this).attr("id");
            var apply_id = line.split( "_" );

            job_id = "#applyJobId_" + apply_id[1];
            client_id = "#applyClientId_" + apply_id[1];
            user_id = "#applyUserId_" + apply_id[1];

            // フォームの送信データをAJAXで取得する
            var form_data = {
                job_id: $(job_id).val(),
                user_id: $(user_id).val(),
                client_id: $(client_id).val(),
                ajax: "1"
            };
            // jQueryのAJAXファンクションを利用
            $.ajax({
                url: "<?php echo base_url();?>job/apply/",
                type: "POST",
                data: form_data,

                // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                success: function(msg){
                    if(msg!=0){
                        alert("エントリーに失敗しました"+msg);
                    }
                    $("#applyBtn_" + apply_id[1]).hide();
                    $("#applyMsg_" + apply_id[1]).text("応募済");
                    $("#likeBtn_" + apply_id[1]).hide();
//                    $("#likeMsg_" + apply_id[1]).text("応募済");
                },
                error:function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("エントリーに失敗しました code:101");
                }
            });
        }
        return false;
    })
</script>