<div class="title clearfix">
    <div class="midasi">
        <p class="tit">ジョブ </p>
    </div>
    <div class="sibori">

    </div>
</div>
<div class="toukou">
    <a href="<?php echo base_url();?>job/like/">お気に入り</a>

    <form method ="post" name="searchpage" action="<?php echo base_url();?>job/home/" id="search">
        <h3>求人を探す</h3>

        <div class="box">
            <div class="inner clearfix">
                <h4>職種 <span class="ss">※3つまで選択できます</span></h4>
                <div class="list">
                    <label for="job01" class="CheckBoxLabelClass navichecklabel"><input type="checkbox" name="job[]" class="checkbox chk" value="10753,10754,10755,10756,10757,10758,10765,10725" id="job01" <?php if(in_array("10753,10754,10755,10756,10757,10758,10765,10725", (array)$jobs)):?>checked<?php endif;?> />営業</label>
                    <label for="job02" class="CheckBoxLabelClass navichecklabel"><input type="checkbox" name="job[]" class="checkbox chk" value="10775" id="job02" <?php if(in_array("10775", (array)$jobs)):?>checked<?php endif;?> />経理･財務</label>
                    <label for="job03" class="CheckBoxLabelClass navichecklabel"><input type="checkbox" name="job[]" class="checkbox chk" value="10773,10774,10776" id="job03" <?php if(in_array("10773,10774,10776", (array)$jobs)):?>checked<?php endif;?> />法務･人事･総務･広報</label>
                    <label for="job04" class="CheckBoxLabelClass navichecklabel"><input type="checkbox" name="job[]" class="checkbox chk" value="10777,10762" id="job04" <?php if(in_array("10777,10762", (array)$jobs)):?>checked<?php endif;?> />一般事務</label>
                    <label for="job05" class="CheckBoxLabelClass navichecklabel"><input type="checkbox" name="job[]" class="checkbox chk" value="10768,10769" <?php if(in_array("10768,10769", (array)$jobs)):?>checked<?php endif;?> id="job05" />経営･管理職･企画･マーケティング</label>
                    <label for="job06" class="CheckBoxLabelClass navichecklabel"><input type="checkbox" name="job[]" class="checkbox chk" value="10734,10735,10736,10737,10738,10739,10740,10741,10742,10743,11290" <?php if(in_array("10734,10735,10736,10737,10738,10739,10740,10741,10742,10743,11290", (array)$jobs)):?>checked<?php endif;?> id="job06">技術職(電気･電子･機械)</label>
                    <label for="job07" class="CheckBoxLabelClass navichecklabel"><input type="checkbox" name="job[]" class="checkbox chk" value="10779,10780,10781,10782,10783" <?php if(in_array("10779,10780,10781,10782,10783", (array)$jobs)):?>checked<?php endif;?> id="job07">技術職(医療･化学･食品)</label>
                    <label for="job08" class="CheckBoxLabelClass navichecklabel"><input type="checkbox" name="job[]" class="checkbox chk" value="10749,10750,10751" <?php if(in_array("10749,10750,10751", (array)$jobs)):?>checked<?php endif;?> id="job08">技術職(建築･土木･その他)</label>
                    <label for="job09" class="CheckBoxLabelClass navichecklabel"><input type="checkbox" name="job[]" class="checkbox chk" value="10716,10717,10718,10719,10720,10721,10722,10723,10724" <?php if(in_array("10716,10717,10718,10719,10720,10721,10722,10723,10724", (array)$jobs)):?>checked<?php endif;?> id="job09">ITエンジニア(システム開発･インフラ･SE)</label>
                    <label for="job10" class="CheckBoxLabelClass navichecklabel"><input type="checkbox" name="job[]" class="checkbox chk" value="10727,10728,10729,10730,10731,10732" <?php if(in_array("10727,10728,10729,10730,10731,10732", (array)$jobs)):?>checked<?php endif;?> id="job10">Web関連職･メディア･ゲーム･デザイン</label>
                    <label for="job11" class="CheckBoxLabelClass navichecklabel"><input type="checkbox" name="job[]" class="checkbox chk" value="10770" <?php if(in_array("10770", (array)$jobs)):?>checked<?php endif;?> id="job11">専門職(士業･金融･不動産･コンサルタント)</label>
                    <label for="job12" class="CheckBoxLabelClass navichecklabel"><input type="checkbox" name="job[]" class="checkbox chk" value="10760,10761,10762" <?php if(in_array("10760,10761,10762", (array)$jobs)):?>checked<?php endif;?> id="job12">サービス･外食･販売</label>
                    <label for="job13" class="CheckBoxLabelClass navichecklabel"><input type="checkbox" name="job[]" class="checkbox chk" value="10771,10766" <?php if(in_array("10771,10766", (array)$jobs)):?>checked<?php endif;?> id="job13">教育･保育･物流･購買･その他 </label>

                </div>
            </div>
        </div>

        <div class="box fl">
            <div class="inner clearfix">
                <h4>勤務地<span class="ss"></span></h4>
                <div class="list">
                    <label for="pref01" class="radiolabel naviprelabel">
                        <input type="radio" class="radio prefecture" name="pref" value="76" id="pref01" <?php if($pref==76):?>checked<?php endif;?> />
                        青森県</label>
                    <label for="pref02" class="radiolabel naviprelabel">
                        <input type="radio" class="radio prefecture" name="pref" value="77" id="pref02" <?php if($pref==77):?>checked<?php endif;?> />
                        岩手県</label>
                    <label for="pref03" class="radiolabel naviprelabel">
                        <input type="radio" class="radio prefecture" name="pref" value="78" id="pref03" <?php if($pref==78):?>checked<?php endif;?> />
                        宮城県</label>
                    <label for="pref04" class="radiolabel naviprelabel">
                        <input type="radio" class="radio prefecture" name="pref" value="79" id="pref04" <?php if($pref==79):?>checked<?php endif;?> />
                        秋田県</label><br>
                    <label for="pref05" class="radiolabel naviprelabel">
                        <input type="radio" class="radio prefecture" name="pref" value="80" id="pref05" <?php if($pref==80):?>checked<?php endif;?> />
                        山形県</label>
                    <label for="pref06" class="radiolabel naviprelabel">
                        <input type="radio" class="radio prefecture" name="pref" value="81" id="pref06" <?php if($pref==81):?>checked<?php endif;?> />
                        福島県</label>
                    <select name="pref2" id="pref2">
                        <option value="">それ以外の地域</option>
                        <option value="75" <?php if($pref2==75):?>selected<?php endif;?> >北海道</option>
                        <option value="82" <?php if($pref2==82):?>selected<?php endif;?> >茨城県</option>
                        <option value="83" <?php if($pref2==83):?>selected<?php endif;?> >栃木県</option>
                        <option value="84" <?php if($pref2==84):?>selected<?php endif;?> >群馬県</option>
                        <option value="85" <?php if($pref2==85):?>selected<?php endif;?> >埼玉県</option>
                        <option value="86" <?php if($pref2==86):?>selected<?php endif;?> >千葉県</option>
                        <option value="87" <?php if($pref2==87):?>selected<?php endif;?> >東京都</option>
                        <option value="88" <?php if($pref2==88):?>selected<?php endif;?> >神奈川県</option>
                        <option value="89" <?php if($pref2==89):?>selected<?php endif;?> >新潟県</option>
                        <option value="90" <?php if($pref2==90):?>selected<?php endif;?> >富山県</option>
                        <option value="91" <?php if($pref2==91):?>selected<?php endif;?> >石川県</option>
                        <option value="92" <?php if($pref2==92):?>selected<?php endif;?> >福井県</option>
                        <option value="93" <?php if($pref2==93):?>selected<?php endif;?> >山梨県</option>
                        <option value="94" <?php if($pref2==94):?>selected<?php endif;?> >長野県</option>
                        <option value="95" <?php if($pref2==95):?>selected<?php endif;?> >岐阜県</option>
                        <option value="96" <?php if($pref2==96):?>selected<?php endif;?> >静岡県</option>
                        <option value="97" <?php if($pref2==97):?>selected<?php endif;?> >愛知県</option>
                        <option value="98" <?php if($pref2==98):?>selected<?php endif;?> >三重県</option>
                        <option value="99" <?php if($pref2==99):?>selected<?php endif;?> >滋賀県</option>
                        <option value="100" <?php if($pref2==100):?>selected<?php endif;?> >京都府</option>
                        <option value="101" <?php if($pref2==101):?>selected<?php endif;?> >大阪府</option>
                        <option value="102" <?php if($pref2==102):?>selected<?php endif;?> >兵庫県</option>
                        <option value="103" <?php if($pref2==103):?>selected<?php endif;?> >奈良県</option>
                        <option value="104" <?php if($pref2==104):?>selected<?php endif;?> >和歌山県</option>
                        <option value="105" <?php if($pref2==105):?>selected<?php endif;?> >鳥取県</option>
                        <option value="106" <?php if($pref2==106):?>selected<?php endif;?> >島根県</option>
                        <option value="107" <?php if($pref2==107):?>selected<?php endif;?> >岡山県</option>
                        <option value="108" <?php if($pref2==108):?>selected<?php endif;?> >広島県</option>
                        <option value="109" <?php if($pref2==109):?>selected<?php endif;?> >山口県</option>
                        <option value="110" <?php if($pref2==110):?>selected<?php endif;?> >徳島県</option>
                        <option value="111" <?php if($pref2==111):?>selected<?php endif;?> >香川県</option>
                        <option value="112" <?php if($pref2==112):?>selected<?php endif;?> >愛媛県</option>
                        <option value="113" <?php if($pref2==113):?>selected<?php endif;?> >高知県</option>
                        <option value="114" <?php if($pref2==114):?>selected<?php endif;?> >福岡県</option>
                        <option value="115" <?php if($pref2==115):?>selected<?php endif;?> >佐賀県</option>
                        <option value="116" <?php if($pref2==116):?>selected<?php endif;?> >長崎県</option>
                        <option value="117" <?php if($pref2==117):?>selected<?php endif;?> >熊本県</option>
                        <option value="118" <?php if($pref2==118):?>selected<?php endif;?> >大分県</option>
                        <option value="119" <?php if($pref2==119):?>selected<?php endif;?> >宮崎県</option>
                        <option value="120" <?php if($pref2==120):?>selected<?php endif;?> >鹿児島県</option>
                        <option value="121" <?php if($pref2==121):?>selected<?php endif;?> >沖縄県</option>
                        <option value="74" <?php if($pref2==74):?>selected<?php endif;?> >海外</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="box fl">
            <div class="inner clearfix">
                <h4>年収</h4>
                <div class="list">
                    <select name="year_income" id="year_income">
                        <option value=""  selected>選択してください</option>
                        <option value="100" <?php if($year_income==100):?>selected<?php endif;?> >100万円</option>
                        <option value="200" <?php if($year_income==200):?>selected<?php endif;?> >200万円</option>
                        <option value="300" <?php if($year_income==300):?>selected<?php endif;?> >300万円</option>
                        <option value="400" <?php if($year_income==400):?>selected<?php endif;?> >400万円</option>
                        <option value="500" <?php if($year_income==500):?>selected<?php endif;?> >500万円</option>
                        <option value="600" <?php if($year_income==600):?>selected<?php endif;?> >600万円</option>
                        <option value="700" <?php if($year_income==700):?>selected<?php endif;?> >700万円</option>
                        <option value="800" <?php if($year_income==800):?>selected<?php endif;?> >800万円</option>
                        <option value="900" <?php if($year_income==900):?>selected<?php endif;?> >900万円</option>
                        <option value="1000" <?php if($year_income==1000):?>selected<?php endif;?> >1000万円</option>
                    </select>
                    　以上
                </div>
            </div>
        </div>
        <div class="box fl">
            <div class="inner clearfix">
                <h4>フリーワード</h4>
                <div class="list">
                    <input name="keyword" type="text" class="txtM" placeholder="入力例：正社員　経理"  style="width:100%;" value="<?php echo htmlspecialchars($keyword,ENT_QUOTES,'UTF-8');?>" /><br />

                    <label for="or" class="radiolabel">
                        <input type="radio" class="radio" name="keyword_flg" id="or" value="or" <?php if($keyword_flg=="or"):?>checked<?php endif;?>>
                        いずれかの文字を含む</label>
                    <label for="and" class="radiolabel">
                        <input type="radio" class="radio" name="keyword_flg" id="and" value="and" <?php if($keyword_flg=="and"):?>checked<?php endif;?>>
                        すべての文字を含む</label>
                </div>
            </div>
        </div>
        <div class="box">
            <div class="inner clearfix">
                <p class="send"><input class="image" type="submit" value="この条件で検索する"></p>
            </div>
        </div>
    </form>

    <div class="sibori">
        <?php echo $total;?>件
        <p class="page">
            <?php echo $this->pagination->create_links(); ?>
        </p>
    </div>
    <div class="naiyo">
        <p style="color:#FF0000;margin-top:10px;margin-bottom:10px"><?php echo $clean->purify($msg);?></p>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <th style="width:35%">ポジション名</th>
                <th style="width:30%">概要</th>
                <th style="width:30%">年収</th>
                <th style="width:8%">詳細</th>
                <th style="width:8%">お気に入り</th>
                <th style="width:8%">応募</th>
            </tr>
            <?php foreach ($contents->result()  as $val): ?>
                <?php $flg=0;?>
                <?php if(!empty($processes)):?>
                    <?php foreach ($processes["Item"] as $v): ?>
                        <?php if($v["Process.P_Job"]["Job"]["Job.P_Id"]==$val->job_id):?>
                            <?php $flg=1;?>
                        <?php endif;?>
                    <?php endforeach;?>
                <?php endif;?>
                <tr>
                    <td><?php echo str_replace('-','/',$clean->purify($val->job_title));?></td>
                    <td><?php echo $clean->purify($val->background);?></td>
                    <td>
                        <?php //echo $clean->purify($val->area_detail);?>
                        <?php //echo $clean->purify($val->experience);?>
                        <?php //echo $clean->purify($val->qualification);?>
                        <?php //echo $clean->purify($val->worktime);?>
                        <?php //echo $clean->purify($val->holiday);?>
                        <?php //echo $clean->purify($val->benefits);?>
                        <?php echo $clean->purify($val->minsalary);?>～
                        <?php echo $clean->purify($val->maxsalary);?>
                        <?php //echo $clean->purify($val->jobcategory_name);?>
                        <?php //echo $clean->purify($val->prefecture_name);?>
                        <?php //echo $clean->purify($val->j_updated);?>
                        <?php //echo $clean->purify($val->employ_name);?>
                        <?php //echo $clean->purify($val->c_title);?>
                        <?php //echo $clean->purify($val->i_name);?>
                    </td>
                    <td>
                        <form action="<?php echo base_url();?>job/detail/" method="post">
                            <input type="submit" name="submit" value="詳細" />
                            <?php echo form_hidden($csrf); ?>
                            <input type="hidden" name="jobId" class="jobId" value="<?php echo $clean->purify($val->job_id);?>" />
                            <input type="hidden" name="userId" class="userId" value="1" />
                        </form>
                    </td>
                    <td>
                        <div id="likeMsg_<?php echo $clean->purify($val->job_id);?>">
                        <?php if($flg==1):?>応募済み<?php endif;?>
                        <?php if($flg==0):?>
                        <form action="<?php echo base_url();?>job/add/" method="post" id="likeBtn_<?php echo $clean->purify($val->job_id);?>">
                            <input type="submit" name="jobSubmit" class="like<?php if(isset($likes[$val->job_id])):?> registed<?php endif;?>" value="お気に入り" />
                            <?php echo form_hidden($csrf); ?>
                            <input type="hidden" name="likeJobId" class="likeJobId" value="<?php echo $clean->purify($val->job_id);?>" />
                            <input type="hidden" name="likeUserId" class="likeUserId" value="1" />
                            <input type="hidden" name="likeRegisted" class="likeResigsted" value="<?php if(isset($likes[$val->job_id])):?>1<?php endif;?>" />
                            <input type="hidden" name="likeId" class="likeId" value="<?php if(isset($likes[$val->job_id])):?><?php echo $clean->purify($likes[$val->job_id]);?><?php endif;?>" />
                        </form>
                        <?php endif;?>
                        </div>
                    </td>
                    <td>
                        <div id="applyMsg_<?php echo $clean->purify($val->job_id);?>">
                            <?php if($flg==1):?>応募済み<?php endif;?>
                            <?php if($flg==0):?>
                            <form action="<?php echo base_url();?>job/apply/" method="post" id="applyBtn_<?php echo $clean->purify($val->job_id);?>">
                                <input type="submit" name="applySubmit" class="apply" value="応募" id="apply_<?php echo $clean->purify($val->job_id);?>" />
                                <?php echo form_hidden($csrf); ?>
                                <input type="hidden" name="applyJobId" id="applyJobId_<?php echo $clean->purify($val->job_id);?>" value="<?php echo $clean->purify($val->job_id);?>" />
                                <input type="hidden" name="applyClientId" id="applyClientId_<?php echo $clean->purify($val->job_id);?>" value="<?php echo $clean->purify($val->client_id);?>" />
                                <input type="hidden" name="applyUserId" id="applyUserId_<?php echo $clean->purify($val->job_id);?>" value="1" />
                            </form>
                        <?php endif;?>
                            </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
    <script>
        $(".like").click(function() {
            idx = $(".like").index(this);

            regist = $(".likeResigsted").eq(idx).val();

            // フォームの送信データをAJAXで取得する
            var form_data = {
                job_id: $(".likeJobId").eq(idx).val(),
                user_id: $(".likeUserId").eq(idx).val(),
                like_id: $(".likeId").eq(idx).val(),
                regist: regist,
                ajax: "1"
            };
            // jQueryのAJAXファンクションを利用
            $.ajax({
                url: "<?php echo base_url();?>job/add/",
                type: "POST",
                data: form_data,

                // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                success: function(msg){
                    if(regist){
                        $(".like").eq(idx).removeClass('registed');
                        $(".likeResigsted").eq(idx).val("");
                        $(".likeId").eq(idx).val("");
                    }else{
                        $(".like").eq(idx).addClass('registed');
                        $(".likeResigsted").eq(idx).val(1);
                        $(".likeId").eq(idx).val(msg);
                    }
                },
                error:function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("お気に入り追加に失敗しました");
                }
            });

            return false;
        })
    </script>
    <style>
        .like {
            background:#FFF;
            color:#555;
        }
        .registed {
            background: #ebebeb;
            color:#b3b3b3;
        }
        #search{
            display:none;
        }
    </style>

    <script>
        $(".apply").click(function() {
            if(confirm("応募をします")) {
                line = $(this).attr("id");
                var apply_id = line.split( "_" );

                job_id = "#applyJobId_" + apply_id[1];
                client_id = "#applyClientId_" + apply_id[1];
                user_id = "#applyUserId_" + apply_id[1];

                // フォームの送信データをAJAXで取得する
                var form_data = {
                    job_id: $(job_id).val(),
                    user_id: $(user_id).val(),
                    client_id: $(client_id).val(),
                    ajax: "1"
                };
                // jQueryのAJAXファンクションを利用
                $.ajax({
                    url: "<?php echo base_url();?>job/apply/",
                    type: "POST",
                    data: form_data,

                    // url, POSTデータ, form_dataの取得に成功したら、mgsファンクションを実行する
                    success: function(msg){
                        if(msg!=0){
                            alert("エントリーに失敗しました"+msg);
                        }
                        $("#applyBtn_" + apply_id[1]).hide();
                        $("#applyMsg_" + apply_id[1]).text("応募済");
                        $("#likeBtn_" + apply_id[1]).hide();
                        $("#likeMsg_" + apply_id[1]).text("応募済");
                    },
                    error:function(XMLHttpRequest, textStatus, errorThrown) {
                        alert("エントリーに失敗しました code:101");
                    }
                });
            }
            return false;
        })
    </script>