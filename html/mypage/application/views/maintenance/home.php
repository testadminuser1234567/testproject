<?php
function esc($str){
    return htmlspecialchars($str,ENT_QUOTES,"UTF-8");
}
?>
    <section class="co">
        <div class="inner">
            <h1><span class="line">メンテナンスのご案内</span></h1>
<p>ただ今、マイページはメンテナンス中です。<br>
下記日時にて実施しておりますので、メンテナンス終了まで今しばらくお待ちくださいませ。</p>
<div class="cautionBox">
<?php $week = array("日", "月", "火", "水", "木", "金", "土");?>
<?php
$year1  = substr($start,0,4);
$month1 = substr($start,5,2);
$day1 = substr($start,8,2);
$time1 = substr($start,10,6);
$datetime = new DateTime();
$datetime->setDate($year1, $month1, $day1);
$weekList = array("日", "月", "火", "水", "木", "金", "土");
$w = (int)$datetime->format('w');


$year2  = substr($end,0,4);
$month2 = substr($end,5,2);
$day2 = substr($end,8,2);
$time2 = substr($end,10,6);
$datetime2 = new DateTime();
$datetime2->setDate($year2, $month2, $day2);
$w2 = (int)$datetime2->format('w');
?>
<p>メンテナンス時間：<?php echo esc($year1);?>/<?php echo esc($month1);?>/<?php echo esc($day1);?>(<?php echo $weekList[$w];?>)  <?php echo esc($time1);?>～ <?php echo esc($year2);?>/<?php echo esc($month2);?>/<?php echo esc($day2);?>(<?php echo $weekList[$w2];?>)  <?php echo esc($time2);?><br>
お急ぎの場合は<a href="mailto:info@hurex.co.jp">info@hurex.co.jp</a>までご連絡ください。</p>
</div>
<p>ご不便をお掛け致しますがご了承の程何卒よろしくお願いいたします。</p>
        </div>
    </section>