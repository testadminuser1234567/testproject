<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);
$job_ary=array();
if(!empty($tmpjob['Item'])){
    foreach($tmpjob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $job_ary as $key => $value) {
        $sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($sort, SORT_ASC, $job_ary);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);
$tantou_job_ary=array();
if(!empty($tmptantoujob['Item'])){
    foreach($tmptantoujob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $tantou_job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $tantou_job_ary as $key => $value) {
        $tantou_sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($tantou_sort, SORT_ASC, $tantou_job_ary);

$start=date('Y');
$end=$start-65;
?>
<script src="<?php echo base_url();?>js/form_validation_login.js"></script>
<h1 class="no">現在の希望条件</h1>
<!-- co start -->
<div id="co">
    <div id="coSub" class="clearfix">
        <main id="mainCo">
            <!-- conditionList start -->
            <div id="conditionList">
                <div class="inner">
                    <h2>希望勤務地</h2>
                    <table class="place">
                        <tbody>
                        <tr>
                            <th>第１希望<br />
                            </th>
                            <td>
                                <?php $tflg=0;?>
                                <?php if(!empty($tmparea['Item'])):?>
                                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                                        <?php if(!empty($v['Item'])):?>
                                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                                <?php if(!empty($v2["Items"]['Item'])):?>
                                                    <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                                        <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                            <?php if(@${$segment}->kibouarea1==$v3['Option.P_Id']):?><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
                                                        <?php else:?>
                                                            <?php if($tflg==0):?>
                                                                <?php if(@${$segment}->kibouarea1==$v2['Items']['Item']['Option.P_Id']):?><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
                                                                <?php $tflg=1;?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </td>
                        </tr>

                        <tr>
                            <th>第２希望<br />
                            </th>
                            <td>
                                <?php $tflg=0;?>
                                <?php if(!empty($tmparea['Item'])):?>
                                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                                        <?php if(!empty($v['Item'])):?>
                                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                                <?php if(!empty($v2["Items"]['Item'])):?>
                                                    <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                                        <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                            <?php if(@${$segment}->kibouarea2==$v3['Option.P_Id']):?><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
                                                        <?php else:?>
                                                            <?php if($tflg==0):?>
                                                                <?php if(@${$segment}->kibouarea2==$v2['Items']['Item']['Option.P_Id']):?><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
                                                                <?php $tflg=1;?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </td>
                        </tr>
                        <tr>
                            <th>第３希望<br />
                            </th>
                            <td>
                                <?php $tflg=0;?>
                                <?php if(!empty($tmparea['Item'])):?>
                                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                                        <?php if(!empty($v['Item'])):?>
                                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                                <?php if(!empty($v2["Items"]['Item'])):?>
                                                    <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                                        <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                            <?php if(@${$segment}->kibouarea3==$v3['Option.P_Id']):?><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
                                                        <?php else:?>
                                                            <?php if($tflg==0):?>
                                                                <?php if(@${$segment}->kibouarea3==$v2['Items']['Item']['Option.P_Id']):?><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
                                                                <?php $tflg=1;?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <h2>希望職種</h2>
                    <div class="txtBox"><p><?php if(!empty($job_ary)):?><?php foreach($job_ary as $k=>$v):?><?php if(!empty(${$segment}->kiboujob)):?><?php foreach(${$segment}->kiboujob as $jk=>$jv):?><?php if($jv==$v["Option.P_Id"]):?><?php echo str_replace("　","",htmlspecialchars($v['Option.P_Name'],ENT_QUOTES,'UTF-8'));?><?php if($jk!=count(${$segment}->kiboujob)-1):?>、<?php endif;?><?php endif;?><?php endforeach;?><?php endif;?><?php endforeach;?><?php endif;?></p></div>
                    <h2>希望年収</h2>
                    <div class="txtBox">
                        <p>                   <?php if(!empty($tmpincome['Item'])):?>
                                <?php foreach($tmpincome['Item'] as $k=>$v):?>
                                    <?php if(!empty($v['Item'])):?>
                                        <?php foreach($v['Item'] as $k2=>$v2):?>
                                            <?php if($v2["Option.P_Id"]==@${$segment}->income):?><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php else:?>
                                指定なし
                            <?php endif;?></p>
                    </div>
                    <h2>転勤可否</h2>
                    <?php $tenkinVal="";?>
                    <?php if(!empty($tmptenkin['Item'])):?><?php foreach($tmptenkin['Item'] as $k=>$v):?>
                        <?php if(!empty($v['Item'])):?><?php foreach($v['Item'] as $k2=>$v2):?><?php if(in_array($v2['Option.P_Name'], @${$segment}->tenkin, true)):?><?php $tenkinVal .= str_replace(array("　"," "),"",htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8')) . "、";?><?php endif;?><?php endforeach;?><?php endif;?><?php endforeach;?><?php endif;?>
                    <div class="txtBox">
                        <p><?php echo rtrim($tenkinVal,"、");?></p>
                    </div>
                    <div class="aCenter">
                    <p class="btn"><a href="javascript:void(0);" onclick="document.frm.submit();return false;"><span>希望条件を変更</span></a></p>
                    </div>
                    <?php $attributes = array('name' => 'frm', 'id' => 'frm');?>
                    <?php echo form_open('condition/create/',$attributes);?>
                    <p class="btnSend aCenter">
                        <input type="hidden" value="希望条件を編集する" onclick="return check(1);">
                    </p>
                    <?php echo form_close();?>
                </div>
            </div>
            <!-- conditionListt end -->
        </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($looks)):?>
                <div id="historyBox">
                    <h2><span>最近閲覧した求人</span></h2>
                    <div class="inner">
                        <?php foreach($looks as $k=>$v):?>
                            <!-- entry start -->
                            <div class="entry">
                                <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                            </div>
                            <!-- entry end -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
            <!-- historyBox end -->



