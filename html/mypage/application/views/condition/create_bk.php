<?php
//HRBCマスターと連動

//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

?>
<div class="title clearfix">
<p class="tit2">希望条件を登録する</p>
<div class="naiyoToko">
<br/>
<p class="txt">▼下記の項目をご記入下さい。</p>
<div class="error"><?php echo $msg ?></div>
    <?php echo form_open_multipart('condition/confirm');?>
<table border="0" cellspacing="0" cellpadding="0">

    <table border="0" cellspacing="0" cellpadding="0">
 
<tr class="career">
<th colspan="2" class="title">■希望条件</th>
</tr>

<tr>
<th class="<?php if(@${$segment}->kiboujob):?>ok<?php else:?>hissu<?php endif;?>">職種</th>
<td>
<?php if(!empty($tmpjob['Item'])):?>
<?php foreach($tmpjob['Item'] as $k=>$v):?>
<?php if(!empty($v['Item'])):?>
<?php foreach($v['Item'] as $k2=>$v2):?>
<ul>
<li>
<input type="checkbox" class="checkbox jobcheckbox<?php if($k2-1==count($tmpjob['Item'])):?> required<?php endif;?>" name="kiboujob[]" value="<?php echo htmlspecialchars($v2['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty(${$segment}->kiboujob)):?><?php foreach(${$segment}->kiboujob as $jk=>$jv):?><?php if($jv==$v2["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?> id="cat_job0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>">
<label for="cat_job0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>" class="checkboxlabel joblabel">
<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>全て
</label>

<?php if(!empty($v2["Items"]['Item'])):?>
<ul>
<?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
<li>
<input type="checkbox" class="checkbox jobcheckbox<?php if($k3-1==count($tmpjob['Item'])):?> required<?php endif;?>" name="kiboujob[]" value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty(${$segment}->kiboujob)):?><?php foreach(${$segment}->kiboujob as $jk2=>$jv2):?><?php if($jv2==$v3["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?> id="sub_job0<?php echo htmlspecialchars($k3,ENT_QUOTES,'UTF-8');?>">
<label for="sub_job0<?php echo htmlspecialchars($k3,ENT_QUOTES,'UTF-8');?>" class="checkboxlabel joblabel">
<?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
</label>
</li>
<?php endforeach;?>
</ul>
<?php endif;?>
</li>
</ul>
<hr>
<?php endforeach;?>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>
</td>


        <tr>
            <th class="<?php if(@${$segment}->income):?>ok<?php else:?>hissu<?php endif;?> income">希望年収</th>
            <td>
                <?php echo form_input('income', @${$segment}->income);?>万円
            </td>
        </tr>

       <tr>
            <th class="<?php if(@${$segment}->tenkin):?>ok<?php else:?>hissu<?php endif;?> tenkin">転勤</th>
            <td>
<?php if(!empty($tmptenkin['Item'])):?>
<?php foreach($tmptenkin['Item'] as $k=>$v):?>
<?php if(!empty($v['Item'])):?>
<?php foreach($v['Item'] as $k2=>$v2):?>
<label for="tenkin0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>" class="checkboxlabel tenkinlabel">
<input type="checkbox" class="checkbox tenkincheckbox<?php if($k2-1==count($tmptenkin['Item'])):?> required<?php endif;?>" name="tenkin[]" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty(${$segment}->tenkin)):?><?php if(in_array($v2['Option.P_Name'], @${$segment}->tenkin, true)):?>checked<?php endif;?><?php endif;?> id="tenkin0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>">
<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
</label>
<?php endforeach;?>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>
</td>
        </tr>



        <tr>
            <th class="<?php if(@${$segment}->expectarea1):?>ok<?php else:?>hissu<?php endif;?> expectarea1">勤務地<br />
                （第1希望）</th>
            <td>

<?php if(!empty($tmparea['Item'])):?>
<?php foreach($tmparea['Item'] as $k=>$v):?>
<?php if(!empty($v['Item'])):?>
<?php foreach($v['Item'] as $k2=>$v2):?>
<ul>
<li>
<input type="checkbox" class="checkbox areacheckbox<?php if($k2-1==count($tmparea['Item'])):?> required<?php endif;?>" name="kibouarea[]" value="<?php echo htmlspecialchars($v2['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty(${$segment}->kibouarea)):?><?php foreach(${$segment}->kibouarea as $jk=>$jv):?><?php if($jv==$v2["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?> id="cat_area0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>">
<label for="cat_area0<?php echo htmlspecialchars($k2,ENT_QUOTES,'UTF-8');?>" class="checkboxlabel arealabel">
<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
</label>

<?php if(!empty($v2["Items"]['Item'])):?>
<ul>
<?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
<li>
<input type="checkbox" class="checkbox areacheckbox<?php if($k3-1==count($tmparea['Item'])):?> required<?php endif;?>" name="kibouarea[]" value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty(${$segment}->kibouarea)):?><?php foreach(${$segment}->kibouarea as $jk2=>$jv2):?><?php if($jv2==$v3["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?> id="sub_area0<?php echo htmlspecialchars($k3,ENT_QUOTES,'UTF-8');?>">
<label for="sub_area0<?php echo htmlspecialchars($k3,ENT_QUOTES,'UTF-8');?>" class="checkboxlabel arealabel">
<?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
</label>
</li>
<?php endforeach;?>
</ul>
<?php endif;?>
</li>
</ul>
<hr>
<?php endforeach;?>
<?php endif;?>
<?php endforeach;?>
<?php endif;?>
	</td>
        </tr>

        <tr>
            <th class="<?php if(@${$segment}->expectarea2):?>ok<?php else:?><?php endif;?> expectarea2">勤務地<br />
                （第2希望）</th>
            <td>

                <select id="expectarea2" name="expectarea2" class="" style="">
                    <option value="" selected="selected" label="">お選びください</option>
                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                        <?php foreach($v['Item'] as $k2=>$v2):?>
                            <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                <option value="<?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->expectarea2==$v3['Option.P_Name']):?>selected<?php endif;?> ><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                <?php print_r($v3['Option.P_Name']);?>
                            <?php endforeach;?>
                        <?php endforeach;?>
                    <?php endforeach;?>
                </select></td>
        </tr>

        <tr>
            <th class="<?php if(@${$segment}->expectarea3):?>ok<?php else:?><?php endif;?> expectarea3">勤務地<br />
                （第3希望）</th>
            <td>

                <select id="expectarea3" name="expectarea3" class="" style="">
                    <option value="" selected="selected" label="">お選びください</option>
                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                        <?php foreach($v['Item'] as $k2=>$v2):?>
                            <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                <option value="<?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->expectarea3==$v3['Option.P_Name']):?>selected<?php endif;?> ><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                <?php print_r($v3['Option.P_Name']);?>
                            <?php endforeach;?>
                        <?php endforeach;?>
                    <?php endforeach;?>
                </select></td>
        </tr>


 
</div>
</div>

            </td>
        </tr>
        </table>
    </div>
</div>
<div class="toukou noLine clearfix">
<p class="b" style="text-align:center">
<?php echo form_hidden($csrf); ?>
<input type="submit" value="確認する" style="width: 150px; height: 30px;font-weight:bold;" onclick="return check(1);"  />
</div>
</div>
<?php echo form_close();?>

<script>
    /*
    function check(param){
        e1 = $("#expectarea1").val();
        e2 = $("#expectarea2").val();
        e3 = $("#expectarea3").val();

        if(e1 && e2){
            if(e1 == e2){
                $("#expectarea1").parent().append("<div class='error'>勤務地（第1希望）と勤務地（第2希望）は別の都道府県を選択してください。。</div>");
            }
        }
        if(e1 && e3){
            if(e1 == e3){
                $("#expectarea1").parent().append("<div class='error'>勤務地（第1希望）と勤務地（第3希望）は別の都道府県を選択してください。。</div>");
            }
        }
        if(e2 && e3){
            if(e2 == e3){
                $("#expectarea2").parent().append("<div class='error'>勤務地（第2希望）と勤務地（第3希望）は別の都道府県を選択してください。。</div>");
            }
        }
        //エラー処理
        if($("div.error").size() > 0){
            $('html,body').animate({ scrollTop: $("div.error:first").offset().top-150}, 'slow');
            return false;
        }else{
//            return true;
            return false;
        }
    }
    */

//チェンジイベント
$(function(){
    $('ul input[type="checkbox"]').change(function(){
        if ($(this).is(':checked')) {
            $(this).parent().find('input[type="checkbox"]').prop('checked', true);
        }
        else {
            $(this).parent().find('input[type="checkbox"]').prop('checked', false);
            $(this).parents('li').each(function(){
                $(this).children('input[type="checkbox"]').prop('checked', false);
            });
        }
    });
})
</script>
<style>
ul li{
	list-style-type:none;
}
</style>