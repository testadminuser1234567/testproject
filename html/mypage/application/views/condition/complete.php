<!-- co start -->
<h1>希望条件の変更完了</h1>
<!-- co start -->
<div id="co">
    <div id="coSub" class="clearfix">
        <main id="mainCo">
            <!-- conditionList start -->
            <div id="conditionList" class="finish">
                <div class="inner">
                    <p class="finish">希望条件の変更が完了しました。<br>
                        希望条件に合致する求人はコチラ▼</p>
                    <div class="aCenter">
                    <p class="btnFinish">
                        <a href="<?php echo base_url();?>search/home/"><span class="ico">希望条件に合致する求人をみる</span></a>
                    </p>
                    </div>
                    <p class="txtLink"><a href="<?php echo base_url();?>condition/home/">現在の希望条件を確認する→</a></p>
                </div>
            </div>
            <!-- conditionListt end -->
        </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($looks)):?>
                <div id="historyBox">
                    <h2><span>最近閲覧した求人</span></h2>
                    <div class="inner">
                        <?php foreach($looks as $k=>$v):?>
                            <!-- entry start -->
                            <div class="entry">
                                <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                            </div>
                            <!-- entry end -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
            <!-- historyBox end -->