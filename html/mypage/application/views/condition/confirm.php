<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);
$job_ary=array();
if(!empty($tmpjob['Item'])){
    foreach($tmpjob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $job_ary as $key => $value) {
        $sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($sort, SORT_ASC, $job_ary);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);
$tantou_job_ary=array();
if(!empty($tmptantoujob['Item'])){
    foreach($tmptantoujob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $tantou_job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $tantou_job_ary as $key => $value) {
        $tantou_sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($tantou_sort, SORT_ASC, $tantou_job_ary);
?>
<h1>希望条件の編集</h1>
<!-- co start -->
<div id="co">
    <div id="coSub" class="clearfix">
        <main id="mainCo">
            <div class="registInputPage form">
                <table width="100%" class="confirm">
                    <tr>
                        <th class="<?php if(@${$segment}->kibouarea1):?>ok<?php else:?>hissu<?php endif;?> kibouarea1">希望勤務地：第１希望<br />
                        </th>
                        <td>

                            <?php $tflg=0;?>
                            <?php if(!empty($tmparea['Item'])):?>
                                <?php foreach($tmparea['Item'] as $k=>$v):?>
                                    <?php if(!empty($v['Item'])):?>
                                        <?php foreach($v['Item'] as $k2=>$v2):?>
                                            <?php if(!empty($v2["Items"]['Item'])):?>
                                                <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>

                                                    <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>

                                                        <?php if(@${$segment}->kibouarea1==$v3['Option.P_Id']):?><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>

                                                    <?php else:?>
                                                        <?php if($tflg==0):?>
                                                            <?php if(@${$segment}->kibouarea1==$v2['Items']['Item']['Option.P_Id']):?><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
                                                            <?php $tflg=1;?>
                                                        <?php endif;?>

                                                    <?php endif;?>

                                                <?php endforeach;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->kibouarea2):?>ok<?php else:?><?php endif;?> kibouarea2">希望勤務地：第２希望<br />
                        </th>
                        <td>

                            <?php $tflg=0;?>
                            <?php if(!empty($tmparea['Item'])):?>
                                <?php foreach($tmparea['Item'] as $k=>$v):?>
                                    <?php if(!empty($v['Item'])):?>
                                        <?php foreach($v['Item'] as $k2=>$v2):?>
                                            <?php if(!empty($v2["Items"]['Item'])):?>
                                                <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>

                                                    <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>

                                                        <?php if(@${$segment}->kibouarea2==$v3['Option.P_Id']):?><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>

                                                    <?php else:?>
                                                        <?php if($tflg==0):?>
                                                            <?php if(@${$segment}->kibouarea2==$v2['Items']['Item']['Option.P_Id']):?><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
                                                            <?php $tflg=1;?>
                                                        <?php endif;?>

                                                    <?php endif;?>

                                                <?php endforeach;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->kibouarea3):?>ok<?php else:?><?php endif;?> kibouarea3">希望勤務地：第３希望<br />
                        </th>
                        <td>

                            <?php $tflg=0;?>
                            <?php if(!empty($tmparea['Item'])):?>
                                <?php foreach($tmparea['Item'] as $k=>$v):?>
                                    <?php if(!empty($v['Item'])):?>
                                        <?php foreach($v['Item'] as $k2=>$v2):?>
                                            <?php if(!empty($v2["Items"]['Item'])):?>
                                                <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>

                                                    <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>

                                                        <?php if(@${$segment}->kibouarea3==$v3['Option.P_Id']):?><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>

                                                    <?php else:?>
                                                        <?php if($tflg==0):?>
                                                            <?php if(@${$segment}->kibouarea3==$v2['Items']['Item']['Option.P_Id']):?><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?><?php endif;?>
                                                            <?php $tflg=1;?>
                                                        <?php endif;?>

                                                    <?php endif;?>

                                                <?php endforeach;?>
                                            <?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->kiboujob):?>ok<?php else:?><?php endif;?>">希望職種</th>
                        <td>
                            <?php if(!empty($job_ary)):?>
                                <?php foreach($job_ary as $k=>$v):?>
                                    <?php if(!empty($_POST["kiboujob"])):?>
                                        <?php foreach($_POST["kiboujob"] as $pk => $pv):?>
                                            <?php if($pv==$v['Option.P_Id']):?><?php echo htmlspecialchars($v['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br /><?php endif;?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        </td>


                    <tr>
                        <th class="<?php if(@${$segment}->income):?>ok<?php else:?><?php endif;?> income">希望年収</th>
                        <td>
                            <?php if(empty(${$segment}->income)):?>指定なし
                            <?php else:?>
                                <?php if(!empty($tmpincome['Item'])):?>
                                    <?php foreach($tmpincome['Item'] as $k=>$v):?>
                                        <?php if(!empty($v['Item'])):?>
                                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                                <?php if($_POST["income"]==$v2['Option.P_Id']):?><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?><br /><?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                <?php endif;?>
                            <?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th class="<?php if(@${$segment}->tenkin):?>ok<?php else:?><?php endif;?> tenkin">転勤</th>
                        <td>
                            <?php if(!empty($_POST["tenkin"])):?>
                                <?php foreach($_POST["tenkin"] as $k => $v):?>
                                    <?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?><br />
                                <?php endforeach;?>
                            <?php endif;?>
                        </td>
                    </tr>


                </table>



                <!-- start -->
                <div class="confirmBtnBox clearfix">
                    <div class="send">
                        <?php echo form_open('condition/add');?>
                        <p class="btnSend">
                            <input type="submit" value="保存する" onclick="window.onbeforeunload=null;">
                        </p>
                        <?php echo form_hidden('mode', 'send'); ?>
                        <?php echo form_hidden('kibouarea1', @${$segment}->kibouarea1); ?>
                        <?php echo form_hidden('kibouarea2', @${$segment}->kibouarea2); ?>
                        <?php echo form_hidden('kibouarea3', @${$segment}->kibouarea3); ?>
                        <?php echo form_hidden('kiboujob', @${$segment}->kiboujob); ?>
                        <?php echo form_hidden('income', ${$segment}->income); ?>
                        <?php echo form_hidden('tenkin', @${$segment}->tenkin); ?>
                        <?php echo form_hidden($csrf); ?>
                        <?php echo form_close();?>
                    </div>
                    <div class="back">
                        <?php echo form_open('condition/create/');?>
                        <p class="btnBack">
                            <input type="submit" value="入力画面に戻って修正する" onclick="window.onbeforeunload=null;">
                        </p>
                        <?php echo form_hidden('mode', 'return'); ?>
                        <?php echo form_hidden('kibouarea1', @${$segment}->kibouarea1); ?>
                        <?php echo form_hidden('kibouarea2', @${$segment}->kibouarea2); ?>
                        <?php echo form_hidden('kibouarea3', @${$segment}->kibouarea3); ?>
                        <?php echo form_hidden('kiboujob', @${$segment}->kiboujob); ?>
                        <?php echo form_hidden('income', ${$segment}->income); ?>
                        <?php echo form_hidden('tenkin', @${$segment}->tenkin); ?>
                        <?php echo form_hidden($csrf); ?>
                        <?php echo form_close();?>
                    </div>
                </div>
                <!-- end -->




                <!-- conditionListt end -->
        </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($looks)):?>
                <div id="historyBox">
                    <h2><span>最近閲覧した求人</span></h2>
                    <div class="inner">
                        <?php foreach($looks as $k=>$v):?>
                            <!-- entry start -->
                            <div class="entry">
                                <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                            </div>
                            <!-- entry end -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
            <!-- historyBox end -->
