<?php
//HRBCマスターと連動
//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);
$job_ary=array();
if(!empty($tmpjob['Item'])){
    foreach($tmpjob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $job_ary as $key => $value) {
        $sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($sort, SORT_ASC, $job_ary);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

$tmpincome = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/income.json', true);
$tmpincome = json_decode($tmpincome,true);

$tmptantoujob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobmypagecd.json', true);
$tmptantoujob = json_decode($tmptantoujob,true);
$tantou_job_ary=array();
if(!empty($tmptantoujob['Item'])){
    foreach($tmptantoujob['Item'] as $k=>$v){
        if(!empty($v['Item'])){
            foreach($v['Item'] as $k2=>$v2){
                $tantou_job_ary[] = $v2;
            }
        }
    }
    foreach ((array) $tantou_job_ary as $key => $value) {
        $tantou_sort[$key] = $value["Option.P_Order"];
    }
}
array_multisort($tantou_sort, SORT_ASC, $tantou_job_ary);

$start=date('Y');
$end=$start-65;
?>
<script src="<?php echo base_url();?>js/jquery-1.6.4.min.js" charset="utf-8"></script>
<script src="<?php echo base_url();?>js/form_validation_login.js?j=<?php echo date('Ymdhis');?>"></script>
<h1>希望条件の編集</h1>
<!-- co start -->
<div id="co">
    <div id="coSub" class="clearfix">
        <main id="mainCo">
            <div id="editTxt">
                <div class="inner">
                    <p>希望条件を編集します。下記フォームにご入力の上、[入力した内容に変更する]ボタンを押してください。</p>
                </div>
            </div>
            <div class="error"><?php echo $msg ?></div>
            <?php echo form_open_multipart('condition/add/');?>


            <div class="registInputPage form">
                <table width="100%" >
                    <tr>
                        <th class=" ">希望勤務地<br />
                        </th>
                        <td>
                            <table>
                                <tbody>
                                <tr>
                                    <th>第1希望<br>
                                        <img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->kibouarea1):?>ok<?php else:?>hissu<?php endif;?>.png" class="kibouarea1" width="49" height="14" alt=""/></th>
                                    <td>
                            <?php $tflg=0;?>
                            <?php if(!empty($tmparea['Item'])):?>
                                <select name="kibouarea1" id="kibouarea1" class="hissu required">
                                    <option value="" selected="selected" label="">▼お選びください</option>
                                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                                        <?php if(!empty($v['Item'])):?>
                                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                                <?php if(!empty($v2["Items"]['Item'])):?>
                                                    <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                                        <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                            <option value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea1==$v3['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>

                                                        <?php else:?>
                                                            <?php if($tflg==0):?>
                                                                <option value="<?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea1==$v2['Items']['Item']['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                                                <?php $tflg=1;?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </select>
                            <?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th>第2希望<br>
                            <img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->kibouarea2):?>ok<?php else:?>nini<?php endif;?>.png" class="kibouarea2" width="49" height="14" alt=""/></th>
                        <td>

                            <?php $tflg=0;?>
                            <?php if(!empty($tmparea['Item'])):?>
                                <select name="kibouarea2" id="kibouarea2" class="">
                                    <option value="" selected="selected" label="">▼お選びください</option>
                                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                                        <?php if(!empty($v['Item'])):?>
                                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                                <?php if(!empty($v2["Items"]['Item'])):?>
                                                    <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                                        <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                            <option value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea2==$v3['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>

                                                        <?php else:?>
                                                            <?php if($tflg==0):?>
                                                                <option value="<?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea2==$v2['Items']['Item']['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                                                <?php $tflg=1;?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </select>
                            <?php endif;?>
                        </td>
                    </tr>

                                <th>第3希望<br>
                                    <img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->kibouarea3):?>ok<?php else:?>nini<?php endif;?>.png" class="kibouarea3" width="49" height="14" alt=""/></th>
                                <td>
                            <?php $tflg=0;?>
                            <?php if(!empty($tmparea['Item'])):?>
                                <select name="kibouarea3" id="kibouarea3" class="">
                                    <option value="" selected="selected" label="">▼お選びください</option>
                                    <?php foreach($tmparea['Item'] as $k=>$v):?>
                                        <?php if(!empty($v['Item'])):?>
                                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                                <?php if(!empty($v2["Items"]['Item'])):?>
                                                    <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                                        <?php if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)):?>
                                                            <option value="<?php echo htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea3==$v3['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>

                                                        <?php else:?>
                                                            <?php if($tflg==0):?>
                                                                <option value="<?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->kibouarea3==$v2['Items']['Item']['Option.P_Id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                                                <?php $tflg=1;?>
                                                            <?php endif;?>
                                                        <?php endif;?>
                                                    <?php endforeach;?>
                                                <?php endif;?>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </select>
                            <?php endif;?>

                        </td>
                    </tr>
                                </tbody>
                            </table>

                    <tr>
                        <th >
                            希望職種<br><img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->kiboujob):?>ok<?php else:?>hissu<?php endif;?>.png" class="<?php if(@${$segment}->kiboujob):?>ok<?php else:?>hissu<?php endif;?> kiboujob" width="49" height="14" alt=""/></th>
                        <td class="">
                            <?php if(!empty($job_ary)):?>
                                <ul class="full clearfix checkboxrequired">
                                    <?php foreach($job_ary as $k=>$v):?>
                                        <li <?php if($v['Option.P_Name']=="営業系"):?>class="ss"<?php elseif($v['Option.P_Name']=="管理部門/事務系"):?>class="ss no"<?php endif;?>>
                                            <input type="checkbox" class="checkbox joballchk jobcheckbox<?php if($k2-1==count($tmpjob['Item'])):?> required<?php endif;?>" name="kiboujob[]" value="<?php echo htmlspecialchars($v['Option.P_Id'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty(${$segment}->kiboujob)):?><?php foreach(${$segment}->kiboujob as $jk=>$jv):?><?php if($jv==$v["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?> id="c_cat_job0<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>">
                                            <label id="joblabel<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" for="c_cat_job0<?php echo htmlspecialchars($v["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" class="checkboxlabel joblabel CheckBoxLabelClass joballnavichecklabel <?php if(!empty(${$segment}->kiboujob)):?><?php foreach(${$segment}->kiboujob as $jk=>$jv):?><?php if($jv==$v["Option.P_Id"]):?>checked<?php endif;?><?php endforeach;?><?php endif;?>">
                                                <?php echo htmlspecialchars($v['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
                                            </label>
                                        </li>
                                    <?php endforeach;?>
                                </ul>
                            <?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th >希望年収<br>
                            <img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->income):?>ok<?php else:?>hissu<?php endif;?>.png" class="<?php if(@${$segment}->income):?>ok<?php else:?>hissu<?php endif;?> income" width="49" height="14" alt=""/></th>
                        <td>
                            <?php if(!empty($tmpincome['Item'])):?>
                                <select name="income" id="income" class="required">
                                    <option value="">指定なし</option>
                                    <?php foreach($tmpincome['Item'] as $k=>$v):?>
                                        <?php if(!empty($v['Item'])):?>
                                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                                <option value="<?php echo htmlspecialchars($v2["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" <?php if($v2["Option.P_Id"]==@${$segment}->income):?>selected<?php endif;?>><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                            <?php endforeach;?>
                                        <?php endif;?>
                                    <?php endforeach;?>
                                </select>
                            <?php endif;?>
                        </td>
                    </tr>

                    <tr>
                        <th>転勤可否<br>
                            <img src="<?php echo base_url();?>images/ico_<?php if(@${$segment}->tenkin):?>ok<?php else:?>hissu<?php endif;?>.png" width="49"  class="<?php if(@${$segment}->tenkin):?>ok<?php else:?>hissu<?php endif;?> tenkin" height="14" alt=""/></th>
                        <td class="checkboxrequired">
                            <?php if(!empty($tmptenkin['Item'])):?>
                                <?php foreach($tmptenkin['Item'] as $k=>$v):?>
                                    <?php if(!empty($v['Item'])):?>
                                        <?php foreach($v['Item'] as $k2=>$v2):?>
                                            <input type="checkbox" class="checkbox chk tenkincheckbox<?php if($k2-1==count($tmptenkin['Item'])):?> required<?php endif;?>" name="tenkin[]" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(!empty(${$segment}->tenkin)):?><?php if(in_array($v2['Option.P_Name'], @${$segment}->tenkin, true)):?>checked<?php endif;?><?php endif;?> id="tenkin0<?php echo htmlspecialchars($v2["Option.P_Id"],ENT_QUOTES,'UTF-8');?>">
                                            <label for="tenkin0<?php echo htmlspecialchars($v2["Option.P_Id"],ENT_QUOTES,'UTF-8');?>" class="checkboxlabel tenkinlabel  CheckBoxLabelClass navichecklabel">
                                                <?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>
                                            </label>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        </td>
                    </tr>




                </table>
                <?php echo form_hidden($csrf); ?>
                <div class="btnBox">
                <button class="btn b02" onclick="return check(1);"><span>入力した内容に変更する</span></button>
</div>
            </div>

            <?php echo form_close();?>

            <!-- conditionListt end -->
        </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($looks)):?>
                <div id="historyBox">
                    <h2><span>最近閲覧した求人</span></h2>
                    <div class="inner">
                        <?php foreach($looks as $k=>$v):?>
                            <!-- entry start -->
                            <div class="entry">
                                <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                            </div>
                            <!-- entry end -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
            <!-- historyBox end -->
