<?php
header("Content-type: text/html; charset=utf-8");
header("X-Content-Type-Options: nosniff");
header("X-FRAME-OPTIONS: SAMEORIGIN");
header("X-FRAME-OPTIONS: DENY");

defined('BASEPATH') OR exit('No direct script access allowed');

/*
echo "\nERROR: ",
	$heading,
	"\n\n",
	$message,
	"\n\n";
*/
?>
<?php 
$ref="";
if(!empty($_SERVER["HTTP_REFERER"])){
	$ref = $_SERVER['HTTP_REFERER'];
}
if(empty($ref)){
	$ref="/mypage/user/home";
}
?>

<!-- co start -->
    <section class="co">
        <div class="inner">
            <h1><span class="line">サーバエラー</span></h1>
            <p class="aCenter">サーバエラーが発生しました。<br>
            <a href="#" onclick="history.back();" class="color">こちら</a>からもう一度処理を行ってください。</p>
        </div>
    </section>
<!-- co end -->
<?php //include(BaseDir.kanriDirectory.'/include/footer_nologin.php'); ?>
</body>
</html>
