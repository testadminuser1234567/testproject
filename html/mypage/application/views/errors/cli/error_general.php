<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
echo "\nERROR: ",
	$heading,
	"\n\n",
	$message,
	"\n\n";
*/
?>
    <section class="co">
        <div class="inner">
            <h1><span class="line">アクセス失敗</span></h1>
            <p class="aCenter">処理が途中で中断されました。<br>
            <a href="<?php echo base_url();?>user/home/" class="color">こちら</a>からトップページへお戻りください。</p>
        </div>
    </section>