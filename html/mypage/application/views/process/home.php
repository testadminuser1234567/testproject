<?php
//HRBCマスターと連動

//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

?>
<script src="<?php echo base_url();?>js/jquery.tile.js"></script>
<h1>応募状況</h1>
<!-- co start -->
<div id="co">
    <div id="coSub" class="clearfix">
        <main id="mainCo">
            <!-- applicationList start -->
            <div id="applicationList">
                <p class="notes">タップで詳細が開きます</p>
                <!-- type start -->
                <?php //if(!empty($offer)):?>
                <div class="typeBox type01">
                    <h2 class="<?php if(!empty($offer)):?>tap<?php endif;?>"><a href="javascript:;"><span class="title">オファー</span><span class="number"><strong><?php echo $clean->purify(count($offer));?></strong>件</span></a></h2>
                    <!-- jobList start -->
                    <div class="jobList b01">
                        <div class="jobInner">
                            <?php foreach($offer as $k => $v):?>
                                <!-- jobBox start -->
                                <div class="jobBox">
                                    <div class="inner">
                                        <h3>
                                            <?php if($v["job"]["phaze"]==$phaze):?>
                                                <a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><?php echo $clean->purify($v["job"]["job_title"]);?></a>
                                            <?php else:?>
                                                <?php echo $clean->purify($v["job"]["job_title"]);?>
                                            <?php endif;?>
                                        </h3>
                                        <div class="tableWrap">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <th>企業名</th>
                                                    <td>
                                                        <?php if($v['clientNameDisplay']=="公開"):?>
                                                            <?php echo $clean->purify($v["client"]["c_title"]);?>
                                                        <?php else:?>
                                                            -
                                                        <?php endif?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>職種</th>
                                                    <td><?php echo $clean->purify($v["jobcategoryName"]);?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php if($v["job"]["phaze"]==$phaze):?>
                                            <!--
                                        <?php $tmp_id1 = "frm1_" . $clean->purify($v["job"]["job_id"]);?>
                                        <?php $tmp_id2 = "frm2_" . $clean->purify($v["job"]["job_id"]);?>
                                        <li>
                                            <?php echo form_open_multipart('process/apply',"id=\"$tmp_id1\"");?>
                                            <input type="button" name="apply_submit" id="apply_<?php echo $clean->purify($v["job"]["job_id"]);?>" class="application apply" value="応募する" />
                                            <input type="hidden" name="job_name" value="<?php echo $clean->purify($v["job"]["job_title"]);?>" />
                                            <input type="hidden" name="job_id" value="<?php echo $clean->purify($v["job"]["job_id"]);?>" />
                                            <input type="hidden" name="client_id" value="<?php echo $clean->purify($v["client"]["c_id"]);?>" />
                                            <input type="hidden" name="client_name" value="<?php echo $clean->purify($v["client"]["c_title"]);?>" />
                                            <input type="hidden" name="processId" value="<?php echo $clean->purify($v["processId"]);?>" />
                                            <input type="hidden" name="phaseId" value="<?php echo $clean->purify($v["phazeId"]);?>" />
                                            <input type="hidden" name="phasedate" value="<?php echo $clean->purify($v["phasedate"]);?>" />
                                            <input type="hidden" name="phazeFlg" value="<?php echo $clean->purify($v["phazeFlg"]);?>" />
                                            <?php echo form_close();?>
                                        </li>
                                        <li>
                                            <?php echo form_open_multipart('process/refusal',"id=\"$tmp_id2\"");?>
                                            <input type="button" name="refuse_submit" id="refusal_<?php echo $clean->purify($v["job"]["job_id"]);?>" class="application refusal" value="辞退する" />
                                            <input type="hidden" name="job_name" value="<?php echo $clean->purify($v["job"]["job_title"]);?>" />
                                            <input type="hidden" name="job_id" value="<?php echo $clean->purify($v["job"]["job_id"]);?>" />
                                            <input type="hidden" name="client_id" value="<?php echo $clean->purify($v["client"]["c_id"]);?>" />
                                            <input type="hidden" name="client_name" value="<?php echo $clean->purify($v["client"]["c_title"]);?>" />
                                            <input type="hidden" name="processId" value="<?php echo $clean->purify($v["processId"]);?>" />
                                            <input type="hidden" name="phaseId" value="<?php echo $clean->purify($v["phazeId"]);?>" />
                                            <input type="hidden" name="phasedate" value="<?php echo $clean->purify($v["phasedate"]);?>" />
                                            <input type="hidden" name="phazeFlg" value="<?php echo $clean->purify($v["phazeFlg"]);?>" />
                                            <?php echo form_close();?>
                                        </li>
-->
                                            <p class="btn"><a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><span>この求人の詳細をみる</span></a>
                                        <?php else:?>
                                            <p class="btn finish">募集終了</p>
                                        <?php endif;?>
                                        </p>
                                    </div>
                                </div>
                                <!-- jobBox end -->
                            <?php endforeach;?>
                        </div>
                    </div>
                    <!-- jobList end -->
                </div>
                <!-- type end -->
                <?php //endif;?>
                <?php //if(!empty(contact)):?>
                <!-- type start -->
                <div class="typeBox type02">
                    <h2 class="<?php if(!empty($contact)):?>tap<?php endif;?>"><a href="javascript:;"><span class="title">問い合わせ中</span><span class="number"><strong><?php echo $clean->purify(count($contact));?></strong>件</span></a></h2>
                    <!-- jobList start -->
                    <div class="jobList b02">
                        <div class="jobInner">
                            <?php foreach($contact as $k => $v):?>
                                <!-- jobBox start -->
                                <div class="jobBox">
                                    <div class="inner">
                                        <h3>
                                            <?php if($v["job"]["phaze"]==$phaze):?>
                                                <a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><?php echo $clean->purify($v["job"]["job_title"]);?></a>
                                            <?php else:?>
                                                <?php echo $clean->purify($v["job"]["job_title"]);?>
                                            <?php endif;?>
                                        </h3>
                                        <div class="tableWrap">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <th>企業名</th>
                                                    <td>
                                                        <?php if($v['clientNameDisplay']=="公開"):?>
                                                            <?php echo $clean->purify($v["client"]["c_title"]);?>
                                                        <?php else:?>
                                                            -
                                                        <?php endif?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>職種</th>
                                                    <td><?php echo $clean->purify($v["jobcategoryName"]);?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php if($v["job"]["phaze"]==$phaze):?>
                                            <p class="btn"><a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><span>この求人の詳細をみる</span></a></p>
                                        <?php else:?>
                                            <p class="btn finish">募集終了</p>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <!-- jobBox end -->
                            <?php endforeach;?>
                        </div>
                    </div>
                    <!-- jobList end -->
                </div>
                <!-- type end -->
                <?php //endif;?>
                <?php //if(!empty($apply)):?>
                <!-- type start -->
                <div class="typeBox type02">
                    <h2 class="<?php if(!empty($apply)):?>tap<?php endif;?>"><a href="javascript:;"><span class="title">応募中</span><span class="number"><strong><?php echo $clean->purify(count($apply));?></strong>件</span></a></h2>
                    <!-- jobList start -->
                    <div class="jobList b03">
                        <div class="jobInner">
                            <?php foreach($apply as $k => $v):?>
                                <!-- jobBox start -->
                                <div class="jobBox">
                                    <div class="inner">
                                        <h3>
                                            <?php if($v["job"]["phaze"]==$phaze):?>
                                                <a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><?php echo $clean->purify($v["job"]["job_title"]);?></a>
                                            <?php else:?>
                                                <?php echo $clean->purify($v["job"]["job_title"]);?>
                                            <?php endif;?>
                                        </h3>
                                        <div class="tableWrap">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <th>企業名</th>
                                                    <td>
                                                        <?php if($v['clientNameDisplay']=="公開"):?>
                                                            <?php echo $clean->purify($v["client"]["c_title"]);?>
                                                        <?php else:?>
                                                            -
                                                        <?php endif?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>職種</th>
                                                    <td><?php echo $clean->purify($v["jobcategoryName"]);?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php if($v["job"]["phaze"]==$phaze):?>
                                            <p class="btn"><a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><span>この求人の詳細をみる</span></a></p>
                                        <?php else:?>
                                            <p class="btn finish">募集終了</p>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <!-- jobBox end -->
                            <?php endforeach;?>
                        </div>
                    </div>
                    <!-- jobList end -->
                </div>
                <!-- type end -->
                <?php //endif;?>
                <?php //if(!empty($selection)):?>
                <!-- type start -->
                <div class="typeBox type02">
                    <h2 class="<?php if(!empty($selection)):?>tap<?php endif;?>"><a href="javascript:;"><span class="title">面接</span><span class="number"><strong><?php echo $clean->purify(count($selection));?></strong>件</span></a></h2>
                    <!-- jobList start -->
                    <div class="jobList b04">
                        <div class="jobInner">
                            <?php foreach($selection as $k => $v):?>
                                <!-- jobBox start -->
                                <div class="jobBox">
                                    <div class="inner">
                                        <p class="interview">面接日:<strong><?php if($v["opname"]=="書類OK"):?>調整中<?php else:?><?php echo $clean->purify(date('Y/m/d',strtotime($v["phasedate"])));?><?php endif;?></strong></p>
                                        <h3>
                                            <?php if($v["job"]["phaze"]==$phaze):?>
                                                <a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><?php echo $clean->purify($v["job"]["job_title"]);?></a>
                                            <?php else:?>
                                                <?php echo $clean->purify($v["job"]["job_title"]);?>
                                            <?php endif;?>
                                        </h3>
                                        <div class="tableWrap">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <th>企業名</th>
                                                    <td>
                                                        <?php if($v['clientNameDisplay']=="公開"):?>
                                                            <?php echo $clean->purify($v["client"]["c_title"]);?>
                                                        <?php else:?>
                                                            -
                                                        <?php endif?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>職種</th>
                                                    <td><?php echo $clean->purify($v["jobcategoryName"]);?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php if($v["job"]["phaze"]==$phaze):?>
                                            <p class="btn"><a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><span>この求人の詳細をみる</span></a></p>
                                        <?php else:?>
                                            <p class="btn finish">募集終了</p>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <!-- jobBox end -->
                            <?php endforeach;?>
                        </div>
                    </div>
                    <!-- jobList end -->
                </div>
                <!-- type end -->
                <?php //endif;?>
                <?php //if(!empty($appointment)):?>
                <!-- type start -->
                <div class="typeBox type02">
                    <h2 class="<?php if(!empty($appointment)):?>tap<?php endif;?>"><a href="javascript:;"><span class="title">内定</span><span class="number"><strong><?php echo $clean->purify(count($appointment));?></strong>件</span></a></h2>
                    <!-- jobList start -->
                    <div class="jobList b05">
                        <div class="jobInner">
                            <?php foreach($appointment as $k => $v):?>
                                <!-- jobBox start -->
                                <div class="jobBox">
                                    <div class="inner">
                                        <h3>
                                            <?php if($v["job"]["phaze"]==$phaze):?>
                                                <a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><?php echo $clean->purify($v["job"]["job_title"]);?></a>
                                            <?php else:?>
                                                <?php echo $clean->purify($v["job"]["job_title"]);?>
                                            <?php endif;?>
                                        </h3>
                                        <div class="tableWrap">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <th>企業名</th>
                                                    <td>
                                                        <?php if($v['clientNameDisplay']=="公開"):?>
                                                            <?php echo $clean->purify($v["client"]["c_title"]);?>
                                                        <?php else:?>
                                                            -
                                                        <?php endif?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>職種</th>
                                                    <td><?php echo $clean->purify($v["jobcategoryName"]);?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php if($v["job"]["phaze"]==$phaze):?>
                                            <p class="btn"><a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><span>この求人の詳細をみる</span></a></p>
                                        <?php else:?>
                                            <p class="btn finish">内定</p>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <!-- jobBox end -->
                            <?php endforeach;?>
                        </div>
                    </div>
                    <!-- jobList end -->
                </div>
                <!-- type end -->
                <?php //endif;?>
                <?php //if(!empty($refuse)):?>
                <!-- type start -->
                <div class="typeBox type03">
                    <h2 class="<?php if(!empty($refuse)):?>tap<?php endif;?>"><a href="javascript:;"><span class="title">辞退済み</span><span class="number"><strong><?php echo $clean->purify(count($refuse));?></strong>件</span></a></h2>
                    <!-- jobList start -->
                    <div class="jobList b06">
                        <div class="jobInner">
                            <?php foreach($refuse as $k => $v):?>
                                <!-- jobBox start -->
                                <div class="jobBox">
                                    <div class="inner">
                                        <h3>
                                            <?php if($v["job"]["phaze"]==$phaze):?>
                                                <a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><?php echo $clean->purify($v["job"]["job_title"]);?></a>
                                            <?php else:?>
                                                <?php echo $clean->purify($v["job"]["job_title"]);?>
                                            <?php endif;?>
                                        </h3>
                                        <div class="tableWrap">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <th>企業名</th>
                                                    <td>
                                                        <?php if($v['clientNameDisplay']=="公開"):?>
                                                            <?php echo $clean->purify($v["client"]["c_title"]);?>
                                                        <?php else:?>
                                                            -
                                                        <?php endif?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>職種</th>
                                                    <td><?php echo $clean->purify($v["jobcategoryName"]);?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php if($v["job"]["phaze"]==$phaze):?>

                                            <div class="btnBox">
                                                <p class="btn"><a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><span>この求人の詳細をみる</span></a></p>
                                                <?php if($v["opname"]=="本人NG"):?>

                                                    <?php $tmp_id1 = "frm1_" . $clean->purify($v["job"]["job_id"]);?>
                                                    <?php $tmp_id2 = "frm2_" . $clean->purify($v["job"]["job_id"]);?>
                                                    <?php echo form_open_multipart('process/apply',"id=\"$tmp_id1\"");?>
                                                    <p class="btn reentry"><button name="apply_submit" id="apply_<?php echo $clean->purify($v["job"]["job_id"]);?>" class="application apply" value="やっぱり応募する"><span><span class="ss">やっぱり</span><br>
応募する</span></button>
                                                    </p>
                                                    <input type="hidden" name="job_name" value="<?php echo $clean->purify($v["job"]["job_title"]);?>" />
                                                    <input type="hidden" name="job_id" value="<?php echo $clean->purify($v["job"]["job_id"]);?>" />
                                                    <input type="hidden" name="client_id" value="<?php echo $clean->purify($v["client"]["c_id"]);?>" />
                                                    <input type="hidden" name="client_name" value="<?php echo $clean->purify($v["client"]["c_title"]);?>" />
                                                    <input type="hidden" name="processId" value="<?php echo $clean->purify($v["processId"]);?>" />
                                                    <input type="hidden" name="phaseId" value="<?php echo $clean->purify($v["phazeId"]);?>" />
                                                    <input type="hidden" name="phasedate" value="<?php echo $clean->purify($v["phasedate"]);?>" />
                                                    <input type="hidden" name="phazeFlg" value="<?php echo $clean->purify($v["phazeFlg"]);?>" />
                                                    <?php echo form_close();?>
                                                <?php endif;?>
                                            </div>
                                        <?php else:?>
                                            <p class="btn finish">辞退済み</p>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <!-- jobBox end -->
                            <?php endforeach;?>
                        </div>
                    </div>
                    <!-- jobList end -->
                </div>
                <!-- type end -->
                <?php //endif;?>
                <?php //if(!empty($sendoff)):?>
                <!-- type start -->
                <div class="typeBox type03">
                    <h2 class="<?php if(!empty($sendoff)):?>tap<?php endif;?>"><a href="javascript:;"><span class="title">お見送り</span><span class="number"><strong><?php echo $clean->purify(count($sendoff));?></strong>件</span></a></h2>
                    <!-- jobList start -->
                    <div class="jobList b07">
                        <div class="jobInner">
                            <?php foreach($sendoff as $k => $v):?>
                                <!-- jobBox start -->
                                <div class="jobBox">
                                    <div class="inner">
                                        <h3>
                                            <?php if($v["job"]["phaze"]==$phaze):?>
                                                <a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><?php echo $clean->purify($v["job"]["job_title"]);?></a>
                                            <?php else:?>
                                                <?php echo $clean->purify($v["job"]["job_title"]);?>
                                            <?php endif;?>
                                        </h3>
                                        <div class="tableWrap">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <th>企業名</th>
                                                    <td>
                                                        <?php if($v['clientNameDisplay']=="公開"):?>
                                                            <?php echo $clean->purify($v["client"]["c_title"]);?>
                                                        <?php else:?>
                                                            -
                                                        <?php endif?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>職種</th>
                                                    <td><?php echo $clean->purify($v["jobcategoryName"]);?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php if($v["job"]["phaze"]==$phaze):?>
                                            <div class="btnBox">
                                                <p class="btn"><a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><span>この求人の詳細をみる</span></a></p>
                                            </div>
                                        <?php else:?>
                                            <p class="btn finish">お見送り</p>
                                        <?php endif;?>
                                    </div>
                                </div>
                                <!-- jobBox end -->
                            <?php endforeach;?>
                        </div>
                    </div>
                    <!-- jobList end -->
                </div>
                <!-- type end -->
                <?php //endif;?>
                <?php //if(!empty($finish)):?>
                <!-- type start -->
                <div class="typeBox last type03">
                    <h2 class="<?php if(!empty($finish)):?>tap<?php endif;?>"><a href="javascript:;"><span class="title">募集終了</span><span class="number"><strong><?php echo $clean->purify(count($finish));?></strong>件</span></a></h2>
                    <!-- jobList start -->
                    <div class="jobList b08">
                        <div class="jobInner">
                            <?php foreach($finish as $k => $v):?>
                                <!-- jobBox start -->
                                <div class="jobBox">
                                    <div class="inner">
                                        <h3>
                                            <?php //if($v["job"]["phaze"]==$phaze):?>
                                            <!--
                                                    <a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><?php echo $clean->purify($v["job"]["job_title"]);?></a>
                                                    -->
                                            <?php //else:?>
                                            <?php echo $clean->purify($v["job"]["job_title"]);?>
                                            <?php //endif;?>
                                        </h3>
                                        <div class="tableWrap">
                                            <table>
                                                <tbody>
                                                <tr>
                                                    <th>企業名</th>
                                                    <td>
                                                        <?php if($v['clientNameDisplay']=="公開"):?>
                                                            <?php echo $clean->purify($v["client"]["c_title"]);?>
                                                        <?php else:?>
                                                            -
                                                        <?php endif?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>職種</th>
                                                    <td><?php echo $clean->purify($v["jobcategoryName"]);?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php //if($v["job"]["phaze"]==$phaze):?>
                                        <!--
                                                <div class="btnBox">
                                                    <p class="btn"><a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><span>この求人の詳細をみる</span></a></p>
                                                </div>
                                                -->
                                        <?php //else:?>
                                        <p class="btn finish">募集終了</p>
                                        <?php //endif;?>
                                    </div>
                                </div>
                                <!-- jobBox end -->
                            <?php endforeach;?>
                        </div>
                    </div>
                    <!-- jobList end -->
                </div>
                <!-- type end -->
                <?php //endif;?>
                <!-- btnTel start -->
                <script>
                    $(function(){
                        $(".apply").click(function() {
                            if(confirm("ご応募いただき、ありがとうございます。求人のご応募について、確認次第、順次ご回答いたします。")) {
                                apply_tmp_id = $(this).attr("id");
                                var apply_info = apply_tmp_id.split( "_" );

                                var apply_id = apply_info[0];
                                var job_id = apply_info[1];

                                $("#lists_" + job_id).before("<div id='loading' style='text-align:center;margin-right:0px;'><img src='<?php echo base_url();?>images/loading.gif' /></div>");
                                $("#lists_" + job_id).hide();


                                var form_id = "frm1_" + job_id;
                                ga('send', 'event', 'myPageApply', 'CTA', 'entryButton_Apply');
                                $('#'+form_id).submit();
                            }
                        })

                        $(".refusal").click(function() {
                            if(confirm("応募を辞退しますか？")) {
                                refusal_tmp_id = $(this).attr("id");
                                var refusal_info = refusal_tmp_id.split( "_" );

                                var refusal_id = refusal_info[0];
                                var job_id = refusal_info[1];

                                $("#lists_" + job_id).before("<div id='loading' style='text-align:center;margin-right:0px;'><img src='<?php echo base_url();?>images/loading.gif' /></div>");
                                $("#lists_" + job_id).hide();


                                var form_id = "frm2_" + job_id;
                                ga('send', 'event', 'myPageApply', 'CTA', 'entryButton_notApply');
                                $('#'+form_id).submit();
                            }
                        })
                    });
                </script>
                <div class="btnTel">
                    <a href="tel:0120141150">
                        <p class="txt">応募の取消などのお問い合わせはコチラ</p>
                        <p class="img"><img src="<?php echo base_url();?>images/btn_tel.png" width="200" height="25" alt=""/></p>
                        <p class="txt">タップで電話がかかります</p>
                    </a>
                </div>
                <!-- btnTel end -->
            </div>
            <!-- applicationList end -->
        </main>
        <aside id="subCo">
            <!-- historyBox start -->
            <?php if(!empty($looks)):?>
                <div id="historyBox">
                    <h2><span>最近閲覧した求人</span></h2>
                    <div class="inner">
                        <?php foreach($looks as $k=>$v):?>
                            <!-- entry start -->
                            <div class="entry">
                                <p><a href="<?php echo base_url();?>search/detail/<?php echo htmlspecialchars($v->job_id, ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v->job_title, ENT_QUOTES, "UTF-8");?></a></p>
                            </div>
                            <!-- entry end -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endif;?>
            <!-- historyBox end -->
            <script>
                $(function(){
                    $(".typeBox h2.tap").next().hide();
                    $(".typeBox h2.tap").on("click", function() {
                        $(this).next().slideToggle(1);
                        $(this).toggleClass('active');
                    });
                    <?php if(!empty($sort) && @$sort=="offer"):?>
                    $(".typeBox h2").eq(0).click();
                    <?php endif;?>
                });
            </script>
            <script>
                if ( window.matchMedia( 'screen and (min-width:769px)' ).matches ) {
                    $('#applicationList .typeBox h2').on('click',function() {

                        setTimeout(function(){
                            $('#applicationList .jobList.b01 .jobBox h3').tile(2);
                            $('#applicationList .jobList.b01 .jobBox .tableWrap').tile(2);
                            $('#applicationList .jobList.b02 .jobBox h3').tile(2);
                            $('#applicationList .jobList.b02 .jobBox .tableWrap').tile(2);
                            $('#applicationList .jobList.b03 .jobBox h3').tile(2);
                            $('#applicationList .jobList.b03 .jobBox .tableWrap').tile(2);
                            $('#applicationList .jobList.b04 .jobBox h3').tile(2);
                            $('#applicationList .jobList.b04 .jobBox .tableWrap').tile(2);
                            $('#applicationList .jobList.b05 .jobBox h3').tile(2);
                            $('#applicationList .jobList.b05 .jobBox .tableWrap').tile(2);
                            $('#applicationList .jobList.b06 .jobBox h3').tile(2);
                            $('#applicationList .jobList.b06 .jobBox .tableWrap').tile(2);
                            $('#applicationList .jobList.b07 .jobBox h3').tile(2);
                            $('#applicationList .jobList.b07 .jobBox .tableWrap').tile(2);
                            $('#applicationList .jobList.b08 .jobBox h3').tile(2);
                            $('#applicationList .jobList.b08 .jobBox .tableWrap').tile(2);
                        },100);
                        return false;

                    } );
                }
            </script>