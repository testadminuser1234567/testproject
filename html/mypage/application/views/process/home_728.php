<?php
//HRBCマスターと連動

//職種の設定
$tmpjob = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjob = json_decode($tmpjob,true);

$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmparea = json_decode($tmparea,true);

$tmptenkin = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/tenkin.json', true);
$tmptenkin = json_decode($tmptenkin,true);

?>
<!-- co start -->
<section class="co jobList">
    <div class="inner">
        <h1><span class="line">応募状況</span></h1>

        <!-- select -->
        <div id="phaseBox">
            <div class="inner clearfix">
                <h2>フェーズで絞り込む</h2>
                <p>
                    <select name="phase" id="phase">
                        <option value="">選択してください</option>
                        <option value="<?php echo urlencode('求人詳細問合せ中');?>" <?php if($sort=="求人詳細問合せ中"):?>selected<?php endif;?>>求人詳細問合せ中</option>
                        <option value="<?php echo urlencode('オファー');?>" <?php if($sort=="オファー"):?>selected<?php endif;?>>オファー</option>
                        <option value="<?php echo urlencode('辞退');?>" <?php if($sort=="辞退"):?>selected<?php endif;?>>辞退</option>
                        <option value="<?php echo urlencode('応募中');?>" <?php if($sort=="応募中"):?>selected<?php endif;?>>応募中</option>
                        <option value="<?php echo urlencode('見送り');?>" <?php if($sort=="見送り"):?>selected<?php endif;?>>見送り</option>
                        <option value="<?php echo urlencode('選考中');?>" <?php if($sort=="選考中"):?>selected<?php endif;?>>面接</option>
                        <option value="<?php echo urlencode('内定');?>" <?php if($sort=="内定"):?>selected<?php endif;?>>内定</option>
                        <option value="<?php echo urlencode('入社予定');?>" <?php if($sort=="入社予定"):?>selected<?php endif;?>>入社予定</option>
                        <option value="<?php echo urlencode('案件終了');?>" <?php if($sort=="案件終了"):?>selected<?php endif;?>>案件終了</option>
                    </select>
                </p>
            </div>
        </div>
        <script type="text/javascript">
            $(function(){
                $("#phase").change(function(){
                    phase = $("#phase").val();
                    url = "<?php echo base_url() . $this->uri->segment(1) . '/' . $this->uri->segment(2) . '/';?>" + phase;
                    location.href = url;
                });
            });
        </script>

        <?php if(!empty($processes)):?>
            <p><span class="point"><?php echo $total;?></span>件中 <?php echo $offset;?>〜<?php echo $hani;?>件の検索結果を表示しています。</p>
            <!-- pagenation start -->
            <?php if(!empty($paging)):?>

                <div class="pagenation">
                    <?php echo $paging ?>
                </div>
            <?php endif;?>
            <!-- pagenation end -->



            <!-- box start -->
            <?php foreach ($processes as $k => $v): ?>
                <?php if($v["mypageCheck"]!=11312):?>
                    <a id="<?php echo $clean->purify($v["job"]["job_id"]);?>"></a>
                    <div class="box">
                    <div class="inner">
                    <p class="tag"><?php echo $v["phaze"];?>
                        <input type="hidden" name="phaseId" value="<?php echo $clean->purify($v["phazeId"]);?>" /></p>
                    <h2><a href="<?php echo base_url();?>search/detail/<?php echo $clean->purify($v["job"]["job_id"]);?>"><?php echo $clean->purify($v["job"]["job_title"]);?></a></h2>
                    <table>
                        <tbody>
                        <tr>
                            <th>企業</th>
                            <td>
                                <?php if($v['clientNameDisplay']=="公開"):?>
                                    <?php echo $clean->purify($v["client"]["c_title"]);?>
                                <?php else:?>
                                    -
                                <?php endif?>
                            </td>
                        </tr>
                        <tr>
                            <th>職種</th>
                            <td><?php echo $clean->purify($v["jobcategoryName"]);?></td>
                        </tr>
                        <tr>
                            <th>更新日</th>
                            <td>
                                <?php echo $clean->purify($v["phasedate"]);?>
                            </td>
                        </tr>
                    </table>
                    <!-- btnBox start -->
                    <?php if($v["phaze"]=="オファー"):?>
                        <div class="btnBox centered" id="lists_<?php echo $clean->purify($v["job"]["job_id"]);?>">
                            <ul class="clearfix">
                                <?php if($v["job"]["phaze"]==$phaze):?>
				<?php $tmp_id1 = "frm1_" . $clean->purify($v["job"]["job_id"]);?>
				<?php $tmp_id2 = "frm2_" . $clean->purify($v["job"]["job_id"]);?>
                                    <li>
					<?php echo form_open_multipart('process/apply',"id=\"$tmp_id1\"");?>
                                        <input type="button" name="apply_submit" id="apply_<?php echo $clean->purify($v["job"]["job_id"]);?>" class="application apply" value="応募する" />
                                        <input type="hidden" name="job_name" value="<?php echo $clean->purify($v["job"]["job_title"]);?>" />
                                        <input type="hidden" name="job_id" value="<?php echo $clean->purify($v["job"]["job_id"]);?>" />
                                        <input type="hidden" name="client_id" value="<?php echo $clean->purify($v["client"]["c_id"]);?>" />
                                        <input type="hidden" name="client_name" value="<?php echo $clean->purify($v["client"]["c_title"]);?>" />
                                        <input type="hidden" name="processId" value="<?php echo $clean->purify($v["processId"]);?>" />
                                        <input type="hidden" name="phaseId" value="<?php echo $clean->purify($v["phazeId"]);?>" />
                                        <input type="hidden" name="phasedate" value="<?php echo $clean->purify($v["phasedate"]);?>" />
                                        <input type="hidden" name="phazeFlg" value="<?php echo $clean->purify($v["phazeFlg"]);?>" />
					<?php echo form_close();?>
                                    </li>
                                    <li>
					<?php echo form_open_multipart('process/refusal',"id=\"$tmp_id2\"");?>
                                        <input type="button" name="refuse_submit" id="refusal_<?php echo $clean->purify($v["job"]["job_id"]);?>" class="application refusal" value="辞退する" />
                                        <input type="hidden" name="job_name" value="<?php echo $clean->purify($v["job"]["job_title"]);?>" />
                                        <input type="hidden" name="job_id" value="<?php echo $clean->purify($v["job"]["job_id"]);?>" />
                                        <input type="hidden" name="client_id" value="<?php echo $clean->purify($v["client"]["c_id"]);?>" />
                                        <input type="hidden" name="client_name" value="<?php echo $clean->purify($v["client"]["c_title"]);?>" />
                                        <input type="hidden" name="processId" value="<?php echo $clean->purify($v["processId"]);?>" />
                                        <input type="hidden" name="phaseId" value="<?php echo $clean->purify($v["phazeId"]);?>" />
                                        <input type="hidden" name="phasedate" value="<?php echo $clean->purify($v["phasedate"]);?>" />
                                        <input type="hidden" name="phazeFlg" value="<?php echo $clean->purify($v["phazeFlg"]);?>" />
					<?php echo form_close();?>
                                    </li>
                                <?php else:?>
                                    <li>
                                        募集終了
                                    </li>
                                <?php endif;?>
                            </ul>
                        </div>
                    <?php endif;?>
                <?php else:?>
                    -
                <?php endif;?>

                </div>
                </div>
                <!-- box end -->
            <?php endforeach; ?>
    <script>
        $(function(){
            $(".apply").click(function() {
                if(confirm("お問い合わせまたはご応募いただき、ありがとうございます。求人の詳細または応募については、確認次第、順次ご回答いたします。")) {
			apply_tmp_id = $(this).attr("id");
	                var apply_info = apply_tmp_id.split( "_" );

        	        var apply_id = apply_info[0];
	                var job_id = apply_info[1];

　　		        $("#lists_" + job_id).before("<div id='loading' style='text-align:center;margin-right:0px;'><img src='<?php echo base_url();?>images/loading.gif' /></div>");
		        $("#lists_" + job_id).hide();


			var form_id = "frm1_" + job_id;
			ga('send', 'event', 'myPageApply', 'CTA', 'entryButton_Apply');
			$('#'+form_id).submit();
		}
            })

            $(".refusal").click(function() {
                if(confirm("応募を辞退しますか？")) {
			refusal_tmp_id = $(this).attr("id");
	                var refusal_info = refusal_tmp_id.split( "_" );

        	        var refusal_id = refusal_info[0];
	                var job_id = refusal_info[1];

　　		        $("#lists_" + job_id).before("<div id='loading' style='text-align:center;margin-right:0px;'><img src='<?php echo base_url();?>images/loading.gif' /></div>");
		        $("#lists_" + job_id).hide();


			var form_id = "frm2_" + job_id;
			ga('send', 'event', 'myPageApply', 'CTA', 'entryButton_notApply');
			$('#'+form_id).submit();
		}
            })
	});
    </script>
            <p class="txtTel"><a href="tel:0120141150">応募の取り消しは<strong>お電話(0120-14-1150)</strong>にて承ります。</a></p>


            <p><span class="point"><?php echo $total;?></span>件中 <?php echo $offset;?>〜<?php echo $hani;?>件の検索結果を表示しています。</p>
            <!-- pagenation start -->
            <?php if(!empty($paging)):?>

                <div class="pagenation">
                    <?php echo $paging ?>
                </div>
            <?php endif;?>

            <!-- pagenation end -->
        <?php else:?>
            <p>現在、応募はありません。</p>
        <?php endif;?>
    </div>
</section>
