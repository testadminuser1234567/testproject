<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Express extends MY_Controller {

    //////////////////////////////////////////////////////////
    //conf
    //////////////////////////////////////////////////////////

    //カテゴリー、画像、ソート、日付指定がありの場合はon ※ソートと日付はどっちか一つ
    protected $use = array("category"=>"on", "file"=>"off", "sort" => "off", "date"=>"on", "display"=>"on");

    //カテゴリーのtable名
    protected $category_table = "expresses_categories";

    //タイトル
    protected $sub_title = "高速バス";

    //segment
    protected $segment = "express";

    //database
    protected $database = "expresses";

    //画像がonの時 許可ファイル
    protected $allow = 'gif|jpg|png|jpeg|doc|xls|ppt|pdf';

    //サムネイル有り無し（PDFなどの時はなし)
    protected $thumb = "off";
    protected $width = 100;
    protected $height = 100;

    //ページネーションの数
    protected $page_limit = 10000000000;

    //////////////////////////////////////////////////////////
    // construct
    //////////////////////////////////////////////////////////
    function __construct()
    {
        parent::__construct();
    }

    //////////////////////////////////////////////////////////
    //一覧出力
    //////////////////////////////////////////////////////////
    function home()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');
        $role = $this->session->userdata('login_role');
        $this->_role_check($role);

        $cat = $this->uri->segment(3);
        //カテゴリー取得
        $ret = $this->db->order_by('category_sort asc')->get_where($this->category_table, array("category_del"=>0, "category_id"=>$cat));
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
        $category = $ret->result();
        $cat_id = @$category[0]->category_id;
        $data["category"]["category_id"] = $cat_id;
        $cat_title = @$category[0]->category_title;
        $data["category"]["category_title"] = $cat_title;

        //カテゴリーチェック
        if (empty($cat_id) || empty($cat_title))
        {
            $this->session->set_userdata(array("msg"=>"不正な処理が行われました。"));
            redirect("auth/logout");
        }

        $posts = $this->input->post();

        $msg = $this->session->userdata('msg');
        $this->session->unset_userdata('msg');

        $params=array();
        $params["category_id"] = $cat_id;
        $params["del"] = 0;

        $disp="";
        $url="/";
        $order ="";
        /*
            //order sort or create_date
            if($this->use["sort"]=="on"){
                $order="sort asc";
            }else if($this->use["date"] == "on"){
                $order="create_date desc, updated desc";
            }else{
                echo "ソートか日付の設定をして下さい。";
                exit;
            }
        */
        $order="sort asc";

        //Pagination
        $this->load->library("pagination");
        $config['base_url'] = base_url() . $this->uri->segment(1) . "/" . $this->uri->segment(2) ."/" . $url;
        $config["total_rows"] = $this->db->get_where($this->database, $params)->num_rows();
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

        $config["per_page"] = $this->page_limit;
        $config["num_links"] = 2;
        $config['page_query_string'] = TRUE;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><span class="current">';
        $config['cur_tag_close'] = '</span></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);

        //data設定
        if(!empty($this->input->get('per_page'))){
            $offset = $this->input->get('per_page');
        }else{
            $offset = 0;
        }
        $data["contents"] = $this->db->order_by($order)->get_where($this->database, $params, $config["per_page"], $offset);
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
        $data["render"] = $this->segment."/home";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $msg;
        $data["cat"] = $cat;
        $data["disp"] = $disp;
        $data["use"] = $this->use;
        $data["sub_title"] = $this->sub_title;
        $data["segment"] = $this->segment;
        $data["role"] = $role;

        $this->load->view("template", $data);
    }

    //////////////////////////////////////////////////////////
    //登録情報入力
    //////////////////////////////////////////////////////////
    function regist()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');
        $role = $this->session->userdata('login_role');
        $this->_role_check($role);

        $posts = $this->input->post();

        //カテゴリー取得
        $cat = $this->uri->segment(3);
        //カテゴリー取得
        $ret = $this->db->order_by('category_sort asc')->get_where($this->category_table, array("category_del"=>0, "category_id"=>$cat));
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

        $category = $ret->result();
        $cat_id = @$category[0]->category_id;
        $data["category"]["category_id"] = $cat_id;
        $cat_title = @$category[0]->category_title;
        $data["category"]["category_title"] = $cat_title;

        //カテゴリーチェック
        if (empty($cat_id) || empty($cat_title))
        {
            $this->session->set_userdata(array("msg"=>"不正な処理が行われました。"));
            redirect("auth/logout");
        }


        //idが設定されていて戻るじゃない場合は編集
        if(!empty($posts['id']) && empty($posts['mode'])){
            $data[$this->segment] = $this->db->get_where($this->database, array('id' => $posts['id']))->row();
            $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
        }else{
            $data[$this->segment] = (object) $posts;
            //idがない場合は空データ設定
            if(empty($data[$this->segment]->id)){
                $data[$this->segment]->id = "";
            }
        }

        $error = "";

        //data設定
        $data["render"] = $this->segment."/regist";
        $data["cat"] = $cat;
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = "";
        $data["use"] = $this->use;
        $data["sub_title"] = $this->sub_title;
        $data["role"] = $role;
        $data["segment"] = $this->segment;

        $this->load->view("template", $data);

    }

    //////////////////////////////////////////////////////////
    //登録情報確認
    //////////////////////////////////////////////////////////
    function confirm()
    {
        //カテゴリー取得
        $data["categories"] = $this->db->order_by('category_sort asc')->get_where($this->category_table, array("category_del"=>0, "category_display"=>1));
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

        //バリデーション実行
        $error = "";
        $tmp_error = 0;
        $e_msg = "";
        $error = $this->setValidation();

        $posts = $this->input->post();

        //オブジェクトへ変換
        $data[$this->segment] = (object)$posts;


        if($error !== ""){
            //data設定
            $data["render"] = $this->segment."/regist";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["use"] = $this->use;
            $data["sub_title"] = $this->sub_title;
            $data["msg"] = $error;
            $data["segment"] = $this->segment;

            $this->load->view("template", $data);
        }else{
            if($this->use["file"] == "on"){
                //fileアップロード
                $result=array();
                $files = $_FILES;
                $cpt = count($files['userfile']['name']);
                for ($i = 0; $i < $cpt; $i++) {
                    $filename="";
                    $require=1;
                    $allow =  $this->allow;
                    $overwrite=true;
                    //@param(post, num, files, directory, require, filename)
                    $result[]= $this->fileUpload(@$posts['image'.($i+1)], $i , $files, $this->segment, $require, $filename, $overwrite,$allow);

                    if(!empty($result[$i]['error'])){
                        $tmp_error = 1;
                        $tmp_cnt = $i+1;
                        $e_msg .=  $result[$i]['error'] . "ファイル". $tmp_cnt . "<br />";
                    }
                }

                if($this->thumb=="on"){
                    //サムネイル
                    if(!empty($result[0]['upload_data']['full_path'])) $this->resizeImage($result[0]['upload_data']['full_path'], $this->width, $this->height, "_thumb", "auto");
                }

                /*
                /   画像とアップロードファイルの設定
                /	１．アップロードされている場合はそのファイルを使用
                /	２．アップロードがない場合はpostデータ（元々のファイルか未設定）を使用
                /	３．削除チェックしてある場合は設定しない
                */

                /////////////////////////////////////////////////////////////////////////////
                //1枚目
                /////////////////////////////////////////////////////////////////////////////
                if(!empty($result[0]['upload_data']['file_size']) || !empty($posts['image1'])){
                    (!empty($result[0]['upload_data']['file_size'])) ? $data[$this->segment]->image1=basename($result[0]['upload_data']['file_name']) : $data[$this->segment]->image1=$posts['image1'];
                }else{
                    $data[$this->segment]->image1="";
                }
                //if(@$posts['image1_del'] == "on"){
                //	$data[$this->segment]->image1="";
                //}

                /////////////////////////////////////////////////////////////////////////////
                //2枚目以降
                /////////////////////////////////////////////////////////////////////////////
                for ($i = 1; $i < $cpt; $i++) {
                    if(!empty($result[$i]['upload_data']['file_size']) || !empty($posts['image'.($i+1)])){
                        (!empty($result[$i]['upload_data']['file_size'])) ? $data[$this->segment]->{"image".($i+1)}=basename($result[$i]['upload_data']['file_name']) : $data[$this->segment]->{"image".($i+1)}=$posts['image'.($i+1)];
                    }else{
                        $data[$this->segment]->{"image".($i+1)}="";
                    }
                    if(@$posts['image' . ($i+1) . '_del'] == "on"){
                        $data[$this->segment]->{"image".($i+1)}="";
                    }
                }
            }

            if($tmp_error != 1){
                //data設定
                $data["render"] = $this->segment."/confirm";
                $data["clean"] = $this->clean;
                $data["csrf"] = $this->_get_csrf_nonce();
                $data["msg"] = $error;
                $data["use"] = $this->use;
                $data["sub_title"] = $this->sub_title;
                $data["segment"] = $this->segment;

                $this->load->view("template", $data);
            }else{
                $error="";
                $error .= "<br />" . $e_msg;

                //data設定
                $data["render"] = $this->segment."/regist";
                $data["clean"] = $this->clean;
                $data["csrf"] = $this->_get_csrf_nonce();
                $data["msg"] = $error;
                $data["use"] = $this->use;
                $data["sub_title"] = $this->sub_title;
                $data["segment"] = $this->segment;

                $this->load->view("template", $data);
            }
        }
    }

    //////////////////////////////////////////////////////////
    //登録処理
    //////////////////////////////////////////////////////////
    function add()
    {
        //カテゴリー取得
        $cat = $this->uri->segment(3);
        //カテゴリー取得
        $ret = $this->db->order_by('category_sort asc')->get_where($this->category_table, array("category_del"=>0, "category_id"=>$cat));
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

        $error = "";

        $category = $ret->result();
        $cat_id = @$category[0]->category_id;
        $data["category"]["category_id"] = $cat_id;
        $cat_title = @$category[0]->category_title;
        $data["category"]["category_title"] = $cat_title;

        if($_POST["content"]=="<p><br></p>"){
            $_POST["content"]="";
        }
        if($_POST["ryokin_calendar"]=="<p><br></p>"){
            $_POST["ryokin_calendar"]="";
        }

        $posts = $this->input->post();

        //オブジェクトへ変換
        $data[$this->segment] = (object)$posts;

        //プレビュー
        if($posts["action"]=="preview"){
            //data設定
            $data["render"] = $this->segment."/preview";
            $data["clean"] = $this->clean;
            $data["csrf"] = $this->_get_csrf_nonce();
            $data["msg"] = $error;
            //$data["error2"] = $error2;
            $data["use"] = $this->use;
            $data["sub_title"] = $this->sub_title;
            //$data["role"] = $role;
            $data["segment"] = $this->segment;

            //同じカテゴリーのデータ取得
            $params=array();
            $params["category_id"] = $cat_id;
            $params["display"] = 1;
            $params["del"] = 0;
            if($posts["id"]){
                $params["id != "] = $posts["id"];
            }
            $tmpsame = $this->db->order_by("sort asc")->get_where($this->database, $params);
            $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
            $data["link"] = $tmpsame->result();

            $this->load->view("template_preview", $data);
        }else{

            $this->_login_check();
            $login_id = $this->session->userdata('login_id');
            $role = $this->session->userdata('login_role');
            $this->_role_check($role);


            //バリデーション実行
            $error = $this->setValidation();

            $error2="";
            if(empty($posts["facility1"]) && empty($posts["facility2"]) && empty($posts["facility3"]) && empty($posts["facility4"]) && empty($posts["facility5"]) && empty($posts["facility6"]) && empty($posts["facility7"]) && empty($posts["facility8"])){
                $error2 = "<p class=\"error\">設備は必須フィールドです</p>";
            }

            //カテゴリーチェック
            if (empty($cat_id) || empty($cat_title))
            {
                $this->session->set_userdata(array("msg"=>"不正な処理が行われました。"));
                redirect("auth/logout");
            }

            if($error !== "" || $error2 !== ""){
                if(empty($error)) $error = $error2;
                //data設定
                $data["render"] = $this->segment."/regist";
                $data["clean"] = $this->clean;
                $data["csrf"] = $this->_get_csrf_nonce();
                $data["msg"] = $error;
                $data["use"] = $this->use;
                $data["error2"] = $error2;
                $data["sub_title"] = $this->sub_title;
                $data["segment"] = $this->segment;
                $data["role"] = $role;

                $this->load->view("template", $data);
            }else{
                //idありの場合、現在のカテゴリー、ソート、表示/非表示
                $tmpCat="";
                $tmpSort="";
                $tmpDisp="";
                $tmpDatas = $this->db->get_where($this->database, array('id' => $posts['id']))->row();
                $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
                if(!empty($tmpDatas)){
                    $tmpCat = $tmpDatas->category_id;
                    $tmpSort = $tmpDatas->sort;
                    $tmpDisp = $tmpDatas->display;
                }

                if(empty($posts['id'])){
                    $insert['created'] = date('Y-m-d');
                }
                $insert['id'] = $posts['id'];
                $insert['description'] = $posts['description'];
                $insert['content'] = $posts['content'];
                $insert['category_id'] = $cat_id;
                $insert['create_date'] = $posts['create_date'];

                $insert['link_text'] = $posts['link_text'];
                $insert['link'] = $posts['link'];
                $insert['reserve'] = $posts['reserve'];
                $insert['facility1'] = $posts['facility1'];
                $insert['facility2'] = $posts['facility2'];
                $insert['facility3'] = $posts['facility3'];
                $insert['facility4'] = $posts['facility4'];
                $insert['facility5'] = $posts['facility5'];
                $insert['facility6'] = $posts['facility6'];
                $insert['facility7'] = $posts['facility7'];
                $insert['facility8'] = $posts['facility8'];
                $insert['discount1'] = $posts['discount1'];
                $insert['discount2'] = $posts['discount2'];
                $insert['discount3'] = $posts['discount3'];
                $insert['discount4'] = $posts['discount4'];
                $insert['discount5'] = $posts['discount5'];
                $insert['discount6'] = $posts['discount6'];
                $insert['discount7'] = $posts['discount7'];
                $insert['fee'] = $posts['fee'];
                $insert['busnet_url'] = $posts['busnet_url'];
                $insert['orainet_url'] = $posts['orainet_url'];
                $insert['travel_form'] = $posts['travel_form'];
                $insert['travel_company'] = $posts['travel_company'];
                $insert['travel_distance'] = $posts['travel_distance'];
                $insert['travel_time'] = $posts['travel_time'];
                $insert['travel_driver'] = $posts['travel_driver'];
                $insert['travel_insurance'] = $posts['travel_insurance'];
                $insert['tel_reserve_flg'] = $posts['tel_reserve_flg'];
                $insert['tel_reserve_text'] = $posts['tel_reserve_text'];
                $insert['orainet_reserve_flg'] = $posts['orainet_reserve_flg'];
                $insert['orainet_reserve_text'] = $posts['orainet_reserve_text'];
                $insert['busnet_reserve_flg'] = $posts['busnet_reserve_flg'];
                $insert['busnet_reserve_text'] = $posts['busnet_reserve_text'];
                $insert['incar_flg'] = $posts['incar_flg'];
                $insert['incar_text'] = $posts['incar_text'];
                $insert['credit_flg'] = $posts['credit_flg'];
                $insert['credit_text'] = $posts['credit_text'];
                $insert['money_flg'] = $posts['money_flg'];
                $insert['money_text'] = $posts['money_text'];
                $insert['kyodo_flg'] = $posts['kyodo_flg'];
                $insert['kyodo_text'] = $posts['kyodo_text'];
                $insert['busta_flg'] = $posts['busta_flg'];
                $insert['busta_text'] = $posts['busta_text'];
                $insert['jrkanto_flg'] = $posts['jrkanto_flg'];
                $insert['jrkanto_text'] = $posts['jrkanto_text'];
                $insert['lawson_flg'] = $posts['lawson_flg'];
                $insert['lawson_text'] = $posts['lawson_text'];
                $insert['famima_flg'] = $posts['famima_flg'];
                $insert['famima_text'] = $posts['famima_text'];
                $insert['sankusu_flg'] = $posts['sankusu_flg'];
                $insert['sankusu_text'] = $posts['sankusu_text'];
                $insert['ministop_flg'] = $posts['ministop_flg'];
                $insert['ministop_text'] = $posts['ministop_text'];
                $insert['jtb_flg'] = $posts['jtb_flg'];
                $insert['jtb_text'] = $posts['jtb_text'];
                $insert['seikyo_flg'] = $posts['seikyo_flg'];
                $insert['seikyo_text'] = $posts['seikyo_text'];
                $insert['seven_flg'] = $posts['seven_flg'];
                $insert['seven_text'] = $posts['seven_text'];
                $insert['content'] = $posts['content'];
                $insert['jikoku_text'] = $posts['jikoku_text'];
                $insert['jikoku_file'] = $posts['jikoku_file'];
                $insert['jikoku_table'] = $posts['jikoku_table'];
                $insert['ryokin_text'] = $posts['ryokin_text'];
                $insert['ryokin_file'] = $posts['ryokin_file'];
                $insert['ryokin_table'] = $posts['ryokin_table'];
                $insert['ryokin_calendar'] = $posts['ryokin_calendar'];
                $insert['bustei_text'] = $posts['bustei_text'];
                $insert['bustei'] = $posts['bustei'];

                //画像ありの場合
                if($this->use["file"] == "on"){
                    $insert['image1'] = $posts['image1'];
                }

                //表示/非表示ありの場合
                if($this->use["display"] == "on"){
                    $insert['display'] = $posts['display'];
                }

                $this->load->model($this->database . '_model');
                $tmpDb=$this->database."_model";
                $ret = $this->$tmpDb->save($insert['id'], $insert, "id");
                $this->saveLog($this->segment . "/" . $this->uri->segment(2) . "/" . $this->uri->segment(3) , $this->db->last_query());

                //ソートの最後に入れる
                if(empty($posts['id'])){
                    $insert_id = $this->db->insert_id();
                    $sortDatas = $this->db->order_by('sort', 'desc')->get_where($this->database, array('del'=> 0, 'category_id' => $cat_id))->row();
                    $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
                    if(!empty($sortDatas)){
                        $sort_last = $sortDatas->sort + 1;
                        $data = array('sort' => $sort_last);
                        $this->db->where('id', $insert_id);
                        $this->db->update($this->database, $data);
                        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
                    }
                }
                $url="";

                if(empty($ret)){
                    //エラー処理
                }else{
                    //登録成功
                    $this->session->set_userdata(array("msg"=>"登録が完了しました。"));
                    redirect($this->segment."/home/".$cat_id . "/");
                }
            }
        }
    }

    //////////////////////////////////////////////////////////
    //登録情報削除
    //////////////////////////////////////////////////////////
    function delete()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');
        $role = $this->session->userdata('login_role');
        $this->_role_check($role);

        $posts = $this->input->post();

        //CSRFチェック
        /*
            if ($this->_valid_csrf_nonce() === FALSE)
            {
                $this->session->set_userdata(array("msg"=>"不正な処理が行われました。"));
                redirect($this->segment."/home");
            }
        */

        $url="";

        if($posts['id'] && is_numeric($posts['id'])){
            $data = array(
                'del' => 1,
                'sort' => 99999999
            );
            $this->db->where('id', $posts['id']);
            $ret = $this->db->update($this->database, $data);
            $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

            if($ret){
                /*
                        if($this->use["sort"] == "on"){

                            //表示/非表示の情報を取得
                            $tmpDisp="";
                            $tmpDatas = $this->db->get_where($this->database, array('id' => $posts['id']))->row();
                            if(!empty($tmpDatas)){
                                $tmpDisp = $tmpDatas->display;
                            }

                            //表示/非表示ありの場合で非表示の場合は何もしない
                            if($this->use["display"] == "on" && $tmpDisp==2){
                            }else{
                                //ソート番号の並び替え（カウントダウン）
                                $query = $this->db->order_by('sort asc')->get_where($this->database, array("del"=>0, 'category_id'=>$posts['category_id'], 'sort >'=>$posts["sort"], "display"=>1)); ;
                                foreach ($query->result() as $row){
                                    $sort_num=$row->sort - 1;
                                    $data = array('sort' => $sort_num);
                                    $this->db->where('id', $row->id);
                                    $this->db->update($this->database, $data);
                                }
                            }
                        }
                */
                //ソート番号の並び替え（カウントダウン）
                $query = $this->db->order_by('sort asc')->get_where($this->database, array("del"=>0, 'category_id'=>$posts['category_id'], 'sort >'=>$posts["sort"])); ;
                $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
                foreach ($query->result() as $row){
                    $sort_num=$row->sort - 1;
                    $data = array('sort' => $sort_num);
                    $this->db->where('id', $row->id);
                    $this->db->update($this->database, $data);
                    $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
                }

                //カテゴリーありの場合
                if($this->use["category"] == "on"){
                    $url .= $posts["category_id"] . "/";
                }

                //表示/非表示ありの場合
                if($this->use["display"] == "on"){
                    $url .= $posts["display"] . "/";
                }

                //削除成功
                $this->session->set_userdata(array("msg"=>"削除が完了しました。"));
                redirect($this->segment."/home/".$url);
            }else{
                $this->session->set_userdata(array("msg"=>"削除に失敗しました。"));
                redirect($this->segment."/home/".$url);
            }
        }else{
            redirect("auth/login");
        }
    }

    //////////////////////////////////////////////////////////
    //公開/非公開
    //////////////////////////////////////////////////////////
    function display()
    {
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');
        $role = $this->session->userdata('login_role');
        $this->_role_check($role);

        $id = $this->uri->segment(3);
        $cat = $this->uri->segment(4);
        //idチェック
        if (empty($id) || !is_numeric($id))
        {
            $this->session->set_userdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/home");
        }
        $data = $this->db->get_where($this->database, array('id' => $id))->row();
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
        $display = $data->display;
        if($display==1){
            $new_display = 2;
        }else{
            $new_display = 1;
        }

        $param = array('display' => $new_display);
        $this->db->where('id', $id);
        $this->db->update($this->database, $param);
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

        $url="";
        $this->session->set_userdata(array("msg"=>"処理が完了しました。"));
        redirect($this->segment."/home/".$cat . "/");
    }

    //////////////////////////////////////////////////////////
    //複製
    //////////////////////////////////////////////////////////
    function copy(){
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');
        $role = $this->session->userdata('login_role');
        $this->_role_check($role);

        $posts = $this->input->post();
        if(empty($posts["id"]))
        {
            $this->session->set_userdata(array("msg"=>"不正な処理が行われました。"));
            redirect($this->segment."/home".$posts["category_id"]);
        }

        $data = $this->db->get_where($this->database, array('id' => $posts["id"]))->row();
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

        $insert = (array)$data;
        $insert["id"] = "";
        $insert["description"] = $insert["description"] . "(複製)";

        $sortDatas = $this->db->order_by('sort', 'desc')->get_where($this->database, array('del'=> 0, 'category_id' => $posts["category_id"]))->row();
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
        if(!empty($sortDatas)){
            $sort = $sortDatas->sort + 1;
        }else{
            $sort = 99999999999;
        }
        $insert["sort"] = $sort;

        //複製は非表示
        $insert["display"] = 2;

        $this->load->model($this->database . '_model');
        $tmpDb=$this->database."_model";
        $ret = $this->$tmpDb->save($insert['id'], $insert, "id");
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

        //登録成功
        $this->session->set_userdata(array("msg"=>"登録が完了しました。"));
        redirect($this->segment."/home/".$posts["category_id"] . "/");
    }

    //////////////////////////////////////////////////////////
    //プレビュー
    //////////////////////////////////////////////////////////
    function preview(){
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');
        $role = $this->session->userdata('login_role');
        $this->_role_check($role);

        $posts = $this->input->post();

        //カテゴリー取得
        $cat = $this->uri->segment(3);
        //カテゴリー取得
        $ret = $this->db->order_by('category_sort asc')->get_where($this->category_table, array("category_del"=>0, "category_id"=>$cat));
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
        $category = $ret->result();
        $cat_id = @$category[0]->category_id;
        $data["category"]["category_id"] = $cat_id;
        $cat_title = @$category[0]->category_title;
        $data["category"]["category_title"] = $cat_title;

        //カテゴリーチェック
        if (empty($cat_id) || empty($cat_title))
        {
            $this->session->set_userdata(array("msg"=>"不正な処理が行われました。"));
            redirect("auth/logout");
        }

        $error="";
        //オブジェクトへ変換
        $data[$this->segment] = $this->db->get_where($this->database, array('id' => $posts['id']))->row();
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

        //プレビュー
        //data設定
        $data["render"] = $this->segment."/preview";
        $data["clean"] = $this->clean;
        $data["csrf"] = $this->_get_csrf_nonce();
        $data["msg"] = $error;
        $data["use"] = $this->use;
        $data["sub_title"] = $this->sub_title;
        $data["segment"] = $this->segment;

        //バス停情報ソート順にする
        /*
            $busteis=array();
            $k=0;
            for($i=1;$i<=30;$i++){
                $num1 = "bustei_title".$i;
                $tmp_title = $data[$this->segment]->$num1;
                $num2 = "bustei_address".$i;
                $tmp_address = $data[$this->segment]->$num2;
                $num3 = "bustei_sort".$i;
                $tmp_sort = $data[$this->segment]->$num3;

                if(!empty($tmp_title) || !empty($tmp_address) || !empty($tmp_sort)){
                    $busteis[$k]["bustei_title"] = $tmp_title;
                    $busteis[$k]["bustei_address"] = $tmp_address;
                    $busteis[$k]["bustei_sort"] = $tmp_sort;
                    $k++;
                }
            }
            //並び替え
            foreach ((array) $busteis as $key => $value) {
                $sort[$key] = $value['bustei_sort'];
            }
            array_multisort($sort, SORT_ASC, $busteis);
            $data["bustei"] = $busteis;
        */

        //同じカテゴリーのデータ取得
        $params=array();
        $params["category_id"] = $cat_id;
        $params["display"] = 1;
        $params["del"] = 0;
        if($posts["id"]){
            $params["id != "] = $posts["id"];
        }
        $tmpsame = $this->db->order_by("sort asc")->get_where($this->database, $params);
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
        $data["same"] = $tmpsame->result();

        $this->load->view("template_preview", $data);
    }

    ///////////////////////////////////////////////////////////
    // summernote フィアるアップ
    ///////////////////////////////////////////////////////////
    function upload(){
        $result=array();
        $files = $_FILES;
        $filename="";
        $require=1;
        $allow =  $this->allow;
        $overwrite=true;
        //@param(post, num, files, directory, require, filename)

        $result[0]= $this->wyfileUpload("", 0 , $files, $this->segment, $require, $filename, $overwrite,$allow);
        if(!empty($result[0]['error'])){
            $ret = "error";
        }else{
            $success = UploadUrl . $this->segment . "/" . $result[0]['upload_data']['file_name'];
            $ret = $success;
        }
        echo $ret;
    }

    ///////////////////////////////////////////////////////////
    // 時刻表
    ///////////////////////////////////////////////////////////
    function uploadjikoku(){
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');
        $role = $this->session->userdata('login_role');
        $this->_role_check($role);

        $result=array();
        $files = $_FILES;
        $filename="";
        $require=1;
        $allow =  "csv";
        $overwrite=true;

        //@param(post, num, files, directory, require, filename)
        $result=array();
        $upDir = UploadDirectory . $this->segment . "/jikoku";
        //ファイルアップロード
        if ( $files["file"]['size'] != 0){
            if (!file_exists($upDir)) {
                if ( !mkdir( $upDir ) ) {
                    echo "ディレクトリ作成の権限がありません。<br>管理者に問い合わせてください。";
                    exit;
                }
            }

            $_FILES["userfile"]['name']= $files["file"]['name'];
            $_FILES["userfile"]['type']= $files["file"]['type'];
            $_FILES["userfile"]['tmp_name']= $files["file"]['tmp_name'];
            $_FILES["userfile"]['error']= $files["file"]['error'];
            $_FILES["userfile"]['size']= $files["file"]['size'];

            $fn = $_FILES["userfile"]['name'];
            $ext = substr($fn, strrpos($fn, '.') + 1);
            if(empty($filename)){
                $filename = date('Ymdhis') . "_" . mt_rand() . "." .$ext;
            }
            $config['upload_path'] = $upDir;
            $config['allowed_types'] = $allow;
            $config['max_size']	= '10000000';
            $config['file_name']	= $filename;
            $config['overwrite'] = $overwrite;

            $this->upload->initialize($config);

            if ( ! $this->upload->do_upload())
            {
                $result = array('error' => $this->upload->display_errors());
                $result["error"] = "<div class=\"jikokuTbl\">" . $result["error"] . "</div>";
            }
            else
            {
                $ret = array('upload_data' => $this->upload->data());

                //CSVをテーブルに変換

                $table="";
                $cnt=0;
                $tmpTitle=0;
                $file = UploadDirectory . $this->segment . "/jikoku/" . $ret["upload_data"]["file_name"];
                if ( ( $handle = fopen ( $file, "r" ) ) !== FALSE ) {
                    $table.= "<table class=\"jikokuTbl\">\n";
                    while ( ( $data = fgetcsv ( $handle, 100000, ",", '"' ) ) !== FALSE ) {
                        $table.= "\t<tr>\n";
                        for ( $i = 0; $i < count( $data ); $i++ ) {
                            if($cnt==0){
                                $t1 = nl2br(mb_convert_encoding($data[$i],'UTF-8','sjis'));
                                $t1 = str_replace(array("\r\n", "\r", "\n"), '', $t1);
                                $table.= "\t\t<th>" . $t1 . "</th>\n";
                            }else{
                                if($tmpTitle==0){
                                    $t2 = nl2br(mb_convert_encoding($data[$i],'UTF-8','sjis'));
                                    $t2 = str_replace(array("\r\n", "\r", "\n"), '', $t2);
                                    $table.= "\t\t<td class=\"title\">" . $t2. "</td>\n";
                                }else{
                                    $t3 = nl2br(mb_convert_encoding($data[$i],'UTF-8','sjis'));
                                    $t3 = str_replace(array("\r\n", "\r", "\n"), '', $t3);
                                    $table.= "\t\t<td>" . $t3 . "</td>\n";
                                }
                                $tmpTitle++;
                            }
                        }
                        $tmpTitle=0;
                        $cnt++;
                        $table.= "\t</tr>\n";
                    }
                    $table.= "</table>\n";
                    fclose ( $handle );
                }
                $result["content"] = $table;

                $csvdata="";
                $cnt=0;
                $file = UploadDirectory . $this->segment . "/jikoku/" . $ret["upload_data"]["file_name"];
                if ( ( $handle = fopen ( $file, "r" ) ) !== FALSE ) {
                    $csvdata.= "";
                    while ( ( $data = fgetcsv ( $handle, 100000, ",", '"' ) ) !== FALSE ) {
                        $csvdata.= "";
                        for ( $i = 0; $i < count( $data ); $i++ ) {
                            if($i == count($data)-1){
                                $csvdata.= "" . str_replace(",","<comma>",str_replace(array("\r\n", "\r", "\n"), '<br>', mb_convert_encoding($data[$i],'UTF-8','sjis')));
                            }else{
                                $csvdata.= "" . str_replace(",","<comma>",str_replace(array("\r\n", "\r", "\n"), '<br>',mb_convert_encoding($data[$i],'UTF-8','sjis'))) . ",";
                            }
                        }
                        $cnt++;
                        $csvdata.= "\n";
                    }
                    $csvdata.= "";
                    fclose ( $handle );
                }
                $csvdata = rtrim($csvdata,"\n");
                $result["hidden"] = $csvdata;

                $result["error"]="";
            }
            //ファイル変更なし
        }else if(!empty($image)){
            $result[] = $upDir . $image;
        }else{
            //$result[0] = "";
            if($require == 1){
                // エラー処理
                $result['error'] = "ファイルを選択して下さい。";
            }else{
                $result="";
            }
        }
        print_r(json_encode($result));
    }

    ///////////////////////////////////////////////////////////
    // 料金表
    ///////////////////////////////////////////////////////////
    function uploadryokin(){
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');
        $role = $this->session->userdata('login_role');
        $this->_role_check($role);

        $result=array();
        $files = $_FILES;
        $filename="";
        $require=1;
        $allow =  "csv";
        $overwrite=true;

        //@param(post, num, files, directory, require, filename)
        $result=array();
        $upDir = UploadDirectory . $this->segment . "/ryokin";
        //ファイルアップロード
        if ( $files["file"]['size'] != 0){
            if (!file_exists($upDir)) {
                if ( !mkdir( $upDir ) ) {
                    echo "ディレクトリ作成の権限がありません。<br>管理者に問い合わせてください。";
                    exit;
                }
            }

            $_FILES["userfile"]['name']= $files["file"]['name'];
            $_FILES["userfile"]['type']= $files["file"]['type'];
            $_FILES["userfile"]['tmp_name']= $files["file"]['tmp_name'];
            $_FILES["userfile"]['error']= $files["file"]['error'];
            $_FILES["userfile"]['size']= $files["file"]['size'];

            $fn = $_FILES["userfile"]['name'];
            $ext = substr($fn, strrpos($fn, '.') + 1);
            if(empty($filename)){
                $filename = date('Ymdhis') . "_" . mt_rand() . "." .$ext;
            }
            $config['upload_path'] = $upDir;
            $config['allowed_types'] = $allow;
            $config['max_size']	= '10000000';
            $config['file_name']	= $filename;
            $config['overwrite'] = $overwrite;

            $this->upload->initialize($config);

            if ( ! $this->upload->do_upload())
            {
                $result = array('error' => $this->upload->display_errors());
                $result["error"] = "<div class=\"ryokinTbl\">" . $result["error"] . "</div>";
            }
            else
            {
                $ret = array('upload_data' => $this->upload->data());

                //CSVをテーブルに変換
                $table="";
                $cnt=0;
                $tmpTitle=0;
                $file = UploadDirectory . $this->segment . "/ryokin/" . $ret["upload_data"]["file_name"];
                if ( ( $handle = fopen ( $file, "r" ) ) !== FALSE ) {
                    $table.= "<table class=\"ryokinTbl\">\n";
                    while ( ( $data = fgetcsv ( $handle, 100000, ",", '"' ) ) !== FALSE ) {
                        $table.= "\t<tr>\n";
                        for ( $i = 0; $i < count( $data ); $i++ ) {
                            if($cnt==0){
                                $t1 = nl2br(mb_convert_encoding($data[$i],'UTF-8','sjis'));
                                $t1 = str_replace(array("\r\n", "\r", "\n"), '', $t1);
                                $table.= "\t\t<th>" . $t1 . "</th>\n";
                            }else{
                                if($tmpTitle==0){
                                    $t2 = nl2br(mb_convert_encoding($data[$i],'UTF-8','sjis'));
                                    $t2 = str_replace(array("\r\n", "\r", "\n"), '', $t2);
                                    $table.= "\t\t<td class=\"title\">" . $t2. "</td>\n";
                                }else{
                                    $t3 = nl2br(mb_convert_encoding($data[$i],'UTF-8','sjis'));
                                    $t3 = str_replace(array("\r\n", "\r", "\n"), '', $t3);
                                    $table.= "\t\t<td>" . $t3 . "</td>\n";
                                }
                                $tmpTitle++;
                            }
                        }
                        $tmpTitle=0;
                        $cnt++;
                        $table.= "\t</tr>\n";
                    }
                    $table.= "</table>\n";
                    fclose ( $handle );
                }
                $result["content"] = $table;
                $result["error"]="";

                $csvdata="";
                $cnt=0;
                $file = UploadDirectory . $this->segment . "/ryokin/" . $ret["upload_data"]["file_name"];
                if ( ( $handle = fopen ( $file, "r" ) ) !== FALSE ) {
                    $csvdata.= "";
                    while ( ( $data = fgetcsv ( $handle, 100000, ",", '"' ) ) !== FALSE ) {
                        $csvdata.= "";
                        for ( $i = 0; $i < count( $data ); $i++ ) {
                            if($i == count($data)-1){
                                $csvdata.= "" . str_replace(",","<comma>",str_replace(array("\r\n", "\r", "\n"), '<br>', mb_convert_encoding($data[$i],'UTF-8','sjis')));
                            }else{
                                $csvdata.= "" . str_replace(",","<comma>",str_replace(array("\r\n", "\r", "\n"), '<br>',mb_convert_encoding($data[$i],'UTF-8','sjis'))) . ",";
                            }
                        }
                        $cnt++;
                        $csvdata.= "\n";
                    }
                    $csvdata.= "";
                    fclose ( $handle );
                }
                $csvdata = rtrim($csvdata,"\n");
                $result["hidden"] = $csvdata;
            }
            //ファイル変更なし
        }else if(!empty($image)){
            $result[] = $upDir . $image;
        }else{
            //$result[0] = "";
            if($require == 1){
                // エラー処理
                $result['error'] = "ファイルを選択して下さい。";
            }else{
                $result="";
            }
        }
        print_r(json_encode($result));
    }

    ///////////////////////////////////////////////////////////
    //　エクスポート
    ///////////////////////////////////////////////////////////
    function export(){
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');
        $role = $this->session->userdata('login_role');
        $this->_role_check($role);

        $cat = $this->uri->segment(3);
        $id = $this->uri->segment(4);

        //オブジェクトへ変換
        $data = $this->db->get_where($this->database, array('id' => $id))->row();
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
        $buff="";
        if($cat=="jikoku"){
            $buff=$data->jikoku_file;
        }else{
            $buff=$data->ryokin_file;
        }

        $datas = array();
        $buff = explode( "\n", $buff );
        $cnt = count( $buff );
        for( $i=0;$i<$cnt;$i++ ){
            $lines = explode(",",$buff[$i]);
            foreach($lines as $k => $v){
                $datas[$i][$k] = str_replace("<comma>",",",str_replace("<br>","\n",$v));
            }
        }

        try {

            //CSV形式で情報をファイルに出力のための準備
            $csvFileName = '/tmp/' . time() . rand() . '.csv';
            $res = fopen($csvFileName, 'w');
            if ($res === FALSE) {
                throw new Exception('ファイルの書き込みに失敗しました。');
            }

            // ループしながら出力
            foreach($datas as $dataInfo) {

                // 文字コード変換。エクセルで開けるようにする
                mb_convert_variables('SJIS', 'UTF-8', $dataInfo);

                // ファイルに書き出しをする
                fputcsv($res, $dataInfo);
            }

            // ハンドル閉じる
            fclose($res);

            // ダウンロード開始
            header('Content-Type: application/octet-stream');

            // ここで渡されるファイルがダウンロード時のファイル名になる
            header('Content-Disposition: attachment; filename=sampaleCsv.csv');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . filesize($csvFileName));
            readfile($csvFileName);

        } catch(Exception $e) {

            // 例外処理をここに書きます
            echo $e->getMessage();

        }


    }

    //////////////////////////////////////////////////////////
    //ソート
    //////////////////////////////////////////////////////////
    function sort(){
        $this->_login_check();
        $login_id = $this->session->userdata('login_id');
        $role = $this->session->userdata('login_role');
        $this->_role_check($role);

        $posts = $this->input->post();

        $id=$posts["id"];
        $cat=$posts["category_id"];
        $sort = $posts["sort"];

        if(empty($id)){
            $this->session->set_userdata(array("msg"=>"不正な処理が行われました。エラーコード９"));
            redirect("/auth/logout");
        }
        if(empty($cat)){
            $this->session->set_userdata(array("msg"=>"不正な処理が行われました。エラーコード１０"));
            redirect( "/auth/logout");
        }
        if(empty($sort)){
            $this->session->set_userdata(array("msg"=>"不正な処理が行われました。エラーコード１２"));
            redirect( "/auth/logout");
        }

        //現在のソート番号取得
        $data = $this->db->get_where($this->database, array('id' => $id, 'del'=>0))->row();
        $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
        $nowSort = $data->sort;

        //ソートアップかダウンから対象データ取得
        if($sort=="down"){
            $data2 = $this->db->order_by('sort asc')->get_where($this->database, array("del"=>0, "category_id"=>$cat, "sort >" =>  $nowSort));
            $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

            if(empty($data2->num_rows())){
                $this->session->set_userdata(array("msg"=>"対象のデータが存在しません。エラーコード１３"));
                redirect($this->segment . "/home/".$cat);
            }

            //データの入れ替え（前後を逆にする）
            $update_param1 = array('sort' => $data2->result('array')[0]['sort']);
            $this->db->where('id', $id);
            $this->db->update($this->database, $update_param1);
            $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

            $update_param2 = array('sort' => $nowSort);
            $this->db->where('id', $data2->result('array')[0]['id']);
            $this->db->update($this->database, $update_param2);
            $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

        }else if($sort=="up"){
            $down = $nowSort - 1;
            $data2 = $this->db->order_by('sort desc')->get_where($this->database, array("del"=>0, "category_id"=>$cat, "sort <" => $nowSort));
            $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

            if(empty($data2->num_rows())){
                $this->session->set_userdata(array("msg"=>"対象のデータが存在しません。エラーコード１４"));
                redirect($this->segment . "/home/".$cat);
            }

            //データの入れ替え（前後を逆にする）
            $update_param1 = array('sort' => $data2->result('array')[0]['sort']);
            $this->db->where('id', $id);
            $this->db->update($this->database, $update_param1);
            $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());

            $update_param2 = array('sort' => $nowSort);
            $this->db->where('id', $data2->result('array')[0]['id']);
            $this->db->update($this->database, $update_param2);
            $this->saveLog($this->segment . "/" . $this->uri->segment(1) . "/" . $this->uri->segment(2) , $this->db->last_query());
        }

        $this->session->set_userdata(array("msg"=>"並び替えが完了しました。"));
        redirect($this->segment."/home/".$cat);
    }

    //////////////////////////////////////////////////////////
    //バリデーション
    //////////////////////////////////////////////////////////
    private function setValidation($validation=null)
    {

        $errors ="";
        $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
        //$this->form_validation->set_rules("facility", "設備","required");
        //$this->form_validation->set_rules("discount", "割引","required");
        $this->form_validation->set_rules("fee", "運賃","required");
        $this->form_validation->set_rules("travel_form", "運行形態","required");
        $this->form_validation->set_rules("travel_company", "運行会社","required");
        $this->form_validation->set_rules("travel_distance", "実車距離","required");
        $this->form_validation->set_rules("travel_time", "所要時間","required");
        $this->form_validation->set_rules("travel_driver", "運転者","required");
        $this->form_validation->set_rules("travel_insurance", "バス任意保険","required");
        $this->form_validation->set_rules("content", "本文","required");
        $this->form_validation->set_rules("jikoku_file", "時刻表","required");
        $this->form_validation->set_rules("ryokin_file", "料金表","required");
        $this->form_validation->set_rules("bustei", "バス停","required");
        //$this->form_validation->set_rules("ryokin_calendar", "運賃カレンダー","required");
        /*
                $this->form_validation->set_rules("bustei_title1", "バス停情報タイトル１","required");
                $this->form_validation->set_rules("bustei_sort1", "バス停情報並び順１","required");
                $this->form_validation->set_rules("bustei_address1", "バス停情報住所１","required");
        */
        //$this->form_validation->set_rules("content", "本文","required");

        if($this->form_validation->run() == FALSE){
            $errors = validation_errors();
        }
        return $errors;
    }
}

