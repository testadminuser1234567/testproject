<div class="title clearfix">
<p class="tit2">ユーザー情報を登録する</p>
<div class="naiyoToko">
<br/>
<p class="txt">▼下記の項目をご記入下さい。</p>
<div class="error"><?php echo $clean->purify($msg); ?></div>
<table border="0" cellspacing="0" cellpadding="0">
    <tr>
        <th>氏名</th>
        <td>
            <?php echo set_value('shimei'); ?>
        </td>
    </tr>
    <tr>
        <th>ふりがな</th>
        <td>
            <?php echo set_value('kana'); ?>
        </td>
    </tr>
    <tr>
        <th>生年月日</th>
        <td>
            <?php echo set_value('year'); ?>年<?php echo set_value('month'); ?>月<?php echo set_value('day'); ?>日
        </td>
    </tr>
    <tr>
        <th>性別</th>
        <td>
            <?php echo set_value('sex'); ?>
        </td>
    </tr>
    <tr>
        <th>住所</th>
        <td>
            <?php echo set_value('pref'); ?>
        </td>
    </tr>
    <tr>
        <th>電話</th>
        <td>
            <?php echo set_value('tel1'); ?>
        </td>
    </tr>
    <tr>
        <th>メールアドレス</th>
        <td>
            <?php echo set_value('mail1'); ?>
        </td>
    </tr>
    <tr>
        <th>パスワード</th>
        <td>
            <?php echo set_value('password'); ?>
        </td>
    </tr>
    <tr>
        <th>最終学歴</th>
        <td>
            <?php echo set_value('school_div_id'); ?>
        </td>
    </tr>
    <tr>
        <th>経験社数</th>
        <td>
            <?php echo set_value('company_number'); ?>社
        </td>
    </tr>
    <tr>
        <th>就業状況</th>
        <td>
            <?php echo set_value('jokyo'); ?>
        </td>
    </tr>
    <tr>
        <th>その他、<br />ご希望などございましたら<br />記入をお願いいたします</th>
        <td>
            <?php echo set_value('comment'); ?>
        </td>
    </tr>
    <?php if(!empty(${$segment}->file1)):?>
    <tr>
        <th>履歴書</th>
        <td>
            <?php echo htmlspecialchars(${$segment}->file1, ENT_QUOTES, 'UTF-8'); ?>
        </td>
    </tr>
    <?php endif;?>
    <?php if(!empty(${$segment}->file2)):?>
    <tr>
        <th>職務経歴書</th>
        <td>
            <?php echo htmlspecialchars(${$segment}->file2, ENT_QUOTES, 'UTF-8'); ?>
        </td>
    </tr>
    <?php endif;?>
    <?php if(!empty(${$segment}->file2)):?>
    <tr>
        <th>その他</th>
        <td>
            <?php echo htmlspecialchars(${$segment}->file3, ENT_QUOTES, 'UTF-8'); ?>
        </td>
    </tr>
    <?php endif;?>
</table>
</div>
</div>

<table style="margin:0 auto;margin-top:10px">
    <tr>
        <td>
            <?php echo form_open('member/regist');?>
            <?php echo form_submit('submit', '内容を修正する', 'style="width: 150px; height: 30px;font-weight:bold;float:right"');?>
            <?php echo form_hidden('mode', 'return'); ?>
            <?php echo form_hidden('id', ${$segment}->id); ?>
            <?php echo form_hidden('shimei', ${$segment}->shimei); ?>
            <?php echo form_hidden('kana', ${$segment}->kana); ?>
            <?php echo form_hidden('year', ${$segment}->year); ?>
            <?php echo form_hidden('month', ${$segment}->month); ?>
            <?php echo form_hidden('day', ${$segment}->day); ?>
            <?php echo form_hidden('sex', ${$segment}->sex); ?>
            <?php echo form_hidden('pref', ${$segment}->pref); ?>
            <?php echo form_hidden('tel1', ${$segment}->tel1); ?>
            <?php echo form_hidden('mail1', ${$segment}->mail1); ?>
            <?php echo form_hidden('file1', ${$segment}->file1); ?>
            <?php echo form_hidden('file2', ${$segment}->file2); ?>
            <?php echo form_hidden('file3', ${$segment}->file3); ?>
            <?php echo form_hidden('agree', ${$segment}->agree); ?>
            <?php echo form_hidden('school_div_id', ${$segment}->school_div_id); ?>
            <?php echo form_hidden('jokyo', ${$segment}->jokyo); ?>
            <?php echo form_hidden('comment', ${$segment}->comment); ?>
            <?php echo form_hidden('company_number', ${$segment}->company_number); ?>
            <?php echo form_hidden('password', ${$segment}->password); ?>
            <?php echo form_close();?>
        </td>
        <td>
            <?php echo form_open('member/add');?>
            <?php echo form_submit('submit', '登録する', 'style="width: 150px; height: 30px; margin-left:10px; font-weight:bold;"');?>
            <?php echo form_hidden('mode', 'return'); ?>
            <?php echo form_hidden('id', ${$segment}->id); ?>
            <?php echo form_hidden('shimei', ${$segment}->shimei); ?>
            <?php echo form_hidden('kana', ${$segment}->kana); ?>
            <?php echo form_hidden('year', ${$segment}->year); ?>
            <?php echo form_hidden('month', ${$segment}->month); ?>
            <?php echo form_hidden('day', ${$segment}->day); ?>
            <?php echo form_hidden('sex', ${$segment}->sex); ?>
            <?php echo form_hidden('pref', ${$segment}->pref); ?>
            <?php echo form_hidden('tel1', ${$segment}->tel1); ?>
            <?php echo form_hidden('mail1', ${$segment}->mail1); ?>
            <?php echo form_hidden('file1', ${$segment}->file1); ?>
            <?php echo form_hidden('file2', ${$segment}->file2); ?>
            <?php echo form_hidden('file3', ${$segment}->file3); ?>
            <?php echo form_hidden('agree', ${$segment}->agree); ?>
            <?php echo form_hidden('school_div_id', ${$segment}->school_div_id); ?>
            <?php echo form_hidden('jokyo', ${$segment}->jokyo); ?>
            <?php echo form_hidden('comment', ${$segment}->comment); ?>
            <?php echo form_hidden('company_number', ${$segment}->company_number); ?>
            <?php echo form_hidden('password', ${$segment}->password); ?>

            <?php echo form_hidden($csrf); ?>
            <?php echo form_close();?>
        </td>
    </tr>
</table>
<br/>
</div>
