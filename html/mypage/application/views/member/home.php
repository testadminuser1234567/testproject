<div class="title clearfix">
    <div class="midasi">
        <p class="tit">プロセス </p>
    </div>
    <div class="sibori">
        <p class="page">
            <?php echo $this->pagination->create_links(); ?>
        </p>
    </div>
</div>
<?php if($role==1):?>
    <form method="post" action="<?php echo base_url();?>member/regist">
        <input type="submit" name="submit" value="新規登録する" style="margin-left:30px;width:150px;height:30px;margin-bottom:10px">
    </form>
<?php endif;?>
<div class="toukou">
    <div class="naiyo">
        <p style="color:#FF0000;margin-top:10px;margin-bottom:10px"><?php echo $clean->purify($msg);?></p>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <th style="width:10%">JOB</th>
                <th style="width:10%">企業</th>
                <th style="width:10%">職種</th>
                <!--
                <th style="width:10%">選考プロセス担当</th>
                -->
                <th style="width:10%">フェーズ</th>
                <th style="width:10%">更新日</th>
                <th style="width:10%" colspan="2">応募</th>
                <!--
            <th style="width:8%">パスワード変更</th>
            -->
            </tr>
            <?php if(!empty($process)):?>
                <?php foreach ($process as $k => $v): ?>
                    <?php if($v["mypageCheck"]==11311):?>
                        <tr>
                            <td>
                                <?php echo $clean->purify($v["job"]["job_title"]);?>
                            </td>
                            <td>
                                <?php echo $clean->purify($v["client"]["c_title"]);?>
                            </td>
                            <td>
                                <?php echo $clean->purify($v["jobcategoryName"]);?>
                            </td>
                            <!--
                            <td>
                                担当
                            </td>
                            -->
                            <td>
                                <?php echo $v["phaze"];?>
                                <input type="hidden" name="phaseId" value="<?php echo $clean->purify($v["phazeId"]);?>" />
                            </td>
                            <td>
                                <?php echo $clean->purify($v["updated"]);?>
                            </td>
                            <?php if($v["phazeFlg"]==1):?>
                                <td>
                                    <form action="<?php echo base_url();?>member/apply/" method="post">
                                        <input type="submit" name="submit" value="応募する" onclick="return confirm('この案件に応募します。')" />
                                        <input type="hidden" name="jobname" value="<?php echo $clean->purify($v["job"]["job_title"]);?>" />
                                        <input type="hidden" name="clientname" value="<?php echo $clean->purify($v["client"]["c_title"]);?>" />
                                        <input type="hidden" name="processId" value="<?php echo $clean->purify($v["processId"]);?>" />
                                        <input type="hidden" name="phaseId" value="<?php echo $clean->purify($v["phazeId"]);?>" />
                                        <input type="hidden" name="phasedate" value="<?php echo $clean->purify($v["phasedate"]);?>" />
                                        <?php echo form_hidden($csrf); ?>
                                    </form>
                                </td>
                                <td>
                                    <form action="<?php echo base_url();?>member/cancel/" method="post">
                                        <input type="submit" name="submit" value="辞退する" onclick="return confirm('この案件を辞退しますか。')" />
                                        <input type="hidden" name="jobname" value="<?php echo $clean->purify($v["job"]["job_title"]);?>" />
                                        <input type="hidden" name="clientname" value="<?php echo $clean->purify($v["client"]["c_title"]);?>" />
                                        <input type="hidden" name="processId" value="<?php echo $clean->purify($v["processId"]);?>" />
                                        <input type="hidden" name="phaseId" value="<?php echo $clean->purify($v["phazeId"]);?>" />
                                        <input type="hidden" name="phasedate" value="<?php echo $clean->purify($v["phasedate"]);?>" />
                                        <?php echo form_hidden($csrf); ?>
                                    </form>
                                </td>
                            <?php else:?>
                                <td colspan="2">-</td>
                            <?php endif;?>
                        </tr>
                    <?php endif;?>
                <?php endforeach; ?>
            <?php endif;?>
        </table>
    </div>
    <div style="text-align: center;"><a href="#">応募を取り消す</a></div>
