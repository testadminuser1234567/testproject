


<div id="login">
<p class="title"><span>ログイン</span> ID・パスワードを入力下さい。</p>
<div class="error"><?php echo $clean->purify($msg); ?></div>
<?php echo form_open("member/login");?>
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<th scope="row">ユーザID</th>
<td><input type-"text" name="email" value="" /></td>
</tr>
<tr>
<th scope="row">パスワード</th>
<td><input type="password" name="password" autocomplete="off" /></td>
</tr>
<tr>
<th></th>
<td><input type="submit" name="submit" value="ログイン" /></td>
</tr>
    <tr>
        <td><input type="checkbox" name="loginkey"/>ログインを記憶する </td>
    </tr>
</table>
<?php echo form_close();?>
<?php if(Kanri=="on"):?>
<p style="margin-left:100px;margin-top:15px;">パスワードを忘れた場合は<a href="<?php echo base_url();?>member/forget_password">こちら</a></p>
<?php endif;?>
</div>

<div>
    <a href="<?php echo $clean->purify($fbloginUrl); ?>">facebookでログイン</a>
</div>

<div>
    <a href="<?php echo $clean->purify($googleUrl); ?>">googleでログイン</a>
</div>