<div class="title clearfix">
<p class="tit2">パスワードを変更する</p>
<div class="naiyoToko">
<br/>
<p class="txt">▼下記の項目をご記入下さい。</p>
<div class="error"><?php echo $clean->purify($msg); ?></div>
<form name="frm" action="<?php echo base_url();?>member/reset_password" method="post">
<table border="0" cellspacing="0" cellpadding="0">
<tr>
<th>ユーザーID</th>
<td>
<?php echo $clean->purify(@$member->id);?><input type="hidden" name="email" value="<?php echo $clean->purify(@$member->email);?>" />
</td>
</tr>
    <tr>
        <th class="hissu">パスワード</th>
        <td><input name="password" type="password" id="password" value="" />　 <br><span style="color:#FF0000">※8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</span></td>
    </tr>
    <tr>
        <th class="hissu">パスワード（確認用）</th>
        <td><input name="password_check" type="password" id="password_check" value="" />　 <br><span style="color:#FF0000">※8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</span></td>
    </tr>
<tr>
</table>
</div>
</div>

<div class="toukou noLine clearfix">
<p class="back"><a href="<?php echo base_url();?>member/home">&lt;&lt;ユーザー一覧へ戻る</a></p>
<p class="b" style="text-align:center">
    <input type="hidden" name="id" value="<?php echo @$member->id;?>" />
    <input type="hidden" name="email" value="<?php echo @$member->email;?>" />
<?php echo form_hidden($csrf); ?>
<input type="submit" value="更新する" style="width: 150px; height: 30px;font-weight:bold;" onclick="viewport();" />
</div>
</div>
</form>
