<?php
//HRBCマスターと連動
$tmppref = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/work.json', true);
$tmpwork = json_decode($tmpwork,true);
?>

<div class="title clearfix">
    <p class="tit2">ユーザー情報を登録する</p>
    <div class="naiyoToko">
        <br/>
        <p class="txt">▼下記の項目をご記入下さい。</p>
        <div class="error"><?php echo $clean->purify($msg); ?></div>
        <?php echo form_open_multipart('member/confirm');?>
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <th class="hissu">氏名</th>
                <td>
                    <?php echo form_input('shimei', @${$segment}->shimei);?>
                </td>
            </tr>
            <tr>
                <th class="hissu">ふりがな</th>
                <td>
                    <?php echo form_input('kana', @${$segment}->kana);?>
                </td>
            </tr>
            <tr>
                <th class="hissu">生年月日</th>
                <td>
                    <?php
                    $y= date('Y');
                    $y_from = $y-18;
                    $y_to = $y-63;
                    $options_y = array(""=>"年");
                    for($y=$y_from;$y>=$y_to;$y--){
                        $options_y[$y] = $y . "年";
                    }
                    $options_m=array(""=>"月");
                    for($m=1;$m<=12;$m++){
                        $options_m[$m] = $m . "月";
                    }
                    $options_d=array(""=>"日");
                    for($d=1;$d<=31;$d++){
                        $options_d[$d] = $d . "日";
                    }
                    ?>
                    <?php echo form_dropdown('year', $options_y, @${$segment}->year);?>
                    <?php echo form_dropdown('month', $options_m, @${$segment}->month);?>
                    <?php echo form_dropdown('day', $options_d, @${$segment}->day);?>
                </td>
            </tr>
            <tr>
                <th class="hissu">性別</th>
                <td>
                    <?php foreach($tmpgender['Item'] as $k=>$v):?>
                        <?php if(!empty($v['Item'])):?>
                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                <input type="radio" name="sex" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(count($v['Item'])-1 == $k2):?>class="required"<?php endif;?>  <?php if(@${$segment}->sex==$v2['Option.P_Name']):?>checked<?php endif;?>  />

                                <?php echo htmlspecialchars($v2['Option.P_Name'], ENT_QUOTES, 'UTF-8');?>
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>
                </td>
            </tr>
            <tr>
                <th class="hissu">住所</th>
                <td>

                    <select id="pref" name="pref" class="hissu required">
                        <option value="" selected="selected" label="">▼お選びください</option>
                        <?php foreach($tmppref['Item'] as $k=>$v):?>
                            <?php if(!empty($v['Item'])):?>
                                <?php foreach($v['Item'] as $k2=>$v2):?>
                                    <?php if(!empty($v2['Items']['Item'])):?>
                                        <?php foreach($v2['Items']['Item'] as $k3=>$v3):?>
                                            <option value="<?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->pref==$v3['Option.P_Name']):?>selected<?php endif;?> ><?php echo htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                            <?php set_value($v3['Option.P_Name']);?>
                                        <?php endforeach;?>
                                    <?php endif;?>
                                <?php endforeach;?>
                            <?php endif;?>
                        <?php endforeach;?>
                    </select></td>
            </tr>
            <tr>
                <th class="hissu">電話</th>
                <td>
                    <?php echo form_input('tel1', @${$segment}->tel1);?>
                </td>
            </tr>
            <tr>
                <th class="hissu">メールアドレス</th>
                <td>
                    <?php echo form_input('mail1', @${$segment}->mail1);?>
                </td>
            </tr>
            <tr>
                <th class="hissu">パスワード</th>
                <td><?php echo form_password('password', @${$segment}->password);?>　 <br><span style="color:#FF0000">※8～20文字の半角英数形式でアルファベットと数字を組み合わせてください。</span></td>
            </tr>

            <tr>
                <th class="hissu">最終学歴</th>
                <td><select id="school_div_id" name="school_div_id" class="hissu required">
                        <option value="" selected="selected" label="">▼お選びください</option>
                        <?php foreach($tmpbackground['Item'] as $k=>$v):?>
                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                <option value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(@${$segment}->school_div_id==$v2['Option.P_Name']):?>selected<?php endif;?> ><?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?></option>
                                <?php echo set_value($v2['Option.P_Name']);?>
                            <?php endforeach;?>
                        <?php endforeach;?>
                    </select></td>
            </tr>
            <tr>
                <th class="hissu">経験社数</th>
                <td>
                    <?php
                    $options_c=array(""=>"▼");
                    for($c=1;$c<=5;$c++){
                        $options_c[$c] = $c . "社";
                    }
                    ?>
                    <?php echo form_dropdown('company_number', $options_c, @${$segment}->company_number);?>
                </td>
            </tr>
            <tr>
                <th class="hissu">就業状況</th>
                <td>

                    <?php foreach($tmpwork['Item'] as $k=>$v):?>
                        <?php if(!empty($v['Item'])):?>
                            <?php foreach($v['Item'] as $k2=>$v2):?>
                                <input type="radio" name="jokyo" value="<?php echo htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');?>" <?php if(count($v['Item'])-1 == $k2):?>class="required"<?php endif;?> <?php if(@${$segment}->jokyo==$v2['Option.P_Name']):?>checked<?php endif;?>  />

                                <?php echo htmlspecialchars($v2['Option.P_Name'], ENT_QUOTES, 'UTF-8');?>
                            <?php endforeach;?>
                        <?php endif;?>
                    <?php endforeach;?>

                </td>
            </tr>
            <tr class="career">
                <th>その他、<br />ご希望などございましたら<br />記入をお願いいたします</th>
                <td><?php echo form_textarea('comment', @${$segment}->comment);?></td>
            </tr>
        </table>
    </div>
</div>

<table width="650">
    <tr class="career">
        <th colspan="2" class="title">■ 履歴書・職務経歴書</th>
    </tr>

    <tr>
        <th>履歴書</th>
        <td><input type="file" name="userfile[]" >
            <input type="hidden" name="file1" id="file1" value="<?php echo htmlspecialchars(@${$segment}->file1, ENT_QUOTES, 'UTF-8');?>"/>
            <?php if(@${$segment}->file1):?>
                <br/>
                選択されているファイル： <?php echo htmlspecialchars(@${$segment}->file1, ENT_QUOTES, 'UTF-8');?>
            <?php endif;?></td>
    </tr>
    <tr>
        <th>職務経歴書</th>
        <td><input type="file" name="userfile[]" >
            <input type="hidden" name="file2" id="file2" value="<?php echo htmlspecialchars(@${$segment}->file2, ENT_QUOTES, 'UTF-8');?>"/>
            <?php if(@${$segment}->file2):?>
                <br/>
                選択されているファイル： <?php echo htmlspecialchars(@${$segment}->file2, ENT_QUOTES, 'UTF-8');?>
            <?php endif;?></td>
    </tr>
    <tr>
        <th>その他</th>
        <td><input type="file" name="userfile[]" >
            <input type="hidden" name="file3" id="file3" value="<?php echo htmlspecialchars(@${$segment}->file3, ENT_QUOTES, 'UTF-8');?>"/>
            <?php if(@${$segment}->file3):?>
                <br/>
                選択されているファイル： <?php echo htmlspecialchars(@${$segment}->file3, ENT_QUOTES, 'UTF-8');?>
            <?php endif;?></td>
    </tr>
</table>
<p>※すでに履歴書、職務経歴書をご作成の方はこちらより添付をお願いいたします。<br />
    ※Word, Excel, PowerPoint, PDF, jpegファイルがアップロード可能です。<br />
    ※ファイル容量は合計2MBまでとなっております。</p>

<!-- privacyBox start -->
<div id="privacyBox">
    <div id="privacyBoxSub">
        <h2>■ 個人情報保護方針 <span class="point">※</span></h2>
        <textarea id="poricyBox" readonly>
プライバシーポリシー

ヒューレックス株式会社は、個人情報保護の重要性に鑑み、また、業務に従事するすべての者が、その責任を認識し、個人情報の保護に関する法律（職安法第51条および個人情報保護法）に即し個人情報保護法その他の関連法令・ガイドラインを遵守して、個人情報を適正に取り扱うとともに、安全管理について適切な措置を講じます。

ヒューレックス株式会社は、個人情報の取扱いが適正に行われるように従業者への教育・指導を徹底し、適正な取扱いが行われるよう取り組んでまいります。また、個人情報の取扱いに関する苦情・相談に迅速に対応し、当社の個人情報の取扱い及び安全管理に関わる適切な措置については、適宜見直し改善いたします。


（1）個人情報の取得
ヒューレックス株式会社は、十分な安全管理措置を講じたうえで、業務上必要な範囲で、かつ、適法で公正な手段により個人情報を取得します。

（2）個人情報の利用目的
ヒューレックス株式会社は、ご登録者の経歴、職歴を正しく把握し、適切なアドバイスや求人企業をご紹介するために必要な情報をご登録者よりお預かりいたします。取得した個人情報を、当社の営む求人紹介業務、および転職サービス（海外・国内研修）、転職にともなう渡航・旅行に関連するサービスの提供等に利用します。

上記の利用目的の変更は、相当の関連性を有すると合理的に認められる範囲においてのみ行い、変更する場合には、その内容をご本人に対し、原則として書面等により通知し、またはホームページ（URL）等により公表します。

（3）個人データの安全管理措置
ヒューレックス株式会社は、取り扱う個人データの漏えい、滅失またはき損の防止その他の個人データの安全管理のため、安全管理に関する取扱規程等の整備および実施体制の整備等、十分なセキュリティ対策を講じるとともに、正確性・最新性を確保するために、必要かつ適切な措置を講じ、万が一、問題等が発生した場合は、速やかに適当な是正対策をします。

（4）個人データの第三者への提供
ヒューレックス株式会社は、法令または裁判所その他の政府機関より適法に開示を要求された場合を除き、ご本人の同意なく求人会社を含め第三者に個人データを提供することはありません。ご登録された個人情報の変更、削除を希望される場合は、 touroku＠hurex.co.jp までご連絡ください。

（5）当社に対するご照会
下記の窓口にお問い合わせください。ご照会者の詳細を確認させていただいたうえで対応させていただきますので、あらかじめご了承願います。


【お問い合わせ窓口】
ヒューレックス株式会社
〒980-6117 宮城県仙台市青葉区中央1-3-1 アエル17階
TEL: 0800-808-4150 FAX: 022-723-1738
Email: touroku＠hurex.co.jp
担当：神谷貴宏

以上
</textarea>
        <p class="checkboxrequired" style="text-align:center;"><span class="hissu"><input type="hidden" name="agree" value="0" /><input type="checkbox" class="rc" name="agree" value="1" id="agreeChk" <?php if(@${$segment}->agree=="1"):?>checked<?php endif;?>> <label for="agreeChk">同意する</label></span></p>
    </div>
</div>

<div class="toukou noLine clearfix">
    <p class="b" style="text-align:center">
        <input type="hidden" name="id" value="<?php echo @${$segment}->id;?>" />
        <?php echo form_hidden($csrf); ?>
        <input type="submit" value="登録する" style="width: 150px; height: 30px;font-weight:bold;" onclick="viewport();" />
</div>
</div>

<?php echo form_close();?>
