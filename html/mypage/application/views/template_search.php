<?php
header("Content-type: text/html; charset=utf-8");
header("X-Content-Type-Options: nosniff");
header("X-FRAME-OPTIONS: SAMEORIGIN");
header("X-FRAME-OPTIONS: DENY");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="robots" content="noindex,nofollow">
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <link href="<?php echo base_url();?>css/import.css?<?php echo date('Ymd'); ?>" rel="stylesheet" media="all">
    <title>HUREXマイページ</title>
    <script src="<?php echo base_url();?>js/jquery-1.11.1.min.js?<?php echo date('Ymd'); ?>" charset="utf-8"></script>
    <!-- modal start -->
    <link rel="stylesheet" href="<?php echo base_url();?>js/remodal/remodal.css?<?php echo date('Ymd'); ?>">
    <link rel="stylesheet" href="<?php echo base_url();?>js/remodal/remodal-default-theme.css?<?php echo date('Ymd'); ?>">
    <script src="<?php echo base_url();?>js/remodal/remodal.js?<?php echo date('Ymd'); ?>"></script>
    <!-- modal end -->
    <script src="<?php echo base_url();?>js/jquery.page-scroller-308.js?<?php echo date('Ymd'); ?>" charset="utf-8"></script>
    <script src="<?php echo base_url();?>js/rollover.js?<?php echo date('Ymd'); ?>"></script>
    <script src="<?php echo base_url();?>js/common.js?<?php echo date('Ymd'); ?>"></script>
    <script src="<?php echo base_url();?>js/floating.js?<?php echo date('Ymd'); ?>"></script>
    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>js/html5shiv.js?<?php echo date('Ymd'); ?>"></script>
    <script src="<?php echo base_url();?>js/css3-mediaqueries.js?<?php echo date('Ymd'); ?>"></script>
    <![endif]-->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
</head>

<body id="<?php echo $body_id;?>">
    <div id="wrapper">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <div id="floating"> 
    <!-- header start -->
    <header id="header">
        <div class="inner clearfix">
            <p class="logo"><a href="<?php echo base_url();?>user/home/"><img src="<?php echo BaseUrl;?>images/logo.png" width="170" height="28" alt="HUREXマイページ"/></a>
            </p>
            <div id="headerContents" class="pc">
                <ul class="clearfix">
                    <li><a href="https://www.hurex.jp/" target="_blank">HUREXホームページ</a>
                    </li>
                    <li><a href="<?php echo BaseUrl;?>manual/page1" target="_blank">ご利用マニュアル</a>
                    </li>
                </ul>
                <p id="txtContact">サービスについてのお問い合わせ<span class="tel"><img src="<?php echo BaseUrl;?>images/ico_freedial.png" width="21" height="12" alt=""/>0120-14-1150</span>
                </p>
            </div>
        </div>
    </header>
    <!-- header end -->
    <?php include(BaseDir.kanriDirectory.'/include/navi.php'); ?>
        </div>

<?php $this->load->view($render);?>


<?php include(BaseDir.kanriDirectory.'/include/footer.php'); ?>
<?php include(BaseDir.kanriDirectory.'/include/modal.php'); ?>
    <div id="manualBox">
        <div class="box man">
           <div class="inner man">
               <h2 class="man"><span class="line man">ご利用マニュアルのご案内</span></h2>
            <p class="man">HUREXマイページにご登録いただきありがとうございます。<br>
                貴方の転職をよりスムーズに進めますようサポートさせていただきます。</p>
<p class="man">マイページのご利用方法については下記[ご利用マニュアル]をご覧ください。<br>
[ご利用マニュアル]はマイページ内からも閲覧できます。</p>
           <p class="manual man"><a class="man close" href="<?php echo BaseUrl;?>manual/" target="_blank"><img class="man" src="<?php echo BaseUrl;?>images/btn_manual.png" alt="ご利用マニュアル" width="240" height="54"/></a></p>
            <p class="check man"><label for="manualBtn" class="man"><input class="man" type="checkbox" name="manualBtn" id="manualBtn">今後表示しない</label></p>
            <p class="close"><a href="javascript:;">閉じる</a></p>
        </div>
        </div>
    </div>
    </div>
</body>
</html>
