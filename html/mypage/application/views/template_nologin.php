<?php 
header("Content-type: text/html; charset=utf-8");
header("X-Content-Type-Options: nosniff");
header("X-FRAME-OPTIONS: SAMEORIGIN");
header("X-FRAME-OPTIONS: DENY");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="robots" content="noindex,nofollow">
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="-1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=2.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <link href="<?php echo base_url();?>css/import.css?<?php echo date('Ymd'); ?>" rel="stylesheet" media="all">
    <title>HUREXマイページ</title>
    <script src="<?php echo base_url();?>js/jquery-1.11.1.min.js?<?php echo date('Ymd'); ?>" charset="utf-8"></script>
    <script src="<?php echo base_url();?>js/jquery.page-scroller-308.js?<?php echo date('Ymd'); ?>" charset="utf-8"></script>
    <script src="<?php echo base_url();?>js/rollover.js?<?php echo date('Ymd'); ?>"></script>
    <script src="<?php echo base_url();?>js/common.js?<?php echo date('Ymd'); ?>"></script>
    <!--[if lt IE 9]>
    <script src="<?php echo base_url();?>js/html5shiv.js?<?php echo date('Ymd'); ?>"></script>
    <script src="<?php echo base_url();?>js/css3-mediaqueries.js?<?php echo date('Ymd'); ?>"></script>
    <![endif]-->
<!-- Google Tag Manager -->
<script>
	var date = new Date();
	var trans_id = parseInt((new Date)/1000);
dataLayer = [{
    'transactionId': trans_id,
    'transactionAffiliation': 'HUREX_WEB',
    'transactionTotal': 20000 ,
    'transactionTax': 0,
    'transactionShipping': 0,
    'transactionProducts': [{
        'sku': 'es0001',
        'name': 'エントリー',
        'category': 'WEB',
        'price': 20000,
        'quantity': 1    
    }]
}];
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<?php if(!empty($_GET["action"]) && $_GET["action"] == "new"):?>
<!-- Event snippet for 新規登録（仮登録）完了 conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-833457655/8JppCI70t3oQ95u2jQM'});
</script>
<?php endif;?>

<?php if(!empty($_GET["action"]) && $_GET["action"] == "job_new"):?>
<!-- Event snippet for 求人詳細問合せ conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-833457655/83_kCNPbunoQ95u2jQM'});
</script>
<?php endif;?>

<?php if(!empty($_GET["action"]) && $_GET["action"] == "ui_new"):?>
<!-- Event snippet for UIターン conversion page -->
<script>
  gtag('event', 'conversion', {'send_to': 'AW-833457655/jjI6CJH0t3oQ95u2jQM'});
</script>
<?php endif;?>
</head>
<body>
    <div id="wrapper">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php include(BaseDir.kanriDirectory.'/include/header.php'); ?>
<!-- co start -->
<?php $this->load->view($render);?>
<!-- co end -->
<?php include(BaseDir.kanriDirectory.'/include/footer_nologin.php'); ?>
    </div>
</body>
</html>