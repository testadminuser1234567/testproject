<?php
ini_set('error_reporting', 1);
ini_set('display_errors', 1);
ini_set( 'default_charset', 'UTF-8' );

mb_language('Japanese');
ini_set('default_charset', 'UTF-8');
ini_set('mbstring.detect_order', 'auto');
ini_set('mbstring.http_input'  , 'auto');
ini_set('mbstring.http_output' , 'pass');
ini_set('mbstring.internal_encoding', 'UTF-8');
ini_set('mbstring.script_encoding'  , 'UTF-8');
ini_set('mbstring.substitute_character', 'none');
mb_regex_encoding('UTF-8');

$today = date('Y-m-d', strtotime('-1 day', time()));

require("./conf/config.php");
$dsn = 'mysql:dbname=' . $confDB .';host=' . $confDBhost . ';port=' . $confDBport;
$user = $confDBuser;
$password = $confDBpassword;

try{
    $dbh = new PDO($dsn, $user, $password,
        array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET `utf8`"
        ));

}catch (PDOException $e){
    print('Error:'.$e->getMessage());
    die();
}



//メンバーを取得
$sql = "SELECT id, hash_email, count(hash_email) as cnt FROM members GROUP BY hash_email HAVING COUNT(hash_email) > 1";

$stmt = $dbh->prepare($sql);
$stmt->execute();
$datas = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
    $datas[] = $result;
}
$mes="";
if(!empty($datas)){
	foreach($datas as $k => $v){
		$mes.="id: ".$v["id"] . " hash: " . $v["hash_mail"] . " cnt: " . $v["cnt"] . "\r\n";
	}
}


$to      = 'tosa@smt-net.co.jp';
$subject = 'マイページhash_mail重複チェック';
$message = $mes;
$headers = 'From: touroku@hurex.jp' . "\r\n";

mb_send_mail($to, $subject, $message, $headers);