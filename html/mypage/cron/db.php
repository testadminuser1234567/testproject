<?php
mb_language("japanese");
mb_internal_encoding("UTF-8");

// dbバックアップ
$to = "tosa@smt-net.co.jp";
$from = "tosa@smt-net.co.jp";


           //file_get_contents関数でデータを取得
	   $url = "https://preview:123456@www.hurex.jp/mypage/backup/index";
            if($xml = @file_get_contents($url, false, $context)){
                //ここにデータ取得が成功した時の処理
            }else{
                //エラー処理
                if(count($http_response_header) > 0){
                    //「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
                    $status_code = explode(' ', $http_response_header[0]);  //「$status_code[1]」にステータスコードの数字のみが入る

		$e = $status_code[0] . " " . $status_code[1] . " " . $status_code[2];

                    //エラーの判別
                    switch($status_code[1]){
                        //404エラーの場合
                        case 404:
                $subject = "dbバックアップ404エラー";
                $message = "404エラーです" . $e;
                mb_send_mail($to,$subject,$message,"From:".$from);
                            break;

                        //500エラーの場合
                        case 500:
                $subject = "dbバックアップ500エラー";
                $message = "500エラーです" . $e;
                mb_send_mail($to,$subject,$message,"From:".$from);
                            break;

                        //その他のエラーの場合
                        default:
                $subject = "dbバックアップ";
                $message = "処理結果" . $e;
                mb_send_mail($to,$subject,$message,"From:".$from);

                    }
                }else{
                $subject = "dbバックアップタイムアウトエラー";
                $message = "タイムアウトエラーです" . $e;
                mb_send_mail($to,$subject,$message,"From:".$from);
                }
            }