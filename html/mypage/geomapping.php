<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>じょぶちず</title>
</head>

<body>

<?php

mb_language("Japanese");//文字コードの設定
mb_internal_encoding("UTF-8");

// 郵便番号を入れて緯度経度を求める。
$zip = $_GET["zip"];
$zip = urlencode($zip);

$url = "http://geoapi.heartrails.com/api/xml?method=searchByPostal&postal=" .
       $zip;
$contents= simplexml_load_file($url);
$y_lng =  (string)$contents->location->y;
$x_lat =  (string)$contents->location->x;
$lnglat = $y_lng . "," . $x_lat;

//パラメータ取得
$cname = $_GET["name"];
$c_age = $_GET["age"];
$caddr = $_GET["address"];
$c_kk1 = $_GET["kk1"];
$c_kk2 = $_GET["kk2"];
$c_kk3 = $_GET["kk3"];
$c_kj  = $_GET["kj"];
$c_ej  = $_GET["ej"];
$c_rid = $_GET["rjid"];

$now   = date("Ymd");
$bdate = str_replace("-", "", $c_age);
$c_age = floor(($now-$bdate)/10000).' 歳';



?>



<style>
  .wrap{width: 1000px;margin: 0 auto;}  
  .right{float: right;}
  .left{float: left;width: 342px;}
  .left>div{box-sizing: border-box;padding: 20px;}
  .left-title{background: #c60000;color: white;text-align: center;font-weight: bold;}
  .left>div:nth-child(even){background: #f2f2f2;}
  .left-th{font-size: 12px;font-weight: bold;margin-bottom: 6px;}
</style>

<div class="wrap">

<div class="left">
  <div class="left-title">キャンディデイト情報</div>
  <div>
    <div class="left-th">名前（レジュメID）</div>
    <div class="left-td"><?php echo $cname . ' ( ' . $c_rid . ' )' ?></div>
  </div>
  <div>
    <div class="left-th">住所</div>
    <div class="left-td"><?php echo $caddr ?></div>
  </div>
  <div>
    <div class="left-th">年齢</div>
    <div class="left-td"><?php echo $c_age ?></div>
  </div>
  <div>
    <div class="left-th">希望勤務地</div>
    <div class="left-td">
        第1希望：<?php echo $c_kk1 ?><br>
        第2希望：<?php echo $c_kk2 ?><br>
        第3希望：<?php echo $c_kk3 ?>
    </div>
  </div>
  <div>
    <div class="left-th">希望職種</div>
    <div class="left-td">
      <?php echo $c_kj ?>
    </div>
  </div>
  <div>
    <div class="left-th">経験職種</div>
    <div class="left-td">
      <?php echo $c_ej ?>
    </div>
  </div>
  
  
</div>
<div class="right"><iframe src="https://www.google.com/maps/d/u/0/embed?mid=1yT8mtFG4Bi8rkDGiApKhsu0nmH2Vz8bi&z=10&ll=<?php echo $lnglat ?>" width="640" height="700"></iframe></div>


</div>



</body>
</html>
