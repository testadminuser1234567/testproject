<?php

$mail = "test4@smt-net.co.jp";
$test = "テスト送信テスト送信2";
$comment = "あaaaあ";

$param = "";
$param .= "email=" . urlencode($mail) . "&";
$param .= "test=" . urlencode($test) . "&";
$param .= "comment=" . urlencode($comment); 

$url = "https://info.hurex.jp/l/578091/2018-07-20/2z48ln?" . $param; // リクエストするURLとパラメータ

// curlの処理を始める合図
$curl = curl_init($url);

// リクエストのオプションをセットしていく
curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); // メソッド指定
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 証明書の検証を行わない
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // レスポンスを文字列で受け取る

// レスポンスを変数に入れる
$response = curl_exec($curl);
$info    = curl_getinfo($curl);
$errorNo = curl_errno($curl);

// OK以外はエラーなので空白配列を返す
if ($errorNo !== CURLE_OK) {
	// 詳しくエラーハンドリングしたい場合はerrorNoで確認
	// タイムアウトの場合はCURLE_OPERATION_TIMEDOUT
	return [];
}

// 200以外のステータスコードは失敗とみなし空配列を返す
if ($info['http_code'] !== 200) {
	return [];
}

// curlの処理を終了
curl_close($curl);

print_r($response);

/*
$url = 'https://info.hurex.jp/l/578091/2018-07-20/2z48ln ';
 
$data = array(
    "email"=> urlencode("test@smt-net.co.jp")
);
 
$options = array(
  'http' => array(
    'method'  => 'GET',
    'content' => json_encode( $data ),
    'header'=>  "Content-Type: application/json\r\n" .
                "Accept: application/json\r\n"
    )
);

if($xml = @file_get_contents($url, false, stream_context_create($options))){
	//ここにデータ取得が成功した時の処理
}else{
	preg_match("/[0-9]{3}/", $http_response_header[0], $stcode);
	//エラー処理
	if(count($http_response_header) > 0){
		//「$http_response_header[0]」にはステータスコードがセットされているのでそれを取得
		$c_code=explode('',$http_response_header[0]);//「$c_code[1]」にステータスコードの数字のみが入る
		echo $c_code;
	}else{
		//タイムアウトの場合or存在しないドメインだった場合
		echo "timeout";
	}
}
print_r($xml);

$xml = simplexml_load_string($xml);
//$result = json_decode($result);
 
//var_dump($xml);

echo "<iframe src=\"https://info.hurex.jp/l/578091/2018-07-20/2z48ln?email=" . urlencode("test@smt-net.co.jp") . "\" width=\"1\" height=\"1\"></iframe>";
*/