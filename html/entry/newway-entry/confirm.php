<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="index,follow" />
<meta name="keywords" content="転職,求人,東北,仙台,宮城,ヒューレックス,福島,山形" />
<meta name="description" content="東北地方の求人多数！UターンIターン転職希望、地元で仕事探し、正社員・契約社員の求人情報をお求めの方は、転職エージェントのヒューレックスへ。" />
<meta http-equiv="imagetoolbar" content="no" />
<meta http-equiv="Content-Language" content="ja" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="JavaScript" />
<link rel="shortcut icon" href="/favicon.ico" />
<link href="../../css/import.css" rel="stylesheet" type="text/css" media="all" />
<link rel="canonical" href="https://www.hurex.jp/entry/newway-entry/confirm.php">
<link href="./css/form.css" rel="stylesheet" type="text/css" />
<title>新卒採用エントリーフォーム | 新卒採用情報｜採用情報｜会社概要 |東北・宮城・仙台の求人、転職ならヒューレックス株式会社</title>
<meta name="copyright" content="" />
<script type="text/javascript" src="../../js/jquery-1.6.4.min.js" charset="utf-8"></script>
<link rel="stylesheet" href="../../js/remodal/remodal.css">
<link rel="stylesheet" href="../../js/remodal/remodal-default-theme.css">
<script src="../../js/remodal/remodal.js"></script>
<script type="text/javascript" src="../../js/rollover.js"></script>
<script type="text/javascript" src="../../js/common.js"></script>
<!--[if lte IE 6]>   
<script type="text/javascript" src="../../js/DD_belatedPNG_0.0.8a.js">   
</script>   
<script type="text/javascript">  /* EXAMPLE */  DD_belatedPNG.fix('img');</script>   
<![endif]-->

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,document,'script','//connect.facebook.net/en_US/fbevents.js');
fbq('init', '924312387607433');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=924312387607433&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<body>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-N7BDQM"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-N7BDQM');</script>
<!-- End Google Tag Manager -->
<!-- header start -->
<div id="header" class="clearfix">
<h1>新卒採用エントリーフォーム | 東北・宮城・仙台の求人、転職ならヒューレックス株式会社</h1>
<?php $Path = "../../"; include(dirname(__FILE__).'/../../include/header.html'); ?>
</div>
<!-- header end -->
<?php $Path = "../../"; include(dirname(__FILE__).'/../../include/navi.html'); ?>
<!-- main start -->
<div id="main" class="common">
<div id="mainArea">
<p><img src="../../company/img/title.png" alt="会社概要" /></p>
</div>
</div>
<!-- main end -->
<!-- pan start -->
<div id="pan">
<div id="panSub">
<ul class="clearfix">
<li><a href="../../index.html">ホーム</a></li>
<li><a href="../../company/index.html">会社概要</a></li>
<li><a href="../../company/adoption/">採用情報</a></li>
<li><a href="../../company/adoption/newgraduate.html">新卒採用情報</a></li>
<li>新卒採用エントリーフォーム</li>
</ul>
</div>
</div>
<!-- pan end -->
<!-- co start -->
<div id="co" class="clearfix">
<div id="coL">
<!-- page start -->

<div class="page" id="entryPage">



<h3>新卒採用エントリーフォーム</h3>
<p class="pctL">
<script type="text/javascript" src="https://seal.verisign.com/getseal?host_name=www.hurex.jp&amp;size=M&amp;use_flash=YES&amp;use_transparent=YES&amp;lang=ja"></script>
</p>
<p>当サイトは、お客様のプライバシー保護のため、個人情報入力ページにおいてSSL暗号化通信を採用しています</p>

<br class="clearBT" />
<?php require_once("../system/esc.php");?>
	<form action="mail.php" method="post">
<input type="hidden" name="mode" value="send">
<p>以下の内容で送信致します。</p>
<table width="650" summary="フォーム" class="normal">
				<tr>
					<th>募集職種<span class="point">※</span></th>
					<td width="70%"><div><?php echo esc($job);?></div></td>
				</tr>
				<tr>
					<th>氏名<span class="point">※</span></th>
					<td><div>
						<?php echo esc($onamae);?>
					</div></td>
				</tr>
				<tr>
					<th>フリガナ<span class="point">※</span></th>
					<td><div>
						<?php echo esc($kana);?>
					</div></td>
				</tr>
				<tr>
					<th>郵便番号<span class="point">※</span></th>
					<td>
						<div>
							
							<?php echo esc($zip);?>
						</div>
					</td>
				</tr>
				<tr>
					<th>住所<span class="point">※</span></th>
					<td>
						<div>
							
						<?php echo esc($address);?>
						</div>
					</td>
				</tr>
				<tr>
					<th>電話番号<span class="point">※</span></th>
					<td>
						<div>
							
							<?php echo esc($tel);?>
						</div>
					</td>
				</tr>
				<tr>
					<th>E-mail<span class="point">※</span></th>
					<td>
						<div>
							
							<?php echo esc($mail);?>
						</div>
					</td>
				</tr>
				<tr>
					<th>生年月日<span class="point">※</span></th>
					<td>
						<div>
							
							<?php echo esc($birthday);?>
						</div>
					</td>
				</tr>
				<tr>
					<th>最終学歴(卒業見込み)<span class="point">※</span></th>
					<td>
						<div><?php echo nl2br(hp($graduate));?>
						</div>
					</td>
				</tr>
				<tr>
					<th>学校学部名<span class="point">※</span></th>
					<td>
						<div>
							<?php echo esc($gakubu);?>
						</div>
					</td>
				</tr>
				<tr>
					<th>応募理由<span class="point">※</span></th>
					<td>
						<div>
							
							<?php echo nl2br(hp($reason));?>
						</div>
					</td>
				</tr>
				<tr>
					<th>学生生活で頑張ったことは何ですか<span class="point">※</span></th>
					<td>
						<div>
							
						<?php echo nl2br(hp($school));?>
						</div>
					</td>
				</tr>
				<tr>
					<th>今までで一番困難だったことは何ですか<span class="point">※</span></th>
					<td>
						<div>
							
							<?php echo nl2br(hp($hard));?>
						</div>
					</td>
				</tr>
				<tr>
					<th>その困難をどのように乗り越えましたか<span class="point">※</span></th>
					<td>
						<div>
							
						<?php echo nl2br(hp($over));?>
						</div>
					</td>
				</tr>
</table>
<form id="entryForm" action="mail.php" method="post">
			<p class="aCenter">
<input type="submit" name="ret" value=" 戻る " style="float:left;margin-left:250px" />
<input type="hidden" name="mode" value="return">
<input type="hidden" name="job" value="<?php echo esc($job);?>">
<input type="hidden" name="onamae" value="<?php echo esc($onamae);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="zip" value="<?php echo esc($zip);?>">
<input type="hidden" name="address" value="<?php echo esc($address);?>">
<input type="hidden" name="tel" value="<?php echo esc($tel);?>">
<input type="hidden" name="birthday" value="<?php echo esc($birthday);?>">
<input type="hidden" name="mail" value="<?php echo esc($mail);?>">
<input type="hidden" name="graduate" value="<?php echo hp($graduate);?>">
<input type="hidden" name="gakubu" value="<?php echo esc($gakubu);?>">
<input type="hidden" name="reason" value="<?php echo hp($reason);?>">
<input type="hidden" name="school" value="<?php echo hp($school);?>">
<input type="hidden" name="hard" value="<?php echo hp($hard);?>">
<input type="hidden" name="over" value="<?php echo hp($over);?>">
			</p>
</form>
<form id="entryForm" action="mail.php" method="post">
			<p class="aCenter">
<input type="submit" name="submit" value="送信する" style="float:left;margin-left:20px;margin-top:-5px;" />
<input type="hidden" name="mode" value="send">
<input type="hidden" name="job" value="<?php echo esc($job);?>">
<input type="hidden" name="onamae" value="<?php echo esc($onamae);?>">
<input type="hidden" name="kana" value="<?php echo esc($kana);?>">
<input type="hidden" name="zip" value="<?php echo esc($zip);?>">
<input type="hidden" name="address" value="<?php echo esc($address);?>">
<input type="hidden" name="tel" value="<?php echo esc($tel);?>">
<input type="hidden" name="birthday" value="<?php echo esc($birthday);?>">
<input type="hidden" name="mail" value="<?php echo esc($mail);?>">
<input type="hidden" name="graduate" value="<?php echo hp($graduate);?>">
<input type="hidden" name="gakubu" value="<?php echo esc($gakubu);?>">
<input type="hidden" name="reason" value="<?php echo hp($reason);?>">
<input type="hidden" name="school" value="<?php echo hp($school);?>">
<input type="hidden" name="hard" value="<?php echo hp($hard);?>">
<input type="hidden" name="over" value="<?php echo hp($over);?>">
			</p>
</form>






</div>
</div>
<div id="coR">
<?php $Path = "../../"; include(dirname(__FILE__).'/../../include/right.html'); ?>
</div>
</div>
<!-- co end -->
<?php $Path = "../../"; include(dirname(__FILE__).'/../../include/footer.html'); ?>

<?php include($_SERVER["DOCUMENT_ROOT"] . '/include/adroll.html');?>
</body>
</html>