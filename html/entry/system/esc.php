<?php
function esc($str){
    return htmlspecialchars($str,ENT_QUOTES,"UTF-8");
}

function aesc($str) {
    if (is_array($str)) {
        return array_map("aesc", $str);
    } else {
        return htmlspecialchars($str, ENT_QUOTES, "UTF-8");
    }
}

function imp($str){
	return implode("\n", $str);
}

function br($str){
	return implode("<br>", $str);
}

function chkval($str, $val){
	if(is_array($str)){
		if(in_array($val, $str)){
			return "checked";
		}
	}else{
		if($str == $val) return "checked";
	}
}

function pf($dirty_html) {
	require_once($_SERVER["DOCUMENT_ROOT"] .  '/syskanri/htmlpurifier/library/HTMLPurifier.auto.php');
	$config = HTMLPurifier_Config::createDefault();
	$config->set('Core.Encoding', 'UTF-8');
	$config->set('HTML.Doctype', 'XHTML 1.0 Transitional');
	$purifier = new HTMLPurifier();
	$clean_html = $purifier->purify( $dirty_html );
	return $clean_html;
}