$(function(){
    $(window).on('beforeunload', function() {
        return "このページから移動してもよろしいですか？";
    });
});

//エラー処理用
var data = {};
data.shimei    = "氏名";
data.kana     = "ふりがな";
data.year    = "年";
data.month    = "月";
data.day    = "日";
data.sex    = "性別";
data.zip      = "郵便番号";
data.pref      = "都道府県";
data.address      = "住所";
data.tel1       = "電話";
data.mail1      = "メール";
data.mail2      = "メール（携帯）";
data.agree = "個人情報保護方針の同意にチェックをお願いいたします。";
data.kibo_pref1="希望勤務地";
data.school_div_id="最終学歴";
data.school="学校名";
data.company_number="経験社数";
data.jokyo="就業状況";
data.kibo_job1="希望職種";
data.expectarea1="勤務地（第1希望）";

$(function(){
	if($("#shimei").val()){
		$("#shimei").css("background-color","#FFF");
	}
	if($("#kana").val()){
		$("#kana").css("background-color","#FFF");
	}
	if($("#year").val()){
		$("#year").css("background-color","#FFF");
	}
	if($("#month").val()){
		$("#month").css("background-color","#FFF");
	}
	if($("#day").val()){
		$("#day").css("background-color","#FFF");
	}
	cnt_tmp = $('.sexradio').length;
	checked_tmp = 0;
	for(i=0;i<cnt_tmp;i++){
		if($(".sexradio").eq(i).prop("checked")){
			 checked_tmp++;
		}
	}
	if (checked_tmp > 0) {
		$(".sexlabel").css("background-color","#FFF");
	}
	if($("#pref").val()){
		$("#pref").css("background-color","#FFF");
	}
	if($("#tel1").val()){
		$("#tel1").css("background-color","#FFF");
	}
	if($("#mail1").val()){
		$("#mail1").css("background-color","#FFF");
	}
	cnt_tmp = $('.gakurekiradio').length;
	checked_tmp = 0;
	for(i=0;i<cnt_tmp;i++){
		if($(".gakurekiradio").eq(i).prop("checked")){
			 checked_tmp++;
		}
	}
	if (checked_tmp > 0) {
		$(".gakurekilabel").css("background-color","#FFF");
	}
	cnt_tmp = $('.compradio').length;
	checked_tmp = 0;
	for(i=0;i<cnt_tmp;i++){
		if($(".compradio").eq(i).prop("checked")){
			 checked_tmp++;
		}
	}
	if (checked_tmp > 0) {
		$(".companylabel").css("background-color","#FFF");
	}
	cnt_tmp = $('.jokyoradio').length;
	checked_tmp = 0;
	for(i=0;i<cnt_tmp;i++){
		if($(".jokyoradio").eq(i).prop("checked")){
			 checked_tmp++;
		}
	}
	if (checked_tmp > 0) {
		$(".jokyolabel").css("background-color","#FFF");
	}
	if($("#expectarea1").val()){
		$("#expectarea1").css("background-color","#FFF");
	}

	//氏名
	$("input#shimei").blur(function() {
		if($("#shimei").val()){
			$(".shimei").removeClass("hissu");
			$(".shimei").addClass("ok");
			$("#shimei").css("background-color","#FFF");
		}else{
			$(".shimei").removeClass("ok");
			$(".shimei").addClass("hissu");
			$("#shimei").css("background-color","#fffafa");
		}
	});

	//かな
	$("input#kana").blur(function() {
		if($("#kana").val()){
			$(".kana").removeClass("hissu");
			$(".kana").addClass("ok");
			$("#kana").css("background-color","#FFF");
		}else{
			$(".kana").removeClass("ok");
			$(".kana").addClass("hissu");
			$("#kana").css("background-color","#fffafa");
		}
	});

	//生年月日
	$("select#year").change(function() {
		if($("#year").val() && $("#month").val() && $("#day").val()){
			$(".birth").removeClass("hissu");
			$(".birth").addClass("ok");
			$("#year").css("background-color","#FFF");
		}else{
			$(".birth").removeClass("ok");
			$(".birth").addClass("hissu");
			$("#year").css("background-color","#fffafa");
		}
	});
	$("select#month").change(function() {
		if($("#year").val() && $("#month").val() && $("#day").val()){
			$(".birth").removeClass("hissu");
			$(".birth").addClass("ok");
			$("#month").css("background-color","#FFF");
		}else{
			$(".birth").removeClass("ok");
			$(".birth").addClass("hissu");
			$("#month").css("background-color","#fffafa");
		}
	});
	$("select#day").change(function() {
		if($("#year").val() && $("#month").val() && $("#day").val()){
			$(".birth").removeClass("hissu");
			$(".birth").addClass("ok");
			$("#day").css("background-color","#FFF");
		}else{
			$(".birth").removeClass("ok");
			$(".birth").addClass("hissu");
			$("#day").css("background-color","#fffafa");
		}
	});

	//性別
	$('.sexradio').click(function(){
	    var idx = $(".sexradio").index(this);
	    var cnt = $('.sexradio').length;
	    var checked = 0;
	    for(i=0;i<cnt;i++){
		if($(".sexradio").eq(i).prop("checked")){
			 checked++;
		}
	    }
	    if (checked > 0) {
			$(".sex").removeClass("hissu");
			$(".sex").addClass("ok");
			$(".sexlabel").css("background-color","#FFF");
	    }else{
			$(".sex").removeClass("ok");
			$(".sex").addClass("hissu");
			$(".sexlabel").css("background-color","#fffafa");
	    }
	})

	//現住所
	$("select#pref").change(function() {
		if($("#pref").val()){
			$(".pref").removeClass("hissu");
			$(".pref").addClass("ok");
		}else{
			$(".pref").removeClass("ok");
			$(".pref").addClass("hissu");
		}
	});

	//電話
	$("input#tel1").blur(function() {
		if($("#tel1").val()){
			$(".tel1").removeClass("hissu");
			$(".tel1").addClass("ok");
			$("#tel1").css("background-color","#FFF");
		}else{
			$(".tel1").removeClass("ok");
			$(".tel1").addClass("hissu");
			$("#tel1").css("background-color","#fffafa");
		}
	});

	//メール
	$("input#mail1").blur(function() {
		$("div.mailerr").remove();
		if($("#mail1").val()){
			if($("#mail1").val() && !$("#mail1").val().match(/.+@.+\..+/g)){
				$("#mailerr").append("<div class='mailerr error'>入力エラー：メールアドレスの形式が異なります。</div>");
				$(".mail1").removeClass("ok");
				$(".mail1").addClass("hissu");
				$("#mail1").css("background-color","#fffafa");
			}else{
				$(".mail1").removeClass("hissu");
				$(".mail1").addClass("ok");
				$("#mail1").css("background-color","#FFF");
			}
		}else{
				$("#mailerr").append("<div class='mailerr error'>入力エラー：メールアドレスを入力してください。</div>");
			$(".mail1").removeClass("ok");
			$(".mail1").addClass("hissu");
			$("#mail1").css("background-color","#fffafa");
		}
	});


	//学歴
	$('.gakurekiradio').click(function(){
	    var idx = $(".gakurekiradio").index(this);
	    var cnt = $('.gakurekiradio').length;
	    var checked = 0;
	    for(i=0;i<cnt;i++){
		if($(".gakurekiradio").eq(i).prop("checked")){
			 checked++;
		}
	    }
	    if (checked > 0) {
			$(".school_div_id").removeClass("hissu");
			$(".school_div_id").addClass("ok");
			$(".gakurekilabel").css("background-color","#fff");
	    }else{
			$(".school_div_id").removeClass("ok");
			$(".school_div_id").addClass("hissu");
			$(".gakurekilabel").css("background-color","#fffafa");
	    }
	})

	//経験社数
	$('.compradio').click(function(){
	    var idx = $(".compradio").index(this);
	    var cnt = $('.compradio').length;
	    var checked = 0;
	    for(i=0;i<cnt;i++){
		if($(".compradio").eq(i).prop("checked")){
			 checked++;
		}
	    }
	    if (checked > 0) {
			$(".company_number").removeClass("hissu");
			$(".company_number").addClass("ok");
			$(".companylabel").css("background-color","#fff");
	    }else{
			$(".company_number").removeClass("ok");
			$(".company_number").addClass("hissu");
			$(".companylabel").css("background-color","#fffafa");
	    }
	})

	//就業状況
	$('.jokyoradio').click(function(){
	    var idx = $(".jokyoradio").index(this);
	    var cnt = $('.jokyoradio').length;
	    var checked = 0;
	    for(i=0;i<cnt;i++){
		if($(".jokyoradio").eq(i).prop("checked")){
			 checked++;
		}
	    }
	    if (checked > 0) {
			$(".jokyo").removeClass("hissu");
			$(".jokyo").addClass("ok");
			$(".jokyolabel").css("background-color","#fff");
	    }else{
			$(".jokyo").removeClass("ok");
			$(".jokyo").addClass("hissu");
			$(".jokyolabel").css("background-color","#fffafa");
	    }
	})

	//勤務地１
	$("select#expectarea1").change(function() {
		if($("#expectarea1").val()){
			$(".expectarea1").removeClass("hissu");
			$(".expectarea1").addClass("ok");
			$("#expectarea1").css("background-color","#fff");
		}else{
			$(".expectarea1").removeClass("ok");
			$(".expectarea1").addClass("hissu");
			$("#expectarea1").css("background-color","#fffafa");
		}
	});

	//勤務地２
	$("select#expectarea2").change(function() {
		if($("#expectarea2").val()){
			$(".expectarea2").removeClass("");
			$(".expectarea2").addClass("ok");
			$("#expectarea2").css("background-color","#fff");
		}else{
			$(".expectarea2").removeClass("ok");
			$(".expectarea2").addClass("");
//			$("#expectarea2").css("background-color","#fffafa");
		}
	});

	//勤務地3
	$("select#expectarea3").change(function() {
		if($("#expectarea3").val()){
			$(".expectarea3").removeClass("");
			$(".expectarea3").addClass("ok");
			$("#expectarea3").css("background-color","#fff");
		}else{
			$(".expectarea3").removeClass("ok");
			$(".expectarea3").addClass("");
//			$("#expectarea3").css("background-color","#fffafa");
		}
	});

	//希望質問
	$("textarea#comment").blur(function() {
		if($("#comment").val()){
			$(".comment").removeClass("");
			$(".comment").addClass("ok");
		}else{
			$(".comment").removeClass("ok");
			$(".comment").addClass("");
		}
	});

	//////////////////////////////////////////////////////////////////

	$('select#kibo_jiki_year').change(function() {
		if($("select[name='kibo_jiki_year']").val()){
			$("#kibou_jiki").attr("checked", false);
		}
	});
	$('select#kibo_jiki_month').change(function() {
		if($("select[name='kibo_jiki_month']").val()){
			$("#kibou_jiki").attr("checked", false);
		}
	});
/*
	$('select#kibo_jiki_day').change(function() {
		if($("select[name='kibo_jiki_day']").val()){
			$("#kibou_jiki").attr("checked", false);
		}
	});
*/

	$("#kibou_jiki").click(function() {
		$("select#kibo_jiki_year").val("");
		$("select#kibo_jiki_month").val("");
//		$("select#kibo_jiki_day").val("");
	});


	//色の制御
/*
	$(":text, textarea").keyup(function(){
		cls = $(this).attr("class");
		if(cls.match(/required/)){
			tmpId = $(this).attr("id");
			if($(this).val()){
				$(this).css("background-color","#FFF");
			}else{
				$(this).css("background-color","#fffafa");
			}
		}
	});
*/
	$(":radio").change(function(){
		var tmp_name = $(this).attr("name");
		var radio_tmp = document.getElementsByName(tmp_name);
		var cnt = 0;
		tmp = tmp_name.replace("[]", "");
		for(var i=0;i<radio_tmp.length;i++) {
			if (radio_tmp[i].checked) {
				cnt = 1;
			}
		}
		if(cnt == 0){
			$(this).parent().css("background-color","#fffafa");
		}else{
			$(this).parent().css("background-color","#fff");
		}
	});

	$("select").change(function(){
		cls = $(this).attr("class");
		if(cls.match(/required/)){
			tmpId = $(this).attr("id");
			if($(this).val()){
				$(this).css("background-color","#FFF");
			}else{
				$(this).css("background-color","#fffafa");
			}
		}
	});

	$("select[name='renraku1']").change(function(){
		renraku1 = $("select[name='renraku1']").val();
		renraku2 = $("select[name='renraku2']").val();
		if(!renraku1 && !renraku2){
			$("#renraku1").css("background-color","#fffafa");
			$("#renraku2").css("background-color","#fffafa");
		}else{
			$("#renraku1").css("background-color","#FFF");
			$("#renraku2").css("background-color","#FFF");
		}
	});
	$("select[name='renraku2']").change(function(){
		renraku1 = $("select[name='renraku1']").val();
		renraku2 = $("select[name='renraku2']").val();
		if(!renraku1 && !renraku2){
			$("#renraku1").css("background-color","#fffafa");
			$("#renraku2").css("background-color","#fffafa");
		}else{
			$("#renraku1").css("background-color","#FFF");
			$("#renraku2").css("background-color","#FFF");
		}
	});


	$("select[name='kibo_jiki_year']").change(function(){
		kibo_jiki_year = $("select[name='kibo_jiki_year']").val();
		kibo_jiki_month = $("select[name='kibo_jiki_month']").val();
		kibou_jiki = $("#kibou_jiki").prop('checked');

		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month)){
			$("#kibo_jiki_year").css("background-color","#fffafa");
			$("#kibo_jiki_month").css("background-color","#fffafa");
			$("#kb").css("background-color","#fffafa");
		}else{
			$("#kibo_jiki_year").css("background-color","#FFF");
			$("#kibo_jiki_month").css("background-color","#FFF");
			$("#kb").css("background-color","#FFF");
		}
	});

	$("select[name='kibo_jiki_month']").change(function(){
		kibo_jiki_year = $("select[name='kibo_jiki_year']").val();
		kibo_jiki_month = $("select[name='kibo_jiki_month']").val();
		kibou_jiki = $("#kibou_jiki").prop('checked');

		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month)){
			$("#kibo_jiki_year").css("background-color","#fffafa");
			$("#kibo_jiki_month").css("background-color","#fffafa");
			$("#kb").css("background-color","#fffafa");
		}else{
			$("#kibo_jiki_year").css("background-color","#FFF");
			$("#kibo_jiki_month").css("background-color","#FFF");
			$("#kb").css("background-color","#FFF");
		}
	});

	$("#kibou_jiki").change(function(){
		kibo_jiki_year = $("select[name='kibo_jiki_year']").val();
		kibo_jiki_month = $("select[name='kibo_jiki_month']").val();
		kibou_jiki = $("#kibou_jiki").prop('checked');

		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month)){
			$("#kibo_jiki_year").css("background-color","#fffafa");
			$("#kibo_jiki_month").css("background-color","#fffafa");
			$("#kb").css("background-color","#fffafa");
		}else{
			$("#kibo_jiki_year").css("background-color","#FFF");
			$("#kibo_jiki_month").css("background-color","#FFF");
			$("#kb").css("background-color","#FFF");
		}
	});

	$("#agreeChk").change(function(){
		agr = $("#agreeChk").prop('checked');
		if(agr){
			$("#btnKakunin").attr("src","img/btn_check_on.png");
	                       $("#agr").addClass('checked');
		}else{
			$("#btnKakunin").attr("src","img/btn_check_off.png");
	                       $("#agr").removeClass('checked');
		}
/*
		if(agr==0){
			$(this).parent().css("background-color","#fffafa");
		}else{
			$(this).parent().css("background-color","#fff");
		}
*/
	});
});

function check(param){

	window.onbeforeunload=null;

//$(function(){
	var tmp = "";
//	$("form").submit(function(){
		//エラーの初期化
		$("div.error").remove();
		$("div.mailerr").remove();

		$("td").removeClass("error");
		
		//テキスト、テキストエリアのチェック
		$(":text, textarea").filter(".required").each(function(){
			if($(this).val()==""){
				tmp = $(this).attr("id");
				$(this).parent().append("<div class='error'>" + data[tmp] + "を入力してください。</div>");
			}

			//数値のチェック
			$(this).filter(".number").each(function(){
				if(isNaN($(this).val())){
					$(this).parent().append("<div class='error'>数値のみ選択可能です。</div>");
				}
			});

		
/*
			//メールアドレスチェック
			$(this).filter(".mail").each(function(){
				if($(this).val() && !$(this).val().match(/.+@.+\..+/g)){
					$(this).parent().append("<div class='error'>メールアドレスの形式が異なります。</div>");
				}
			});
		
			//メールアドレス確認用
			$(this).filter(".mail_chk").each(function(){
				if($(this).val() && $(this).val() != $("input[name=" + $(this).attr("name").replace(/^(.+)_chk$/, "$1")+"]").val()){
					$(this).parent().append("<div class='error'>メールアドレスが一致しません。</div>");
				}
			});
*/
		});
		
		//電話番号
		if($("#tel1").val()=="" && $("#tel2").val()==""){
			$("#tel1").parent().append("<div class='error'>自宅もしくは携帯のどちらかは必ず御入力下さい。</div>");
		}

/*
		//メールアドレス
		if($("#mail1").val()=="" && $("#mail2").val()==""){
			$("#mail1").parent().append("<div class='error'>自宅もしくは携帯のどちらかは必ず御入力下さい。</div>");
		}
*/
		if($("#mail1").val()==""){
			$("div.mailerr").remove();
			$("#mailerr").append("<div class='mailerr error'>入力エラー：メールアドレスを入力してください。</div>");
			//$("#mail1").parent().append("<div class='error'>メールアドレスを入力して下さい。</div>");
		}else{
			$("div.mailerr").remove();
				if($("#mail1").val() && !$("#mail1").val().match(/.+@.+\..+/g)){
					$("#mailerr").append("<div class='mailerr error'>入力エラー：メールアドレスの形式が異なります。</div>");
					//$("#mail1").parent().append("<div class='error'>メールアドレスの形式が異なります。</div>");
				}
		}


		//ラジオボタンのチェック
		$(":radio").filter(".required").each(function(){
			var tmp_name = $(this).attr("name");
			var radio_tmp = document.getElementsByName(tmp_name);
			var cnt = 0;
			tmp = tmp_name.replace("[]", "");
			for(var i=0;i<radio_tmp.length;i++) {
				if (radio_tmp[i].checked) {
					cnt = 1;
				}
			}
			if(cnt == 0){
				$("#"+tmp_name+"_err").parent().append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
			}
		});
		
		//リストのチェック
		$("select").filter(".required").each(function(){
			if($("select[name="+$(this).attr("name")+"]").children(':selected').val() == ""){
				tmp = $(this).attr("id");
				$(this).parent().append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
			}
		});
		
		//チェックボックスのチェック
		$(".checkboxrequired").each(function(){
			if($(":checkbox:checked", this).size() == 0){
				tmp = $("input",this).attr("name");
				//name属性を配列にしているために[]を削除
				tmp = tmp.replace("[]", "");
				if(tmp=="agree"){
					$(this).append("<div class='error'>" + data[tmp] + "</div>");
				}else{
					$(this).append("<div class='error'>" + data[tmp] + "を選択してください。</div>");
				}
			}
		});

		//環境依存文字簡易チェック
		$(":text, textarea").filter(".izon").each(function(){
			var show = $(this).attr("id");
			var text = $(this).val();
			var c_regP = "[①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳ⅠⅡⅢⅣⅤⅥⅦⅧⅨⅩ㍉㌔㌢㍍㌘㌧㌃㌶㍑㍗㌍㌦㌣㌫㍊㌻㎜㎝㎞㎎㎏㏄㎡㍻〝〟№㏍℡㊤㊥㊦㊧㊨㈱㈲㈹㍾㍽㍼]";
			if(text.match(c_regP)){
				$(this).parent().append("<div class='error'>" + data[show] + "に環境依存文字が入力されています。</div>");
			}
		});
		
		renraku1 = $("select[name='renraku1']").val();
		renraku2 = $("select[name='renraku2']").val();
		if(!renraku1 && !renraku2){
				$("#renraku1").parent().append("<div class='error'>連絡希望時間を選択して下さい。</div>");
		}

		kibo_jiki_year = $("select[name='kibo_jiki_year']").val();
		kibo_jiki_month = $("select[name='kibo_jiki_month']").val();
//		kibo_jiki_day = $("select[name='kibo_jiki_day']").val();
		kibou_jiki = $("#kibou_jiki").prop('checked');

//		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month || !kibo_jiki_day)){
		if(kibou_jiki==0 && (!kibo_jiki_year || !kibo_jiki_month)){
				$("#kibo_jiki_year").parent().append("<div class='error'>転職希望時期を入力して下さい。</div>");
		}

		e1 = $("#expectarea1").val();
		e2 = $("#expectarea2").val();
		e3 = $("#expectarea3").val();

		if(e1 && e2){
			if(e1 == e2){
				$("#expectarea1").parent().append("<div class='error'>勤務地（第1希望）と勤務地（第2希望）は別の都道府県を選択してください。。</div>");
			}
		}
		if(e1 && e3){
			if(e1 == e3){
				$("#expectarea1").parent().append("<div class='error'>勤務地（第1希望）と勤務地（第3希望）は別の都道府県を選択してください。。</div>");
			}
		}
		if(e2 && e3){
			if(e2 == e3){
				$("#expectarea2").parent().append("<div class='error'>勤務地（第2希望）と勤務地（第3希望）は別の都道府県を選択してください。。</div>");
			}
		}
		//エラー処理
		if($("div.error").size() > 0){
			$('html,body').animate({ scrollTop: $("div.error:first").offset().top-150}, 'slow');
			return false;
		}else{
			return true;
		}

//	});
//});
}