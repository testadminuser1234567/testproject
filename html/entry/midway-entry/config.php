<?php
//入力ページの設定
define('Index', 'index.html');
//確認ページの設定（使わない場合はこのまま）
define('Confirm', 'confirm.php');
//完了ページの設定
define('Finish', 'finish.php');

define('ENCODE', 'UTF-8');

//ユーザの名前設定
$unit_name = $onamae . "様";
$email = $mail;
$send_from = "recruit@hurex.co.jp";

//サンクスメール設定
$thanks_mail_subject = "中途採用へのエントリーありがとうございました。【ヒューレックス株式会社】";
$thanks_mail_from = "recruit@hurex.co.jp";
$thanks_mail_str = "ヒューレックス株式会社";
$thanks_body= <<< EOF
この度はヒューレックス株式会社公式ホームページ
中途採用エントリーフォームよりご応募頂きまして誠にありがとうございました。

下記内容にて受け付けましたので内容をご確認下さいませ。

――――――――――――――――――――
【送信内容】
希望職種： $job
氏名    ： $onamae
フリガナ： $kana
郵便番号： $zip
住所　　： $address
電話番号： $tel
E-mail  ： $mail
生年月日： $birthday

履歴書　　： $file1
職務経歴書： $file2
――――――――――――――――――――

内容確認の上、担当よりご連絡いたします。

どうぞ宜しくお願いいたします。

※本メールは自動返信されております。
※本アドレスは送信専用となっております。
ご返信いただいてもご回答できかねますので、ご了承ください。

EOF;

//通知メール設定
$order_mail_subject = "【中途採用】HPよりエントリーがありました。";
$order_mail_to[] = array('recruit@hurex.co.jp', 'ヒューレックス株式会社');
//$order_mail_to[] = array('tosa@smt-net.co.jp', 'ヒューレックス');
//$order_mail_to[] = array('info@smt-net.co.jp', '追加宛先');
$order_body= <<< EOF
ホームページ【中途採用ページ】より
下記内容にてエントリーがありました。

────────────────────────────
▼送信内容
────────────────────────────

希望職種： $job
氏名    ： $onamae
フリガナ： $kana
郵便番号： $zip
住所　　： $address
電話番号： $tel
E-mail  ： $mail
生年月日： $birthday

履歴書　　： $file1
職務経歴書： $file2


EOF;

//項目チェック用
$validation->set_rules('job','希望職種','required');
$validation->set_rules('onamae','氏名','required');
$validation->set_rules('kana','フリガナ','required');
$validation->set_rules('zip','郵便番号','required');
$validation->set_rules('address','住所','required');
$validation->set_rules('tel','電話番号','required');
$validation->set_rules('birthday','生年月日','required');
$validation->set_rules('mail','E-mail','required|valid_email');

//ログファイル出力 on or off
$log = "off";

?>