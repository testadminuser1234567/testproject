<?php
session_start();
/*
ini_set('error_reporting', E_ALL);
ini_set('display_errors', '1');
*/
/*
/  データ取得
*/
include("config.php");

//パラメータ格納用
$param = array();
$param['phase'] = 6;
$param['publish'] = 11103;
$param['recommend']= 10591;

///////////////////////////////////////////////
// 都道府県から検索
///////////////////////////////////////////////
$tmppref=array();
foreach($prefecture as $k=>$v){
	$tmppref[$v] = $k;
}
$prefAry = array();

$kitakantoSearch="";
$kitakantoParam="";
$kitakantoVal = array();

$otherSearch="";
$otherParam="";
$otherVal = array();
foreach($tmppref as $k=>$v){
	if(preg_match('/秋田/',$k)){
		$akita = $v;
	}else if(preg_match('/青森/',$k)){
		$aomori = $v;
	}else if(preg_match('/岩手/',$k)){
		$iwate = $v;
	}else if(preg_match('/宮城/',$k)){
		$miyagi = $v;
	}else if(preg_match('/山形/',$k)){
		$yamagata = $v;
	}else if(preg_match('/福島/',$k)){
		$fukushima = $v;
	}else if(preg_match('/茨城/',$k) || preg_match('/栃木/',$k)){
		if(empty($kitakantoSearch)) {
			$kitakantoSearch.=" and ( ";
		}else{
			$kitakantoSearch.=" or ";
		}
		$kitakantoVal[] = $v;
		$kitakantoParam = ':kitakanto'.$v;
		$kitakantoSearch .= " FIND_IN_SET(" . $kitakantoParam . ",j.prefecture_id)";

	}else if(!preg_match('/秋田/',$k) && !preg_match('/青森/',$k) && !preg_match('/秋田/',$k) && !preg_match('/岩手/',$k) && !preg_match('/宮城/',$k) && !preg_match('/山形/',$k) && !preg_match('/福島/',$k) && !preg_match('/茨城/',$k) && !preg_match('/栃木/',$k)){
		if(empty($otherSearch)) {
			$otherSearch.=" and ( ";
		}else{
			$otherSearch.=" or ";
		}
		$otherVal[] = $v;
		$otherParam = ':other'.$v;
		$otherSearch .= " FIND_IN_SET(" . $otherParam . ",j.prefecture_id)";
	}
}

if($kitakantoSearch) $kitakantoSearch .= " ) ";

if($otherSearch) $otherSearch .= " ) ";

/////////////////////////////////////////////////
// 都道府県ここまで
/////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////
// 青森抽出
////////////////////////////////////////////////////////////////////
$param["aomori"] = $aomori;
$sql_list = "SELECT j.job_id, j.job_title, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish and FIND_IN_SET(:aomori, j.prefecture_id) and j.recommend=:recommend group by j.job_id order by j.j_updated desc limit 0, 25";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$aomori = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$aomori[] = $result;
}
unset($param["aomori"]);


////////////////////////////////////////////////////////////////////
// 岩手抽出
////////////////////////////////////////////////////////////////////
$param["iwate"] = $iwate;
$sql_list = "SELECT j.job_id, j.job_title, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish and FIND_IN_SET(:iwate, j.prefecture_id) and j.recommend=:recommend group by j.job_id order by j.j_updated desc limit 0, 25";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$iwate = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$iwate[] = $result;
}
unset($param["iwate"]);

////////////////////////////////////////////////////////////////////
// 宮城抽出
////////////////////////////////////////////////////////////////////
$param["miyagi"] = $miyagi;
$sql_list = "SELECT j.job_id, j.job_title, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish and FIND_IN_SET(:miyagi, j.prefecture_id) and j.recommend=:recommend group by j.job_id order by j.j_updated desc limit 0, 25";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$miyagi = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$miyagi[] = $result;
}
unset($param["miyagi"]);

////////////////////////////////////////////////////////////////////
// 秋田抽出
////////////////////////////////////////////////////////////////////
$param["akita"] = $akita;
$sql_list = "SELECT j.job_id, j.job_title, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish and FIND_IN_SET(:akita, j.prefecture_id) and j.recommend=:recommend group by j.job_id order by j.j_updated desc limit 0, 25";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$akita = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$akita[] = $result;
}
unset($param["akita"]);

////////////////////////////////////////////////////////////////////
// 山形抽出
////////////////////////////////////////////////////////////////////
$param["yamagata"] = $yamagata;
$sql_list = "SELECT j.job_id, j.job_title, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish and FIND_IN_SET(:yamagata, j.prefecture_id) and j.recommend=:recommend group by j.job_id order by j.j_updated desc limit 0, 25";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$yamagata = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$yamagata[] = $result;
}
unset($param["yamagata"]);

////////////////////////////////////////////////////////////////////
// 福島抽出
////////////////////////////////////////////////////////////////////
$param["fukushima"] = $fukushima;
$sql_list = "SELECT j.job_id, j.job_title, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish and FIND_IN_SET(:fukushima, j.prefecture_id) and j.recommend=:recommend group by j.job_id order by j.j_updated desc limit 0, 25";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$fukushima = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$fukushima[] = $result;
}
unset($param["fukushima"]);

////////////////////////////////////////////////////////////////////
// 北関東
////////////////////////////////////////////////////////////////////
if(!empty($kitakantoVal)){
	foreach($kitakantoVal as $k=>$v){
		$param['kitakanto' . $v ] = $v;
	}
}
$sql_list = "SELECT j.job_id, j.job_title, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish " . $kitakantoSearch . " and j.recommend=:recommend group by j.job_id order by j.j_updated desc limit 0, 25";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$kitakanto = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$kitakanto[] = $result;
}
//パラメータリセット
foreach($param as $k=>$v){
	if(preg_match("/kitakanto/",$k)){
		unset($param[$k]);
	}
}

////////////////////////////////////////////////////////////////////
// その他抽出
////////////////////////////////////////////////////////////////////
if(!empty($otherVal)){
	foreach($otherVal as $k=>$v){
		$param['other' . $v ] = $v;
	}
}
$sql_list = "SELECT j.job_id, j.job_title, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish " . $otherSearch . " and j.recommend=:recommend group by j.job_id order by j.j_updated desc limit 0, 25";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$other = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$other[] = $result;
}
//パラメータリセット
foreach($param as $k=>$v){
	if(preg_match("/other/",$k)){
		unset($param[$k]);
	}
}