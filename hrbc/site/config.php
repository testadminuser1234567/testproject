<?php

//hrbcシステムディレクトリ
$dir = $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/";

//一覧表示件数
define('PAGE_CNT', 20);


//security
session_start();
define('ENVIRONMENT', 'production');
define('BASEPATH', $_SERVER["DOCUMENT_ROOT"] .'/syskanri/system/');
define('APPPATH', $_SERVER["DOCUMENT_ROOT"] .'/syskanri/application/');
require(APPPATH . 'config/config.php');
require(APPPATH . 'config/database.php');
require(BASEPATH . 'core/Common.php');
require(BASEPATH . 'database/DB.php');

require_once($_SERVER["DOCUMENT_ROOT"] .  '/syskanri/application/config/config.php');

//PDO
$dsn = 'mysql:dbname=' . dbDatabase .';host=' . dbHostname . ';port=' . dbPort;
$user = dbUsername;
$password = dbPassword;
		
try{
	$dbh = new PDO($dsn, $user, $password,
		array(
		     	PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET `utf8`"
		));

}catch (PDOException $e){
	print('Error:'.$e->getMessage());
	die();
}

function hp($dirty_html) {
	require_once($_SERVER["DOCUMENT_ROOT"] .  '/syskanri/htmlpurifier/library/HTMLPurifier.auto.php');
	$config = HTMLPurifier_Config::createDefault();
	$config->set('Core.Encoding', 'UTF-8');
	$config->set('HTML.Doctype', 'XHTML 1.0 Transitional');
	$config->set('Attr.AllowedFrameTargets', array('_blank','_self'));
	$config->set('HTML.Trusted', true);
	$purifier = new HTMLPurifier($config);
	$clean_html = $purifier->purify( $dirty_html );
	return $clean_html;
}