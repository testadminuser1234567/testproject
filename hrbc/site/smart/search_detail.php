<?php
/*
/  詳細ページのデータ取得用
*/
include($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/site/config.php");

$id = $_GET['id'];
$id = htmlspecialchars($id, ENT_QUOTES, 'UTF-8');
if(!is_numeric($id)){
	$id = 0;
}

//パラメータ格納用
$param = array();
$param['phase'] = 6;
$param['publish'] = 11103;
$param['jobid'] = $id;

/////////////////////////////////////////////////////////////////////
// 各都道府県用のパラメータ
// [75] => 北海道 [76] => 青森県 [77] => 岩手県 [78] => 宮城県 [79] => 秋田県 [80] => 山形県 [81] => 福島県 [82] => 茨城県 [83] => 栃木県 [84] => 群馬県 [85] => 埼玉県 [86] => 千葉県 [87] => 東京都 [88] => 神奈川県 [89] => 新潟県 [90] => 富山県 [91] => 石川県 [92] => 福井県 [93] => 山梨県 [94] => 長野県 [95] => 岐阜県 [96] => 静岡県 [97] => 愛知県 [98] => 三重県 [99] => 滋賀県 [100] => 京都府 [101] => 大阪府 [102] => 兵庫県 [103] => 奈良県 [104] => 和歌山県 [105] => 鳥取県 [106] => 島根県 [107] => 岡山県 [108] => 広島県 [109] => 山口県 [110] => 徳島県 [111] => 香川県 [112] => 愛媛県 [113] => 高知県 [114] => 福岡県 [115] => 佐賀県 [116] => 長崎県 [117] => 熊本県 [118] => 大分県 [119] => 宮崎県 [120] => 鹿児島県 [121] => 沖縄県 [11453] => 海外
/////////////////////////////////////////////////////////////////////
$add_sql="";
$url = $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
//北海道
if (strstr($url, '/hokkaido-search/')) {
	$add_sql = " and FIND_IN_SET (75, prefecture_id)";
//青森
}else if (strstr($url, '/aomori-search/')) {
	$add_sql = " and FIND_IN_SET (76, prefecture_id)";
//岩手
}else if (strstr($url, '/iwate-search/')) {
	$add_sql = " and FIND_IN_SET (77, prefecture_id)";
//秋田
}else if (strstr($url, '/miyagi-search/')) {
	$add_sql = " and FIND_IN_SET (78, prefecture_id)";
//山形
}else if (strstr($url, '/akita-search/')) {
	$add_sql = " and FIND_IN_SET (79, prefecture_id)";
//宮城
}else if (strstr($url, '/yamagata-search/')) {
	$add_sql = " and FIND_IN_SET (80, prefecture_id)";
//福島
}else if (strstr($url, '/fukushima-search/')) {
	$add_sql = " and FIND_IN_SET (81, prefecture_id)";
//茨城
}else if (strstr($url, '/ibaraki-search/')) {
	$add_sql = " and FIND_IN_SET (82, prefecture_id)";
//栃木
}else if (strstr($url, '/tochigi-search/')) {
	$add_sql = " and FIND_IN_SET (83, prefecture_id)";
//群馬
}else if (strstr($url, '/gunma-search/')) {
	$add_sql = " and FIND_IN_SET (84, prefecture_id)";
//埼玉
}else if (strstr($url, '/saitama-search/')) {
	$add_sql = " and FIND_IN_SET (85, prefecture_id)";
//千葉
}else if (strstr($url, '/chiba-search/')) {
	$add_sql = " and FIND_IN_SET (86, prefecture_id)";
//東京
}else if (strstr($url, '/tokyo-search/')) {
	$add_sql = " and FIND_IN_SET (87, prefecture_id)";
//神奈川
}else if (strstr($url, '/kanagawa-search/')) {
	$add_sql = " and FIND_IN_SET (88, prefecture_id)";
//新潟
}else if (strstr($url, '/niigata-search/')) {
	$add_sql = " and FIND_IN_SET (89, prefecture_id)";
//富山
}else if (strstr($url, '/toyama-search/')) {
	$add_sql = " and FIND_IN_SET (90, prefecture_id)";
//石川
}else if (strstr($url, '/ishikawa-search/')) {
	$add_sql = " and FIND_IN_SET (91, prefecture_id)";
//福井
}else if (strstr($url, '/fukui-search/')) {
	$add_sql = " and FIND_IN_SET (92, prefecture_id)";
//山梨
}else if (strstr($url, '/yamanashi-search/')) {
	$add_sql = " and FIND_IN_SET (93, prefecture_id)";
//長野
}else if (strstr($url, '/nagano-search/')) {
	$add_sql = " and FIND_IN_SET (94, prefecture_id)";
//岐阜
}else if (strstr($url, '/gifu-search/')) {
	$add_sql = " and FIND_IN_SET (95, prefecture_id)";
//静岡
}else if (strstr($url, '/shizuoka-search/')) {
	$add_sql = " and FIND_IN_SET (96, prefecture_id)";
//愛知
}else if (strstr($url, '/aichi-search/')) {
	$add_sql = " and FIND_IN_SET (97, prefecture_id)";
//三重
}else if (strstr($url, '/mie-search/')) {
	$add_sql = " and FIND_IN_SET (98, prefecture_id)";
//滋賀
}else if (strstr($url, '/shiga-search/')) {
	$add_sql = " and FIND_IN_SET (99, prefecture_id)";
//京都
}else if (strstr($url, '/kyoto-search/')) {
	$add_sql = " and FIND_IN_SET (100, prefecture_id)";
//大阪
}else if (strstr($url, '/osaka-search/')) {
	$add_sql = " and FIND_IN_SET (101, prefecture_id)";
//兵庫
}else if (strstr($url, '/hyogo-search/')) {
	$add_sql = " and FIND_IN_SET (102, prefecture_id)";
//奈良
}else if (strstr($url, '/nara-search/')) {
	$add_sql = " and FIND_IN_SET (103, prefecture_id)";
//和歌山
}else if (strstr($url, '/wakayama-search/')) {
	$add_sql = " and FIND_IN_SET (104, prefecture_id)";
//鳥取
}else if (strstr($url, '/tottori-search/')) {
	$add_sql = " and FIND_IN_SET (105, prefecture_id)";
//島根
}else if (strstr($url, '/shimane-search/')) {
	$add_sql = " and FIND_IN_SET (106, prefecture_id)";
//岡山
}else if (strstr($url, '/okayama-search/')) {
	$add_sql = " and FIND_IN_SET (107, prefecture_id)";
//広島
}else if (strstr($url, '/hiroshima-search/')) {
	$add_sql = " and FIND_IN_SET (108, prefecture_id)";
//山口
}else if (strstr($url, '/yamaguchi-search/')) {
	$add_sql = " and FIND_IN_SET (109, prefecture_id)";
//徳島
}else if (strstr($url, '/tokushima-search/')) {
	$add_sql = " and FIND_IN_SET (110, prefecture_id)";
//香川
}else if (strstr($url, '/kagawa-search/')) {
	$add_sql = " and FIND_IN_SET (111, prefecture_id)";
//愛媛
}else if (strstr($url, '/ehime-search/')) {
	$add_sql = " and FIND_IN_SET (112, prefecture_id)";
//高知
}else if (strstr($url, '/kochi-search/')) {
	$add_sql = " and FIND_IN_SET (113, prefecture_id)";
//福岡
}else if (strstr($url, '/fukuoka-search/')) {
	$add_sql = " and FIND_IN_SET (114, prefecture_id)";
//佐賀
}else if (strstr($url, '/saga-search/')) {
	$add_sql = " and FIND_IN_SET (115, prefecture_id)";
//長崎
}else if (strstr($url, '/nagasaki-search/')) {
	$add_sql = " and FIND_IN_SET (116, prefecture_id)";
//熊本
}else if (strstr($url, '/kumamoto-search/')) {
	$add_sql = " and FIND_IN_SET (117, prefecture_id)";
//大分
}else if (strstr($url, '/oita-search/')) {
	$add_sql = " and FIND_IN_SET (118, prefecture_id)";
//宮崎
}else if (strstr($url, '/miyazaki-search/')) {
	$add_sql = " and FIND_IN_SET (119, prefecture_id)";
//鹿児島
}else if (strstr($url, '/kagoshima-search/')) {
	$add_sql = " and FIND_IN_SET (120, prefecture_id)";
//沖縄
}else if (strstr($url, '/okinawa-search/')) {
	$add_sql = " and FIND_IN_SET (121, prefecture_id)";
//海外
}else if (strstr($url, '/kaigai-search/')) {
	$add_sql = " and FIND_IN_SET (11453, prefecture_id)";
}


//データ取得
$sql_list = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish and j.job_id = :jobid" . $add_sql." group by j.job_id";
//$sql_list = "SELECT j.job_id, j.job_title, j.client_id, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, group_concat(distinct i.i_id separator '|') as i_id, group_concat(distinct i.i_name separator '|') as i_name, group_concat(distinct p.p_name order by p_id asc separator '|') as p_name, group_concat(distinct p.p_id order by p_id asc separator '|') as p_id, group_concat(distinct jc.jc_id separator '|') as jc_id, group_concat(distinct e.e_name order by e_id asc separator '|') as e_name, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join industries as i on j.client_id = i.client_id left join prefectures as p on j.job_id = p.job_id left join jobcategories as jc on .j.job_id = jc.job_id left join employments as e on j.job_id = e.job_id left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish and j.job_id = :jobid group by j.job_id";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$datas = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$datas[] = $result;
}
$data = $datas[0];

if(empty($data)){
	$redirectUrl = "http://www.hurex.jp/smart/job-search/404.html";
	header("HTTP/1.0 404 Not Found");
	print_r(file_get_contents($redirectUrl));
	exit;

}

////////////////////////////////////////////
// 類似データ取得
////////////////////////////////////////////

$param_related['phase'] = $param['phase'];
$param_related['publish'] = $param['publish'];

//業種
$bizSearch="";
$bizSql = " left join clients as c on j.client_id = c.c_id ";
$bizSql2 = ", group_concat(distinct c.c_industry_name separator '|') as i_name";
if(!empty($data['c_industry_id'])){
	$param_related["c_industry_id"] = $data['c_industry_id'];
	$bizSearch = " FIND_IN_SET(c.c_industry_id, :c_industry_id)";
}

//職種
$jobSearch="";
if(!empty($data['jobcategory_id'])){
	$param_related["jobcategory"] = $data['jobcategory_id'];
	$jobSearch = " or FIND_IN_SET(j.jobcategory_id ,:jobcategory ) ";
}



//都道府県
$prefSearch="";
if(!empty($data['prefecture_id'])){
	$param_related["prefecture_id"] = $data['prefecture_id'];
	$prefSearch = " or FIND_IN_SET(j.prefecture_id ,:prefecture_id ) ";
}

$sql_list = "SELECT j.job_id, j.job_title, j.client_id, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.j_updated, j.prefecture_name, j.jobcategory_name,j.employ_name" . $bizSql2 . " FROM jobs as j" . $bizSql . " where (j.phaze = :phase and j.publish =:publish)  and ( " . $bizSearch . $jobSearch . $prefSearch . " ) group by j.job_id order by rand() limit 0, 4";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param_related);
$related = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$related[] = $result;
}
