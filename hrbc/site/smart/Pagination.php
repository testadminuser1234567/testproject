<?php

class Pagination {

	var $base_url			= ''; // The page we are linking to
	var $total_rows  		= ''; // Total number of items (database results)
	var $per_page	 		= 10; // Max number of items you want shown per page
	var $num_links			=  2; // Number of "digit" links to show before/after the currently viewed page
	var $move_page	 		=  0; // The current page being viewed
	var $first_link   		= '&lsaquo; First';
	var $next_link			= '&gt;';
	var $prev_link			= '&lt;';
	var $last_link			= 'Last &rsaquo;';
	var $uri_segment		= 3;
	var $full_tag_open		= '<ul class="clearfix">';
	var $full_tag_close		= '</ul>';
	var $first_tag_open		= '<li>';
	var $first_tag_close		= '</li>';
	var $last_tag_open		= '<li>';
	var $last_tag_close		= '</li>';
	var $cur_tag_open		= "<li><span>";
	var $cur_tag_close		= '</span></li>';
	var $next_tag_open		= '<li>';
	var $next_tag_close		= '</li>';
	var $prev_tag_open		= '<li>';
	var $prev_tag_close		= '</li>';
	var $num_tag_open		= '<li>';
	var $num_tag_close		= '</li>';
	var $page_query_string	= FALSE;
	var $query_string_segment = 'per_page';
	var $cur_page			= 0;
	var $offset				= '';
	var $page				= 0;
	var $total_page			= 0;
	var $date			= '';
	var $ky	="";

	function Pagination($params = array())
	{
		if (count($params) > 0)
		{
			$this->initialize($params);		
		}
	}
	
	function initialize($params = array())
	{
		if (count($params) > 0)
		{
			foreach ($params as $key => $val)
			{
				if (isset($this->$key))
				{
					$this->$key = $val;
				}
			}
		}
	}
	
	function get_offset()
	{
		if (null != $this->page && 0 != $this->page ) {
			$this->page = (int) $this->page;
		}else{
			$this->page = 1;
		}

		$this->offset = $this->page * $this->per_page - $this->per_page;
		$this->total_page = ceil($this->total_rows/$this->per_page);
		if($this->total_rows < $this->offset){
			$this->offset = $this->total_rows - ($this->total_rows - $this->per_page * ($this->total_page - 1));
		}
		
		return $this->offset;
	}
	
	
	function create_links()
	{
		//0件は出力なし
		if ($this->total_rows == 0 OR $this->per_page == 0)
		{
		   return '';
		}

		//総ページ数計算
		$num_pages = ceil($this->total_rows / $this->per_page);
		//総ページが1ページだったら終了
		if ($num_pages == 1)
		{
			return '';
		}

		//現在のページ取得
		if($this->cur_page){
			$this->cur_page = (int) $this->cur_page;
		}else{
			$this->cur_page = 1;
		}

		if($this->cur_page != 1 && $this->cur_page != ''){
//			$this->move_page = ($this->cur_page) + $this->per_page;
			//現在のページ×ページ毎の表示件数
			$this->move_page = ($this->cur_page) * $this->per_page;
		}else{
			$this->move_page = 0;
		}

		$this->num_links = (int)$this->num_links;
		
		if ($this->num_links < 1)
		{
			show_error('Your number of links must be a positive number.');
		}
				
		if ( ! is_numeric($this->cur_page))
		{
			$this->move_page = 0;
		}

		//ページが最終頁を超えたら最終頁をセット
		if ($this->move_page > $this->total_rows)
		{
			$this->cur_page = ceil( $this->total_rows / $this->per_page);
			$this->move_page = ($this->cur_page) * $this->per_page;
//			$this->move_page = 0;
		}
		
		$uri_page_number = $this->move_page;
		$this->move_page = floor(($this->move_page/$this->per_page) + 1);

		//開始と終了の計算
		$start = (($this->cur_page - $this->num_links) > 0) ? $this->cur_page - ($this->num_links - 1) : 1;
		$end   = (($this->cur_page + $this->num_links) < $num_pages) ? $this->cur_page + $this->num_links : $num_pages;

		$this->base_url = rtrim($this->base_url, '/') .'?';

		$output = '';
		$parameter="";
		if($this->ky){
			$parameter = "&ky=" . $this->ky;
		}

		//最初のリンク
		if  ($this->move_page > ($this->num_links + 1))
		{
				$output .= $this->first_tag_open.'<a href="'.$this->base_url.'page=1' . $parameter .'#searchResult'.'">'.$this->first_link.'</a>'.$this->first_tag_close;
		}

		//前へのリンク
		if  ($this->move_page != 1)
		{
//			$i = $uri_page_number - $this->per_page;
			$i = $this->cur_page -1;
			if ($i == 0){
				$i = '';
			}else{
				$i = 'page=' . $i;
			}

				$output .= $this->prev_tag_open.'<a href="'.$this->base_url. $i.$parameter .'#searchResult">'.$this->prev_link.'</a>'.$this->prev_tag_close;
		}

		// digit リンク
		for ($loop = $start-1; $loop <= $end; $loop++)
		{
			$i = ($loop * $this->per_page) - $this->per_page;
			$i = $loop;

			if ($i > 0)
			{
				if ($this->cur_page == $loop)
				{
					$output .= "" . $this->cur_tag_open.$loop.$this->cur_tag_close . ""; // Current page
				}
				else
				{
					$n = ($i == 0) ? '' : 'page='.$i;
	
						$output .= $this->num_tag_open.'<a href="'.$this->base_url.$n.$parameter.'#searchResult">'.$loop.'</a>'.$this->num_tag_close;
				}
			}
		}

		//次へ
		if ($this->move_page <= $num_pages)
		{
				$output .= $this->next_tag_open.'<a href="'.$this->base_url. 'page=' .($this->cur_page + 1).$parameter.'#searchResult">'.$this->next_link.'</a>'.$this->next_tag_close;
		}

		//最期へ
		if (($this->move_page + $this->num_links) <= $num_pages)
		{
				$output .= $this->last_tag_open.'<a href="'.$this->base_url . 'page=' .$num_pages.$parameter.'#searchResult">'.$this->last_link.'</a>'.$this->last_tag_close;
		}

		//スラッシュの対応
		$output = preg_replace("#([^:])//+#", "\\1/", $output);

		// Add the wrapper HTML if exists
		$output = $this->full_tag_open.$output.$this->full_tag_close;
		
		return $output;		
	}
}
