<form method ="post" name="searchpage" action="./index.html#searchResult">
<table>
<tr>
<th>業種</th>
<td>
							<p>
							<select name="biz1" style="width:190px;">
								<option value="">--------</option>
								<?php foreach($biz as $k => $v):?>
								<option value="<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['biz1'] == $k):?>selected<?php endif;?>><?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?>全般</option>
								<?php endforeach;?>
							</select>
							</p>
							<p>
							<select name="biz2" style="width:190px;">
								<option value="">--------</option>
								<?php foreach($biz as $k => $v):?>
								<option value="<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['biz2'] == $k):?>selected<?php endif;?>><?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?>全般</option>
								<?php endforeach;?>
							</select>
							</p>
							<p>
							<select name="biz3" style="width:190px;">
								<option value="">--------</option>
								<?php foreach($biz as $k => $v):?>
								<option value="<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['biz3'] == $k):?>selected<?php endif;?>><?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?>全般</option>
								<?php endforeach;?>
							</select>
							</p>
							<p class="aRight">※業種は3つまで選べます </p>
</td>
<tr>
<th>職種</th>
<td>
							<p>
							<select name="job1"  style="width:190px;">
									<option value="">--------</option>
									<?php foreach($job as $k=>$v):?>
										<?php if(substr($k,0,3)=="opt"):?>
										<optgroup label="<?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?>">
										<?php else:?>
										<option value="<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['job1'] == $k):?>selected<?php endif;?>><?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?></option>
										<?php endif;?>
									<?php endforeach;?>
							</select>
							</p>
							<p>
							<select name="job2"  style="width:190px;">
									<option value="">--------</option>
									<?php foreach($job as $k=>$v):?>
										<?php if(substr($k,0,3)=="opt"):?>
										<optgroup label="<?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?>">
										<?php else:?>
										<option value="<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['job2'] == $k):?>selected<?php endif;?>><?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?></option>
										<?php endif;?>
									<?php endforeach;?>
							</select>
							</p>
							<p>
							<select name="job3"  style="width:190px;">
									<option value="">--------</option>
									<?php foreach($job as $k=>$v):?>
										<?php if(substr($k,0,3)=="opt"):?>
										<optgroup label="<?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?>">
										<?php else:?>
										<option value="<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['job3'] == $k):?>selected<?php endif;?>><?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?></option>
										<?php endif;?>
									<?php endforeach;?>
							</select>
							</p>
							<p class="aRight">※職種は3つまで選べます </p>
</td>
</tr>
<tr>
<th>勤務地</th>
<td>
							<select name="pref" disabled>
									<option value="" selected><?php echo htmlspecialchars($prefname, ENT_QUOTES, 'UTF-8');?></option>
							</select>													</td>
</tr>
<tr>
<th>年収</th>
<td>
							<select name="year_income" style="width:190px;">
								<option value=""  selected>--------</option>
								<option value="100" <?php if($_SESSION['year_income'] == 100):?>selected<?php endif;?>>100万円以上</option>
								<option value="200" <?php if($_SESSION['year_income'] == 200):?>selected<?php endif;?>>200万円以上</option>
								<option value="300" <?php if($_SESSION['year_income'] == 300):?>selected<?php endif;?>>300万円以上</option>
								<option value="400" <?php if($_SESSION['year_income'] == 400):?>selected<?php endif;?>>400万円以上</option>
								<option value="500" <?php if($_SESSION['year_income'] == 500):?>selected<?php endif;?>>500万円以上</option>
								<option value="600" <?php if($_SESSION['year_income'] == 600):?>selected<?php endif;?>>600万円以上</option>
								<option value="700" <?php if($_SESSION['year_income'] == 700):?>selected<?php endif;?>>700万円以上</option>
								<option value="800" <?php if($_SESSION['year_income'] == 800):?>selected<?php endif;?>>800万円以上</option>
								<option value="900" <?php if($_SESSION['year_income'] == 900):?>selected<?php endif;?>>900万円以上</option>
								<option value="1000" <?php if($_SESSION['year_income'] == 1000):?>selected<?php endif;?>>1000万円以上</option>
							</select>
</td>
</tr>
<tr>
<th colspan="2">キーワード<br>
						<input type="text" name="keyword" value="<?php echo htmlspecialchars($_SESSION['keyword'], ENT_QUOTES, 'UTF-8');?>" style="width:90%;" />
							<br />（スペース区切り
							<label style="margin-left:10px"><input type="radio" name="keyword_flg" value="or" <?php if($_SESSION['keyword_flg']=="or" || empty($_SESSION['keyword_flg'])):?>checked<?php endif;?>>or</label>
							<label><input type="radio" name="keyword_flg" value="and" <?php if($_SESSION['keyword_flg']=="and"):?>checked<?php endif;?>>and</label>）
</th>
</tr>
</table>
<p class="aCenter"><input type="image" src="../images/btn_search.png" width="205" height="46"></p>
</form>
