&nbsp;<?php
/*
/    詳細ページのテンプレート
*/
?>

<!-- btnDetailBox floating  start-->
<div id="btnDetailBox">
   <div class="inner">
       <ul class="clearfix">
    <li class="btnMore"><a href="../jump/index.php?id=<?php echo htmlspecialchars($data['job_id'], ENT_QUOTES, 'UTF-8');?>&detail=floating" class="detail_cv_sp_floating_bottom" target="_blank">求人の詳細を聞く</a></li>
       <li class="btnSearch"><a data-remodal-target="modal" href="#modal"><img src="../images/btn_search_floating.png" width="60" height="43" alt="求人検索"/></a></li>
       </ul>
       <script>
        /*$(function() {
    var btn = $('#btnDetailBox').css("display","none");   

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
           btn.css("display","block").stop().animate({'top' : '0px'}, 200);
        } else {
            btn.stop().animate({'top' : '-60px'}, 200, function(){
				btn.css("display","none");
				});
        }
    });

});*/
       </script>
    </div>
</div>
<!-- btnDetailBox floating  end -->

<?php if(empty($data)):?>
<div class="detailContents">
		<p>お探しの求人情報はございません。</p>
			<p style = "text-align : center; padding : 20px !important;">
						<a href="./index.html">一覧に戻る</a>
			</p>

<?php else:?>
<!-- box start -->
<section class="box jobDetail">

		<!-- 求人詳細 -->
		<h1><span><?php echo htmlspecialchars($data['job_title'], ENT_QUOTES, 'UTF-8');?></span></h1>
<div class="boxSub">

			<p class="aCenter btnFlg">
						<a href="../jump/index.php?id=<?php echo htmlspecialchars($data['job_id'], ENT_QUOTES, 'UTF-8');?>&detail=button01_top" class="detail_cv_sp_top_new" target="_blank"><img src="../images/btn_entry_detail.png" alt="求人の詳細を聞く" width="85%" /></a></p>
<!--<div class="btnReentry">
<p class="aCenter"><a href="https://www.hurex.jp/smart/reentry/index.php?id=<?php echo htmlspecialchars($data['job_id'], ENT_QUOTES, 'UTF-8');?>" class="detail_cv_sp_top_repeat">過去にご登録頂いた事がある方は<span class="underline">コチラ</span></a></p>
</div>-->
<p class="headline">求人詳細情報 [JOB<?php echo htmlspecialchars($data['job_id'], ENT_QUOTES, 'UTF-8');?>]</p>
</div>
</section>
<!-- box end -->
<!-- detailContents start -->
<div class="detailContents">
			<table class="normal">
<?php if(preg_match("/renew_2015/", $_SERVER["REQUEST_URI"])):?>
				<tr>
					<th class="sub">募集職種</th>
					<td>
						<?php if(!empty($data['jobcategory_name'])):?>
						<?php $tmpjv = explode(",",$data['jobcategory_name']);?>
						<?php foreach($tmpjv as $jk => $jv):?>
						<?php echo htmlspecialchars($jv, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpjv) > 1):?><br /><?php endif;?>
						<?php endforeach;?>
						<?php endif;?>
					</td>
				</tr>
<?php endif;?>

				<tr>
					<th class="sub">募集背景</th>
					<td><?php echo nl2br(htmlspecialchars($data['background'], ENT_QUOTES, 'UTF-8'));?></td>
				</tr>
				<tr>
					<th class="sub">仕事内容</th>
					<td><?php echo nl2br(htmlspecialchars($data['summary'], ENT_QUOTES, 'UTF-8'));?></td>
				</tr>
				<tr>
					<th class="sub">応募資格</th>
					<td><?php echo nl2br(htmlspecialchars($data['qualification'], ENT_QUOTES, 'UTF-8'));?>
					</td>
				</tr>
				<tr>
					<th class="sub">雇用形態</th>
					<td>
						<?php if(!empty($data['employ_name'])):?>
						<?php $tmpe = explode(",",$data['employ_name']);?>
						<?php foreach($tmpe as $ek => $ev):?>
						<?php echo htmlspecialchars($ev, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpe) > 1):?><br /><?php endif;?>
						<?php endforeach;?>
						<?php endif;?>
					</td>
				</tr>
				<tr>
					<th class="sub">想定年収</th>
					<td>
<?php if(!empty($data['minsalary']) && $data["minsalary"] >= 400 && !empty($data["maxsalary"])):?>
<?php echo htmlspecialchars($data['minsalary'], ENT_QUOTES, 'UTF-8');?><?php if(empty($data['maxsalary'])):?>万円<?php endif;?>
<?php elseif(empty($data["maxsalary"])):?>
<?php echo htmlspecialchars($data['minsalary'], ENT_QUOTES, 'UTF-8');?>万円
<?php endif;?>
<?php if(!empty($data['maxsalary'])):?>
～
<?php echo htmlspecialchars($data['maxsalary'], ENT_QUOTES, 'UTF-8');?>万円&nbsp;
<?php endif;?>
					</td>
				</tr>

<!--
				<tr>
					<th class="sub">年収について補足</th>
					<td>
						<?php //if(!empty($data['free_items'][28]['memo'])):?>【給与形態メモ】<?php //endif;?><?php //echo nl2br(htmlspecialchars($data['free_items'][28]['memo'], ENT_QUOTES, 'UTF-8'));?><?php //if(!empty($data['free_items'][28]['memo'])):?><br /><br /><?php //endif;?>
						<?php //if(!empty($data['income_month_from'])):?>【月額:下限】<?php //echo htmlspecialchars($data['income_month_from'], ENT_QUOTES, 'UTF-8');?><?php //endif;?><?php //if(!empty($data['income_month_to'])):?><br>【月額:上限】<?php //echo htmlspecialchars($data['income_month_to'], ENT_QUOTES, 'UTF-8');?><?php //endif;?>
<?php //if(!empty($data['income_month_from'])):?><br><br><?php //endif;?>
<?php //if(!empty($data['bonus_num'])):?>【賞与】<?php //echo htmlspecialchars($data['bonus_num'], ENT_QUOTES, 'UTF-8');?>回、<?php //endif;?><br><?php //if(!empty($data['bonus_num'])):?>【賞与昨年実績】<?php //echo htmlspecialchars($data['bonus'], ENT_QUOTES, 'UTF-8');?><?php //endif;?><?php //if(!empty($data['bonus_memo'])):?><br /><?php //echo nl2br(htmlspecialchars($data['bonus_memo'], ENT_QUOTES, 'UTF-8'));?><?php //endif;?><br></td>
				</tr>
-->

				<tr>
					<th class="sub">勤務地</th>
					<td>
<?php
$prefectures=array();
if(!empty($data["prefecture_name"])){
	$tmpp = explode(",",$data['prefecture_name']);
	$tmpid = explode(",",$data['prefecture_id']);
	foreach($tmpid as $pk => $pv){
		$prefectures[$pk]["id"] = $pv;
		$prefectures[$pk]["name"] = $tmpp[$pk];
	}
}

foreach ((array) $prefectures as $key => $value) {
    $sort[$key] = $value['id'];
}

array_multisort($sort, SORT_ASC, $prefectures);
?>
						<?php if(!empty($prefectures)):?>
						<?php foreach($prefectures as $pk => $pv):?>
						<?php echo htmlspecialchars($pv["name"], ENT_QUOTES, 'UTF-8');?><?php if(count($tmpp) > 1):?><br /><?php endif;?>
						<?php endforeach;?>
						<?php endif;?>
					</td>
				</tr>
				<tr>
					<th class="sub">勤務地(詳細)</th>
					<td>
	<?php echo nl2br(htmlspecialchars($data['area_detail'], ENT_QUOTES, 'UTF-8'));?>
					</td>
				</tr>

<!--
				<tr>
					<th class="sub">転勤の有無</th>
					<td>
						<?php //if(!empty($data['free_items'][23]['items'])):?>
						<?php //foreach($data['free_items'][23]['items'] as $ek => $ev):?>
						<?php //echo htmlspecialchars($tenkin[$ev], ENT_QUOTES, 'UTF-8');?>
						<?php //endforeach;?>
						<?php //endif;?>
					</td>
				</tr>
				<tr>
					<th class="sub">募集年齢</th>
					<td>
						<?php //if($data['age_limit_type'] == 0):?>
						<?php //elseif($data['age_limit_type'] ==1):?>
							<?php //if(!empty($data['age_from']) || !empty($data['age_to'])):?>
							<?php //echo htmlspecialchars($data['age_from'], ENT_QUOTES, 'UTF-8');?>歳～<?php //echo htmlspecialchars($data['age_to'], ENT_QUOTES, 'UTF-8');?>歳
							<?php //endif;?>
						<?php //else:?>
							<?php //if($data['age_limit_type'] == 9):?>年齢不問
							<?php //endif;?>
						<?php //endif;?>
					</td>
				</tr>
				<tr>
					<th class="sub">年齢制限の理由</th>
					<td>
						<?php //if(!empty($data['age_limit_reason'])):?>
							<?php //if($data['age_limit_reason'] == 1):?>長期勤続によるキャリア形成を図るため新規学卒者等を対象とする
							<?php //elseif($data['age_limit_reason'] == 2):?>技能・ノウハウ等の継承の観点から、年齢構成を維持・回復させるため特定年齢層を対象とする
							<?php //elseif($data['age_limit_reason'] == 6):?>芸術・芸能の分野における表現の真実性等の要請がある
							<?php //elseif($data['age_limit_reason'] == 9):?>行政の施策を踏まえて中高年齢者に限定した募集・採用する
							<?php //elseif($data['age_limit_reason'] == 10):?>労働基準法等の法令により、特定の年齢層の労働者の就業等が禁止・制限されている
							<?php //elseif($data['age_limit_reason'] == 11):?>定年年齢を上限として当該上限年齢未満の労働者を対象とする
							<?php //endif;?>
						<?php //endif;?>
					</td>
				</tr>
				<tr>
					<th class="sub">最終学歴</th>
					<td>
						<?php //if(!empty($data['free_items'][120]['items'])):?>
						<?php //foreach($data['free_items'][120]['items'] as $ek => $ev):?>
						<?php //echo htmlspecialchars($gakureki[$ev], ENT_QUOTES, 'UTF-8');?>
						<?php //endforeach;?>
						<?php //endif;?><br />
						<?php //echo nl2br(htmlspecialchars($data['free_items'][120]['memo'], ENT_QUOTES, 'UTF-8'));?>
					</td>
				</tr>
				<tr>
					<th class="sub">各種保険</th>
					<td>
						<?php //if(!empty($data['free_items'][33]['items'])):?>【保険】<br />
						<?php //foreach($data['free_items'][33]['items'] as $ek => $ev):?>
						<?php //echo htmlspecialchars($insurence[$ev], ENT_QUOTES, 'UTF-8');?><br />
						<?php //endforeach;?>
						<?php //endif;?><?php //echo htmlspecialchars($data['free_items'][33]['memo'], ENT_QUOTES, 'UTF-8');?>
					</td>
				</tr>
-->
				<tr>
					<th class="sub">諸手当</th>
					<td><?php echo nl2br(htmlspecialchars($data['benefits'], ENT_QUOTES, 'UTF-8'));?></td>
				</tr>
				<tr>
					<th class="sub">休日休暇</th>
					<td><?php echo nl2br(htmlspecialchars($data['holiday'], ENT_QUOTES, 'UTF-8'));?></td>
				</tr>
				<tr>
					<th class="sub">勤務時間</th>
					<td><?php echo nl2br(htmlspecialchars($data['worktime'], ENT_QUOTES, 'UTF-8'));?></td>
				</tr>
<!--
				<tr>
					<th class="sub">就業日</th>
					<td>
 						<?php //if(!empty($data['free_items'][134]['items'])):?>
						<?php //foreach($data['free_items'][134]['items'] as $ek => $ev):?>
						<?php //echo htmlspecialchars($working_days[$ev], ENT_QUOTES, 'UTF-8');?>&nbsp;&nbsp;
						<?php //endforeach;?>
						<?php //endif;?><br /><?php //echo nl2br(htmlspecialchars($data['free_items'][134]['memo'], ENT_QUOTES, 'UTF-8'));?>
					</td>
				</tr>
-->

<tr>
				<th colspan="2" class="title">会社概要</th>
</tr>
<!--
				<tr>
					<th class="sub">会社名</th>
					<td><?php //echo htmlspecialchars($data['recruit_company']['name'], ENT_QUOTES, 'UTF-8');?></td>
				</tr>
-->
				<tr>
					<th class="sub">業種</th>
					<td>
						<?php if(!empty($data['i_name'])):?>
						<?php $tmpp = explode(",",$data['i_name']);?>
						<?php foreach($tmpp as $pk => $pv):?>
						<?php echo htmlspecialchars($pv, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpp) > 1):?><br /><?php endif;?>
						<?php endforeach;?>
						<?php endif;?>
					</td>
				</tr>
				<tr>
					<th class="sub">事業内容</th>
					<td><?php echo nl2br(htmlspecialchars($data['c_naiyo'], ENT_QUOTES, 'UTF-8'));?></td>
				</tr>
<!--
				<tr>
					<th class="sub">資本金</th>
					<td><?php //echo htmlspecialchars($data['recruit_company']['capital'], ENT_QUOTES, 'UTF-8');?>円</td>
				</tr>
-->
				<tr>
					<th class="sub">従業員数</th>
					<td><?php echo nl2br(htmlspecialchars($data['c_employment'], ENT_QUOTES, 'UTF-8'));?></td>
				</tr>
<!--
				<tr>
					<th class="sub">事業内容</th>
					<td><?php //echo nl2br(htmlspecialchars($data['recruit_company']['business_summary'], ENT_QUOTES, 'UTF-8'));?></td>
				</tr>
				<tr>
					<th class="sub">会社の特徴</th>
					<td><?php //echo nl2br(htmlspecialchars($data['recruit_company']['characteristic'], ENT_QUOTES, 'UTF-8'));?></td>
				</tr>
-->
			</table>

<p class="txtlink"><a href="../jump/index.php?id=<?php echo htmlspecialchars($data['job_id'], ENT_QUOTES, 'UTF-8');?>&detail=text" class="detail_cv_sp_middle" target="_blank">求人の詳細を聞く ⇒</a></p>
    
<h2 class="title ss">この求人を見た方は、このような求人も見ています</h2>
<!-- likeBox start -->
<div id="likeBox">
<div id="likeBoxSub">
<div id="likeBoxAll" class="clearfix">
<?php $cnt=0;?>
<?php if(!empty($related)):?>
<?php foreach($related as $k=>$v):?>
<?php if($cnt>3) break;?>
<?php if($v['job_id']!=$_GET['id']):?>
<!-- start -->
<div class="box">
<div class="title">
<h2><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></h2>
</div>
<div class="txt">
<p class="income">年収：<strong>
<?php if(!empty($v['minsalary']) && $v["minsalary"] >= 400 && !empty($v["maxsalary"])):?>
<?php echo htmlspecialchars($v['minsalary'], ENT_QUOTES, 'UTF-8');?>万円
<?php elseif(empty($v["maxsalary"])):?>
<?php echo htmlspecialchars($v['minsalary'], ENT_QUOTES, 'UTF-8');?>万円
<?php endif;?>
<?php if(!empty($v['maxsalary'])):?>
~
<?php echo htmlspecialchars($v['maxsalary'], ENT_QUOTES, 'UTF-8');?>万円
<?php endif;?>
</strong></p>
    <p class="btn"><a href="https://www.hurex.jp/mypage/signup/regist?p=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>">求人の詳細をきく</a></p>
</div>
</div>
<!-- end -->
<?php endif;?>
<?php $cnt++;?>
<?php endforeach;?>
<?php endif;?>
</div>
</div>
</div>
<!-- likeBox end -->
<p class="mb20"><a href="../jump/index.php?p=form&detail=button03_pj" target="_blank"><img src="../images/bnr_private.png" alt="他の求人をもっと見たい方" width="100%" /></a></p>
<p class="mb20"><a href="https://www.hurex.jp/lp/excelletos/" target="_blank"><img src="../images/bnr_excelletos.png" alt="一流の転職をするなら「EXCELLETOS」" width="100%" /></a></p>
<?php endif;?>
