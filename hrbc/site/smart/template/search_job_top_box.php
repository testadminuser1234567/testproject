<form method ="post" name="searchpage" action="./job-search/index.html#searchResult">
<table>
<tr>
<th>業種</th>
<td>
							<select name="biz1" style="width:190px;">
								<option value="">--------</option>
									<optgroup label="">
								<?php foreach($biz as $k => $v):?>
								<option value="<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['biz1'] == $k):?>selected<?php endif;?>><?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?>全般</option>
									</optgroup>
								<?php endforeach;?>
							</select>
</td>
<tr>
<th>職種</th>
<td>
							<select name="job1"  style="width:190px;">
									<option value="">--------</option>
									<optgroup label="[営業系]">
										<?php foreach($select_list[0] as $k => $v):?>
										<option value="<?php echo htmlspecialchars($v['id'], ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['job1'] == $v['id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v['label'], ENT_QUOTES, 'UTF-8');?></option>
										<?php endforeach;?>
										<option value="e999" <?php if($_SESSION['job1'] == "e999"):?>selected<?php endif;?>>営業系全般</option>										
									</optgroup>
									<optgroup label="[経営幹部/企画/マーケティング系]">
										<?php foreach($select_list[1] as $k => $v):?>
										<option value="<?php echo htmlspecialchars($v['id'], ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['job1'] == $v['id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v['label'], ENT_QUOTES, 'UTF-8');?></option>
										<?php endforeach;?>
										<option value="m999" <?php if($_SESSION['job1'] == "m999"):?>selected<?php endif;?>>経営幹部/企画/マーケティング系全般</option>										
									</optgroup>
									<optgroup label="[管理部門/事務系]">
										<?php foreach($select_list[2] as $k => $v):?>
										<option value="<?php echo htmlspecialchars($v['id'], ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['job1'] == $v['id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v['label'], ENT_QUOTES, 'UTF-8');?></option>
										<?php endforeach;?>
										<option value="k999" <?php if($_SESSION['job1'] == "k999"):?>selected<?php endif;?>>管理部門/事務系全般</option>										
									</optgroup>
									<optgroup label="[IT系(SE/インフラエンジニア)]">
										<?php foreach($select_list[3] as $k => $v):?>
										<option value="<?php echo htmlspecialchars($v['id'], ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['job1'] == $v['id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v['label'], ENT_QUOTES, 'UTF-8');?></option>
										<?php endforeach;?>
										<option value="i999" <?php if($_SESSION['job1'] == "i999"):?>selected<?php endif;?>>IT系(SE/インフラエンジニア)全般</option>										
									</optgroup>
									<optgroup label="[技術系（電気/電子/機械）　]">
										<?php foreach($select_list[4] as $k => $v):?>
										<option value="<?php echo htmlspecialchars($v['id'], ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['job1'] == $v['id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v['label'], ENT_QUOTES, 'UTF-8');?></option>
										<?php endforeach;?>
										<option value="d999" <?php if($_SESSION['job1'] == "d999"):?>selected<?php endif;?>>技術系（電気/電子/機械）全般</option>										
									</optgroup>
									<optgroup label="[技術系（メディカル/化学/食品）]">
										<?php foreach($select_list[5] as $k => $v):?>
										<option value="<?php echo htmlspecialchars($v['id'], ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['job1'] == $v['id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v['label'], ENT_QUOTES, 'UTF-8');?></option>
										<?php endforeach;?>
										<option value="c999" <?php if($_SESSION['job1'] == "c999"):?>selected<?php endif;?>>技術系(メディカル/化学/食品)全般</option>										
									</optgroup>
									<optgroup label="[技術系（建築/土木）　]">
										<?php foreach($select_list[6] as $k => $v):?>
										<option value="<?php echo htmlspecialchars($v['id'], ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['job1'] == $v['id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v['label'], ENT_QUOTES, 'UTF-8');?></option>
										<?php endforeach;?>
										<option value="t999" <?php if($_SESSION['job1'] == "t999"):?>selected<?php endif;?>>技術系(建築/土木)全般</option>										
									</optgroup>
									<optgroup label="[専門職系（コンサルタント/金融/不動産）　]">
										<?php foreach($select_list[7] as $k => $v):?>
										<option value="<?php echo htmlspecialchars($v['id'], ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['job1'] == $v['id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v['label'], ENT_QUOTES, 'UTF-8');?></option>
										<?php endforeach;?>
										<option value="s999" <?php if($_SESSION['job1'] == "s999"):?>selected<?php endif;?>>専門職系（コンサルタント/金融/不動産）全般</option>										
									</optgroup>
									<optgroup label="[Web/クリエイティブ系]">
										<?php foreach($select_list[8] as $k => $v):?>
										<option value="<?php echo htmlspecialchars($v['id'], ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['job1'] == $v['id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v['label'], ENT_QUOTES, 'UTF-8');?></option>
										<?php endforeach;?>
										<option value="w999" <?php if($_SESSION['job1'] == "w999"):?>selected<?php endif;?>>Web/クリエイティブ系全般</option>										
									</optgroup>
									<optgroup label="[サービス系（人材/小売/フードetc)]">
										<?php foreach($select_list[9] as $k => $v):?>
										<option value="<?php echo htmlspecialchars($v['id'], ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['job1'] == $v['id']):?>selected<?php endif;?>><?php echo htmlspecialchars($v['label'], ENT_QUOTES, 'UTF-8');?></option>
										<?php endforeach;?>
										<option value="j999" <?php if($_SESSION['job1'] == "j999"):?>selected<?php endif;?>>サービス系(人材/小売/フードetc)全般</option>										
									</optgroup>
							</select>
</td>
</tr>
<tr>
<th>勤務地</th>
<td>
<select name="pref" style="width:190px;">
<option value="">--------</option>
<?php foreach($prefecture as $k => $v):?>
<option value="<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['pref'] == $k):?>selected<?php endif;?>><?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?></option>
<?php endforeach;?>
</select>
</td>
</tr>
<tr>
<th>年収</th>
<td>
							<select name="year_income" style="width:190px;">
								<option value=""  selected>--------</option>
								<option value="100" <?php if($_SESSION['year_income'] == 100):?>selected<?php endif;?>>100万円以上</option>
								<option value="200" <?php if($_SESSION['year_income'] == 200):?>selected<?php endif;?>>200万円以上</option>
								<option value="300" <?php if($_SESSION['year_income'] == 300):?>selected<?php endif;?>>300万円以上</option>
								<option value="400" <?php if($_SESSION['year_income'] == 400):?>selected<?php endif;?>>400万円以上</option>
								<option value="500" <?php if($_SESSION['year_income'] == 500):?>selected<?php endif;?>>500万円以上</option>
								<option value="600" <?php if($_SESSION['year_income'] == 600):?>selected<?php endif;?>>600万円以上</option>
								<option value="700" <?php if($_SESSION['year_income'] == 700):?>selected<?php endif;?>>700万円以上</option>
								<option value="800" <?php if($_SESSION['year_income'] == 800):?>selected<?php endif;?>>800万円以上</option>
								<option value="900" <?php if($_SESSION['year_income'] == 900):?>selected<?php endif;?>>900万円以上</option>
								<option value="1000" <?php if($_SESSION['year_income'] == 1000):?>selected<?php endif;?>>1000万円以上</option>
							</select>
</td>
</tr>
<tr>
<th colspan="2">キーワード<br>
						<input type="text" name="keyword" value="<?php echo htmlspecialchars($_SESSION['keyword'], ENT_QUOTES, 'UTF-8');?>" style="width:90%;" />
							<br />（スペース区切り
							<label style="margin-left:10px"><input type="radio" name="keyword_flg" value="or" <?php if($_SESSION['keyword_flg']=="or" || empty($_SESSION['keyword_flg'])):?>checked<?php endif;?>>or</label>
							<label><input type="radio" name="keyword_flg" value="and" <?php if($_SESSION['keyword_flg']=="and"):?>checked<?php endif;?>>and</label>）
</th>
</tr>
</table>
<p class="aCenter"><input type="image" src="images/btn_search.png" width="205" height="46"></p>
</form>