<ul class="clearfix">
<?php if(!empty($miyagi) || !empty($akita)):?>

<?php if(!empty($miyagi)):?>
<?php foreach($miyagi as $k => $v):?>
<li class="wide"><a href="./job-search/detail.html?id=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>"><span class="date"><?php echo str_replace('-','.',substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));?></span><br><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></a></li>
<?php endforeach;?>
<?php endif;?>

<?php if(!empty($other)):?>
<?php foreach($other as $k => $v):?>
<li class="wide"><a href="./job-search/detail.html?id=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>"><span class="date"><?php echo str_replace('-','.',substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));?></span><br><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></a></li>
<?php endforeach;?>
<?php endif;?>

<?php else:?>
<li>
現在新着求人情報はございません。
</li>
<?php endif;?>
</ul>
