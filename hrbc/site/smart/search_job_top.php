<?php
session_start();
/*
ini_set('error_reporting', E_ALL);
ini_set('display_errors', '1');
*/
/*
/  データ取得
*/
include( $_SERVER["DOCUMENT_ROOT"] . "/../hrbc/site/config.php");

//パラメータ格納用
$param = array();
$param['phase'] = 6;
$param['publish'] = 11103;
$param['recommend']= 10591;

///////////////////////////////////////////////
// 都道府県から検索
///////////////////////////////////////////////
$tmppref=array();
foreach($prefecture as $k=>$v){
	$tmppref[$v] = $k;
}
$prefAry = array();
$otherSearch="";
$otherParam="";
$otherVal = array();

foreach($tmppref as $k=>$v){
	if(preg_match('/秋田/',$k)){
		$akita = $v;
	}else if(preg_match('/青森/',$k)){
		$aomori = $v;
	}else if(preg_match('/岩手/',$k)){
		$iwate = $v;
	}else if(preg_match('/宮城/',$k)){
		$miyagi = $v;
	}else if(preg_match('/山形/',$k)){
		$yamagata = $v;
	}else if(preg_match('/福島/',$k)){
		$fukushima = $v;
	}else if(!preg_match('/秋田/',$k) && !preg_match('/青森/',$k) && !preg_match('/秋田/',$k) && !preg_match('/岩手/',$k) && !preg_match('/宮城/',$k) && !preg_match('/山形/',$k) && !preg_match('/福島/',$k)){
		if(empty($otherSearch)) {
			$otherSearch.=" and ( ";
		}else{
			$otherSearch.=" or ";
		}
		$otherVal[] = $v;
		$otherParam = ':other'.$v;
		$otherSearch .= " FIND_IN_SET(" . $otherParam . ",j.prefecture_id)";
	}
}
if($otherSearch) $otherSearch .= " ) ";

////////////////////////////////////////////////////////////////////
// 宮城抽出 3件
////////////////////////////////////////////////////////////////////
$param["miyagi"] = $miyagi;
$sql_list = "SELECT j.job_id, j.job_title, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish and FIND_IN_SET(:miyagi, j.prefecture_id) and j.recommend=:recommend group by j.job_id order by j.j_updated desc limit 0, 3";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$miyagi = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$miyagi[] = $result;
}
unset($param["miyagi"]);

////////////////////////////////////////////////////////////////////
// 岩手・山形・福島2件
////////////////////////////////////////////////////////////////////
$param["other"] = $iwate . "," . $yamagata . "," . $fukushima;
$sql_list = "SELECT j.job_id, j.job_title, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish and FIND_IN_SET(j.prefecture_id, :other) and j.recommend=:recommend group by j.job_id order by j.j_updated desc limit 0, 2";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$other = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$other[] = $result;
}
unset($param["other"]);

