<?php
////////////////////////////////////////////////////////////////////
// db抽出
////////////////////////////////////////////////////////////////////

$sql = "SELECT count(distinct j.job_id) as cnt FROM jobs as j" . $bizSql . " where j.phaze = :phase and j.publish =:publish " . $bizSearch . $jobSearch . $prefSearch . $employSearch . $incomeSearch . $keySearch;
$stmt = $dbh->prepare($sql);
$stmt->execute($param);
$count = $stmt->fetch(PDO::FETCH_NUM);
$total_cnt = $count[0];

//ページネーション
$config["page"] = $page;
$config["base_url"] = "index.html";
$config["cur_page"] = $page;
$config["per_page"] = PAGE_CNT;
$config["total_rows"] = $total_cnt;
$config["first_link"] = "&laquo";
$config["last_link"] = "&raquo";
$config['num_links'] = 5;

$paginate->initialize($config);
//ページネーションのリンク取得
$pagination = $paginate->create_links();
$pagination = hp($pagination);

//データ用オフセット取得
$offset = $paginate->get_offset();
$limit=PAGE_CNT;

//データ取得
$sql_list = "SELECT j.job_id, j.job_title, j.client_id, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.j_updated, j.prefecture_name, j.prefecture_id, j.jobcategory_name,j.employ_name" . $bizSql2 . " FROM jobs as j" . $bizSql . " where j.phaze = :phase and j.publish =:publish " . $bizSearch . $jobSearch . $prefSearch . $employSearch . $incomeSearch . $keySearch . " group by j.job_id order by j.j_updated desc limit $offset, $limit";
//$sql_list = "SELECT j.job_id, j.job_title, j.client_id, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, group_concat(distinct jc.jc_name separator '|') as jc_name, group_concat(distinct i.i_name separator '|') as i_name, group_concat(distinct p.p_name order by p_id asc separator '|') as p_name, group_concat(distinct e.e_name order by e_id asc separator '|') as e_name, j.j_updated FROM jobs as j left join industries as i on j.client_id = i.client_id left join prefectures as p on j.job_id = p.job_id left join jobcategories as jc on .j.job_id = jc.job_id left join employments as e on j.job_id = e.job_id where j.phaze = :phase and j.publish =:publish " . $bizSearch . $jobSearch . $prefSearch . $incomeSearch . $keySearch . " group by j.job_id order by j.j_updated desc limit $offset, $limit";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$datas = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$datas[] = $result;
}

$offset = $offset + 1;

$hani = $offset - 1 + PAGE_CNT;
if($hani >= $total_cnt){
	$hani = $total_cnt;
}


