<?php

//接続認証
/*
	$hash_key=base64_decode($_GET['hk']);
	$method = "AES-128-CBC";
	$pass = "Wq08wrkUi";
	$options = false;
	$iv = "2017080200000000";
	//情報を複合化
	$dec = openssl_decrypt( $hash_key,
	                        $method,
	                        $pass,
	                        $options,
	                        $iv );
	if($dec == FALSE){
	    $error['err_code'] = "E020002";
	    $error['message']  = "Error Hash Key Can not decode";
	    echo json_encode($error);
	    exit;
	}else
	if(!preg_match("/^[0-9]+$/",$dec)){
	    $error['err_code'] = "E020002";
	    $error['message']  = "Error Hash Key not match";
	    echo json_encode($error);
	    exit;
	}else
	if($dec <= (time() - 900) || $dec >= (time() + 900)){
	    $error['err_code'] = "E020003";
	    $error['message']  = "Error Hash Time Out.";
	    echo json_encode($error);
	    exit;
	}
*/

if($_GET['hk'] != "022a4f1e2cf879fc224a19c1bfad0dc5"){
    $error['err_code'] = "E020002";
    $error['message']  = "Error Hash Key not match";
    echo json_encode($error);
    exit;
}

//hrbcシステムディレクトリ
$dir = $_SERVER["DOCUMENT_ROOT"] . "/../../hrbc/";

//一覧表示件数
if(!empty($_GET['rows']) && is_numeric($_GET['rows'])){
	define('PAGE_CNT', $_GET['rows']);
}else{
	define('PAGE_CNT', 20);
}


//security
session_start();
define('ENVIRONMENT', 'production');
define('BASEPATH', $_SERVER["DOCUMENT_ROOT"] .'/syskanri/system/');
define('APPPATH', $_SERVER["DOCUMENT_ROOT"] .'/syskanri/application/');
require(APPPATH . 'config/config.php');
require(APPPATH . 'config/database.php');
require(BASEPATH . 'core/Common.php');
require(BASEPATH . 'database/DB.php');

require_once($_SERVER["DOCUMENT_ROOT"] .  '/syskanri/application/config/config.php');

//PDO
$dsn = 'mysql:dbname=' . dbDatabase .';host=' . dbHostname . ';port=' . dbPort;
$user = dbUsername;
$password = dbPassword;
		
try{
	$dbh = new PDO($dsn, $user, $password,
		array(
		     	PDO::MYSQL_ATTR_INIT_COMMAND => "SET CHARACTER SET `utf8`"
		));

}catch (PDOException $e){
	print('Error:'.$e->getMessage());
	die();
}

function hp($dirty_html) {
	require_once($_SERVER["DOCUMENT_ROOT"] .  '/syskanri/htmlpurifier/library/HTMLPurifier.auto.php');
	$config = HTMLPurifier_Config::createDefault();
	$config->set('Core.Encoding', 'UTF-8');
	$config->set('HTML.Doctype', 'XHTML 1.0 Transitional');
	$config->set('Attr.AllowedFrameTargets', array('_blank','_self'));
	$config->set('HTML.Trusted', true);
	$purifier = new HTMLPurifier($config);
	$clean_html = $purifier->purify( $dirty_html );
	return $clean_html;
}