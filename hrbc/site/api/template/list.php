<?php
/*
/ 求人情報一情報出力
*/
?>
	<div class="boxwrap">
		<!-- Paging -->
				<?php if($total_cnt == 0):?>
				<?php else:?>
				<p class="searchResult">
				<span class="point"><?php echo htmlspecialchars($total_cnt, ENT_QUOTES, 'UTF-8');?></span>件中 <?php echo $offset;?>〜<?php echo $hani;?>件の検索結果を表示しています。
				</p>
				<?php endif;?>
				
				<div class="PagingContainer clearfix">
					<div class="pageNavi centered clearfix">
						<?php echo $pagination;?>
					</div>
				</div>
			<!-- /Paging -->

			<?php if($total_cnt > 0):?>
			<?php foreach($datas as $k => $v):?>
		    	<table width="650"cellspacing="1" class="normal">
					<col width="100">
					<col width="630">
		    		<tr>
		    			<th colspan="2"><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></th>
		    		</tr>
		    		<tr>
		    			<th align="rigtht">募集職種</th>
		    			<td>
						<?php if(!empty($v['jobcategory_name'])):?>
						<?php $tmpjv = explode(",",$v['jobcategory_name']);?>
						<?php foreach($tmpjv as $jk => $jv):?>
						<?php echo htmlspecialchars($jv, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpjv) > 1):?><br /><?php endif;?>
						<?php endforeach;?>
						<?php endif;?>
                                        </td>
		    		</tr>
		    		<tr>
		    			<th align="rigtht">業種</th>
		    			<td>
						<?php if(!empty($v['i_name'])):?>
						<?php $tmpi = explode(",",$v['i_name']);?>
						<?php foreach($tmpi as $ik => $iv):?>
						<?php echo htmlspecialchars($iv, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpi) > 1):?><br /><?php endif;?>
						<?php endforeach;?>
						<?php endif;?>
                                        </td>
		    		</tr>
		    		<tr>
		    			<th align="rigtht">仕事内容</th>
		    			<td><?php echo nl2br(htmlspecialchars($v['summary'], ENT_QUOTES, 'UTF-8'));?></td>
		    		</tr>
		    		<tr>
		    			<th align="rigtht">勤務地</th>
		    			<td>
						<?php if(!empty($v['prefecture_name'])):?>
						<?php $tmpp = explode(",",$v['prefecture_name']);?>
						<?php foreach($tmpp as $pk => $pv):?>
						<?php echo htmlspecialchars($pv, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpp) > 1):?> /<?php endif;?>
						<?php endforeach;?>
						<?php endif;?>
					</td>
		    		</tr>
					<tr>
					<th>雇用形態</th>
					<td>
						<?php if(!empty($v['employ_name'])):?>
						<?php $tmpe = explode(",",$v['employ_name']);?>
						<?php foreach($tmpe as $ek => $ev):?>
						<?php echo htmlspecialchars($ev, ENT_QUOTES, 'UTF-8');?><?php if(count($tmpe) > 1):?><br /><?php endif;?>
						<?php endforeach;?>
						<?php endif;?>
					</td>
					</tr>
		    		<tr>
		    			<th>想定年収</th>
		    			<td><?php if(!empty($v['minsalary'])):?><?php echo htmlspecialchars($v['minsalary'], ENT_QUOTES, 'UTF-8');?><?php if(empty($v['maxsalary'])):?>万円<?php else:?>〜<?php endif;?><?php endif;?><?php if(!empty($v['maxsalary'])):?><?php echo htmlspecialchars($v['maxsalary'], ENT_QUOTES, 'UTF-8');?>万円&nbsp;<?php endif;?></td>
		    		</tr>
		    		<tr>
		    			<th colspan="2" class="chsBtn">
		    				<a href="./detail.html?id=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>">
			    				<span class="btn"> <img src="../images/btn_more_off.gif" alt="この求人の詳細を見る"/></span>
			    			</a>
		    			</th>
		    		</tr>
		    	</table>
			<?php endforeach;?>
			<?php else:?>
			<p>お探しの条件での求人情報はございません。</p>
			<?php endif;?>
		    
				<!-- Paging -->
				<div class="PagingContainer clearfix">
					<div class="pageNavi centered clearfix">
						<?php echo $pagination;?>
					</div>
				</div>
	</div>
