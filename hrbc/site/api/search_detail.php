<?php
/*
/  詳細ページのデータ取得用
*/
include("config.php");

$id = $_GET['id'];
$id = htmlspecialchars($id, ENT_QUOTES, 'UTF-8');
if(!is_numeric($id)){
	$id = 0;
}

//パラメータ格納用
$param = array();
$param['phase'] = 6;
$param['publish'] = 11103;
$param['jobid'] = $id;


//データ取得
$sql_list = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish and j.job_id = :jobid group by j.job_id";
//$sql_list = "SELECT j.job_id, j.job_title, j.client_id, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, group_concat(distinct i.i_id separator '|') as i_id, group_concat(distinct i.i_name separator '|') as i_name, group_concat(distinct p.p_name order by p_id asc separator '|') as p_name, group_concat(distinct p.p_id order by p_id asc separator '|') as p_id, group_concat(distinct jc.jc_id separator '|') as jc_id, group_concat(distinct e.e_name order by e_id asc separator '|') as e_name, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join industries as i on j.client_id = i.client_id left join prefectures as p on j.job_id = p.job_id left join jobcategories as jc on .j.job_id = jc.job_id left join employments as e on j.job_id = e.job_id left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish and j.job_id = :jobid group by j.job_id";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$datas = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$datas[] = $result;
}
$data = $datas[0];

if(empty($data)){
	$redirectUrl = "http://www.hurex.jp/job-search/404.html";
	header("HTTP/1.0 404 Not Found");
	print_r(file_get_contents($redirectUrl));
	exit;

}


////////////////////////////////////////////
// 類似データ取得
////////////////////////////////////////////

$param_related['phase'] = $param['phase'];
$param_related['publish'] = $param['publish'];

//業種
$bizSearch="";
$bizSql = " left join clients as c on j.client_id = c.c_id ";
$bizSql2 = ", group_concat(distinct c.c_industry_name separator '|') as i_name";
if(!empty($data['c_industry_id'])){
	$param_related["c_industry_id"] = $data['c_industry_id'];
	$bizSearch = " FIND_IN_SET(c.c_industry_id, :c_industry_id)";
}

//職種
$jobSearch="";
if(!empty($data['jobcategory_id'])){
	$param_related["jobcategory"] = $data['jobcategory_id'];
	$jobSearch = " and FIND_IN_SET(j.jobcategory_id ,:jobcategory ) ";
}



//都道府県
$prefSearch="";
if(!empty($data['prefecture_id'])){
	$param_related["prefecture_id"] = $data['prefecture_id'];
	$prefSearch = " and FIND_IN_SET(j.prefecture_id ,:prefecture_id ) ";
}

$sql_list = "SELECT j.job_id, j.job_title, j.client_id, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.j_updated, j.prefecture_name, j.jobcategory_name,j.employ_name" . $bizSql2 . " FROM jobs as j" . $bizSql . " where (j.phaze = :phase and j.publish =:publish) and ( " . $bizSearch . $jobSearch . $prefSearch . " ) group by j.job_id order by j.job_id desc limit 0, 11";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param_related);
$related = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$related[] = $result;
}


//description
$description = "";
if(!empty($data['employ_name'])){
	$description .= "雇用形態:";
	$tmpe = explode(",",$data['employ_name']);
	foreach($tmpe as $ek => $ev){
		$description .= str_replace(array("\r\n", "\r", "\n"), '', htmlspecialchars($ev, ENT_QUOTES, 'UTF-8'));
		if(count($tmpe) > 1){
			$description.=" ";
		}
	}
	$description .= "、";
}

if(!empty($data['minsalary']) || !empty($data['maxsalary'])){
	$description .= "想定年収:";
}
if(!empty($data['minsalary'])){
	$description .= str_replace(array("\r\n", "\r", "\n"), '', htmlspecialchars($data['minsalary'], ENT_QUOTES, 'UTF-8'));
	if(empty($data['maxsalary'])){
		$description.="万円";
	}else{
		$description.="〜";
	}
}
if(!empty($data['maxsalary'])){
	$description.= str_replace(array("\r\n", "\r", "\n"), '', htmlspecialchars($data['maxsalary'], ENT_QUOTES, 'UTF-8')) . "万円";
}
if(!empty($data['minsalary']) || !empty($data['maxsalary'])){
	$description .= "、";
}

if(!empty($data['prefecture_name'])){
	$description .= "勤務地:";
	$tmpp = explode(",",$data['prefecture_name']);
	foreach($tmpp as $pk => $pv){
		$description .= str_replace(array("\r\n", "\r", "\n"), '', htmlspecialchars($pv, ENT_QUOTES, 'UTF-8'));
	}
}

if(!empty($data['area_detail'])){
	$description .= "(" . str_replace(array("\r\n", "\r", "\n"), '', htmlspecialchars($data['area_detail'], ENT_QUOTES, 'UTF-8')) . ")";
}
if(!empty($data['prefecture_name']) || !empty($data['area_detail'])){
	$description .= "、";
}

if(!empty($data['background'])){
	$description .= "求人の背景:";
	$description .= str_replace(array("\r\n", "\r", "\n"), '', htmlspecialchars($data['background'], ENT_QUOTES, 'UTF-8'));
}
