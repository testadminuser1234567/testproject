<?php

//session スタート済みでnoticeが出るのでコメントアウト
//session_start();


/*
/  データ取得
*/
require_once("config.php");
require_once ('Pagination.php');

$limit = PAGE_CNT;

//パラメータ格納用
$param = array();
$param['phase'] = 6;
$param['publish'] = 11103;

$paginate = new Pagination();
//page_start_num
if(!empty( $_GET['page'] )){
	$page = $_GET['page'];
}else{
	$page = 1;
}
$page = htmlspecialchars($page, ENT_QUOTES, 'UTF-8');
if(!is_numeric($page)){
	$page = 1;
}
$page_start_num = ($page - 1) * PAGE_CNT;

//postデータ マスターデータと名前被らないように注意！！


if(!empty($_POST)){
	foreach($_POST as $key=>$val){
		$send_param[$key]=$val;
	}
}

if(!empty($_GET)){
	foreach($_GET as $key=>$val){
		$send_param[$key]=$val;
	}
}


if(!empty($send_param)){
	sessionClear();
	$biz1 = $send_param['biz1'];
	$_SESSION['biz1'] = $biz1;
	$biz2 = $send_param['biz2'];
	$_SESSION['biz2'] = $biz2;
	$biz3 = $send_param['biz3'];
	$_SESSION['biz3'] = $biz3;
/*
	$job1 = $send_param['job1'];
	$_SESSION['job1'] = $job1;
	$job2 = $send_param['job2'];
	$_SESSION['job2'] = $job2;
	$job3 = $send_param['job3'];
	$_SESSION['job3'] = $job3;
*/
	$job1="";
	$jobs = array();
	if(!empty($send_param["job"])){
		foreach($send_param["job"] as $k=>$v){
			$job1 .= $v .",";
			$_SESSION['jobs'][] = $v;
			$jobs[] = $v;
		}
	}
	$_SESSION['job1'] = $job1;

	if(empty($send_param["pref"]) && !empty($send_param["pref2"])){
		$pref = $send_param['pref2'];
		$pref2=$pref;
		$_SESSION['pref'] = $pref;
	}else if(!empty($send_param["pref"]) && empty($send_param["pref2"])){
		$pref = $send_param['pref'];
		$_SESSION['pref'] = $pref;
	}else if(empty($send_param["pref_kaso"]) && !empty($send_param["pref2_kaso"])){
		$pref = $send_param['pref2_kaso'];
		$pref2=$pref;
		$_SESSION['pref'] = $pref;
	}else if(!empty($send_param["pref_kaso"]) && empty($send_param["pref2_kaso"])){
		$pref = $send_param['pref_kaso'];
		$_SESSION['pref'] = $pref;
	}

/*
	$pref = $send_param['pref'];
	$_SESSION['pref'] = $pref;
*/
	$employees = $send_param['employee'];
	$_SESSION['employees'] = $employees;
	$keyword = $send_param['keyword'];
	$_SESSION['keyword'] = $keyword;
	$keyword_flg = $send_param['keyword_flg'];
	$_SESSION['keyword_flg'] = $keyword_flg;
	$year_income = $send_param['year_income'];
	$_SESSION['year_income'] = $year_income;
//ページネーションとか
}else{
	if(!empty($_SERVER['HTTP_REFERER'])){
		$ref = $_SERVER['HTTP_REFERER'];
	}else{
		$ref = "";
	}
	if(empty($uri)){
		$uri = "";
	}
	$now_url = "http://" .$_SERVER['SERVER_NAME'] . $uri;

	if($ref != $now_url && empty($_GET['page'])){
		sessionClear();
	}
	$jobs = array();
	$biz1 = "";
	$biz2 = "";
	$biz3 = "";
	$job1 = "";
	$jobs = "";
		
	if(array_key_exists('biz1',$_SESSION)) $biz1 = $_SESSION['biz1'];
	if(array_key_exists('biz2',$_SESSION)) $biz2 = $_SESSION['biz2'];
	if(array_key_exists('biz3',$_SESSION)) $biz3 = $_SESSION['biz3'];
	if(array_key_exists('job1',$_SESSION)) $job1 = $_SESSION['job1'];
	if(array_key_exists('jobs',$_SESSION)) $jobs = $_SESSION['jobs'];
/*
	$job1 = $_SESSION['job1'];
	$job2 = $_SESSION['job2'];
	$job3 = $_SESSION['job3'];
*/
	$pref        = "";
	$employees   = "";
	$keyword     = "";
	$keyword_flg = "";
	$year_income = "";

	if(array_key_exists('pref',$_SESSION))		 $pref        = $_SESSION['pref'];
	if(array_key_exists('employees',$_SESSION))	 $employees   = $_SESSION['employees'];
	if(array_key_exists('keyword',$_SESSION))	 $keyword     = $_SESSION['keyword'];
	if(array_key_exists('keyword_flg',$_SESSION))$keyword_flg = $_SESSION['keyword_flg'];
	if(array_key_exists('year_income',$_SESSION))$year_income = $_SESSION['year_income'];
}


//雇用形態を配列に格納（パラメータ渡し用）
$employ_ary = array();
if(!empty($employees)){
	foreach($employees as $k => $v){
		$employ_ary[$k] = $v;
	}
}

//キーワードフラグ or->0 and ->1
if($keyword_flg == "or"){
	$kflg = 0;
}else{
	$kflg = 1;
}

/////////////////////////////////////////////
// 職種から検索された場合
////////////////////////////////////////////
$tmpshokushu=array();
foreach($job as $k=>$v){
	if(preg_match('/全般/',$v))
	$tmpshokushu[$v] = $k;
}
$shokushu = "";

$uri = $_SERVER["REQUEST_URI"];

if(preg_match('/eigyou-search/', $uri)){
	foreach($tmpshokushu as $k=>$v){
		if(preg_match('/営業系/',$k)){
			$shokushu = $k;
			$job1 = $v;
		}
	}
}else if(preg_match('/it-search/', $uri)){
	foreach($tmpshokushu as $k=>$v){
		if(preg_match('/IT/',$k)){
			$shokushu = $k;
			$job1 = $v;
		}
	}
}else if(preg_match('/denki-search/', $uri)){
	foreach($tmpshokushu as $k=>$v){
		if(preg_match('/電気\/電子\/機械/',$k)){
			$shokushu = $k;
			$job1 = $v;
		}
	}
}else if(preg_match('/kenchiku-search/', $uri)){
	foreach($tmpshokushu as $k=>$v){
		if(preg_match('/建築\/土木/',$k)){
			$shokushu = $k;
			$job1 = $v;
		}
	}
}else if(preg_match('/medical-search/', $uri)){
	foreach($tmpshokushu as $k=>$v){
		if(preg_match('/メディカル\/化学\/食品/',$k)){
			$shokushu = $k;
			$job1 = $v;
		}
	}
}else if(preg_match('/jimu-search/', $uri)){
	foreach($tmpshokushu as $k=>$v){
		if(preg_match('/管理部門\/事務系/',$k)){
			$shokushu = $k;
			$job1 = $v;
		}
	}
}else if(preg_match('/service-search/', $uri)){
	foreach($tmpshokushu as $k=>$v){
		if(preg_match('/人材\/小売\/フード/',$k)){
			$shokushu = $k;
			$job1 = $v;
		}
	}
}
////////////////////////////////////////////////
// 職種ここまで
////////////////////////////////////////////////

///////////////////////////////////////////////
// 都道府県から検索
///////////////////////////////////////////////
$tmppref=array();
foreach($prefecture as $k=>$v){
	$tmppref[$v] = $k;
}
$prefname = "";

if(preg_match('/akita-search/', $uri)){
	foreach($tmppref as $k=>$v){
		if(preg_match('/秋田/',$k)){
			$prefname = $k;
			$pref = $v;
		}
	}
}else if(preg_match('/aomori-search/', $uri)){
	foreach($tmppref as $k=>$v){
		if(preg_match('/青森/',$k)){
			$prefname = $k;
			$pref = $v;
		}
	}
}else if(preg_match('/iwate-search/', $uri)){
	foreach($tmppref as $k=>$v){
		if(preg_match('/岩手/',$k)){
			$prefname = $k;
			$pref = $v;
		}
	}
}else if(preg_match('/miyagi-search/', $uri)){
	foreach($tmppref as $k=>$v){
		if(preg_match('/宮城/',$k)){
			$prefname = $k;
			$pref = $v;
		}
	}
}else if(preg_match('/yamagata-search/', $uri)){
	foreach($tmppref as $k=>$v){
		if(preg_match('/山形/',$k)){
			$prefname = $k;
			$pref = $v;
		}
	}
}else if(preg_match('/fukushima-search/', $uri)){
	foreach($tmppref as $k=>$v){
		if(preg_match('/福島/',$k)){
			$prefname = $k;
			$pref = $v;
		}
	}
}else if(preg_match('/kitakantou-search/', $uri)){
	foreach($tmppref as $k=>$v){
		if(preg_match('/茨城/',$k) || preg_match('/栃木/',$k)){
			$prefname = "茨城県・栃木県";
			$pref .= $v.",";
		}
	}
	$pref = rtrim($pref,",");
}else if(preg_match('/l_miyagi/', $uri)){
	foreach($tmppref as $k=>$v){
		if(preg_match('/宮城/',$k)){
			$prefname = $k;
			$pref = $v;
		}
	}
}else if(preg_match('/l_sendai/', $uri)){
	foreach($tmppref as $k=>$v){
		if(preg_match('/宮城/',$k)){
			$prefname = $k;
			$pref = $v;
		}
	}
}else if(preg_match('/l_iwate/', $uri)){
	foreach($tmppref as $k=>$v){
		if(preg_match('/岩手/',$k)){
			$prefname = $k;
			$pref = $v;
		}
	}
}else if(preg_match('/l_fukushima/', $uri)){
	foreach($tmppref as $k=>$v){
		if(preg_match('/福島/',$k)){
			$prefname = $k;
			$pref = $v;
		}
	}
}else if(preg_match('/l_yamagata/', $uri)){
	foreach($tmppref as $k=>$v){
		if(preg_match('/山形/',$k)){
			$prefname = $k;
			$pref = $v;
		}
	}
}else if(preg_match('/l_hurex/', $uri)){
	foreach($tmppref as $k=>$v){
		if(!preg_match('/東京/',$k) && !preg_match('/神奈川/',$k) && !preg_match('/群馬/',$k) && !preg_match('/大阪/',$k) && !preg_match('/北海道/',$k) && !preg_match('/茨城/',$k) && !preg_match('/栃木/',$k) && !preg_match('/埼玉/',$k) && !preg_match('/静岡/',$k)){
			$prefname = "その他地域";
			$pref .= $v.",";
		}
	}
	$pref = rtrim($pref,",");
}else if(preg_match('/other-search/', $uri)){
	foreach($tmppref as $k=>$v){
		if(!preg_match('/秋田/',$k) && !preg_match('/青森/',$k) && !preg_match('/秋田/',$k) && !preg_match('/岩手/',$k) && !preg_match('/宮城/',$k) && !preg_match('/山形/',$k) && !preg_match('/福島/',$k) && !preg_match('/茨城/',$k) && !preg_match('/栃木/',$k)){
			$prefname = "その他地域";
			$pref .= $v.",";
		}
	}
	$pref = rtrim($pref,",");
}

/////////////////////////////////////////////////
// 都道府県ここまで
/////////////////////////////////////////////////

////////////////////////////////////////////////////////////
//パラメータ設定
////////////////////////////////////////////////////////////

//業種
$tmpbiz="";
$bizSearch;
if(!empty($biz1)){
	$tmpbiz .= $biz1 .",";
}
if(!empty($biz2)){
	$tmpbiz .= $biz2 .",";
}
if(!empty($biz3)){
	$tmpbiz .= $biz3 .",";
}
$bizSql = " left join clients as c on j.client_id = c.c_id ";
$bizSql2 = ", group_concat(distinct c.c_industry_name separator '|') as i_name";
/*
if($param["industry"]){
	$param["industry"] = rtrim($param['industry'],",");;
	$bizSearch = " and FIND_IN_SET(c.c_industry_id, :industry)";
}
*/
$tmpbiz =  rtrim($tmpbiz,",");
$bizSearch="";
$bizParam="";
$bizcnt=0;
if(!empty($tmpbiz)){
	$arybiz = explode(",", $tmpbiz);
	foreach($arybiz as $k=>$v){
		if(empty($bizSearch)) {
			$bizSearch.=" and ( ";
		}else{
			$bizSearch.=" or ";
		}
		$bizParam = ':industry'.$k;
		$param["industry" . $k] = $v;
		$bizSearch .= " FIND_IN_SET(" . $bizParam . ",c.c_industry_id)";
	}
}
if($bizSearch) $bizSearch .= " ) ";

//職種
$tmpjob="";
//$jobSearch;
$jobcnt=0;
if(!empty($job1)){
	$ary1 = explode(",", $job1);
	foreach($ary1 as $k=>$v){
		$tmpjob .= $v .",";
	}
}
/*
if(!empty($job2)){
	$ary2 = explode(",", $job2);
	foreach($ary2 as $k=>$v){
		$tmpjob .= $v .",";
	}
}
if(!empty($job3)){
	$ary3 = explode(",", $job3);
	foreach($ary3 as $k=>$v){
		$tmpjob .= $v .",";
	}
}
*/
/*
if($param["jobcategory"]){
	$param["jobcategory"] = rtrim($param["jobcategory"],",");
	$jobSearch = " and FIND_IN_SET(j.jobcategory_id ,:jobcategory ) ";
}
*/
$tmpjob =  rtrim($tmpjob,",");
$jobSearch="";
$jcParam="";
$jccnt=0;
if(!empty($tmpjob)){
	$aryjc = explode(",", $tmpjob);
	foreach($aryjc as $k=>$v){
		if(empty($jobSearch)) {
			$jobSearch.=" and ( ";
		}else{
			$jobSearch.=" or ";
		}
		$jcParam = ':jobcategory'.$k;
		$param["jobcategory" . $k] = $v;
		$jobSearch .= " FIND_IN_SET(" . $jcParam . ",j.jobcategory_id)";
	}
}
if($jobSearch) $jobSearch .= " ) ";

//勤務地
$tmppref=array();
$prefSearch="";
$prefParam="";
$prefcnt=0;
if(!empty($pref)){
	$aryp = explode(",", $pref);
	foreach($aryp as $k=>$v){
		if(empty($prefSearch)) {
			$prefSearch.=" and ( ";
		}else{
			$prefSearch.=" or ";
		}
		$prefParam = ':prefecture_id'.$k;
		$param["prefecture_id" . $k] = $v;
		$prefSearch .= " FIND_IN_SET(" . $prefParam . ",j.prefecture_id)";
	}
}
if($prefSearch) $prefSearch .= " ) ";

//雇用形態
$tmpemploy=array();
$employSearch;
$employcnt=0;
if(!empty($employ_ary)){
	foreach($employ_ary as $k=>$v){
		$param["employ_id"] .= $v . ",";
	}
}
if(	array_key_exists("employ_id",$param) && $param["employ_id"]){
	$param["employ_id"] = rtrim($param["employ_id"],",");
	$employSearch = " and FIND_IN_SET(j.employ_id, :employ_id) ";
}else{
	$employSearch 		= "";
}

//給与
$incomeSearch;
if(!empty($year_income)){
	$param["minsalary"] = $year_income;
	$incomeSearch = " and j.minsalary >= :minsalary ";
}else{
	$incomeSearch = "";
}

//キーワード
$key="";
$keySearch = "";
if(!empty($keyword)){
	$key = $keyword;
	$key = mb_convert_kana($key, 's');
	$keyArr = preg_split('/[\s]+/', $key, -1, PREG_SPLIT_NO_EMPTY);
//	$keyArr = preg_split('/,/', $key, -1, PREG_SPLIT_NO_EMPTY);

	$keySearch .= "and (";
	foreach($keyArr as $k=> $v){
		$param['keyname'.$k] = "%" . $v . "%";
		if($k==0){
			$keySearch .= " concat(j.job_title,' ', j.background,' ', j.summary, ' ', j.area_detail, ' ', j.experience, ' ', j.qualification, ' ', j.worktime, ' ', j.holiday, ' ', j.benefits, ' ', j.employ_name) like :keyname".$k." ";
		}else{
			$keySearch .= $keyword_flg . " concat(j.job_title,' ', j.background,' ', j.summary, ' ', j.area_detail, ' ', j.experience, ' ', j.qualification, ' ', j.worktime, ' ', j.holiday, ' ', j.benefits, ' ', j.employ_name) like :keyname".$k . " ";
		}
		$order_key[] = " and concat(j.job_title,' ', j.background,' ', j.summary, ' ', j.area_detail, ' ', j.experience, ' ', j.qualification, ' ', j.worktime, ' ', j.holiday, ' ', j.benefits, ' ', j.employ_name) like :keyname".$k." ";
	}

	$keySearch .= " )";
}
/*
if($_SERVER['REMOTE_ADDR'] == "219.117.236.169"){
	echo "<pre>";
	var_dump($param,$order_key);
	echo "</pre>";
}
*/

function sessionClear(){
	unset($_SESSION['biz1']);
	unset($_SESSION['biz2']);
	unset($_SESSION['biz3']);
	unset($_SESSION['job1']);
//	unset($_SESSION['job2']);
//	unset($_SESSION['job3']);
	unset($_SESSION['jobs']);
	unset($_SESSION['pref']);
	unset($_SESSION['employees']);
	unset($_SESSION['keyword']);
	unset($_SESSION['keyword_flg']);
	unset($_SESSION['year_income']);
}