<?php
/*
/  一覧ページのマスターデータ取得用
*/

ini_set( 'default_charset', 'UTF-8' );

//都道府県の設定
$prefecture = array();
$json = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/area.json', true);
$tmppref = json_decode($json,true);

        $tflg=0;
        foreach($tmppref['Item'] as $k=>$v){
            if(!empty($v["Item"])){
                foreach($v['Item'] as $k2=>$v2){
                    if(!empty($v2["Items"]["Item"])){
                        foreach($v2['Items']['Item'] as $k3=>$v3) {
                            if(!preg_match("/Option/",$k3) && !preg_match("/Items/",$k3)){
$prefecture[htmlspecialchars($v3['Option.P_Id'],ENT_QUOTES,'UTF-8')] = htmlspecialchars($v3['Option.P_Name'],ENT_QUOTES,'UTF-8');
                            }else{
                                if($tflg==0){
$prefecture[htmlspecialchars($v2['Items']['Item']['Option.P_Id'],ENT_QUOTES,'UTF-8')] = htmlspecialchars($v2['Items']['Item']['Option.P_Name'],ENT_QUOTES,'UTF-8');
                                    $tflg=1;
                                }
                            }
                        }
                    }
                }
            }
        }


//業種の設定
$biz = array();
$json = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/industry.json', true);
$tmpindustry = json_decode($json,true);

foreach($tmpindustry['Item'] as $k=>$v){
	foreach($v['Item'] as $k2 => $v2){
		$biz[htmlspecialchars($v2['Option.P_Id'],ENT_QUOTES,'UTF-8')] = htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');
	}
}

//職種の設定
$job = array();
$json = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/jobcategory.json', true);
$tmpjc = json_decode($json,true);
$tmpalljob;
foreach($tmpjc['Item'] as $k=>$v){
	foreach($v['Item'] as $k2 => $v2){
		//jobグループ
		$job['opt'.htmlspecialchars($v2['Option.P_Id'],ENT_QUOTES,'UTF-8')] = htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');
		
		foreach($v2['Items'] as $k3 => $v3){
			$tmpalljob="";
			foreach($v3 as $k4 => $v4){
				//job
				$job[htmlspecialchars($v4['Option.P_Id'],ENT_QUOTES,'UTF-8')] = htmlspecialchars($v4['Option.P_Name'],ENT_QUOTES,'UTF-8');
				$tmpalljob .= htmlspecialchars($v4['Option.P_Id'],ENT_QUOTES,'UTF-8') . ",";
			}
			$job[rtrim($tmpalljob, ",")] = htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8')."全般";
		}
	}
}

//雇用形態
$employee = array();
$json = file_get_contents($_SERVER["DOCUMENT_ROOT"] . '/../hrbc/employment.json', true);
$tmpemp = json_decode($json,true);

foreach($tmpemp['Item'] as $k=>$v){
	foreach($v['Item'] as $k2 => $v2){
		$employee[htmlspecialchars($v2['Option.P_Id'],ENT_QUOTES,'UTF-8')] = htmlspecialchars($v2['Option.P_Name'],ENT_QUOTES,'UTF-8');
	}
}