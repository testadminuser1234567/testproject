        <form method ="post" name="searchpage" action="./">
			<p>下記絞り込み検索にてご希望のご条件を選択して下さい。職種・業種は複数の選択が可能です。</p>
			<table width="650" class="normal" id="condition" summary="検索条件">
				<tbody>
					<tr>
						<th>業種</th>
						<td>
							<p><select name="biz1">
								<option value="">--------</option>
								<?php foreach($biz as $k => $v):?>
								<option value="<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['biz1'] == $k):?>selected<?php endif;?>><?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?>全般</option>
								<?php endforeach;?>
							</select></p>
							<p><select name="biz2">
								<option value="">--------</option>
								<?php foreach($biz as $k => $v):?>
								<option value="<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['biz2'] == $k):?>selected<?php endif;?>><?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?>全般</option>
								<?php endforeach;?>
							</select></p>
							<p><select name="biz3">
								<option value="">--------</option>
								<?php foreach($biz as $k => $v):?>
								<option value="<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['biz3'] == $k):?>selected<?php endif;?>><?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?>全般</option>
								<?php endforeach;?>
							</select></p>
							業種は3つまで選べます。
						</td>
					</tr>
					<tr id="item02">
						<th>職種</th>
						<td>
							<p>
								<select name="job1" disabled>
									<option value="">--------</option>
									<option value="" selected><?php echo htmlspecialchars($shokushu, ENT_QUOTES, 'UTF-8');?></option>
								</select>
							</p>
								別の職種で求人を探す方はこちら： <a href = "../job-search/"><U>求人検索</U></a>
						</td>
					</tr>
					<tr id="item03">
						<th>勤務地</th>
						<td>
<select name="pref">
<option value="">--------</option>
<?php foreach($prefecture as $k => $v):?>
<option value="<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" <?php if($_SESSION['pref'] == $k):?>selected<?php endif;?>><?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?></option>
<?php endforeach;?>
</select>
																				</td>
					</tr>
					<tr id="item04">
						<th>雇用形態</th>
						<td>
							<?php foreach($employee as $k => $v):?>
							<input type="checkbox" class="rc" name="employee[]" value="<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" id="emptypeid<?php echo htmlspecialchars($k, ENT_QUOTES, 'UTF-8');?>" <?php if(!empty($_SESSION['employees']))if(in_array($k, $_SESSION['employees'])):?>checked<?php endif;?> ><label for="employee"><?php echo htmlspecialchars($v, ENT_QUOTES, 'UTF-8');?></label>
							&nbsp;&nbsp;&nbsp;
							<?php endforeach;?>
						</td>
					</tr>
					<tr id="item05">
						<th>キーワード</th>
						<td>
							<input type="text" name="keyword" value="<?php echo htmlspecialchars($_SESSION['keyword'], ENT_QUOTES, 'UTF-8');?>" class="txtM"  style="width:200px;" />
							（スペース区切り
							<label style="margin-left:10px"><input type="radio" name="keyword_flg" value="or" <?php if($_SESSION['keyword_flg']=="or" || empty($_SESSION['keyword_flg'])):?>checked<?php endif;?>>or</label>
							<label><input type="radio" name="keyword_flg" value="and" <?php if($_SESSION['keyword_flg']=="and"):?>checked<?php endif;?>>and</label>）
						</td>
					</tr>
					<tr id="item07">
						<th class="ico07">給与条件</th>
						<td>
							年収
							<select name="year_income">
								<option value=""  selected>---</option>
								<option value="100" <?php if($_SESSION['year_income'] == 100):?>selected<?php endif;?>>100万円</option>
								<option value="200" <?php if($_SESSION['year_income'] == 200):?>selected<?php endif;?>>200万円</option>
								<option value="300" <?php if($_SESSION['year_income'] == 300):?>selected<?php endif;?>>300万円</option>
								<option value="400" <?php if($_SESSION['year_income'] == 400):?>selected<?php endif;?>>400万円</option>
								<option value="500" <?php if($_SESSION['year_income'] == 500):?>selected<?php endif;?>>500万円</option>
								<option value="600" <?php if($_SESSION['year_income'] == 600):?>selected<?php endif;?>>600万円</option>
								<option value="700" <?php if($_SESSION['year_income'] == 700):?>selected<?php endif;?>>700万円</option>
								<option value="800" <?php if($_SESSION['year_income'] == 800):?>selected<?php endif;?>>800万円</option>
								<option value="900" <?php if($_SESSION['year_income'] == 900):?>selected<?php endif;?>>900万円</option>
								<option value="1000" <?php if($_SESSION['year_income'] == 1000):?>selected<?php endif;?>>1000万円</option>
							</select>以上
						</td>
					</tr>
				</tbody>
			</table>
			<p class="aCenter"><input type="image" src="../images/search_btn_click.gif" alt="" value="この条件で検索する"onClick="document.searchpage.PAGENO.value='1';document.searchpage.submit();"></p>
		</form>
	</div>
