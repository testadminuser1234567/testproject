<div id="searchPrefTab">
<ul id="searchPrefTabNavi" class="clearfix">
<li><a href="#tabBox01" class="nopscr" id="tab01"><span>宮城県</span></a></li>
<li><a href="#tabBox02" class="nopscr" id="tab02"><span>青森県</span></a></li>
<li><a href="#tabBox03" class="nopscr" id="tab03"><span>秋田県</span></a></li>
<li><a href="#tabBox04" class="nopscr" id="tab04"><span>岩手県</span></a></li>
<li><a href="#tabBox05" class="nopscr" id="tab05"><span>山形県</span></a></li>
<li><a href="#tabBox06" class="nopscr" id="tab06"><span>福島県</span></a></li>
<li><a href="#tabBox07" class="nopscr" id="tab07"><span>北関東</span></a></li>
<li><a href="#tabBox08" class="nopscr" id="tab08"><span>その他の県</span></a></li>
</ul>
<div id="searchPrefTabAll">
<div id="searchPrefTabSub">
<?php
	$today = strtotime(date('Y-m-d'));
?>
<!--　　宮城　　-->

<div id="tabBox01" class="tabSub">
<?php if(!empty($miyagi)):?>
<?php foreach($miyagi as $k => $v):?>
<!-- entry start -->
<div class="entry clearfix">
<p class="date"><?php echo str_replace('-','.',substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));?></p>
<p class="title"><a href="./job-search/detail.html?id=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></a>
<?php 
$date = strtotime(substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));
$dayDiff = abs($today - $date) / 86400; //(60 * 60 * 24)
if ($dayDiff < 7):?>
<!--
<img src="images/ico_new.gif" />
-->
<?php endif;?>
</p>
</div>
<!-- entry end -->
<?php endforeach;?>
<?php else:?>
<div class="entry clearfix">
<p>現在イチオシ求人情報はございません。</p>
</div>
<?php endif;?>
<p class="aCenter"><a href="miyagi-search/"><img src="images/btn_search_miyagi.gif" /></a></p>
</div>

<!--　　青森　　-->
<div id="tabBox02" class="tabSub" style="display:none;">
<?php if(!empty($aomori)):?>
<?php foreach($aomori as $k => $v):?>
<!-- entry start -->
<div class="entry clearfix">
<p class="date"><?php echo str_replace('-','.',substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));?></p>
<p class="title"><a href="./job-search/detail.html?id=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></a>
<?php 
$date = strtotime(substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));
$dayDiff = abs($today - $date) / 86400; //(60 * 60 * 24)
if ($dayDiff < 7):?>
<!--
<img src="images/ico_new.gif" />
-->

<?php endif;?>
</p>
</div>
<!-- entry end -->
<?php endforeach;?>
<?php else:?>
<div class="entry clearfix">
<p>現在イチオシ求人情報はございません。</p>
</div>
<?php endif;?>
<p class="aCenter"><a href="aomori-search/"><img src="images/btn_search_aomori.gif" /></a></p>
</div>

<!--　　秋田　　-->
<div id="tabBox03" class="tabSub" style="display:none;">
<?php if(!empty($akita)):?>
<?php foreach($akita as $k => $v):?>
<!-- entry start -->
<div class="entry clearfix">
<p class="date"><?php echo str_replace('-','.',substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));?></p>
<p class="title"><a href="./job-search/detail.html?id=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></a>
<?php 
$date = strtotime(substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));
$dayDiff = abs($today - $date) / 86400; //(60 * 60 * 24)
if ($dayDiff < 7):?>
<!--
<img src="images/ico_new.gif" />
-->

<?php endif;?>
</p>
</div>
<!-- entry end -->
<?php endforeach;?>
<?php else:?>
<div class="entry clearfix">
<p>現在イチオシ求人情報はございません。</p>
</div>
<?php endif;?>
<p class="aCenter"><a href="akita-search/"><img src="images/btn_search_akita.gif" /></a></p>
</div>



<!--　　岩手　　-->
<div id="tabBox04" class="tabSub" style="display:none;">
<?php if(!empty($iwate)):?>
<?php foreach($iwate as $k => $v):?>
<!-- entry start -->
<div class="entry clearfix">
<p class="date"><?php echo str_replace('-','.',substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));?></p>
<p class="title"><a href="./job-search/detail.html?id=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></a>
<?php 
$date = strtotime(substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));
$dayDiff = abs($today - $date) / 86400; //(60 * 60 * 24)
if ($dayDiff < 7):?>
<!--
<img src="images/ico_new.gif" />
-->

<?php endif;?>
</p>
</div>
<!-- entry end -->
<?php endforeach;?>
<?php else:?>
<div class="entry clearfix">
<p>現在イチオシ求人情報はございません。</p>
</div>
<?php endif;?>
<p class="aCenter"><a href="iwate-search/"><img src="images/btn_search_iwate.gif" /></a></p>
</div>



<!--　　山形　　-->
<div id="tabBox05" class="tabSub" style="display:none;">
<?php if(!empty($yamagata)):?>
<?php foreach($yamagata as $k => $v):?>
<!-- entry start -->
<div class="entry clearfix">
<p class="date"><?php echo str_replace('-','.',substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));?></p>
<p class="title"><a href="./job-search/detail.html?id=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></a>
<?php 
$date = strtotime(substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));
$dayDiff = abs($today - $date) / 86400; //(60 * 60 * 24)
if ($dayDiff < 7):?>
<!--
<img src="images/ico_new.gif" />
-->

<?php endif;?>
</p>
</div>
<!-- entry end -->
<?php endforeach;?>
<?php else:?>
<div class="entry clearfix">
<p>現在イチオシ求人情報はございません。</p>
</div>
<?php endif;?>
<p class="aCenter"><a href="yamagata-search/"><img src="images/btn_search_yamagata.gif" /></a></p>
</div>

<!--　　福島　　-->
<div id="tabBox06" class="tabSub" style="display:none;">
<?php if(!empty($fukushima)):?>
<?php foreach($fukushima as $k => $v):?>
<!-- entry start -->
<div class="entry clearfix">
<p class="date"><?php echo str_replace('-','.',substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));?></p>
<p class="title"><a href="./job-search/detail.html?id=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></a>
<?php 
$date = strtotime(substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));
$dayDiff = abs($today - $date) / 86400; //(60 * 60 * 24)
if ($dayDiff < 7):?>
<!--
<img src="images/ico_new.gif" />
-->

<?php endif;?>
</p>
</div>
<!-- entry end -->
<?php endforeach;?>
<?php else:?>
<div class="entry clearfix">
<p>現在イチオシ求人情報はございません。</p>
</div>
<?php endif;?>
<p class="aCenter"><a href="fukushima-search/"><img src="images/btn_search_fukushima.gif" /></a></p>
</div>


<div id="tabBox07" class="tabSub" style="display:none;">
<?php if(!empty($kitakanto)):?>
<?php foreach($kitakanto as $k => $v):?>
<!-- entry start -->
<div class="entry clearfix">
<p class="date"><?php echo str_replace('-','.',substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));?></p>
<p class="title"><a href="./job-search/detail.html?id=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></a>
<?php 
$date = strtotime(substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));
$dayDiff = abs($today - $date) / 86400; //(60 * 60 * 24)
if ($dayDiff < 7):?>
<!--
<img src="images/ico_new.gif" />
-->

<?php endif;?>
</p>
</div>
<!-- entry end -->
<?php endforeach;?>
<?php else:?>
<div class="entry clearfix">
<p>現在イチオシ求人情報はございません。</p>
</div>
<?php endif;?>
<p class="aCenter"><a href="kitakantou-search/"><img src="images/btn_search_kitakantou.gif" /></a></p>
</div>


<div id="tabBox08" class="tabSub" style="display:none;">
<?php if(!empty($other)):?>
<?php foreach($other as $k => $v):?>
<!-- entry start -->
<div class="entry clearfix">
<p class="date"><?php echo str_replace('-','.',substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));?></p>
<p class="title"><a href="./job-search/detail.html?id=<?php echo htmlspecialchars($v['job_id'], ENT_QUOTES, 'UTF-8');?>"><?php echo htmlspecialchars($v['job_title'], ENT_QUOTES, 'UTF-8');?></a>
<?php 
$date = strtotime(substr(htmlspecialchars($v['j_updated'], ENT_QUOTES, 'UTF-8'),0,10));
$dayDiff = abs($today - $date) / 86400; //(60 * 60 * 24)
if ($dayDiff < 7):?>
<!--
<img src="images/ico_new.gif" />
-->

<?php endif;?>
</p>
</div>
<!-- entry end -->
<?php endforeach;?>
<?php else:?>
<div class="entry clearfix">
<p>現在イチオシ求人情報はございません。</p>
</div>
<?php endif;?>
<p class="aCenter"><a href="other-search/"><img src="images/btn_search_other.gif" /></a></p>
</div>





</div>
</div>
