<?php
//////////////////////////////////////////////////////////////////////
// 認証
//////////////////////////////////////////////////////////////////////

// リクエストを行うURLの指定 
$url = "https://api-hrbc-jp.porterscloud.com/v1/oauth"; 

/*
//送信パラメータ
$param = array('app_id'=>'725d6f07ddaede98231a01f7783a78ef','response_type'=>'code_direct');

$data = http_build_query($param);

$unit_url = $url . "?" . $data;

$opts = array(
	'http'=>array(
		'method' => 'GET'
		)
	);
$context = stream_context_create($opts);

$xml = file_get_contents($unit_url, false, $context);
$xml = simplexml_load_string($xml);
*/
            $url = $url .  "?app_id=725d6f07ddaede98231a01f7783a78ef&response_type=code_direct";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8'),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);
$json = json_encode($xml);
$jsonD = json_decode($json,true);
$code = $jsonD["Code"];



//////////////////////////////////////////////////////////////////////
// アクセストークン
//////////////////////////////////////////////////////////////////////

$url = "https://api-hrbc-jp.porterscloud.com/v1/token"; 
/*
//送信パラメータ
$param = array('code'=>$code, 'grant_type'=>'oauth_code','app_id'=>'725d6f07ddaede98231a01f7783a78ef','secret'=>'9ea5f52d29da05f55f9fee47cd0f55b9bba28ebe9c64d8d3a51bdc7a0d11713d');

$data = http_build_query($param);

$unit_url = $url . "?" . $data;

$opts = array(
	'http'=>array(
		'method' => 'GET',
		)
	);
$context = stream_context_create($opts);

$xml = file_get_contents($unit_url, false, $context);
*/
            $url = $url .  "?code=".$code."&grant_type=oauth_code&app_id=725d6f07ddaede98231a01f7783a78ef&secret=9ea5f52d29da05f55f9fee47cd0f55b9bba28ebe9c64d8d3a51bdc7a0d11713d";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8'),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

//$xml = simplexml_load_string($xml);
$json = json_encode($xml);
$jsonD = json_decode($json,true);
$token = $jsonD['AccessToken'];



//////////////////////////////////////////////////////////////////////
// パーティション
//////////////////////////////////////////////////////////////////////

$url = "https://api-hrbc-jp.porterscloud.com/v1/partition"; 
/*
//送信パラメータ
$param = array('request_type'=>1);

$data = http_build_query($param);

$unit_url = $url . "?" . $data;

$opts = array(
	'http'=>array(
		'method' => 'GET',
		'header' => "X-porters-hrbc-oauth-token: $token\r\n"
					. "Content-Type'=>'application/xml; charset=UTF-8\r\n"
		)
	);
$context = stream_context_create($opts);


//xml解析
$xml = file_get_contents($unit_url, false, $context);
$xml = simplexml_load_string($xml);
*/
            $url = "https://api-hrbc-jp.porterscloud.com/v1/partition?request_type=1";
            $options = array
            (
                CURLOPT_URL            => $url,
//        CURLOPT_POST           => true,
//        CURLOPT_POSTFIELDS     => $post,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "UTF-8",
                CURLOPT_HTTPHEADER     => array('Content-Type: application/xml; charset=UTF-8', 'X-porters-hrbc-oauth-token: '. $token),
                CURLINFO_HEADER_OUT    => true,
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $res = curl_exec($ch);
            $info = curl_getinfo($ch);
            $xml = simplexml_load_string($res);

$json = json_encode($xml);
$jsonD = json_decode($json,true);
$partitionId = $jsonD['Item']['Partition.P_Id'];

//////////////////////////////////////////////////////////////////////
// cache化
//////////////////////////////////////////////////////////////////////
$arrs = array(
	"token" => $token,
	"partition" => $partitionId
);

$json = json_encode($arrs);

$cachFile = "/usr/home/ad111u8txo/hrbc/token/cache.json";
/*
if(file_exists($cachFile)){
	unlink($cachFile);
}
*/
$fp = fopen($cachFile, "w" );
fputs($fp, $json);
fclose( $fp );
