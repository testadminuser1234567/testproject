<?php
/*
/  詳細ページのデータ取得用
*/
include($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/site/config.php");


$id = $_POST['job_id'];
$id = htmlspecialchars($id, ENT_QUOTES, 'UTF-8');
if(!is_numeric($id)){
	$id = 0;
}

//パラメータ格納用
$param = array();
$param['phase'] = 6;
$param['publish'] = 11103;
$param['jobid'] = $id;


//データ取得
$sql_list = "SELECT j.job_id, j.job_title, j.client_id, j.keywords,j.ideal, j.background, j.summary, j.area_detail, j.experience, j.qualification, j.worktime, j.holiday, j.benefits, j.minsalary, j.maxsalary, j.jobcategory_name, j.jobcategory_id, j.prefecture_name, j.prefecture_id, j.employ_name, c.c_industry_name as i_name, c.c_industry_id, c.c_naiyo, c.c_title, c.c_employment, j.j_updated FROM jobs as j left join clients as c on j.client_id = c.c_id where j.phaze = :phase and j.publish =:publish and j.job_id = :jobid group by j.job_id";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$datas = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$datas[] = $result;
}
$data = $datas[0];
