<?php
/*
/   キャリアエントリーの送信プログラム
*/
ini_set( 'default_charset', 'UTF-8' );
include($_SERVER["DOCUMENT_ROOT"] . "/../hrbc/site/config.php");
include($dir . "system_entry/mimes.php");

$error="";
//ファイルが無ければ再作成
if(!file_exists($dir . 'token/cache.json')){
	require($dir . 'conf/auth.php');
}
//ファイル時間20分ごとのcronでミスがあったとき
$ftime = filemtime($cpass);
$twminbf = strtotime( "-20 min" );
if($ftime < $twminbf){
	require($dir . 'conf/auth.php');
}

if(empty($token) || empty($partition)){
	require($dir . 'conf/auth.php');
}

//ファイル読込
$json = file_get_contents($dir . 'token/cache.json', true);
$arr = json_decode($json,true);
$token = $arr['token'];
$partition = $arr['partition'];

//HRBCマスターと連動
$tmppref = file_get_contents($dir. 'pref.json', true);
$tmppref = json_decode($tmppref,true);

$tmpgender = file_get_contents($dir . 'gender.json', true);
$tmpgender = json_decode($tmpgender,true);

$tmpbackground = file_get_contents($dir . 'background.json', true);
$tmpbackground = json_decode($tmpbackground,true);

$tmpwork = file_get_contents($dir . 'work.json', true);
$tmpwork = json_decode($tmpwork,true);

$tmparea = file_get_contents($dir . 'area.json', true);
$tmparea = json_decode($tmparea,true);

////////////////////////////////////////////////////////////////////
// レジュメ等設定
////////////////////////////////////////////////////////////////////

/*
//データ１
if(!empty($file1)){
	$file1_dir = "/usr/home/z113109/entry/smart/" . $file1;
	//mime取得
	$extension1 = strtolower(pathinfo($file1_dir, PATHINFO_EXTENSION));
	$mime_type1  = $mimes[$extension1];
	if(is_array($mime_type1)){
		$mime_type1 = $mime_type1[0];
	}
	//base64化
	$fp = fopen($file1_dir, "r");
	$file1_tmp = fread($fp, filesize($file1_dir));
	fclose($fp);
	$file1_data = base64_encode($file1_tmp);
}

if(!empty($file2)){
	//データ２
	$file2_dir = "/usr/home/z113109/entry/smart/" . $file2;
	//mime取得
	$extension2 = strtolower(pathinfo($file2_dir, PATHINFO_EXTENSION));
	$mime_type2  = $mimes[$extension2];
	if(is_array($mime_type2)){
		$mime_type2 = $mime_type2[0];
	}
	//base64化
	$fp = fopen($file2_dir, "r");
	$file2_tmp = fread($fp, filesize($file2_dir));
	fclose($fp);
	$file2_data = base64_encode($file2_tmp);
}
*/

//////////////////////////////////////////////////////////////////////////////////////////
// その他情報設定
//////////////////////////////////////////////////////////////////////////////////////////

//誕生日
$birthday = $year . "/" . $month . "/" . $day;

//性別
foreach($tmpgender['Item'] as $k=>$v){
	foreach($v['Item'] as $k2=>$v2){
		if($sex==$v2['Option.P_Name']){
			$gender = "<" . htmlspecialchars($v2['Option.P_Alias'],ENT_QUOTES,'UTF-8') . "/>";
		}
	}
}

//都道府県
foreach($tmppref['Item'] as $k=>$v){
	foreach($v['Item'] as $k2=>$v2){
		if($pref==$v2['Option.P_Name']){
			$prefecture = "<" . htmlspecialchars($v2['Option.P_Alias'],ENT_QUOTES,'UTF-8') . "/>";
		}
	}
}

//学歴
$school_div="";
foreach($tmpbackground['Item'] as $k=>$v){
	foreach($v['Item'] as $k2=>$v2){
		if($school_div_id==$v2['Option.P_Name']){
			$school_div = "<" . htmlspecialchars($v2['Option.P_Alias'],ENT_QUOTES,'UTF-8') . "/>";
		}
	}
}

//就業状況
foreach($tmpwork['Item'] as $k=>$v){
	foreach($v['Item'] as $k2=>$v2){
		if($jokyo==$v2['Option.P_Name']){
			$present_status = "<" . htmlspecialchars($v2['Option.P_Alias'],ENT_QUOTES,'UTF-8') . "/>";
		}
	}
}

//希望勤務地
$expectarea="";
foreach($tmparea['Item'] as $k=>$v){
    foreach($v['Item'] as $k2=>$v2){
        foreach($v2['Items']['Item'] as $k3=>$v3) {
            if ($expectarea1 == $v3['Option.P_Name']) {
                $expectarea .= "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
            }
            if ($expectarea2 == $v3['Option.P_Name']) {
                $expectarea .= "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
            }
            if ($expectarea3 == $v3['Option.P_Name']) {
                $expectarea .= "<" . htmlspecialchars($v3['Option.P_Alias'], ENT_QUOTES, 'UTF-8') . "/>";
            }
        }
    }
}

//$company_number = $company_number - 1;

//job情報
$param['job_id'] = $job_id;
$sql_list = "SELECT * FROM clients as c left join jobs as j on c.c_id = j.client_id where j.job_id = :job_id";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$datas = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$datas[] = $result;
}
$client_title = $datas[0]['c_title'];

$sql_list = "SELECT * FROM jobs where job_id = :job_id";
$stmt = $dbh->prepare($sql_list);
$stmt->execute($param);
$datas = array();
while($result = $stmt->fetch(PDO::FETCH_ASSOC)){
	$datas[] = $result;
}
$job_title = $datas[0]['job_title'];
$todaydate = Date('Y/m/d');

$hrbcparam = "【エントリー日】" . $todaydate . "\n【求人企業】" . $client_title .  "\n【希望JOB】 " . $job_title . " JOB" . $job_id . "\n【SP】東北Ｕ・Ｉターン転職相談会in東京からのエントリー";


//////////////////////////////////////////////////////////////////////
// HRBC Person
//////////////////////////////////////////////////////////////////////
$url = "https://api-hrbc-jp.porterscloud.com/v1/candidate?partition=".$partition; 

$xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Candidate><Item><Person.P_Id>-1</Person.P_Id><Person.P_Owner>1</Person.P_Owner><Person.P_Name>' . $shimei . '</Person.P_Name><Person.P_Reading>' . $kana . '</Person.P_Reading><Person.P_Telephone>' . $tel1 .'</Person.P_Telephone><Person.P_Street>' . $pref . '</Person.P_Street><Person.P_Mail>' . $mail1 .'</Person.P_Mail></Item></Candidate>';

$header = array(
	"Content-Type: application/xml; charset=UTF-8",
	"X-porters-hrbc-oauth-token: $token"
    );

$options = array('http' => array(
    'method' => 'POST',
    'content' => $xml,
    'header' => implode("\r\n", $header)
));

$xml = file_get_contents($url, false, stream_context_create($options));

//xml解析
$pErr="";
$pId = "";
$xml = simplexml_load_string($xml);
if($xml->Code!=0){
	$pErr=$xml->Code;
}else{
	$pCode="";
	$json = json_encode($xml);
	$arr = json_decode($json,true);
	foreach($arr['Item'] as $k => $v){
		if($k=="Id"){
			$pId = $v;
		}
		if($k=="Code"){
			$pErr = $v;
	
		}
	}
}

if(!$pId || $pErr){
	$error.= "エラーが発生しました。(Person: " . $pErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
}

if(empty($error)){
	///////////////////////////////////////////////////////////////
	// HRBC Resume
	///////////////////////////////////////////////////////////////
	$url = "https://api-hrbc-jp.porterscloud.com/v1/resume?partition=".$partition;

    //※レジュメにメールとTELを入れるとエラーになる
    $xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Resume><Item><Resume.P_Id>-1</Resume.P_Id><Resume.P_Owner>1</Resume.P_Owner><Resume.P_Candidate>'.$pId.'</Resume.P_Candidate><Resume.P_Name>' . $shimei . '</Resume.P_Name><Resume.U_91DBD8B96BAAD6B33B538CF2494377>' . $kana . '</Resume.U_91DBD8B96BAAD6B33B538CF2494377><Resume.P_DateOfBirth>' . $birthday . '</Resume.P_DateOfBirth><Resume.P_Gender>' . $gender . '</Resume.P_Gender><Resume.P_CurrentStatus>' . $present_status . '</Resume.P_CurrentStatus><Resume.P_Memo>' . $comment .'</Resume.P_Memo><Resume.P_ChangeJobsCount>' . $company_number . '</Resume.P_ChangeJobsCount><Resume.U_7F3DF7F61EEF0679AA432646777832>' . $prefecture .'</Resume.U_7F3DF7F61EEF0679AA432646777832><Resume.P_ExpectArea>' . $expectarea . '</Resume.P_ExpectArea><Resume.U_673275A4DA445C9B23AC18BDD7C4DC>' . $school_div . '</Resume.U_673275A4DA445C9B23AC18BDD7C4DC><Resume.U_13C45F7FFAFC1AB430E6038C76125A>' . $hrbcparam . '</Resume.U_13C45F7FFAFC1AB430E6038C76125A></Item></Resume>';

	$header = array(
		"Content-Type: application/xml; charset=UTF-8",
		"X-porters-hrbc-oauth-token: $token"
	    );

	$options = array('http' => array(
	    'method' => 'POST',
	    'content' => $xml,
	    'header' => implode("\r\n", $header)
	));
	$xml = file_get_contents($url, false, stream_context_create($options));

	//xml解析
	$rErr="";
	$rId = "";
	$xml = simplexml_load_string($xml);
	if($xml->Code!=0){
		$rErr=$xml->Code;
	}else{
		$rCode="";
		$json = json_encode($xml);
		$arr = json_decode($json,true);
		foreach($arr['Item'] as $k => $v){
			if($k=="Id"){
				$rId = $v;
			}
			if($k=="Code"){
				$rErr = $v;
			}
		}
	}
	if(!$rId || $rErr){
		$error.= "エラーが発生しました。(Resume: ".$rErr . ")<br />画面をメモして管理者へ問い合わせて下さい。";
	}
}

/*
if(empty($error)){
	///////////////////////////////////////////////////////////////
	// HRBC Attachment
	///////////////////////////////////////////////////////////////
	if(!empty($file1) || !empty($file2)){

		$url = "https://api-hrbc-jp.porterscloud.com/v1/attachment?partition=".$partition; 

		//file1
		$param="";
		if(!empty($file1)){
			$param .= '<Item><Id>-1</Id><Resource>17</Resource><ResourceId>'.$rId.'</ResourceId><FileName>'.$file1.'</FileName><Content>'.$file1_data.'</Content><ContentType>'.$mime_type1.'</ContentType></Item>';
		}
		if(!empty($file2)){
			$param .= '<Item><Id>-1</Id><Resource>17</Resource><ResourceId>'.$rId.'</ResourceId><FileName>'.$file2.'</FileName><Content>'.$file2_data.'</Content><ContentType>'.$mime_type2.'</ContentType></Item>';
		}
	

		$xml ='<?xml version="1.0" encoding="UTF-8" standalone="yes"?><Attachment>' . $param . '</Attachment>';

		$header = array(
			"Content-Type: application/xml; charset=UTF-8",
			"X-porters-hrbc-oauth-token: $token"
		    );
	
		$options = array('http' => array(
		    'method' => 'POST',
		    'content' => $xml,
		    'header' => implode("\r\n", $header)
		));
		$xml = file_get_contents($url, false, stream_context_create($options));
	
		//xml解析
		$aErr;
		$aId = "";
		$xml = simplexml_load_string($xml);

		$aCode="";
		$json = json_encode($xml);
		$arr = json_decode($json,true);
		foreach($arr['Item'] as $k => $v){

			if($k=="Id"){
				$aId = $v;
			}
			if($k=="Code"){
				$aErr = $v;
			}
		}

		if(!$aId){
			$error.= "エラーが発生しました。(Attachment: ".$aErr.")<br />画面をメモして管理者へ問い合わせて下さい。";
		}
	}
}
*/